
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An OrderRefnum is an alternate way of referring to a TransOrder.
 * 
 * <p>Java class for OrderRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrderRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="OrderRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRefnumType", propOrder = {
    "orderRefnumQualifierGid",
    "orderRefnumValue"
})
public class OrderRefnumType {

    @XmlElement(name = "OrderRefnumQualifierGid", required = true)
    protected GLogXMLGidType orderRefnumQualifierGid;
    @XmlElement(name = "OrderRefnumValue", required = true)
    protected String orderRefnumValue;

    /**
     * Gets the value of the orderRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderRefnumQualifierGid() {
        return orderRefnumQualifierGid;
    }

    /**
     * Sets the value of the orderRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderRefnumQualifierGid(GLogXMLGidType value) {
        this.orderRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the orderRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderRefnumValue() {
        return orderRefnumValue;
    }

    /**
     * Sets the value of the orderRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderRefnumValue(String value) {
        this.orderRefnumValue = value;
    }

}

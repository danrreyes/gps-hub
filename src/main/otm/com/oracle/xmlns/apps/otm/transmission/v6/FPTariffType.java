
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             FP_Tarriff is the tariff data from the contract (rate offering) used to rate the shipment associated with this invoice or bill.
 *          
 * 
 * <p>Java class for FPTariffType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FPTariffType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TariffAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IssuingCarrierID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TariffRefnumType"/&gt;
 *         &lt;element name="PrimaryPubAuthority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegulatoryAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffItemNum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TariffItemNumType" minOccurs="0"/&gt;
 *         &lt;element name="TariffItemPart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FreightClassCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffSuppID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffSection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffEffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FPTariffType", propOrder = {
    "tariffAgencyCode",
    "issuingCarrierID",
    "tariffRefnum",
    "primaryPubAuthority",
    "regulatoryAgencyCode",
    "tariffItemNum",
    "tariffItemPart",
    "freightClassCode",
    "tariffSuppID",
    "tariffSection",
    "tariffEffectiveDate"
})
public class FPTariffType {

    @XmlElement(name = "TariffAgencyCode", required = true)
    protected String tariffAgencyCode;
    @XmlElement(name = "IssuingCarrierID")
    protected String issuingCarrierID;
    @XmlElement(name = "TariffRefnum", required = true)
    protected TariffRefnumType tariffRefnum;
    @XmlElement(name = "PrimaryPubAuthority")
    protected String primaryPubAuthority;
    @XmlElement(name = "RegulatoryAgencyCode")
    protected String regulatoryAgencyCode;
    @XmlElement(name = "TariffItemNum")
    protected TariffItemNumType tariffItemNum;
    @XmlElement(name = "TariffItemPart")
    protected String tariffItemPart;
    @XmlElement(name = "FreightClassCode")
    protected String freightClassCode;
    @XmlElement(name = "TariffSuppID")
    protected String tariffSuppID;
    @XmlElement(name = "TariffSection")
    protected String tariffSection;
    @XmlElement(name = "TariffEffectiveDate")
    protected GLogDateTimeType tariffEffectiveDate;

    /**
     * Gets the value of the tariffAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffAgencyCode() {
        return tariffAgencyCode;
    }

    /**
     * Sets the value of the tariffAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffAgencyCode(String value) {
        this.tariffAgencyCode = value;
    }

    /**
     * Gets the value of the issuingCarrierID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuingCarrierID() {
        return issuingCarrierID;
    }

    /**
     * Sets the value of the issuingCarrierID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuingCarrierID(String value) {
        this.issuingCarrierID = value;
    }

    /**
     * Gets the value of the tariffRefnum property.
     * 
     * @return
     *     possible object is
     *     {@link TariffRefnumType }
     *     
     */
    public TariffRefnumType getTariffRefnum() {
        return tariffRefnum;
    }

    /**
     * Sets the value of the tariffRefnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffRefnumType }
     *     
     */
    public void setTariffRefnum(TariffRefnumType value) {
        this.tariffRefnum = value;
    }

    /**
     * Gets the value of the primaryPubAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryPubAuthority() {
        return primaryPubAuthority;
    }

    /**
     * Sets the value of the primaryPubAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryPubAuthority(String value) {
        this.primaryPubAuthority = value;
    }

    /**
     * Gets the value of the regulatoryAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegulatoryAgencyCode() {
        return regulatoryAgencyCode;
    }

    /**
     * Sets the value of the regulatoryAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegulatoryAgencyCode(String value) {
        this.regulatoryAgencyCode = value;
    }

    /**
     * Gets the value of the tariffItemNum property.
     * 
     * @return
     *     possible object is
     *     {@link TariffItemNumType }
     *     
     */
    public TariffItemNumType getTariffItemNum() {
        return tariffItemNum;
    }

    /**
     * Sets the value of the tariffItemNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffItemNumType }
     *     
     */
    public void setTariffItemNum(TariffItemNumType value) {
        this.tariffItemNum = value;
    }

    /**
     * Gets the value of the tariffItemPart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffItemPart() {
        return tariffItemPart;
    }

    /**
     * Sets the value of the tariffItemPart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffItemPart(String value) {
        this.tariffItemPart = value;
    }

    /**
     * Gets the value of the freightClassCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightClassCode() {
        return freightClassCode;
    }

    /**
     * Sets the value of the freightClassCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightClassCode(String value) {
        this.freightClassCode = value;
    }

    /**
     * Gets the value of the tariffSuppID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffSuppID() {
        return tariffSuppID;
    }

    /**
     * Sets the value of the tariffSuppID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffSuppID(String value) {
        this.tariffSuppID = value;
    }

    /**
     * Gets the value of the tariffSection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffSection() {
        return tariffSection;
    }

    /**
     * Sets the value of the tariffSection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffSection(String value) {
        this.tariffSection = value;
    }

    /**
     * Gets the value of the tariffEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTariffEffectiveDate() {
        return tariffEffectiveDate;
    }

    /**
     * Sets the value of the tariffEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTariffEffectiveDate(GLogDateTimeType value) {
        this.tariffEffectiveDate = value;
    }

}

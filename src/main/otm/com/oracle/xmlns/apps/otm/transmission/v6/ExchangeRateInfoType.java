
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the set of exchange rate information to use for currency conversions. This element is supported on the outbound only, except in the
 *             CommercialInvoice.
 *          
 * 
 * <p>Java class for ExchangeRateInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExchangeRateInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExchangeRateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExchangeRateInfoType", propOrder = {
    "exchangeRateGid",
    "exchangeRateDt"
})
public class ExchangeRateInfoType {

    @XmlElement(name = "ExchangeRateGid")
    protected GLogXMLGidType exchangeRateGid;
    @XmlElement(name = "ExchangeRateDt")
    protected GLogDateTimeType exchangeRateDt;

    /**
     * Gets the value of the exchangeRateGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getExchangeRateGid() {
        return exchangeRateGid;
    }

    /**
     * Sets the value of the exchangeRateGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setExchangeRateGid(GLogXMLGidType value) {
        this.exchangeRateGid = value;
    }

    /**
     * Gets the value of the exchangeRateDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExchangeRateDt() {
        return exchangeRateDt;
    }

    /**
     * Sets the value of the exchangeRateDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExchangeRateDt(GLogDateTimeType value) {
        this.exchangeRateDt = value;
    }

}

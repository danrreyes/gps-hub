
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Indicates the special services required to make a delivery. Used to deprecate SpecialService element
 * 
 * <p>Java class for SpclServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpclServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="SpecialServiceDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SpclServiceGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsArbitrary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsForDriverChk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsForEquipChk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsForPUChk" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsForRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PULevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DriverLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ForSourceDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTimeBased" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsIgnorable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PayableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BillableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DriverTimeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpclServiceType", propOrder = {
    "intSavedQuery",
    "specialServiceGid",
    "transactionCode",
    "specialServiceDesc",
    "spclServiceGroupGid",
    "modeProfileGid",
    "isArbitrary",
    "isForDriverChk",
    "isForEquipChk",
    "isForPUChk",
    "isForRating",
    "puLevel",
    "driverLevel",
    "equipLevel",
    "forSourceDest",
    "activityTypeGid",
    "sequenceNumber",
    "isTimeBased",
    "isIgnorable",
    "payableIndicatorGid",
    "billableIndicatorGid",
    "driverTimeType"
})
public class SpclServiceType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "SpecialServiceDesc")
    protected String specialServiceDesc;
    @XmlElement(name = "SpclServiceGroupGid")
    protected GLogXMLGidType spclServiceGroupGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "IsArbitrary")
    protected String isArbitrary;
    @XmlElement(name = "IsForDriverChk")
    protected String isForDriverChk;
    @XmlElement(name = "IsForEquipChk")
    protected String isForEquipChk;
    @XmlElement(name = "IsForPUChk")
    protected String isForPUChk;
    @XmlElement(name = "IsForRating")
    protected String isForRating;
    @XmlElement(name = "PULevel")
    protected String puLevel;
    @XmlElement(name = "DriverLevel")
    protected String driverLevel;
    @XmlElement(name = "EquipLevel")
    protected String equipLevel;
    @XmlElement(name = "ForSourceDest")
    protected String forSourceDest;
    @XmlElement(name = "ActivityTypeGid")
    protected GLogXMLGidType activityTypeGid;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "IsTimeBased")
    protected String isTimeBased;
    @XmlElement(name = "IsIgnorable")
    protected String isIgnorable;
    @XmlElement(name = "PayableIndicatorGid")
    protected GLogXMLGidType payableIndicatorGid;
    @XmlElement(name = "BillableIndicatorGid")
    protected GLogXMLGidType billableIndicatorGid;
    @XmlElement(name = "DriverTimeType")
    protected String driverTimeType;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Sets the value of the specialServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the specialServiceDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialServiceDesc() {
        return specialServiceDesc;
    }

    /**
     * Sets the value of the specialServiceDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialServiceDesc(String value) {
        this.specialServiceDesc = value;
    }

    /**
     * Gets the value of the spclServiceGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpclServiceGroupGid() {
        return spclServiceGroupGid;
    }

    /**
     * Sets the value of the spclServiceGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpclServiceGroupGid(GLogXMLGidType value) {
        this.spclServiceGroupGid = value;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the isArbitrary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsArbitrary() {
        return isArbitrary;
    }

    /**
     * Sets the value of the isArbitrary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsArbitrary(String value) {
        this.isArbitrary = value;
    }

    /**
     * Gets the value of the isForDriverChk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForDriverChk() {
        return isForDriverChk;
    }

    /**
     * Sets the value of the isForDriverChk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForDriverChk(String value) {
        this.isForDriverChk = value;
    }

    /**
     * Gets the value of the isForEquipChk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForEquipChk() {
        return isForEquipChk;
    }

    /**
     * Sets the value of the isForEquipChk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForEquipChk(String value) {
        this.isForEquipChk = value;
    }

    /**
     * Gets the value of the isForPUChk property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForPUChk() {
        return isForPUChk;
    }

    /**
     * Sets the value of the isForPUChk property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForPUChk(String value) {
        this.isForPUChk = value;
    }

    /**
     * Gets the value of the isForRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsForRating() {
        return isForRating;
    }

    /**
     * Sets the value of the isForRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsForRating(String value) {
        this.isForRating = value;
    }

    /**
     * Gets the value of the puLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPULevel() {
        return puLevel;
    }

    /**
     * Sets the value of the puLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPULevel(String value) {
        this.puLevel = value;
    }

    /**
     * Gets the value of the driverLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverLevel() {
        return driverLevel;
    }

    /**
     * Sets the value of the driverLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverLevel(String value) {
        this.driverLevel = value;
    }

    /**
     * Gets the value of the equipLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipLevel() {
        return equipLevel;
    }

    /**
     * Sets the value of the equipLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipLevel(String value) {
        this.equipLevel = value;
    }

    /**
     * Gets the value of the forSourceDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForSourceDest() {
        return forSourceDest;
    }

    /**
     * Sets the value of the forSourceDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForSourceDest(String value) {
        this.forSourceDest = value;
    }

    /**
     * Gets the value of the activityTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityTypeGid() {
        return activityTypeGid;
    }

    /**
     * Sets the value of the activityTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityTypeGid(GLogXMLGidType value) {
        this.activityTypeGid = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the isTimeBased property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTimeBased() {
        return isTimeBased;
    }

    /**
     * Sets the value of the isTimeBased property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTimeBased(String value) {
        this.isTimeBased = value;
    }

    /**
     * Gets the value of the isIgnorable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsIgnorable() {
        return isIgnorable;
    }

    /**
     * Sets the value of the isIgnorable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsIgnorable(String value) {
        this.isIgnorable = value;
    }

    /**
     * Gets the value of the payableIndicatorGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPayableIndicatorGid() {
        return payableIndicatorGid;
    }

    /**
     * Sets the value of the payableIndicatorGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPayableIndicatorGid(GLogXMLGidType value) {
        this.payableIndicatorGid = value;
    }

    /**
     * Gets the value of the billableIndicatorGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBillableIndicatorGid() {
        return billableIndicatorGid;
    }

    /**
     * Sets the value of the billableIndicatorGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBillableIndicatorGid(GLogXMLGidType value) {
        this.billableIndicatorGid = value;
    }

    /**
     * Gets the value of the driverTimeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverTimeType() {
        return driverTimeType;
    }

    /**
     * Sets the value of the driverTimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverTimeType(String value) {
        this.driverTimeType = value;
    }

}

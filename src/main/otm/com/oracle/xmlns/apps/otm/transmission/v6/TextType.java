
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to specify the use of a text template.
 *             On inbound, either TextTemplateGid or TextTemplate can be specified.
 *             On outbound, only TextTemplateGid is provided.
 *             TextPatternOverride is inbound only.
 *             TextInstance is outbound only.
 *             TextPatternOverride and TextItem are currently applicable only to Documents.
 *          
 * 
 * <p>Java class for TextType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TextType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="TextTemplateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;element name="TextTemplate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextTemplateType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="TextDocumentDefinition" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextDocumentDefinitionType" minOccurs="0"/&gt;
 *         &lt;element name="TextPatternOverride" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TextItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextItemType" minOccurs="0"/&gt;
 *         &lt;element name="TextInstance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextType", propOrder = {
    "textTemplateGid",
    "textTemplate",
    "textDocumentDefinition",
    "textPatternOverride",
    "textItem",
    "textInstance",
    "transactionCode"
})
public class TextType {

    @XmlElement(name = "TextTemplateGid")
    protected GLogXMLGidType textTemplateGid;
    @XmlElement(name = "TextTemplate")
    protected TextTemplateType textTemplate;
    @XmlElement(name = "TextDocumentDefinition")
    protected TextDocumentDefinitionType textDocumentDefinition;
    @XmlElement(name = "TextPatternOverride")
    protected String textPatternOverride;
    @XmlElement(name = "TextItem")
    protected TextItemType textItem;
    @XmlElement(name = "TextInstance")
    protected String textInstance;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;

    /**
     * Gets the value of the textTemplateGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTextTemplateGid() {
        return textTemplateGid;
    }

    /**
     * Sets the value of the textTemplateGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTextTemplateGid(GLogXMLGidType value) {
        this.textTemplateGid = value;
    }

    /**
     * Gets the value of the textTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link TextTemplateType }
     *     
     */
    public TextTemplateType getTextTemplate() {
        return textTemplate;
    }

    /**
     * Sets the value of the textTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextTemplateType }
     *     
     */
    public void setTextTemplate(TextTemplateType value) {
        this.textTemplate = value;
    }

    /**
     * Gets the value of the textDocumentDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link TextDocumentDefinitionType }
     *     
     */
    public TextDocumentDefinitionType getTextDocumentDefinition() {
        return textDocumentDefinition;
    }

    /**
     * Sets the value of the textDocumentDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextDocumentDefinitionType }
     *     
     */
    public void setTextDocumentDefinition(TextDocumentDefinitionType value) {
        this.textDocumentDefinition = value;
    }

    /**
     * Gets the value of the textPatternOverride property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextPatternOverride() {
        return textPatternOverride;
    }

    /**
     * Sets the value of the textPatternOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextPatternOverride(String value) {
        this.textPatternOverride = value;
    }

    /**
     * Gets the value of the textItem property.
     * 
     * @return
     *     possible object is
     *     {@link TextItemType }
     *     
     */
    public TextItemType getTextItem() {
        return textItem;
    }

    /**
     * Sets the value of the textItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextItemType }
     *     
     */
    public void setTextItem(TextItemType value) {
        this.textItem = value;
    }

    /**
     * Gets the value of the textInstance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextInstance() {
        return textInstance;
    }

    /**
     * Sets the value of the textInstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextInstance(String value) {
        this.textInstance = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

}

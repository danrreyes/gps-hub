
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * cost details of bulkplan assignment
 * 
 * <p>Java class for FleetBulkPlanCostType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FleetBulkPlanCostType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CostCategoryGid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PreCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AfterCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FleetBulkPlanCostType", propOrder = {
    "costCategoryGid",
    "preCost",
    "afterCost"
})
public class FleetBulkPlanCostType {

    @XmlElement(name = "CostCategoryGid", required = true)
    protected String costCategoryGid;
    @XmlElement(name = "PreCost")
    protected GLogXMLFinancialAmountType preCost;
    @XmlElement(name = "AfterCost")
    protected GLogXMLFinancialAmountType afterCost;

    /**
     * Gets the value of the costCategoryGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostCategoryGid() {
        return costCategoryGid;
    }

    /**
     * Sets the value of the costCategoryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostCategoryGid(String value) {
        this.costCategoryGid = value;
    }

    /**
     * Gets the value of the preCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPreCost() {
        return preCost;
    }

    /**
     * Sets the value of the preCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPreCost(GLogXMLFinancialAmountType value) {
        this.preCost = value;
    }

    /**
     * Gets the value of the afterCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAfterCost() {
        return afterCost;
    }

    /**
     * Sets the value of the afterCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAfterCost(GLogXMLFinancialAmountType value) {
        this.afterCost = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ProtectiveSvc is an element of RailDetail used to specify protective service
 *             and ventilation instructions on a rail specific invoice.
 *          
 * 
 * <p>Java class for ProtectiveSvcType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProtectiveSvcType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProtectiveSvcCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProtectiveSvcRuleCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Temperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TemperatureType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProtectiveSvcType", propOrder = {
    "protectiveSvcCode",
    "protectiveSvcRuleCode",
    "temperature"
})
public class ProtectiveSvcType {

    @XmlElement(name = "ProtectiveSvcCode")
    protected String protectiveSvcCode;
    @XmlElement(name = "ProtectiveSvcRuleCode")
    protected String protectiveSvcRuleCode;
    @XmlElement(name = "Temperature")
    protected TemperatureType temperature;

    /**
     * Gets the value of the protectiveSvcCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtectiveSvcCode() {
        return protectiveSvcCode;
    }

    /**
     * Sets the value of the protectiveSvcCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtectiveSvcCode(String value) {
        this.protectiveSvcCode = value;
    }

    /**
     * Gets the value of the protectiveSvcRuleCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtectiveSvcRuleCode() {
        return protectiveSvcRuleCode;
    }

    /**
     * Sets the value of the protectiveSvcRuleCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtectiveSvcRuleCode(String value) {
        this.protectiveSvcRuleCode = value;
    }

    /**
     * Gets the value of the temperature property.
     * 
     * @return
     *     possible object is
     *     {@link TemperatureType }
     *     
     */
    public TemperatureType getTemperature() {
        return temperature;
    }

    /**
     * Sets the value of the temperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link TemperatureType }
     *     
     */
    public void setTemperature(TemperatureType value) {
        this.temperature = value;
    }

}

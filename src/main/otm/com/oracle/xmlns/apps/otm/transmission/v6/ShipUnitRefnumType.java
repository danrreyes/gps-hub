
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipUnitRefnum is an alternate method for identifying an ShipUnit in a Shipment. It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for ShipUnitRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipUnitRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipUnitRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitRefnumType", propOrder = {
    "shipUnitRefnumQualifierGid",
    "shipUnitRefnumValue"
})
public class ShipUnitRefnumType {

    @XmlElement(name = "ShipUnitRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipUnitRefnumQualifierGid;
    @XmlElement(name = "ShipUnitRefnumValue", required = true)
    protected String shipUnitRefnumValue;

    /**
     * Gets the value of the shipUnitRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitRefnumQualifierGid() {
        return shipUnitRefnumQualifierGid;
    }

    /**
     * Sets the value of the shipUnitRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitRefnumQualifierGid(GLogXMLGidType value) {
        this.shipUnitRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the shipUnitRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitRefnumValue() {
        return shipUnitRefnumValue;
    }

    /**
     * Sets the value of the shipUnitRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitRefnumValue(String value) {
        this.shipUnitRefnumValue = value;
    }

}

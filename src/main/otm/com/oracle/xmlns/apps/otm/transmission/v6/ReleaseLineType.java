
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ReleaseLine specifies and item and quantity that is released.
 *          
 * 
 * <p>Java class for ReleaseLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseLineType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemRefType" minOccurs="0"/&gt;
 *         &lt;element name="OrderBaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OrderBaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InitialItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ItemQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemQuantityType" minOccurs="0"/&gt;
 *         &lt;element name="PackageDimensions" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackageDimensionsType" minOccurs="0"/&gt;
 *         &lt;element name="ManufacturedCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsDrawback" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumLayersPerShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuantityPerLayer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="BuyGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SellGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialInvoiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialInvoiceDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SecondaryWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLineHazmatInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLineHazmatInfoType" minOccurs="0"/&gt;
 *         &lt;element name="BilledQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/&gt;
 *         &lt;element name="PricePerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PricePerUnitType" minOccurs="0"/&gt;
 *         &lt;element name="TotalBilledAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="FreeAlongSide" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="BrandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsSplitAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ItemAttributes" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemAttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OrLineSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLineIntlClassification" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLineIntlClassificationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLineAllocationInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ReleaseLineAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="OrLineEquipRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}THUCapacityEquipRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OrLinePackageRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}THUCapacityPackageRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="PackWithGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseLineType", propOrder = {
    "releaseLineGid",
    "transactionCode",
    "packagedItemRef",
    "orderBaseGid",
    "orderBaseLineGid",
    "initialItemGid",
    "itemQuantity",
    "packageDimensions",
    "manufacturedCountryCode3Gid",
    "isDrawback",
    "packagedItemSpecRef",
    "packagedItemSpecCount",
    "numLayersPerShipUnit",
    "quantityPerLayer",
    "transportHandlingUnitRef",
    "buyGeneralLedgerGid",
    "sellGeneralLedgerGid",
    "commercialInvoiceGid",
    "commercialInvoiceDesc",
    "secondaryWeightVolume",
    "releaseLineHazmatInfo",
    "billedQuantity",
    "pricePerUnit",
    "totalBilledAmount",
    "freeAlongSide",
    "brandName",
    "isSplitAllowed",
    "shipUnitSpecProfileGid",
    "itemAttributes",
    "refnum",
    "remark",
    "orLineSpecialService",
    "involvedParty",
    "releaseLineIntlClassification",
    "text",
    "releaseLineAllocationInfo",
    "orLineEquipRefUnit",
    "orLinePackageRefUnit",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "packWithGroup"
})
public class ReleaseLineType {

    @XmlElement(name = "ReleaseLineGid")
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "PackagedItemRef")
    protected PackagedItemRefType packagedItemRef;
    @XmlElement(name = "OrderBaseGid")
    protected GLogXMLGidType orderBaseGid;
    @XmlElement(name = "OrderBaseLineGid")
    protected GLogXMLGidType orderBaseLineGid;
    @XmlElement(name = "InitialItemGid")
    protected GLogXMLGidType initialItemGid;
    @XmlElement(name = "ItemQuantity")
    protected ItemQuantityType itemQuantity;
    @XmlElement(name = "PackageDimensions")
    protected PackageDimensionsType packageDimensions;
    @XmlElement(name = "ManufacturedCountryCode3Gid")
    protected GLogXMLGidType manufacturedCountryCode3Gid;
    @XmlElement(name = "IsDrawback")
    protected String isDrawback;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "PackagedItemSpecCount")
    protected String packagedItemSpecCount;
    @XmlElement(name = "NumLayersPerShipUnit")
    protected String numLayersPerShipUnit;
    @XmlElement(name = "QuantityPerLayer")
    protected String quantityPerLayer;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "BuyGeneralLedgerGid")
    protected GLogXMLGidType buyGeneralLedgerGid;
    @XmlElement(name = "SellGeneralLedgerGid")
    protected GLogXMLGidType sellGeneralLedgerGid;
    @XmlElement(name = "CommercialInvoiceGid")
    protected GLogXMLGidType commercialInvoiceGid;
    @XmlElement(name = "CommercialInvoiceDesc")
    protected String commercialInvoiceDesc;
    @XmlElement(name = "SecondaryWeightVolume")
    protected WeightVolumeType secondaryWeightVolume;
    @XmlElement(name = "ReleaseLineHazmatInfo")
    protected ReleaseLineHazmatInfoType releaseLineHazmatInfo;
    @XmlElement(name = "BilledQuantity")
    protected GLogXMLFlexQuantityType billedQuantity;
    @XmlElement(name = "PricePerUnit")
    protected PricePerUnitType pricePerUnit;
    @XmlElement(name = "TotalBilledAmount")
    protected GLogXMLFinancialAmountType totalBilledAmount;
    @XmlElement(name = "FreeAlongSide")
    protected GLogXMLFinancialAmountType freeAlongSide;
    @XmlElement(name = "BrandName")
    protected String brandName;
    @XmlElement(name = "IsSplitAllowed")
    protected String isSplitAllowed;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "ItemAttributes")
    protected List<ItemAttributeType> itemAttributes;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "OrLineSpecialService")
    protected List<SpecialServiceType> orLineSpecialService;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "ReleaseLineIntlClassification")
    protected List<ReleaseLineIntlClassificationType> releaseLineIntlClassification;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "ReleaseLineAllocationInfo")
    protected ReleaseLineType.ReleaseLineAllocationInfo releaseLineAllocationInfo;
    @XmlElement(name = "OrLineEquipRefUnit")
    protected List<THUCapacityEquipRefUnitType> orLineEquipRefUnit;
    @XmlElement(name = "OrLinePackageRefUnit")
    protected List<THUCapacityPackageRefUnitType> orLinePackageRefUnit;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "PackWithGroup")
    protected String packWithGroup;

    /**
     * Gets the value of the releaseLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Sets the value of the releaseLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the packagedItemRef property.
     * 
     * @return
     *     possible object is
     *     {@link PackagedItemRefType }
     *     
     */
    public PackagedItemRefType getPackagedItemRef() {
        return packagedItemRef;
    }

    /**
     * Sets the value of the packagedItemRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagedItemRefType }
     *     
     */
    public void setPackagedItemRef(PackagedItemRefType value) {
        this.packagedItemRef = value;
    }

    /**
     * Gets the value of the orderBaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderBaseGid() {
        return orderBaseGid;
    }

    /**
     * Sets the value of the orderBaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderBaseGid(GLogXMLGidType value) {
        this.orderBaseGid = value;
    }

    /**
     * Gets the value of the orderBaseLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderBaseLineGid() {
        return orderBaseLineGid;
    }

    /**
     * Sets the value of the orderBaseLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderBaseLineGid(GLogXMLGidType value) {
        this.orderBaseLineGid = value;
    }

    /**
     * Gets the value of the initialItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInitialItemGid() {
        return initialItemGid;
    }

    /**
     * Sets the value of the initialItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInitialItemGid(GLogXMLGidType value) {
        this.initialItemGid = value;
    }

    /**
     * Gets the value of the itemQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link ItemQuantityType }
     *     
     */
    public ItemQuantityType getItemQuantity() {
        return itemQuantity;
    }

    /**
     * Sets the value of the itemQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemQuantityType }
     *     
     */
    public void setItemQuantity(ItemQuantityType value) {
        this.itemQuantity = value;
    }

    /**
     * Gets the value of the packageDimensions property.
     * 
     * @return
     *     possible object is
     *     {@link PackageDimensionsType }
     *     
     */
    public PackageDimensionsType getPackageDimensions() {
        return packageDimensions;
    }

    /**
     * Sets the value of the packageDimensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageDimensionsType }
     *     
     */
    public void setPackageDimensions(PackageDimensionsType value) {
        this.packageDimensions = value;
    }

    /**
     * Gets the value of the manufacturedCountryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getManufacturedCountryCode3Gid() {
        return manufacturedCountryCode3Gid;
    }

    /**
     * Sets the value of the manufacturedCountryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setManufacturedCountryCode3Gid(GLogXMLGidType value) {
        this.manufacturedCountryCode3Gid = value;
    }

    /**
     * Gets the value of the isDrawback property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDrawback() {
        return isDrawback;
    }

    /**
     * Sets the value of the isDrawback property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDrawback(String value) {
        this.isDrawback = value;
    }

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Gets the value of the packagedItemSpecCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemSpecCount() {
        return packagedItemSpecCount;
    }

    /**
     * Sets the value of the packagedItemSpecCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemSpecCount(String value) {
        this.packagedItemSpecCount = value;
    }

    /**
     * Gets the value of the numLayersPerShipUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumLayersPerShipUnit() {
        return numLayersPerShipUnit;
    }

    /**
     * Sets the value of the numLayersPerShipUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumLayersPerShipUnit(String value) {
        this.numLayersPerShipUnit = value;
    }

    /**
     * Gets the value of the quantityPerLayer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantityPerLayer() {
        return quantityPerLayer;
    }

    /**
     * Sets the value of the quantityPerLayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantityPerLayer(String value) {
        this.quantityPerLayer = value;
    }

    /**
     * Gets the value of the transportHandlingUnitRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Sets the value of the transportHandlingUnitRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Gets the value of the buyGeneralLedgerGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBuyGeneralLedgerGid() {
        return buyGeneralLedgerGid;
    }

    /**
     * Sets the value of the buyGeneralLedgerGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBuyGeneralLedgerGid(GLogXMLGidType value) {
        this.buyGeneralLedgerGid = value;
    }

    /**
     * Gets the value of the sellGeneralLedgerGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellGeneralLedgerGid() {
        return sellGeneralLedgerGid;
    }

    /**
     * Sets the value of the sellGeneralLedgerGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellGeneralLedgerGid(GLogXMLGidType value) {
        this.sellGeneralLedgerGid = value;
    }

    /**
     * Gets the value of the commercialInvoiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommercialInvoiceGid() {
        return commercialInvoiceGid;
    }

    /**
     * Sets the value of the commercialInvoiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommercialInvoiceGid(GLogXMLGidType value) {
        this.commercialInvoiceGid = value;
    }

    /**
     * Gets the value of the commercialInvoiceDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialInvoiceDesc() {
        return commercialInvoiceDesc;
    }

    /**
     * Sets the value of the commercialInvoiceDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialInvoiceDesc(String value) {
        this.commercialInvoiceDesc = value;
    }

    /**
     * Gets the value of the secondaryWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getSecondaryWeightVolume() {
        return secondaryWeightVolume;
    }

    /**
     * Sets the value of the secondaryWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setSecondaryWeightVolume(WeightVolumeType value) {
        this.secondaryWeightVolume = value;
    }

    /**
     * Gets the value of the releaseLineHazmatInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseLineHazmatInfoType }
     *     
     */
    public ReleaseLineHazmatInfoType getReleaseLineHazmatInfo() {
        return releaseLineHazmatInfo;
    }

    /**
     * Sets the value of the releaseLineHazmatInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseLineHazmatInfoType }
     *     
     */
    public void setReleaseLineHazmatInfo(ReleaseLineHazmatInfoType value) {
        this.releaseLineHazmatInfo = value;
    }

    /**
     * Gets the value of the billedQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getBilledQuantity() {
        return billedQuantity;
    }

    /**
     * Sets the value of the billedQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setBilledQuantity(GLogXMLFlexQuantityType value) {
        this.billedQuantity = value;
    }

    /**
     * Gets the value of the pricePerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link PricePerUnitType }
     *     
     */
    public PricePerUnitType getPricePerUnit() {
        return pricePerUnit;
    }

    /**
     * Sets the value of the pricePerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricePerUnitType }
     *     
     */
    public void setPricePerUnit(PricePerUnitType value) {
        this.pricePerUnit = value;
    }

    /**
     * Gets the value of the totalBilledAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalBilledAmount() {
        return totalBilledAmount;
    }

    /**
     * Sets the value of the totalBilledAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalBilledAmount(GLogXMLFinancialAmountType value) {
        this.totalBilledAmount = value;
    }

    /**
     * Gets the value of the freeAlongSide property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getFreeAlongSide() {
        return freeAlongSide;
    }

    /**
     * Sets the value of the freeAlongSide property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setFreeAlongSide(GLogXMLFinancialAmountType value) {
        this.freeAlongSide = value;
    }

    /**
     * Gets the value of the brandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Sets the value of the brandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Gets the value of the isSplitAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSplitAllowed() {
        return isSplitAllowed;
    }

    /**
     * Sets the value of the isSplitAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSplitAllowed(String value) {
        this.isSplitAllowed = value;
    }

    /**
     * Gets the value of the shipUnitSpecProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Sets the value of the shipUnitSpecProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the itemAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemAttributeType }
     * 
     * 
     */
    public List<ItemAttributeType> getItemAttributes() {
        if (itemAttributes == null) {
            itemAttributes = new ArrayList<ItemAttributeType>();
        }
        return this.itemAttributes;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the orLineSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orLineSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrLineSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getOrLineSpecialService() {
        if (orLineSpecialService == null) {
            orLineSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.orLineSpecialService;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the releaseLineIntlClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseLineIntlClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseLineIntlClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseLineIntlClassificationType }
     * 
     * 
     */
    public List<ReleaseLineIntlClassificationType> getReleaseLineIntlClassification() {
        if (releaseLineIntlClassification == null) {
            releaseLineIntlClassification = new ArrayList<ReleaseLineIntlClassificationType>();
        }
        return this.releaseLineIntlClassification;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Gets the value of the releaseLineAllocationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseLineType.ReleaseLineAllocationInfo }
     *     
     */
    public ReleaseLineType.ReleaseLineAllocationInfo getReleaseLineAllocationInfo() {
        return releaseLineAllocationInfo;
    }

    /**
     * Sets the value of the releaseLineAllocationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseLineType.ReleaseLineAllocationInfo }
     *     
     */
    public void setReleaseLineAllocationInfo(ReleaseLineType.ReleaseLineAllocationInfo value) {
        this.releaseLineAllocationInfo = value;
    }

    /**
     * Gets the value of the orLineEquipRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orLineEquipRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrLineEquipRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THUCapacityEquipRefUnitType }
     * 
     * 
     */
    public List<THUCapacityEquipRefUnitType> getOrLineEquipRefUnit() {
        if (orLineEquipRefUnit == null) {
            orLineEquipRefUnit = new ArrayList<THUCapacityEquipRefUnitType>();
        }
        return this.orLineEquipRefUnit;
    }

    /**
     * Gets the value of the orLinePackageRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orLinePackageRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrLinePackageRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THUCapacityPackageRefUnitType }
     * 
     * 
     */
    public List<THUCapacityPackageRefUnitType> getOrLinePackageRefUnit() {
        if (orLinePackageRefUnit == null) {
            orLinePackageRefUnit = new ArrayList<THUCapacityPackageRefUnitType>();
        }
        return this.orLinePackageRefUnit;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the packWithGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackWithGroup() {
        return packWithGroup;
    }

    /**
     * Sets the value of the packWithGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackWithGroup(String value) {
        this.packWithGroup = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ReleaseLineAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "releaseLineAllocByType"
    })
    public static class ReleaseLineAllocationInfo {

        @XmlElement(name = "ReleaseLineAllocByType")
        protected List<ReleaseAllocType> releaseLineAllocByType;

        /**
         * Gets the value of the releaseLineAllocByType property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the releaseLineAllocByType property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReleaseLineAllocByType().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReleaseAllocType }
         * 
         * 
         */
        public List<ReleaseAllocType> getReleaseLineAllocByType() {
            if (releaseLineAllocByType == null) {
                releaseLineAllocByType = new ArrayList<ReleaseAllocType>();
            }
            return this.releaseLineAllocByType;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             (Both) PaymentModeDetail is one of MotorDetail, OceanDetail, RailDetail, or GenericDetail.
 *             Its used to convey detail information for a given mode of transportation or generically. In the case of outbound i.e. in case of bill only GenericDetail is used.
 *          
 * 
 * <p>Java class for PaymentModeDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentModeDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="MotorDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MotorDetailType"/&gt;
 *         &lt;element name="OceanDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OceanDetailType"/&gt;
 *         &lt;element name="RailDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailDetailType"/&gt;
 *         &lt;element name="GenericDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericDetailType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentModeDetailType", propOrder = {
    "motorDetail",
    "oceanDetail",
    "railDetail",
    "genericDetail"
})
public class PaymentModeDetailType {

    @XmlElement(name = "MotorDetail")
    protected MotorDetailType motorDetail;
    @XmlElement(name = "OceanDetail")
    protected OceanDetailType oceanDetail;
    @XmlElement(name = "RailDetail")
    protected RailDetailType railDetail;
    @XmlElement(name = "GenericDetail")
    protected GenericDetailType genericDetail;

    /**
     * Gets the value of the motorDetail property.
     * 
     * @return
     *     possible object is
     *     {@link MotorDetailType }
     *     
     */
    public MotorDetailType getMotorDetail() {
        return motorDetail;
    }

    /**
     * Sets the value of the motorDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link MotorDetailType }
     *     
     */
    public void setMotorDetail(MotorDetailType value) {
        this.motorDetail = value;
    }

    /**
     * Gets the value of the oceanDetail property.
     * 
     * @return
     *     possible object is
     *     {@link OceanDetailType }
     *     
     */
    public OceanDetailType getOceanDetail() {
        return oceanDetail;
    }

    /**
     * Sets the value of the oceanDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link OceanDetailType }
     *     
     */
    public void setOceanDetail(OceanDetailType value) {
        this.oceanDetail = value;
    }

    /**
     * Gets the value of the railDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RailDetailType }
     *     
     */
    public RailDetailType getRailDetail() {
        return railDetail;
    }

    /**
     * Sets the value of the railDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RailDetailType }
     *     
     */
    public void setRailDetail(RailDetailType value) {
        this.railDetail = value;
    }

    /**
     * Gets the value of the genericDetail property.
     * 
     * @return
     *     possible object is
     *     {@link GenericDetailType }
     *     
     */
    public GenericDetailType getGenericDetail() {
        return genericDetail;
    }

    /**
     * Sets the value of the genericDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericDetailType }
     *     
     */
    public void setGenericDetail(GenericDetailType value) {
        this.genericDetail = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * SkuTransaction is the xml representation of the sku_transaction, sku_transaction_descriptor,
 *             and sku_transaction_d_remark
 *             tables.
 *          
 * 
 * <p>Java class for SkuTransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SkuTransactionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SkuTransactionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="SkuGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="WarehouseLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SupplierCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OwnerCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Quantity1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Quantity2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SkuTransactionDescriptor" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuTransactionDescriptorType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuTransactionType", propOrder = {
    "skuTransactionGid",
    "skuGid",
    "transactionType",
    "packagedItemGid",
    "warehouseLocationGid",
    "supplierCorporationGid",
    "ownerCorporationGid",
    "quantity1",
    "quantity2",
    "transactionDt",
    "description",
    "skuTransactionDescriptor"
})
public class SkuTransactionType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SkuTransactionGid", required = true)
    protected GLogXMLGidType skuTransactionGid;
    @XmlElement(name = "SkuGid", required = true)
    protected GLogXMLGidType skuGid;
    @XmlElement(name = "TransactionType", required = true)
    protected String transactionType;
    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;
    @XmlElement(name = "WarehouseLocationGid")
    protected GLogXMLGidType warehouseLocationGid;
    @XmlElement(name = "SupplierCorporationGid")
    protected GLogXMLGidType supplierCorporationGid;
    @XmlElement(name = "OwnerCorporationGid")
    protected GLogXMLGidType ownerCorporationGid;
    @XmlElement(name = "Quantity1")
    protected String quantity1;
    @XmlElement(name = "Quantity2")
    protected String quantity2;
    @XmlElement(name = "TransactionDt")
    protected GLogDateTimeType transactionDt;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "SkuTransactionDescriptor")
    protected List<SkuTransactionDescriptorType> skuTransactionDescriptor;

    /**
     * Gets the value of the skuTransactionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuTransactionGid() {
        return skuTransactionGid;
    }

    /**
     * Sets the value of the skuTransactionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuTransactionGid(GLogXMLGidType value) {
        this.skuTransactionGid = value;
    }

    /**
     * Gets the value of the skuGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuGid() {
        return skuGid;
    }

    /**
     * Sets the value of the skuGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuGid(GLogXMLGidType value) {
        this.skuGid = value;
    }

    /**
     * Gets the value of the transactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionType() {
        return transactionType;
    }

    /**
     * Sets the value of the transactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionType(String value) {
        this.transactionType = value;
    }

    /**
     * Gets the value of the packagedItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Sets the value of the packagedItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

    /**
     * Gets the value of the warehouseLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWarehouseLocationGid() {
        return warehouseLocationGid;
    }

    /**
     * Sets the value of the warehouseLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWarehouseLocationGid(GLogXMLGidType value) {
        this.warehouseLocationGid = value;
    }

    /**
     * Gets the value of the supplierCorporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSupplierCorporationGid() {
        return supplierCorporationGid;
    }

    /**
     * Sets the value of the supplierCorporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSupplierCorporationGid(GLogXMLGidType value) {
        this.supplierCorporationGid = value;
    }

    /**
     * Gets the value of the ownerCorporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOwnerCorporationGid() {
        return ownerCorporationGid;
    }

    /**
     * Sets the value of the ownerCorporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOwnerCorporationGid(GLogXMLGidType value) {
        this.ownerCorporationGid = value;
    }

    /**
     * Gets the value of the quantity1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity1() {
        return quantity1;
    }

    /**
     * Sets the value of the quantity1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity1(String value) {
        this.quantity1 = value;
    }

    /**
     * Gets the value of the quantity2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity2() {
        return quantity2;
    }

    /**
     * Sets the value of the quantity2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity2(String value) {
        this.quantity2 = value;
    }

    /**
     * Gets the value of the transactionDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransactionDt() {
        return transactionDt;
    }

    /**
     * Sets the value of the transactionDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransactionDt(GLogDateTimeType value) {
        this.transactionDt = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the skuTransactionDescriptor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the skuTransactionDescriptor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuTransactionDescriptor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuTransactionDescriptorType }
     * 
     * 
     */
    public List<SkuTransactionDescriptorType> getSkuTransactionDescriptor() {
        if (skuTransactionDescriptor == null) {
            skuTransactionDescriptor = new ArrayList<SkuTransactionDescriptorType>();
        }
        return this.skuTransactionDescriptor;
    }

}

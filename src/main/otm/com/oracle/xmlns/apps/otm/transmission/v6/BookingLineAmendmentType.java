
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Will contain either the BookLineAmendViaRelease or the BookLineAmendViaServiceProvider but not both.
 *             The selection is based on the agent used to send the notification for the message.
 *          
 * 
 * <p>Java class for BookingLineAmendmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookingLineAmendmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BookLineAmendViaRelease" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookLineAmendViaReleaseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="BookLineAmendViaServiceProvider" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookLineAmendViaServiceProviderType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CharterVoyage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CharterVoyageType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Release" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingLineAmendmentType", propOrder = {
    "bookLineAmendViaRelease",
    "bookLineAmendViaServiceProvider",
    "charterVoyage",
    "release"
})
public class BookingLineAmendmentType
    extends OTMTransactionOut
{

    @XmlElement(name = "BookLineAmendViaRelease")
    protected List<BookLineAmendViaReleaseType> bookLineAmendViaRelease;
    @XmlElement(name = "BookLineAmendViaServiceProvider")
    protected List<BookLineAmendViaServiceProviderType> bookLineAmendViaServiceProvider;
    @XmlElement(name = "CharterVoyage")
    protected List<CharterVoyageType> charterVoyage;
    @XmlElement(name = "Release")
    protected List<ReleaseType> release;

    /**
     * Gets the value of the bookLineAmendViaRelease property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bookLineAmendViaRelease property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookLineAmendViaRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookLineAmendViaReleaseType }
     * 
     * 
     */
    public List<BookLineAmendViaReleaseType> getBookLineAmendViaRelease() {
        if (bookLineAmendViaRelease == null) {
            bookLineAmendViaRelease = new ArrayList<BookLineAmendViaReleaseType>();
        }
        return this.bookLineAmendViaRelease;
    }

    /**
     * Gets the value of the bookLineAmendViaServiceProvider property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bookLineAmendViaServiceProvider property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookLineAmendViaServiceProvider().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookLineAmendViaServiceProviderType }
     * 
     * 
     */
    public List<BookLineAmendViaServiceProviderType> getBookLineAmendViaServiceProvider() {
        if (bookLineAmendViaServiceProvider == null) {
            bookLineAmendViaServiceProvider = new ArrayList<BookLineAmendViaServiceProviderType>();
        }
        return this.bookLineAmendViaServiceProvider;
    }

    /**
     * Gets the value of the charterVoyage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the charterVoyage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCharterVoyage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CharterVoyageType }
     * 
     * 
     */
    public List<CharterVoyageType> getCharterVoyage() {
        if (charterVoyage == null) {
            charterVoyage = new ArrayList<CharterVoyageType>();
        }
        return this.charterVoyage;
    }

    /**
     * Gets the value of the release property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the release property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRelease().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseType }
     * 
     * 
     */
    public List<ReleaseType> getRelease() {
        if (release == null) {
            release = new ArrayList<ReleaseType>();
        }
        return this.release;
    }

}

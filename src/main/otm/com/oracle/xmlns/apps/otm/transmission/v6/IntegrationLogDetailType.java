
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * IntegrationLogDetail is a structure containing the details of the log message - a qualifier/value pair
 * 
 * <p>Java class for IntegrationLogDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntegrationLogDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ILogDetailQualGid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ILogDetailValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntegrationLogDetailType", propOrder = {
    "iLogDetailQualGid",
    "iLogDetailValue"
})
public class IntegrationLogDetailType {

    @XmlElement(name = "ILogDetailQualGid", required = true)
    protected String iLogDetailQualGid;
    @XmlElement(name = "ILogDetailValue", required = true)
    protected String iLogDetailValue;

    /**
     * Gets the value of the iLogDetailQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILogDetailQualGid() {
        return iLogDetailQualGid;
    }

    /**
     * Sets the value of the iLogDetailQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILogDetailQualGid(String value) {
        this.iLogDetailQualGid = value;
    }

    /**
     * Gets the value of the iLogDetailValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILogDetailValue() {
        return iLogDetailValue;
    }

    /**
     * Sets the value of the iLogDetailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILogDetailValue(String value) {
        this.iLogDetailValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Driver Header contains general information about Driver
 * 
 * <p>Java class for DriverHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DateOfBirth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="DefHomeLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;element name="DefHomeLocation" minOccurs="0"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/&gt;
 *                   &lt;/sequence&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="WorkRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DedAcctLocnProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CorporationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrefConstraintConfigGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AdjHireDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EndDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DriverTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ResourceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UseHosHistory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PrevSightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrevSightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="UserDefIconInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserDefIconInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverHeaderType", propOrder = {
    "firstName",
    "lastName",
    "dateOfBirth",
    "defHomeLocGid",
    "defHomeLocation",
    "workRegionGid",
    "dedAcctLocnProfileGid",
    "corporationProfileGid",
    "contactGid",
    "rateOfferingGid",
    "prefConstraintConfigGid",
    "adjHireDate",
    "endDate",
    "driverTypeGid",
    "resourceType",
    "useHosHistory",
    "isActive",
    "sightingLocGid",
    "sightingDate",
    "prevSightingLocGid",
    "prevSightingDate",
    "userDefIconInfo",
    "indicator"
})
public class DriverHeaderType {

    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "DateOfBirth")
    protected GLogDateTimeType dateOfBirth;
    @XmlElement(name = "DefHomeLocGid")
    protected GLogXMLGidType defHomeLocGid;
    @XmlElement(name = "DefHomeLocation")
    protected DriverHeaderType.DefHomeLocation defHomeLocation;
    @XmlElement(name = "WorkRegionGid")
    protected GLogXMLGidType workRegionGid;
    @XmlElement(name = "DedAcctLocnProfileGid")
    protected GLogXMLGidType dedAcctLocnProfileGid;
    @XmlElement(name = "CorporationProfileGid")
    protected GLogXMLGidType corporationProfileGid;
    @XmlElement(name = "ContactGid")
    protected GLogXMLGidType contactGid;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "PrefConstraintConfigGid")
    protected GLogXMLGidType prefConstraintConfigGid;
    @XmlElement(name = "AdjHireDate")
    protected GLogDateTimeType adjHireDate;
    @XmlElement(name = "EndDate")
    protected GLogDateTimeType endDate;
    @XmlElement(name = "DriverTypeGid")
    protected GLogXMLGidType driverTypeGid;
    @XmlElement(name = "ResourceType")
    protected String resourceType;
    @XmlElement(name = "UseHosHistory")
    protected String useHosHistory;
    @XmlElement(name = "IsActive")
    protected String isActive;
    @XmlElement(name = "SightingLocGid")
    protected GLogXMLGidType sightingLocGid;
    @XmlElement(name = "SightingDate")
    protected GLogDateTimeType sightingDate;
    @XmlElement(name = "PrevSightingLocGid")
    protected GLogXMLGidType prevSightingLocGid;
    @XmlElement(name = "PrevSightingDate")
    protected GLogDateTimeType prevSightingDate;
    @XmlElement(name = "UserDefIconInfo")
    protected UserDefIconInfoType userDefIconInfo;
    @XmlElement(name = "Indicator")
    protected String indicator;

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDateOfBirth(GLogDateTimeType value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the defHomeLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDefHomeLocGid() {
        return defHomeLocGid;
    }

    /**
     * Sets the value of the defHomeLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDefHomeLocGid(GLogXMLGidType value) {
        this.defHomeLocGid = value;
    }

    /**
     * Gets the value of the defHomeLocation property.
     * 
     * @return
     *     possible object is
     *     {@link DriverHeaderType.DefHomeLocation }
     *     
     */
    public DriverHeaderType.DefHomeLocation getDefHomeLocation() {
        return defHomeLocation;
    }

    /**
     * Sets the value of the defHomeLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverHeaderType.DefHomeLocation }
     *     
     */
    public void setDefHomeLocation(DriverHeaderType.DefHomeLocation value) {
        this.defHomeLocation = value;
    }

    /**
     * Gets the value of the workRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWorkRegionGid() {
        return workRegionGid;
    }

    /**
     * Sets the value of the workRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWorkRegionGid(GLogXMLGidType value) {
        this.workRegionGid = value;
    }

    /**
     * Gets the value of the dedAcctLocnProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDedAcctLocnProfileGid() {
        return dedAcctLocnProfileGid;
    }

    /**
     * Sets the value of the dedAcctLocnProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDedAcctLocnProfileGid(GLogXMLGidType value) {
        this.dedAcctLocnProfileGid = value;
    }

    /**
     * Gets the value of the corporationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCorporationProfileGid() {
        return corporationProfileGid;
    }

    /**
     * Sets the value of the corporationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCorporationProfileGid(GLogXMLGidType value) {
        this.corporationProfileGid = value;
    }

    /**
     * Gets the value of the contactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Sets the value of the contactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the prefConstraintConfigGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrefConstraintConfigGid() {
        return prefConstraintConfigGid;
    }

    /**
     * Sets the value of the prefConstraintConfigGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrefConstraintConfigGid(GLogXMLGidType value) {
        this.prefConstraintConfigGid = value;
    }

    /**
     * Gets the value of the adjHireDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAdjHireDate() {
        return adjHireDate;
    }

    /**
     * Sets the value of the adjHireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAdjHireDate(GLogDateTimeType value) {
        this.adjHireDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDate(GLogDateTimeType value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the driverTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverTypeGid() {
        return driverTypeGid;
    }

    /**
     * Sets the value of the driverTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverTypeGid(GLogXMLGidType value) {
        this.driverTypeGid = value;
    }

    /**
     * Gets the value of the resourceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceType(String value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the useHosHistory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseHosHistory() {
        return useHosHistory;
    }

    /**
     * Sets the value of the useHosHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseHosHistory(String value) {
        this.useHosHistory = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the sightingLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSightingLocGid() {
        return sightingLocGid;
    }

    /**
     * Sets the value of the sightingLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSightingLocGid(GLogXMLGidType value) {
        this.sightingLocGid = value;
    }

    /**
     * Gets the value of the sightingDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSightingDate() {
        return sightingDate;
    }

    /**
     * Sets the value of the sightingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSightingDate(GLogDateTimeType value) {
        this.sightingDate = value;
    }

    /**
     * Gets the value of the prevSightingLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrevSightingLocGid() {
        return prevSightingLocGid;
    }

    /**
     * Sets the value of the prevSightingLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrevSightingLocGid(GLogXMLGidType value) {
        this.prevSightingLocGid = value;
    }

    /**
     * Gets the value of the prevSightingDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPrevSightingDate() {
        return prevSightingDate;
    }

    /**
     * Sets the value of the prevSightingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPrevSightingDate(GLogDateTimeType value) {
        this.prevSightingDate = value;
    }

    /**
     * Gets the value of the userDefIconInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public UserDefIconInfoType getUserDefIconInfo() {
        return userDefIconInfo;
    }

    /**
     * Sets the value of the userDefIconInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public void setUserDefIconInfo(UserDefIconInfoType value) {
        this.userDefIconInfo = value;
    }

    /**
     * Gets the value of the indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Sets the value of the indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class DefHomeLocation {

        @XmlElement(name = "Location", required = true)
        protected LocationType location;

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setLocation(LocationType value) {
            this.location = value;
        }

    }

}

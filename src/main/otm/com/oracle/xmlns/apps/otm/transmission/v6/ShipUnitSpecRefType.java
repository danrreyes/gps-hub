
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ShipUnitSpecRef may be used to reference an existing ship unit spec, or define a new one.
 * 
 * <p>Java class for ShipUnitSpecRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitSpecRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="ShipUnitSpecGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipUnitSpec" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitSpecType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitSpecRefType", propOrder = {
    "shipUnitSpecGid",
    "shipUnitSpec"
})
public class ShipUnitSpecRefType {

    @XmlElement(name = "ShipUnitSpecGid")
    protected GLogXMLGidType shipUnitSpecGid;
    @XmlElement(name = "ShipUnitSpec")
    protected ShipUnitSpecType shipUnitSpec;

    /**
     * Gets the value of the shipUnitSpecGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecGid() {
        return shipUnitSpecGid;
    }

    /**
     * Sets the value of the shipUnitSpecGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecGid(GLogXMLGidType value) {
        this.shipUnitSpecGid = value;
    }

    /**
     * Gets the value of the shipUnitSpec property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitSpecType }
     *     
     */
    public ShipUnitSpecType getShipUnitSpec() {
        return shipUnitSpec;
    }

    /**
     * Sets the value of the shipUnitSpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitSpecType }
     *     
     */
    public void setShipUnitSpec(ShipUnitSpecType value) {
        this.shipUnitSpec = value;
    }

}

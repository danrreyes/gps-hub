
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="X_LANE_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="X_LANE_XID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="SOURCE_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CONSTRAINT_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PLANNING_PARAMETER_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SOURCE_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DEST_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "xlanerow"
})
@XmlRootElement(name = "X_LANE")
public class XLANE {

    @XmlElement(name = "X_LANE_ROW")
    protected List<XLANE.XLANEROW> xlanerow;

    /**
     * Gets the value of the xlanerow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the xlanerow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getXLANEROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link XLANE.XLANEROW }
     * 
     * 
     */
    public List<XLANE.XLANEROW> getXLANEROW() {
        if (xlanerow == null) {
            xlanerow = new ArrayList<XLANE.XLANEROW>();
        }
        return this.xlanerow;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="X_LANE_XID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="SOURCE_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_PROVINCE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_POSTAL_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_COUNTRY_CODE3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_ZONE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_ZONE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_ZONE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_ZONE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_GEO_HIERARCHY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_REGION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CONSTRAINT_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PLANNING_PARAMETER_SET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_RAIL_SPLC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SOURCE_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DEST_RAIL_STATION_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "xlanegid",
        "xlanexid",
        "sourcelocationgid",
        "sourcecity",
        "sourceprovincecode",
        "sourcepostalcode",
        "sourcecountrycode3GID",
        "sourcezone1",
        "sourcezone2",
        "sourcezone3",
        "sourcezone4",
        "sourcegeohierarchygid",
        "destlocationgid",
        "destcity",
        "destprovincecode",
        "destpostalcode",
        "destcountrycode3GID",
        "destzone1",
        "destzone2",
        "destzone3",
        "destzone4",
        "destgeohierarchygid",
        "sourceregiongid",
        "destregiongid",
        "constraintsetgid",
        "planningparametersetgid",
        "sourcerailsplcgid",
        "destrailsplcgid",
        "sourcerailstationcodegid",
        "destrailstationcodegid",
        "domainname",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate"
    })
    public static class XLANEROW {

        @XmlElement(name = "X_LANE_GID", required = true)
        protected String xlanegid;
        @XmlElement(name = "X_LANE_XID", required = true)
        protected String xlanexid;
        @XmlElement(name = "SOURCE_LOCATION_GID")
        protected String sourcelocationgid;
        @XmlElement(name = "SOURCE_CITY")
        protected String sourcecity;
        @XmlElement(name = "SOURCE_PROVINCE_CODE")
        protected String sourceprovincecode;
        @XmlElement(name = "SOURCE_POSTAL_CODE")
        protected String sourcepostalcode;
        @XmlElement(name = "SOURCE_COUNTRY_CODE3_GID")
        protected String sourcecountrycode3GID;
        @XmlElement(name = "SOURCE_ZONE1")
        protected String sourcezone1;
        @XmlElement(name = "SOURCE_ZONE2")
        protected String sourcezone2;
        @XmlElement(name = "SOURCE_ZONE3")
        protected String sourcezone3;
        @XmlElement(name = "SOURCE_ZONE4")
        protected String sourcezone4;
        @XmlElement(name = "SOURCE_GEO_HIERARCHY_GID")
        protected String sourcegeohierarchygid;
        @XmlElement(name = "DEST_LOCATION_GID")
        protected String destlocationgid;
        @XmlElement(name = "DEST_CITY")
        protected String destcity;
        @XmlElement(name = "DEST_PROVINCE_CODE")
        protected String destprovincecode;
        @XmlElement(name = "DEST_POSTAL_CODE")
        protected String destpostalcode;
        @XmlElement(name = "DEST_COUNTRY_CODE3_GID")
        protected String destcountrycode3GID;
        @XmlElement(name = "DEST_ZONE1")
        protected String destzone1;
        @XmlElement(name = "DEST_ZONE2")
        protected String destzone2;
        @XmlElement(name = "DEST_ZONE3")
        protected String destzone3;
        @XmlElement(name = "DEST_ZONE4")
        protected String destzone4;
        @XmlElement(name = "DEST_GEO_HIERARCHY_GID")
        protected String destgeohierarchygid;
        @XmlElement(name = "SOURCE_REGION_GID")
        protected String sourceregiongid;
        @XmlElement(name = "DEST_REGION_GID")
        protected String destregiongid;
        @XmlElement(name = "CONSTRAINT_SET_GID")
        protected String constraintsetgid;
        @XmlElement(name = "PLANNING_PARAMETER_SET_GID")
        protected String planningparametersetgid;
        @XmlElement(name = "SOURCE_RAIL_SPLC_GID")
        protected String sourcerailsplcgid;
        @XmlElement(name = "DEST_RAIL_SPLC_GID")
        protected String destrailsplcgid;
        @XmlElement(name = "SOURCE_RAIL_STATION_CODE_GID")
        protected String sourcerailstationcodegid;
        @XmlElement(name = "DEST_RAIL_STATION_CODE_GID")
        protected String destrailstationcodegid;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Gets the value of the xlanegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXLANEGID() {
            return xlanegid;
        }

        /**
         * Sets the value of the xlanegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXLANEGID(String value) {
            this.xlanegid = value;
        }

        /**
         * Gets the value of the xlanexid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXLANEXID() {
            return xlanexid;
        }

        /**
         * Sets the value of the xlanexid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXLANEXID(String value) {
            this.xlanexid = value;
        }

        /**
         * Gets the value of the sourcelocationgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCELOCATIONGID() {
            return sourcelocationgid;
        }

        /**
         * Sets the value of the sourcelocationgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCELOCATIONGID(String value) {
            this.sourcelocationgid = value;
        }

        /**
         * Gets the value of the sourcecity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCECITY() {
            return sourcecity;
        }

        /**
         * Sets the value of the sourcecity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCECITY(String value) {
            this.sourcecity = value;
        }

        /**
         * Gets the value of the sourceprovincecode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEPROVINCECODE() {
            return sourceprovincecode;
        }

        /**
         * Sets the value of the sourceprovincecode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEPROVINCECODE(String value) {
            this.sourceprovincecode = value;
        }

        /**
         * Gets the value of the sourcepostalcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEPOSTALCODE() {
            return sourcepostalcode;
        }

        /**
         * Sets the value of the sourcepostalcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEPOSTALCODE(String value) {
            this.sourcepostalcode = value;
        }

        /**
         * Gets the value of the sourcecountrycode3GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCECOUNTRYCODE3GID() {
            return sourcecountrycode3GID;
        }

        /**
         * Sets the value of the sourcecountrycode3GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCECOUNTRYCODE3GID(String value) {
            this.sourcecountrycode3GID = value;
        }

        /**
         * Gets the value of the sourcezone1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE1() {
            return sourcezone1;
        }

        /**
         * Sets the value of the sourcezone1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE1(String value) {
            this.sourcezone1 = value;
        }

        /**
         * Gets the value of the sourcezone2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE2() {
            return sourcezone2;
        }

        /**
         * Sets the value of the sourcezone2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE2(String value) {
            this.sourcezone2 = value;
        }

        /**
         * Gets the value of the sourcezone3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE3() {
            return sourcezone3;
        }

        /**
         * Sets the value of the sourcezone3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE3(String value) {
            this.sourcezone3 = value;
        }

        /**
         * Gets the value of the sourcezone4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEZONE4() {
            return sourcezone4;
        }

        /**
         * Sets the value of the sourcezone4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEZONE4(String value) {
            this.sourcezone4 = value;
        }

        /**
         * Gets the value of the sourcegeohierarchygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEGEOHIERARCHYGID() {
            return sourcegeohierarchygid;
        }

        /**
         * Sets the value of the sourcegeohierarchygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEGEOHIERARCHYGID(String value) {
            this.sourcegeohierarchygid = value;
        }

        /**
         * Gets the value of the destlocationgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTLOCATIONGID() {
            return destlocationgid;
        }

        /**
         * Sets the value of the destlocationgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTLOCATIONGID(String value) {
            this.destlocationgid = value;
        }

        /**
         * Gets the value of the destcity property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTCITY() {
            return destcity;
        }

        /**
         * Sets the value of the destcity property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTCITY(String value) {
            this.destcity = value;
        }

        /**
         * Gets the value of the destprovincecode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTPROVINCECODE() {
            return destprovincecode;
        }

        /**
         * Sets the value of the destprovincecode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTPROVINCECODE(String value) {
            this.destprovincecode = value;
        }

        /**
         * Gets the value of the destpostalcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTPOSTALCODE() {
            return destpostalcode;
        }

        /**
         * Sets the value of the destpostalcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTPOSTALCODE(String value) {
            this.destpostalcode = value;
        }

        /**
         * Gets the value of the destcountrycode3GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTCOUNTRYCODE3GID() {
            return destcountrycode3GID;
        }

        /**
         * Sets the value of the destcountrycode3GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTCOUNTRYCODE3GID(String value) {
            this.destcountrycode3GID = value;
        }

        /**
         * Gets the value of the destzone1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE1() {
            return destzone1;
        }

        /**
         * Sets the value of the destzone1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE1(String value) {
            this.destzone1 = value;
        }

        /**
         * Gets the value of the destzone2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE2() {
            return destzone2;
        }

        /**
         * Sets the value of the destzone2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE2(String value) {
            this.destzone2 = value;
        }

        /**
         * Gets the value of the destzone3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE3() {
            return destzone3;
        }

        /**
         * Sets the value of the destzone3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE3(String value) {
            this.destzone3 = value;
        }

        /**
         * Gets the value of the destzone4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTZONE4() {
            return destzone4;
        }

        /**
         * Sets the value of the destzone4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTZONE4(String value) {
            this.destzone4 = value;
        }

        /**
         * Gets the value of the destgeohierarchygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTGEOHIERARCHYGID() {
            return destgeohierarchygid;
        }

        /**
         * Sets the value of the destgeohierarchygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTGEOHIERARCHYGID(String value) {
            this.destgeohierarchygid = value;
        }

        /**
         * Gets the value of the sourceregiongid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCEREGIONGID() {
            return sourceregiongid;
        }

        /**
         * Sets the value of the sourceregiongid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCEREGIONGID(String value) {
            this.sourceregiongid = value;
        }

        /**
         * Gets the value of the destregiongid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTREGIONGID() {
            return destregiongid;
        }

        /**
         * Sets the value of the destregiongid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTREGIONGID(String value) {
            this.destregiongid = value;
        }

        /**
         * Gets the value of the constraintsetgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCONSTRAINTSETGID() {
            return constraintsetgid;
        }

        /**
         * Sets the value of the constraintsetgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCONSTRAINTSETGID(String value) {
            this.constraintsetgid = value;
        }

        /**
         * Gets the value of the planningparametersetgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPLANNINGPARAMETERSETGID() {
            return planningparametersetgid;
        }

        /**
         * Sets the value of the planningparametersetgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPLANNINGPARAMETERSETGID(String value) {
            this.planningparametersetgid = value;
        }

        /**
         * Gets the value of the sourcerailsplcgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCERAILSPLCGID() {
            return sourcerailsplcgid;
        }

        /**
         * Sets the value of the sourcerailsplcgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCERAILSPLCGID(String value) {
            this.sourcerailsplcgid = value;
        }

        /**
         * Gets the value of the destrailsplcgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTRAILSPLCGID() {
            return destrailsplcgid;
        }

        /**
         * Sets the value of the destrailsplcgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTRAILSPLCGID(String value) {
            this.destrailsplcgid = value;
        }

        /**
         * Gets the value of the sourcerailstationcodegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSOURCERAILSTATIONCODEGID() {
            return sourcerailstationcodegid;
        }

        /**
         * Sets the value of the sourcerailstationcodegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSOURCERAILSTATIONCODEGID(String value) {
            this.sourcerailstationcodegid = value;
        }

        /**
         * Gets the value of the destrailstationcodegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESTRAILSTATIONCODEGID() {
            return destrailstationcodegid;
        }

        /**
         * Sets the value of the destrailstationcodegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESTRAILSTATIONCODEGID(String value) {
            this.destrailstationcodegid = value;
        }

        /**
         * Gets the value of the domainname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Sets the value of the domainname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Gets the value of the insertuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Sets the value of the insertuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Gets the value of the insertdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Sets the value of the insertdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Gets the value of the updateuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Sets the value of the updateuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Gets the value of the updatedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Sets the value of the updatedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Gets the value of the num property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Sets the value of the num property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Integration Command.
 *             When the IntCommandName = "PropagateShipUnitChanges", IntArg would support ArgName field "Type" and ArgValue with the following values:
 *             None, Upstream, Downstream, UpAndDownstream
 *             For backward compatibility, the default value for Type will be Downstream.
 *          
 * 
 * <p>Java class for IntCommandType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntCommandType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntCommandName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IntArg" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntCommandType", propOrder = {
    "intCommandName",
    "intArg"
})
public class IntCommandType {

    @XmlElement(name = "IntCommandName", required = true)
    protected String intCommandName;
    @XmlElement(name = "IntArg")
    protected List<IntCommandType.IntArg> intArg;

    /**
     * Gets the value of the intCommandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntCommandName() {
        return intCommandName;
    }

    /**
     * Sets the value of the intCommandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntCommandName(String value) {
        this.intCommandName = value;
    }

    /**
     * Gets the value of the intArg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the intArg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntArg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntCommandType.IntArg }
     * 
     * 
     */
    public List<IntCommandType.IntArg> getIntArg() {
        if (intArg == null) {
            intArg = new ArrayList<IntCommandType.IntArg>();
        }
        return this.intArg;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "argName",
        "argValue"
    })
    public static class IntArg {

        @XmlElement(name = "ArgName", required = true)
        protected String argName;
        @XmlElement(name = "ArgValue", required = true)
        protected String argValue;

        /**
         * Gets the value of the argName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgName() {
            return argName;
        }

        /**
         * Sets the value of the argName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgName(String value) {
            this.argName = value;
        }

        /**
         * Gets the value of the argValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgValue() {
            return argValue;
        }

        /**
         * Sets the value of the argValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgValue(String value) {
            this.argValue = value;
        }

    }

}

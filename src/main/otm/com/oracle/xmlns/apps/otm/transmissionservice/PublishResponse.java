
package com.oracle.xmlns.apps.otm.transmissionservice;

import com.oracle.xmlns.apps.otm.transmission.v6.TransmissionAck;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionAck"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transmissionAck"
})
@XmlRootElement(name = "publishResponse")
public class PublishResponse {

    @XmlElement(name = "TransmissionAck", namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", required = true)
    protected TransmissionAck transmissionAck;

    /**
     * Gets the value of the transmissionAck property.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionAck }
     *     
     */
    public TransmissionAck getTransmissionAck() {
        return transmissionAck;
    }

    /**
     * Sets the value of the transmissionAck property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionAck }
     *     
     */
    public void setTransmissionAck(TransmissionAck value) {
        this.transmissionAck = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PaymentSummaryDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentSummaryDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BilledRated" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BilledRatedType"/&gt;
 *         &lt;element name="FreightRate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FreightRateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentSummaryDetailType", propOrder = {
    "sequenceNumber",
    "billedRated",
    "freightRate"
})
public class PaymentSummaryDetailType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "BilledRated", required = true)
    protected BilledRatedType billedRated;
    @XmlElement(name = "FreightRate", required = true)
    protected FreightRateType freightRate;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the billedRated property.
     * 
     * @return
     *     possible object is
     *     {@link BilledRatedType }
     *     
     */
    public BilledRatedType getBilledRated() {
        return billedRated;
    }

    /**
     * Sets the value of the billedRated property.
     * 
     * @param value
     *     allowed object is
     *     {@link BilledRatedType }
     *     
     */
    public void setBilledRated(BilledRatedType value) {
        this.billedRated = value;
    }

    /**
     * Gets the value of the freightRate property.
     * 
     * @return
     *     possible object is
     *     {@link FreightRateType }
     *     
     */
    public FreightRateType getFreightRate() {
        return freightRate;
    }

    /**
     * Sets the value of the freightRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link FreightRateType }
     *     
     */
    public void setFreightRate(FreightRateType value) {
        this.freightRate = value;
    }

}

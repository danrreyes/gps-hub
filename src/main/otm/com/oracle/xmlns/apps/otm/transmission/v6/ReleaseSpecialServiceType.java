
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * The special services required for the order release against a base order. The special
 *             services differ from the accessories in that the customer does not pay for special services.
 *          
 * 
 * <p>Java class for ReleaseSpecialServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseSpecialServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="SpclService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpclServiceType"/&gt;
 *           &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Time" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ForSourceDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseSpecialServiceType", propOrder = {
    "spclService",
    "specialServiceGid",
    "time",
    "forSourceDest"
})
public class ReleaseSpecialServiceType {

    @XmlElement(name = "SpclService")
    protected SpclServiceType spclService;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "Time")
    protected GLogXMLDurationType time;
    @XmlElement(name = "ForSourceDest")
    protected String forSourceDest;

    /**
     * Gets the value of the spclService property.
     * 
     * @return
     *     possible object is
     *     {@link SpclServiceType }
     *     
     */
    public SpclServiceType getSpclService() {
        return spclService;
    }

    /**
     * Sets the value of the spclService property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpclServiceType }
     *     
     */
    public void setSpclService(SpclServiceType value) {
        this.spclService = value;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Sets the value of the specialServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Gets the value of the time property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTime() {
        return time;
    }

    /**
     * Sets the value of the time property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTime(GLogXMLDurationType value) {
        this.time = value;
    }

    /**
     * Gets the value of the forSourceDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForSourceDest() {
        return forSourceDest;
    }

    /**
     * Sets the value of the forSourceDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForSourceDest(String value) {
        this.forSourceDest = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies an Appt Rule Set defined for a location resource. This is for outbound xml.
 * 
 * <p>Java class for ApptRuleSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApptRuleSetType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ApptRuleSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ApptRuleSetName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EffectiveDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="ExpiryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="FlexCommodityProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ContactProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityCheckOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTemperatureControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SourceRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="DestinationRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="ApptSsActivityProfGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ApptActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApptRuleSetType", propOrder = {
    "apptRuleSetGid",
    "apptRuleSetName",
    "effectiveDt",
    "expiryDt",
    "flexCommodityProfileGid",
    "equipmentGroupProfileGid",
    "modeProfileGid",
    "serviceProviderProfileGid",
    "contactProfileGid",
    "flexCommodityCheckOption",
    "isTemperatureControl",
    "isHazardous",
    "sourceRegionGid",
    "destinationRegionGid",
    "apptSsActivityProfGid",
    "apptActivityType"
})
public class ApptRuleSetType {

    @XmlElement(name = "ApptRuleSetGid")
    protected GLogXMLGidType apptRuleSetGid;
    @XmlElement(name = "ApptRuleSetName", required = true)
    protected String apptRuleSetName;
    @XmlElement(name = "EffectiveDt", required = true)
    protected GLogDateTimeType effectiveDt;
    @XmlElement(name = "ExpiryDt", required = true)
    protected GLogDateTimeType expiryDt;
    @XmlElement(name = "FlexCommodityProfileGid")
    protected GLogXMLGidType flexCommodityProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "ContactProfileGid")
    protected GLogXMLGidType contactProfileGid;
    @XmlElement(name = "FlexCommodityCheckOption")
    protected String flexCommodityCheckOption;
    @XmlElement(name = "IsTemperatureControl")
    protected String isTemperatureControl;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "SourceRegionGid")
    protected GLogXMLRegionGidType sourceRegionGid;
    @XmlElement(name = "DestinationRegionGid")
    protected GLogXMLRegionGidType destinationRegionGid;
    @XmlElement(name = "ApptSsActivityProfGid")
    protected GLogXMLGidType apptSsActivityProfGid;
    @XmlElement(name = "ApptActivityType")
    protected String apptActivityType;

    /**
     * Gets the value of the apptRuleSetGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getApptRuleSetGid() {
        return apptRuleSetGid;
    }

    /**
     * Sets the value of the apptRuleSetGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setApptRuleSetGid(GLogXMLGidType value) {
        this.apptRuleSetGid = value;
    }

    /**
     * Gets the value of the apptRuleSetName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptRuleSetName() {
        return apptRuleSetName;
    }

    /**
     * Sets the value of the apptRuleSetName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptRuleSetName(String value) {
        this.apptRuleSetName = value;
    }

    /**
     * Gets the value of the effectiveDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDt() {
        return effectiveDt;
    }

    /**
     * Sets the value of the effectiveDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDt(GLogDateTimeType value) {
        this.effectiveDt = value;
    }

    /**
     * Gets the value of the expiryDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpiryDt() {
        return expiryDt;
    }

    /**
     * Sets the value of the expiryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpiryDt(GLogDateTimeType value) {
        this.expiryDt = value;
    }

    /**
     * Gets the value of the flexCommodityProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityProfileGid() {
        return flexCommodityProfileGid;
    }

    /**
     * Sets the value of the flexCommodityProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityProfileGid(GLogXMLGidType value) {
        this.flexCommodityProfileGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the serviceProviderProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Sets the value of the serviceProviderProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Gets the value of the contactProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactProfileGid() {
        return contactProfileGid;
    }

    /**
     * Sets the value of the contactProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactProfileGid(GLogXMLGidType value) {
        this.contactProfileGid = value;
    }

    /**
     * Gets the value of the flexCommodityCheckOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexCommodityCheckOption() {
        return flexCommodityCheckOption;
    }

    /**
     * Sets the value of the flexCommodityCheckOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexCommodityCheckOption(String value) {
        this.flexCommodityCheckOption = value;
    }

    /**
     * Gets the value of the isTemperatureControl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemperatureControl() {
        return isTemperatureControl;
    }

    /**
     * Sets the value of the isTemperatureControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemperatureControl(String value) {
        this.isTemperatureControl = value;
    }

    /**
     * Gets the value of the isHazardous property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Sets the value of the isHazardous property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Gets the value of the sourceRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getSourceRegionGid() {
        return sourceRegionGid;
    }

    /**
     * Sets the value of the sourceRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setSourceRegionGid(GLogXMLRegionGidType value) {
        this.sourceRegionGid = value;
    }

    /**
     * Gets the value of the destinationRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestinationRegionGid() {
        return destinationRegionGid;
    }

    /**
     * Sets the value of the destinationRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestinationRegionGid(GLogXMLRegionGidType value) {
        this.destinationRegionGid = value;
    }

    /**
     * Gets the value of the apptSsActivityProfGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getApptSsActivityProfGid() {
        return apptSsActivityProfGid;
    }

    /**
     * Sets the value of the apptSsActivityProfGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setApptSsActivityProfGid(GLogXMLGidType value) {
        this.apptSsActivityProfGid = value;
    }

    /**
     * Gets the value of the apptActivityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptActivityType() {
        return apptActivityType;
    }

    /**
     * Sets the value of the apptActivityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptActivityType(String value) {
        this.apptActivityType = value;
    }

}

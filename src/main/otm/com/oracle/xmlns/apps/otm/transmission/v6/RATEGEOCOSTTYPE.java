
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RATE_GEO_COST_TYPE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RATE_GEO_COST_TYPE"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="RATE_GEO_COST_ROW" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_ITEM_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_SECTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_ITEM_NUMBER_SUFFIX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_ITEM_PART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_FREIGHT_CLASS_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EX_PARTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_BASIS_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_COLUMN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DISTANCE_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_BASIS_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_GEO_COST_OPERAND_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPER1_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LEFT_OPERAND1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LOW_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="HIGH_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AND_OR1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPER2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LEFT_OPERAND2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LOW_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="HIGH_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AND_OR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPER3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LEFT_OPERAND3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LOW_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="HIGH_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="AND_OR3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPER4_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LEFT_OPERAND4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LOW_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="HIGH_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_UNIT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_UNIT_COUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_MULTIPLIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_MULTIPLIER_SCALAR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_ACTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_BREAK_COMPARATOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_MULTIPLIER_OPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_MULTIPLIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_UNIT_COUNT2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_UNIT_UOM_CODE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USES_UNIT_BREAKS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CALENDAR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IS_FILED_AS_TARIFF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="COST_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CALENDAR_ACTIVITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_UNIT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_UNIT_BREAK_PROFILE2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_BREAK_COMPARATOR2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="COST_CATEGORY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="COST_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_GEO_COST_WEIGHT_BREAK" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_WEIGHT_BREAK_TYPE" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_GEO_COST_UNIT_BREAK" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_UNIT_BREAK_TYPE" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPERAND_QUALIFIER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPERAND_QUALIFIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPERAND_QUALIFIER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="OPERAND_QUALIFIER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_QUALIFIER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_QUALIFIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_BREAK_COMP_QUALIFIER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_BREAK_COMP_QUALIFIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RATE_GEO_COST_TYPE", propOrder = {
    "rategeocostrow"
})
public class RATEGEOCOSTTYPE {

    @XmlElement(name = "RATE_GEO_COST_ROW")
    protected List<RATEGEOCOSTTYPE.RATEGEOCOSTROW> rategeocostrow;

    /**
     * Gets the value of the rategeocostrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rategeocostrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEGEOCOSTROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RATEGEOCOSTTYPE.RATEGEOCOSTROW }
     * 
     * 
     */
    public List<RATEGEOCOSTTYPE.RATEGEOCOSTROW> getRATEGEOCOSTROW() {
        if (rategeocostrow == null) {
            rategeocostrow = new ArrayList<RATEGEOCOSTTYPE.RATEGEOCOSTROW>();
        }
        return this.rategeocostrow;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_ITEM_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_SECTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_ITEM_NUMBER_SUFFIX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_ITEM_PART" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_FREIGHT_CLASS_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EX_PARTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_BASIS_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_COLUMN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DISTANCE_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_BASIS_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_COST_OPERAND_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPER1_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LEFT_OPERAND1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LOW_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="HIGH_VALUE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AND_OR1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPER2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LEFT_OPERAND2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LOW_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="HIGH_VALUE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AND_OR2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPER3_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LEFT_OPERAND3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LOW_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="HIGH_VALUE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="AND_OR3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPER4_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LEFT_OPERAND4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LOW_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="HIGH_VALUE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_UNIT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_UNIT_COUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_MULTIPLIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_MULTIPLIER_SCALAR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_ACTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_BREAK_COMPARATOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_MULTIPLIER_OPTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_MULTIPLIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_UNIT_COUNT2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_UNIT_UOM_CODE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USES_UNIT_BREAKS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CALENDAR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_FILED_AS_TARIFF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="COST_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CALENDAR_ACTIVITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_UNIT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_UNIT_BREAK_PROFILE2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_BREAK_COMPARATOR2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="COST_CATEGORY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="COST_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_COST_WEIGHT_BREAK" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_WEIGHT_BREAK_TYPE" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_COST_UNIT_BREAK" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_UNIT_BREAK_TYPE" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPERAND_QUALIFIER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPERAND_QUALIFIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPERAND_QUALIFIER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="OPERAND_QUALIFIER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_QUALIFIER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_QUALIFIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_BREAK_COMP_QUALIFIER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_BREAK_COMP_QUALIFIER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rategeocostseq",
        "domainname",
        "description",
        "equipmentgroupprofilegid",
        "tariffitemnumber",
        "tariffsection",
        "tariffitemnumbersuffix",
        "tariffitempart",
        "tarifffreightclasscode",
        "exparte",
        "ratebasisnumber",
        "tariffcolumn",
        "tariffdistance",
        "tariffdistanceuomcode",
        "tariffdistancebase",
        "distancequalifier",
        "ratebasisqualifier",
        "rategeocostgroupgid",
        "rategeocostoperandseq",
        "oper1GID",
        "leftoperand1",
        "lowvalue1",
        "highvalue1",
        "andor1",
        "oper2GID",
        "leftoperand2",
        "lowvalue2",
        "highvalue2",
        "andor2",
        "oper3GID",
        "leftoperand3",
        "lowvalue3",
        "highvalue3",
        "andor3",
        "oper4GID",
        "leftoperand4",
        "lowvalue4",
        "highvalue4",
        "chargeamount",
        "chargecurrencygid",
        "chargeamountbase",
        "chargeunituomcode",
        "chargeunitcount",
        "chargemultiplier",
        "chargemultiplierscalar",
        "chargeaction",
        "chargebreakcomparator",
        "chargetype",
        "chargemultiplieroption",
        "chargemultiplier2",
        "chargeunitcount2",
        "chargeunituomcode2",
        "chargesequence",
        "usesunitbreaks",
        "dimratefactorgid",
        "roundingtype",
        "roundinginterval",
        "roundingfieldslevel",
        "roundingapplication",
        "mincost",
        "mincostcurrencygid",
        "mincostbase",
        "maxcost",
        "maxcostcurrencygid",
        "maxcostbase",
        "paymentmethodcodegid",
        "effectivedate",
        "expirationdate",
        "calendargid",
        "isfiledastariff",
        "tier",
        "costtype",
        "calendaractivitygid",
        "rateunitbreakprofilegid",
        "rateunitbreakprofile2GID",
        "chargebreakcomparator2GID",
        "externalratingenginegid",
        "extrefieldsetgid",
        "costcategorygid",
        "costcodegid",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate",
        "rategeocostweightbreak",
        "rategeocostunitbreak",
        "attribute1",
        "attribute2",
        "attribute3",
        "attribute4",
        "attribute5",
        "attribute6",
        "attribute7",
        "attribute8",
        "attribute9",
        "attribute10",
        "attribute11",
        "attribute12",
        "attribute13",
        "attribute14",
        "attribute15",
        "attribute16",
        "attribute17",
        "attribute18",
        "attribute19",
        "attribute20",
        "attributenumber1",
        "attributenumber2",
        "attributenumber3",
        "attributenumber4",
        "attributenumber5",
        "attributenumber6",
        "attributenumber7",
        "attributenumber8",
        "attributenumber9",
        "attributenumber10",
        "attributedate1",
        "attributedate2",
        "attributedate3",
        "attributedate4",
        "attributedate5",
        "attributedate6",
        "attributedate7",
        "attributedate8",
        "attributedate9",
        "attributedate10",
        "operandqualifier1",
        "operandqualifier2",
        "operandqualifier3",
        "operandqualifier4",
        "chargequalifier1",
        "chargequalifier2",
        "chargebreakcompqualifier1",
        "chargebreakcompqualifier2"
    })
    public static class RATEGEOCOSTROW {

        @XmlElement(name = "RATE_GEO_COST_SEQ")
        protected String rategeocostseq;
        @XmlElement(name = "DOMAIN_NAME")
        protected String domainname;
        @XmlElement(name = "DESCRIPTION")
        protected String description;
        @XmlElement(name = "EQUIPMENT_GROUP_PROFILE_GID")
        protected String equipmentgroupprofilegid;
        @XmlElement(name = "TARIFF_ITEM_NUMBER")
        protected String tariffitemnumber;
        @XmlElement(name = "TARIFF_SECTION")
        protected String tariffsection;
        @XmlElement(name = "TARIFF_ITEM_NUMBER_SUFFIX")
        protected String tariffitemnumbersuffix;
        @XmlElement(name = "TARIFF_ITEM_PART")
        protected String tariffitempart;
        @XmlElement(name = "TARIFF_FREIGHT_CLASS_CODE")
        protected String tarifffreightclasscode;
        @XmlElement(name = "EX_PARTE")
        protected String exparte;
        @XmlElement(name = "RATE_BASIS_NUMBER")
        protected String ratebasisnumber;
        @XmlElement(name = "TARIFF_COLUMN")
        protected String tariffcolumn;
        @XmlElement(name = "TARIFF_DISTANCE")
        protected String tariffdistance;
        @XmlElement(name = "TARIFF_DISTANCE_UOM_CODE")
        protected String tariffdistanceuomcode;
        @XmlElement(name = "TARIFF_DISTANCE_BASE")
        protected String tariffdistancebase;
        @XmlElement(name = "DISTANCE_QUALIFIER")
        protected String distancequalifier;
        @XmlElement(name = "RATE_BASIS_QUALIFIER")
        protected String ratebasisqualifier;
        @XmlElement(name = "RATE_GEO_COST_GROUP_GID")
        protected String rategeocostgroupgid;
        @XmlElement(name = "RATE_GEO_COST_OPERAND_SEQ")
        protected String rategeocostoperandseq;
        @XmlElement(name = "OPER1_GID")
        protected String oper1GID;
        @XmlElement(name = "LEFT_OPERAND1")
        protected String leftoperand1;
        @XmlElement(name = "LOW_VALUE1")
        protected String lowvalue1;
        @XmlElement(name = "HIGH_VALUE1")
        protected String highvalue1;
        @XmlElement(name = "AND_OR1")
        protected String andor1;
        @XmlElement(name = "OPER2_GID")
        protected String oper2GID;
        @XmlElement(name = "LEFT_OPERAND2")
        protected String leftoperand2;
        @XmlElement(name = "LOW_VALUE2")
        protected String lowvalue2;
        @XmlElement(name = "HIGH_VALUE2")
        protected String highvalue2;
        @XmlElement(name = "AND_OR2")
        protected String andor2;
        @XmlElement(name = "OPER3_GID")
        protected String oper3GID;
        @XmlElement(name = "LEFT_OPERAND3")
        protected String leftoperand3;
        @XmlElement(name = "LOW_VALUE3")
        protected String lowvalue3;
        @XmlElement(name = "HIGH_VALUE3")
        protected String highvalue3;
        @XmlElement(name = "AND_OR3")
        protected String andor3;
        @XmlElement(name = "OPER4_GID")
        protected String oper4GID;
        @XmlElement(name = "LEFT_OPERAND4")
        protected String leftoperand4;
        @XmlElement(name = "LOW_VALUE4")
        protected String lowvalue4;
        @XmlElement(name = "HIGH_VALUE4")
        protected String highvalue4;
        @XmlElement(name = "CHARGE_AMOUNT")
        protected String chargeamount;
        @XmlElement(name = "CHARGE_CURRENCY_GID")
        protected String chargecurrencygid;
        @XmlElement(name = "CHARGE_AMOUNT_BASE")
        protected String chargeamountbase;
        @XmlElement(name = "CHARGE_UNIT_UOM_CODE")
        protected String chargeunituomcode;
        @XmlElement(name = "CHARGE_UNIT_COUNT")
        protected String chargeunitcount;
        @XmlElement(name = "CHARGE_MULTIPLIER")
        protected String chargemultiplier;
        @XmlElement(name = "CHARGE_MULTIPLIER_SCALAR")
        protected String chargemultiplierscalar;
        @XmlElement(name = "CHARGE_ACTION")
        protected String chargeaction;
        @XmlElement(name = "CHARGE_BREAK_COMPARATOR")
        protected String chargebreakcomparator;
        @XmlElement(name = "CHARGE_TYPE")
        protected String chargetype;
        @XmlElement(name = "CHARGE_MULTIPLIER_OPTION")
        protected String chargemultiplieroption;
        @XmlElement(name = "CHARGE_MULTIPLIER2")
        protected String chargemultiplier2;
        @XmlElement(name = "CHARGE_UNIT_COUNT2")
        protected String chargeunitcount2;
        @XmlElement(name = "CHARGE_UNIT_UOM_CODE2")
        protected String chargeunituomcode2;
        @XmlElement(name = "CHARGE_SEQUENCE")
        protected String chargesequence;
        @XmlElement(name = "USES_UNIT_BREAKS")
        protected String usesunitbreaks;
        @XmlElement(name = "DIM_RATE_FACTOR_GID")
        protected String dimratefactorgid;
        @XmlElement(name = "ROUNDING_TYPE")
        protected String roundingtype;
        @XmlElement(name = "ROUNDING_INTERVAL")
        protected String roundinginterval;
        @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
        protected String roundingfieldslevel;
        @XmlElement(name = "ROUNDING_APPLICATION")
        protected String roundingapplication;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_CURRENCY_GID")
        protected String mincostcurrencygid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "MAX_COST")
        protected String maxcost;
        @XmlElement(name = "MAX_COST_CURRENCY_GID")
        protected String maxcostcurrencygid;
        @XmlElement(name = "MAX_COST_BASE")
        protected String maxcostbase;
        @XmlElement(name = "PAYMENT_METHOD_CODE_GID")
        protected String paymentmethodcodegid;
        @XmlElement(name = "EFFECTIVE_DATE")
        protected String effectivedate;
        @XmlElement(name = "EXPIRATION_DATE")
        protected String expirationdate;
        @XmlElement(name = "CALENDAR_GID")
        protected String calendargid;
        @XmlElement(name = "IS_FILED_AS_TARIFF")
        protected String isfiledastariff;
        @XmlElement(name = "TIER")
        protected String tier;
        @XmlElement(name = "COST_TYPE")
        protected String costtype;
        @XmlElement(name = "CALENDAR_ACTIVITY_GID")
        protected String calendaractivitygid;
        @XmlElement(name = "RATE_UNIT_BREAK_PROFILE_GID")
        protected String rateunitbreakprofilegid;
        @XmlElement(name = "RATE_UNIT_BREAK_PROFILE2_GID")
        protected String rateunitbreakprofile2GID;
        @XmlElement(name = "CHARGE_BREAK_COMPARATOR2_GID")
        protected String chargebreakcomparator2GID;
        @XmlElement(name = "EXTERNAL_RATING_ENGINE_GID")
        protected String externalratingenginegid;
        @XmlElement(name = "EXT_RE_FIELDSET_GID")
        protected String extrefieldsetgid;
        @XmlElement(name = "COST_CATEGORY_GID")
        protected String costcategorygid;
        @XmlElement(name = "COST_CODE_GID")
        protected String costcodegid;
        @XmlElement(name = "INSERT_USER")
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE")
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlElement(name = "RATE_GEO_COST_WEIGHT_BREAK")
        protected RATEGEOCOSTWEIGHTBREAKTYPE rategeocostweightbreak;
        @XmlElement(name = "RATE_GEO_COST_UNIT_BREAK")
        protected RATEGEOCOSTUNITBREAKTYPE rategeocostunitbreak;
        @XmlElement(name = "ATTRIBUTE1")
        protected String attribute1;
        @XmlElement(name = "ATTRIBUTE2")
        protected String attribute2;
        @XmlElement(name = "ATTRIBUTE3")
        protected String attribute3;
        @XmlElement(name = "ATTRIBUTE4")
        protected String attribute4;
        @XmlElement(name = "ATTRIBUTE5")
        protected String attribute5;
        @XmlElement(name = "ATTRIBUTE6")
        protected String attribute6;
        @XmlElement(name = "ATTRIBUTE7")
        protected String attribute7;
        @XmlElement(name = "ATTRIBUTE8")
        protected String attribute8;
        @XmlElement(name = "ATTRIBUTE9")
        protected String attribute9;
        @XmlElement(name = "ATTRIBUTE10")
        protected String attribute10;
        @XmlElement(name = "ATTRIBUTE11")
        protected String attribute11;
        @XmlElement(name = "ATTRIBUTE12")
        protected String attribute12;
        @XmlElement(name = "ATTRIBUTE13")
        protected String attribute13;
        @XmlElement(name = "ATTRIBUTE14")
        protected String attribute14;
        @XmlElement(name = "ATTRIBUTE15")
        protected String attribute15;
        @XmlElement(name = "ATTRIBUTE16")
        protected String attribute16;
        @XmlElement(name = "ATTRIBUTE17")
        protected String attribute17;
        @XmlElement(name = "ATTRIBUTE18")
        protected String attribute18;
        @XmlElement(name = "ATTRIBUTE19")
        protected String attribute19;
        @XmlElement(name = "ATTRIBUTE20")
        protected String attribute20;
        @XmlElement(name = "ATTRIBUTE_NUMBER1")
        protected String attributenumber1;
        @XmlElement(name = "ATTRIBUTE_NUMBER2")
        protected String attributenumber2;
        @XmlElement(name = "ATTRIBUTE_NUMBER3")
        protected String attributenumber3;
        @XmlElement(name = "ATTRIBUTE_NUMBER4")
        protected String attributenumber4;
        @XmlElement(name = "ATTRIBUTE_NUMBER5")
        protected String attributenumber5;
        @XmlElement(name = "ATTRIBUTE_NUMBER6")
        protected String attributenumber6;
        @XmlElement(name = "ATTRIBUTE_NUMBER7")
        protected String attributenumber7;
        @XmlElement(name = "ATTRIBUTE_NUMBER8")
        protected String attributenumber8;
        @XmlElement(name = "ATTRIBUTE_NUMBER9")
        protected String attributenumber9;
        @XmlElement(name = "ATTRIBUTE_NUMBER10")
        protected String attributenumber10;
        @XmlElement(name = "ATTRIBUTE_DATE1")
        protected String attributedate1;
        @XmlElement(name = "ATTRIBUTE_DATE2")
        protected String attributedate2;
        @XmlElement(name = "ATTRIBUTE_DATE3")
        protected String attributedate3;
        @XmlElement(name = "ATTRIBUTE_DATE4")
        protected String attributedate4;
        @XmlElement(name = "ATTRIBUTE_DATE5")
        protected String attributedate5;
        @XmlElement(name = "ATTRIBUTE_DATE6")
        protected String attributedate6;
        @XmlElement(name = "ATTRIBUTE_DATE7")
        protected String attributedate7;
        @XmlElement(name = "ATTRIBUTE_DATE8")
        protected String attributedate8;
        @XmlElement(name = "ATTRIBUTE_DATE9")
        protected String attributedate9;
        @XmlElement(name = "ATTRIBUTE_DATE10")
        protected String attributedate10;
        @XmlElement(name = "OPERAND_QUALIFIER1")
        protected String operandqualifier1;
        @XmlElement(name = "OPERAND_QUALIFIER2")
        protected String operandqualifier2;
        @XmlElement(name = "OPERAND_QUALIFIER3")
        protected String operandqualifier3;
        @XmlElement(name = "OPERAND_QUALIFIER4")
        protected String operandqualifier4;
        @XmlElement(name = "CHARGE_QUALIFIER1")
        protected String chargequalifier1;
        @XmlElement(name = "CHARGE_QUALIFIER2")
        protected String chargequalifier2;
        @XmlElement(name = "CHARGE_BREAK_COMP_QUALIFIER1")
        protected String chargebreakcompqualifier1;
        @XmlElement(name = "CHARGE_BREAK_COMP_QUALIFIER2")
        protected String chargebreakcompqualifier2;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Gets the value of the rategeocostseq property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTSEQ() {
            return rategeocostseq;
        }

        /**
         * Sets the value of the rategeocostseq property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTSEQ(String value) {
            this.rategeocostseq = value;
        }

        /**
         * Gets the value of the domainname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Sets the value of the domainname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDESCRIPTION() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDESCRIPTION(String value) {
            this.description = value;
        }

        /**
         * Gets the value of the equipmentgroupprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEQUIPMENTGROUPPROFILEGID() {
            return equipmentgroupprofilegid;
        }

        /**
         * Sets the value of the equipmentgroupprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEQUIPMENTGROUPPROFILEGID(String value) {
            this.equipmentgroupprofilegid = value;
        }

        /**
         * Gets the value of the tariffitemnumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFITEMNUMBER() {
            return tariffitemnumber;
        }

        /**
         * Sets the value of the tariffitemnumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFITEMNUMBER(String value) {
            this.tariffitemnumber = value;
        }

        /**
         * Gets the value of the tariffsection property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFSECTION() {
            return tariffsection;
        }

        /**
         * Sets the value of the tariffsection property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFSECTION(String value) {
            this.tariffsection = value;
        }

        /**
         * Gets the value of the tariffitemnumbersuffix property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFITEMNUMBERSUFFIX() {
            return tariffitemnumbersuffix;
        }

        /**
         * Sets the value of the tariffitemnumbersuffix property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFITEMNUMBERSUFFIX(String value) {
            this.tariffitemnumbersuffix = value;
        }

        /**
         * Gets the value of the tariffitempart property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFITEMPART() {
            return tariffitempart;
        }

        /**
         * Sets the value of the tariffitempart property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFITEMPART(String value) {
            this.tariffitempart = value;
        }

        /**
         * Gets the value of the tarifffreightclasscode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFFREIGHTCLASSCODE() {
            return tarifffreightclasscode;
        }

        /**
         * Sets the value of the tarifffreightclasscode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFFREIGHTCLASSCODE(String value) {
            this.tarifffreightclasscode = value;
        }

        /**
         * Gets the value of the exparte property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPARTE() {
            return exparte;
        }

        /**
         * Sets the value of the exparte property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPARTE(String value) {
            this.exparte = value;
        }

        /**
         * Gets the value of the ratebasisnumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEBASISNUMBER() {
            return ratebasisnumber;
        }

        /**
         * Sets the value of the ratebasisnumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEBASISNUMBER(String value) {
            this.ratebasisnumber = value;
        }

        /**
         * Gets the value of the tariffcolumn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFCOLUMN() {
            return tariffcolumn;
        }

        /**
         * Sets the value of the tariffcolumn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFCOLUMN(String value) {
            this.tariffcolumn = value;
        }

        /**
         * Gets the value of the tariffdistance property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFDISTANCE() {
            return tariffdistance;
        }

        /**
         * Sets the value of the tariffdistance property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFDISTANCE(String value) {
            this.tariffdistance = value;
        }

        /**
         * Gets the value of the tariffdistanceuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFDISTANCEUOMCODE() {
            return tariffdistanceuomcode;
        }

        /**
         * Sets the value of the tariffdistanceuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFDISTANCEUOMCODE(String value) {
            this.tariffdistanceuomcode = value;
        }

        /**
         * Gets the value of the tariffdistancebase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFDISTANCEBASE() {
            return tariffdistancebase;
        }

        /**
         * Sets the value of the tariffdistancebase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFDISTANCEBASE(String value) {
            this.tariffdistancebase = value;
        }

        /**
         * Gets the value of the distancequalifier property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDISTANCEQUALIFIER() {
            return distancequalifier;
        }

        /**
         * Sets the value of the distancequalifier property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDISTANCEQUALIFIER(String value) {
            this.distancequalifier = value;
        }

        /**
         * Gets the value of the ratebasisqualifier property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEBASISQUALIFIER() {
            return ratebasisqualifier;
        }

        /**
         * Sets the value of the ratebasisqualifier property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEBASISQUALIFIER(String value) {
            this.ratebasisqualifier = value;
        }

        /**
         * Gets the value of the rategeocostgroupgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTGROUPGID() {
            return rategeocostgroupgid;
        }

        /**
         * Sets the value of the rategeocostgroupgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTGROUPGID(String value) {
            this.rategeocostgroupgid = value;
        }

        /**
         * Gets the value of the rategeocostoperandseq property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTOPERANDSEQ() {
            return rategeocostoperandseq;
        }

        /**
         * Sets the value of the rategeocostoperandseq property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTOPERANDSEQ(String value) {
            this.rategeocostoperandseq = value;
        }

        /**
         * Gets the value of the oper1GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER1GID() {
            return oper1GID;
        }

        /**
         * Sets the value of the oper1GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER1GID(String value) {
            this.oper1GID = value;
        }

        /**
         * Gets the value of the leftoperand1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND1() {
            return leftoperand1;
        }

        /**
         * Sets the value of the leftoperand1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND1(String value) {
            this.leftoperand1 = value;
        }

        /**
         * Gets the value of the lowvalue1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE1() {
            return lowvalue1;
        }

        /**
         * Sets the value of the lowvalue1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE1(String value) {
            this.lowvalue1 = value;
        }

        /**
         * Gets the value of the highvalue1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE1() {
            return highvalue1;
        }

        /**
         * Sets the value of the highvalue1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE1(String value) {
            this.highvalue1 = value;
        }

        /**
         * Gets the value of the andor1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getANDOR1() {
            return andor1;
        }

        /**
         * Sets the value of the andor1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setANDOR1(String value) {
            this.andor1 = value;
        }

        /**
         * Gets the value of the oper2GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER2GID() {
            return oper2GID;
        }

        /**
         * Sets the value of the oper2GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER2GID(String value) {
            this.oper2GID = value;
        }

        /**
         * Gets the value of the leftoperand2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND2() {
            return leftoperand2;
        }

        /**
         * Sets the value of the leftoperand2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND2(String value) {
            this.leftoperand2 = value;
        }

        /**
         * Gets the value of the lowvalue2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE2() {
            return lowvalue2;
        }

        /**
         * Sets the value of the lowvalue2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE2(String value) {
            this.lowvalue2 = value;
        }

        /**
         * Gets the value of the highvalue2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE2() {
            return highvalue2;
        }

        /**
         * Sets the value of the highvalue2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE2(String value) {
            this.highvalue2 = value;
        }

        /**
         * Gets the value of the andor2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getANDOR2() {
            return andor2;
        }

        /**
         * Sets the value of the andor2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setANDOR2(String value) {
            this.andor2 = value;
        }

        /**
         * Gets the value of the oper3GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER3GID() {
            return oper3GID;
        }

        /**
         * Sets the value of the oper3GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER3GID(String value) {
            this.oper3GID = value;
        }

        /**
         * Gets the value of the leftoperand3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND3() {
            return leftoperand3;
        }

        /**
         * Sets the value of the leftoperand3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND3(String value) {
            this.leftoperand3 = value;
        }

        /**
         * Gets the value of the lowvalue3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE3() {
            return lowvalue3;
        }

        /**
         * Sets the value of the lowvalue3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE3(String value) {
            this.lowvalue3 = value;
        }

        /**
         * Gets the value of the highvalue3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE3() {
            return highvalue3;
        }

        /**
         * Sets the value of the highvalue3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE3(String value) {
            this.highvalue3 = value;
        }

        /**
         * Gets the value of the andor3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getANDOR3() {
            return andor3;
        }

        /**
         * Sets the value of the andor3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setANDOR3(String value) {
            this.andor3 = value;
        }

        /**
         * Gets the value of the oper4GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPER4GID() {
            return oper4GID;
        }

        /**
         * Sets the value of the oper4GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPER4GID(String value) {
            this.oper4GID = value;
        }

        /**
         * Gets the value of the leftoperand4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLEFTOPERAND4() {
            return leftoperand4;
        }

        /**
         * Sets the value of the leftoperand4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLEFTOPERAND4(String value) {
            this.leftoperand4 = value;
        }

        /**
         * Gets the value of the lowvalue4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOWVALUE4() {
            return lowvalue4;
        }

        /**
         * Sets the value of the lowvalue4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOWVALUE4(String value) {
            this.lowvalue4 = value;
        }

        /**
         * Gets the value of the highvalue4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHIGHVALUE4() {
            return highvalue4;
        }

        /**
         * Sets the value of the highvalue4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHIGHVALUE4(String value) {
            this.highvalue4 = value;
        }

        /**
         * Gets the value of the chargeamount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNT() {
            return chargeamount;
        }

        /**
         * Sets the value of the chargeamount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNT(String value) {
            this.chargeamount = value;
        }

        /**
         * Gets the value of the chargecurrencygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGECURRENCYGID() {
            return chargecurrencygid;
        }

        /**
         * Sets the value of the chargecurrencygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGECURRENCYGID(String value) {
            this.chargecurrencygid = value;
        }

        /**
         * Gets the value of the chargeamountbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNTBASE() {
            return chargeamountbase;
        }

        /**
         * Sets the value of the chargeamountbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNTBASE(String value) {
            this.chargeamountbase = value;
        }

        /**
         * Gets the value of the chargeunituomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITUOMCODE() {
            return chargeunituomcode;
        }

        /**
         * Sets the value of the chargeunituomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITUOMCODE(String value) {
            this.chargeunituomcode = value;
        }

        /**
         * Gets the value of the chargeunitcount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITCOUNT() {
            return chargeunitcount;
        }

        /**
         * Sets the value of the chargeunitcount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITCOUNT(String value) {
            this.chargeunitcount = value;
        }

        /**
         * Gets the value of the chargemultiplier property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIER() {
            return chargemultiplier;
        }

        /**
         * Sets the value of the chargemultiplier property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIER(String value) {
            this.chargemultiplier = value;
        }

        /**
         * Gets the value of the chargemultiplierscalar property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIERSCALAR() {
            return chargemultiplierscalar;
        }

        /**
         * Sets the value of the chargemultiplierscalar property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIERSCALAR(String value) {
            this.chargemultiplierscalar = value;
        }

        /**
         * Gets the value of the chargeaction property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEACTION() {
            return chargeaction;
        }

        /**
         * Sets the value of the chargeaction property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEACTION(String value) {
            this.chargeaction = value;
        }

        /**
         * Gets the value of the chargebreakcomparator property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEBREAKCOMPARATOR() {
            return chargebreakcomparator;
        }

        /**
         * Sets the value of the chargebreakcomparator property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEBREAKCOMPARATOR(String value) {
            this.chargebreakcomparator = value;
        }

        /**
         * Gets the value of the chargetype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGETYPE() {
            return chargetype;
        }

        /**
         * Sets the value of the chargetype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGETYPE(String value) {
            this.chargetype = value;
        }

        /**
         * Gets the value of the chargemultiplieroption property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIEROPTION() {
            return chargemultiplieroption;
        }

        /**
         * Sets the value of the chargemultiplieroption property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIEROPTION(String value) {
            this.chargemultiplieroption = value;
        }

        /**
         * Gets the value of the chargemultiplier2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEMULTIPLIER2() {
            return chargemultiplier2;
        }

        /**
         * Sets the value of the chargemultiplier2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEMULTIPLIER2(String value) {
            this.chargemultiplier2 = value;
        }

        /**
         * Gets the value of the chargeunitcount2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITCOUNT2() {
            return chargeunitcount2;
        }

        /**
         * Sets the value of the chargeunitcount2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITCOUNT2(String value) {
            this.chargeunitcount2 = value;
        }

        /**
         * Gets the value of the chargeunituomcode2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEUNITUOMCODE2() {
            return chargeunituomcode2;
        }

        /**
         * Sets the value of the chargeunituomcode2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEUNITUOMCODE2(String value) {
            this.chargeunituomcode2 = value;
        }

        /**
         * Gets the value of the chargesequence property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGESEQUENCE() {
            return chargesequence;
        }

        /**
         * Sets the value of the chargesequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGESEQUENCE(String value) {
            this.chargesequence = value;
        }

        /**
         * Gets the value of the usesunitbreaks property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSESUNITBREAKS() {
            return usesunitbreaks;
        }

        /**
         * Sets the value of the usesunitbreaks property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSESUNITBREAKS(String value) {
            this.usesunitbreaks = value;
        }

        /**
         * Gets the value of the dimratefactorgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDIMRATEFACTORGID() {
            return dimratefactorgid;
        }

        /**
         * Sets the value of the dimratefactorgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDIMRATEFACTORGID(String value) {
            this.dimratefactorgid = value;
        }

        /**
         * Gets the value of the roundingtype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGTYPE() {
            return roundingtype;
        }

        /**
         * Sets the value of the roundingtype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGTYPE(String value) {
            this.roundingtype = value;
        }

        /**
         * Gets the value of the roundinginterval property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGINTERVAL() {
            return roundinginterval;
        }

        /**
         * Sets the value of the roundinginterval property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGINTERVAL(String value) {
            this.roundinginterval = value;
        }

        /**
         * Gets the value of the roundingfieldslevel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGFIELDSLEVEL() {
            return roundingfieldslevel;
        }

        /**
         * Sets the value of the roundingfieldslevel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGFIELDSLEVEL(String value) {
            this.roundingfieldslevel = value;
        }

        /**
         * Gets the value of the roundingapplication property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGAPPLICATION() {
            return roundingapplication;
        }

        /**
         * Sets the value of the roundingapplication property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGAPPLICATION(String value) {
            this.roundingapplication = value;
        }

        /**
         * Gets the value of the mincost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Sets the value of the mincost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Gets the value of the mincostcurrencygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTCURRENCYGID() {
            return mincostcurrencygid;
        }

        /**
         * Sets the value of the mincostcurrencygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTCURRENCYGID(String value) {
            this.mincostcurrencygid = value;
        }

        /**
         * Gets the value of the mincostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Sets the value of the mincostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Gets the value of the maxcost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOST() {
            return maxcost;
        }

        /**
         * Sets the value of the maxcost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOST(String value) {
            this.maxcost = value;
        }

        /**
         * Gets the value of the maxcostcurrencygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTCURRENCYGID() {
            return maxcostcurrencygid;
        }

        /**
         * Sets the value of the maxcostcurrencygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTCURRENCYGID(String value) {
            this.maxcostcurrencygid = value;
        }

        /**
         * Gets the value of the maxcostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTBASE() {
            return maxcostbase;
        }

        /**
         * Sets the value of the maxcostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTBASE(String value) {
            this.maxcostbase = value;
        }

        /**
         * Gets the value of the paymentmethodcodegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPAYMENTMETHODCODEGID() {
            return paymentmethodcodegid;
        }

        /**
         * Sets the value of the paymentmethodcodegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPAYMENTMETHODCODEGID(String value) {
            this.paymentmethodcodegid = value;
        }

        /**
         * Gets the value of the effectivedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEFFECTIVEDATE() {
            return effectivedate;
        }

        /**
         * Sets the value of the effectivedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEFFECTIVEDATE(String value) {
            this.effectivedate = value;
        }

        /**
         * Gets the value of the expirationdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIRATIONDATE() {
            return expirationdate;
        }

        /**
         * Sets the value of the expirationdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIRATIONDATE(String value) {
            this.expirationdate = value;
        }

        /**
         * Gets the value of the calendargid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCALENDARGID() {
            return calendargid;
        }

        /**
         * Sets the value of the calendargid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCALENDARGID(String value) {
            this.calendargid = value;
        }

        /**
         * Gets the value of the isfiledastariff property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISFILEDASTARIFF() {
            return isfiledastariff;
        }

        /**
         * Sets the value of the isfiledastariff property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISFILEDASTARIFF(String value) {
            this.isfiledastariff = value;
        }

        /**
         * Gets the value of the tier property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTIER() {
            return tier;
        }

        /**
         * Sets the value of the tier property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTIER(String value) {
            this.tier = value;
        }

        /**
         * Gets the value of the costtype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOSTTYPE() {
            return costtype;
        }

        /**
         * Sets the value of the costtype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOSTTYPE(String value) {
            this.costtype = value;
        }

        /**
         * Gets the value of the calendaractivitygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCALENDARACTIVITYGID() {
            return calendaractivitygid;
        }

        /**
         * Sets the value of the calendaractivitygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCALENDARACTIVITYGID(String value) {
            this.calendaractivitygid = value;
        }

        /**
         * Gets the value of the rateunitbreakprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAKPROFILEGID() {
            return rateunitbreakprofilegid;
        }

        /**
         * Sets the value of the rateunitbreakprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAKPROFILEGID(String value) {
            this.rateunitbreakprofilegid = value;
        }

        /**
         * Gets the value of the rateunitbreakprofile2GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAKPROFILE2GID() {
            return rateunitbreakprofile2GID;
        }

        /**
         * Sets the value of the rateunitbreakprofile2GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAKPROFILE2GID(String value) {
            this.rateunitbreakprofile2GID = value;
        }

        /**
         * Gets the value of the chargebreakcomparator2GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEBREAKCOMPARATOR2GID() {
            return chargebreakcomparator2GID;
        }

        /**
         * Sets the value of the chargebreakcomparator2GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEBREAKCOMPARATOR2GID(String value) {
            this.chargebreakcomparator2GID = value;
        }

        /**
         * Gets the value of the externalratingenginegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTERNALRATINGENGINEGID() {
            return externalratingenginegid;
        }

        /**
         * Sets the value of the externalratingenginegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTERNALRATINGENGINEGID(String value) {
            this.externalratingenginegid = value;
        }

        /**
         * Gets the value of the extrefieldsetgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTREFIELDSETGID() {
            return extrefieldsetgid;
        }

        /**
         * Sets the value of the extrefieldsetgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTREFIELDSETGID(String value) {
            this.extrefieldsetgid = value;
        }

        /**
         * Gets the value of the costcategorygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOSTCATEGORYGID() {
            return costcategorygid;
        }

        /**
         * Sets the value of the costcategorygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOSTCATEGORYGID(String value) {
            this.costcategorygid = value;
        }

        /**
         * Gets the value of the costcodegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOSTCODEGID() {
            return costcodegid;
        }

        /**
         * Sets the value of the costcodegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOSTCODEGID(String value) {
            this.costcodegid = value;
        }

        /**
         * Gets the value of the insertuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Sets the value of the insertuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Gets the value of the insertdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Sets the value of the insertdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Gets the value of the updateuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Sets the value of the updateuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Gets the value of the updatedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Sets the value of the updatedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Gets the value of the rategeocostweightbreak property.
         * 
         * @return
         *     possible object is
         *     {@link RATEGEOCOSTWEIGHTBREAKTYPE }
         *     
         */
        public RATEGEOCOSTWEIGHTBREAKTYPE getRATEGEOCOSTWEIGHTBREAK() {
            return rategeocostweightbreak;
        }

        /**
         * Sets the value of the rategeocostweightbreak property.
         * 
         * @param value
         *     allowed object is
         *     {@link RATEGEOCOSTWEIGHTBREAKTYPE }
         *     
         */
        public void setRATEGEOCOSTWEIGHTBREAK(RATEGEOCOSTWEIGHTBREAKTYPE value) {
            this.rategeocostweightbreak = value;
        }

        /**
         * Gets the value of the rategeocostunitbreak property.
         * 
         * @return
         *     possible object is
         *     {@link RATEGEOCOSTUNITBREAKTYPE }
         *     
         */
        public RATEGEOCOSTUNITBREAKTYPE getRATEGEOCOSTUNITBREAK() {
            return rategeocostunitbreak;
        }

        /**
         * Sets the value of the rategeocostunitbreak property.
         * 
         * @param value
         *     allowed object is
         *     {@link RATEGEOCOSTUNITBREAKTYPE }
         *     
         */
        public void setRATEGEOCOSTUNITBREAK(RATEGEOCOSTUNITBREAKTYPE value) {
            this.rategeocostunitbreak = value;
        }

        /**
         * Gets the value of the attribute1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE1() {
            return attribute1;
        }

        /**
         * Sets the value of the attribute1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE1(String value) {
            this.attribute1 = value;
        }

        /**
         * Gets the value of the attribute2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE2() {
            return attribute2;
        }

        /**
         * Sets the value of the attribute2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE2(String value) {
            this.attribute2 = value;
        }

        /**
         * Gets the value of the attribute3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE3() {
            return attribute3;
        }

        /**
         * Sets the value of the attribute3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE3(String value) {
            this.attribute3 = value;
        }

        /**
         * Gets the value of the attribute4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE4() {
            return attribute4;
        }

        /**
         * Sets the value of the attribute4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE4(String value) {
            this.attribute4 = value;
        }

        /**
         * Gets the value of the attribute5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE5() {
            return attribute5;
        }

        /**
         * Sets the value of the attribute5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE5(String value) {
            this.attribute5 = value;
        }

        /**
         * Gets the value of the attribute6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE6() {
            return attribute6;
        }

        /**
         * Sets the value of the attribute6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE6(String value) {
            this.attribute6 = value;
        }

        /**
         * Gets the value of the attribute7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE7() {
            return attribute7;
        }

        /**
         * Sets the value of the attribute7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE7(String value) {
            this.attribute7 = value;
        }

        /**
         * Gets the value of the attribute8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE8() {
            return attribute8;
        }

        /**
         * Sets the value of the attribute8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE8(String value) {
            this.attribute8 = value;
        }

        /**
         * Gets the value of the attribute9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE9() {
            return attribute9;
        }

        /**
         * Sets the value of the attribute9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE9(String value) {
            this.attribute9 = value;
        }

        /**
         * Gets the value of the attribute10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE10() {
            return attribute10;
        }

        /**
         * Sets the value of the attribute10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE10(String value) {
            this.attribute10 = value;
        }

        /**
         * Gets the value of the attribute11 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE11() {
            return attribute11;
        }

        /**
         * Sets the value of the attribute11 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE11(String value) {
            this.attribute11 = value;
        }

        /**
         * Gets the value of the attribute12 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE12() {
            return attribute12;
        }

        /**
         * Sets the value of the attribute12 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE12(String value) {
            this.attribute12 = value;
        }

        /**
         * Gets the value of the attribute13 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE13() {
            return attribute13;
        }

        /**
         * Sets the value of the attribute13 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE13(String value) {
            this.attribute13 = value;
        }

        /**
         * Gets the value of the attribute14 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE14() {
            return attribute14;
        }

        /**
         * Sets the value of the attribute14 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE14(String value) {
            this.attribute14 = value;
        }

        /**
         * Gets the value of the attribute15 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE15() {
            return attribute15;
        }

        /**
         * Sets the value of the attribute15 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE15(String value) {
            this.attribute15 = value;
        }

        /**
         * Gets the value of the attribute16 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE16() {
            return attribute16;
        }

        /**
         * Sets the value of the attribute16 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE16(String value) {
            this.attribute16 = value;
        }

        /**
         * Gets the value of the attribute17 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE17() {
            return attribute17;
        }

        /**
         * Sets the value of the attribute17 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE17(String value) {
            this.attribute17 = value;
        }

        /**
         * Gets the value of the attribute18 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE18() {
            return attribute18;
        }

        /**
         * Sets the value of the attribute18 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE18(String value) {
            this.attribute18 = value;
        }

        /**
         * Gets the value of the attribute19 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE19() {
            return attribute19;
        }

        /**
         * Sets the value of the attribute19 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE19(String value) {
            this.attribute19 = value;
        }

        /**
         * Gets the value of the attribute20 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE20() {
            return attribute20;
        }

        /**
         * Sets the value of the attribute20 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE20(String value) {
            this.attribute20 = value;
        }

        /**
         * Gets the value of the attributenumber1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER1() {
            return attributenumber1;
        }

        /**
         * Sets the value of the attributenumber1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER1(String value) {
            this.attributenumber1 = value;
        }

        /**
         * Gets the value of the attributenumber2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER2() {
            return attributenumber2;
        }

        /**
         * Sets the value of the attributenumber2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER2(String value) {
            this.attributenumber2 = value;
        }

        /**
         * Gets the value of the attributenumber3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER3() {
            return attributenumber3;
        }

        /**
         * Sets the value of the attributenumber3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER3(String value) {
            this.attributenumber3 = value;
        }

        /**
         * Gets the value of the attributenumber4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER4() {
            return attributenumber4;
        }

        /**
         * Sets the value of the attributenumber4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER4(String value) {
            this.attributenumber4 = value;
        }

        /**
         * Gets the value of the attributenumber5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER5() {
            return attributenumber5;
        }

        /**
         * Sets the value of the attributenumber5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER5(String value) {
            this.attributenumber5 = value;
        }

        /**
         * Gets the value of the attributenumber6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER6() {
            return attributenumber6;
        }

        /**
         * Sets the value of the attributenumber6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER6(String value) {
            this.attributenumber6 = value;
        }

        /**
         * Gets the value of the attributenumber7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER7() {
            return attributenumber7;
        }

        /**
         * Sets the value of the attributenumber7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER7(String value) {
            this.attributenumber7 = value;
        }

        /**
         * Gets the value of the attributenumber8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER8() {
            return attributenumber8;
        }

        /**
         * Sets the value of the attributenumber8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER8(String value) {
            this.attributenumber8 = value;
        }

        /**
         * Gets the value of the attributenumber9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER9() {
            return attributenumber9;
        }

        /**
         * Sets the value of the attributenumber9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER9(String value) {
            this.attributenumber9 = value;
        }

        /**
         * Gets the value of the attributenumber10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER10() {
            return attributenumber10;
        }

        /**
         * Sets the value of the attributenumber10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER10(String value) {
            this.attributenumber10 = value;
        }

        /**
         * Gets the value of the attributedate1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE1() {
            return attributedate1;
        }

        /**
         * Sets the value of the attributedate1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE1(String value) {
            this.attributedate1 = value;
        }

        /**
         * Gets the value of the attributedate2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE2() {
            return attributedate2;
        }

        /**
         * Sets the value of the attributedate2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE2(String value) {
            this.attributedate2 = value;
        }

        /**
         * Gets the value of the attributedate3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE3() {
            return attributedate3;
        }

        /**
         * Sets the value of the attributedate3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE3(String value) {
            this.attributedate3 = value;
        }

        /**
         * Gets the value of the attributedate4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE4() {
            return attributedate4;
        }

        /**
         * Sets the value of the attributedate4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE4(String value) {
            this.attributedate4 = value;
        }

        /**
         * Gets the value of the attributedate5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE5() {
            return attributedate5;
        }

        /**
         * Sets the value of the attributedate5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE5(String value) {
            this.attributedate5 = value;
        }

        /**
         * Gets the value of the attributedate6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE6() {
            return attributedate6;
        }

        /**
         * Sets the value of the attributedate6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE6(String value) {
            this.attributedate6 = value;
        }

        /**
         * Gets the value of the attributedate7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE7() {
            return attributedate7;
        }

        /**
         * Sets the value of the attributedate7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE7(String value) {
            this.attributedate7 = value;
        }

        /**
         * Gets the value of the attributedate8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE8() {
            return attributedate8;
        }

        /**
         * Sets the value of the attributedate8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE8(String value) {
            this.attributedate8 = value;
        }

        /**
         * Gets the value of the attributedate9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE9() {
            return attributedate9;
        }

        /**
         * Sets the value of the attributedate9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE9(String value) {
            this.attributedate9 = value;
        }

        /**
         * Gets the value of the attributedate10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE10() {
            return attributedate10;
        }

        /**
         * Sets the value of the attributedate10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE10(String value) {
            this.attributedate10 = value;
        }

        /**
         * Gets the value of the operandqualifier1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPERANDQUALIFIER1() {
            return operandqualifier1;
        }

        /**
         * Sets the value of the operandqualifier1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPERANDQUALIFIER1(String value) {
            this.operandqualifier1 = value;
        }

        /**
         * Gets the value of the operandqualifier2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPERANDQUALIFIER2() {
            return operandqualifier2;
        }

        /**
         * Sets the value of the operandqualifier2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPERANDQUALIFIER2(String value) {
            this.operandqualifier2 = value;
        }

        /**
         * Gets the value of the operandqualifier3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPERANDQUALIFIER3() {
            return operandqualifier3;
        }

        /**
         * Sets the value of the operandqualifier3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPERANDQUALIFIER3(String value) {
            this.operandqualifier3 = value;
        }

        /**
         * Gets the value of the operandqualifier4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOPERANDQUALIFIER4() {
            return operandqualifier4;
        }

        /**
         * Sets the value of the operandqualifier4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOPERANDQUALIFIER4(String value) {
            this.operandqualifier4 = value;
        }

        /**
         * Gets the value of the chargequalifier1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEQUALIFIER1() {
            return chargequalifier1;
        }

        /**
         * Sets the value of the chargequalifier1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEQUALIFIER1(String value) {
            this.chargequalifier1 = value;
        }

        /**
         * Gets the value of the chargequalifier2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEQUALIFIER2() {
            return chargequalifier2;
        }

        /**
         * Sets the value of the chargequalifier2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEQUALIFIER2(String value) {
            this.chargequalifier2 = value;
        }

        /**
         * Gets the value of the chargebreakcompqualifier1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEBREAKCOMPQUALIFIER1() {
            return chargebreakcompqualifier1;
        }

        /**
         * Sets the value of the chargebreakcompqualifier1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEBREAKCOMPQUALIFIER1(String value) {
            this.chargebreakcompqualifier1 = value;
        }

        /**
         * Gets the value of the chargebreakcompqualifier2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEBREAKCOMPQUALIFIER2() {
            return chargebreakcompqualifier2;
        }

        /**
         * Sets the value of the chargebreakcompqualifier2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEBREAKCOMPQUALIFIER2(String value) {
            this.chargebreakcompqualifier2 = value;
        }

        /**
         * Gets the value of the num property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Sets the value of the num property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }

    }

}

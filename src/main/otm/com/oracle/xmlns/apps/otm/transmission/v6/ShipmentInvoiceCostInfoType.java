
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the invoice cost information that has been copied to the shipment.
 * 
 * <p>Java class for ShipmentInvoiceCostInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentInvoiceCostInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TotalMatchedInvoiceCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="OriginalInvoiceCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="TotalApprovedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentInvoiceCostInfoType", propOrder = {
    "totalMatchedInvoiceCost",
    "originalInvoiceCost",
    "totalApprovedCost"
})
public class ShipmentInvoiceCostInfoType {

    @XmlElement(name = "TotalMatchedInvoiceCost")
    protected GLogXMLFinancialAmountType totalMatchedInvoiceCost;
    @XmlElement(name = "OriginalInvoiceCost")
    protected GLogXMLFinancialAmountType originalInvoiceCost;
    @XmlElement(name = "TotalApprovedCost")
    protected GLogXMLFinancialAmountType totalApprovedCost;

    /**
     * Gets the value of the totalMatchedInvoiceCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalMatchedInvoiceCost() {
        return totalMatchedInvoiceCost;
    }

    /**
     * Sets the value of the totalMatchedInvoiceCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalMatchedInvoiceCost(GLogXMLFinancialAmountType value) {
        this.totalMatchedInvoiceCost = value;
    }

    /**
     * Gets the value of the originalInvoiceCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getOriginalInvoiceCost() {
        return originalInvoiceCost;
    }

    /**
     * Sets the value of the originalInvoiceCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setOriginalInvoiceCost(GLogXMLFinancialAmountType value) {
        this.originalInvoiceCost = value;
    }

    /**
     * Gets the value of the totalApprovedCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalApprovedCost() {
        return totalApprovedCost;
    }

    /**
     * Sets the value of the totalApprovedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalApprovedCost(GLogXMLFinancialAmountType value) {
        this.totalApprovedCost = value;
    }

}

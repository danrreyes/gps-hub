
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to query for a shipment appointment.
 * 
 * <p>Java class for AppointmentQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AppointmentQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="ShipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsOverrideAppointment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppointmentQueryType", propOrder = {
    "shipmentGid",
    "shipmentGroupGid",
    "locationGid",
    "stopSequence",
    "isOverrideAppointment"
})
public class AppointmentQueryType {

    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ShipmentGroupGid")
    protected GLogXMLGidType shipmentGroupGid;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "StopSequence")
    protected String stopSequence;
    @XmlElement(name = "IsOverrideAppointment")
    protected String isOverrideAppointment;

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the shipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGroupGid() {
        return shipmentGroupGid;
    }

    /**
     * Sets the value of the shipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGroupGid(GLogXMLGidType value) {
        this.shipmentGroupGid = value;
    }

    /**
     * Gets the value of the locationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Sets the value of the locationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Gets the value of the stopSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Sets the value of the stopSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Gets the value of the isOverrideAppointment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOverrideAppointment() {
        return isOverrideAppointment;
    }

    /**
     * Sets the value of the isOverrideAppointment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOverrideAppointment(String value) {
        this.isOverrideAppointment = value;
    }

}

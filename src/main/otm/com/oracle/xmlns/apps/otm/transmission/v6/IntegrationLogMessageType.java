
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An IntegrationLogMessage describes an error found in a transmission, transaction or any other type (like Message).
 * 
 * <p>Java class for IntegrationLogMessageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntegrationLogMessageType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ILogSeqNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="WrittenBy" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IMessageClass" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IMessageCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IMessageText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProcessingErrorCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DateTimeStamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="DomainName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IntegrationLogDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntegrationLogDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntegrationLogMessageType", propOrder = {
    "iLogSeqNo",
    "iTransactionNo",
    "objectGid",
    "writtenBy",
    "iMessageClass",
    "iMessageCode",
    "iMessageText",
    "processingErrorCodeGid",
    "dateTimeStamp",
    "domainName",
    "integrationLogDetail"
})
public class IntegrationLogMessageType {

    @XmlElement(name = "ILogSeqNo", required = true)
    protected String iLogSeqNo;
    @XmlElement(name = "ITransactionNo")
    protected String iTransactionNo;
    @XmlElement(name = "ObjectGid")
    protected GLogXMLGidType objectGid;
    @XmlElement(name = "WrittenBy", required = true)
    protected String writtenBy;
    @XmlElement(name = "IMessageClass", required = true)
    protected String iMessageClass;
    @XmlElement(name = "IMessageCode", required = true)
    protected String iMessageCode;
    @XmlElement(name = "IMessageText")
    protected String iMessageText;
    @XmlElement(name = "ProcessingErrorCodeGid")
    protected GLogXMLGidType processingErrorCodeGid;
    @XmlElement(name = "DateTimeStamp", required = true)
    protected GLogDateTimeType dateTimeStamp;
    @XmlElement(name = "DomainName", required = true)
    protected String domainName;
    @XmlElement(name = "IntegrationLogDetail")
    protected List<IntegrationLogDetailType> integrationLogDetail;

    /**
     * Gets the value of the iLogSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getILogSeqNo() {
        return iLogSeqNo;
    }

    /**
     * Sets the value of the iLogSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setILogSeqNo(String value) {
        this.iLogSeqNo = value;
    }

    /**
     * Gets the value of the iTransactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Sets the value of the iTransactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Gets the value of the objectGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getObjectGid() {
        return objectGid;
    }

    /**
     * Sets the value of the objectGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setObjectGid(GLogXMLGidType value) {
        this.objectGid = value;
    }

    /**
     * Gets the value of the writtenBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWrittenBy() {
        return writtenBy;
    }

    /**
     * Sets the value of the writtenBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWrittenBy(String value) {
        this.writtenBy = value;
    }

    /**
     * Gets the value of the iMessageClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMessageClass() {
        return iMessageClass;
    }

    /**
     * Sets the value of the iMessageClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMessageClass(String value) {
        this.iMessageClass = value;
    }

    /**
     * Gets the value of the iMessageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMessageCode() {
        return iMessageCode;
    }

    /**
     * Sets the value of the iMessageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMessageCode(String value) {
        this.iMessageCode = value;
    }

    /**
     * Gets the value of the iMessageText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMessageText() {
        return iMessageText;
    }

    /**
     * Sets the value of the iMessageText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMessageText(String value) {
        this.iMessageText = value;
    }

    /**
     * Gets the value of the processingErrorCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getProcessingErrorCodeGid() {
        return processingErrorCodeGid;
    }

    /**
     * Sets the value of the processingErrorCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setProcessingErrorCodeGid(GLogXMLGidType value) {
        this.processingErrorCodeGid = value;
    }

    /**
     * Gets the value of the dateTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDateTimeStamp() {
        return dateTimeStamp;
    }

    /**
     * Sets the value of the dateTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDateTimeStamp(GLogDateTimeType value) {
        this.dateTimeStamp = value;
    }

    /**
     * Gets the value of the domainName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDomainName() {
        return domainName;
    }

    /**
     * Sets the value of the domainName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDomainName(String value) {
        this.domainName = value;
    }

    /**
     * Gets the value of the integrationLogDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the integrationLogDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntegrationLogDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntegrationLogDetailType }
     * 
     * 
     */
    public List<IntegrationLogDetailType> getIntegrationLogDetail() {
        if (integrationLogDetail == null) {
            integrationLogDetail = new ArrayList<IntegrationLogDetailType>();
        }
        return this.integrationLogDetail;
    }

}

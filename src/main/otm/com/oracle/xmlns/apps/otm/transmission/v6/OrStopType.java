
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Defines intermediate stops on an order
 * 
 * <p>Java class for OrStopType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrStopType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrStopGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="OrStopSeq" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/&gt;
 *           &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="EarlyArrivalTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LateArrivalTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="IsAppointment" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IsAppointmentRequired" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IsApptConfirmRequired" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IsApptConfirm" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LegPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OrStopSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrStopSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrStopType", propOrder = {
    "orStopGid",
    "transactionCode",
    "orStopSeq",
    "location",
    "locationGid",
    "earlyArrivalTime",
    "lateArrivalTime",
    "isAppointment",
    "isAppointmentRequired",
    "isApptConfirmRequired",
    "isApptConfirm",
    "legPosition",
    "remark",
    "orStopSpecialService"
})
public class OrStopType {

    @XmlElement(name = "OrStopGid", required = true)
    protected GLogXMLGidType orStopGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "OrStopSeq", required = true)
    protected String orStopSeq;
    @XmlElement(name = "Location")
    protected LocationType location;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "EarlyArrivalTime")
    protected GLogDateTimeType earlyArrivalTime;
    @XmlElement(name = "LateArrivalTime")
    protected GLogDateTimeType lateArrivalTime;
    @XmlElement(name = "IsAppointment", required = true)
    protected String isAppointment;
    @XmlElement(name = "IsAppointmentRequired", required = true)
    protected String isAppointmentRequired;
    @XmlElement(name = "IsApptConfirmRequired", required = true)
    protected String isApptConfirmRequired;
    @XmlElement(name = "IsApptConfirm", required = true)
    protected String isApptConfirm;
    @XmlElement(name = "LegPosition")
    protected String legPosition;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "OrStopSpecialService")
    protected List<OrStopSpecialServiceType> orStopSpecialService;

    /**
     * Gets the value of the orStopGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrStopGid() {
        return orStopGid;
    }

    /**
     * Sets the value of the orStopGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrStopGid(GLogXMLGidType value) {
        this.orStopGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the orStopSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrStopSeq() {
        return orStopSeq;
    }

    /**
     * Sets the value of the orStopSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrStopSeq(String value) {
        this.orStopSeq = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link LocationType }
     *     
     */
    public LocationType getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType }
     *     
     */
    public void setLocation(LocationType value) {
        this.location = value;
    }

    /**
     * Gets the value of the locationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Sets the value of the locationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Gets the value of the earlyArrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyArrivalTime() {
        return earlyArrivalTime;
    }

    /**
     * Sets the value of the earlyArrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyArrivalTime(GLogDateTimeType value) {
        this.earlyArrivalTime = value;
    }

    /**
     * Gets the value of the lateArrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLateArrivalTime() {
        return lateArrivalTime;
    }

    /**
     * Sets the value of the lateArrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLateArrivalTime(GLogDateTimeType value) {
        this.lateArrivalTime = value;
    }

    /**
     * Gets the value of the isAppointment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAppointment() {
        return isAppointment;
    }

    /**
     * Sets the value of the isAppointment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAppointment(String value) {
        this.isAppointment = value;
    }

    /**
     * Gets the value of the isAppointmentRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAppointmentRequired() {
        return isAppointmentRequired;
    }

    /**
     * Sets the value of the isAppointmentRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAppointmentRequired(String value) {
        this.isAppointmentRequired = value;
    }

    /**
     * Gets the value of the isApptConfirmRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsApptConfirmRequired() {
        return isApptConfirmRequired;
    }

    /**
     * Sets the value of the isApptConfirmRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsApptConfirmRequired(String value) {
        this.isApptConfirmRequired = value;
    }

    /**
     * Gets the value of the isApptConfirm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsApptConfirm() {
        return isApptConfirm;
    }

    /**
     * Sets the value of the isApptConfirm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsApptConfirm(String value) {
        this.isApptConfirm = value;
    }

    /**
     * Gets the value of the legPosition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegPosition() {
        return legPosition;
    }

    /**
     * Sets the value of the legPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegPosition(String value) {
        this.legPosition = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the orStopSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orStopSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrStopSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrStopSpecialServiceType }
     * 
     * 
     */
    public List<OrStopSpecialServiceType> getOrStopSpecialService() {
        if (orStopSpecialService == null) {
            orStopSpecialService = new ArrayList<OrStopSpecialServiceType>();
        }
        return this.orStopSpecialService;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionDocumentType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransmissionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionHeaderType"/&gt;
 *         &lt;element name="TransmissionBody"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transmissionHeader",
    "transmissionBody"
})
@XmlRootElement(name = "Transmission")
public class Transmission
    extends TransmissionDocumentType
{

    @XmlElement(name = "TransmissionHeader", required = true)
    protected TransmissionHeaderType transmissionHeader;
    @XmlElement(name = "TransmissionBody", required = true)
    protected Transmission.TransmissionBody transmissionBody;
    @XmlAttribute(name = "Version")
    protected String version;

    /**
     * Gets the value of the transmissionHeader property.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionHeaderType }
     *     
     */
    public TransmissionHeaderType getTransmissionHeader() {
        return transmissionHeader;
    }

    /**
     * Sets the value of the transmissionHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionHeaderType }
     *     
     */
    public void setTransmissionHeader(TransmissionHeaderType value) {
        this.transmissionHeader = value;
    }

    /**
     * Gets the value of the transmissionBody property.
     * 
     * @return
     *     possible object is
     *     {@link Transmission.TransmissionBody }
     *     
     */
    public Transmission.TransmissionBody getTransmissionBody() {
        return transmissionBody;
    }

    /**
     * Sets the value of the transmissionBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transmission.TransmissionBody }
     *     
     */
    public void setTransmissionBody(Transmission.TransmissionBody value) {
        this.transmissionBody = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "gLogXMLElement"
    })
    public static class TransmissionBody {

        @XmlElement(name = "GLogXMLElement", required = true)
        protected List<GLogXMLElementType> gLogXMLElement;

        /**
         * Gets the value of the gLogXMLElement property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the gLogXMLElement property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGLogXMLElement().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLElementType }
         * 
         * 
         */
        public List<GLogXMLElementType> getGLogXMLElement() {
            if (gLogXMLElement == null) {
                gLogXMLElement = new ArrayList<GLogXMLElementType>();
            }
            return this.gLogXMLElement;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the master B/L notify information.
 * 
 * <p>Java class for MasterBLNotifyInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MasterBLNotifyInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MasterBLNotifyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ContactRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactRefType" minOccurs="0"/&gt;
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MasterBLNotifyInfoType", propOrder = {
    "masterBLNotifyType",
    "contactRef",
    "comMethodGid"
})
public class MasterBLNotifyInfoType {

    @XmlElement(name = "MasterBLNotifyType")
    protected String masterBLNotifyType;
    @XmlElement(name = "ContactRef")
    protected ContactRefType contactRef;
    @XmlElement(name = "ComMethodGid")
    protected GLogXMLGidType comMethodGid;

    /**
     * Gets the value of the masterBLNotifyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterBLNotifyType() {
        return masterBLNotifyType;
    }

    /**
     * Sets the value of the masterBLNotifyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterBLNotifyType(String value) {
        this.masterBLNotifyType = value;
    }

    /**
     * Gets the value of the contactRef property.
     * 
     * @return
     *     possible object is
     *     {@link ContactRefType }
     *     
     */
    public ContactRefType getContactRef() {
        return contactRef;
    }

    /**
     * Sets the value of the contactRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRefType }
     *     
     */
    public void setContactRef(ContactRefType value) {
        this.contactRef = value;
    }

    /**
     * Gets the value of the comMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Sets the value of the comMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

}

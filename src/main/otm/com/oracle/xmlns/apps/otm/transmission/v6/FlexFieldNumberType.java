
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Common type for tables which contain Flex Field columns for NUMBER data type.
 *             Note: Some tables may not have all the available columns. These types just 
 *             define the maximum number of allowed elements.
 *          
 * 
 * <p>Java class for FlexFieldNumberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexFieldNumberType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AttributeNumber1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AttributeNumber20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexFieldNumberType", propOrder = {
    "attributeNumber1",
    "attributeNumber2",
    "attributeNumber3",
    "attributeNumber4",
    "attributeNumber5",
    "attributeNumber6",
    "attributeNumber7",
    "attributeNumber8",
    "attributeNumber9",
    "attributeNumber10",
    "attributeNumber11",
    "attributeNumber12",
    "attributeNumber13",
    "attributeNumber14",
    "attributeNumber15",
    "attributeNumber16",
    "attributeNumber17",
    "attributeNumber18",
    "attributeNumber19",
    "attributeNumber20"
})
public class FlexFieldNumberType {

    @XmlElement(name = "AttributeNumber1")
    protected String attributeNumber1;
    @XmlElement(name = "AttributeNumber2")
    protected String attributeNumber2;
    @XmlElement(name = "AttributeNumber3")
    protected String attributeNumber3;
    @XmlElement(name = "AttributeNumber4")
    protected String attributeNumber4;
    @XmlElement(name = "AttributeNumber5")
    protected String attributeNumber5;
    @XmlElement(name = "AttributeNumber6")
    protected String attributeNumber6;
    @XmlElement(name = "AttributeNumber7")
    protected String attributeNumber7;
    @XmlElement(name = "AttributeNumber8")
    protected String attributeNumber8;
    @XmlElement(name = "AttributeNumber9")
    protected String attributeNumber9;
    @XmlElement(name = "AttributeNumber10")
    protected String attributeNumber10;
    @XmlElement(name = "AttributeNumber11")
    protected String attributeNumber11;
    @XmlElement(name = "AttributeNumber12")
    protected String attributeNumber12;
    @XmlElement(name = "AttributeNumber13")
    protected String attributeNumber13;
    @XmlElement(name = "AttributeNumber14")
    protected String attributeNumber14;
    @XmlElement(name = "AttributeNumber15")
    protected String attributeNumber15;
    @XmlElement(name = "AttributeNumber16")
    protected String attributeNumber16;
    @XmlElement(name = "AttributeNumber17")
    protected String attributeNumber17;
    @XmlElement(name = "AttributeNumber18")
    protected String attributeNumber18;
    @XmlElement(name = "AttributeNumber19")
    protected String attributeNumber19;
    @XmlElement(name = "AttributeNumber20")
    protected String attributeNumber20;

    /**
     * Gets the value of the attributeNumber1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber1() {
        return attributeNumber1;
    }

    /**
     * Sets the value of the attributeNumber1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber1(String value) {
        this.attributeNumber1 = value;
    }

    /**
     * Gets the value of the attributeNumber2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber2() {
        return attributeNumber2;
    }

    /**
     * Sets the value of the attributeNumber2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber2(String value) {
        this.attributeNumber2 = value;
    }

    /**
     * Gets the value of the attributeNumber3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber3() {
        return attributeNumber3;
    }

    /**
     * Sets the value of the attributeNumber3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber3(String value) {
        this.attributeNumber3 = value;
    }

    /**
     * Gets the value of the attributeNumber4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber4() {
        return attributeNumber4;
    }

    /**
     * Sets the value of the attributeNumber4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber4(String value) {
        this.attributeNumber4 = value;
    }

    /**
     * Gets the value of the attributeNumber5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber5() {
        return attributeNumber5;
    }

    /**
     * Sets the value of the attributeNumber5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber5(String value) {
        this.attributeNumber5 = value;
    }

    /**
     * Gets the value of the attributeNumber6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber6() {
        return attributeNumber6;
    }

    /**
     * Sets the value of the attributeNumber6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber6(String value) {
        this.attributeNumber6 = value;
    }

    /**
     * Gets the value of the attributeNumber7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber7() {
        return attributeNumber7;
    }

    /**
     * Sets the value of the attributeNumber7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber7(String value) {
        this.attributeNumber7 = value;
    }

    /**
     * Gets the value of the attributeNumber8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber8() {
        return attributeNumber8;
    }

    /**
     * Sets the value of the attributeNumber8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber8(String value) {
        this.attributeNumber8 = value;
    }

    /**
     * Gets the value of the attributeNumber9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber9() {
        return attributeNumber9;
    }

    /**
     * Sets the value of the attributeNumber9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber9(String value) {
        this.attributeNumber9 = value;
    }

    /**
     * Gets the value of the attributeNumber10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber10() {
        return attributeNumber10;
    }

    /**
     * Sets the value of the attributeNumber10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber10(String value) {
        this.attributeNumber10 = value;
    }

    /**
     * Gets the value of the attributeNumber11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber11() {
        return attributeNumber11;
    }

    /**
     * Sets the value of the attributeNumber11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber11(String value) {
        this.attributeNumber11 = value;
    }

    /**
     * Gets the value of the attributeNumber12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber12() {
        return attributeNumber12;
    }

    /**
     * Sets the value of the attributeNumber12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber12(String value) {
        this.attributeNumber12 = value;
    }

    /**
     * Gets the value of the attributeNumber13 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber13() {
        return attributeNumber13;
    }

    /**
     * Sets the value of the attributeNumber13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber13(String value) {
        this.attributeNumber13 = value;
    }

    /**
     * Gets the value of the attributeNumber14 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber14() {
        return attributeNumber14;
    }

    /**
     * Sets the value of the attributeNumber14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber14(String value) {
        this.attributeNumber14 = value;
    }

    /**
     * Gets the value of the attributeNumber15 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber15() {
        return attributeNumber15;
    }

    /**
     * Sets the value of the attributeNumber15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber15(String value) {
        this.attributeNumber15 = value;
    }

    /**
     * Gets the value of the attributeNumber16 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber16() {
        return attributeNumber16;
    }

    /**
     * Sets the value of the attributeNumber16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber16(String value) {
        this.attributeNumber16 = value;
    }

    /**
     * Gets the value of the attributeNumber17 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber17() {
        return attributeNumber17;
    }

    /**
     * Sets the value of the attributeNumber17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber17(String value) {
        this.attributeNumber17 = value;
    }

    /**
     * Gets the value of the attributeNumber18 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber18() {
        return attributeNumber18;
    }

    /**
     * Sets the value of the attributeNumber18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber18(String value) {
        this.attributeNumber18 = value;
    }

    /**
     * Gets the value of the attributeNumber19 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber19() {
        return attributeNumber19;
    }

    /**
     * Sets the value of the attributeNumber19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber19(String value) {
        this.attributeNumber19 = value;
    }

    /**
     * Gets the value of the attributeNumber20 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeNumber20() {
        return attributeNumber20;
    }

    /**
     * Sets the value of the attributeNumber20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeNumber20(String value) {
        this.attributeNumber20 = value;
    }

}

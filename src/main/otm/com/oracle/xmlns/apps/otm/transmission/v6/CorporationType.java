
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Corporation element supports both inbound and outbound integration. Corporation interface is supported such that Corporation can be sent in and out as
 *             an independent transaction.CorporationGid can be left blank, if corporation is being sent as part of Location element. Otherwise, if Corporation is being sent as a
 *             separate transaction, CorproationGid is mandatory.
 *          
 * 
 * <p>Java class for CorporationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CorporationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="CorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="CorporationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="VatRegistration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VatRegistrationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VatProvincialRegistration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VatProvincialRegistrationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsDomainMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsShippingAgentsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAllowHouseCollect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxHouseCollectAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorporationType", propOrder = {
    "intSavedQuery",
    "corporationGid",
    "transactionCode",
    "replaceChildren",
    "corporationName",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "vatRegistration",
    "vatProvincialRegistration",
    "isDomainMaster",
    "isShippingAgentsActive",
    "isAllowHouseCollect",
    "maxHouseCollectAmount",
    "involvedParty"
})
public class CorporationType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "CorporationGid")
    protected GLogXMLGidType corporationGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "CorporationName")
    protected String corporationName;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "VatRegistration")
    protected List<VatRegistrationType> vatRegistration;
    @XmlElement(name = "VatProvincialRegistration")
    protected List<VatProvincialRegistrationType> vatProvincialRegistration;
    @XmlElement(name = "IsDomainMaster")
    protected String isDomainMaster;
    @XmlElement(name = "IsShippingAgentsActive")
    protected String isShippingAgentsActive;
    @XmlElement(name = "IsAllowHouseCollect")
    protected String isAllowHouseCollect;
    @XmlElement(name = "MaxHouseCollectAmount")
    protected GLogXMLFinancialAmountType maxHouseCollectAmount;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the corporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCorporationGid() {
        return corporationGid;
    }

    /**
     * Sets the value of the corporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCorporationGid(GLogXMLGidType value) {
        this.corporationGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the corporationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorporationName() {
        return corporationName;
    }

    /**
     * Sets the value of the corporationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorporationName(String value) {
        this.corporationName = value;
    }

    /**
     * Gets the value of the pickupRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Sets the value of the pickupRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Gets the value of the dropoffRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Sets the value of the dropoffRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Gets the value of the vatRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vatRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVatRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VatRegistrationType }
     * 
     * 
     */
    public List<VatRegistrationType> getVatRegistration() {
        if (vatRegistration == null) {
            vatRegistration = new ArrayList<VatRegistrationType>();
        }
        return this.vatRegistration;
    }

    /**
     * Gets the value of the vatProvincialRegistration property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vatProvincialRegistration property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVatProvincialRegistration().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VatProvincialRegistrationType }
     * 
     * 
     */
    public List<VatProvincialRegistrationType> getVatProvincialRegistration() {
        if (vatProvincialRegistration == null) {
            vatProvincialRegistration = new ArrayList<VatProvincialRegistrationType>();
        }
        return this.vatProvincialRegistration;
    }

    /**
     * Gets the value of the isDomainMaster property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDomainMaster() {
        return isDomainMaster;
    }

    /**
     * Sets the value of the isDomainMaster property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDomainMaster(String value) {
        this.isDomainMaster = value;
    }

    /**
     * Gets the value of the isShippingAgentsActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShippingAgentsActive() {
        return isShippingAgentsActive;
    }

    /**
     * Sets the value of the isShippingAgentsActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShippingAgentsActive(String value) {
        this.isShippingAgentsActive = value;
    }

    /**
     * Gets the value of the isAllowHouseCollect property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowHouseCollect() {
        return isAllowHouseCollect;
    }

    /**
     * Sets the value of the isAllowHouseCollect property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowHouseCollect(String value) {
        this.isAllowHouseCollect = value;
    }

    /**
     * Gets the value of the maxHouseCollectAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getMaxHouseCollectAmount() {
        return maxHouseCollectAmount;
    }

    /**
     * Sets the value of the maxHouseCollectAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setMaxHouseCollectAmount(GLogXMLFinancialAmountType value) {
        this.maxHouseCollectAmount = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

}

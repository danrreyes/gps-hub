
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * GenericStopOff is used to convey stopoff information pertaining to motor or rail carriers
 * 
 * <p>Java class for StopOffType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StopOffType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="StopReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StopSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopSealType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PickupDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="StopRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/&gt;
 *           &lt;element name="LocationRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefnumType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StopOffType", propOrder = {
    "stopSequence",
    "stopReasonGid",
    "equipmentPrefix",
    "equipmentNumber",
    "stopSeal",
    "pickupDate",
    "deliveryDate",
    "stopRefnum",
    "locationRef",
    "locationRefnum"
})
public class StopOffType {

    @XmlElement(name = "StopSequence", required = true)
    protected String stopSequence;
    @XmlElement(name = "StopReasonGid")
    protected GLogXMLGidType stopReasonGid;
    @XmlElement(name = "EquipmentPrefix")
    protected String equipmentPrefix;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "StopSeal")
    protected List<StopSealType> stopSeal;
    @XmlElement(name = "PickupDate")
    protected GLogDateTimeType pickupDate;
    @XmlElement(name = "DeliveryDate")
    protected GLogDateTimeType deliveryDate;
    @XmlElement(name = "StopRefnum")
    protected List<StopRefnumType> stopRefnum;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "LocationRefnum")
    protected LocationRefnumType locationRefnum;

    /**
     * Gets the value of the stopSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Sets the value of the stopSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Gets the value of the stopReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStopReasonGid() {
        return stopReasonGid;
    }

    /**
     * Sets the value of the stopReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStopReasonGid(GLogXMLGidType value) {
        this.stopReasonGid = value;
    }

    /**
     * Gets the value of the equipmentPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentPrefix() {
        return equipmentPrefix;
    }

    /**
     * Sets the value of the equipmentPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentPrefix(String value) {
        this.equipmentPrefix = value;
    }

    /**
     * Gets the value of the equipmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Sets the value of the equipmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the stopSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the stopSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopSealType }
     * 
     * 
     */
    public List<StopSealType> getStopSeal() {
        if (stopSeal == null) {
            stopSeal = new ArrayList<StopSealType>();
        }
        return this.stopSeal;
    }

    /**
     * Gets the value of the pickupDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPickupDate() {
        return pickupDate;
    }

    /**
     * Sets the value of the pickupDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPickupDate(GLogDateTimeType value) {
        this.pickupDate = value;
    }

    /**
     * Gets the value of the deliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Sets the value of the deliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryDate(GLogDateTimeType value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the stopRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the stopRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopRefnumType }
     * 
     * 
     */
    public List<StopRefnumType> getStopRefnum() {
        if (stopRefnum == null) {
            stopRefnum = new ArrayList<StopRefnumType>();
        }
        return this.stopRefnum;
    }

    /**
     * Gets the value of the locationRef property.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Sets the value of the locationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Gets the value of the locationRefnum property.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefnumType }
     *     
     */
    public LocationRefnumType getLocationRefnum() {
        return locationRefnum;
    }

    /**
     * Sets the value of the locationRefnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefnumType }
     *     
     */
    public void setLocationRefnum(LocationRefnumType value) {
        this.locationRefnum = value;
    }

}

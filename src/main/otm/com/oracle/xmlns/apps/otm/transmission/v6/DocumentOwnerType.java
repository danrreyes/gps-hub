
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the owner of the document.
 *             A document may be owned by a GC3 business object such as an order or a shipment.
 *          
 * 
 * <p>Java class for DocumentOwnerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentOwnerType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DataQueryTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentOwnerType", propOrder = {
    "dataQueryTypeGid",
    "objectGid"
})
public class DocumentOwnerType {

    @XmlElement(name = "DataQueryTypeGid", required = true)
    protected GLogXMLGidType dataQueryTypeGid;
    @XmlElement(name = "ObjectGid", required = true)
    protected GLogXMLGidType objectGid;

    /**
     * Gets the value of the dataQueryTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueryTypeGid() {
        return dataQueryTypeGid;
    }

    /**
     * Sets the value of the dataQueryTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueryTypeGid(GLogXMLGidType value) {
        this.dataQueryTypeGid = value;
    }

    /**
     * Gets the value of the objectGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getObjectGid() {
        return objectGid;
    }

    /**
     * Sets the value of the objectGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setObjectGid(GLogXMLGidType value) {
        this.objectGid = value;
    }

}

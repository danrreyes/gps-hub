
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * TimeWindow is a group of fields for specifying early/late pickup, early/late delivery, and pickup/delivery appointments.
 *             The PickupIsAppt and DeliveryIsAppt elements are only applicable when the TimeWindow is in the TransOrderLine,
 *             TransOrder.ShipUnitDetail.ShipUnit, and Release elements.
 *          
 * 
 * <p>Java class for TimeWindowType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TimeWindowType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EarlyPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LatePickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EarlyDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LateDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PickupIsAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryIsAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeWindowType", propOrder = {
    "earlyPickupDt",
    "latePickupDt",
    "earlyDeliveryDt",
    "lateDeliveryDt",
    "pickupIsAppt",
    "deliveryIsAppt"
})
public class TimeWindowType {

    @XmlElement(name = "EarlyPickupDt")
    protected GLogDateTimeType earlyPickupDt;
    @XmlElement(name = "LatePickupDt")
    protected GLogDateTimeType latePickupDt;
    @XmlElement(name = "EarlyDeliveryDt")
    protected GLogDateTimeType earlyDeliveryDt;
    @XmlElement(name = "LateDeliveryDt")
    protected GLogDateTimeType lateDeliveryDt;
    @XmlElement(name = "PickupIsAppt")
    protected String pickupIsAppt;
    @XmlElement(name = "DeliveryIsAppt")
    protected String deliveryIsAppt;

    /**
     * Gets the value of the earlyPickupDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyPickupDt() {
        return earlyPickupDt;
    }

    /**
     * Sets the value of the earlyPickupDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyPickupDt(GLogDateTimeType value) {
        this.earlyPickupDt = value;
    }

    /**
     * Gets the value of the latePickupDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLatePickupDt() {
        return latePickupDt;
    }

    /**
     * Sets the value of the latePickupDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLatePickupDt(GLogDateTimeType value) {
        this.latePickupDt = value;
    }

    /**
     * Gets the value of the earlyDeliveryDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyDeliveryDt() {
        return earlyDeliveryDt;
    }

    /**
     * Sets the value of the earlyDeliveryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyDeliveryDt(GLogDateTimeType value) {
        this.earlyDeliveryDt = value;
    }

    /**
     * Gets the value of the lateDeliveryDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLateDeliveryDt() {
        return lateDeliveryDt;
    }

    /**
     * Sets the value of the lateDeliveryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLateDeliveryDt(GLogDateTimeType value) {
        this.lateDeliveryDt = value;
    }

    /**
     * Gets the value of the pickupIsAppt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupIsAppt() {
        return pickupIsAppt;
    }

    /**
     * Sets the value of the pickupIsAppt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupIsAppt(String value) {
        this.pickupIsAppt = value;
    }

    /**
     * Gets the value of the deliveryIsAppt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryIsAppt() {
        return deliveryIsAppt;
    }

    /**
     * Sets the value of the deliveryIsAppt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryIsAppt(String value) {
        this.deliveryIsAppt = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Temperature is a structure for specifying temperature in a particular unit of measure.
 * 
 * <p>Java class for TemperatureType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TemperatureType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TemperatureValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TemperatureUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TemperatureType", propOrder = {
    "temperatureValue",
    "temperatureUOMGid"
})
public class TemperatureType {

    @XmlElement(name = "TemperatureValue", required = true)
    protected String temperatureValue;
    @XmlElement(name = "TemperatureUOMGid", required = true)
    protected GLogXMLGidType temperatureUOMGid;

    /**
     * Gets the value of the temperatureValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemperatureValue() {
        return temperatureValue;
    }

    /**
     * Sets the value of the temperatureValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemperatureValue(String value) {
        this.temperatureValue = value;
    }

    /**
     * Gets the value of the temperatureUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTemperatureUOMGid() {
        return temperatureUOMGid;
    }

    /**
     * Sets the value of the temperatureUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTemperatureUOMGid(GLogXMLGidType value) {
        this.temperatureUOMGid = value;
    }

}

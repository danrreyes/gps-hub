
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipUnitLineRefnum is an alternate method for identifying an ShipUnitLine (ShipUnitContent) in a ShipUnit.
 *             It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for ShipUnitLineRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitLineRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipUnitLineRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipUnitLineRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitLineRefnumType", propOrder = {
    "shipUnitLineRefnumQualifierGid",
    "shipUnitLineRefnumValue"
})
public class ShipUnitLineRefnumType {

    @XmlElement(name = "ShipUnitLineRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipUnitLineRefnumQualifierGid;
    @XmlElement(name = "ShipUnitLineRefnumValue", required = true)
    protected String shipUnitLineRefnumValue;

    /**
     * Gets the value of the shipUnitLineRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitLineRefnumQualifierGid() {
        return shipUnitLineRefnumQualifierGid;
    }

    /**
     * Sets the value of the shipUnitLineRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitLineRefnumQualifierGid(GLogXMLGidType value) {
        this.shipUnitLineRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the shipUnitLineRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitLineRefnumValue() {
        return shipUnitLineRefnumValue;
    }

    /**
     * Sets the value of the shipUnitLineRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitLineRefnumValue(String value) {
        this.shipUnitLineRefnumValue = value;
    }

}

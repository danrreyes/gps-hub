
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Provides a ReleaseLine centric view of the ShipUnit(s) on the Shipment.
 * 
 * <p>Java class for ShipUnitViewByReleaseLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitViewByReleaseLineType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipUnitView" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitViewType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitViewByReleaseLineType", propOrder = {
    "releaseLineGid",
    "shipUnitView"
})
public class ShipUnitViewByReleaseLineType {

    @XmlElement(name = "ReleaseLineGid", required = true)
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "ShipUnitView", required = true)
    protected ShipUnitViewType shipUnitView;

    /**
     * Gets the value of the releaseLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Sets the value of the releaseLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Gets the value of the shipUnitView property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitViewType }
     *     
     */
    public ShipUnitViewType getShipUnitView() {
        return shipUnitView;
    }

    /**
     * Sets the value of the shipUnitView property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitViewType }
     *     
     */
    public void setShipUnitView(ShipUnitViewType value) {
        this.shipUnitView = value;
    }

}

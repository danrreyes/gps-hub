
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogDateTimeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogDateTimeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GLogDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TZId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TZOffset" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogDateTimeType", propOrder = {
    "gLogDate",
    "tzId",
    "tzOffset"
})
public class GLogDateTimeType {

    @XmlElement(name = "GLogDate")
    protected String gLogDate;
    @XmlElement(name = "TZId")
    protected String tzId;
    @XmlElement(name = "TZOffset")
    protected String tzOffset;

    /**
     * Gets the value of the gLogDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLogDate() {
        return gLogDate;
    }

    /**
     * Sets the value of the gLogDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLogDate(String value) {
        this.gLogDate = value;
    }

    /**
     * Gets the value of the tzId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTZId() {
        return tzId;
    }

    /**
     * Sets the value of the tzId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTZId(String value) {
        this.tzId = value;
    }

    /**
     * Gets the value of the tzOffset property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTZOffset() {
        return tzOffset;
    }

    /**
     * Sets the value of the tzOffset property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTZOffset(String value) {
        this.tzOffset = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * RegionRef is a reference to a region - either a region gid or a region definition.
 *             It may be used in the context of a Location to specify a region for the location.
 *             It may also be used in the context of an XLaneNode to reference an existing region, or define a new region on-the-fly.
 *          
 * 
 * <p>Java class for RegionRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegionRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RegionHeader"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                   &lt;element name="RegionDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RegionComponent" maxOccurs="unbounded"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/&gt;
 *                             &lt;element name="PostalCodeRange" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PostalCodeRangeType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegionRefType", propOrder = {
    "regionGid",
    "regionHeader"
})
public class RegionRefType {

    @XmlElement(name = "RegionGid")
    protected GLogXMLGidType regionGid;
    @XmlElement(name = "RegionHeader")
    protected RegionRefType.RegionHeader regionHeader;

    /**
     * Gets the value of the regionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionGid() {
        return regionGid;
    }

    /**
     * Sets the value of the regionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionGid(GLogXMLGidType value) {
        this.regionGid = value;
    }

    /**
     * Gets the value of the regionHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RegionRefType.RegionHeader }
     *     
     */
    public RegionRefType.RegionHeader getRegionHeader() {
        return regionHeader;
    }

    /**
     * Sets the value of the regionHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionRefType.RegionHeader }
     *     
     */
    public void setRegionHeader(RegionRefType.RegionHeader value) {
        this.regionHeader = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="RegionDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RegionComponent" maxOccurs="unbounded"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/&gt;
     *                   &lt;element name="PostalCodeRange" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PostalCodeRangeType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "regionGid",
        "regionDesc",
        "regionComponent"
    })
    public static class RegionHeader {

        @XmlElement(name = "RegionGid", required = true)
        protected GLogXMLGidType regionGid;
        @XmlElement(name = "RegionDesc")
        protected String regionDesc;
        @XmlElement(name = "RegionComponent", required = true)
        protected List<RegionRefType.RegionHeader.RegionComponent> regionComponent;

        /**
         * Gets the value of the regionGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getRegionGid() {
            return regionGid;
        }

        /**
         * Sets the value of the regionGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setRegionGid(GLogXMLGidType value) {
            this.regionGid = value;
        }

        /**
         * Gets the value of the regionDesc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegionDesc() {
            return regionDesc;
        }

        /**
         * Sets the value of the regionDesc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegionDesc(String value) {
            this.regionDesc = value;
        }

        /**
         * Gets the value of the regionComponent property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the regionComponent property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRegionComponent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RegionRefType.RegionHeader.RegionComponent }
         * 
         * 
         */
        public List<RegionRefType.RegionHeader.RegionComponent> getRegionComponent() {
            if (regionComponent == null) {
                regionComponent = new ArrayList<RegionRefType.RegionHeader.RegionComponent>();
            }
            return this.regionComponent;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/&gt;
         *         &lt;element name="PostalCodeRange" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PostalCodeRangeType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mileageAddress",
            "postalCodeRange"
        })
        public static class RegionComponent {

            @XmlElement(name = "MileageAddress", required = true)
            protected MileageAddressType mileageAddress;
            @XmlElement(name = "PostalCodeRange")
            protected PostalCodeRangeType postalCodeRange;

            /**
             * Gets the value of the mileageAddress property.
             * 
             * @return
             *     possible object is
             *     {@link MileageAddressType }
             *     
             */
            public MileageAddressType getMileageAddress() {
                return mileageAddress;
            }

            /**
             * Sets the value of the mileageAddress property.
             * 
             * @param value
             *     allowed object is
             *     {@link MileageAddressType }
             *     
             */
            public void setMileageAddress(MileageAddressType value) {
                this.mileageAddress = value;
            }

            /**
             * Gets the value of the postalCodeRange property.
             * 
             * @return
             *     possible object is
             *     {@link PostalCodeRangeType }
             *     
             */
            public PostalCodeRangeType getPostalCodeRange() {
                return postalCodeRange;
            }

            /**
             * Sets the value of the postalCodeRange property.
             * 
             * @param value
             *     allowed object is
             *     {@link PostalCodeRangeType }
             *     
             */
            public void setPostalCodeRange(PostalCodeRangeType value) {
                this.postalCodeRange = value;
            }

        }

    }

}

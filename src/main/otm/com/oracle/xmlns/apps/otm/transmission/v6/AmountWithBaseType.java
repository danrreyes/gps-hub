
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AmountWithBaseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AmountWithBaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FinancialAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialAmountType"/&gt;
 *         &lt;element name="AmountBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AmountWithBaseType", propOrder = {
    "financialAmount",
    "amountBase"
})
public class AmountWithBaseType {

    @XmlElement(name = "FinancialAmount", required = true)
    protected FinancialAmountType financialAmount;
    @XmlElement(name = "AmountBase")
    protected String amountBase;

    /**
     * Gets the value of the financialAmount property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAmountType }
     *     
     */
    public FinancialAmountType getFinancialAmount() {
        return financialAmount;
    }

    /**
     * Sets the value of the financialAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAmountType }
     *     
     */
    public void setFinancialAmount(FinancialAmountType value) {
        this.financialAmount = value;
    }

    /**
     * Gets the value of the amountBase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmountBase() {
        return amountBase;
    }

    /**
     * Sets the value of the amountBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmountBase(String value) {
        this.amountBase = value;
    }

}

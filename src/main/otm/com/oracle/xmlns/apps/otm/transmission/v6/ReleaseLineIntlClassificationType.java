
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the international classification for the order release line.
 *          
 * 
 * <p>Java class for ReleaseLineIntlClassificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseLineIntlClassificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImportExportType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HTSGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryRQ" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/&gt;
 *         &lt;element name="SecondaryRQ" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/&gt;
 *         &lt;element name="TertiaryRQ" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseLineIntlClassificationType", propOrder = {
    "sequenceNumber",
    "importExportType",
    "htsGid",
    "primaryRQ",
    "secondaryRQ",
    "tertiaryRQ"
})
public class ReleaseLineIntlClassificationType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "ImportExportType")
    protected String importExportType;
    @XmlElement(name = "HTSGid")
    protected GLogXMLGidType htsGid;
    @XmlElement(name = "PrimaryRQ")
    protected GLogXMLFlexQuantityType primaryRQ;
    @XmlElement(name = "SecondaryRQ")
    protected GLogXMLFlexQuantityType secondaryRQ;
    @XmlElement(name = "TertiaryRQ")
    protected GLogXMLFlexQuantityType tertiaryRQ;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the importExportType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImportExportType() {
        return importExportType;
    }

    /**
     * Sets the value of the importExportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImportExportType(String value) {
        this.importExportType = value;
    }

    /**
     * Gets the value of the htsGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHTSGid() {
        return htsGid;
    }

    /**
     * Sets the value of the htsGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHTSGid(GLogXMLGidType value) {
        this.htsGid = value;
    }

    /**
     * Gets the value of the primaryRQ property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getPrimaryRQ() {
        return primaryRQ;
    }

    /**
     * Sets the value of the primaryRQ property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setPrimaryRQ(GLogXMLFlexQuantityType value) {
        this.primaryRQ = value;
    }

    /**
     * Gets the value of the secondaryRQ property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getSecondaryRQ() {
        return secondaryRQ;
    }

    /**
     * Sets the value of the secondaryRQ property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setSecondaryRQ(GLogXMLFlexQuantityType value) {
        this.secondaryRQ = value;
    }

    /**
     * Gets the value of the tertiaryRQ property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getTertiaryRQ() {
        return tertiaryRQ;
    }

    /**
     * Sets the value of the tertiaryRQ property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setTertiaryRQ(GLogXMLFlexQuantityType value) {
        this.tertiaryRQ = value;
    }

}

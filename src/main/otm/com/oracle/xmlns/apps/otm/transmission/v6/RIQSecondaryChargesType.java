
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * All secondary charge shipments of RIQ routing option.
 * 
 * <p>Java class for RIQSecondaryChargesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RIQSecondaryChargesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RIQRouteShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQRouteShipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQSecondaryChargesType", propOrder = {
    "riqRouteShipment"
})
public class RIQSecondaryChargesType {

    @XmlElement(name = "RIQRouteShipment")
    protected List<RIQRouteShipmentType> riqRouteShipment;

    /**
     * Gets the value of the riqRouteShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the riqRouteShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQRouteShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQRouteShipmentType }
     * 
     * 
     */
    public List<RIQRouteShipmentType> getRIQRouteShipment() {
        if (riqRouteShipment == null) {
            riqRouteShipment = new ArrayList<RIQRouteShipmentType>();
        }
        return this.riqRouteShipment;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the association between the SEquipment and the Shipment ShipUnit.
 * 
 * <p>Java class for SEquipmentSShipUnitInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SEquipmentSShipUnitInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="SEquipmentGidQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentGidQueryType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="CompartmentNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoadingSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoadingPatternGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="NumStackingLayers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumLoadingRows" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SEquipmentSShipUnitInfoType", propOrder = {
    "sEquipmentGid",
    "sEquipmentGidQuery",
    "compartmentNum",
    "loadingSequence",
    "loadingPatternGid",
    "numStackingLayers",
    "numLoadingRows"
})
public class SEquipmentSShipUnitInfoType {

    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "SEquipmentGidQuery")
    protected SEquipmentGidQueryType sEquipmentGidQuery;
    @XmlElement(name = "CompartmentNum")
    protected String compartmentNum;
    @XmlElement(name = "LoadingSequence")
    protected String loadingSequence;
    @XmlElement(name = "LoadingPatternGid")
    protected GLogXMLGidType loadingPatternGid;
    @XmlElement(name = "NumStackingLayers")
    protected String numStackingLayers;
    @XmlElement(name = "NumLoadingRows")
    protected String numLoadingRows;

    /**
     * Gets the value of the sEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Sets the value of the sEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the sEquipmentGidQuery property.
     * 
     * @return
     *     possible object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public SEquipmentGidQueryType getSEquipmentGidQuery() {
        return sEquipmentGidQuery;
    }

    /**
     * Sets the value of the sEquipmentGidQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public void setSEquipmentGidQuery(SEquipmentGidQueryType value) {
        this.sEquipmentGidQuery = value;
    }

    /**
     * Gets the value of the compartmentNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompartmentNum() {
        return compartmentNum;
    }

    /**
     * Sets the value of the compartmentNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompartmentNum(String value) {
        this.compartmentNum = value;
    }

    /**
     * Gets the value of the loadingSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadingSequence() {
        return loadingSequence;
    }

    /**
     * Sets the value of the loadingSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadingSequence(String value) {
        this.loadingSequence = value;
    }

    /**
     * Gets the value of the loadingPatternGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadingPatternGid() {
        return loadingPatternGid;
    }

    /**
     * Sets the value of the loadingPatternGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadingPatternGid(GLogXMLGidType value) {
        this.loadingPatternGid = value;
    }

    /**
     * Gets the value of the numStackingLayers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumStackingLayers() {
        return numStackingLayers;
    }

    /**
     * Sets the value of the numStackingLayers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumStackingLayers(String value) {
        this.numStackingLayers = value;
    }

    /**
     * Gets the value of the numLoadingRows property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumLoadingRows() {
        return numLoadingRows;
    }

    /**
     * Sets the value of the numLoadingRows property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumLoadingRows(String value) {
        this.numLoadingRows = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the conditional booking information for the Shipment.
 * 
 * <p>Java class for BookingInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookingInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReceiptLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="SailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="RailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="SailCutoffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfDischrgETA" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfExitDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfExitLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ContainerDeliveryLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ContainerPickupLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="CfsCutoffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="CfsCutoffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="SiCutoffDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookingInfoType", propOrder = {
    "receiptLocation",
    "deliveryLocation",
    "sailDt",
    "railDt",
    "sailCutoffDt",
    "portOfDischrgETA",
    "portOfExitDt",
    "portOfExitLocation",
    "containerDeliveryLocation",
    "containerPickupLocation",
    "cfsCutoffLocation",
    "cfsCutoffDt",
    "siCutoffDt"
})
public class BookingInfoType {

    @XmlElement(name = "ReceiptLocation")
    protected GLogXMLLocRefType receiptLocation;
    @XmlElement(name = "DeliveryLocation")
    protected GLogXMLLocRefType deliveryLocation;
    @XmlElement(name = "SailDt")
    protected GLogDateTimeType sailDt;
    @XmlElement(name = "RailDt")
    protected GLogDateTimeType railDt;
    @XmlElement(name = "SailCutoffDt")
    protected GLogDateTimeType sailCutoffDt;
    @XmlElement(name = "PortOfDischrgETA")
    protected GLogDateTimeType portOfDischrgETA;
    @XmlElement(name = "PortOfExitDt")
    protected GLogDateTimeType portOfExitDt;
    @XmlElement(name = "PortOfExitLocation")
    protected GLogXMLLocRefType portOfExitLocation;
    @XmlElement(name = "ContainerDeliveryLocation")
    protected GLogXMLLocRefType containerDeliveryLocation;
    @XmlElement(name = "ContainerPickupLocation")
    protected GLogXMLLocRefType containerPickupLocation;
    @XmlElement(name = "CfsCutoffLocation")
    protected GLogXMLLocRefType cfsCutoffLocation;
    @XmlElement(name = "CfsCutoffDt")
    protected GLogDateTimeType cfsCutoffDt;
    @XmlElement(name = "SiCutoffDt")
    protected GLogDateTimeType siCutoffDt;

    /**
     * Gets the value of the receiptLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getReceiptLocation() {
        return receiptLocation;
    }

    /**
     * Sets the value of the receiptLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setReceiptLocation(GLogXMLLocRefType value) {
        this.receiptLocation = value;
    }

    /**
     * Gets the value of the deliveryLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDeliveryLocation() {
        return deliveryLocation;
    }

    /**
     * Sets the value of the deliveryLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDeliveryLocation(GLogXMLLocRefType value) {
        this.deliveryLocation = value;
    }

    /**
     * Gets the value of the sailDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSailDt() {
        return sailDt;
    }

    /**
     * Sets the value of the sailDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSailDt(GLogDateTimeType value) {
        this.sailDt = value;
    }

    /**
     * Gets the value of the railDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRailDt() {
        return railDt;
    }

    /**
     * Sets the value of the railDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRailDt(GLogDateTimeType value) {
        this.railDt = value;
    }

    /**
     * Gets the value of the sailCutoffDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSailCutoffDt() {
        return sailCutoffDt;
    }

    /**
     * Sets the value of the sailCutoffDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSailCutoffDt(GLogDateTimeType value) {
        this.sailCutoffDt = value;
    }

    /**
     * Gets the value of the portOfDischrgETA property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPortOfDischrgETA() {
        return portOfDischrgETA;
    }

    /**
     * Sets the value of the portOfDischrgETA property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPortOfDischrgETA(GLogDateTimeType value) {
        this.portOfDischrgETA = value;
    }

    /**
     * Gets the value of the portOfExitDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPortOfExitDt() {
        return portOfExitDt;
    }

    /**
     * Sets the value of the portOfExitDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPortOfExitDt(GLogDateTimeType value) {
        this.portOfExitDt = value;
    }

    /**
     * Gets the value of the portOfExitLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfExitLocation() {
        return portOfExitLocation;
    }

    /**
     * Sets the value of the portOfExitLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfExitLocation(GLogXMLLocRefType value) {
        this.portOfExitLocation = value;
    }

    /**
     * Gets the value of the containerDeliveryLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getContainerDeliveryLocation() {
        return containerDeliveryLocation;
    }

    /**
     * Sets the value of the containerDeliveryLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setContainerDeliveryLocation(GLogXMLLocRefType value) {
        this.containerDeliveryLocation = value;
    }

    /**
     * Gets the value of the containerPickupLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getContainerPickupLocation() {
        return containerPickupLocation;
    }

    /**
     * Sets the value of the containerPickupLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setContainerPickupLocation(GLogXMLLocRefType value) {
        this.containerPickupLocation = value;
    }

    /**
     * Gets the value of the cfsCutoffLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getCfsCutoffLocation() {
        return cfsCutoffLocation;
    }

    /**
     * Sets the value of the cfsCutoffLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setCfsCutoffLocation(GLogXMLLocRefType value) {
        this.cfsCutoffLocation = value;
    }

    /**
     * Gets the value of the cfsCutoffDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getCfsCutoffDt() {
        return cfsCutoffDt;
    }

    /**
     * Sets the value of the cfsCutoffDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setCfsCutoffDt(GLogDateTimeType value) {
        this.cfsCutoffDt = value;
    }

    /**
     * Gets the value of the siCutoffDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSiCutoffDt() {
        return siCutoffDt;
    }

    /**
     * Sets the value of the siCutoffDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSiCutoffDt(GLogDateTimeType value) {
        this.siCutoffDt = value;
    }

}

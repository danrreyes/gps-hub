
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies time window preferences for appointment on a location for a service provider.
 * 
 * <p>Java class for LocationServProvPrefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationServProvPrefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LocationServProvPrefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceLocationGid" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="MaxSlotsPerDay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationServProvPrefDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationServProvPrefDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationServProvPrefType", propOrder = {
    "locationServProvPrefGid",
    "locationGid",
    "serviceLocationGid",
    "serviceProviderGid",
    "maxSlotsPerDay",
    "locationServProvPrefDetail"
})
public class LocationServProvPrefType {

    @XmlElement(name = "LocationServProvPrefGid", required = true)
    protected GLogXMLGidType locationServProvPrefGid;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "ServiceLocationGid")
    protected LocationServProvPrefType.ServiceLocationGid serviceLocationGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "MaxSlotsPerDay")
    protected String maxSlotsPerDay;
    @XmlElement(name = "LocationServProvPrefDetail")
    protected List<LocationServProvPrefDetailType> locationServProvPrefDetail;

    /**
     * Gets the value of the locationServProvPrefGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationServProvPrefGid() {
        return locationServProvPrefGid;
    }

    /**
     * Sets the value of the locationServProvPrefGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationServProvPrefGid(GLogXMLGidType value) {
        this.locationServProvPrefGid = value;
    }

    /**
     * Gets the value of the locationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Sets the value of the locationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Gets the value of the serviceLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link LocationServProvPrefType.ServiceLocationGid }
     *     
     */
    public LocationServProvPrefType.ServiceLocationGid getServiceLocationGid() {
        return serviceLocationGid;
    }

    /**
     * Sets the value of the serviceLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationServProvPrefType.ServiceLocationGid }
     *     
     */
    public void setServiceLocationGid(LocationServProvPrefType.ServiceLocationGid value) {
        this.serviceLocationGid = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the maxSlotsPerDay property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxSlotsPerDay() {
        return maxSlotsPerDay;
    }

    /**
     * Sets the value of the maxSlotsPerDay property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxSlotsPerDay(String value) {
        this.maxSlotsPerDay = value;
    }

    /**
     * Gets the value of the locationServProvPrefDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationServProvPrefDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationServProvPrefDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationServProvPrefDetailType }
     * 
     * 
     */
    public List<LocationServProvPrefDetailType> getLocationServProvPrefDetail() {
        if (locationServProvPrefDetail == null) {
            locationServProvPrefDetail = new ArrayList<LocationServProvPrefDetailType>();
        }
        return this.locationServProvPrefDetail;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationGid"
    })
    public static class ServiceLocationGid {

        @XmlElement(name = "LocationGid", required = true)
        protected GLogXMLGidType locationGid;

        /**
         * Gets the value of the locationGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Sets the value of the locationGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

    }

}

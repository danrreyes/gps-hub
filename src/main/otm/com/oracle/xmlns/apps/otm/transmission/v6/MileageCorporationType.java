
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * SourceAddress is the Address of the Source.
 *             Note: The CorporationGid is only valid when the SourceAddress is in the OrderRoutingRuleQuery element.
 *          
 * 
 * <p>Java class for MileageCorporationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MileageCorporationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/&gt;
 *         &lt;element name="CorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageCorporationType", propOrder = {
    "mileageAddress",
    "corporationGid"
})
public class MileageCorporationType {

    @XmlElement(name = "MileageAddress", required = true)
    protected MileageAddressType mileageAddress;
    @XmlElement(name = "CorporationGid")
    protected GLogXMLGidType corporationGid;

    /**
     * Gets the value of the mileageAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MileageAddressType }
     *     
     */
    public MileageAddressType getMileageAddress() {
        return mileageAddress;
    }

    /**
     * Sets the value of the mileageAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageAddressType }
     *     
     */
    public void setMileageAddress(MileageAddressType value) {
        this.mileageAddress = value;
    }

    /**
     * Gets the value of the corporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCorporationGid() {
        return corporationGid;
    }

    /**
     * Sets the value of the corporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCorporationGid(GLogXMLGidType value) {
        this.corporationGid = value;
    }

}

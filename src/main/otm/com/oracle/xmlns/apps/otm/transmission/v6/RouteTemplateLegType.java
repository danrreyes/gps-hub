
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Route Template Leg represents a leg of the route for the plan for a cooperative route.
 *          
 * 
 * <p>Java class for RouteTemplateLegType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RouteTemplateLegType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="RouteTemplateLegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="RouteTemplateLegHdr" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteTemplateLegHdrType" minOccurs="0"/&gt;
 *         &lt;element name="RouteTempLegLocIncompat" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LocCompat" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/&gt;
 *                             &lt;element name="ActivityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="RouteTempLegOrderCompat" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="RouteTempLegShipCompat" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTemplateLegType", propOrder = {
    "intSavedQuery",
    "routeTemplateLegGid",
    "transactionCode",
    "routeTemplateLegHdr",
    "routeTempLegLocIncompat",
    "routeTempLegOrderCompat",
    "routeTempLegShipCompat",
    "refnum",
    "remark",
    "involvedParty"
})
public class RouteTemplateLegType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "RouteTemplateLegGid")
    protected GLogXMLGidType routeTemplateLegGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "RouteTemplateLegHdr")
    protected RouteTemplateLegHdrType routeTemplateLegHdr;
    @XmlElement(name = "RouteTempLegLocIncompat")
    protected RouteTemplateLegType.RouteTempLegLocIncompat routeTempLegLocIncompat;
    @XmlElement(name = "RouteTempLegOrderCompat")
    protected RouteTemplateLegType.RouteTempLegOrderCompat routeTempLegOrderCompat;
    @XmlElement(name = "RouteTempLegShipCompat")
    protected RouteTemplateLegType.RouteTempLegShipCompat routeTempLegShipCompat;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the routeTemplateLegGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteTemplateLegGid() {
        return routeTemplateLegGid;
    }

    /**
     * Sets the value of the routeTemplateLegGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteTemplateLegGid(GLogXMLGidType value) {
        this.routeTemplateLegGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the routeTemplateLegHdr property.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegHdrType }
     *     
     */
    public RouteTemplateLegHdrType getRouteTemplateLegHdr() {
        return routeTemplateLegHdr;
    }

    /**
     * Sets the value of the routeTemplateLegHdr property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegHdrType }
     *     
     */
    public void setRouteTemplateLegHdr(RouteTemplateLegHdrType value) {
        this.routeTemplateLegHdr = value;
    }

    /**
     * Gets the value of the routeTempLegLocIncompat property.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegType.RouteTempLegLocIncompat }
     *     
     */
    public RouteTemplateLegType.RouteTempLegLocIncompat getRouteTempLegLocIncompat() {
        return routeTempLegLocIncompat;
    }

    /**
     * Sets the value of the routeTempLegLocIncompat property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegType.RouteTempLegLocIncompat }
     *     
     */
    public void setRouteTempLegLocIncompat(RouteTemplateLegType.RouteTempLegLocIncompat value) {
        this.routeTempLegLocIncompat = value;
    }

    /**
     * Gets the value of the routeTempLegOrderCompat property.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegType.RouteTempLegOrderCompat }
     *     
     */
    public RouteTemplateLegType.RouteTempLegOrderCompat getRouteTempLegOrderCompat() {
        return routeTempLegOrderCompat;
    }

    /**
     * Sets the value of the routeTempLegOrderCompat property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegType.RouteTempLegOrderCompat }
     *     
     */
    public void setRouteTempLegOrderCompat(RouteTemplateLegType.RouteTempLegOrderCompat value) {
        this.routeTempLegOrderCompat = value;
    }

    /**
     * Gets the value of the routeTempLegShipCompat property.
     * 
     * @return
     *     possible object is
     *     {@link RouteTemplateLegType.RouteTempLegShipCompat }
     *     
     */
    public RouteTemplateLegType.RouteTempLegShipCompat getRouteTempLegShipCompat() {
        return routeTempLegShipCompat;
    }

    /**
     * Sets the value of the routeTempLegShipCompat property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTemplateLegType.RouteTempLegShipCompat }
     *     
     */
    public void setRouteTempLegShipCompat(RouteTemplateLegType.RouteTempLegShipCompat value) {
        this.routeTempLegShipCompat = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LocCompat" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/&gt;
     *                   &lt;element name="ActivityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locCompat"
    })
    public static class RouteTempLegLocIncompat {

        @XmlElement(name = "LocCompat")
        protected List<RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat> locCompat;

        /**
         * Gets the value of the locCompat property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the locCompat property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLocCompat().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat }
         * 
         * 
         */
        public List<RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat> getLocCompat() {
            if (locCompat == null) {
                locCompat = new ArrayList<RouteTemplateLegType.RouteTempLegLocIncompat.LocCompat>();
            }
            return this.locCompat;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/&gt;
         *         &lt;element name="ActivityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "locationRef",
            "activityGid"
        })
        public static class LocCompat {

            @XmlElement(name = "LocationRef", required = true)
            protected LocationRefType locationRef;
            @XmlElement(name = "ActivityGid")
            protected GLogXMLGidType activityGid;

            /**
             * Gets the value of the locationRef property.
             * 
             * @return
             *     possible object is
             *     {@link LocationRefType }
             *     
             */
            public LocationRefType getLocationRef() {
                return locationRef;
            }

            /**
             * Sets the value of the locationRef property.
             * 
             * @param value
             *     allowed object is
             *     {@link LocationRefType }
             *     
             */
            public void setLocationRef(LocationRefType value) {
                this.locationRef = value;
            }

            /**
             * Gets the value of the activityGid property.
             * 
             * @return
             *     possible object is
             *     {@link GLogXMLGidType }
             *     
             */
            public GLogXMLGidType getActivityGid() {
                return activityGid;
            }

            /**
             * Sets the value of the activityGid property.
             * 
             * @param value
             *     allowed object is
             *     {@link GLogXMLGidType }
             *     
             */
            public void setActivityGid(GLogXMLGidType value) {
                this.activityGid = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "refnum"
    })
    public static class RouteTempLegOrderCompat {

        @XmlElement(name = "Refnum")
        protected List<RefnumType> refnum;

        /**
         * Gets the value of the refnum property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the refnum property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefnum().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RefnumType }
         * 
         * 
         */
        public List<RefnumType> getRefnum() {
            if (refnum == null) {
                refnum = new ArrayList<RefnumType>();
            }
            return this.refnum;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "refnum"
    })
    public static class RouteTempLegShipCompat {

        @XmlElement(name = "Refnum")
        protected List<RefnumType> refnum;

        /**
         * Gets the value of the refnum property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the refnum property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getRefnum().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RefnumType }
         * 
         * 
         */
        public List<RefnumType> getRefnum() {
            if (refnum == null) {
                refnum = new ArrayList<RefnumType>();
            }
            return this.refnum;
        }

    }

}

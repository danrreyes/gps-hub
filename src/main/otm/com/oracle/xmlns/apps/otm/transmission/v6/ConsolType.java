
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Shipment Consolidator.
 * 
 *             Note: The CharterVoyage element is not output when the Consol is within the CharterVoyageStowage element.
 *          
 * 
 * <p>Java class for ConsolType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsolType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConsolGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="ConsolType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CharterVoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FlightInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StowageModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AllocatedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="AllocatedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AllocatedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MaxNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CommittedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="CommittedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CommittedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BookedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="BookedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BookedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProducedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="ProducedNumTEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProducedNumFEU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ConsolidationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="CharterVoyage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CharterVoyageType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsolType", propOrder = {
    "consolGid",
    "transactionCode",
    "replaceChildren",
    "consolType",
    "charterVoyageGid",
    "flightInstanceId",
    "stowageModeGid",
    "hazmatType",
    "itineraryProfileGid",
    "allocatedWeightVolume",
    "allocatedNumTEU",
    "allocatedNumFEU",
    "maxWeightVolume",
    "maxNumTEU",
    "maxNumFEU",
    "committedWeightVolume",
    "committedNumTEU",
    "committedNumFEU",
    "bookedWeightVolume",
    "bookedNumTEU",
    "bookedNumFEU",
    "producedWeightVolume",
    "producedNumTEU",
    "producedNumFEU",
    "perspective",
    "consolidationTypeGid",
    "status",
    "refnum",
    "remark",
    "shipmentGid",
    "shipment",
    "charterVoyage",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class ConsolType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ConsolGid", required = true)
    protected GLogXMLGidType consolGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "ConsolType")
    protected String consolType;
    @XmlElement(name = "CharterVoyageGid")
    protected GLogXMLGidType charterVoyageGid;
    @XmlElement(name = "FlightInstanceId")
    protected String flightInstanceId;
    @XmlElement(name = "StowageModeGid")
    protected GLogXMLGidType stowageModeGid;
    @XmlElement(name = "HazmatType")
    protected String hazmatType;
    @XmlElement(name = "ItineraryProfileGid")
    protected GLogXMLGidType itineraryProfileGid;
    @XmlElement(name = "AllocatedWeightVolume")
    protected WeightVolumeType allocatedWeightVolume;
    @XmlElement(name = "AllocatedNumTEU")
    protected String allocatedNumTEU;
    @XmlElement(name = "AllocatedNumFEU")
    protected String allocatedNumFEU;
    @XmlElement(name = "MaxWeightVolume")
    protected WeightVolumeType maxWeightVolume;
    @XmlElement(name = "MaxNumTEU")
    protected String maxNumTEU;
    @XmlElement(name = "MaxNumFEU")
    protected String maxNumFEU;
    @XmlElement(name = "CommittedWeightVolume")
    protected WeightVolumeType committedWeightVolume;
    @XmlElement(name = "CommittedNumTEU")
    protected String committedNumTEU;
    @XmlElement(name = "CommittedNumFEU")
    protected String committedNumFEU;
    @XmlElement(name = "BookedWeightVolume")
    protected WeightVolumeType bookedWeightVolume;
    @XmlElement(name = "BookedNumTEU")
    protected String bookedNumTEU;
    @XmlElement(name = "BookedNumFEU")
    protected String bookedNumFEU;
    @XmlElement(name = "ProducedWeightVolume")
    protected WeightVolumeType producedWeightVolume;
    @XmlElement(name = "ProducedNumTEU")
    protected String producedNumTEU;
    @XmlElement(name = "ProducedNumFEU")
    protected String producedNumFEU;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "ConsolidationTypeGid")
    protected GLogXMLGidType consolidationTypeGid;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ShipmentGid")
    protected List<GLogXMLGidType> shipmentGid;
    @XmlElement(name = "Shipment")
    protected List<ShipmentType> shipment;
    @XmlElement(name = "CharterVoyage")
    protected CharterVoyageType charterVoyage;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Gets the value of the consolGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolGid() {
        return consolGid;
    }

    /**
     * Sets the value of the consolGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolGid(GLogXMLGidType value) {
        this.consolGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the consolType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsolType() {
        return consolType;
    }

    /**
     * Sets the value of the consolType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsolType(String value) {
        this.consolType = value;
    }

    /**
     * Gets the value of the charterVoyageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCharterVoyageGid() {
        return charterVoyageGid;
    }

    /**
     * Sets the value of the charterVoyageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCharterVoyageGid(GLogXMLGidType value) {
        this.charterVoyageGid = value;
    }

    /**
     * Gets the value of the flightInstanceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightInstanceId() {
        return flightInstanceId;
    }

    /**
     * Sets the value of the flightInstanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightInstanceId(String value) {
        this.flightInstanceId = value;
    }

    /**
     * Gets the value of the stowageModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStowageModeGid() {
        return stowageModeGid;
    }

    /**
     * Sets the value of the stowageModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStowageModeGid(GLogXMLGidType value) {
        this.stowageModeGid = value;
    }

    /**
     * Gets the value of the hazmatType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazmatType() {
        return hazmatType;
    }

    /**
     * Sets the value of the hazmatType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazmatType(String value) {
        this.hazmatType = value;
    }

    /**
     * Gets the value of the itineraryProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryProfileGid() {
        return itineraryProfileGid;
    }

    /**
     * Sets the value of the itineraryProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryProfileGid(GLogXMLGidType value) {
        this.itineraryProfileGid = value;
    }

    /**
     * Gets the value of the allocatedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getAllocatedWeightVolume() {
        return allocatedWeightVolume;
    }

    /**
     * Sets the value of the allocatedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setAllocatedWeightVolume(WeightVolumeType value) {
        this.allocatedWeightVolume = value;
    }

    /**
     * Gets the value of the allocatedNumTEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocatedNumTEU() {
        return allocatedNumTEU;
    }

    /**
     * Sets the value of the allocatedNumTEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocatedNumTEU(String value) {
        this.allocatedNumTEU = value;
    }

    /**
     * Gets the value of the allocatedNumFEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocatedNumFEU() {
        return allocatedNumFEU;
    }

    /**
     * Sets the value of the allocatedNumFEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocatedNumFEU(String value) {
        this.allocatedNumFEU = value;
    }

    /**
     * Gets the value of the maxWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxWeightVolume() {
        return maxWeightVolume;
    }

    /**
     * Sets the value of the maxWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxWeightVolume(WeightVolumeType value) {
        this.maxWeightVolume = value;
    }

    /**
     * Gets the value of the maxNumTEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNumTEU() {
        return maxNumTEU;
    }

    /**
     * Sets the value of the maxNumTEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNumTEU(String value) {
        this.maxNumTEU = value;
    }

    /**
     * Gets the value of the maxNumFEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNumFEU() {
        return maxNumFEU;
    }

    /**
     * Sets the value of the maxNumFEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNumFEU(String value) {
        this.maxNumFEU = value;
    }

    /**
     * Gets the value of the committedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getCommittedWeightVolume() {
        return committedWeightVolume;
    }

    /**
     * Sets the value of the committedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setCommittedWeightVolume(WeightVolumeType value) {
        this.committedWeightVolume = value;
    }

    /**
     * Gets the value of the committedNumTEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommittedNumTEU() {
        return committedNumTEU;
    }

    /**
     * Sets the value of the committedNumTEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommittedNumTEU(String value) {
        this.committedNumTEU = value;
    }

    /**
     * Gets the value of the committedNumFEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommittedNumFEU() {
        return committedNumFEU;
    }

    /**
     * Sets the value of the committedNumFEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommittedNumFEU(String value) {
        this.committedNumFEU = value;
    }

    /**
     * Gets the value of the bookedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getBookedWeightVolume() {
        return bookedWeightVolume;
    }

    /**
     * Sets the value of the bookedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setBookedWeightVolume(WeightVolumeType value) {
        this.bookedWeightVolume = value;
    }

    /**
     * Gets the value of the bookedNumTEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookedNumTEU() {
        return bookedNumTEU;
    }

    /**
     * Sets the value of the bookedNumTEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookedNumTEU(String value) {
        this.bookedNumTEU = value;
    }

    /**
     * Gets the value of the bookedNumFEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookedNumFEU() {
        return bookedNumFEU;
    }

    /**
     * Sets the value of the bookedNumFEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookedNumFEU(String value) {
        this.bookedNumFEU = value;
    }

    /**
     * Gets the value of the producedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getProducedWeightVolume() {
        return producedWeightVolume;
    }

    /**
     * Sets the value of the producedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setProducedWeightVolume(WeightVolumeType value) {
        this.producedWeightVolume = value;
    }

    /**
     * Gets the value of the producedNumTEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducedNumTEU() {
        return producedNumTEU;
    }

    /**
     * Sets the value of the producedNumTEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducedNumTEU(String value) {
        this.producedNumTEU = value;
    }

    /**
     * Gets the value of the producedNumFEU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducedNumFEU() {
        return producedNumFEU;
    }

    /**
     * Sets the value of the producedNumFEU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducedNumFEU(String value) {
        this.producedNumFEU = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the consolidationTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolidationTypeGid() {
        return consolidationTypeGid;
    }

    /**
     * Sets the value of the consolidationTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolidationTypeGid(GLogXMLGidType value) {
        this.consolidationTypeGid = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getShipmentGid() {
        if (shipmentGid == null) {
            shipmentGid = new ArrayList<GLogXMLGidType>();
        }
        return this.shipmentGid;
    }

    /**
     * Gets the value of the shipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentType }
     * 
     * 
     */
    public List<ShipmentType> getShipment() {
        if (shipment == null) {
            shipment = new ArrayList<ShipmentType>();
        }
        return this.shipment;
    }

    /**
     * Gets the value of the charterVoyage property.
     * 
     * @return
     *     possible object is
     *     {@link CharterVoyageType }
     *     
     */
    public CharterVoyageType getCharterVoyage() {
        return charterVoyage;
    }

    /**
     * Sets the value of the charterVoyage property.
     * 
     * @param value
     *     allowed object is
     *     {@link CharterVoyageType }
     *     
     */
    public void setCharterVoyage(CharterVoyageType value) {
        this.charterVoyage = value;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}

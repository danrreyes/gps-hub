
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * SSStop is a structure containing stop information
 * 
 * <p>Java class for SSStopType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSStopType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SSStopSequenceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SSLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SSLocationType" minOccurs="0"/&gt;
 *         &lt;element name="RefPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StopType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSStopType", propOrder = {
    "ssStopSequenceNum",
    "ssLocation",
    "refPosition",
    "stopType"
})
public class SSStopType {

    @XmlElement(name = "SSStopSequenceNum")
    protected String ssStopSequenceNum;
    @XmlElement(name = "SSLocation")
    protected SSLocationType ssLocation;
    @XmlElement(name = "RefPosition")
    protected String refPosition;
    @XmlElement(name = "StopType")
    protected String stopType;

    /**
     * Gets the value of the ssStopSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSStopSequenceNum() {
        return ssStopSequenceNum;
    }

    /**
     * Sets the value of the ssStopSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSStopSequenceNum(String value) {
        this.ssStopSequenceNum = value;
    }

    /**
     * Gets the value of the ssLocation property.
     * 
     * @return
     *     possible object is
     *     {@link SSLocationType }
     *     
     */
    public SSLocationType getSSLocation() {
        return ssLocation;
    }

    /**
     * Sets the value of the ssLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSLocationType }
     *     
     */
    public void setSSLocation(SSLocationType value) {
        this.ssLocation = value;
    }

    /**
     * Gets the value of the refPosition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefPosition() {
        return refPosition;
    }

    /**
     * Sets the value of the refPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefPosition(String value) {
        this.refPosition = value;
    }

    /**
     * Gets the value of the stopType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopType() {
        return stopType;
    }

    /**
     * Sets the value of the stopType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopType(String value) {
        this.stopType = value;
    }

}

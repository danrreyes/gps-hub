
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLShipUnitSpecRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLShipUnitSpecRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipUnitSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitSpecRefType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLShipUnitSpecRefType", propOrder = {
    "shipUnitSpecRef"
})
public class GLogXMLShipUnitSpecRefType {

    @XmlElement(name = "ShipUnitSpecRef", required = true)
    protected ShipUnitSpecRefType shipUnitSpecRef;

    /**
     * Gets the value of the shipUnitSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitSpecRefType }
     *     
     */
    public ShipUnitSpecRefType getShipUnitSpecRef() {
        return shipUnitSpecRef;
    }

    /**
     * Sets the value of the shipUnitSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitSpecRefType }
     *     
     */
    public void setShipUnitSpecRef(ShipUnitSpecRefType value) {
        this.shipUnitSpecRef = value;
    }

}

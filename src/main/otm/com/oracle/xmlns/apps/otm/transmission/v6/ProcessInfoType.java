
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ProcessInfo specifies information pertaining to the processing of GLogXMLElements.
 *             Refer to the comments for the individual elements for more information.
 *          
 * 
 * <p>Java class for ProcessInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProcessInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProcessGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProcessSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessInfoType", propOrder = {
    "processGroup",
    "processSequence"
})
public class ProcessInfoType {

    @XmlElement(name = "ProcessGroup")
    protected String processGroup;
    @XmlElement(name = "ProcessSequence")
    protected String processSequence;

    /**
     * Gets the value of the processGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessGroup() {
        return processGroup;
    }

    /**
     * Sets the value of the processGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessGroup(String value) {
        this.processGroup = value;
    }

    /**
     * Gets the value of the processSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessSequence() {
        return processSequence;
    }

    /**
     * Sets the value of the processSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessSequence(String value) {
        this.processSequence = value;
    }

}

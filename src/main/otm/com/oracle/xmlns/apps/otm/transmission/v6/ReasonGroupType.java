
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReasonGroupType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReasonGroupType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReasonGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ReasonGroupDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReasonGroupType", propOrder = {
    "reasonGroupGid",
    "reasonGroupDescription"
})
public class ReasonGroupType {

    @XmlElement(name = "ReasonGroupGid", required = true)
    protected GLogXMLGidType reasonGroupGid;
    @XmlElement(name = "ReasonGroupDescription")
    protected String reasonGroupDescription;

    /**
     * Gets the value of the reasonGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReasonGroupGid() {
        return reasonGroupGid;
    }

    /**
     * Sets the value of the reasonGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReasonGroupGid(GLogXMLGidType value) {
        this.reasonGroupGid = value;
    }

    /**
     * Gets the value of the reasonGroupDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonGroupDescription() {
        return reasonGroupDescription;
    }

    /**
     * Sets the value of the reasonGroupDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonGroupDescription(String value) {
        this.reasonGroupDescription = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to indicate an item and its various packaging(s).
 * 
 * <p>Java class for ItemMasterType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemMasterType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Item" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemType"/&gt;
 *         &lt;element name="Packaging" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagingType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemMasterType", propOrder = {
    "item",
    "packaging"
})
public class ItemMasterType
    extends OTMTransactionInOut
{

    @XmlElement(name = "Item", required = true)
    protected ItemType item;
    @XmlElement(name = "Packaging")
    protected List<PackagingType> packaging;

    /**
     * Gets the value of the item property.
     * 
     * @return
     *     possible object is
     *     {@link ItemType }
     *     
     */
    public ItemType getItem() {
        return item;
    }

    /**
     * Sets the value of the item property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemType }
     *     
     */
    public void setItem(ItemType value) {
        this.item = value;
    }

    /**
     * Gets the value of the packaging property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the packaging property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackaging().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagingType }
     * 
     * 
     */
    public List<PackagingType> getPackaging() {
        if (packaging == null) {
            packaging = new ArrayList<PackagingType>();
        }
        return this.packaging;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the content of the document.
 *          
 * 
 * <p>Java class for DocumentContentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentContentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DocMimeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="DocumentURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *           &lt;element name="DocumentSecureURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *           &lt;element name="DocContentText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *           &lt;element name="DocContentBinary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentContentType", propOrder = {
    "docMimeType",
    "documentURL",
    "documentSecureURL",
    "docContentText",
    "docContentBinary"
})
public class DocumentContentType {

    @XmlElement(name = "DocMimeType")
    protected String docMimeType;
    @XmlElement(name = "DocumentURL")
    protected String documentURL;
    @XmlElement(name = "DocumentSecureURL")
    protected String documentSecureURL;
    @XmlElement(name = "DocContentText")
    protected String docContentText;
    @XmlElement(name = "DocContentBinary")
    protected String docContentBinary;

    /**
     * Gets the value of the docMimeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocMimeType() {
        return docMimeType;
    }

    /**
     * Sets the value of the docMimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocMimeType(String value) {
        this.docMimeType = value;
    }

    /**
     * Gets the value of the documentURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentURL() {
        return documentURL;
    }

    /**
     * Sets the value of the documentURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentURL(String value) {
        this.documentURL = value;
    }

    /**
     * Gets the value of the documentSecureURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentSecureURL() {
        return documentSecureURL;
    }

    /**
     * Sets the value of the documentSecureURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentSecureURL(String value) {
        this.documentSecureURL = value;
    }

    /**
     * Gets the value of the docContentText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocContentText() {
        return docContentText;
    }

    /**
     * Sets the value of the docContentText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocContentText(String value) {
        this.docContentText = value;
    }

    /**
     * Gets the value of the docContentBinary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocContentBinary() {
        return docContentBinary;
    }

    /**
     * Sets the value of the docContentBinary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocContentBinary(String value) {
        this.docContentBinary = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Indicates the special services required to for the shipment. Allows you to define remarks for why a special service applies to the associated
 *             shipment.
 *          
 * 
 * <p>Java class for ShipmentSpecialServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentSpecialServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="SpclService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpclServiceType"/&gt;
 *           &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ShipSpclServiceSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProcessAsFlowThru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CompletionState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PayableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BillableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ActualOccurTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="IsPlanDurFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PlannedDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ActualDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ActualDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="ActualWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="ActualVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="ActualShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActualItemPackageCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipSpclServiceRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipSpclServiceRefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentSpecialServiceType", propOrder = {
    "spclService",
    "specialServiceGid",
    "shipSpclServiceSequence",
    "processAsFlowThru",
    "adjustmentReasonGid",
    "completionState",
    "payableIndicatorGid",
    "billableIndicatorGid",
    "actualOccurTime",
    "isPlanDurFixed",
    "plannedDuration",
    "actualDuration",
    "actualDistance",
    "actualWeight",
    "actualVolume",
    "actualShipUnitCount",
    "actualItemPackageCount",
    "isSystemGenerated",
    "stopSequence",
    "shipSpclServiceRef",
    "remark"
})
public class ShipmentSpecialServiceType {

    @XmlElement(name = "SpclService")
    protected SpclServiceType spclService;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "ShipSpclServiceSequence")
    protected String shipSpclServiceSequence;
    @XmlElement(name = "ProcessAsFlowThru")
    protected String processAsFlowThru;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "CompletionState")
    protected String completionState;
    @XmlElement(name = "PayableIndicatorGid")
    protected GLogXMLGidType payableIndicatorGid;
    @XmlElement(name = "BillableIndicatorGid")
    protected GLogXMLGidType billableIndicatorGid;
    @XmlElement(name = "ActualOccurTime")
    protected GLogDateTimeType actualOccurTime;
    @XmlElement(name = "IsPlanDurFixed")
    protected String isPlanDurFixed;
    @XmlElement(name = "PlannedDuration")
    protected GLogXMLDurationType plannedDuration;
    @XmlElement(name = "ActualDuration")
    protected GLogXMLDurationType actualDuration;
    @XmlElement(name = "ActualDistance")
    protected GLogXMLDistanceType actualDistance;
    @XmlElement(name = "ActualWeight")
    protected GLogXMLWeightType actualWeight;
    @XmlElement(name = "ActualVolume")
    protected GLogXMLVolumeType actualVolume;
    @XmlElement(name = "ActualShipUnitCount")
    protected String actualShipUnitCount;
    @XmlElement(name = "ActualItemPackageCount")
    protected String actualItemPackageCount;
    @XmlElement(name = "IsSystemGenerated")
    protected String isSystemGenerated;
    @XmlElement(name = "StopSequence")
    protected String stopSequence;
    @XmlElement(name = "ShipSpclServiceRef")
    protected List<ShipSpclServiceRefType> shipSpclServiceRef;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;

    /**
     * Gets the value of the spclService property.
     * 
     * @return
     *     possible object is
     *     {@link SpclServiceType }
     *     
     */
    public SpclServiceType getSpclService() {
        return spclService;
    }

    /**
     * Sets the value of the spclService property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpclServiceType }
     *     
     */
    public void setSpclService(SpclServiceType value) {
        this.spclService = value;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Sets the value of the specialServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Gets the value of the shipSpclServiceSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipSpclServiceSequence() {
        return shipSpclServiceSequence;
    }

    /**
     * Sets the value of the shipSpclServiceSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipSpclServiceSequence(String value) {
        this.shipSpclServiceSequence = value;
    }

    /**
     * Gets the value of the processAsFlowThru property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessAsFlowThru() {
        return processAsFlowThru;
    }

    /**
     * Sets the value of the processAsFlowThru property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessAsFlowThru(String value) {
        this.processAsFlowThru = value;
    }

    /**
     * Gets the value of the adjustmentReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Sets the value of the adjustmentReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Gets the value of the completionState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompletionState() {
        return completionState;
    }

    /**
     * Sets the value of the completionState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompletionState(String value) {
        this.completionState = value;
    }

    /**
     * Gets the value of the payableIndicatorGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPayableIndicatorGid() {
        return payableIndicatorGid;
    }

    /**
     * Sets the value of the payableIndicatorGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPayableIndicatorGid(GLogXMLGidType value) {
        this.payableIndicatorGid = value;
    }

    /**
     * Gets the value of the billableIndicatorGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBillableIndicatorGid() {
        return billableIndicatorGid;
    }

    /**
     * Sets the value of the billableIndicatorGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBillableIndicatorGid(GLogXMLGidType value) {
        this.billableIndicatorGid = value;
    }

    /**
     * Gets the value of the actualOccurTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActualOccurTime() {
        return actualOccurTime;
    }

    /**
     * Sets the value of the actualOccurTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActualOccurTime(GLogDateTimeType value) {
        this.actualOccurTime = value;
    }

    /**
     * Gets the value of the isPlanDurFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPlanDurFixed() {
        return isPlanDurFixed;
    }

    /**
     * Sets the value of the isPlanDurFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPlanDurFixed(String value) {
        this.isPlanDurFixed = value;
    }

    /**
     * Gets the value of the plannedDuration property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getPlannedDuration() {
        return plannedDuration;
    }

    /**
     * Sets the value of the plannedDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setPlannedDuration(GLogXMLDurationType value) {
        this.plannedDuration = value;
    }

    /**
     * Gets the value of the actualDuration property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActualDuration() {
        return actualDuration;
    }

    /**
     * Sets the value of the actualDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActualDuration(GLogXMLDurationType value) {
        this.actualDuration = value;
    }

    /**
     * Gets the value of the actualDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getActualDistance() {
        return actualDistance;
    }

    /**
     * Sets the value of the actualDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setActualDistance(GLogXMLDistanceType value) {
        this.actualDistance = value;
    }

    /**
     * Gets the value of the actualWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getActualWeight() {
        return actualWeight;
    }

    /**
     * Sets the value of the actualWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setActualWeight(GLogXMLWeightType value) {
        this.actualWeight = value;
    }

    /**
     * Gets the value of the actualVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getActualVolume() {
        return actualVolume;
    }

    /**
     * Sets the value of the actualVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setActualVolume(GLogXMLVolumeType value) {
        this.actualVolume = value;
    }

    /**
     * Gets the value of the actualShipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualShipUnitCount() {
        return actualShipUnitCount;
    }

    /**
     * Sets the value of the actualShipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualShipUnitCount(String value) {
        this.actualShipUnitCount = value;
    }

    /**
     * Gets the value of the actualItemPackageCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActualItemPackageCount() {
        return actualItemPackageCount;
    }

    /**
     * Sets the value of the actualItemPackageCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActualItemPackageCount(String value) {
        this.actualItemPackageCount = value;
    }

    /**
     * Gets the value of the isSystemGenerated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Sets the value of the isSystemGenerated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

    /**
     * Gets the value of the stopSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Sets the value of the stopSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Gets the value of the shipSpclServiceRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipSpclServiceRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipSpclServiceRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipSpclServiceRefType }
     * 
     * 
     */
    public List<ShipSpclServiceRefType> getShipSpclServiceRef() {
        if (shipSpclServiceRef == null) {
            shipSpclServiceRef = new ArrayList<ShipSpclServiceRefType>();
        }
        return this.shipSpclServiceRef;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

}

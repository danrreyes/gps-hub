
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * LocationRole is a structure specifying a location role.
 *             Locations may play multiple roles.
 *             Examples of location roles include warehouse, crossdock, loading dock, etc.
 *             A calendar may be associated with a location role, to limit the times when a particular activity (such as loading) may occur.
 *          
 * 
 * <p>Java class for LocationRoleType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationRoleType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="XDockIsInboundBias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FixedHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="VarHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="VarHandlingWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="MaxFreightIdleTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="CreatePoolHandlingShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreateXDockHandlingShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationActivityTimeDef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationActivityTimeDefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PctActivityBeforeLocOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PctActivityAfterLocClose" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsMixedFreightTHUAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LocationTHUCapacity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationTHUCapacityType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VarHandlingCostPerVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="VarHandlingVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="RegionalHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RegionalHandlingTimeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationRoleType", propOrder = {
    "locationRoleGid",
    "calendarGid",
    "xDockIsInboundBias",
    "fixedHandlingTime",
    "varHandlingTime",
    "varHandlingWeight",
    "maxFreightIdleTime",
    "createPoolHandlingShipment",
    "createXDockHandlingShipment",
    "locationActivityTimeDef",
    "pctActivityBeforeLocOpen",
    "pctActivityAfterLocClose",
    "isMixedFreightTHUAllowed",
    "shipUnitSpecProfileGid",
    "locationTHUCapacity",
    "varHandlingCostPerVolume",
    "varHandlingVolume",
    "regionalHandlingTime"
})
public class LocationRoleType {

    @XmlElement(name = "LocationRoleGid", required = true)
    protected GLogXMLGidType locationRoleGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "XDockIsInboundBias")
    protected String xDockIsInboundBias;
    @XmlElement(name = "FixedHandlingTime")
    protected GLogXMLDurationType fixedHandlingTime;
    @XmlElement(name = "VarHandlingTime")
    protected GLogXMLDurationType varHandlingTime;
    @XmlElement(name = "VarHandlingWeight")
    protected GLogXMLWeightType varHandlingWeight;
    @XmlElement(name = "MaxFreightIdleTime")
    protected GLogXMLDurationType maxFreightIdleTime;
    @XmlElement(name = "CreatePoolHandlingShipment")
    protected String createPoolHandlingShipment;
    @XmlElement(name = "CreateXDockHandlingShipment")
    protected String createXDockHandlingShipment;
    @XmlElement(name = "LocationActivityTimeDef")
    protected List<LocationActivityTimeDefType> locationActivityTimeDef;
    @XmlElement(name = "PctActivityBeforeLocOpen")
    protected String pctActivityBeforeLocOpen;
    @XmlElement(name = "PctActivityAfterLocClose")
    protected String pctActivityAfterLocClose;
    @XmlElement(name = "IsMixedFreightTHUAllowed")
    protected String isMixedFreightTHUAllowed;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "LocationTHUCapacity")
    protected List<LocationTHUCapacityType> locationTHUCapacity;
    @XmlElement(name = "VarHandlingCostPerVolume")
    protected GLogXMLFinancialAmountType varHandlingCostPerVolume;
    @XmlElement(name = "VarHandlingVolume")
    protected GLogXMLVolumeType varHandlingVolume;
    @XmlElement(name = "RegionalHandlingTime")
    protected List<RegionalHandlingTimeType> regionalHandlingTime;

    /**
     * Gets the value of the locationRoleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationRoleGid() {
        return locationRoleGid;
    }

    /**
     * Sets the value of the locationRoleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationRoleGid(GLogXMLGidType value) {
        this.locationRoleGid = value;
    }

    /**
     * Gets the value of the calendarGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Sets the value of the calendarGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Gets the value of the xDockIsInboundBias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXDockIsInboundBias() {
        return xDockIsInboundBias;
    }

    /**
     * Sets the value of the xDockIsInboundBias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXDockIsInboundBias(String value) {
        this.xDockIsInboundBias = value;
    }

    /**
     * Gets the value of the fixedHandlingTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getFixedHandlingTime() {
        return fixedHandlingTime;
    }

    /**
     * Sets the value of the fixedHandlingTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setFixedHandlingTime(GLogXMLDurationType value) {
        this.fixedHandlingTime = value;
    }

    /**
     * Gets the value of the varHandlingTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getVarHandlingTime() {
        return varHandlingTime;
    }

    /**
     * Sets the value of the varHandlingTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setVarHandlingTime(GLogXMLDurationType value) {
        this.varHandlingTime = value;
    }

    /**
     * Gets the value of the varHandlingWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getVarHandlingWeight() {
        return varHandlingWeight;
    }

    /**
     * Sets the value of the varHandlingWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setVarHandlingWeight(GLogXMLWeightType value) {
        this.varHandlingWeight = value;
    }

    /**
     * Gets the value of the maxFreightIdleTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaxFreightIdleTime() {
        return maxFreightIdleTime;
    }

    /**
     * Sets the value of the maxFreightIdleTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaxFreightIdleTime(GLogXMLDurationType value) {
        this.maxFreightIdleTime = value;
    }

    /**
     * Gets the value of the createPoolHandlingShipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatePoolHandlingShipment() {
        return createPoolHandlingShipment;
    }

    /**
     * Sets the value of the createPoolHandlingShipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatePoolHandlingShipment(String value) {
        this.createPoolHandlingShipment = value;
    }

    /**
     * Gets the value of the createXDockHandlingShipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateXDockHandlingShipment() {
        return createXDockHandlingShipment;
    }

    /**
     * Sets the value of the createXDockHandlingShipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateXDockHandlingShipment(String value) {
        this.createXDockHandlingShipment = value;
    }

    /**
     * Gets the value of the locationActivityTimeDef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationActivityTimeDef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationActivityTimeDef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationActivityTimeDefType }
     * 
     * 
     */
    public List<LocationActivityTimeDefType> getLocationActivityTimeDef() {
        if (locationActivityTimeDef == null) {
            locationActivityTimeDef = new ArrayList<LocationActivityTimeDefType>();
        }
        return this.locationActivityTimeDef;
    }

    /**
     * Gets the value of the pctActivityBeforeLocOpen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPctActivityBeforeLocOpen() {
        return pctActivityBeforeLocOpen;
    }

    /**
     * Sets the value of the pctActivityBeforeLocOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPctActivityBeforeLocOpen(String value) {
        this.pctActivityBeforeLocOpen = value;
    }

    /**
     * Gets the value of the pctActivityAfterLocClose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPctActivityAfterLocClose() {
        return pctActivityAfterLocClose;
    }

    /**
     * Sets the value of the pctActivityAfterLocClose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPctActivityAfterLocClose(String value) {
        this.pctActivityAfterLocClose = value;
    }

    /**
     * Gets the value of the isMixedFreightTHUAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMixedFreightTHUAllowed() {
        return isMixedFreightTHUAllowed;
    }

    /**
     * Sets the value of the isMixedFreightTHUAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMixedFreightTHUAllowed(String value) {
        this.isMixedFreightTHUAllowed = value;
    }

    /**
     * Gets the value of the shipUnitSpecProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Sets the value of the shipUnitSpecProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the locationTHUCapacity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationTHUCapacity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationTHUCapacity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationTHUCapacityType }
     * 
     * 
     */
    public List<LocationTHUCapacityType> getLocationTHUCapacity() {
        if (locationTHUCapacity == null) {
            locationTHUCapacity = new ArrayList<LocationTHUCapacityType>();
        }
        return this.locationTHUCapacity;
    }

    /**
     * Gets the value of the varHandlingCostPerVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVarHandlingCostPerVolume() {
        return varHandlingCostPerVolume;
    }

    /**
     * Sets the value of the varHandlingCostPerVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVarHandlingCostPerVolume(GLogXMLFinancialAmountType value) {
        this.varHandlingCostPerVolume = value;
    }

    /**
     * Gets the value of the varHandlingVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getVarHandlingVolume() {
        return varHandlingVolume;
    }

    /**
     * Sets the value of the varHandlingVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setVarHandlingVolume(GLogXMLVolumeType value) {
        this.varHandlingVolume = value;
    }

    /**
     * Gets the value of the regionalHandlingTime property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the regionalHandlingTime property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegionalHandlingTime().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegionalHandlingTimeType }
     * 
     * 
     */
    public List<RegionalHandlingTimeType> getRegionalHandlingTime() {
        if (regionalHandlingTime == null) {
            regionalHandlingTime = new ArrayList<RegionalHandlingTimeType>();
        }
        return this.regionalHandlingTime;
    }

}

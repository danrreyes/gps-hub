
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * OrderLineRefnum is an alternate method for identifying an order line. It consists of a
 *             qualifier and a value.
 *          
 * 
 * <p>Java class for OrderLineRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderLineRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OrderLineRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="OrderLineRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderLineRefnumType", propOrder = {
    "orderLineRefnumQualifierGid",
    "orderLineRefnumValue"
})
public class OrderLineRefnumType {

    @XmlElement(name = "OrderLineRefnumQualifierGid", required = true)
    protected GLogXMLGidType orderLineRefnumQualifierGid;
    @XmlElement(name = "OrderLineRefnumValue", required = true)
    protected String orderLineRefnumValue;

    /**
     * Gets the value of the orderLineRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderLineRefnumQualifierGid() {
        return orderLineRefnumQualifierGid;
    }

    /**
     * Sets the value of the orderLineRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderLineRefnumQualifierGid(GLogXMLGidType value) {
        this.orderLineRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the orderLineRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineRefnumValue() {
        return orderLineRefnumValue;
    }

    /**
     * Sets the value of the orderLineRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineRefnumValue(String value) {
        this.orderLineRefnumValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             SStatusShipUnit is a structure for specifying received ship unit quantities.
 *          
 * 
 * <p>Java class for SStatusShipUnitType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SStatusShipUnitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice&gt;
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/&gt;
 *           &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ReceivedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="ReceivedShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SStatusShipUnitContent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SStatusShipUnitContentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SStatusShipUnitType", propOrder = {
    "intSavedQuery",
    "shipUnitGid",
    "receivedWeightVolume",
    "receivedShipUnitCount",
    "sStatusShipUnitContent"
})
public class SStatusShipUnitType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "ReceivedWeightVolume")
    protected WeightVolumeType receivedWeightVolume;
    @XmlElement(name = "ReceivedShipUnitCount")
    protected String receivedShipUnitCount;
    @XmlElement(name = "SStatusShipUnitContent")
    protected List<SStatusShipUnitContentType> sStatusShipUnitContent;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the shipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Sets the value of the shipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Gets the value of the receivedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getReceivedWeightVolume() {
        return receivedWeightVolume;
    }

    /**
     * Sets the value of the receivedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setReceivedWeightVolume(WeightVolumeType value) {
        this.receivedWeightVolume = value;
    }

    /**
     * Gets the value of the receivedShipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedShipUnitCount() {
        return receivedShipUnitCount;
    }

    /**
     * Sets the value of the receivedShipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedShipUnitCount(String value) {
        this.receivedShipUnitCount = value;
    }

    /**
     * Gets the value of the sStatusShipUnitContent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the sStatusShipUnitContent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSStatusShipUnitContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SStatusShipUnitContentType }
     * 
     * 
     */
    public List<SStatusShipUnitContentType> getSStatusShipUnitContent() {
        if (sStatusShipUnitContent == null) {
            sStatusShipUnitContent = new ArrayList<SStatusShipUnitContentType>();
        }
        return this.sStatusShipUnitContent;
    }

}

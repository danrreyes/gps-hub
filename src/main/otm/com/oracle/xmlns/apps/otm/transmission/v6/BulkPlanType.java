
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             This is used to provide statistics about the orders that were planned and the shipments that were
 *             created during a given run of bulk plan.
 *          
 * 
 * <p>Java class for BulkPlanType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BulkPlanType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="BulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="NumOfOrdersSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfOrdersUnassigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfOrdersPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentsBuilt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOrdersPlannedFailed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOrdersExcluded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="NumOrderMovementsSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOrderMovementsUnassigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOrderMovementsPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOrderMovementsFailed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOrderMovementsExcluded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="TotalDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="TotalNumStops" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BulkPlanByMode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BulkPlanByModeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="BulkPlanPartition" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BulkPlanPartitionType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BulkPlanType", propOrder = {
    "sendReason",
    "bulkPlanGid",
    "perspective",
    "queryName",
    "startTime",
    "endTime",
    "numOfOrdersSelected",
    "numOfOrdersUnassigned",
    "numOfOrdersPlanned",
    "numOfShipmentsBuilt",
    "numOrdersPlannedFailed",
    "numOrdersExcluded",
    "planningParamSetGid",
    "numOrderMovementsSelected",
    "numOrderMovementsUnassigned",
    "numOrderMovementsPlanned",
    "numOrderMovementsFailed",
    "numOrderMovementsExcluded",
    "totalCost",
    "totalWeightVolume",
    "totalDistance",
    "totalNumStops",
    "bulkPlanByMode",
    "bulkPlanPartition"
})
public class BulkPlanType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "BulkPlanGid", required = true)
    protected GLogXMLGidType bulkPlanGid;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "QueryName")
    protected String queryName;
    @XmlElement(name = "StartTime")
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime")
    protected GLogDateTimeType endTime;
    @XmlElement(name = "NumOfOrdersSelected")
    protected String numOfOrdersSelected;
    @XmlElement(name = "NumOfOrdersUnassigned")
    protected String numOfOrdersUnassigned;
    @XmlElement(name = "NumOfOrdersPlanned")
    protected String numOfOrdersPlanned;
    @XmlElement(name = "NumOfShipmentsBuilt")
    protected String numOfShipmentsBuilt;
    @XmlElement(name = "NumOrdersPlannedFailed")
    protected String numOrdersPlannedFailed;
    @XmlElement(name = "NumOrdersExcluded")
    protected String numOrdersExcluded;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "NumOrderMovementsSelected")
    protected String numOrderMovementsSelected;
    @XmlElement(name = "NumOrderMovementsUnassigned")
    protected String numOrderMovementsUnassigned;
    @XmlElement(name = "NumOrderMovementsPlanned")
    protected String numOrderMovementsPlanned;
    @XmlElement(name = "NumOrderMovementsFailed")
    protected String numOrderMovementsFailed;
    @XmlElement(name = "NumOrderMovementsExcluded")
    protected String numOrderMovementsExcluded;
    @XmlElement(name = "TotalCost")
    protected GLogXMLFinancialAmountType totalCost;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TotalDistance")
    protected GLogXMLDistanceType totalDistance;
    @XmlElement(name = "TotalNumStops")
    protected String totalNumStops;
    @XmlElement(name = "BulkPlanByMode")
    protected List<BulkPlanByModeType> bulkPlanByMode;
    @XmlElement(name = "BulkPlanPartition")
    protected List<BulkPlanPartitionType> bulkPlanPartition;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the bulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkPlanGid() {
        return bulkPlanGid;
    }

    /**
     * Sets the value of the bulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkPlanGid(GLogXMLGidType value) {
        this.bulkPlanGid = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the queryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Sets the value of the queryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the endTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Sets the value of the endTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Gets the value of the numOfOrdersSelected property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersSelected() {
        return numOfOrdersSelected;
    }

    /**
     * Sets the value of the numOfOrdersSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersSelected(String value) {
        this.numOfOrdersSelected = value;
    }

    /**
     * Gets the value of the numOfOrdersUnassigned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersUnassigned() {
        return numOfOrdersUnassigned;
    }

    /**
     * Sets the value of the numOfOrdersUnassigned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersUnassigned(String value) {
        this.numOfOrdersUnassigned = value;
    }

    /**
     * Gets the value of the numOfOrdersPlanned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrdersPlanned() {
        return numOfOrdersPlanned;
    }

    /**
     * Sets the value of the numOfOrdersPlanned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrdersPlanned(String value) {
        this.numOfOrdersPlanned = value;
    }

    /**
     * Gets the value of the numOfShipmentsBuilt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsBuilt() {
        return numOfShipmentsBuilt;
    }

    /**
     * Sets the value of the numOfShipmentsBuilt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsBuilt(String value) {
        this.numOfShipmentsBuilt = value;
    }

    /**
     * Gets the value of the numOrdersPlannedFailed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrdersPlannedFailed() {
        return numOrdersPlannedFailed;
    }

    /**
     * Sets the value of the numOrdersPlannedFailed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrdersPlannedFailed(String value) {
        this.numOrdersPlannedFailed = value;
    }

    /**
     * Gets the value of the numOrdersExcluded property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrdersExcluded() {
        return numOrdersExcluded;
    }

    /**
     * Sets the value of the numOrdersExcluded property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrdersExcluded(String value) {
        this.numOrdersExcluded = value;
    }

    /**
     * Gets the value of the planningParamSetGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Sets the value of the planningParamSetGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Gets the value of the numOrderMovementsSelected property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsSelected() {
        return numOrderMovementsSelected;
    }

    /**
     * Sets the value of the numOrderMovementsSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsSelected(String value) {
        this.numOrderMovementsSelected = value;
    }

    /**
     * Gets the value of the numOrderMovementsUnassigned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsUnassigned() {
        return numOrderMovementsUnassigned;
    }

    /**
     * Sets the value of the numOrderMovementsUnassigned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsUnassigned(String value) {
        this.numOrderMovementsUnassigned = value;
    }

    /**
     * Gets the value of the numOrderMovementsPlanned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsPlanned() {
        return numOrderMovementsPlanned;
    }

    /**
     * Sets the value of the numOrderMovementsPlanned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsPlanned(String value) {
        this.numOrderMovementsPlanned = value;
    }

    /**
     * Gets the value of the numOrderMovementsFailed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsFailed() {
        return numOrderMovementsFailed;
    }

    /**
     * Sets the value of the numOrderMovementsFailed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsFailed(String value) {
        this.numOrderMovementsFailed = value;
    }

    /**
     * Gets the value of the numOrderMovementsExcluded property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderMovementsExcluded() {
        return numOrderMovementsExcluded;
    }

    /**
     * Sets the value of the numOrderMovementsExcluded property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderMovementsExcluded(String value) {
        this.numOrderMovementsExcluded = value;
    }

    /**
     * Gets the value of the totalCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalCost() {
        return totalCost;
    }

    /**
     * Sets the value of the totalCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalCost(GLogXMLFinancialAmountType value) {
        this.totalCost = value;
    }

    /**
     * Gets the value of the totalWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Sets the value of the totalWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Gets the value of the totalDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getTotalDistance() {
        return totalDistance;
    }

    /**
     * Sets the value of the totalDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setTotalDistance(GLogXMLDistanceType value) {
        this.totalDistance = value;
    }

    /**
     * Gets the value of the totalNumStops property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNumStops() {
        return totalNumStops;
    }

    /**
     * Sets the value of the totalNumStops property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNumStops(String value) {
        this.totalNumStops = value;
    }

    /**
     * Gets the value of the bulkPlanByMode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bulkPlanByMode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBulkPlanByMode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BulkPlanByModeType }
     * 
     * 
     */
    public List<BulkPlanByModeType> getBulkPlanByMode() {
        if (bulkPlanByMode == null) {
            bulkPlanByMode = new ArrayList<BulkPlanByModeType>();
        }
        return this.bulkPlanByMode;
    }

    /**
     * Gets the value of the bulkPlanPartition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bulkPlanPartition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBulkPlanPartition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BulkPlanPartitionType }
     * 
     * 
     */
    public List<BulkPlanPartitionType> getBulkPlanPartition() {
        if (bulkPlanPartition == null) {
            bulkPlanPartition = new ArrayList<BulkPlanPartitionType>();
        }
        return this.bulkPlanPartition;
    }

}

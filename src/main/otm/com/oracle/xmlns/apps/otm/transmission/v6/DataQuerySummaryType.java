
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * The DataQuerySummary element is used to provide a summary (i.e. primary Id) of the
 *             notification/data/information being sent to an external system. Certain external
 *             systems
 *             may not want GC3 to send large amounts of data when the system is unprepared
 *             to receive it. Alternatively, this provides a mechanism to send only a summary of
 *             the data. The external system
 *             can retrieve the summary data and request the
 *             individual records from GC3 at appropriate times (e.g. idle times, overnight).
 *          
 * 
 * <p>Java class for DataQuerySummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DataQuerySummaryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType"/&gt;
 *         &lt;element name="DataQueryTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RecordCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DataList" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="DataGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DataQuerySummaryType", propOrder = {
    "transactionCodeGid",
    "sendReason",
    "dataQueryTypeGid",
    "recordCount",
    "dataList"
})
public class DataQuerySummaryType
    extends OTMTransactionOut
{

    @XmlElement(name = "TransactionCodeGid")
    protected GLogXMLGidType transactionCodeGid;
    @XmlElement(name = "SendReason", required = true)
    protected SendReasonType sendReason;
    @XmlElement(name = "DataQueryTypeGid", required = true)
    protected GLogXMLGidType dataQueryTypeGid;
    @XmlElement(name = "RecordCount", required = true)
    protected String recordCount;
    @XmlElement(name = "DataList")
    protected DataQuerySummaryType.DataList dataList;

    /**
     * Gets the value of the transactionCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransactionCodeGid() {
        return transactionCodeGid;
    }

    /**
     * Sets the value of the transactionCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransactionCodeGid(GLogXMLGidType value) {
        this.transactionCodeGid = value;
    }

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the dataQueryTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueryTypeGid() {
        return dataQueryTypeGid;
    }

    /**
     * Sets the value of the dataQueryTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueryTypeGid(GLogXMLGidType value) {
        this.dataQueryTypeGid = value;
    }

    /**
     * Gets the value of the recordCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordCount() {
        return recordCount;
    }

    /**
     * Sets the value of the recordCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordCount(String value) {
        this.recordCount = value;
    }

    /**
     * Gets the value of the dataList property.
     * 
     * @return
     *     possible object is
     *     {@link DataQuerySummaryType.DataList }
     *     
     */
    public DataQuerySummaryType.DataList getDataList() {
        return dataList;
    }

    /**
     * Sets the value of the dataList property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataQuerySummaryType.DataList }
     *     
     */
    public void setDataList(DataQuerySummaryType.DataList value) {
        this.dataList = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="DataGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataGid"
    })
    public static class DataList {

        @XmlElement(name = "DataGid", required = true)
        protected List<GLogXMLGidType> dataGid;

        /**
         * Gets the value of the dataGid property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the dataGid property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDataGid().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLGidType }
         * 
         * 
         */
        public List<GLogXMLGidType> getDataGid() {
            if (dataGid == null) {
                dataGid = new ArrayList<GLogXMLGidType>();
            }
            return this.dataGid;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * TransOrderLineDetail consists of one or more TransOrderLine elements.
 * 
 * <p>Java class for TransOrderLineDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransOrderLineDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransOrderLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderLineType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransOrderLineDetailType", propOrder = {
    "transOrderLine"
})
public class TransOrderLineDetailType {

    @XmlElement(name = "TransOrderLine", required = true)
    protected List<TransOrderLineType> transOrderLine;

    /**
     * Gets the value of the transOrderLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transOrderLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransOrderLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransOrderLineType }
     * 
     * 
     */
    public List<TransOrderLineType> getTransOrderLine() {
        if (transOrderLine == null) {
            transOrderLine = new ArrayList<TransOrderLineType>();
        }
        return this.transOrderLine;
    }

}

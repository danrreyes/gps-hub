
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Provides an alternative for updating actual quantites for a Shipment by referencing the order ship unit.
 *             The ShipModViaOrderSUMatch element provides the fields that are used to match the affected ship units.
 *             The quantities on the ship units that are matched are updated according to the new value. Note that the action may
 *             delete existing ship units or add new ship units as needed to apply the change.
 *             Warning: Using this feature may impact or overwrite any changes made via the Shipment.ShipUnit element.
 * 
 *             The TransactionCode is used to specify if this is a modification, or the order line or ship unit should be added or removed.
 *          
 * 
 * <p>Java class for ShipmentModViaOrderSUType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentModViaOrderSUType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipModViaOrderSUMatch" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipModViaOrderSUMatchType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="AffectsCurrentLegOnly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LengthWidthHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/&gt;
 *         &lt;element name="UnitNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="UnitWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentModViaOrderSUType", propOrder = {
    "shipModViaOrderSUMatch",
    "transactionCode",
    "affectsCurrentLegOnly",
    "shipUnitCount",
    "lengthWidthHeight",
    "unitNetWeightVolume",
    "unitWeightVolume"
})
public class ShipmentModViaOrderSUType {

    @XmlElement(name = "ShipModViaOrderSUMatch", required = true)
    protected ShipModViaOrderSUMatchType shipModViaOrderSUMatch;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "AffectsCurrentLegOnly")
    protected String affectsCurrentLegOnly;
    @XmlElement(name = "ShipUnitCount")
    protected String shipUnitCount;
    @XmlElement(name = "LengthWidthHeight")
    protected LengthWidthHeightType lengthWidthHeight;
    @XmlElement(name = "UnitNetWeightVolume")
    protected WeightVolumeType unitNetWeightVolume;
    @XmlElement(name = "UnitWeightVolume")
    protected WeightVolumeType unitWeightVolume;

    /**
     * Gets the value of the shipModViaOrderSUMatch property.
     * 
     * @return
     *     possible object is
     *     {@link ShipModViaOrderSUMatchType }
     *     
     */
    public ShipModViaOrderSUMatchType getShipModViaOrderSUMatch() {
        return shipModViaOrderSUMatch;
    }

    /**
     * Sets the value of the shipModViaOrderSUMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipModViaOrderSUMatchType }
     *     
     */
    public void setShipModViaOrderSUMatch(ShipModViaOrderSUMatchType value) {
        this.shipModViaOrderSUMatch = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the affectsCurrentLegOnly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAffectsCurrentLegOnly() {
        return affectsCurrentLegOnly;
    }

    /**
     * Sets the value of the affectsCurrentLegOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAffectsCurrentLegOnly(String value) {
        this.affectsCurrentLegOnly = value;
    }

    /**
     * Gets the value of the shipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitCount() {
        return shipUnitCount;
    }

    /**
     * Sets the value of the shipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitCount(String value) {
        this.shipUnitCount = value;
    }

    /**
     * Gets the value of the lengthWidthHeight property.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getLengthWidthHeight() {
        return lengthWidthHeight;
    }

    /**
     * Sets the value of the lengthWidthHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setLengthWidthHeight(LengthWidthHeightType value) {
        this.lengthWidthHeight = value;
    }

    /**
     * Gets the value of the unitNetWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getUnitNetWeightVolume() {
        return unitNetWeightVolume;
    }

    /**
     * Sets the value of the unitNetWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setUnitNetWeightVolume(WeightVolumeType value) {
        this.unitNetWeightVolume = value;
    }

    /**
     * Gets the value of the unitWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getUnitWeightVolume() {
        return unitWeightVolume;
    }

    /**
     * Sets the value of the unitWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setUnitWeightVolume(WeightVolumeType value) {
        this.unitWeightVolume = value;
    }

}

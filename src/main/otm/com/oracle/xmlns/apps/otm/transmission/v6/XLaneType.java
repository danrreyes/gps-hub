
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             An XLane specifies a link from a source node to a destination node.
 *             The source and destination nodes may specify either vague or specific geography.
 *             For example, a source could be an exact location, or an entire state.
 *          
 * 
 * <p>Java class for XLaneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XLaneType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="XLaneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="XLaneSource" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLXLaneNodeType"/&gt;
 *         &lt;element name="XLaneDest" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLXLaneNodeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XLaneType", propOrder = {
    "sendReason",
    "xLaneGid",
    "transactionCode",
    "xLaneSource",
    "xLaneDest"
})
public class XLaneType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "XLaneGid", required = true)
    protected GLogXMLGidType xLaneGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "XLaneSource", required = true)
    protected GLogXMLXLaneNodeType xLaneSource;
    @XmlElement(name = "XLaneDest", required = true)
    protected GLogXMLXLaneNodeType xLaneDest;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the xLaneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getXLaneGid() {
        return xLaneGid;
    }

    /**
     * Sets the value of the xLaneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setXLaneGid(GLogXMLGidType value) {
        this.xLaneGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the xLaneSource property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public GLogXMLXLaneNodeType getXLaneSource() {
        return xLaneSource;
    }

    /**
     * Sets the value of the xLaneSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public void setXLaneSource(GLogXMLXLaneNodeType value) {
        this.xLaneSource = value;
    }

    /**
     * Gets the value of the xLaneDest property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public GLogXMLXLaneNodeType getXLaneDest() {
        return xLaneDest;
    }

    /**
     * Sets the value of the xLaneDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLXLaneNodeType }
     *     
     */
    public void setXLaneDest(GLogXMLXLaneNodeType value) {
        this.xLaneDest = value;
    }

}

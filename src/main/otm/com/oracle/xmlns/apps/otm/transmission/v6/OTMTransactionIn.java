
package com.oracle.xmlns.apps.otm.transmission.v6;

import com.oracle.xmlns.apps.gtm.transmission.v6.GtmStructureType;
import com.oracle.xmlns.apps.gtm.transmission.v6.ServiceRequestType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OTMTransactionIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OTMTransactionIn"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransactionType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OTMTransactionIn")
@XmlSeeAlso({
    ContactGroupType.class,
    ItineraryType.class,
    MileageType.class,
    RouteTemplateType.class,
    ServiceTimeType.class,
    UserType.class,
    TransOrderLinkType.class,
    TransOrderStatusType.class,
    ReleaseInstructionType.class,
    ActualShipmentType.class,
    SShipUnitType.class,
    ShipmentLinkType.class,
    ShipStopType.class,
    TenderResponseType.class,
    DriverCalendarEventType.class,
    TopicType.class,
    GenericStatusUpdateType.class,
    RemoteQueryType.class,
    TransactionAckType.class,
    GtmStructureType.class,
    ServiceRequestType.class
})
public abstract class OTMTransactionIn
    extends GLogXMLTransactionType
{


}

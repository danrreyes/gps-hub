
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipmentGroupRefnum is an alternate method for identifying a Shipment Group. It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for ShipmentGroupRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentGroupRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipmentGroupRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipmentGroupRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentGroupRefnumType", propOrder = {
    "shipmentGroupRefnumQualifierGid",
    "shipmentGroupRefnumValue"
})
public class ShipmentGroupRefnumType {

    @XmlElement(name = "ShipmentGroupRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipmentGroupRefnumQualifierGid;
    @XmlElement(name = "ShipmentGroupRefnumValue", required = true)
    protected String shipmentGroupRefnumValue;

    /**
     * Gets the value of the shipmentGroupRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGroupRefnumQualifierGid() {
        return shipmentGroupRefnumQualifierGid;
    }

    /**
     * Sets the value of the shipmentGroupRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGroupRefnumQualifierGid(GLogXMLGidType value) {
        this.shipmentGroupRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the shipmentGroupRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentGroupRefnumValue() {
        return shipmentGroupRefnumValue;
    }

    /**
     * Sets the value of the shipmentGroupRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentGroupRefnumValue(String value) {
        this.shipmentGroupRefnumValue = value;
    }

}

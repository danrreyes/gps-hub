
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * The ShipmentHeader information includes the shipment global identifier, transaction code,
 *             and others elements.
 * 
 *             The RateServiceGid element is outbound only.
 * 
 *             The SourceLocationRef element and DestinationLocationRef element are outbound only.
 *          
 * 
 * <p>Java class for ShipmentHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="IntCommand" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntCommandType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentModViaOrderLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentModViaOrderLineType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentModViaOrderSU" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentModViaOrderSUType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipRouteExecutionInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipRouteExecutionInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ProcessingCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InternalShipmentStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PlannedShipmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PlannedShipmentInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsServiceProviderFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsTenderContactFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsRateOfferingFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsRateGeoFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GlobalCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/&gt;
 *         &lt;element name="TotalPlannedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="TotalActualCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentCostType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentInfCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentInfCostType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentInvoiceCostInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentInvoiceCostInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Accessorial" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccessorialType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsCostFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsSpotCosted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsServiceTimeFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AirRailRouteCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsRecalcTotals" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="TotalNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="TotalShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EndDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialTermsType" minOccurs="0"/&gt;
 *         &lt;element name="InsuranceInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InsuranceInfoType" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Tariff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TariffType" minOccurs="0"/&gt;
 *         &lt;element name="IsFixedDistance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoadedDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="UnloadedDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="StopCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOrderReleases" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalNumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentRefUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsTemperatureControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EarliestStartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LatestStartDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="VesselGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="VesselName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="SourceLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="DestinationLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="DestLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfLoadLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfDisLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="SourcePierLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="DestPierLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="BookingInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookingInfoType" minOccurs="0"/&gt;
 *         &lt;element name="DelivServprovGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OrigServprovGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EquipMarks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipMarksType" minOccurs="0"/&gt;
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SecondaryDriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PowerUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentInfeasibility" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="FeasibilityCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EmergPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsFixedVoyageFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentHeaderType", propOrder = {
    "sendReason",
    "intSavedQuery",
    "shipmentGid",
    "shipmentName",
    "shipmentRefnum",
    "transactionCode",
    "replaceChildren",
    "intCommand",
    "shipmentModViaOrderLine",
    "shipmentModViaOrderSU",
    "shipRouteExecutionInfo",
    "processingCodeGid",
    "internalShipmentStatus",
    "plannedShipmentInfo",
    "serviceProviderGid",
    "serviceProviderAlias",
    "isServiceProviderFixed",
    "contactGid",
    "isTenderContactFixed",
    "rateOfferingGid",
    "isRateOfferingFixed",
    "rateGeoGid",
    "isRateGeoFixed",
    "globalCurrencyCode",
    "exchangeRateInfo",
    "totalPlannedCost",
    "totalActualCost",
    "totalWeightedCost",
    "shipmentCost",
    "shipmentInfCost",
    "shipmentInvoiceCostInfo",
    "accessorial",
    "isCostFixed",
    "isSpotCosted",
    "isServiceTimeFixed",
    "iTransactionNo",
    "isHazardous",
    "rateServiceGid",
    "rateServiceProfileGid",
    "transportModeGid",
    "airRailRouteCode",
    "isRecalcTotals",
    "totalWeightVolume",
    "totalNetWeightVolume",
    "totalShipUnitCount",
    "totalPackagedItemSpecCount",
    "totalPackagedItemCount",
    "startDt",
    "endDt",
    "commercialTerms",
    "insuranceInfo",
    "involvedParty",
    "accessorialCodeGid",
    "shipmentSpecialService",
    "remark",
    "tariff",
    "isFixedDistance",
    "loadedDistance",
    "unloadedDistance",
    "stopCount",
    "numOrderReleases",
    "totalNumReferenceUnits",
    "equipmentRefUnitGid",
    "isTemperatureControl",
    "earliestStartDt",
    "latestStartDt",
    "voyageGid",
    "vesselGid",
    "vesselName",
    "sourceLocationRef",
    "sourceLocOverrideRef",
    "destinationLocationRef",
    "destLocOverrideRef",
    "portOfLoadLocationRef",
    "portOfLoadLocOverrideRef",
    "portOfDisLocationRef",
    "portOfDisLocOverrideRef",
    "sourcePierLocationRef",
    "destPierLocationRef",
    "bookingInfo",
    "delivServprovGid",
    "origServprovGid",
    "routeCode",
    "equipMarks",
    "text",
    "driverGid",
    "secondaryDriverGid",
    "powerUnitGid",
    "shipmentInfeasibility",
    "planningParamSetGid",
    "emergPhoneNum",
    "isFixedVoyageFlag",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class ShipmentHeaderType {

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ShipmentName")
    protected String shipmentName;
    @XmlElement(name = "ShipmentRefnum")
    protected List<ShipmentRefnumType> shipmentRefnum;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IntCommand")
    protected List<IntCommandType> intCommand;
    @XmlElement(name = "ShipmentModViaOrderLine")
    protected List<ShipmentModViaOrderLineType> shipmentModViaOrderLine;
    @XmlElement(name = "ShipmentModViaOrderSU")
    protected List<ShipmentModViaOrderSUType> shipmentModViaOrderSU;
    @XmlElement(name = "ShipRouteExecutionInfo")
    protected ShipRouteExecutionInfoType shipRouteExecutionInfo;
    @XmlElement(name = "ProcessingCodeGid")
    protected GLogXMLGidType processingCodeGid;
    @XmlElement(name = "InternalShipmentStatus")
    protected List<StatusType> internalShipmentStatus;
    @XmlElement(name = "PlannedShipmentInfo")
    protected PlannedShipmentInfoType plannedShipmentInfo;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "ServiceProviderAlias")
    protected List<ServiceProviderAliasType> serviceProviderAlias;
    @XmlElement(name = "IsServiceProviderFixed")
    protected String isServiceProviderFixed;
    @XmlElement(name = "ContactGid")
    protected GLogXMLGidType contactGid;
    @XmlElement(name = "IsTenderContactFixed")
    protected String isTenderContactFixed;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "IsRateOfferingFixed")
    protected String isRateOfferingFixed;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "IsRateGeoFixed")
    protected String isRateGeoFixed;
    @XmlElement(name = "GlobalCurrencyCode")
    protected String globalCurrencyCode;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "TotalPlannedCost")
    protected GLogXMLFinancialAmountType totalPlannedCost;
    @XmlElement(name = "TotalActualCost")
    protected GLogXMLFinancialAmountType totalActualCost;
    @XmlElement(name = "TotalWeightedCost")
    protected GLogXMLFinancialAmountType totalWeightedCost;
    @XmlElement(name = "ShipmentCost")
    protected List<ShipmentCostType> shipmentCost;
    @XmlElement(name = "ShipmentInfCost")
    protected List<ShipmentInfCostType> shipmentInfCost;
    @XmlElement(name = "ShipmentInvoiceCostInfo")
    protected ShipmentInvoiceCostInfoType shipmentInvoiceCostInfo;
    @XmlElement(name = "Accessorial")
    protected List<AccessorialType> accessorial;
    @XmlElement(name = "IsCostFixed")
    protected String isCostFixed;
    @XmlElement(name = "IsSpotCosted")
    protected String isSpotCosted;
    @XmlElement(name = "IsServiceTimeFixed")
    protected String isServiceTimeFixed;
    @XmlElement(name = "ITransactionNo")
    protected String iTransactionNo;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "AirRailRouteCode")
    protected String airRailRouteCode;
    @XmlElement(name = "IsRecalcTotals")
    protected String isRecalcTotals;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TotalNetWeightVolume")
    protected WeightVolumeType totalNetWeightVolume;
    @XmlElement(name = "TotalShipUnitCount")
    protected String totalShipUnitCount;
    @XmlElement(name = "TotalPackagedItemSpecCount")
    protected String totalPackagedItemSpecCount;
    @XmlElement(name = "TotalPackagedItemCount")
    protected String totalPackagedItemCount;
    @XmlElement(name = "StartDt")
    protected GLogDateTimeType startDt;
    @XmlElement(name = "EndDt")
    protected GLogDateTimeType endDt;
    @XmlElement(name = "CommercialTerms")
    protected CommercialTermsType commercialTerms;
    @XmlElement(name = "InsuranceInfo")
    protected InsuranceInfoType insuranceInfo;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "ShipmentSpecialService")
    protected List<ShipmentSpecialServiceType> shipmentSpecialService;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Tariff")
    protected TariffType tariff;
    @XmlElement(name = "IsFixedDistance")
    protected String isFixedDistance;
    @XmlElement(name = "LoadedDistance")
    protected GLogXMLDistanceType loadedDistance;
    @XmlElement(name = "UnloadedDistance")
    protected GLogXMLDistanceType unloadedDistance;
    @XmlElement(name = "StopCount")
    protected String stopCount;
    @XmlElement(name = "NumOrderReleases")
    protected String numOrderReleases;
    @XmlElement(name = "TotalNumReferenceUnits")
    protected String totalNumReferenceUnits;
    @XmlElement(name = "EquipmentRefUnitGid")
    protected GLogXMLGidType equipmentRefUnitGid;
    @XmlElement(name = "IsTemperatureControl")
    protected String isTemperatureControl;
    @XmlElement(name = "EarliestStartDt")
    protected GLogDateTimeType earliestStartDt;
    @XmlElement(name = "LatestStartDt")
    protected GLogDateTimeType latestStartDt;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "VesselGid")
    protected GLogXMLGidType vesselGid;
    @XmlElement(name = "VesselName")
    protected String vesselName;
    @XmlElement(name = "SourceLocationRef")
    protected GLogXMLLocRefType sourceLocationRef;
    @XmlElement(name = "SourceLocOverrideRef")
    protected GLogXMLLocOverrideRefType sourceLocOverrideRef;
    @XmlElement(name = "DestinationLocationRef")
    protected GLogXMLLocRefType destinationLocationRef;
    @XmlElement(name = "DestLocOverrideRef")
    protected GLogXMLLocOverrideRefType destLocOverrideRef;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfLoadLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfLoadLocOverrideRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "PortOfDisLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfDisLocOverrideRef;
    @XmlElement(name = "SourcePierLocationRef")
    protected GLogXMLLocRefType sourcePierLocationRef;
    @XmlElement(name = "DestPierLocationRef")
    protected GLogXMLLocRefType destPierLocationRef;
    @XmlElement(name = "BookingInfo")
    protected BookingInfoType bookingInfo;
    @XmlElement(name = "DelivServprovGid")
    protected GLogXMLGidType delivServprovGid;
    @XmlElement(name = "OrigServprovGid")
    protected GLogXMLGidType origServprovGid;
    @XmlElement(name = "RouteCode")
    protected List<RouteCodeType> routeCode;
    @XmlElement(name = "EquipMarks")
    protected EquipMarksType equipMarks;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "DriverGid")
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "SecondaryDriverGid")
    protected GLogXMLGidType secondaryDriverGid;
    @XmlElement(name = "PowerUnitGid")
    protected GLogXMLGidType powerUnitGid;
    @XmlElement(name = "ShipmentInfeasibility")
    protected List<ShipmentHeaderType.ShipmentInfeasibility> shipmentInfeasibility;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "EmergPhoneNum")
    protected String emergPhoneNum;
    @XmlElement(name = "IsFixedVoyageFlag")
    protected String isFixedVoyageFlag;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the shipmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentName() {
        return shipmentName;
    }

    /**
     * Sets the value of the shipmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentName(String value) {
        this.shipmentName = value;
    }

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentRefnumType }
     * 
     * 
     */
    public List<ShipmentRefnumType> getShipmentRefnum() {
        if (shipmentRefnum == null) {
            shipmentRefnum = new ArrayList<ShipmentRefnumType>();
        }
        return this.shipmentRefnum;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the intCommand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the intCommand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntCommand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntCommandType }
     * 
     * 
     */
    public List<IntCommandType> getIntCommand() {
        if (intCommand == null) {
            intCommand = new ArrayList<IntCommandType>();
        }
        return this.intCommand;
    }

    /**
     * Gets the value of the shipmentModViaOrderLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentModViaOrderLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentModViaOrderLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentModViaOrderLineType }
     * 
     * 
     */
    public List<ShipmentModViaOrderLineType> getShipmentModViaOrderLine() {
        if (shipmentModViaOrderLine == null) {
            shipmentModViaOrderLine = new ArrayList<ShipmentModViaOrderLineType>();
        }
        return this.shipmentModViaOrderLine;
    }

    /**
     * Gets the value of the shipmentModViaOrderSU property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentModViaOrderSU property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentModViaOrderSU().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentModViaOrderSUType }
     * 
     * 
     */
    public List<ShipmentModViaOrderSUType> getShipmentModViaOrderSU() {
        if (shipmentModViaOrderSU == null) {
            shipmentModViaOrderSU = new ArrayList<ShipmentModViaOrderSUType>();
        }
        return this.shipmentModViaOrderSU;
    }

    /**
     * Gets the value of the shipRouteExecutionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ShipRouteExecutionInfoType }
     *     
     */
    public ShipRouteExecutionInfoType getShipRouteExecutionInfo() {
        return shipRouteExecutionInfo;
    }

    /**
     * Sets the value of the shipRouteExecutionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipRouteExecutionInfoType }
     *     
     */
    public void setShipRouteExecutionInfo(ShipRouteExecutionInfoType value) {
        this.shipRouteExecutionInfo = value;
    }

    /**
     * Gets the value of the processingCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getProcessingCodeGid() {
        return processingCodeGid;
    }

    /**
     * Sets the value of the processingCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setProcessingCodeGid(GLogXMLGidType value) {
        this.processingCodeGid = value;
    }

    /**
     * Gets the value of the internalShipmentStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the internalShipmentStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInternalShipmentStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getInternalShipmentStatus() {
        if (internalShipmentStatus == null) {
            internalShipmentStatus = new ArrayList<StatusType>();
        }
        return this.internalShipmentStatus;
    }

    /**
     * Gets the value of the plannedShipmentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link PlannedShipmentInfoType }
     *     
     */
    public PlannedShipmentInfoType getPlannedShipmentInfo() {
        return plannedShipmentInfo;
    }

    /**
     * Sets the value of the plannedShipmentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlannedShipmentInfoType }
     *     
     */
    public void setPlannedShipmentInfo(PlannedShipmentInfoType value) {
        this.plannedShipmentInfo = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the serviceProviderAlias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the serviceProviderAlias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceProviderAlias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderAliasType }
     * 
     * 
     */
    public List<ServiceProviderAliasType> getServiceProviderAlias() {
        if (serviceProviderAlias == null) {
            serviceProviderAlias = new ArrayList<ServiceProviderAliasType>();
        }
        return this.serviceProviderAlias;
    }

    /**
     * Gets the value of the isServiceProviderFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsServiceProviderFixed() {
        return isServiceProviderFixed;
    }

    /**
     * Sets the value of the isServiceProviderFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsServiceProviderFixed(String value) {
        this.isServiceProviderFixed = value;
    }

    /**
     * Gets the value of the contactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Sets the value of the contactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

    /**
     * Gets the value of the isTenderContactFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTenderContactFixed() {
        return isTenderContactFixed;
    }

    /**
     * Sets the value of the isTenderContactFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTenderContactFixed(String value) {
        this.isTenderContactFixed = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the isRateOfferingFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRateOfferingFixed() {
        return isRateOfferingFixed;
    }

    /**
     * Sets the value of the isRateOfferingFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRateOfferingFixed(String value) {
        this.isRateOfferingFixed = value;
    }

    /**
     * Gets the value of the rateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Sets the value of the rateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Gets the value of the isRateGeoFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRateGeoFixed() {
        return isRateGeoFixed;
    }

    /**
     * Sets the value of the isRateGeoFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRateGeoFixed(String value) {
        this.isRateGeoFixed = value;
    }

    /**
     * Gets the value of the globalCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    /**
     * Sets the value of the globalCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalCurrencyCode(String value) {
        this.globalCurrencyCode = value;
    }

    /**
     * Gets the value of the exchangeRateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Sets the value of the exchangeRateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Gets the value of the totalPlannedCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalPlannedCost() {
        return totalPlannedCost;
    }

    /**
     * Sets the value of the totalPlannedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalPlannedCost(GLogXMLFinancialAmountType value) {
        this.totalPlannedCost = value;
    }

    /**
     * Gets the value of the totalActualCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalActualCost() {
        return totalActualCost;
    }

    /**
     * Sets the value of the totalActualCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalActualCost(GLogXMLFinancialAmountType value) {
        this.totalActualCost = value;
    }

    /**
     * Gets the value of the totalWeightedCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalWeightedCost() {
        return totalWeightedCost;
    }

    /**
     * Sets the value of the totalWeightedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalWeightedCost(GLogXMLFinancialAmountType value) {
        this.totalWeightedCost = value;
    }

    /**
     * Gets the value of the shipmentCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentCostType }
     * 
     * 
     */
    public List<ShipmentCostType> getShipmentCost() {
        if (shipmentCost == null) {
            shipmentCost = new ArrayList<ShipmentCostType>();
        }
        return this.shipmentCost;
    }

    /**
     * Gets the value of the shipmentInfCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentInfCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentInfCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentInfCostType }
     * 
     * 
     */
    public List<ShipmentInfCostType> getShipmentInfCost() {
        if (shipmentInfCost == null) {
            shipmentInfCost = new ArrayList<ShipmentInfCostType>();
        }
        return this.shipmentInfCost;
    }

    /**
     * Gets the value of the shipmentInvoiceCostInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentInvoiceCostInfoType }
     *     
     */
    public ShipmentInvoiceCostInfoType getShipmentInvoiceCostInfo() {
        return shipmentInvoiceCostInfo;
    }

    /**
     * Sets the value of the shipmentInvoiceCostInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentInvoiceCostInfoType }
     *     
     */
    public void setShipmentInvoiceCostInfo(ShipmentInvoiceCostInfoType value) {
        this.shipmentInvoiceCostInfo = value;
    }

    /**
     * Gets the value of the accessorial property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the accessorial property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorial().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessorialType }
     * 
     * 
     */
    public List<AccessorialType> getAccessorial() {
        if (accessorial == null) {
            accessorial = new ArrayList<AccessorialType>();
        }
        return this.accessorial;
    }

    /**
     * Gets the value of the isCostFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCostFixed() {
        return isCostFixed;
    }

    /**
     * Sets the value of the isCostFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCostFixed(String value) {
        this.isCostFixed = value;
    }

    /**
     * Gets the value of the isSpotCosted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSpotCosted() {
        return isSpotCosted;
    }

    /**
     * Sets the value of the isSpotCosted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSpotCosted(String value) {
        this.isSpotCosted = value;
    }

    /**
     * Gets the value of the isServiceTimeFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsServiceTimeFixed() {
        return isServiceTimeFixed;
    }

    /**
     * Sets the value of the isServiceTimeFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsServiceTimeFixed(String value) {
        this.isServiceTimeFixed = value;
    }

    /**
     * Gets the value of the iTransactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Sets the value of the iTransactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Gets the value of the isHazardous property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Sets the value of the isHazardous property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Gets the value of the rateServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Sets the value of the rateServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Gets the value of the rateServiceProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Sets the value of the rateServiceProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Sets the value of the transportModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Gets the value of the airRailRouteCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAirRailRouteCode() {
        return airRailRouteCode;
    }

    /**
     * Sets the value of the airRailRouteCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAirRailRouteCode(String value) {
        this.airRailRouteCode = value;
    }

    /**
     * Gets the value of the isRecalcTotals property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRecalcTotals() {
        return isRecalcTotals;
    }

    /**
     * Sets the value of the isRecalcTotals property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRecalcTotals(String value) {
        this.isRecalcTotals = value;
    }

    /**
     * Gets the value of the totalWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Sets the value of the totalWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Gets the value of the totalNetWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getTotalNetWeightVolume() {
        return totalNetWeightVolume;
    }

    /**
     * Sets the value of the totalNetWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setTotalNetWeightVolume(WeightVolumeType value) {
        this.totalNetWeightVolume = value;
    }

    /**
     * Gets the value of the totalShipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalShipUnitCount() {
        return totalShipUnitCount;
    }

    /**
     * Sets the value of the totalShipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalShipUnitCount(String value) {
        this.totalShipUnitCount = value;
    }

    /**
     * Gets the value of the totalPackagedItemSpecCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemSpecCount() {
        return totalPackagedItemSpecCount;
    }

    /**
     * Sets the value of the totalPackagedItemSpecCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemSpecCount(String value) {
        this.totalPackagedItemSpecCount = value;
    }

    /**
     * Gets the value of the totalPackagedItemCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemCount() {
        return totalPackagedItemCount;
    }

    /**
     * Sets the value of the totalPackagedItemCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemCount(String value) {
        this.totalPackagedItemCount = value;
    }

    /**
     * Gets the value of the startDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartDt() {
        return startDt;
    }

    /**
     * Sets the value of the startDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartDt(GLogDateTimeType value) {
        this.startDt = value;
    }

    /**
     * Gets the value of the endDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDt() {
        return endDt;
    }

    /**
     * Sets the value of the endDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDt(GLogDateTimeType value) {
        this.endDt = value;
    }

    /**
     * Gets the value of the commercialTerms property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialTermsType }
     *     
     */
    public CommercialTermsType getCommercialTerms() {
        return commercialTerms;
    }

    /**
     * Sets the value of the commercialTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialTermsType }
     *     
     */
    public void setCommercialTerms(CommercialTermsType value) {
        this.commercialTerms = value;
    }

    /**
     * Gets the value of the insuranceInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceInfoType }
     *     
     */
    public InsuranceInfoType getInsuranceInfo() {
        return insuranceInfo;
    }

    /**
     * Sets the value of the insuranceInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceInfoType }
     *     
     */
    public void setInsuranceInfo(InsuranceInfoType value) {
        this.insuranceInfo = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the shipmentSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentSpecialServiceType }
     * 
     * 
     */
    public List<ShipmentSpecialServiceType> getShipmentSpecialService() {
        if (shipmentSpecialService == null) {
            shipmentSpecialService = new ArrayList<ShipmentSpecialServiceType>();
        }
        return this.shipmentSpecialService;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the tariff property.
     * 
     * @return
     *     possible object is
     *     {@link TariffType }
     *     
     */
    public TariffType getTariff() {
        return tariff;
    }

    /**
     * Sets the value of the tariff property.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffType }
     *     
     */
    public void setTariff(TariffType value) {
        this.tariff = value;
    }

    /**
     * Gets the value of the isFixedDistance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedDistance() {
        return isFixedDistance;
    }

    /**
     * Sets the value of the isFixedDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedDistance(String value) {
        this.isFixedDistance = value;
    }

    /**
     * Gets the value of the loadedDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getLoadedDistance() {
        return loadedDistance;
    }

    /**
     * Sets the value of the loadedDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setLoadedDistance(GLogXMLDistanceType value) {
        this.loadedDistance = value;
    }

    /**
     * Gets the value of the unloadedDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getUnloadedDistance() {
        return unloadedDistance;
    }

    /**
     * Sets the value of the unloadedDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setUnloadedDistance(GLogXMLDistanceType value) {
        this.unloadedDistance = value;
    }

    /**
     * Gets the value of the stopCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopCount() {
        return stopCount;
    }

    /**
     * Sets the value of the stopCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopCount(String value) {
        this.stopCount = value;
    }

    /**
     * Gets the value of the numOrderReleases property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOrderReleases() {
        return numOrderReleases;
    }

    /**
     * Sets the value of the numOrderReleases property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOrderReleases(String value) {
        this.numOrderReleases = value;
    }

    /**
     * Gets the value of the totalNumReferenceUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNumReferenceUnits() {
        return totalNumReferenceUnits;
    }

    /**
     * Sets the value of the totalNumReferenceUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNumReferenceUnits(String value) {
        this.totalNumReferenceUnits = value;
    }

    /**
     * Gets the value of the equipmentRefUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentRefUnitGid() {
        return equipmentRefUnitGid;
    }

    /**
     * Sets the value of the equipmentRefUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentRefUnitGid(GLogXMLGidType value) {
        this.equipmentRefUnitGid = value;
    }

    /**
     * Gets the value of the isTemperatureControl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemperatureControl() {
        return isTemperatureControl;
    }

    /**
     * Sets the value of the isTemperatureControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemperatureControl(String value) {
        this.isTemperatureControl = value;
    }

    /**
     * Gets the value of the earliestStartDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarliestStartDt() {
        return earliestStartDt;
    }

    /**
     * Sets the value of the earliestStartDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarliestStartDt(GLogDateTimeType value) {
        this.earliestStartDt = value;
    }

    /**
     * Gets the value of the latestStartDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLatestStartDt() {
        return latestStartDt;
    }

    /**
     * Sets the value of the latestStartDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLatestStartDt(GLogDateTimeType value) {
        this.latestStartDt = value;
    }

    /**
     * Gets the value of the voyageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Sets the value of the voyageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Gets the value of the vesselGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVesselGid() {
        return vesselGid;
    }

    /**
     * Sets the value of the vesselGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVesselGid(GLogXMLGidType value) {
        this.vesselGid = value;
    }

    /**
     * Gets the value of the vesselName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVesselName() {
        return vesselName;
    }

    /**
     * Sets the value of the vesselName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVesselName(String value) {
        this.vesselName = value;
    }

    /**
     * Gets the value of the sourceLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourceLocationRef() {
        return sourceLocationRef;
    }

    /**
     * Sets the value of the sourceLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourceLocationRef(GLogXMLLocRefType value) {
        this.sourceLocationRef = value;
    }

    /**
     * Gets the value of the sourceLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getSourceLocOverrideRef() {
        return sourceLocOverrideRef;
    }

    /**
     * Sets the value of the sourceLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setSourceLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.sourceLocOverrideRef = value;
    }

    /**
     * Gets the value of the destinationLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestinationLocationRef() {
        return destinationLocationRef;
    }

    /**
     * Sets the value of the destinationLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestinationLocationRef(GLogXMLLocRefType value) {
        this.destinationLocationRef = value;
    }

    /**
     * Gets the value of the destLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getDestLocOverrideRef() {
        return destLocOverrideRef;
    }

    /**
     * Sets the value of the destLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setDestLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.destLocOverrideRef = value;
    }

    /**
     * Gets the value of the portOfLoadLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Sets the value of the portOfLoadLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Gets the value of the portOfLoadLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfLoadLocOverrideRef() {
        return portOfLoadLocOverrideRef;
    }

    /**
     * Sets the value of the portOfLoadLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfLoadLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfLoadLocOverrideRef = value;
    }

    /**
     * Gets the value of the portOfDisLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Sets the value of the portOfDisLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Gets the value of the portOfDisLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfDisLocOverrideRef() {
        return portOfDisLocOverrideRef;
    }

    /**
     * Sets the value of the portOfDisLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfDisLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfDisLocOverrideRef = value;
    }

    /**
     * Gets the value of the sourcePierLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourcePierLocationRef() {
        return sourcePierLocationRef;
    }

    /**
     * Sets the value of the sourcePierLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourcePierLocationRef(GLogXMLLocRefType value) {
        this.sourcePierLocationRef = value;
    }

    /**
     * Gets the value of the destPierLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestPierLocationRef() {
        return destPierLocationRef;
    }

    /**
     * Sets the value of the destPierLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestPierLocationRef(GLogXMLLocRefType value) {
        this.destPierLocationRef = value;
    }

    /**
     * Gets the value of the bookingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link BookingInfoType }
     *     
     */
    public BookingInfoType getBookingInfo() {
        return bookingInfo;
    }

    /**
     * Sets the value of the bookingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BookingInfoType }
     *     
     */
    public void setBookingInfo(BookingInfoType value) {
        this.bookingInfo = value;
    }

    /**
     * Gets the value of the delivServprovGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivServprovGid() {
        return delivServprovGid;
    }

    /**
     * Sets the value of the delivServprovGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivServprovGid(GLogXMLGidType value) {
        this.delivServprovGid = value;
    }

    /**
     * Gets the value of the origServprovGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigServprovGid() {
        return origServprovGid;
    }

    /**
     * Sets the value of the origServprovGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigServprovGid(GLogXMLGidType value) {
        this.origServprovGid = value;
    }

    /**
     * Gets the value of the routeCode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the routeCode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteCode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteCodeType }
     * 
     * 
     */
    public List<RouteCodeType> getRouteCode() {
        if (routeCode == null) {
            routeCode = new ArrayList<RouteCodeType>();
        }
        return this.routeCode;
    }

    /**
     * Gets the value of the equipMarks property.
     * 
     * @return
     *     possible object is
     *     {@link EquipMarksType }
     *     
     */
    public EquipMarksType getEquipMarks() {
        return equipMarks;
    }

    /**
     * Sets the value of the equipMarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipMarksType }
     *     
     */
    public void setEquipMarks(EquipMarksType value) {
        this.equipMarks = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Gets the value of the driverGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Sets the value of the driverGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Gets the value of the secondaryDriverGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSecondaryDriverGid() {
        return secondaryDriverGid;
    }

    /**
     * Sets the value of the secondaryDriverGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSecondaryDriverGid(GLogXMLGidType value) {
        this.secondaryDriverGid = value;
    }

    /**
     * Gets the value of the powerUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPowerUnitGid() {
        return powerUnitGid;
    }

    /**
     * Sets the value of the powerUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPowerUnitGid(GLogXMLGidType value) {
        this.powerUnitGid = value;
    }

    /**
     * Gets the value of the shipmentInfeasibility property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentInfeasibility property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentInfeasibility().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentHeaderType.ShipmentInfeasibility }
     * 
     * 
     */
    public List<ShipmentHeaderType.ShipmentInfeasibility> getShipmentInfeasibility() {
        if (shipmentInfeasibility == null) {
            shipmentInfeasibility = new ArrayList<ShipmentHeaderType.ShipmentInfeasibility>();
        }
        return this.shipmentInfeasibility;
    }

    /**
     * Gets the value of the planningParamSetGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Sets the value of the planningParamSetGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Gets the value of the emergPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergPhoneNum() {
        return emergPhoneNum;
    }

    /**
     * Sets the value of the emergPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergPhoneNum(String value) {
        this.emergPhoneNum = value;
    }

    /**
     * Gets the value of the isFixedVoyageFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedVoyageFlag() {
        return isFixedVoyageFlag;
    }

    /**
     * Sets the value of the isFixedVoyageFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedVoyageFlag(String value) {
        this.isFixedVoyageFlag = value;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the flexFieldCurrencies property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Sets the value of the flexFieldCurrencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="FeasibilityCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "feasibilityCodeGid"
    })
    public static class ShipmentInfeasibility {

        @XmlElement(name = "FeasibilityCodeGid", required = true)
        protected GLogXMLGidType feasibilityCodeGid;

        /**
         * Gets the value of the feasibilityCodeGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getFeasibilityCodeGid() {
            return feasibilityCodeGid;
        }

        /**
         * Sets the value of the feasibilityCodeGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setFeasibilityCodeGid(GLogXMLGidType value) {
            this.feasibilityCodeGid = value;
        }

    }

}

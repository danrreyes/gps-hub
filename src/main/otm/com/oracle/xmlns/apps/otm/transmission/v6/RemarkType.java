
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Remark allows one to specify structured or unstructured remarks.
 *             Structured remarks may have a RemarkSequence,RemarkQualifierGid, and RemarkText.
 *             Unstructured remarks may have just RemarkText.
 *          
 * 
 * <p>Java class for RemarkType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemarkType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="RemarkSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RemarkQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RemarkLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RemarkText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemarkType", propOrder = {
    "transactionCode",
    "remarkSequence",
    "remarkQualifierGid",
    "remarkLevel",
    "remarkText"
})
public class RemarkType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "RemarkSequence")
    protected String remarkSequence;
    @XmlElement(name = "RemarkQualifierGid")
    protected GLogXMLGidType remarkQualifierGid;
    @XmlElement(name = "RemarkLevel")
    protected String remarkLevel;
    @XmlElement(name = "RemarkText", required = true)
    protected String remarkText;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the remarkSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarkSequence() {
        return remarkSequence;
    }

    /**
     * Sets the value of the remarkSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarkSequence(String value) {
        this.remarkSequence = value;
    }

    /**
     * Gets the value of the remarkQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRemarkQualifierGid() {
        return remarkQualifierGid;
    }

    /**
     * Sets the value of the remarkQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRemarkQualifierGid(GLogXMLGidType value) {
        this.remarkQualifierGid = value;
    }

    /**
     * Gets the value of the remarkLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarkLevel() {
        return remarkLevel;
    }

    /**
     * Sets the value of the remarkLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarkLevel(String value) {
        this.remarkLevel = value;
    }

    /**
     * Gets the value of the remarkText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarkText() {
        return remarkText;
    }

    /**
     * Sets the value of the remarkText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarkText(String value) {
        this.remarkText = value;
    }

}

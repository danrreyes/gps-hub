
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An HazmatItemRef is either a HazmatItem or an HazmatItemGid.
 * 
 * <p>Java class for HazmatItemRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HazmatItemRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="HazmatItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="HazmatItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatItemType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatItemRefType", propOrder = {
    "hazmatItemGid",
    "hazmatItem"
})
public class HazmatItemRefType {

    @XmlElement(name = "HazmatItemGid")
    protected GLogXMLGidType hazmatItemGid;
    @XmlElement(name = "HazmatItem")
    protected HazmatItemType hazmatItem;

    /**
     * Gets the value of the hazmatItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatItemGid() {
        return hazmatItemGid;
    }

    /**
     * Sets the value of the hazmatItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatItemGid(GLogXMLGidType value) {
        this.hazmatItemGid = value;
    }

    /**
     * Gets the value of the hazmatItem property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType }
     *     
     */
    public HazmatItemType getHazmatItem() {
        return hazmatItem;
    }

    /**
     * Sets the value of the hazmatItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType }
     *     
     */
    public void setHazmatItem(HazmatItemType value) {
        this.hazmatItem = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * MilageAddress is a grouping of address fields similar to the normal address, but with the address lines removed.
 *             The RailSPLCGid and RailStationCodeGid elements are used in the RIQQuery interface.
 *          
 * 
 * <p>Java class for MileageAddressType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MileageAddressType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Zone1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zone2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zone3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Zone4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="GeoHierarchyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CountyQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RailSPLCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RailStationCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageAddressType", propOrder = {
    "city",
    "provinceCode",
    "postalCode",
    "countryCode3Gid",
    "zone1",
    "zone2",
    "zone3",
    "zone4",
    "locationGid",
    "geoHierarchyGid",
    "countyQualifier",
    "railSPLCGid",
    "railStationCodeGid"
})
public class MileageAddressType {

    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "ProvinceCode")
    protected String provinceCode;
    @XmlElement(name = "PostalCode")
    protected String postalCode;
    @XmlElement(name = "CountryCode3Gid")
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "Zone1")
    protected String zone1;
    @XmlElement(name = "Zone2")
    protected String zone2;
    @XmlElement(name = "Zone3")
    protected String zone3;
    @XmlElement(name = "Zone4")
    protected String zone4;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "GeoHierarchyGid")
    protected GLogXMLGidType geoHierarchyGid;
    @XmlElement(name = "CountyQualifier")
    protected String countyQualifier;
    @XmlElement(name = "RailSPLCGid")
    protected GLogXMLGidType railSPLCGid;
    @XmlElement(name = "RailStationCodeGid")
    protected GLogXMLGidType railStationCodeGid;

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the provinceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * Sets the value of the provinceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvinceCode(String value) {
        this.provinceCode = value;
    }

    /**
     * Gets the value of the postalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the value of the postalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Gets the value of the countryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Sets the value of the countryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Gets the value of the zone1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone1() {
        return zone1;
    }

    /**
     * Sets the value of the zone1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone1(String value) {
        this.zone1 = value;
    }

    /**
     * Gets the value of the zone2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone2() {
        return zone2;
    }

    /**
     * Sets the value of the zone2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone2(String value) {
        this.zone2 = value;
    }

    /**
     * Gets the value of the zone3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone3() {
        return zone3;
    }

    /**
     * Sets the value of the zone3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone3(String value) {
        this.zone3 = value;
    }

    /**
     * Gets the value of the zone4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZone4() {
        return zone4;
    }

    /**
     * Sets the value of the zone4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZone4(String value) {
        this.zone4 = value;
    }

    /**
     * Gets the value of the locationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Sets the value of the locationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Gets the value of the geoHierarchyGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGeoHierarchyGid() {
        return geoHierarchyGid;
    }

    /**
     * Sets the value of the geoHierarchyGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGeoHierarchyGid(GLogXMLGidType value) {
        this.geoHierarchyGid = value;
    }

    /**
     * Gets the value of the countyQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountyQualifier() {
        return countyQualifier;
    }

    /**
     * Sets the value of the countyQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountyQualifier(String value) {
        this.countyQualifier = value;
    }

    /**
     * Gets the value of the railSPLCGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailSPLCGid() {
        return railSPLCGid;
    }

    /**
     * Sets the value of the railSPLCGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailSPLCGid(GLogXMLGidType value) {
        this.railSPLCGid = value;
    }

    /**
     * Gets the value of the railStationCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailStationCodeGid() {
        return railStationCodeGid;
    }

    /**
     * Sets the value of the railStationCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailStationCodeGid(GLogXMLGidType value) {
        this.railStationCodeGid = value;
    }

}

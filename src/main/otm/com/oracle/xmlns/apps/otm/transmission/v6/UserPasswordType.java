
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             User password specification for insert or update.
 *          
 * 
 * <p>Java class for UserPasswordType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserPasswordType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OldPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGlPassword" minOccurs="0"/&gt;
 *         &lt;element name="NewPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGlPassword"/&gt;
 *         &lt;element name="NewPasswordExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserPasswordType", propOrder = {
    "oldPassword",
    "newPassword",
    "newPasswordExpirationDate"
})
public class UserPasswordType {

    @XmlElement(name = "OldPassword")
    protected GLogXMLGlPassword oldPassword;
    @XmlElement(name = "NewPassword", required = true)
    protected GLogXMLGlPassword newPassword;
    @XmlElement(name = "NewPasswordExpirationDate")
    protected GLogDateTimeType newPasswordExpirationDate;

    /**
     * Gets the value of the oldPassword property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public GLogXMLGlPassword getOldPassword() {
        return oldPassword;
    }

    /**
     * Sets the value of the oldPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public void setOldPassword(GLogXMLGlPassword value) {
        this.oldPassword = value;
    }

    /**
     * Gets the value of the newPassword property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public GLogXMLGlPassword getNewPassword() {
        return newPassword;
    }

    /**
     * Sets the value of the newPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGlPassword }
     *     
     */
    public void setNewPassword(GLogXMLGlPassword value) {
        this.newPassword = value;
    }

    /**
     * Gets the value of the newPasswordExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getNewPasswordExpirationDate() {
        return newPasswordExpirationDate;
    }

    /**
     * Sets the value of the newPasswordExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setNewPasswordExpirationDate(GLogDateTimeType value) {
        this.newPasswordExpirationDate = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             CommercialInvoiceDataIndicates the commercial value * unit count.
 *          
 * 
 * <p>Java class for CommercialInvoiceDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialInvoiceDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CommercialUnitPrice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialUnitPriceQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LineItemTotalCommercialValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="UnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialInvoiceDataType", propOrder = {
    "commercialUnitPrice",
    "commercialUnitPriceQualifier",
    "lineItemTotalCommercialValue",
    "unitCount",
    "packagedItemSpecRef"
})
public class CommercialInvoiceDataType {

    @XmlElement(name = "CommercialUnitPrice")
    protected GLogXMLFinancialAmountType commercialUnitPrice;
    @XmlElement(name = "CommercialUnitPriceQualifier")
    protected String commercialUnitPriceQualifier;
    @XmlElement(name = "LineItemTotalCommercialValue")
    protected GLogXMLFinancialAmountType lineItemTotalCommercialValue;
    @XmlElement(name = "UnitCount")
    protected String unitCount;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;

    /**
     * Gets the value of the commercialUnitPrice property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCommercialUnitPrice() {
        return commercialUnitPrice;
    }

    /**
     * Sets the value of the commercialUnitPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCommercialUnitPrice(GLogXMLFinancialAmountType value) {
        this.commercialUnitPrice = value;
    }

    /**
     * Gets the value of the commercialUnitPriceQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialUnitPriceQualifier() {
        return commercialUnitPriceQualifier;
    }

    /**
     * Sets the value of the commercialUnitPriceQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialUnitPriceQualifier(String value) {
        this.commercialUnitPriceQualifier = value;
    }

    /**
     * Gets the value of the lineItemTotalCommercialValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getLineItemTotalCommercialValue() {
        return lineItemTotalCommercialValue;
    }

    /**
     * Sets the value of the lineItemTotalCommercialValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setLineItemTotalCommercialValue(GLogXMLFinancialAmountType value) {
        this.lineItemTotalCommercialValue = value;
    }

    /**
     * Gets the value of the unitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitCount() {
        return unitCount;
    }

    /**
     * Sets the value of the unitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitCount(String value) {
        this.unitCount = value;
    }

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

}

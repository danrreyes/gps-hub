
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains Inner Pack count information.
 * 
 *             Note: When the PackagedItemSpecRef element is specified in the Packaging element, it should not be specified
 *             again
 *             in the PackagingShipUnit element.
 *          
 * 
 * <p>Java class for PackagingInnerPackType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PackagingInnerPackType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="InnerPackCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackagingInnerPackType", propOrder = {
    "packagedItemSpecRef",
    "innerPackCount"
})
public class PackagingInnerPackType {

    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "InnerPackCount")
    protected String innerPackCount;

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Gets the value of the innerPackCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInnerPackCount() {
        return innerPackCount;
    }

    /**
     * Sets the value of the innerPackCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInnerPackCount(String value) {
        this.innerPackCount = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Provides a Release centric view of the ShipUnit(s) on the Shipment.
 * 
 * <p>Java class for ShipUnitViewByReleaseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitViewByReleaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipUnitView" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitViewType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitViewByReleaseType", propOrder = {
    "releaseGid",
    "shipUnitView"
})
public class ShipUnitViewByReleaseType {

    @XmlElement(name = "ReleaseGid", required = true)
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "ShipUnitView", required = true)
    protected ShipUnitViewType shipUnitView;

    /**
     * Gets the value of the releaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Sets the value of the releaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Gets the value of the shipUnitView property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitViewType }
     *     
     */
    public ShipUnitViewType getShipUnitView() {
        return shipUnitView;
    }

    /**
     * Sets the value of the shipUnitView property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitViewType }
     *     
     */
    public void setShipUnitView(ShipUnitViewType value) {
        this.shipUnitView = value;
    }

}

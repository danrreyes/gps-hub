
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ItemQuantity is a grouping of item related fields.
 * 
 * <p>Java class for ItemQuantityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemQuantityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItemTag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsShippable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsSplitAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemQuantityType", propOrder = {
    "itemTag1",
    "itemTag2",
    "itemTag3",
    "itemTag4",
    "isShippable",
    "isSplitAllowed",
    "weightVolume",
    "packagedItemCount",
    "declaredValue"
})
public class ItemQuantityType {

    @XmlElement(name = "ItemTag1")
    protected String itemTag1;
    @XmlElement(name = "ItemTag2")
    protected String itemTag2;
    @XmlElement(name = "ItemTag3")
    protected String itemTag3;
    @XmlElement(name = "ItemTag4")
    protected String itemTag4;
    @XmlElement(name = "IsShippable")
    protected String isShippable;
    @XmlElement(name = "IsSplitAllowed")
    protected String isSplitAllowed;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "PackagedItemCount")
    protected String packagedItemCount;
    @XmlElement(name = "DeclaredValue")
    protected GLogXMLFinancialAmountType declaredValue;

    /**
     * Gets the value of the itemTag1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag1() {
        return itemTag1;
    }

    /**
     * Sets the value of the itemTag1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag1(String value) {
        this.itemTag1 = value;
    }

    /**
     * Gets the value of the itemTag2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag2() {
        return itemTag2;
    }

    /**
     * Sets the value of the itemTag2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag2(String value) {
        this.itemTag2 = value;
    }

    /**
     * Gets the value of the itemTag3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag3() {
        return itemTag3;
    }

    /**
     * Sets the value of the itemTag3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag3(String value) {
        this.itemTag3 = value;
    }

    /**
     * Gets the value of the itemTag4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag4() {
        return itemTag4;
    }

    /**
     * Sets the value of the itemTag4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag4(String value) {
        this.itemTag4 = value;
    }

    /**
     * Gets the value of the isShippable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShippable() {
        return isShippable;
    }

    /**
     * Sets the value of the isShippable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShippable(String value) {
        this.isShippable = value;
    }

    /**
     * Gets the value of the isSplitAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSplitAllowed() {
        return isSplitAllowed;
    }

    /**
     * Sets the value of the isSplitAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSplitAllowed(String value) {
        this.isSplitAllowed = value;
    }

    /**
     * Gets the value of the weightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Sets the value of the weightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Gets the value of the packagedItemCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemCount() {
        return packagedItemCount;
    }

    /**
     * Sets the value of the packagedItemCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemCount(String value) {
        this.packagedItemCount = value;
    }

    /**
     * Gets the value of the declaredValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDeclaredValue() {
        return declaredValue;
    }

    /**
     * Sets the value of the declaredValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDeclaredValue(GLogXMLFinancialAmountType value) {
        this.declaredValue = value;
    }

}

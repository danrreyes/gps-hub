
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuoteLocationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuoteLocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;sequence minOccurs="0"&gt;
 *           &lt;element name="Type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="QuoteLocInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}QuoteLocInfoType"/&gt;
 *           &lt;element name="ViaLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteLocationType", propOrder = {
    "locationGid",
    "type",
    "quoteLocInfo",
    "viaLocation"
})
public class QuoteLocationType {

    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "Type")
    protected String type;
    @XmlElement(name = "QuoteLocInfo")
    protected QuoteLocInfoType quoteLocInfo;
    @XmlElement(name = "ViaLocation")
    protected GLogXMLLocGidType viaLocation;

    /**
     * Gets the value of the locationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Sets the value of the locationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the quoteLocInfo property.
     * 
     * @return
     *     possible object is
     *     {@link QuoteLocInfoType }
     *     
     */
    public QuoteLocInfoType getQuoteLocInfo() {
        return quoteLocInfo;
    }

    /**
     * Sets the value of the quoteLocInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuoteLocInfoType }
     *     
     */
    public void setQuoteLocInfo(QuoteLocInfoType value) {
        this.quoteLocInfo = value;
    }

    /**
     * Gets the value of the viaLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getViaLocation() {
        return viaLocation;
    }

    /**
     * Sets the value of the viaLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setViaLocation(GLogXMLLocGidType value) {
        this.viaLocation = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the cost reference information for the VAT Line Items.
 *          
 * 
 * <p>Java class for LineItemVatCostRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LineItemVatCostRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CostReferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CostQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="VatBasisAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="VatAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemVatCostRefType", propOrder = {
    "costReferenceGid",
    "costQualGid",
    "vatBasisAmount",
    "vatAmount"
})
public class LineItemVatCostRefType {

    @XmlElement(name = "CostReferenceGid", required = true)
    protected GLogXMLGidType costReferenceGid;
    @XmlElement(name = "CostQualGid")
    protected GLogXMLGidType costQualGid;
    @XmlElement(name = "VatBasisAmount")
    protected GLogXMLFinancialAmountType vatBasisAmount;
    @XmlElement(name = "VatAmount")
    protected GLogXMLFinancialAmountType vatAmount;

    /**
     * Gets the value of the costReferenceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostReferenceGid() {
        return costReferenceGid;
    }

    /**
     * Sets the value of the costReferenceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostReferenceGid(GLogXMLGidType value) {
        this.costReferenceGid = value;
    }

    /**
     * Gets the value of the costQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostQualGid() {
        return costQualGid;
    }

    /**
     * Sets the value of the costQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostQualGid(GLogXMLGidType value) {
        this.costQualGid = value;
    }

    /**
     * Gets the value of the vatBasisAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatBasisAmount() {
        return vatBasisAmount;
    }

    /**
     * Sets the value of the vatBasisAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatBasisAmount(GLogXMLFinancialAmountType value) {
        this.vatBasisAmount = value;
    }

    /**
     * Gets the value of the vatAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getVatAmount() {
        return vatAmount;
    }

    /**
     * Sets the value of the vatAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setVatAmount(GLogXMLFinancialAmountType value) {
        this.vatAmount = value;
    }

}

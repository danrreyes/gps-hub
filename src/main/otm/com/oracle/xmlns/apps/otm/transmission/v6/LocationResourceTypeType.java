
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the resource type defined for the location.
 * 
 * <p>Java class for LocationResourceTypeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationResourceTypeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ResourceTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AutoScheduleAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ConstraintAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationResource" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationResourceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationResourceTypeType", propOrder = {
    "resourceTypeGid",
    "calendarGid",
    "autoScheduleAppt",
    "constraintAppt",
    "description",
    "locationResource"
})
public class LocationResourceTypeType {

    @XmlElement(name = "ResourceTypeGid", required = true)
    protected GLogXMLGidType resourceTypeGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "AutoScheduleAppt")
    protected String autoScheduleAppt;
    @XmlElement(name = "ConstraintAppt")
    protected String constraintAppt;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "LocationResource")
    protected List<LocationResourceType> locationResource;

    /**
     * Gets the value of the resourceTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResourceTypeGid() {
        return resourceTypeGid;
    }

    /**
     * Sets the value of the resourceTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResourceTypeGid(GLogXMLGidType value) {
        this.resourceTypeGid = value;
    }

    /**
     * Gets the value of the calendarGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Sets the value of the calendarGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Gets the value of the autoScheduleAppt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoScheduleAppt() {
        return autoScheduleAppt;
    }

    /**
     * Sets the value of the autoScheduleAppt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoScheduleAppt(String value) {
        this.autoScheduleAppt = value;
    }

    /**
     * Gets the value of the constraintAppt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConstraintAppt() {
        return constraintAppt;
    }

    /**
     * Sets the value of the constraintAppt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConstraintAppt(String value) {
        this.constraintAppt = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the locationResource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationResource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationResource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationResourceType }
     * 
     * 
     */
    public List<LocationResourceType> getLocationResource() {
        if (locationResource == null) {
            locationResource = new ArrayList<LocationResourceType>();
        }
        return this.locationResource;
    }

}

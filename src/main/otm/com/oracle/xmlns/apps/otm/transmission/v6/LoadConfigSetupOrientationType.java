
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the load configuration set up orientation
 * 
 * <p>Java class for LoadConfigSetupOrientationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoadConfigSetupOrientationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LoadConfigSetupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="OrientationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="IsPreferred" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsFloorCompatible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxTopWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="MaxLengthOverhang" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/&gt;
 *         &lt;element name="MaxWidthOverhang" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWidthType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadConfigSetupOrientationType", propOrder = {
    "loadConfigSetupGid",
    "orientationGid",
    "isPreferred",
    "isFloorCompatible",
    "maxTopWeight",
    "maxLengthOverhang",
    "maxWidthOverhang"
})
public class LoadConfigSetupOrientationType {

    @XmlElement(name = "LoadConfigSetupGid", required = true)
    protected GLogXMLGidType loadConfigSetupGid;
    @XmlElement(name = "OrientationGid", required = true)
    protected GLogXMLGidType orientationGid;
    @XmlElement(name = "IsPreferred")
    protected String isPreferred;
    @XmlElement(name = "IsFloorCompatible")
    protected String isFloorCompatible;
    @XmlElement(name = "MaxTopWeight")
    protected GLogXMLWeightType maxTopWeight;
    @XmlElement(name = "MaxLengthOverhang")
    protected GLogXMLLengthType maxLengthOverhang;
    @XmlElement(name = "MaxWidthOverhang")
    protected GLogXMLWidthType maxWidthOverhang;

    /**
     * Gets the value of the loadConfigSetupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfigSetupGid() {
        return loadConfigSetupGid;
    }

    /**
     * Sets the value of the loadConfigSetupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfigSetupGid(GLogXMLGidType value) {
        this.loadConfigSetupGid = value;
    }

    /**
     * Gets the value of the orientationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrientationGid() {
        return orientationGid;
    }

    /**
     * Sets the value of the orientationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrientationGid(GLogXMLGidType value) {
        this.orientationGid = value;
    }

    /**
     * Gets the value of the isPreferred property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreferred() {
        return isPreferred;
    }

    /**
     * Sets the value of the isPreferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreferred(String value) {
        this.isPreferred = value;
    }

    /**
     * Gets the value of the isFloorCompatible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFloorCompatible() {
        return isFloorCompatible;
    }

    /**
     * Sets the value of the isFloorCompatible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFloorCompatible(String value) {
        this.isFloorCompatible = value;
    }

    /**
     * Gets the value of the maxTopWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxTopWeight() {
        return maxTopWeight;
    }

    /**
     * Sets the value of the maxTopWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxTopWeight(GLogXMLWeightType value) {
        this.maxTopWeight = value;
    }

    /**
     * Gets the value of the maxLengthOverhang property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getMaxLengthOverhang() {
        return maxLengthOverhang;
    }

    /**
     * Sets the value of the maxLengthOverhang property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setMaxLengthOverhang(GLogXMLLengthType value) {
        this.maxLengthOverhang = value;
    }

    /**
     * Gets the value of the maxWidthOverhang property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public GLogXMLWidthType getMaxWidthOverhang() {
        return maxWidthOverhang;
    }

    /**
     * Sets the value of the maxWidthOverhang property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public void setMaxWidthOverhang(GLogXMLWidthType value) {
        this.maxWidthOverhang = value;
    }

}

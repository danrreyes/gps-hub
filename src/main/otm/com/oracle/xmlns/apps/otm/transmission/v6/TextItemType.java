
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * If the text was retrieved from an item-level template, this represents item context
 *             information needed by downstream systems.
 *          
 * 
 * <p>Java class for TextItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TextItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TextItemContextType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TextItemContextGid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TextItemContextSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextItemType", propOrder = {
    "textItemContextType",
    "textItemContextGid",
    "textItemContextSeq"
})
public class TextItemType {

    @XmlElement(name = "TextItemContextType", required = true)
    protected String textItemContextType;
    @XmlElement(name = "TextItemContextGid", required = true)
    protected String textItemContextGid;
    @XmlElement(name = "TextItemContextSeq")
    protected String textItemContextSeq;

    /**
     * Gets the value of the textItemContextType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextItemContextType() {
        return textItemContextType;
    }

    /**
     * Sets the value of the textItemContextType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextItemContextType(String value) {
        this.textItemContextType = value;
    }

    /**
     * Gets the value of the textItemContextGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextItemContextGid() {
        return textItemContextGid;
    }

    /**
     * Sets the value of the textItemContextGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextItemContextGid(String value) {
        this.textItemContextGid = value;
    }

    /**
     * Gets the value of the textItemContextSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextItemContextSeq() {
        return textItemContextSeq;
    }

    /**
     * Sets the value of the textItemContextSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextItemContextSeq(String value) {
        this.textItemContextSeq = value;
    }

}

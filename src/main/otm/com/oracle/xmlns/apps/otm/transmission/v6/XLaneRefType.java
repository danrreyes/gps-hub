
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An XLaneRef is either a reference to an existing lane, or a new lane definition.
 * 
 * <p>Java class for XLaneRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XLaneRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="XLaneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="XLane" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}XLaneType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XLaneRefType", propOrder = {
    "xLaneGid",
    "xLane"
})
public class XLaneRefType {

    @XmlElement(name = "XLaneGid")
    protected GLogXMLGidType xLaneGid;
    @XmlElement(name = "XLane")
    protected XLaneType xLane;

    /**
     * Gets the value of the xLaneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getXLaneGid() {
        return xLaneGid;
    }

    /**
     * Sets the value of the xLaneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setXLaneGid(GLogXMLGidType value) {
        this.xLaneGid = value;
    }

    /**
     * Gets the value of the xLane property.
     * 
     * @return
     *     possible object is
     *     {@link XLaneType }
     *     
     */
    public XLaneType getXLane() {
        return xLane;
    }

    /**
     * Sets the value of the xLane property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLaneType }
     *     
     */
    public void setXLane(XLaneType value) {
        this.xLane = value;
    }

}

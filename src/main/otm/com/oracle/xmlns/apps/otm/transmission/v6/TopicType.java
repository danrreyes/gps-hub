
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             An example would be to initiate a Bulk Plan through the Integration interface after all the TransOrders
 *             have been loaded.
 *          
 * 
 * <p>Java class for TopicType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TopicType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TopicAliasName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TopicArg" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TopicArgName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="TopicArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TopicType", propOrder = {
    "topicAliasName",
    "topicArg"
})
public class TopicType
    extends OTMTransactionIn
{

    @XmlElement(name = "TopicAliasName", required = true)
    protected String topicAliasName;
    @XmlElement(name = "TopicArg")
    protected List<TopicType.TopicArg> topicArg;

    /**
     * Gets the value of the topicAliasName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTopicAliasName() {
        return topicAliasName;
    }

    /**
     * Sets the value of the topicAliasName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTopicAliasName(String value) {
        this.topicAliasName = value;
    }

    /**
     * Gets the value of the topicArg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the topicArg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTopicArg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TopicType.TopicArg }
     * 
     * 
     */
    public List<TopicType.TopicArg> getTopicArg() {
        if (topicArg == null) {
            topicArg = new ArrayList<TopicType.TopicArg>();
        }
        return this.topicArg;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TopicArgName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="TopicArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "topicArgName",
        "topicArgValue"
    })
    public static class TopicArg {

        @XmlElement(name = "TopicArgName", required = true)
        protected String topicArgName;
        @XmlElement(name = "TopicArgValue", required = true)
        protected String topicArgValue;

        /**
         * Gets the value of the topicArgName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTopicArgName() {
            return topicArgName;
        }

        /**
         * Sets the value of the topicArgName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTopicArgName(String value) {
            this.topicArgName = value;
        }

        /**
         * Gets the value of the topicArgValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTopicArgValue() {
            return topicArgValue;
        }

        /**
         * Sets the value of the topicArgValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTopicArgValue(String value) {
            this.topicArgValue = value;
        }

    }

}

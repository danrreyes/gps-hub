
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Corresponds to the sku table.
 *          
 * 
 * <p>Java class for SkuType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SkuType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SkuGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="SkuObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *             &lt;element name="SkuObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="WarehouseLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="SupplierCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OwnerCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSpecGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LastInventoryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AverageAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SkuQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuQuantityType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SkuLevel" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuLevelType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SkuCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuCostType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SkuDescriptor" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuDescriptorType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SkuQuantityAsset" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SkuQuantityAssetType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuType", propOrder = {
    "skuGid",
    "transactionCode",
    "replaceChildren",
    "skuObjectType",
    "skuObjectGid",
    "warehouseLocationGid",
    "supplierCorporationGid",
    "ownerCorporationGid",
    "shipUnitSpecGid",
    "lastInventoryDt",
    "averageAge",
    "description",
    "indicator",
    "skuQuantity",
    "skuLevel",
    "skuCost",
    "skuDescriptor",
    "status",
    "involvedParty",
    "skuQuantityAsset"
})
public class SkuType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SkuGid", required = true)
    protected GLogXMLGidType skuGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "SkuObjectType")
    protected String skuObjectType;
    @XmlElement(name = "SkuObjectGid")
    protected GLogXMLGidType skuObjectGid;
    @XmlElement(name = "WarehouseLocationGid", required = true)
    protected GLogXMLGidType warehouseLocationGid;
    @XmlElement(name = "SupplierCorporationGid")
    protected GLogXMLGidType supplierCorporationGid;
    @XmlElement(name = "OwnerCorporationGid")
    protected GLogXMLGidType ownerCorporationGid;
    @XmlElement(name = "ShipUnitSpecGid")
    protected GLogXMLGidType shipUnitSpecGid;
    @XmlElement(name = "LastInventoryDt")
    protected GLogDateTimeType lastInventoryDt;
    @XmlElement(name = "AverageAge")
    protected String averageAge;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "SkuQuantity")
    protected List<SkuQuantityType> skuQuantity;
    @XmlElement(name = "SkuLevel")
    protected List<SkuLevelType> skuLevel;
    @XmlElement(name = "SkuCost")
    protected List<SkuCostType> skuCost;
    @XmlElement(name = "SkuDescriptor")
    protected List<SkuDescriptorType> skuDescriptor;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "SkuQuantityAsset")
    protected List<SkuQuantityAssetType> skuQuantityAsset;

    /**
     * Gets the value of the skuGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuGid() {
        return skuGid;
    }

    /**
     * Sets the value of the skuGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuGid(GLogXMLGidType value) {
        this.skuGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the skuObjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuObjectType() {
        return skuObjectType;
    }

    /**
     * Sets the value of the skuObjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuObjectType(String value) {
        this.skuObjectType = value;
    }

    /**
     * Gets the value of the skuObjectGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSkuObjectGid() {
        return skuObjectGid;
    }

    /**
     * Sets the value of the skuObjectGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSkuObjectGid(GLogXMLGidType value) {
        this.skuObjectGid = value;
    }

    /**
     * Gets the value of the warehouseLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWarehouseLocationGid() {
        return warehouseLocationGid;
    }

    /**
     * Sets the value of the warehouseLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWarehouseLocationGid(GLogXMLGidType value) {
        this.warehouseLocationGid = value;
    }

    /**
     * Gets the value of the supplierCorporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSupplierCorporationGid() {
        return supplierCorporationGid;
    }

    /**
     * Sets the value of the supplierCorporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSupplierCorporationGid(GLogXMLGidType value) {
        this.supplierCorporationGid = value;
    }

    /**
     * Gets the value of the ownerCorporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOwnerCorporationGid() {
        return ownerCorporationGid;
    }

    /**
     * Sets the value of the ownerCorporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOwnerCorporationGid(GLogXMLGidType value) {
        this.ownerCorporationGid = value;
    }

    /**
     * Gets the value of the shipUnitSpecGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecGid() {
        return shipUnitSpecGid;
    }

    /**
     * Sets the value of the shipUnitSpecGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecGid(GLogXMLGidType value) {
        this.shipUnitSpecGid = value;
    }

    /**
     * Gets the value of the lastInventoryDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLastInventoryDt() {
        return lastInventoryDt;
    }

    /**
     * Sets the value of the lastInventoryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLastInventoryDt(GLogDateTimeType value) {
        this.lastInventoryDt = value;
    }

    /**
     * Gets the value of the averageAge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAverageAge() {
        return averageAge;
    }

    /**
     * Sets the value of the averageAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAverageAge(String value) {
        this.averageAge = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Sets the value of the indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Gets the value of the skuQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the skuQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuQuantityType }
     * 
     * 
     */
    public List<SkuQuantityType> getSkuQuantity() {
        if (skuQuantity == null) {
            skuQuantity = new ArrayList<SkuQuantityType>();
        }
        return this.skuQuantity;
    }

    /**
     * Gets the value of the skuLevel property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the skuLevel property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuLevel().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuLevelType }
     * 
     * 
     */
    public List<SkuLevelType> getSkuLevel() {
        if (skuLevel == null) {
            skuLevel = new ArrayList<SkuLevelType>();
        }
        return this.skuLevel;
    }

    /**
     * Gets the value of the skuCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the skuCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuCostType }
     * 
     * 
     */
    public List<SkuCostType> getSkuCost() {
        if (skuCost == null) {
            skuCost = new ArrayList<SkuCostType>();
        }
        return this.skuCost;
    }

    /**
     * Gets the value of the skuDescriptor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the skuDescriptor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuDescriptor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuDescriptorType }
     * 
     * 
     */
    public List<SkuDescriptorType> getSkuDescriptor() {
        if (skuDescriptor == null) {
            skuDescriptor = new ArrayList<SkuDescriptorType>();
        }
        return this.skuDescriptor;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the skuQuantityAsset property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the skuQuantityAsset property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkuQuantityAsset().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkuQuantityAssetType }
     * 
     * 
     */
    public List<SkuQuantityAssetType> getSkuQuantityAsset() {
        if (skuQuantityAsset == null) {
            skuQuantityAsset = new ArrayList<SkuQuantityAssetType>();
        }
        return this.skuQuantityAsset;
    }

}

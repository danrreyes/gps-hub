
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RemoteQueryReplyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemoteQueryReplyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="RIQQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQueryReplyType"/&gt;
 *         &lt;element name="ShipmentQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentQueryReplyType"/&gt;
 *         &lt;element name="TransmissionReportQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionReportQueryReplyType"/&gt;
 *         &lt;element name="GenericQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericQueryReplyType"/&gt;
 *         &lt;element name="AppointmentQueryReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentQueryReplyType"/&gt;
 *         &lt;element name="OrderRoutingRuleReply" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRoutingRuleReplyType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteQueryReplyType", propOrder = {
    "riqQueryReply",
    "shipmentQueryReply",
    "transmissionReportQueryReply",
    "genericQueryReply",
    "appointmentQueryReply",
    "orderRoutingRuleReply"
})
public class RemoteQueryReplyType
    extends OTMTransactionOut
{

    @XmlElement(name = "RIQQueryReply")
    protected RIQQueryReplyType riqQueryReply;
    @XmlElement(name = "ShipmentQueryReply")
    protected ShipmentQueryReplyType shipmentQueryReply;
    @XmlElement(name = "TransmissionReportQueryReply")
    protected TransmissionReportQueryReplyType transmissionReportQueryReply;
    @XmlElement(name = "GenericQueryReply")
    protected GenericQueryReplyType genericQueryReply;
    @XmlElement(name = "AppointmentQueryReply")
    protected AppointmentQueryReplyType appointmentQueryReply;
    @XmlElement(name = "OrderRoutingRuleReply")
    protected OrderRoutingRuleReplyType orderRoutingRuleReply;

    /**
     * Gets the value of the riqQueryReply property.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryReplyType }
     *     
     */
    public RIQQueryReplyType getRIQQueryReply() {
        return riqQueryReply;
    }

    /**
     * Sets the value of the riqQueryReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryReplyType }
     *     
     */
    public void setRIQQueryReply(RIQQueryReplyType value) {
        this.riqQueryReply = value;
    }

    /**
     * Gets the value of the shipmentQueryReply property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentQueryReplyType }
     *     
     */
    public ShipmentQueryReplyType getShipmentQueryReply() {
        return shipmentQueryReply;
    }

    /**
     * Sets the value of the shipmentQueryReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentQueryReplyType }
     *     
     */
    public void setShipmentQueryReply(ShipmentQueryReplyType value) {
        this.shipmentQueryReply = value;
    }

    /**
     * Gets the value of the transmissionReportQueryReply property.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionReportQueryReplyType }
     *     
     */
    public TransmissionReportQueryReplyType getTransmissionReportQueryReply() {
        return transmissionReportQueryReply;
    }

    /**
     * Sets the value of the transmissionReportQueryReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionReportQueryReplyType }
     *     
     */
    public void setTransmissionReportQueryReply(TransmissionReportQueryReplyType value) {
        this.transmissionReportQueryReply = value;
    }

    /**
     * Gets the value of the genericQueryReply property.
     * 
     * @return
     *     possible object is
     *     {@link GenericQueryReplyType }
     *     
     */
    public GenericQueryReplyType getGenericQueryReply() {
        return genericQueryReply;
    }

    /**
     * Sets the value of the genericQueryReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericQueryReplyType }
     *     
     */
    public void setGenericQueryReply(GenericQueryReplyType value) {
        this.genericQueryReply = value;
    }

    /**
     * Gets the value of the appointmentQueryReply property.
     * 
     * @return
     *     possible object is
     *     {@link AppointmentQueryReplyType }
     *     
     */
    public AppointmentQueryReplyType getAppointmentQueryReply() {
        return appointmentQueryReply;
    }

    /**
     * Sets the value of the appointmentQueryReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link AppointmentQueryReplyType }
     *     
     */
    public void setAppointmentQueryReply(AppointmentQueryReplyType value) {
        this.appointmentQueryReply = value;
    }

    /**
     * Gets the value of the orderRoutingRuleReply property.
     * 
     * @return
     *     possible object is
     *     {@link OrderRoutingRuleReplyType }
     *     
     */
    public OrderRoutingRuleReplyType getOrderRoutingRuleReply() {
        return orderRoutingRuleReply;
    }

    /**
     * Sets the value of the orderRoutingRuleReply property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderRoutingRuleReplyType }
     *     
     */
    public void setOrderRoutingRuleReply(OrderRoutingRuleReplyType value) {
        this.orderRoutingRuleReply = value;
    }

}

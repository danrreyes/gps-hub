
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InterimFlightInstanceRowType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InterimFlightInstanceRowType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FLIGHT_INSTANCE_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FLIGHT_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RECORD_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LEAVE_TIME_STAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ARRIVAL_TIME_STAMP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SRC_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SERVPROV_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SERVICE_CLASS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="STOP_COUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EQUIPMENT_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VEHICLE_TYPE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InterimFlightInstanceRowType", propOrder = {
    "flightinstanceid",
    "flightnumber",
    "recordcode",
    "leavetimestamp",
    "arrivaltimestamp",
    "srclocationgid",
    "destlocationgid",
    "servprovgid",
    "serviceclass",
    "stopcount",
    "equipmentgroupgid",
    "domainname",
    "insertdate",
    "updatedate",
    "insertuser",
    "updateuser",
    "vehicletypegid"
})
public class InterimFlightInstanceRowType {

    @XmlElement(name = "FLIGHT_INSTANCE_ID")
    protected String flightinstanceid;
    @XmlElement(name = "FLIGHT_NUMBER")
    protected String flightnumber;
    @XmlElement(name = "RECORD_CODE")
    protected String recordcode;
    @XmlElement(name = "LEAVE_TIME_STAMP")
    protected String leavetimestamp;
    @XmlElement(name = "ARRIVAL_TIME_STAMP")
    protected String arrivaltimestamp;
    @XmlElement(name = "SRC_LOCATION_GID")
    protected String srclocationgid;
    @XmlElement(name = "DEST_LOCATION_GID")
    protected String destlocationgid;
    @XmlElement(name = "SERVPROV_GID")
    protected String servprovgid;
    @XmlElement(name = "SERVICE_CLASS")
    protected String serviceclass;
    @XmlElement(name = "STOP_COUNT")
    protected String stopcount;
    @XmlElement(name = "EQUIPMENT_GROUP_GID")
    protected String equipmentgroupgid;
    @XmlElement(name = "DOMAIN_NAME")
    protected String domainname;
    @XmlElement(name = "INSERT_DATE")
    protected String insertdate;
    @XmlElement(name = "UPDATE_DATE")
    protected String updatedate;
    @XmlElement(name = "INSERT_USER")
    protected String insertuser;
    @XmlElement(name = "UPDATE_USER")
    protected String updateuser;
    @XmlElement(name = "VEHICLE_TYPE_GID")
    protected String vehicletypegid;
    @XmlAttribute(name = "num")
    protected String num;

    /**
     * Gets the value of the flightinstanceid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTINSTANCEID() {
        return flightinstanceid;
    }

    /**
     * Sets the value of the flightinstanceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTINSTANCEID(String value) {
        this.flightinstanceid = value;
    }

    /**
     * Gets the value of the flightnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTNUMBER() {
        return flightnumber;
    }

    /**
     * Sets the value of the flightnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTNUMBER(String value) {
        this.flightnumber = value;
    }

    /**
     * Gets the value of the recordcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECORDCODE() {
        return recordcode;
    }

    /**
     * Sets the value of the recordcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECORDCODE(String value) {
        this.recordcode = value;
    }

    /**
     * Gets the value of the leavetimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLEAVETIMESTAMP() {
        return leavetimestamp;
    }

    /**
     * Sets the value of the leavetimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLEAVETIMESTAMP(String value) {
        this.leavetimestamp = value;
    }

    /**
     * Gets the value of the arrivaltimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARRIVALTIMESTAMP() {
        return arrivaltimestamp;
    }

    /**
     * Sets the value of the arrivaltimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARRIVALTIMESTAMP(String value) {
        this.arrivaltimestamp = value;
    }

    /**
     * Gets the value of the srclocationgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCLOCATIONGID() {
        return srclocationgid;
    }

    /**
     * Sets the value of the srclocationgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCLOCATIONGID(String value) {
        this.srclocationgid = value;
    }

    /**
     * Gets the value of the destlocationgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTLOCATIONGID() {
        return destlocationgid;
    }

    /**
     * Sets the value of the destlocationgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTLOCATIONGID(String value) {
        this.destlocationgid = value;
    }

    /**
     * Gets the value of the servprovgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVPROVGID() {
        return servprovgid;
    }

    /**
     * Sets the value of the servprovgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVPROVGID(String value) {
        this.servprovgid = value;
    }

    /**
     * Gets the value of the serviceclass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVICECLASS() {
        return serviceclass;
    }

    /**
     * Sets the value of the serviceclass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVICECLASS(String value) {
        this.serviceclass = value;
    }

    /**
     * Gets the value of the stopcount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTOPCOUNT() {
        return stopcount;
    }

    /**
     * Sets the value of the stopcount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTOPCOUNT(String value) {
        this.stopcount = value;
    }

    /**
     * Gets the value of the equipmentgroupgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEQUIPMENTGROUPGID() {
        return equipmentgroupgid;
    }

    /**
     * Sets the value of the equipmentgroupgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEQUIPMENTGROUPGID(String value) {
        this.equipmentgroupgid = value;
    }

    /**
     * Gets the value of the domainname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMAINNAME() {
        return domainname;
    }

    /**
     * Sets the value of the domainname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMAINNAME(String value) {
        this.domainname = value;
    }

    /**
     * Gets the value of the insertdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTDATE() {
        return insertdate;
    }

    /**
     * Sets the value of the insertdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTDATE(String value) {
        this.insertdate = value;
    }

    /**
     * Gets the value of the updatedate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEDATE() {
        return updatedate;
    }

    /**
     * Sets the value of the updatedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEDATE(String value) {
        this.updatedate = value;
    }

    /**
     * Gets the value of the insertuser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTUSER() {
        return insertuser;
    }

    /**
     * Sets the value of the insertuser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTUSER(String value) {
        this.insertuser = value;
    }

    /**
     * Gets the value of the updateuser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEUSER() {
        return updateuser;
    }

    /**
     * Sets the value of the updateuser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEUSER(String value) {
        this.updateuser = value;
    }

    /**
     * Gets the value of the vehicletypegid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVEHICLETYPEGID() {
        return vehicletypegid;
    }

    /**
     * Sets the value of the vehicletypegid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVEHICLETYPEGID(String value) {
        this.vehicletypegid = value;
    }

    /**
     * Gets the value of the num property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Sets the value of the num property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

}

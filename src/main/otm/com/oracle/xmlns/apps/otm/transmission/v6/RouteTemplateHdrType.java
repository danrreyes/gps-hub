
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The RouteTemplateHdr contains general information about the route template.
 *          
 * 
 * <p>Java class for RouteTemplateHdrType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RouteTemplateHdrType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CreationSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="StartDepotLocRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="EndDepotLocRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="NumInstances" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IsClosedLoop" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AllowOverlappingShipments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RouteTempStrategicInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteTempStrategicInfoType" minOccurs="0"/&gt;
 *         &lt;element name="UserDefIconInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserDefIconInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTemplateHdrType", propOrder = {
    "creationSource",
    "isActive",
    "perspective",
    "effectiveDate",
    "expirationDate",
    "startDepotLocRef",
    "endDepotLocRef",
    "serviceProviderGid",
    "equipmentGroupProfileGid",
    "transportModeGid",
    "numInstances",
    "isClosedLoop",
    "allowOverlappingShipments",
    "routeTempStrategicInfo",
    "userDefIconInfo",
    "indicator"
})
public class RouteTemplateHdrType {

    @XmlElement(name = "CreationSource")
    protected String creationSource;
    @XmlElement(name = "IsActive")
    protected String isActive;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "EffectiveDate", required = true)
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "StartDepotLocRef")
    protected GLogXMLLocRefType startDepotLocRef;
    @XmlElement(name = "EndDepotLocRef")
    protected GLogXMLLocRefType endDepotLocRef;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "NumInstances", required = true)
    protected String numInstances;
    @XmlElement(name = "IsClosedLoop")
    protected String isClosedLoop;
    @XmlElement(name = "AllowOverlappingShipments")
    protected String allowOverlappingShipments;
    @XmlElement(name = "RouteTempStrategicInfo")
    protected RouteTempStrategicInfoType routeTempStrategicInfo;
    @XmlElement(name = "UserDefIconInfo")
    protected UserDefIconInfoType userDefIconInfo;
    @XmlElement(name = "Indicator")
    protected String indicator;

    /**
     * Gets the value of the creationSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationSource() {
        return creationSource;
    }

    /**
     * Sets the value of the creationSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationSource(String value) {
        this.creationSource = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the startDepotLocRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getStartDepotLocRef() {
        return startDepotLocRef;
    }

    /**
     * Sets the value of the startDepotLocRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setStartDepotLocRef(GLogXMLLocRefType value) {
        this.startDepotLocRef = value;
    }

    /**
     * Gets the value of the endDepotLocRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getEndDepotLocRef() {
        return endDepotLocRef;
    }

    /**
     * Sets the value of the endDepotLocRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setEndDepotLocRef(GLogXMLLocRefType value) {
        this.endDepotLocRef = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Sets the value of the transportModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Gets the value of the numInstances property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumInstances() {
        return numInstances;
    }

    /**
     * Sets the value of the numInstances property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumInstances(String value) {
        this.numInstances = value;
    }

    /**
     * Gets the value of the isClosedLoop property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsClosedLoop() {
        return isClosedLoop;
    }

    /**
     * Sets the value of the isClosedLoop property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsClosedLoop(String value) {
        this.isClosedLoop = value;
    }

    /**
     * Gets the value of the allowOverlappingShipments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowOverlappingShipments() {
        return allowOverlappingShipments;
    }

    /**
     * Sets the value of the allowOverlappingShipments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowOverlappingShipments(String value) {
        this.allowOverlappingShipments = value;
    }

    /**
     * Gets the value of the routeTempStrategicInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RouteTempStrategicInfoType }
     *     
     */
    public RouteTempStrategicInfoType getRouteTempStrategicInfo() {
        return routeTempStrategicInfo;
    }

    /**
     * Sets the value of the routeTempStrategicInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteTempStrategicInfoType }
     *     
     */
    public void setRouteTempStrategicInfo(RouteTempStrategicInfoType value) {
        this.routeTempStrategicInfo = value;
    }

    /**
     * Gets the value of the userDefIconInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public UserDefIconInfoType getUserDefIconInfo() {
        return userDefIconInfo;
    }

    /**
     * Sets the value of the userDefIconInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public void setUserDefIconInfo(UserDefIconInfoType value) {
        this.userDefIconInfo = value;
    }

    /**
     * Gets the value of the indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Sets the value of the indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

}

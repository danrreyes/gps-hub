
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ContactRef may be used to reference an existing contact or define a new one.
 * 
 * <p>Java class for ContactRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="Contact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactType"/&gt;
 *         &lt;element name="ContactGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactGroupType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactRefType", propOrder = {
    "contactGid",
    "contact",
    "contactGroup"
})
public class ContactRefType {

    @XmlElement(name = "ContactGid")
    protected GLogXMLGidType contactGid;
    @XmlElement(name = "Contact")
    protected ContactType contact;
    @XmlElement(name = "ContactGroup")
    protected ContactGroupType contactGroup;

    /**
     * Gets the value of the contactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Sets the value of the contactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link ContactType }
     *     
     */
    public ContactType getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactType }
     *     
     */
    public void setContact(ContactType value) {
        this.contact = value;
    }

    /**
     * Gets the value of the contactGroup property.
     * 
     * @return
     *     possible object is
     *     {@link ContactGroupType }
     *     
     */
    public ContactGroupType getContactGroup() {
        return contactGroup;
    }

    /**
     * Sets the value of the contactGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactGroupType }
     *     
     */
    public void setContactGroup(ContactGroupType value) {
        this.contactGroup = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * PaymentDetail is a component of ShipmentPayment.
 *             It details the charges which comprise the TotalPayment.
 *          
 * 
 * <p>Java class for PaymentDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PaymentDetailNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PaymentDetailQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PaymentDetailValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentDetailType", propOrder = {
    "paymentDetailNumber",
    "paymentDetailQualifierGid",
    "paymentDetailValue"
})
public class PaymentDetailType {

    @XmlElement(name = "PaymentDetailNumber", required = true)
    protected String paymentDetailNumber;
    @XmlElement(name = "PaymentDetailQualifierGid", required = true)
    protected GLogXMLGidType paymentDetailQualifierGid;
    @XmlElement(name = "PaymentDetailValue", required = true)
    protected GLogXMLFinancialAmountType paymentDetailValue;

    /**
     * Gets the value of the paymentDetailNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentDetailNumber() {
        return paymentDetailNumber;
    }

    /**
     * Sets the value of the paymentDetailNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentDetailNumber(String value) {
        this.paymentDetailNumber = value;
    }

    /**
     * Gets the value of the paymentDetailQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentDetailQualifierGid() {
        return paymentDetailQualifierGid;
    }

    /**
     * Sets the value of the paymentDetailQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentDetailQualifierGid(GLogXMLGidType value) {
        this.paymentDetailQualifierGid = value;
    }

    /**
     * Gets the value of the paymentDetailValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPaymentDetailValue() {
        return paymentDetailValue;
    }

    /**
     * Sets the value of the paymentDetailValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPaymentDetailValue(GLogXMLFinancialAmountType value) {
        this.paymentDetailValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             TenderResponse is a structure used by service providers to respond to a TenderOffer.
 *          
 * 
 * <p>Java class for TenderResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TenderResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType"/&gt;
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DeclineReasonCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BidAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="PickupDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="TendRespSEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TendRespSEquipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ConditionalBooking" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ConditionalBookingType" minOccurs="0"/&gt;
 *         &lt;element name="TendRespAcceptedSU" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TendRespAcceptedSUType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TenderResponseType", propOrder = {
    "sendReason",
    "iTransactionNo",
    "serviceProviderAlias",
    "shipmentRefnum",
    "actionCode",
    "declineReasonCodeGid",
    "bidAmount",
    "pickupDate",
    "tendRespSEquipment",
    "remark",
    "conditionalBooking",
    "tendRespAcceptedSU"
})
public class TenderResponseType
    extends OTMTransactionIn
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "ITransactionNo", required = true)
    protected String iTransactionNo;
    @XmlElement(name = "ServiceProviderAlias", required = true)
    protected ServiceProviderAliasType serviceProviderAlias;
    @XmlElement(name = "ShipmentRefnum")
    protected List<ShipmentRefnumType> shipmentRefnum;
    @XmlElement(name = "ActionCode")
    protected String actionCode;
    @XmlElement(name = "DeclineReasonCodeGid")
    protected GLogXMLGidType declineReasonCodeGid;
    @XmlElement(name = "BidAmount")
    protected GLogXMLFinancialAmountType bidAmount;
    @XmlElement(name = "PickupDate")
    protected GLogDateTimeType pickupDate;
    @XmlElement(name = "TendRespSEquipment")
    protected List<TendRespSEquipmentType> tendRespSEquipment;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ConditionalBooking")
    protected ConditionalBookingType conditionalBooking;
    @XmlElement(name = "TendRespAcceptedSU")
    protected List<TendRespAcceptedSUType> tendRespAcceptedSU;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the iTransactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Sets the value of the iTransactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Gets the value of the serviceProviderAlias property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public ServiceProviderAliasType getServiceProviderAlias() {
        return serviceProviderAlias;
    }

    /**
     * Sets the value of the serviceProviderAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public void setServiceProviderAlias(ServiceProviderAliasType value) {
        this.serviceProviderAlias = value;
    }

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentRefnumType }
     * 
     * 
     */
    public List<ShipmentRefnumType> getShipmentRefnum() {
        if (shipmentRefnum == null) {
            shipmentRefnum = new ArrayList<ShipmentRefnumType>();
        }
        return this.shipmentRefnum;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the declineReasonCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDeclineReasonCodeGid() {
        return declineReasonCodeGid;
    }

    /**
     * Sets the value of the declineReasonCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDeclineReasonCodeGid(GLogXMLGidType value) {
        this.declineReasonCodeGid = value;
    }

    /**
     * Gets the value of the bidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getBidAmount() {
        return bidAmount;
    }

    /**
     * Sets the value of the bidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setBidAmount(GLogXMLFinancialAmountType value) {
        this.bidAmount = value;
    }

    /**
     * Gets the value of the pickupDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPickupDate() {
        return pickupDate;
    }

    /**
     * Sets the value of the pickupDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPickupDate(GLogDateTimeType value) {
        this.pickupDate = value;
    }

    /**
     * Gets the value of the tendRespSEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the tendRespSEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTendRespSEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TendRespSEquipmentType }
     * 
     * 
     */
    public List<TendRespSEquipmentType> getTendRespSEquipment() {
        if (tendRespSEquipment == null) {
            tendRespSEquipment = new ArrayList<TendRespSEquipmentType>();
        }
        return this.tendRespSEquipment;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the conditionalBooking property.
     * 
     * @return
     *     possible object is
     *     {@link ConditionalBookingType }
     *     
     */
    public ConditionalBookingType getConditionalBooking() {
        return conditionalBooking;
    }

    /**
     * Sets the value of the conditionalBooking property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionalBookingType }
     *     
     */
    public void setConditionalBooking(ConditionalBookingType value) {
        this.conditionalBooking = value;
    }

    /**
     * Gets the value of the tendRespAcceptedSU property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the tendRespAcceptedSU property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTendRespAcceptedSU().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TendRespAcceptedSUType }
     * 
     * 
     */
    public List<TendRespAcceptedSUType> getTendRespAcceptedSU() {
        if (tendRespAcceptedSU == null) {
            tendRespAcceptedSU = new ArrayList<TendRespAcceptedSUType>();
        }
        return this.tendRespAcceptedSU;
    }

}

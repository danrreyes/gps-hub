
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the time window in the number of seconds after midnight of day 1 of the week.
 *          
 * 
 * <p>Java class for TimeWindowSecType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TimeWindowSecType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EarlyDepartureTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LateDepartureTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EarlyArrivalTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LateArrivalTimeSec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TimeWindowSecType", propOrder = {
    "earlyDepartureTimeSec",
    "lateDepartureTimeSec",
    "earlyArrivalTimeSec",
    "lateArrivalTimeSec"
})
public class TimeWindowSecType {

    @XmlElement(name = "EarlyDepartureTimeSec")
    protected String earlyDepartureTimeSec;
    @XmlElement(name = "LateDepartureTimeSec")
    protected String lateDepartureTimeSec;
    @XmlElement(name = "EarlyArrivalTimeSec")
    protected String earlyArrivalTimeSec;
    @XmlElement(name = "LateArrivalTimeSec")
    protected String lateArrivalTimeSec;

    /**
     * Gets the value of the earlyDepartureTimeSec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarlyDepartureTimeSec() {
        return earlyDepartureTimeSec;
    }

    /**
     * Sets the value of the earlyDepartureTimeSec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarlyDepartureTimeSec(String value) {
        this.earlyDepartureTimeSec = value;
    }

    /**
     * Gets the value of the lateDepartureTimeSec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLateDepartureTimeSec() {
        return lateDepartureTimeSec;
    }

    /**
     * Sets the value of the lateDepartureTimeSec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLateDepartureTimeSec(String value) {
        this.lateDepartureTimeSec = value;
    }

    /**
     * Gets the value of the earlyArrivalTimeSec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEarlyArrivalTimeSec() {
        return earlyArrivalTimeSec;
    }

    /**
     * Sets the value of the earlyArrivalTimeSec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEarlyArrivalTimeSec(String value) {
        this.earlyArrivalTimeSec = value;
    }

    /**
     * Gets the value of the lateArrivalTimeSec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLateArrivalTimeSec() {
        return lateArrivalTimeSec;
    }

    /**
     * Sets the value of the lateArrivalTimeSec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLateArrivalTimeSec(String value) {
        this.lateArrivalTimeSec = value;
    }

}

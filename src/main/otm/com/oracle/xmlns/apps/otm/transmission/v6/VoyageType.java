
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The Voyage is used to specify world wide vessel schedule information.
 *          
 * 
 * <p>Java class for VoyageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoyageType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="VoyageServiceTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="VoyageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DepartureRegionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ArrivalRegionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DepartureDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="Vessel" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VesselType" minOccurs="0"/&gt;
 *         &lt;element name="VoyageLoc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VoyageLocType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoyageType", propOrder = {
    "sendReason",
    "voyageGid",
    "serviceProviderGid",
    "voyageServiceTypeGid",
    "voyageName",
    "departureRegionName",
    "arrivalRegionName",
    "departureDt",
    "vessel",
    "voyageLoc",
    "location"
})
public class VoyageType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "VoyageGid", required = true)
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "ServiceProviderGid", required = true)
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "VoyageServiceTypeGid", required = true)
    protected GLogXMLGidType voyageServiceTypeGid;
    @XmlElement(name = "VoyageName")
    protected String voyageName;
    @XmlElement(name = "DepartureRegionName")
    protected String departureRegionName;
    @XmlElement(name = "ArrivalRegionName")
    protected String arrivalRegionName;
    @XmlElement(name = "DepartureDt")
    protected GLogDateTimeType departureDt;
    @XmlElement(name = "Vessel")
    protected VesselType vessel;
    @XmlElement(name = "VoyageLoc")
    protected List<VoyageLocType> voyageLoc;
    @XmlElement(name = "Location")
    protected List<LocationType> location;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the voyageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Sets the value of the voyageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the voyageServiceTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageServiceTypeGid() {
        return voyageServiceTypeGid;
    }

    /**
     * Sets the value of the voyageServiceTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageServiceTypeGid(GLogXMLGidType value) {
        this.voyageServiceTypeGid = value;
    }

    /**
     * Gets the value of the voyageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageName() {
        return voyageName;
    }

    /**
     * Sets the value of the voyageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageName(String value) {
        this.voyageName = value;
    }

    /**
     * Gets the value of the departureRegionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureRegionName() {
        return departureRegionName;
    }

    /**
     * Sets the value of the departureRegionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureRegionName(String value) {
        this.departureRegionName = value;
    }

    /**
     * Gets the value of the arrivalRegionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrivalRegionName() {
        return arrivalRegionName;
    }

    /**
     * Sets the value of the arrivalRegionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrivalRegionName(String value) {
        this.arrivalRegionName = value;
    }

    /**
     * Gets the value of the departureDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDepartureDt() {
        return departureDt;
    }

    /**
     * Sets the value of the departureDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDepartureDt(GLogDateTimeType value) {
        this.departureDt = value;
    }

    /**
     * Gets the value of the vessel property.
     * 
     * @return
     *     possible object is
     *     {@link VesselType }
     *     
     */
    public VesselType getVessel() {
        return vessel;
    }

    /**
     * Sets the value of the vessel property.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselType }
     *     
     */
    public void setVessel(VesselType value) {
        this.vessel = value;
    }

    /**
     * Gets the value of the voyageLoc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the voyageLoc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoyageLoc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoyageLocType }
     * 
     * 
     */
    public List<VoyageLocType> getVoyageLoc() {
        if (voyageLoc == null) {
            voyageLoc = new ArrayList<VoyageLocType>();
        }
        return this.voyageLoc;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

}

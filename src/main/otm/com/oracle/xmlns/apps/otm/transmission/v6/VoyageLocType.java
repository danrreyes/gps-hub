
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * VoyageLoc specifies the individual locations within the voyage path.
 * 
 * <p>Java class for VoyageLocType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoyageLocType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VoyageLocSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/&gt;
 *         &lt;element name="DepartureOrArrival" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DepartureOrArrivalDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="EstDepOrArrDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ActDepOrArrDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoyageLocType", propOrder = {
    "voyageLocSequence",
    "locationRef",
    "departureOrArrival",
    "departureOrArrivalDt",
    "estDepOrArrDt",
    "actDepOrArrDt"
})
public class VoyageLocType {

    @XmlElement(name = "VoyageLocSequence", required = true)
    protected String voyageLocSequence;
    @XmlElement(name = "LocationRef", required = true)
    protected LocationRefType locationRef;
    @XmlElement(name = "DepartureOrArrival", required = true)
    protected String departureOrArrival;
    @XmlElement(name = "DepartureOrArrivalDt", required = true)
    protected GLogDateTimeType departureOrArrivalDt;
    @XmlElement(name = "EstDepOrArrDt")
    protected GLogDateTimeType estDepOrArrDt;
    @XmlElement(name = "ActDepOrArrDt")
    protected GLogDateTimeType actDepOrArrDt;

    /**
     * Gets the value of the voyageLocSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoyageLocSequence() {
        return voyageLocSequence;
    }

    /**
     * Sets the value of the voyageLocSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoyageLocSequence(String value) {
        this.voyageLocSequence = value;
    }

    /**
     * Gets the value of the locationRef property.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Sets the value of the locationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Gets the value of the departureOrArrival property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartureOrArrival() {
        return departureOrArrival;
    }

    /**
     * Sets the value of the departureOrArrival property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartureOrArrival(String value) {
        this.departureOrArrival = value;
    }

    /**
     * Gets the value of the departureOrArrivalDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDepartureOrArrivalDt() {
        return departureOrArrivalDt;
    }

    /**
     * Sets the value of the departureOrArrivalDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDepartureOrArrivalDt(GLogDateTimeType value) {
        this.departureOrArrivalDt = value;
    }

    /**
     * Gets the value of the estDepOrArrDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstDepOrArrDt() {
        return estDepOrArrDt;
    }

    /**
     * Sets the value of the estDepOrArrDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstDepOrArrDt(GLogDateTimeType value) {
        this.estDepOrArrDt = value;
    }

    /**
     * Gets the value of the actDepOrArrDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActDepOrArrDt() {
        return actDepOrArrDt;
    }

    /**
     * Sets the value of the actDepOrArrDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActDepOrArrDt(GLogDateTimeType value) {
        this.actDepOrArrDt = value;
    }

}

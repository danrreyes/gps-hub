
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to specify Packaging Reference Unit information.
 * 
 * <p>Java class for PackageRefUnittype complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PackageRefUnittype"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PackageRefUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="NumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TotalNumReferenceUnits" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackageRefUnittype", propOrder = {
    "packageRefUnitGid",
    "numReferenceUnits",
    "totalNumReferenceUnits"
})
public class PackageRefUnittype {

    @XmlElement(name = "PackageRefUnitGid", required = true)
    protected GLogXMLGidType packageRefUnitGid;
    @XmlElement(name = "NumReferenceUnits", required = true)
    protected String numReferenceUnits;
    @XmlElement(name = "TotalNumReferenceUnits", required = true)
    protected String totalNumReferenceUnits;

    /**
     * Gets the value of the packageRefUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackageRefUnitGid() {
        return packageRefUnitGid;
    }

    /**
     * Sets the value of the packageRefUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackageRefUnitGid(GLogXMLGidType value) {
        this.packageRefUnitGid = value;
    }

    /**
     * Gets the value of the numReferenceUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumReferenceUnits() {
        return numReferenceUnits;
    }

    /**
     * Sets the value of the numReferenceUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumReferenceUnits(String value) {
        this.numReferenceUnits = value;
    }

    /**
     * Gets the value of the totalNumReferenceUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalNumReferenceUnits() {
        return totalNumReferenceUnits;
    }

    /**
     * Sets the value of the totalNumReferenceUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalNumReferenceUnits(String value) {
        this.totalNumReferenceUnits = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the load configuration result for each piece of the ship unit.
 *             This is only applicable when the ShipUnit is in the Shipment.
 *          
 * 
 * <p>Java class for ShipUnitPieceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitPieceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PieceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LoadingSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="StackingLayer" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OrientationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="XOrdinate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType"/&gt;
 *         &lt;element name="YOrdinate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType"/&gt;
 *         &lt;element name="ZOrdinate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType"/&gt;
 *         &lt;element name="LoadConfig3DPatternGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PatternInstanceCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitPieceType", propOrder = {
    "pieceNumber",
    "loadingSequence",
    "stackingLayer",
    "orientationGid",
    "xOrdinate",
    "yOrdinate",
    "zOrdinate",
    "loadConfig3DPatternGid",
    "patternInstanceCount"
})
public class ShipUnitPieceType {

    @XmlElement(name = "PieceNumber", required = true)
    protected String pieceNumber;
    @XmlElement(name = "LoadingSequence", required = true)
    protected String loadingSequence;
    @XmlElement(name = "StackingLayer", required = true)
    protected String stackingLayer;
    @XmlElement(name = "OrientationGid", required = true)
    protected GLogXMLGidType orientationGid;
    @XmlElement(name = "XOrdinate", required = true)
    protected GLogXMLLengthType xOrdinate;
    @XmlElement(name = "YOrdinate", required = true)
    protected GLogXMLLengthType yOrdinate;
    @XmlElement(name = "ZOrdinate", required = true)
    protected GLogXMLLengthType zOrdinate;
    @XmlElement(name = "LoadConfig3DPatternGid")
    protected GLogXMLGidType loadConfig3DPatternGid;
    @XmlElement(name = "PatternInstanceCount")
    protected String patternInstanceCount;

    /**
     * Gets the value of the pieceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPieceNumber() {
        return pieceNumber;
    }

    /**
     * Sets the value of the pieceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPieceNumber(String value) {
        this.pieceNumber = value;
    }

    /**
     * Gets the value of the loadingSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadingSequence() {
        return loadingSequence;
    }

    /**
     * Sets the value of the loadingSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadingSequence(String value) {
        this.loadingSequence = value;
    }

    /**
     * Gets the value of the stackingLayer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackingLayer() {
        return stackingLayer;
    }

    /**
     * Sets the value of the stackingLayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackingLayer(String value) {
        this.stackingLayer = value;
    }

    /**
     * Gets the value of the orientationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrientationGid() {
        return orientationGid;
    }

    /**
     * Sets the value of the orientationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrientationGid(GLogXMLGidType value) {
        this.orientationGid = value;
    }

    /**
     * Gets the value of the xOrdinate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getXOrdinate() {
        return xOrdinate;
    }

    /**
     * Sets the value of the xOrdinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setXOrdinate(GLogXMLLengthType value) {
        this.xOrdinate = value;
    }

    /**
     * Gets the value of the yOrdinate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getYOrdinate() {
        return yOrdinate;
    }

    /**
     * Sets the value of the yOrdinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setYOrdinate(GLogXMLLengthType value) {
        this.yOrdinate = value;
    }

    /**
     * Gets the value of the zOrdinate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getZOrdinate() {
        return zOrdinate;
    }

    /**
     * Sets the value of the zOrdinate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setZOrdinate(GLogXMLLengthType value) {
        this.zOrdinate = value;
    }

    /**
     * Gets the value of the loadConfig3DPatternGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfig3DPatternGid() {
        return loadConfig3DPatternGid;
    }

    /**
     * Sets the value of the loadConfig3DPatternGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfig3DPatternGid(GLogXMLGidType value) {
        this.loadConfig3DPatternGid = value;
    }

    /**
     * Gets the value of the patternInstanceCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPatternInstanceCount() {
        return patternInstanceCount;
    }

    /**
     * Sets the value of the patternInstanceCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPatternInstanceCount(String value) {
        this.patternInstanceCount = value;
    }

}

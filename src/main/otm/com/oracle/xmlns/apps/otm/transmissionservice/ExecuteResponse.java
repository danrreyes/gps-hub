
package com.oracle.xmlns.apps.otm.transmissionservice;

import com.oracle.xmlns.apps.otm.transmission.v6.Transmission;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Transmission"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transmission"
})
@XmlRootElement(name = "executeResponse")
public class ExecuteResponse {

    @XmlElement(name = "Transmission", namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", required = true)
    protected Transmission transmission;

    /**
     * Gets the value of the transmission property.
     * 
     * @return
     *     possible object is
     *     {@link Transmission }
     *     
     */
    public Transmission getTransmission() {
        return transmission;
    }

    /**
     * Sets the value of the transmission property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transmission }
     *     
     */
    public void setTransmission(Transmission value) {
        this.transmission = value;
    }

}

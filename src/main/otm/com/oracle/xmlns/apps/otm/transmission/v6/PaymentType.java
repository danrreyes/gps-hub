
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             (Both) Payment is a common xml structure used to represent invoices, bills, and vouchers.
 *          
 * 
 * <p>Java class for PaymentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PaymentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PaymentHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentHeaderType"/&gt;
 *         &lt;element name="PaymentModeDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentModeDetailType" minOccurs="0"/&gt;
 *         &lt;element name="PaymentSummary" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentSummaryType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ChildPayments" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ChildPaymentsType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentType", propOrder = {
    "paymentHeader",
    "paymentModeDetail",
    "paymentSummary",
    "childPayments"
})
public class PaymentType {

    @XmlElement(name = "PaymentHeader", required = true)
    protected PaymentHeaderType paymentHeader;
    @XmlElement(name = "PaymentModeDetail")
    protected PaymentModeDetailType paymentModeDetail;
    @XmlElement(name = "PaymentSummary")
    protected List<PaymentSummaryType> paymentSummary;
    @XmlElement(name = "ChildPayments")
    protected ChildPaymentsType childPayments;

    /**
     * Gets the value of the paymentHeader property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentHeaderType }
     *     
     */
    public PaymentHeaderType getPaymentHeader() {
        return paymentHeader;
    }

    /**
     * Sets the value of the paymentHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentHeaderType }
     *     
     */
    public void setPaymentHeader(PaymentHeaderType value) {
        this.paymentHeader = value;
    }

    /**
     * Gets the value of the paymentModeDetail property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentModeDetailType }
     *     
     */
    public PaymentModeDetailType getPaymentModeDetail() {
        return paymentModeDetail;
    }

    /**
     * Sets the value of the paymentModeDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentModeDetailType }
     *     
     */
    public void setPaymentModeDetail(PaymentModeDetailType value) {
        this.paymentModeDetail = value;
    }

    /**
     * Gets the value of the paymentSummary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the paymentSummary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentSummary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PaymentSummaryType }
     * 
     * 
     */
    public List<PaymentSummaryType> getPaymentSummary() {
        if (paymentSummary == null) {
            paymentSummary = new ArrayList<PaymentSummaryType>();
        }
        return this.paymentSummary;
    }

    /**
     * Gets the value of the childPayments property.
     * 
     * @return
     *     possible object is
     *     {@link ChildPaymentsType }
     *     
     */
    public ChildPaymentsType getChildPayments() {
        return childPayments;
    }

    /**
     * Sets the value of the childPayments property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChildPaymentsType }
     *     
     */
    public void setChildPayments(ChildPaymentsType value) {
        this.childPayments = value;
    }

}

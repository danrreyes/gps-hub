
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * RemoteQueryStatus is used to return status information as part of a remote query reply.
 *          
 * 
 * <p>Java class for RemoteQueryStatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemoteQueryStatusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RemoteQueryStatusCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RemoteQueryStatusSubject" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RemoteQueryStatusMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StackTrace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionReport" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionReportType" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteQueryStatusType", propOrder = {
    "remoteQueryStatusCode",
    "remoteQueryStatusSubject",
    "remoteQueryStatusMessage",
    "stackTrace",
    "transactionReport",
    "perspective"
})
public class RemoteQueryStatusType {

    @XmlElement(name = "RemoteQueryStatusCode", required = true)
    protected String remoteQueryStatusCode;
    @XmlElement(name = "RemoteQueryStatusSubject", required = true)
    protected String remoteQueryStatusSubject;
    @XmlElement(name = "RemoteQueryStatusMessage")
    protected String remoteQueryStatusMessage;
    @XmlElement(name = "StackTrace")
    protected String stackTrace;
    @XmlElement(name = "TransactionReport")
    protected TransactionReportType transactionReport;
    @XmlElement(name = "Perspective")
    protected String perspective;

    /**
     * Gets the value of the remoteQueryStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteQueryStatusCode() {
        return remoteQueryStatusCode;
    }

    /**
     * Sets the value of the remoteQueryStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteQueryStatusCode(String value) {
        this.remoteQueryStatusCode = value;
    }

    /**
     * Gets the value of the remoteQueryStatusSubject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteQueryStatusSubject() {
        return remoteQueryStatusSubject;
    }

    /**
     * Sets the value of the remoteQueryStatusSubject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteQueryStatusSubject(String value) {
        this.remoteQueryStatusSubject = value;
    }

    /**
     * Gets the value of the remoteQueryStatusMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteQueryStatusMessage() {
        return remoteQueryStatusMessage;
    }

    /**
     * Sets the value of the remoteQueryStatusMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteQueryStatusMessage(String value) {
        this.remoteQueryStatusMessage = value;
    }

    /**
     * Gets the value of the stackTrace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Sets the value of the stackTrace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackTrace(String value) {
        this.stackTrace = value;
    }

    /**
     * Gets the value of the transactionReport property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionReportType }
     *     
     */
    public TransactionReportType getTransactionReport() {
        return transactionReport;
    }

    /**
     * Sets the value of the transactionReport property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionReportType }
     *     
     */
    public void setTransactionReport(TransactionReportType value) {
        this.transactionReport = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains additional information about the the line item.
 * 
 * <p>Java class for ItemAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemAttributeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItemFeatureQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ItemFeatureValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemAttributeType", propOrder = {
    "itemFeatureQualGid",
    "itemFeatureValue"
})
public class ItemAttributeType {

    @XmlElement(name = "ItemFeatureQualGid", required = true)
    protected GLogXMLGidType itemFeatureQualGid;
    @XmlElement(name = "ItemFeatureValue", required = true)
    protected String itemFeatureValue;

    /**
     * Gets the value of the itemFeatureQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemFeatureQualGid() {
        return itemFeatureQualGid;
    }

    /**
     * Sets the value of the itemFeatureQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemFeatureQualGid(GLogXMLGidType value) {
        this.itemFeatureQualGid = value;
    }

    /**
     * Gets the value of the itemFeatureValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemFeatureValue() {
        return itemFeatureValue;
    }

    /**
     * Sets the value of the itemFeatureValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemFeatureValue(String value) {
        this.itemFeatureValue = value;
    }

}

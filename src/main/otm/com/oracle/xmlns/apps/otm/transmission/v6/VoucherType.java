
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The Shipments that are sent out in the Voucher are those related to the Invoice.
 *             When the Voucher is sent for a parent Invoice which contains child Invoices,
 *             the Shipment(s) for those child Invoices are sent out in the Voucher of the parent.
 * 
 *             When Voucher is referenced within the context of the AllocationBase element, the child Payment element is omitted
 *             because it already occurs under AllocationBase.Invoice.
 *          
 * 
 * <p>Java class for VoucherType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoucherType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="VoucherGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="VoucherNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AutoApproved" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ApprovedBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/&gt;
 *         &lt;element name="AmountToPay" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="AmountToPayWithTax" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ApprovedDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="VoucherRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VoucherRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VoucherStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Payment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PaymentType" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="VoucherInvoiceLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VoucherInvoiceLineItemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VatAnalysis" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VatAnalysisType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherType", propOrder = {
    "sendReason",
    "voucherGid",
    "voucherNum",
    "autoApproved",
    "approvedBy",
    "exchangeRateInfo",
    "amountToPay",
    "amountToPayWithTax",
    "approvedDate",
    "voucherRefnum",
    "voucherStatus",
    "remark",
    "payment",
    "adjustmentReasonGid",
    "voucherInvoiceLineItem",
    "vatAnalysis",
    "shipment",
    "shipmentGid",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class VoucherType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "VoucherGid", required = true)
    protected GLogXMLGidType voucherGid;
    @XmlElement(name = "VoucherNum")
    protected String voucherNum;
    @XmlElement(name = "AutoApproved", required = true)
    protected String autoApproved;
    @XmlElement(name = "ApprovedBy")
    protected String approvedBy;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "AmountToPay", required = true)
    protected GLogXMLFinancialAmountType amountToPay;
    @XmlElement(name = "AmountToPayWithTax")
    protected GLogXMLFinancialAmountType amountToPayWithTax;
    @XmlElement(name = "ApprovedDate", required = true)
    protected GLogDateTimeType approvedDate;
    @XmlElement(name = "VoucherRefnum")
    protected List<VoucherRefnumType> voucherRefnum;
    @XmlElement(name = "VoucherStatus")
    protected List<StatusType> voucherStatus;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Payment")
    protected PaymentType payment;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "VoucherInvoiceLineItem")
    protected List<VoucherInvoiceLineItemType> voucherInvoiceLineItem;
    @XmlElement(name = "VatAnalysis")
    protected List<VatAnalysisType> vatAnalysis;
    @XmlElement(name = "Shipment")
    protected List<ShipmentType> shipment;
    @XmlElement(name = "ShipmentGid")
    protected List<GLogXMLGidType> shipmentGid;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the voucherGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoucherGid() {
        return voucherGid;
    }

    /**
     * Sets the value of the voucherGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoucherGid(GLogXMLGidType value) {
        this.voucherGid = value;
    }

    /**
     * Gets the value of the voucherNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherNum() {
        return voucherNum;
    }

    /**
     * Sets the value of the voucherNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherNum(String value) {
        this.voucherNum = value;
    }

    /**
     * Gets the value of the autoApproved property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoApproved() {
        return autoApproved;
    }

    /**
     * Sets the value of the autoApproved property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoApproved(String value) {
        this.autoApproved = value;
    }

    /**
     * Gets the value of the approvedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovedBy() {
        return approvedBy;
    }

    /**
     * Sets the value of the approvedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovedBy(String value) {
        this.approvedBy = value;
    }

    /**
     * Gets the value of the exchangeRateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Sets the value of the exchangeRateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Gets the value of the amountToPay property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAmountToPay() {
        return amountToPay;
    }

    /**
     * Sets the value of the amountToPay property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAmountToPay(GLogXMLFinancialAmountType value) {
        this.amountToPay = value;
    }

    /**
     * Gets the value of the amountToPayWithTax property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAmountToPayWithTax() {
        return amountToPayWithTax;
    }

    /**
     * Sets the value of the amountToPayWithTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAmountToPayWithTax(GLogXMLFinancialAmountType value) {
        this.amountToPayWithTax = value;
    }

    /**
     * Gets the value of the approvedDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getApprovedDate() {
        return approvedDate;
    }

    /**
     * Sets the value of the approvedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setApprovedDate(GLogDateTimeType value) {
        this.approvedDate = value;
    }

    /**
     * Gets the value of the voucherRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the voucherRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoucherRefnumType }
     * 
     * 
     */
    public List<VoucherRefnumType> getVoucherRefnum() {
        if (voucherRefnum == null) {
            voucherRefnum = new ArrayList<VoucherRefnumType>();
        }
        return this.voucherRefnum;
    }

    /**
     * Gets the value of the voucherStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the voucherStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getVoucherStatus() {
        if (voucherStatus == null) {
            voucherStatus = new ArrayList<StatusType>();
        }
        return this.voucherStatus;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the payment property.
     * 
     * @return
     *     possible object is
     *     {@link PaymentType }
     *     
     */
    public PaymentType getPayment() {
        return payment;
    }

    /**
     * Sets the value of the payment property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentType }
     *     
     */
    public void setPayment(PaymentType value) {
        this.payment = value;
    }

    /**
     * Gets the value of the adjustmentReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Sets the value of the adjustmentReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Gets the value of the voucherInvoiceLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the voucherInvoiceLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherInvoiceLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoucherInvoiceLineItemType }
     * 
     * 
     */
    public List<VoucherInvoiceLineItemType> getVoucherInvoiceLineItem() {
        if (voucherInvoiceLineItem == null) {
            voucherInvoiceLineItem = new ArrayList<VoucherInvoiceLineItemType>();
        }
        return this.voucherInvoiceLineItem;
    }

    /**
     * Gets the value of the vatAnalysis property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the vatAnalysis property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVatAnalysis().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VatAnalysisType }
     * 
     * 
     */
    public List<VatAnalysisType> getVatAnalysis() {
        if (vatAnalysis == null) {
            vatAnalysis = new ArrayList<VatAnalysisType>();
        }
        return this.vatAnalysis;
    }

    /**
     * Gets the value of the shipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentType }
     * 
     * 
     */
    public List<ShipmentType> getShipment() {
        if (shipment == null) {
            shipment = new ArrayList<ShipmentType>();
        }
        return this.shipment;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getShipmentGid() {
        if (shipmentGid == null) {
            shipmentGid = new ArrayList<GLogXMLGidType>();
        }
        return this.shipmentGid;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the flexFieldCurrencies property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Sets the value of the flexFieldCurrencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}

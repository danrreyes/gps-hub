
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Height contains sub-elements for height unit of measure and value.
 * 
 * <p>Java class for HeightType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HeightType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HeightValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="HeightUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HeightType", propOrder = {
    "heightValue",
    "heightUOMGid"
})
public class HeightType {

    @XmlElement(name = "HeightValue", required = true)
    protected String heightValue;
    @XmlElement(name = "HeightUOMGid", required = true)
    protected GLogXMLGidType heightUOMGid;

    /**
     * Gets the value of the heightValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHeightValue() {
        return heightValue;
    }

    /**
     * Sets the value of the heightValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHeightValue(String value) {
        this.heightValue = value;
    }

    /**
     * Gets the value of the heightUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHeightUOMGid() {
        return heightUOMGid;
    }

    /**
     * Sets the value of the heightUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHeightUOMGid(GLogXMLGidType value) {
        this.heightUOMGid = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Weight is a group of fields for specifying weight.
 *             It includes sub-elements for weight value and unit of measure.
 *          
 * 
 * <p>Java class for WeightType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WeightType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WeightValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="WeightUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WeightType", propOrder = {
    "weightValue",
    "weightUOMGid"
})
public class WeightType {

    @XmlElement(name = "WeightValue", required = true)
    protected String weightValue;
    @XmlElement(name = "WeightUOMGid", required = true)
    protected GLogXMLGidType weightUOMGid;

    /**
     * Gets the value of the weightValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightValue() {
        return weightValue;
    }

    /**
     * Sets the value of the weightValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightValue(String value) {
        this.weightValue = value;
    }

    /**
     * Gets the value of the weightUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWeightUOMGid() {
        return weightUOMGid;
    }

    /**
     * Sets the value of the weightUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWeightUOMGid(GLogXMLGidType value) {
        this.weightUOMGid = value;
    }

}

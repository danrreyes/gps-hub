
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the reply for an appointment query.
 * 
 * <p>Java class for AppointmentQueryReplyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AppointmentQueryReplyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AppointmentQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentQueryType"/&gt;
 *         &lt;element name="AppointmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentInfoType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AppointmentMissing" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentMissingType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AppointmentQueryReplyType", propOrder = {
    "appointmentQuery",
    "appointmentInfo",
    "appointmentMissing"
})
public class AppointmentQueryReplyType {

    @XmlElement(name = "AppointmentQuery", required = true)
    protected AppointmentQueryType appointmentQuery;
    @XmlElement(name = "AppointmentInfo")
    protected List<AppointmentInfoType> appointmentInfo;
    @XmlElement(name = "AppointmentMissing")
    protected List<AppointmentMissingType> appointmentMissing;

    /**
     * Gets the value of the appointmentQuery property.
     * 
     * @return
     *     possible object is
     *     {@link AppointmentQueryType }
     *     
     */
    public AppointmentQueryType getAppointmentQuery() {
        return appointmentQuery;
    }

    /**
     * Sets the value of the appointmentQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link AppointmentQueryType }
     *     
     */
    public void setAppointmentQuery(AppointmentQueryType value) {
        this.appointmentQuery = value;
    }

    /**
     * Gets the value of the appointmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the appointmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppointmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppointmentInfoType }
     * 
     * 
     */
    public List<AppointmentInfoType> getAppointmentInfo() {
        if (appointmentInfo == null) {
            appointmentInfo = new ArrayList<AppointmentInfoType>();
        }
        return this.appointmentInfo;
    }

    /**
     * Gets the value of the appointmentMissing property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the appointmentMissing property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppointmentMissing().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppointmentMissingType }
     * 
     * 
     */
    public List<AppointmentMissingType> getAppointmentMissing() {
        if (appointmentMissing == null) {
            appointmentMissing = new ArrayList<AppointmentMissingType>();
        }
        return this.appointmentMissing;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains a list of child elements to be replaced when using the RC TransactionCode. If this element is not
 *             specified within the object when the TransactionCode is RC, then all of the child objects will be replaced.
 *             Refer to TransactionCode element.
 *          
 * 
 * <p>Java class for ReplaceChildrenType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReplaceChildrenType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ManagedChild" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReplaceChildrenType", propOrder = {
    "managedChild"
})
public class ReplaceChildrenType {

    @XmlElement(name = "ManagedChild")
    protected List<String> managedChild;

    /**
     * Gets the value of the managedChild property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the managedChild property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getManagedChild().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getManagedChild() {
        if (managedChild == null) {
            managedChild = new ArrayList<String>();
        }
        return this.managedChild;
    }

}

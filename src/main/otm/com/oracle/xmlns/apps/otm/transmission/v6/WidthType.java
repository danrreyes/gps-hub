
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Width contains sub-elements for width unit of measture and value.
 * 
 * <p>Java class for WidthType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WidthType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="WidthValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="WidthUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WidthType", propOrder = {
    "widthValue",
    "widthUOMGid"
})
public class WidthType {

    @XmlElement(name = "WidthValue", required = true)
    protected String widthValue;
    @XmlElement(name = "WidthUOMGid", required = true)
    protected GLogXMLGidType widthUOMGid;

    /**
     * Gets the value of the widthValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWidthValue() {
        return widthValue;
    }

    /**
     * Sets the value of the widthValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWidthValue(String value) {
        this.widthValue = value;
    }

    /**
     * Gets the value of the widthUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getWidthUOMGid() {
        return widthUOMGid;
    }

    /**
     * Sets the value of the widthUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setWidthUOMGid(GLogXMLGidType value) {
        this.widthUOMGid = value;
    }

}

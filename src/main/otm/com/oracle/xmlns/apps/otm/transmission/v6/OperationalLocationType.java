
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies a sub location, such as terminals at a port. When the element is specified in the Location, it indicates that the
 *             Location is a sub location of another Location.
 *          
 * 
 * <p>Java class for OperationalLocationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OperationalLocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ParentLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType"/&gt;
 *         &lt;element name="OperLocDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OperLocDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperationalLocationType", propOrder = {
    "parentLocationGid",
    "operLocDetail"
})
public class OperationalLocationType {

    @XmlElement(name = "ParentLocationGid", required = true)
    protected GLogXMLLocGidType parentLocationGid;
    @XmlElement(name = "OperLocDetail")
    protected List<OperLocDetailType> operLocDetail;

    /**
     * Gets the value of the parentLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getParentLocationGid() {
        return parentLocationGid;
    }

    /**
     * Sets the value of the parentLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setParentLocationGid(GLogXMLLocGidType value) {
        this.parentLocationGid = value;
    }

    /**
     * Gets the value of the operLocDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the operLocDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperLocDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperLocDetailType }
     * 
     * 
     */
    public List<OperLocDetailType> getOperLocDetail() {
        if (operLocDetail == null) {
            operLocDetail = new ArrayList<OperLocDetailType>();
        }
        return this.operLocDetail;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains the RouteCode represting the combined RouteCode for the Rule_11 type rail shipments.
 * 
 * <p>Java class for GLogXMLRouteCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLRouteCodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RouteCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLRouteCodeType", propOrder = {
    "routeCode"
})
public class GLogXMLRouteCodeType {

    @XmlElement(name = "RouteCode", required = true)
    protected RouteCodeType routeCode;

    /**
     * Gets the value of the routeCode property.
     * 
     * @return
     *     possible object is
     *     {@link RouteCodeType }
     *     
     */
    public RouteCodeType getRouteCode() {
        return routeCode;
    }

    /**
     * Sets the value of the routeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteCodeType }
     *     
     */
    public void setRouteCode(RouteCodeType value) {
        this.routeCode = value;
    }

}

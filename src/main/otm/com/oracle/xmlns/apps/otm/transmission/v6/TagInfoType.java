
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * The TagInfo element is used to specify tagging information for tracing purposes. When an TransOrder is created with the
 *             tag information, the fields will be copied when the release is created. Similarly if the fields are on the Release,
 *             they will be copied to the Shipment when it is created.
 *          
 * 
 * <p>Java class for TagInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TagInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItemTag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TagInfoType", propOrder = {
    "itemTag1",
    "itemTag2",
    "itemTag3",
    "itemTag4"
})
public class TagInfoType {

    @XmlElement(name = "ItemTag1")
    protected String itemTag1;
    @XmlElement(name = "ItemTag2")
    protected String itemTag2;
    @XmlElement(name = "ItemTag3")
    protected String itemTag3;
    @XmlElement(name = "ItemTag4")
    protected String itemTag4;

    /**
     * Gets the value of the itemTag1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag1() {
        return itemTag1;
    }

    /**
     * Sets the value of the itemTag1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag1(String value) {
        this.itemTag1 = value;
    }

    /**
     * Gets the value of the itemTag2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag2() {
        return itemTag2;
    }

    /**
     * Sets the value of the itemTag2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag2(String value) {
        this.itemTag2 = value;
    }

    /**
     * Gets the value of the itemTag3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag3() {
        return itemTag3;
    }

    /**
     * Sets the value of the itemTag3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag3(String value) {
        this.itemTag3 = value;
    }

    /**
     * Gets the value of the itemTag4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag4() {
        return itemTag4;
    }

    /**
     * Sets the value of the itemTag4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag4(String value) {
        this.itemTag4 = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReleaseAllocType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseAllocType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AllocTypeQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ReleaseAllocShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocShipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseAllocType", propOrder = {
    "allocTypeQualGid",
    "releaseAllocShipment"
})
public class ReleaseAllocType {

    @XmlElement(name = "AllocTypeQualGid", required = true)
    protected GLogXMLGidType allocTypeQualGid;
    @XmlElement(name = "ReleaseAllocShipment")
    protected List<ReleaseAllocShipmentType> releaseAllocShipment;

    /**
     * Gets the value of the allocTypeQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocTypeQualGid() {
        return allocTypeQualGid;
    }

    /**
     * Sets the value of the allocTypeQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocTypeQualGid(GLogXMLGidType value) {
        this.allocTypeQualGid = value;
    }

    /**
     * Gets the value of the releaseAllocShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseAllocShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseAllocShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseAllocShipmentType }
     * 
     * 
     */
    public List<ReleaseAllocShipmentType> getReleaseAllocShipment() {
        if (releaseAllocShipment == null) {
            releaseAllocShipment = new ArrayList<ReleaseAllocShipmentType>();
        }
        return this.releaseAllocShipment;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * PostalCodeRange is a range of postal codes.
 *             Example: 191 to 195.
 *             In this case the HnameComponentGid would be USZIP3.
 *          
 * 
 * <p>Java class for PostalCodeRangeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PostalCodeRangeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HnameComponentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="LowRangeValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="HighRangeValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PostalCodeRangeType", propOrder = {
    "hnameComponentGid",
    "lowRangeValue",
    "highRangeValue"
})
public class PostalCodeRangeType {

    @XmlElement(name = "HnameComponentGid", required = true)
    protected GLogXMLGidType hnameComponentGid;
    @XmlElement(name = "LowRangeValue", required = true)
    protected String lowRangeValue;
    @XmlElement(name = "HighRangeValue", required = true)
    protected String highRangeValue;

    /**
     * Gets the value of the hnameComponentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHnameComponentGid() {
        return hnameComponentGid;
    }

    /**
     * Sets the value of the hnameComponentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHnameComponentGid(GLogXMLGidType value) {
        this.hnameComponentGid = value;
    }

    /**
     * Gets the value of the lowRangeValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLowRangeValue() {
        return lowRangeValue;
    }

    /**
     * Sets the value of the lowRangeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLowRangeValue(String value) {
        this.lowRangeValue = value;
    }

    /**
     * Gets the value of the highRangeValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHighRangeValue() {
        return highRangeValue;
    }

    /**
     * Sets the value of the highRangeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHighRangeValue(String value) {
        this.highRangeValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to identify the location of a port.
 * 
 * <p>Java class for PortIdentifierType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PortIdentifierType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PortIdentifierValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PortIdentifierQualifier" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PortIdentifierType", propOrder = {
    "portIdentifierValue",
    "portIdentifierQualifier"
})
public class PortIdentifierType {

    @XmlElement(name = "PortIdentifierValue", required = true)
    protected String portIdentifierValue;
    @XmlElement(name = "PortIdentifierQualifier", required = true)
    protected String portIdentifierQualifier;

    /**
     * Gets the value of the portIdentifierValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortIdentifierValue() {
        return portIdentifierValue;
    }

    /**
     * Sets the value of the portIdentifierValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortIdentifierValue(String value) {
        this.portIdentifierValue = value;
    }

    /**
     * Gets the value of the portIdentifierQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortIdentifierQualifier() {
        return portIdentifierQualifier;
    }

    /**
     * Sets the value of the portIdentifierQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortIdentifierQualifier(String value) {
        this.portIdentifierQualifier = value;
    }

}

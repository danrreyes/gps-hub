
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLContactRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLContactRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ContactRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactRefType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLContactRefType", propOrder = {
    "contactRef"
})
public class GLogXMLContactRefType {

    @XmlElement(name = "ContactRef", required = true)
    protected ContactRefType contactRef;

    /**
     * Gets the value of the contactRef property.
     * 
     * @return
     *     possible object is
     *     {@link ContactRefType }
     *     
     */
    public ContactRefType getContactRef() {
        return contactRef;
    }

    /**
     * Sets the value of the contactRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRefType }
     *     
     */
    public void setContactRef(ContactRefType value) {
        this.contactRef = value;
    }

}

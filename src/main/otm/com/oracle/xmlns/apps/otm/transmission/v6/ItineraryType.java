
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The itinerary defines the path between two locations. It is described in terms of the legs.
 *             An itinerary can have one or more legs, and consists of the constraints for developing the shipment.
 *          
 * 
 * <p>Java class for ItineraryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItineraryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MinWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MaxWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="CorporationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="XLaneRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}XLaneRefType" minOccurs="0"/&gt;
 *         &lt;element name="IsMultiStop" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="TotalTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="UseDeconsolidationPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UseConsolidationPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MinStopoffWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MaxPoolWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MaxDistBtwPickupStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="MaxDistBtwDeliveryStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="RadiusForPickupStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="RadiusPctForPickupStops" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RadiusForDeliveryStops" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="RadiusPctForDeliveryStops" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalStopsConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PickupStopsConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryStopsConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MinTLWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MinTLUsagePercentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxSmallDirectWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MaxXDockWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="IsMatchConsolPoolToSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsMatchDeconsolPoolToDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsDestBundlePreferred" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazmatModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Rank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IsRule11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IncoTermProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DepotProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PaymentMethodProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItineraryDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItineraryType", propOrder = {
    "itineraryGid",
    "transactionCode",
    "replaceChildren",
    "itineraryName",
    "description",
    "minWeightVolume",
    "maxWeightVolume",
    "corporationProfileGid",
    "calendarGid",
    "xLaneRef",
    "isMultiStop",
    "perspective",
    "totalCost",
    "totalTransitTime",
    "useDeconsolidationPool",
    "useConsolidationPool",
    "minStopoffWeightVolume",
    "maxPoolWeightVolume",
    "maxDistBtwPickupStops",
    "maxDistBtwDeliveryStops",
    "radiusForPickupStops",
    "radiusPctForPickupStops",
    "radiusForDeliveryStops",
    "radiusPctForDeliveryStops",
    "totalStopsConstraint",
    "pickupStopsConstraint",
    "deliveryStopsConstraint",
    "minTLWeightVolume",
    "minTLUsagePercentage",
    "maxSmallDirectWeightVolume",
    "maxXDockWeightVolume",
    "isMatchConsolPoolToSource",
    "isMatchDeconsolPoolToDest",
    "isDestBundlePreferred",
    "hazmatModeGid",
    "rank",
    "isActive",
    "itineraryType",
    "isRule11",
    "incoTermProfileGid",
    "depotProfileGid",
    "paymentMethodProfileGid",
    "itineraryDetail",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class ItineraryType
    extends OTMTransactionIn
{

    @XmlElement(name = "ItineraryGid", required = true)
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "ItineraryName")
    protected String itineraryName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "MinWeightVolume")
    protected WeightVolumeType minWeightVolume;
    @XmlElement(name = "MaxWeightVolume")
    protected WeightVolumeType maxWeightVolume;
    @XmlElement(name = "CorporationProfileGid")
    protected GLogXMLGidType corporationProfileGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "XLaneRef")
    protected XLaneRefType xLaneRef;
    @XmlElement(name = "IsMultiStop")
    protected String isMultiStop;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "TotalCost")
    protected GLogXMLFinancialAmountType totalCost;
    @XmlElement(name = "TotalTransitTime")
    protected GLogXMLDurationType totalTransitTime;
    @XmlElement(name = "UseDeconsolidationPool")
    protected String useDeconsolidationPool;
    @XmlElement(name = "UseConsolidationPool")
    protected String useConsolidationPool;
    @XmlElement(name = "MinStopoffWeightVolume")
    protected WeightVolumeType minStopoffWeightVolume;
    @XmlElement(name = "MaxPoolWeightVolume")
    protected WeightVolumeType maxPoolWeightVolume;
    @XmlElement(name = "MaxDistBtwPickupStops")
    protected GLogXMLDistanceType maxDistBtwPickupStops;
    @XmlElement(name = "MaxDistBtwDeliveryStops")
    protected GLogXMLDistanceType maxDistBtwDeliveryStops;
    @XmlElement(name = "RadiusForPickupStops")
    protected GLogXMLDistanceType radiusForPickupStops;
    @XmlElement(name = "RadiusPctForPickupStops")
    protected String radiusPctForPickupStops;
    @XmlElement(name = "RadiusForDeliveryStops")
    protected GLogXMLDistanceType radiusForDeliveryStops;
    @XmlElement(name = "RadiusPctForDeliveryStops")
    protected String radiusPctForDeliveryStops;
    @XmlElement(name = "TotalStopsConstraint")
    protected String totalStopsConstraint;
    @XmlElement(name = "PickupStopsConstraint")
    protected String pickupStopsConstraint;
    @XmlElement(name = "DeliveryStopsConstraint")
    protected String deliveryStopsConstraint;
    @XmlElement(name = "MinTLWeightVolume")
    protected WeightVolumeType minTLWeightVolume;
    @XmlElement(name = "MinTLUsagePercentage")
    protected String minTLUsagePercentage;
    @XmlElement(name = "MaxSmallDirectWeightVolume")
    protected WeightVolumeType maxSmallDirectWeightVolume;
    @XmlElement(name = "MaxXDockWeightVolume")
    protected WeightVolumeType maxXDockWeightVolume;
    @XmlElement(name = "IsMatchConsolPoolToSource")
    protected String isMatchConsolPoolToSource;
    @XmlElement(name = "IsMatchDeconsolPoolToDest")
    protected String isMatchDeconsolPoolToDest;
    @XmlElement(name = "IsDestBundlePreferred")
    protected String isDestBundlePreferred;
    @XmlElement(name = "HazmatModeGid")
    protected GLogXMLGidType hazmatModeGid;
    @XmlElement(name = "Rank")
    protected String rank;
    @XmlElement(name = "IsActive")
    protected String isActive;
    @XmlElement(name = "ItineraryType", required = true)
    protected String itineraryType;
    @XmlElement(name = "IsRule11")
    protected String isRule11;
    @XmlElement(name = "IncoTermProfileGid")
    protected GLogXMLGidType incoTermProfileGid;
    @XmlElement(name = "DepotProfileGid")
    protected GLogXMLGidType depotProfileGid;
    @XmlElement(name = "PaymentMethodProfileGid")
    protected GLogXMLGidType paymentMethodProfileGid;
    @XmlElement(name = "ItineraryDetail")
    protected List<ItineraryDetailType> itineraryDetail;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Gets the value of the itineraryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Sets the value of the itineraryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the itineraryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItineraryName() {
        return itineraryName;
    }

    /**
     * Sets the value of the itineraryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItineraryName(String value) {
        this.itineraryName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the minWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMinWeightVolume() {
        return minWeightVolume;
    }

    /**
     * Sets the value of the minWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMinWeightVolume(WeightVolumeType value) {
        this.minWeightVolume = value;
    }

    /**
     * Gets the value of the maxWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxWeightVolume() {
        return maxWeightVolume;
    }

    /**
     * Sets the value of the maxWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxWeightVolume(WeightVolumeType value) {
        this.maxWeightVolume = value;
    }

    /**
     * Gets the value of the corporationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCorporationProfileGid() {
        return corporationProfileGid;
    }

    /**
     * Sets the value of the corporationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCorporationProfileGid(GLogXMLGidType value) {
        this.corporationProfileGid = value;
    }

    /**
     * Gets the value of the calendarGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Sets the value of the calendarGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Gets the value of the xLaneRef property.
     * 
     * @return
     *     possible object is
     *     {@link XLaneRefType }
     *     
     */
    public XLaneRefType getXLaneRef() {
        return xLaneRef;
    }

    /**
     * Sets the value of the xLaneRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLaneRefType }
     *     
     */
    public void setXLaneRef(XLaneRefType value) {
        this.xLaneRef = value;
    }

    /**
     * Gets the value of the isMultiStop property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMultiStop() {
        return isMultiStop;
    }

    /**
     * Sets the value of the isMultiStop property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMultiStop(String value) {
        this.isMultiStop = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the totalCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalCost() {
        return totalCost;
    }

    /**
     * Sets the value of the totalCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalCost(GLogXMLFinancialAmountType value) {
        this.totalCost = value;
    }

    /**
     * Gets the value of the totalTransitTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTotalTransitTime() {
        return totalTransitTime;
    }

    /**
     * Sets the value of the totalTransitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTotalTransitTime(GLogXMLDurationType value) {
        this.totalTransitTime = value;
    }

    /**
     * Gets the value of the useDeconsolidationPool property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseDeconsolidationPool() {
        return useDeconsolidationPool;
    }

    /**
     * Sets the value of the useDeconsolidationPool property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseDeconsolidationPool(String value) {
        this.useDeconsolidationPool = value;
    }

    /**
     * Gets the value of the useConsolidationPool property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseConsolidationPool() {
        return useConsolidationPool;
    }

    /**
     * Sets the value of the useConsolidationPool property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseConsolidationPool(String value) {
        this.useConsolidationPool = value;
    }

    /**
     * Gets the value of the minStopoffWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMinStopoffWeightVolume() {
        return minStopoffWeightVolume;
    }

    /**
     * Sets the value of the minStopoffWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMinStopoffWeightVolume(WeightVolumeType value) {
        this.minStopoffWeightVolume = value;
    }

    /**
     * Gets the value of the maxPoolWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxPoolWeightVolume() {
        return maxPoolWeightVolume;
    }

    /**
     * Sets the value of the maxPoolWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxPoolWeightVolume(WeightVolumeType value) {
        this.maxPoolWeightVolume = value;
    }

    /**
     * Gets the value of the maxDistBtwPickupStops property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getMaxDistBtwPickupStops() {
        return maxDistBtwPickupStops;
    }

    /**
     * Sets the value of the maxDistBtwPickupStops property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setMaxDistBtwPickupStops(GLogXMLDistanceType value) {
        this.maxDistBtwPickupStops = value;
    }

    /**
     * Gets the value of the maxDistBtwDeliveryStops property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getMaxDistBtwDeliveryStops() {
        return maxDistBtwDeliveryStops;
    }

    /**
     * Sets the value of the maxDistBtwDeliveryStops property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setMaxDistBtwDeliveryStops(GLogXMLDistanceType value) {
        this.maxDistBtwDeliveryStops = value;
    }

    /**
     * Gets the value of the radiusForPickupStops property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getRadiusForPickupStops() {
        return radiusForPickupStops;
    }

    /**
     * Sets the value of the radiusForPickupStops property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setRadiusForPickupStops(GLogXMLDistanceType value) {
        this.radiusForPickupStops = value;
    }

    /**
     * Gets the value of the radiusPctForPickupStops property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadiusPctForPickupStops() {
        return radiusPctForPickupStops;
    }

    /**
     * Sets the value of the radiusPctForPickupStops property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadiusPctForPickupStops(String value) {
        this.radiusPctForPickupStops = value;
    }

    /**
     * Gets the value of the radiusForDeliveryStops property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getRadiusForDeliveryStops() {
        return radiusForDeliveryStops;
    }

    /**
     * Sets the value of the radiusForDeliveryStops property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setRadiusForDeliveryStops(GLogXMLDistanceType value) {
        this.radiusForDeliveryStops = value;
    }

    /**
     * Gets the value of the radiusPctForDeliveryStops property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadiusPctForDeliveryStops() {
        return radiusPctForDeliveryStops;
    }

    /**
     * Sets the value of the radiusPctForDeliveryStops property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadiusPctForDeliveryStops(String value) {
        this.radiusPctForDeliveryStops = value;
    }

    /**
     * Gets the value of the totalStopsConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalStopsConstraint() {
        return totalStopsConstraint;
    }

    /**
     * Sets the value of the totalStopsConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalStopsConstraint(String value) {
        this.totalStopsConstraint = value;
    }

    /**
     * Gets the value of the pickupStopsConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupStopsConstraint() {
        return pickupStopsConstraint;
    }

    /**
     * Sets the value of the pickupStopsConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupStopsConstraint(String value) {
        this.pickupStopsConstraint = value;
    }

    /**
     * Gets the value of the deliveryStopsConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryStopsConstraint() {
        return deliveryStopsConstraint;
    }

    /**
     * Sets the value of the deliveryStopsConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryStopsConstraint(String value) {
        this.deliveryStopsConstraint = value;
    }

    /**
     * Gets the value of the minTLWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMinTLWeightVolume() {
        return minTLWeightVolume;
    }

    /**
     * Sets the value of the minTLWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMinTLWeightVolume(WeightVolumeType value) {
        this.minTLWeightVolume = value;
    }

    /**
     * Gets the value of the minTLUsagePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinTLUsagePercentage() {
        return minTLUsagePercentage;
    }

    /**
     * Sets the value of the minTLUsagePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinTLUsagePercentage(String value) {
        this.minTLUsagePercentage = value;
    }

    /**
     * Gets the value of the maxSmallDirectWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxSmallDirectWeightVolume() {
        return maxSmallDirectWeightVolume;
    }

    /**
     * Sets the value of the maxSmallDirectWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxSmallDirectWeightVolume(WeightVolumeType value) {
        this.maxSmallDirectWeightVolume = value;
    }

    /**
     * Gets the value of the maxXDockWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getMaxXDockWeightVolume() {
        return maxXDockWeightVolume;
    }

    /**
     * Sets the value of the maxXDockWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setMaxXDockWeightVolume(WeightVolumeType value) {
        this.maxXDockWeightVolume = value;
    }

    /**
     * Gets the value of the isMatchConsolPoolToSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMatchConsolPoolToSource() {
        return isMatchConsolPoolToSource;
    }

    /**
     * Sets the value of the isMatchConsolPoolToSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMatchConsolPoolToSource(String value) {
        this.isMatchConsolPoolToSource = value;
    }

    /**
     * Gets the value of the isMatchDeconsolPoolToDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMatchDeconsolPoolToDest() {
        return isMatchDeconsolPoolToDest;
    }

    /**
     * Sets the value of the isMatchDeconsolPoolToDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMatchDeconsolPoolToDest(String value) {
        this.isMatchDeconsolPoolToDest = value;
    }

    /**
     * Gets the value of the isDestBundlePreferred property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDestBundlePreferred() {
        return isDestBundlePreferred;
    }

    /**
     * Sets the value of the isDestBundlePreferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDestBundlePreferred(String value) {
        this.isDestBundlePreferred = value;
    }

    /**
     * Gets the value of the hazmatModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatModeGid() {
        return hazmatModeGid;
    }

    /**
     * Sets the value of the hazmatModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatModeGid(GLogXMLGidType value) {
        this.hazmatModeGid = value;
    }

    /**
     * Gets the value of the rank property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRank() {
        return rank;
    }

    /**
     * Sets the value of the rank property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRank(String value) {
        this.rank = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the itineraryType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItineraryType() {
        return itineraryType;
    }

    /**
     * Sets the value of the itineraryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItineraryType(String value) {
        this.itineraryType = value;
    }

    /**
     * Gets the value of the isRule11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRule11() {
        return isRule11;
    }

    /**
     * Sets the value of the isRule11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRule11(String value) {
        this.isRule11 = value;
    }

    /**
     * Gets the value of the incoTermProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermProfileGid() {
        return incoTermProfileGid;
    }

    /**
     * Sets the value of the incoTermProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermProfileGid(GLogXMLGidType value) {
        this.incoTermProfileGid = value;
    }

    /**
     * Gets the value of the depotProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDepotProfileGid() {
        return depotProfileGid;
    }

    /**
     * Sets the value of the depotProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDepotProfileGid(GLogXMLGidType value) {
        this.depotProfileGid = value;
    }

    /**
     * Gets the value of the paymentMethodProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodProfileGid() {
        return paymentMethodProfileGid;
    }

    /**
     * Sets the value of the paymentMethodProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodProfileGid(GLogXMLGidType value) {
        this.paymentMethodProfileGid = value;
    }

    /**
     * Gets the value of the itineraryDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itineraryDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItineraryDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItineraryDetailType }
     * 
     * 
     */
    public List<ItineraryDetailType> getItineraryDetail() {
        if (itineraryDetail == null) {
            itineraryDetail = new ArrayList<ItineraryDetailType>();
        }
        return this.itineraryDetail;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Location override reference.
 * 
 * <p>Java class for LocationOverrideRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationOverrideRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="LocationOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="LocationOverride"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LocationOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                   &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                   &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationOverrideRefType", propOrder = {
    "locationOverrideGid",
    "locationOverride"
})
public class LocationOverrideRefType {

    @XmlElement(name = "LocationOverrideGid")
    protected GLogXMLGidType locationOverrideGid;
    @XmlElement(name = "LocationOverride")
    protected LocationOverrideRefType.LocationOverride locationOverride;

    /**
     * Gets the value of the locationOverrideGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationOverrideGid() {
        return locationOverrideGid;
    }

    /**
     * Sets the value of the locationOverrideGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationOverrideGid(GLogXMLGidType value) {
        this.locationOverrideGid = value;
    }

    /**
     * Gets the value of the locationOverride property.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideRefType.LocationOverride }
     *     
     */
    public LocationOverrideRefType.LocationOverride getLocationOverride() {
        return locationOverride;
    }

    /**
     * Sets the value of the locationOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideRefType.LocationOverride }
     *     
     */
    public void setLocationOverride(LocationOverrideRefType.LocationOverride value) {
        this.locationOverride = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LocationOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationOverrideGid",
        "transactionCode",
        "locationGid",
        "locationOverrideInfo"
    })
    public static class LocationOverride {

        @XmlElement(name = "LocationOverrideGid", required = true)
        protected GLogXMLGidType locationOverrideGid;
        @XmlElement(name = "TransactionCode", required = true)
        @XmlSchemaType(name = "string")
        protected TransactionCodeType transactionCode;
        @XmlElement(name = "LocationGid", required = true)
        protected GLogXMLGidType locationGid;
        @XmlElement(name = "LocationOverrideInfo")
        protected LocationOverrideInfoType locationOverrideInfo;

        /**
         * Gets the value of the locationOverrideGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationOverrideGid() {
            return locationOverrideGid;
        }

        /**
         * Sets the value of the locationOverrideGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationOverrideGid(GLogXMLGidType value) {
            this.locationOverrideGid = value;
        }

        /**
         * Gets the value of the transactionCode property.
         * 
         * @return
         *     possible object is
         *     {@link TransactionCodeType }
         *     
         */
        public TransactionCodeType getTransactionCode() {
            return transactionCode;
        }

        /**
         * Sets the value of the transactionCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransactionCodeType }
         *     
         */
        public void setTransactionCode(TransactionCodeType value) {
            this.transactionCode = value;
        }

        /**
         * Gets the value of the locationGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Sets the value of the locationGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

        /**
         * Gets the value of the locationOverrideInfo property.
         * 
         * @return
         *     possible object is
         *     {@link LocationOverrideInfoType }
         *     
         */
        public LocationOverrideInfoType getLocationOverrideInfo() {
            return locationOverrideInfo;
        }

        /**
         * Sets the value of the locationOverrideInfo property.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationOverrideInfoType }
         *     
         */
        public void setLocationOverrideInfo(LocationOverrideInfoType value) {
            this.locationOverrideInfo = value;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             OceanLineItem is an element of OceanDetail used to specify invoice line items
 *             for ocean invoices.
 *          
 * 
 * <p>Java class for OceanLineItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OceanLineItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AssignedNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CommonInvoiceLineElements" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommonInvoiceLineElementsType"/&gt;
 *         &lt;element name="ExportImportLicense" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExportImportLicenseType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OceanLineItemType", propOrder = {
    "assignedNum",
    "commonInvoiceLineElements",
    "exportImportLicense"
})
public class OceanLineItemType {

    @XmlElement(name = "AssignedNum", required = true)
    protected String assignedNum;
    @XmlElement(name = "CommonInvoiceLineElements", required = true)
    protected CommonInvoiceLineElementsType commonInvoiceLineElements;
    @XmlElement(name = "ExportImportLicense")
    protected ExportImportLicenseType exportImportLicense;

    /**
     * Gets the value of the assignedNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignedNum() {
        return assignedNum;
    }

    /**
     * Sets the value of the assignedNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignedNum(String value) {
        this.assignedNum = value;
    }

    /**
     * Gets the value of the commonInvoiceLineElements property.
     * 
     * @return
     *     possible object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public CommonInvoiceLineElementsType getCommonInvoiceLineElements() {
        return commonInvoiceLineElements;
    }

    /**
     * Sets the value of the commonInvoiceLineElements property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonInvoiceLineElementsType }
     *     
     */
    public void setCommonInvoiceLineElements(CommonInvoiceLineElementsType value) {
        this.commonInvoiceLineElements = value;
    }

    /**
     * Gets the value of the exportImportLicense property.
     * 
     * @return
     *     possible object is
     *     {@link ExportImportLicenseType }
     *     
     */
    public ExportImportLicenseType getExportImportLicense() {
        return exportImportLicense;
    }

    /**
     * Sets the value of the exportImportLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExportImportLicenseType }
     *     
     */
    public void setExportImportLicense(ExportImportLicenseType value) {
        this.exportImportLicense = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains common hazardous materials information.
 * 
 * <p>Java class for HazmatCommonInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HazmatCommonInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HazActivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NetExplosiveContentWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="IsLimitedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsReportableQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RqTechnicalName1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/&gt;
 *         &lt;element name="RqTechnicalName2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsNos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NosTechnicalName1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/&gt;
 *         &lt;element name="NosTechnicalName2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsToxicInhalation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InhalationHazardZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPassengerAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCommercialAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlashPoint" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/&gt;
 *         &lt;element name="EmergencyTemperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/&gt;
 *         &lt;element name="ControlTemperature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTemperatureType" minOccurs="0"/&gt;
 *         &lt;element name="IsMarinePollutant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MpTechnicalName1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/&gt;
 *         &lt;element name="MpTechnicalName2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TechnicalNameGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsOilContained" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAllPacked" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChemicalFormula" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ConcentrationPercent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CritSafetyIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IDGAddDescInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OuterPackagingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OuterPackingCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazmatPackageType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackingCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PackingInstructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PhysicalForm" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RadioactiveLabel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RadPackaging" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Radioactivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RadioactivityUnits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Radionuclide" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SubstanceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SurfaceReading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransportIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaterialsType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Units" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExternalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsOverpack" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatCommonInfoType", propOrder = {
    "hazActivity",
    "description",
    "netExplosiveContentWeight",
    "isLimitedQuantity",
    "isReportableQuantity",
    "rqTechnicalName1Gid",
    "rqTechnicalName2Gid",
    "isNos",
    "nosTechnicalName1Gid",
    "nosTechnicalName2Gid",
    "isToxicInhalation",
    "inhalationHazardZone",
    "isPassengerAircraftForbid",
    "isCommercialAircraftForbid",
    "flashPoint",
    "emergencyTemperature",
    "controlTemperature",
    "isMarinePollutant",
    "mpTechnicalName1Gid",
    "mpTechnicalName2Gid",
    "isOilContained",
    "isAllPacked",
    "authorization",
    "chemicalFormula",
    "concentrationPercent",
    "critSafetyIndex",
    "idgAddDescInfo",
    "maxQuantity",
    "outerPackagingType",
    "outerPackingCount",
    "hazmatPackageType",
    "packingCount",
    "packingInstructions",
    "physicalForm",
    "qValue",
    "hazQuantity",
    "radioactiveLabel",
    "radPackaging",
    "radioactivity",
    "radioactivityUnits",
    "radionuclide",
    "substanceNumber",
    "surfaceReading",
    "transportIndex",
    "materialsType",
    "units",
    "externalCode",
    "isOverpack"
})
public class HazmatCommonInfoType {

    @XmlElement(name = "HazActivity")
    protected String hazActivity;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "NetExplosiveContentWeight")
    protected GLogXMLWeightType netExplosiveContentWeight;
    @XmlElement(name = "IsLimitedQuantity")
    protected String isLimitedQuantity;
    @XmlElement(name = "IsReportableQuantity")
    protected String isReportableQuantity;
    @XmlElement(name = "RqTechnicalName1Gid")
    protected TechnicalNameGidType rqTechnicalName1Gid;
    @XmlElement(name = "RqTechnicalName2Gid")
    protected TechnicalNameGidType rqTechnicalName2Gid;
    @XmlElement(name = "IsNos")
    protected String isNos;
    @XmlElement(name = "NosTechnicalName1Gid")
    protected TechnicalNameGidType nosTechnicalName1Gid;
    @XmlElement(name = "NosTechnicalName2Gid")
    protected TechnicalNameGidType nosTechnicalName2Gid;
    @XmlElement(name = "IsToxicInhalation")
    protected String isToxicInhalation;
    @XmlElement(name = "InhalationHazardZone")
    protected String inhalationHazardZone;
    @XmlElement(name = "IsPassengerAircraftForbid")
    protected String isPassengerAircraftForbid;
    @XmlElement(name = "IsCommercialAircraftForbid")
    protected String isCommercialAircraftForbid;
    @XmlElement(name = "FlashPoint")
    protected GLogXMLTemperatureType flashPoint;
    @XmlElement(name = "EmergencyTemperature")
    protected GLogXMLTemperatureType emergencyTemperature;
    @XmlElement(name = "ControlTemperature")
    protected GLogXMLTemperatureType controlTemperature;
    @XmlElement(name = "IsMarinePollutant")
    protected String isMarinePollutant;
    @XmlElement(name = "MpTechnicalName1Gid")
    protected TechnicalNameGidType mpTechnicalName1Gid;
    @XmlElement(name = "MpTechnicalName2Gid")
    protected TechnicalNameGidType mpTechnicalName2Gid;
    @XmlElement(name = "IsOilContained")
    protected String isOilContained;
    @XmlElement(name = "IsAllPacked")
    protected String isAllPacked;
    @XmlElement(name = "Authorization")
    protected String authorization;
    @XmlElement(name = "ChemicalFormula")
    protected String chemicalFormula;
    @XmlElement(name = "ConcentrationPercent")
    protected String concentrationPercent;
    @XmlElement(name = "CritSafetyIndex")
    protected String critSafetyIndex;
    @XmlElement(name = "IDGAddDescInfo")
    protected String idgAddDescInfo;
    @XmlElement(name = "MaxQuantity")
    protected String maxQuantity;
    @XmlElement(name = "OuterPackagingType")
    protected String outerPackagingType;
    @XmlElement(name = "OuterPackingCount")
    protected String outerPackingCount;
    @XmlElement(name = "HazmatPackageType")
    protected String hazmatPackageType;
    @XmlElement(name = "PackingCount", required = true)
    protected String packingCount;
    @XmlElement(name = "PackingInstructions")
    protected String packingInstructions;
    @XmlElement(name = "PhysicalForm")
    protected String physicalForm;
    @XmlElement(name = "QValue")
    protected String qValue;
    @XmlElement(name = "HazQuantity")
    protected String hazQuantity;
    @XmlElement(name = "RadioactiveLabel")
    protected String radioactiveLabel;
    @XmlElement(name = "RadPackaging")
    protected String radPackaging;
    @XmlElement(name = "Radioactivity")
    protected String radioactivity;
    @XmlElement(name = "RadioactivityUnits")
    protected String radioactivityUnits;
    @XmlElement(name = "Radionuclide")
    protected String radionuclide;
    @XmlElement(name = "SubstanceNumber")
    protected String substanceNumber;
    @XmlElement(name = "SurfaceReading")
    protected String surfaceReading;
    @XmlElement(name = "TransportIndex")
    protected String transportIndex;
    @XmlElement(name = "MaterialsType")
    protected String materialsType;
    @XmlElement(name = "Units")
    protected String units;
    @XmlElement(name = "ExternalCode")
    protected String externalCode;
    @XmlElement(name = "IsOverpack")
    protected String isOverpack;

    /**
     * Gets the value of the hazActivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazActivity() {
        return hazActivity;
    }

    /**
     * Sets the value of the hazActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazActivity(String value) {
        this.hazActivity = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the netExplosiveContentWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getNetExplosiveContentWeight() {
        return netExplosiveContentWeight;
    }

    /**
     * Sets the value of the netExplosiveContentWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setNetExplosiveContentWeight(GLogXMLWeightType value) {
        this.netExplosiveContentWeight = value;
    }

    /**
     * Gets the value of the isLimitedQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLimitedQuantity() {
        return isLimitedQuantity;
    }

    /**
     * Sets the value of the isLimitedQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLimitedQuantity(String value) {
        this.isLimitedQuantity = value;
    }

    /**
     * Gets the value of the isReportableQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsReportableQuantity() {
        return isReportableQuantity;
    }

    /**
     * Sets the value of the isReportableQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsReportableQuantity(String value) {
        this.isReportableQuantity = value;
    }

    /**
     * Gets the value of the rqTechnicalName1Gid property.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getRqTechnicalName1Gid() {
        return rqTechnicalName1Gid;
    }

    /**
     * Sets the value of the rqTechnicalName1Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setRqTechnicalName1Gid(TechnicalNameGidType value) {
        this.rqTechnicalName1Gid = value;
    }

    /**
     * Gets the value of the rqTechnicalName2Gid property.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getRqTechnicalName2Gid() {
        return rqTechnicalName2Gid;
    }

    /**
     * Sets the value of the rqTechnicalName2Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setRqTechnicalName2Gid(TechnicalNameGidType value) {
        this.rqTechnicalName2Gid = value;
    }

    /**
     * Gets the value of the isNos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNos() {
        return isNos;
    }

    /**
     * Sets the value of the isNos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNos(String value) {
        this.isNos = value;
    }

    /**
     * Gets the value of the nosTechnicalName1Gid property.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getNosTechnicalName1Gid() {
        return nosTechnicalName1Gid;
    }

    /**
     * Sets the value of the nosTechnicalName1Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setNosTechnicalName1Gid(TechnicalNameGidType value) {
        this.nosTechnicalName1Gid = value;
    }

    /**
     * Gets the value of the nosTechnicalName2Gid property.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getNosTechnicalName2Gid() {
        return nosTechnicalName2Gid;
    }

    /**
     * Sets the value of the nosTechnicalName2Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setNosTechnicalName2Gid(TechnicalNameGidType value) {
        this.nosTechnicalName2Gid = value;
    }

    /**
     * Gets the value of the isToxicInhalation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsToxicInhalation() {
        return isToxicInhalation;
    }

    /**
     * Sets the value of the isToxicInhalation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsToxicInhalation(String value) {
        this.isToxicInhalation = value;
    }

    /**
     * Gets the value of the inhalationHazardZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInhalationHazardZone() {
        return inhalationHazardZone;
    }

    /**
     * Sets the value of the inhalationHazardZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInhalationHazardZone(String value) {
        this.inhalationHazardZone = value;
    }

    /**
     * Gets the value of the isPassengerAircraftForbid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPassengerAircraftForbid() {
        return isPassengerAircraftForbid;
    }

    /**
     * Sets the value of the isPassengerAircraftForbid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPassengerAircraftForbid(String value) {
        this.isPassengerAircraftForbid = value;
    }

    /**
     * Gets the value of the isCommercialAircraftForbid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCommercialAircraftForbid() {
        return isCommercialAircraftForbid;
    }

    /**
     * Sets the value of the isCommercialAircraftForbid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCommercialAircraftForbid(String value) {
        this.isCommercialAircraftForbid = value;
    }

    /**
     * Gets the value of the flashPoint property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getFlashPoint() {
        return flashPoint;
    }

    /**
     * Sets the value of the flashPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setFlashPoint(GLogXMLTemperatureType value) {
        this.flashPoint = value;
    }

    /**
     * Gets the value of the emergencyTemperature property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getEmergencyTemperature() {
        return emergencyTemperature;
    }

    /**
     * Sets the value of the emergencyTemperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setEmergencyTemperature(GLogXMLTemperatureType value) {
        this.emergencyTemperature = value;
    }

    /**
     * Gets the value of the controlTemperature property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public GLogXMLTemperatureType getControlTemperature() {
        return controlTemperature;
    }

    /**
     * Sets the value of the controlTemperature property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLTemperatureType }
     *     
     */
    public void setControlTemperature(GLogXMLTemperatureType value) {
        this.controlTemperature = value;
    }

    /**
     * Gets the value of the isMarinePollutant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMarinePollutant() {
        return isMarinePollutant;
    }

    /**
     * Sets the value of the isMarinePollutant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMarinePollutant(String value) {
        this.isMarinePollutant = value;
    }

    /**
     * Gets the value of the mpTechnicalName1Gid property.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getMpTechnicalName1Gid() {
        return mpTechnicalName1Gid;
    }

    /**
     * Sets the value of the mpTechnicalName1Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setMpTechnicalName1Gid(TechnicalNameGidType value) {
        this.mpTechnicalName1Gid = value;
    }

    /**
     * Gets the value of the mpTechnicalName2Gid property.
     * 
     * @return
     *     possible object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public TechnicalNameGidType getMpTechnicalName2Gid() {
        return mpTechnicalName2Gid;
    }

    /**
     * Sets the value of the mpTechnicalName2Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link TechnicalNameGidType }
     *     
     */
    public void setMpTechnicalName2Gid(TechnicalNameGidType value) {
        this.mpTechnicalName2Gid = value;
    }

    /**
     * Gets the value of the isOilContained property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOilContained() {
        return isOilContained;
    }

    /**
     * Sets the value of the isOilContained property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOilContained(String value) {
        this.isOilContained = value;
    }

    /**
     * Gets the value of the isAllPacked property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllPacked() {
        return isAllPacked;
    }

    /**
     * Sets the value of the isAllPacked property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllPacked(String value) {
        this.isAllPacked = value;
    }

    /**
     * Gets the value of the authorization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Sets the value of the authorization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

    /**
     * Gets the value of the chemicalFormula property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChemicalFormula() {
        return chemicalFormula;
    }

    /**
     * Sets the value of the chemicalFormula property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChemicalFormula(String value) {
        this.chemicalFormula = value;
    }

    /**
     * Gets the value of the concentrationPercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConcentrationPercent() {
        return concentrationPercent;
    }

    /**
     * Sets the value of the concentrationPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConcentrationPercent(String value) {
        this.concentrationPercent = value;
    }

    /**
     * Gets the value of the critSafetyIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCritSafetyIndex() {
        return critSafetyIndex;
    }

    /**
     * Sets the value of the critSafetyIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCritSafetyIndex(String value) {
        this.critSafetyIndex = value;
    }

    /**
     * Gets the value of the idgAddDescInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDGAddDescInfo() {
        return idgAddDescInfo;
    }

    /**
     * Sets the value of the idgAddDescInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDGAddDescInfo(String value) {
        this.idgAddDescInfo = value;
    }

    /**
     * Gets the value of the maxQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxQuantity() {
        return maxQuantity;
    }

    /**
     * Sets the value of the maxQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxQuantity(String value) {
        this.maxQuantity = value;
    }

    /**
     * Gets the value of the outerPackagingType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOuterPackagingType() {
        return outerPackagingType;
    }

    /**
     * Sets the value of the outerPackagingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOuterPackagingType(String value) {
        this.outerPackagingType = value;
    }

    /**
     * Gets the value of the outerPackingCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOuterPackingCount() {
        return outerPackingCount;
    }

    /**
     * Sets the value of the outerPackingCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOuterPackingCount(String value) {
        this.outerPackingCount = value;
    }

    /**
     * Gets the value of the hazmatPackageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazmatPackageType() {
        return hazmatPackageType;
    }

    /**
     * Sets the value of the hazmatPackageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazmatPackageType(String value) {
        this.hazmatPackageType = value;
    }

    /**
     * Gets the value of the packingCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackingCount() {
        return packingCount;
    }

    /**
     * Sets the value of the packingCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackingCount(String value) {
        this.packingCount = value;
    }

    /**
     * Gets the value of the packingInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackingInstructions() {
        return packingInstructions;
    }

    /**
     * Sets the value of the packingInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackingInstructions(String value) {
        this.packingInstructions = value;
    }

    /**
     * Gets the value of the physicalForm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhysicalForm() {
        return physicalForm;
    }

    /**
     * Sets the value of the physicalForm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhysicalForm(String value) {
        this.physicalForm = value;
    }

    /**
     * Gets the value of the qValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQValue() {
        return qValue;
    }

    /**
     * Sets the value of the qValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQValue(String value) {
        this.qValue = value;
    }

    /**
     * Gets the value of the hazQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazQuantity() {
        return hazQuantity;
    }

    /**
     * Sets the value of the hazQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazQuantity(String value) {
        this.hazQuantity = value;
    }

    /**
     * Gets the value of the radioactiveLabel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadioactiveLabel() {
        return radioactiveLabel;
    }

    /**
     * Sets the value of the radioactiveLabel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadioactiveLabel(String value) {
        this.radioactiveLabel = value;
    }

    /**
     * Gets the value of the radPackaging property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadPackaging() {
        return radPackaging;
    }

    /**
     * Sets the value of the radPackaging property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadPackaging(String value) {
        this.radPackaging = value;
    }

    /**
     * Gets the value of the radioactivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadioactivity() {
        return radioactivity;
    }

    /**
     * Sets the value of the radioactivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadioactivity(String value) {
        this.radioactivity = value;
    }

    /**
     * Gets the value of the radioactivityUnits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadioactivityUnits() {
        return radioactivityUnits;
    }

    /**
     * Sets the value of the radioactivityUnits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadioactivityUnits(String value) {
        this.radioactivityUnits = value;
    }

    /**
     * Gets the value of the radionuclide property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRadionuclide() {
        return radionuclide;
    }

    /**
     * Sets the value of the radionuclide property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRadionuclide(String value) {
        this.radionuclide = value;
    }

    /**
     * Gets the value of the substanceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubstanceNumber() {
        return substanceNumber;
    }

    /**
     * Sets the value of the substanceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubstanceNumber(String value) {
        this.substanceNumber = value;
    }

    /**
     * Gets the value of the surfaceReading property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurfaceReading() {
        return surfaceReading;
    }

    /**
     * Sets the value of the surfaceReading property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurfaceReading(String value) {
        this.surfaceReading = value;
    }

    /**
     * Gets the value of the transportIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportIndex() {
        return transportIndex;
    }

    /**
     * Sets the value of the transportIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportIndex(String value) {
        this.transportIndex = value;
    }

    /**
     * Gets the value of the materialsType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaterialsType() {
        return materialsType;
    }

    /**
     * Sets the value of the materialsType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaterialsType(String value) {
        this.materialsType = value;
    }

    /**
     * Gets the value of the units property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnits() {
        return units;
    }

    /**
     * Sets the value of the units property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnits(String value) {
        this.units = value;
    }

    /**
     * Gets the value of the externalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalCode() {
        return externalCode;
    }

    /**
     * Sets the value of the externalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalCode(String value) {
        this.externalCode = value;
    }

    /**
     * Gets the value of the isOverpack property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOverpack() {
        return isOverpack;
    }

    /**
     * Sets the value of the isOverpack property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOverpack(String value) {
        this.isOverpack = value;
    }

}

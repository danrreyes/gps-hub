
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to specify a pre-formatted selection of text.
 * 
 * <p>Java class for TextTemplateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TextTemplateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TextTemplateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TextTemplatePattern" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DataQueryTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TextTemplateIsModifiable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TextTemplateDocumentDef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextTemplateDocumentDefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextTemplateType", propOrder = {
    "textTemplateGid",
    "textTemplatePattern",
    "dataQueryTypeGid",
    "textTemplateIsModifiable",
    "textTemplateDocumentDef",
    "transactionCode"
})
public class TextTemplateType {

    @XmlElement(name = "TextTemplateGid", required = true)
    protected GLogXMLGidType textTemplateGid;
    @XmlElement(name = "TextTemplatePattern", required = true)
    protected String textTemplatePattern;
    @XmlElement(name = "DataQueryTypeGid")
    protected GLogXMLGidType dataQueryTypeGid;
    @XmlElement(name = "TextTemplateIsModifiable")
    protected String textTemplateIsModifiable;
    @XmlElement(name = "TextTemplateDocumentDef")
    protected List<TextTemplateDocumentDefType> textTemplateDocumentDef;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;

    /**
     * Gets the value of the textTemplateGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTextTemplateGid() {
        return textTemplateGid;
    }

    /**
     * Sets the value of the textTemplateGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTextTemplateGid(GLogXMLGidType value) {
        this.textTemplateGid = value;
    }

    /**
     * Gets the value of the textTemplatePattern property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextTemplatePattern() {
        return textTemplatePattern;
    }

    /**
     * Sets the value of the textTemplatePattern property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextTemplatePattern(String value) {
        this.textTemplatePattern = value;
    }

    /**
     * Gets the value of the dataQueryTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueryTypeGid() {
        return dataQueryTypeGid;
    }

    /**
     * Sets the value of the dataQueryTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueryTypeGid(GLogXMLGidType value) {
        this.dataQueryTypeGid = value;
    }

    /**
     * Gets the value of the textTemplateIsModifiable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextTemplateIsModifiable() {
        return textTemplateIsModifiable;
    }

    /**
     * Sets the value of the textTemplateIsModifiable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextTemplateIsModifiable(String value) {
        this.textTemplateIsModifiable = value;
    }

    /**
     * Gets the value of the textTemplateDocumentDef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the textTemplateDocumentDef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextTemplateDocumentDef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextTemplateDocumentDefType }
     * 
     * 
     */
    public List<TextTemplateDocumentDefType> getTextTemplateDocumentDef() {
        if (textTemplateDocumentDef == null) {
            textTemplateDocumentDef = new ArrayList<TextTemplateDocumentDefType>();
        }
        return this.textTemplateDocumentDef;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

}

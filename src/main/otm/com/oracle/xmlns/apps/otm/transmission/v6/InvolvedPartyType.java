
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An InvolvedParty specifies a contact that is in some way involved with a TransOrder, Shipment, or Release.
 *             Either the ContactRef is required to be specified, or the InvolvedPartyLocationRef is required and the contact will be
 *             selected as the primary contact for the location.
 *             The LocationOverrideInfo element is only applicable when the InvolvedParty is in the Shipment.ShipmentHeader or the Release elements.
 *             The TransactionCode in the InvolvedParty is only supported in the Driver interface in 6.0.
 *          
 * 
 * <p>Java class for InvolvedPartyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="InvolvedPartyLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ContactRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactRefType" minOccurs="0"/&gt;
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyType", propOrder = {
    "transactionCode",
    "involvedPartyQualifierGid",
    "involvedPartyLocationRef",
    "contactRef",
    "comMethodGid",
    "locationOverrideInfo"
})
public class InvolvedPartyType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "InvolvedPartyQualifierGid", required = true)
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "InvolvedPartyLocationRef")
    protected GLogXMLLocRefType involvedPartyLocationRef;
    @XmlElement(name = "ContactRef")
    protected ContactRefType contactRef;
    @XmlElement(name = "ComMethodGid")
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "LocationOverrideInfo")
    protected LocationOverrideInfoType locationOverrideInfo;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the involvedPartyQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Sets the value of the involvedPartyQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Gets the value of the involvedPartyLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getInvolvedPartyLocationRef() {
        return involvedPartyLocationRef;
    }

    /**
     * Sets the value of the involvedPartyLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setInvolvedPartyLocationRef(GLogXMLLocRefType value) {
        this.involvedPartyLocationRef = value;
    }

    /**
     * Gets the value of the contactRef property.
     * 
     * @return
     *     possible object is
     *     {@link ContactRefType }
     *     
     */
    public ContactRefType getContactRef() {
        return contactRef;
    }

    /**
     * Sets the value of the contactRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRefType }
     *     
     */
    public void setContactRef(ContactRefType value) {
        this.contactRef = value;
    }

    /**
     * Gets the value of the comMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Sets the value of the comMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Gets the value of the locationOverrideInfo property.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public LocationOverrideInfoType getLocationOverrideInfo() {
        return locationOverrideInfo;
    }

    /**
     * Sets the value of the locationOverrideInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public void setLocationOverrideInfo(LocationOverrideInfoType value) {
        this.locationOverrideInfo = value;
    }

}

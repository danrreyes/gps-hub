
package com.oracle.xmlns.apps.otm.transmission.v6;

import com.oracle.xmlns.apps.gtm.transmission.v6.ServiceResponseType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OTMTransactionOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OTMTransactionOut"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransactionType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OTMTransactionOut")
@XmlSeeAlso({
    BulkPlanType.class,
    BulkRatingType.class,
    BulkContMoveType.class,
    BulkTrailerBuildType.class,
    FleetBulkPlanType.class,
    RateOfferingType.class,
    RateGeoType.class,
    OBShipUnitType.class,
    OBLineType.class,
    PlannedShipmentType.class,
    TenderOfferType.class,
    BookingLineAmendmentType.class,
    VoyageType.class,
    DemurrageTransactionType.class,
    RemoteQueryReplyType.class,
    DataQuerySummaryType.class,
    AccrualType.class,
    BillingType.class,
    AllocationBaseType.class,
    VoucherType.class,
    FinancialSystemFeedType.class,
    ServiceResponseType.class
})
public abstract class OTMTransactionOut
    extends GLogXMLTransactionType
{


}

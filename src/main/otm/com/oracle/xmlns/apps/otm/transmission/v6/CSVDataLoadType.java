
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Refer to the Data Management Guide document for details on the CSV file format that applies to the CsvColumnList and
 *             CsvRow elements.
 *          
 * 
 * <p>Java class for CSVDataLoadType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CSVDataLoadType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CsvCommand" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CsvTableName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CsvColumnList" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CsvRow" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CSVDataLoadType", propOrder = {
    "csvCommand",
    "csvTableName",
    "csvColumnList",
    "csvRow"
})
public class CSVDataLoadType
    extends OTMTransactionInOut
{

    @XmlElement(name = "CsvCommand", required = true)
    protected String csvCommand;
    @XmlElement(name = "CsvTableName", required = true)
    protected String csvTableName;
    @XmlElement(name = "CsvColumnList", required = true)
    protected String csvColumnList;
    @XmlElement(name = "CsvRow")
    protected List<String> csvRow;

    /**
     * Gets the value of the csvCommand property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsvCommand() {
        return csvCommand;
    }

    /**
     * Sets the value of the csvCommand property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsvCommand(String value) {
        this.csvCommand = value;
    }

    /**
     * Gets the value of the csvTableName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsvTableName() {
        return csvTableName;
    }

    /**
     * Sets the value of the csvTableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsvTableName(String value) {
        this.csvTableName = value;
    }

    /**
     * Gets the value of the csvColumnList property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsvColumnList() {
        return csvColumnList;
    }

    /**
     * Sets the value of the csvColumnList property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsvColumnList(String value) {
        this.csvColumnList = value;
    }

    /**
     * Gets the value of the csvRow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the csvRow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCsvRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCsvRow() {
        if (csvRow == null) {
            csvRow = new ArrayList<String>();
        }
        return this.csvRow;
    }

}

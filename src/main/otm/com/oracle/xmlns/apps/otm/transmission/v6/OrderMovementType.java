
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             An Order Movement represents part of the routing of an order. It is also a collection of ship units that
 *             are
 *             unplanned for certain part of a route.
 * 
 *             The ShipUnit in the OrderMovement correspond to the same ShipUnit object in the Shipment (i.e. the
 *             S_SHIP_UNIT table).
 *          
 * 
 * <p>Java class for OrderMovementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderMovementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="OrderMovementGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="OrderReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsTemporary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreationProcessType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OrderPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="IsFixedSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipToLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="IsFixedDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EarlyPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="OperationEarlyPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LatePickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EarlyDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LateDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="OperationLateDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ReuseEquipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ParentSourceLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ParentDestLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OrigLegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OrigLegPosition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OrigSellOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitLengthWidthHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLWHType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/&gt;
 *         &lt;element name="UserDefIconInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserDefIconInfoType" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SourceTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DestTag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ExpectedTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ExpectedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="CalculateContractedRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CalculateServiceTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipWithGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LegClassificationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OrderMovementD" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderMovementDType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderMovementType", propOrder = {
    "intSavedQuery",
    "orderMovementGid",
    "transactionCode",
    "replaceChildren",
    "orderReleaseGid",
    "isTemporary",
    "creationProcessType",
    "orderPriority",
    "totalShipUnitCount",
    "totalWeightVolume",
    "sequenceNumber",
    "perspective",
    "shipFromLocationRef",
    "isFixedSource",
    "isTemplate",
    "shipToLocationRef",
    "isFixedDest",
    "earlyPickupDt",
    "operationEarlyPickupDt",
    "latePickupDt",
    "earlyDeliveryDt",
    "lateDeliveryDt",
    "operationLateDeliveryDt",
    "reuseEquipment",
    "parentSourceLocGid",
    "parentDestLocGid",
    "origLegGid",
    "origLegPosition",
    "bulkPlanGid",
    "origSellOMGid",
    "shipUnitLengthWidthHeight",
    "shipUnitDiameter",
    "userDefIconInfo",
    "transportHandlingUnitGid",
    "indicator",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "sourceTag",
    "destTag",
    "shipmentGid",
    "expectedTransitTime",
    "expectedCost",
    "calculateContractedRate",
    "calculateServiceTime",
    "rateOfferingGid",
    "rateGeoGid",
    "equipmentGroupGid",
    "equipmentGroupProfileGid",
    "transportModeGid",
    "modeProfileGid",
    "serviceProviderGid",
    "serviceProviderProfileGid",
    "rateServiceProfileGid",
    "shipWithGroup",
    "legClassificationGid",
    "orderMovementD",
    "refnum",
    "remark",
    "status",
    "involvedParty",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class OrderMovementType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "OrderMovementGid")
    protected GLogXMLGidType orderMovementGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "OrderReleaseGid")
    protected GLogXMLGidType orderReleaseGid;
    @XmlElement(name = "IsTemporary")
    protected String isTemporary;
    @XmlElement(name = "CreationProcessType")
    protected String creationProcessType;
    @XmlElement(name = "OrderPriority")
    protected String orderPriority;
    @XmlElement(name = "TotalShipUnitCount")
    protected String totalShipUnitCount;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "ShipFromLocationRef")
    protected GLogXMLLocRefType shipFromLocationRef;
    @XmlElement(name = "IsFixedSource")
    protected String isFixedSource;
    @XmlElement(name = "IsTemplate")
    protected String isTemplate;
    @XmlElement(name = "ShipToLocationRef")
    protected GLogXMLLocRefType shipToLocationRef;
    @XmlElement(name = "IsFixedDest")
    protected String isFixedDest;
    @XmlElement(name = "EarlyPickupDt")
    protected GLogDateTimeType earlyPickupDt;
    @XmlElement(name = "OperationEarlyPickupDt")
    protected GLogDateTimeType operationEarlyPickupDt;
    @XmlElement(name = "LatePickupDt")
    protected GLogDateTimeType latePickupDt;
    @XmlElement(name = "EarlyDeliveryDt")
    protected GLogDateTimeType earlyDeliveryDt;
    @XmlElement(name = "LateDeliveryDt")
    protected GLogDateTimeType lateDeliveryDt;
    @XmlElement(name = "OperationLateDeliveryDt")
    protected GLogDateTimeType operationLateDeliveryDt;
    @XmlElement(name = "ReuseEquipment")
    protected String reuseEquipment;
    @XmlElement(name = "ParentSourceLocGid")
    protected GLogXMLGidType parentSourceLocGid;
    @XmlElement(name = "ParentDestLocGid")
    protected GLogXMLGidType parentDestLocGid;
    @XmlElement(name = "OrigLegGid")
    protected GLogXMLGidType origLegGid;
    @XmlElement(name = "OrigLegPosition")
    protected String origLegPosition;
    @XmlElement(name = "BulkPlanGid")
    protected GLogXMLGidType bulkPlanGid;
    @XmlElement(name = "OrigSellOMGid")
    protected GLogXMLGidType origSellOMGid;
    @XmlElement(name = "ShipUnitLengthWidthHeight")
    protected GLogXMLLWHType shipUnitLengthWidthHeight;
    @XmlElement(name = "ShipUnitDiameter")
    protected GLogXMLDiameterType shipUnitDiameter;
    @XmlElement(name = "UserDefIconInfo")
    protected UserDefIconInfoType userDefIconInfo;
    @XmlElement(name = "TransportHandlingUnitGid")
    protected GLogXMLGidType transportHandlingUnitGid;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "SourceTag")
    protected String sourceTag;
    @XmlElement(name = "DestTag")
    protected String destTag;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ExpectedTransitTime")
    protected GLogXMLDurationType expectedTransitTime;
    @XmlElement(name = "ExpectedCost")
    protected GLogXMLFinancialAmountType expectedCost;
    @XmlElement(name = "CalculateContractedRate")
    protected String calculateContractedRate;
    @XmlElement(name = "CalculateServiceTime")
    protected String calculateServiceTime;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "ShipWithGroup")
    protected String shipWithGroup;
    @XmlElement(name = "LegClassificationGid")
    protected GLogXMLGidType legClassificationGid;
    @XmlElement(name = "OrderMovementD")
    protected List<OrderMovementDType> orderMovementD;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the orderMovementGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderMovementGid() {
        return orderMovementGid;
    }

    /**
     * Sets the value of the orderMovementGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderMovementGid(GLogXMLGidType value) {
        this.orderMovementGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the orderReleaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrderReleaseGid() {
        return orderReleaseGid;
    }

    /**
     * Sets the value of the orderReleaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrderReleaseGid(GLogXMLGidType value) {
        this.orderReleaseGid = value;
    }

    /**
     * Gets the value of the isTemporary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemporary() {
        return isTemporary;
    }

    /**
     * Sets the value of the isTemporary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemporary(String value) {
        this.isTemporary = value;
    }

    /**
     * Gets the value of the creationProcessType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreationProcessType() {
        return creationProcessType;
    }

    /**
     * Sets the value of the creationProcessType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreationProcessType(String value) {
        this.creationProcessType = value;
    }

    /**
     * Gets the value of the orderPriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPriority() {
        return orderPriority;
    }

    /**
     * Sets the value of the orderPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPriority(String value) {
        this.orderPriority = value;
    }

    /**
     * Gets the value of the totalShipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalShipUnitCount() {
        return totalShipUnitCount;
    }

    /**
     * Sets the value of the totalShipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalShipUnitCount(String value) {
        this.totalShipUnitCount = value;
    }

    /**
     * Gets the value of the totalWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Sets the value of the totalWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the shipFromLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipFromLocationRef() {
        return shipFromLocationRef;
    }

    /**
     * Sets the value of the shipFromLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipFromLocationRef(GLogXMLLocRefType value) {
        this.shipFromLocationRef = value;
    }

    /**
     * Gets the value of the isFixedSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedSource() {
        return isFixedSource;
    }

    /**
     * Sets the value of the isFixedSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedSource(String value) {
        this.isFixedSource = value;
    }

    /**
     * Gets the value of the isTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemplate() {
        return isTemplate;
    }

    /**
     * Sets the value of the isTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemplate(String value) {
        this.isTemplate = value;
    }

    /**
     * Gets the value of the shipToLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipToLocationRef() {
        return shipToLocationRef;
    }

    /**
     * Sets the value of the shipToLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipToLocationRef(GLogXMLLocRefType value) {
        this.shipToLocationRef = value;
    }

    /**
     * Gets the value of the isFixedDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedDest() {
        return isFixedDest;
    }

    /**
     * Sets the value of the isFixedDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedDest(String value) {
        this.isFixedDest = value;
    }

    /**
     * Gets the value of the earlyPickupDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyPickupDt() {
        return earlyPickupDt;
    }

    /**
     * Sets the value of the earlyPickupDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyPickupDt(GLogDateTimeType value) {
        this.earlyPickupDt = value;
    }

    /**
     * Gets the value of the operationEarlyPickupDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getOperationEarlyPickupDt() {
        return operationEarlyPickupDt;
    }

    /**
     * Sets the value of the operationEarlyPickupDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setOperationEarlyPickupDt(GLogDateTimeType value) {
        this.operationEarlyPickupDt = value;
    }

    /**
     * Gets the value of the latePickupDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLatePickupDt() {
        return latePickupDt;
    }

    /**
     * Sets the value of the latePickupDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLatePickupDt(GLogDateTimeType value) {
        this.latePickupDt = value;
    }

    /**
     * Gets the value of the earlyDeliveryDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyDeliveryDt() {
        return earlyDeliveryDt;
    }

    /**
     * Sets the value of the earlyDeliveryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyDeliveryDt(GLogDateTimeType value) {
        this.earlyDeliveryDt = value;
    }

    /**
     * Gets the value of the lateDeliveryDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLateDeliveryDt() {
        return lateDeliveryDt;
    }

    /**
     * Sets the value of the lateDeliveryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLateDeliveryDt(GLogDateTimeType value) {
        this.lateDeliveryDt = value;
    }

    /**
     * Gets the value of the operationLateDeliveryDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getOperationLateDeliveryDt() {
        return operationLateDeliveryDt;
    }

    /**
     * Sets the value of the operationLateDeliveryDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setOperationLateDeliveryDt(GLogDateTimeType value) {
        this.operationLateDeliveryDt = value;
    }

    /**
     * Gets the value of the reuseEquipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReuseEquipment() {
        return reuseEquipment;
    }

    /**
     * Sets the value of the reuseEquipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReuseEquipment(String value) {
        this.reuseEquipment = value;
    }

    /**
     * Gets the value of the parentSourceLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParentSourceLocGid() {
        return parentSourceLocGid;
    }

    /**
     * Sets the value of the parentSourceLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParentSourceLocGid(GLogXMLGidType value) {
        this.parentSourceLocGid = value;
    }

    /**
     * Gets the value of the parentDestLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParentDestLocGid() {
        return parentDestLocGid;
    }

    /**
     * Sets the value of the parentDestLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParentDestLocGid(GLogXMLGidType value) {
        this.parentDestLocGid = value;
    }

    /**
     * Gets the value of the origLegGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigLegGid() {
        return origLegGid;
    }

    /**
     * Sets the value of the origLegGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigLegGid(GLogXMLGidType value) {
        this.origLegGid = value;
    }

    /**
     * Gets the value of the origLegPosition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigLegPosition() {
        return origLegPosition;
    }

    /**
     * Sets the value of the origLegPosition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigLegPosition(String value) {
        this.origLegPosition = value;
    }

    /**
     * Gets the value of the bulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkPlanGid() {
        return bulkPlanGid;
    }

    /**
     * Sets the value of the bulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkPlanGid(GLogXMLGidType value) {
        this.bulkPlanGid = value;
    }

    /**
     * Gets the value of the origSellOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigSellOMGid() {
        return origSellOMGid;
    }

    /**
     * Sets the value of the origSellOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigSellOMGid(GLogXMLGidType value) {
        this.origSellOMGid = value;
    }

    /**
     * Gets the value of the shipUnitLengthWidthHeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLWHType }
     *     
     */
    public GLogXMLLWHType getShipUnitLengthWidthHeight() {
        return shipUnitLengthWidthHeight;
    }

    /**
     * Sets the value of the shipUnitLengthWidthHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLWHType }
     *     
     */
    public void setShipUnitLengthWidthHeight(GLogXMLLWHType value) {
        this.shipUnitLengthWidthHeight = value;
    }

    /**
     * Gets the value of the shipUnitDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getShipUnitDiameter() {
        return shipUnitDiameter;
    }

    /**
     * Sets the value of the shipUnitDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setShipUnitDiameter(GLogXMLDiameterType value) {
        this.shipUnitDiameter = value;
    }

    /**
     * Gets the value of the userDefIconInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public UserDefIconInfoType getUserDefIconInfo() {
        return userDefIconInfo;
    }

    /**
     * Sets the value of the userDefIconInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public void setUserDefIconInfo(UserDefIconInfoType value) {
        this.userDefIconInfo = value;
    }

    /**
     * Gets the value of the transportHandlingUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportHandlingUnitGid() {
        return transportHandlingUnitGid;
    }

    /**
     * Sets the value of the transportHandlingUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportHandlingUnitGid(GLogXMLGidType value) {
        this.transportHandlingUnitGid = value;
    }

    /**
     * Gets the value of the indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Sets the value of the indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Gets the value of the pickupRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Sets the value of the pickupRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Gets the value of the dropoffRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Sets the value of the dropoffRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Gets the value of the sourceTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceTag() {
        return sourceTag;
    }

    /**
     * Sets the value of the sourceTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceTag(String value) {
        this.sourceTag = value;
    }

    /**
     * Gets the value of the destTag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestTag() {
        return destTag;
    }

    /**
     * Sets the value of the destTag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestTag(String value) {
        this.destTag = value;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the expectedTransitTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getExpectedTransitTime() {
        return expectedTransitTime;
    }

    /**
     * Sets the value of the expectedTransitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setExpectedTransitTime(GLogXMLDurationType value) {
        this.expectedTransitTime = value;
    }

    /**
     * Gets the value of the expectedCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getExpectedCost() {
        return expectedCost;
    }

    /**
     * Sets the value of the expectedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setExpectedCost(GLogXMLFinancialAmountType value) {
        this.expectedCost = value;
    }

    /**
     * Gets the value of the calculateContractedRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateContractedRate() {
        return calculateContractedRate;
    }

    /**
     * Sets the value of the calculateContractedRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateContractedRate(String value) {
        this.calculateContractedRate = value;
    }

    /**
     * Gets the value of the calculateServiceTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateServiceTime() {
        return calculateServiceTime;
    }

    /**
     * Sets the value of the calculateServiceTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateServiceTime(String value) {
        this.calculateServiceTime = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the rateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Sets the value of the rateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Sets the value of the equipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Sets the value of the transportModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the serviceProviderProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Sets the value of the serviceProviderProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Gets the value of the rateServiceProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Sets the value of the rateServiceProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Gets the value of the shipWithGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipWithGroup() {
        return shipWithGroup;
    }

    /**
     * Sets the value of the shipWithGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipWithGroup(String value) {
        this.shipWithGroup = value;
    }

    /**
     * Gets the value of the legClassificationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegClassificationGid() {
        return legClassificationGid;
    }

    /**
     * Sets the value of the legClassificationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegClassificationGid(GLogXMLGidType value) {
        this.legClassificationGid = value;
    }

    /**
     * Gets the value of the orderMovementD property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orderMovementD property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderMovementD().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderMovementDType }
     * 
     * 
     */
    public List<OrderMovementDType> getOrderMovementD() {
        if (orderMovementD == null) {
            orderMovementD = new ArrayList<OrderMovementDType>();
        }
        return this.orderMovementD;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}

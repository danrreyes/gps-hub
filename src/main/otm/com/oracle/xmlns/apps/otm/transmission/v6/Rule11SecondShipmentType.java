
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains the information from the second Shipment in a Rail Rule 11 move.
 * 
 * <p>Java class for Rule11SecondShipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Rule11SecondShipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" minOccurs="0"/&gt;
 *         &lt;element name="Rule11SecShipRateOffering" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Rule11SecShipRateOfferingType" minOccurs="0"/&gt;
 *         &lt;element name="Rule11SecShipRateGeo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Rule11SecShipRateGeoType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rule11SecondShipmentType", propOrder = {
    "shipmentRefnum",
    "rule11SecShipRateOffering",
    "rule11SecShipRateGeo"
})
public class Rule11SecondShipmentType {

    @XmlElement(name = "ShipmentRefnum")
    protected ShipmentRefnumType shipmentRefnum;
    @XmlElement(name = "Rule11SecShipRateOffering")
    protected Rule11SecShipRateOfferingType rule11SecShipRateOffering;
    @XmlElement(name = "Rule11SecShipRateGeo")
    protected Rule11SecShipRateGeoType rule11SecShipRateGeo;

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentRefnumType }
     *     
     */
    public ShipmentRefnumType getShipmentRefnum() {
        return shipmentRefnum;
    }

    /**
     * Sets the value of the shipmentRefnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentRefnumType }
     *     
     */
    public void setShipmentRefnum(ShipmentRefnumType value) {
        this.shipmentRefnum = value;
    }

    /**
     * Gets the value of the rule11SecShipRateOffering property.
     * 
     * @return
     *     possible object is
     *     {@link Rule11SecShipRateOfferingType }
     *     
     */
    public Rule11SecShipRateOfferingType getRule11SecShipRateOffering() {
        return rule11SecShipRateOffering;
    }

    /**
     * Sets the value of the rule11SecShipRateOffering property.
     * 
     * @param value
     *     allowed object is
     *     {@link Rule11SecShipRateOfferingType }
     *     
     */
    public void setRule11SecShipRateOffering(Rule11SecShipRateOfferingType value) {
        this.rule11SecShipRateOffering = value;
    }

    /**
     * Gets the value of the rule11SecShipRateGeo property.
     * 
     * @return
     *     possible object is
     *     {@link Rule11SecShipRateGeoType }
     *     
     */
    public Rule11SecShipRateGeoType getRule11SecShipRateGeo() {
        return rule11SecShipRateGeo;
    }

    /**
     * Sets the value of the rule11SecShipRateGeo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Rule11SecShipRateGeoType }
     *     
     */
    public void setRule11SecShipRateGeo(Rule11SecShipRateGeoType value) {
        this.rule11SecShipRateGeo = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             RailEquipment is an element of RailDetail used to specify rail specific equipment information on an invoice.
 *          
 * 
 * <p>Java class for RailEquipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RailEquipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSealType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentDescriptionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType" minOccurs="0"/&gt;
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="Length" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthType" minOccurs="0"/&gt;
 *         &lt;element name="Dunnage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="OwnershipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailEquipmentType", propOrder = {
    "sequenceNumber",
    "equipmentPrefix",
    "equipmentNumber",
    "equipmentInitialNumber",
    "sEquipmentGid",
    "equipmentSeal",
    "equipmentDescriptionCode",
    "weight",
    "tareWeight",
    "length",
    "dunnage",
    "ownershipCode"
})
public class RailEquipmentType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "EquipmentPrefix")
    protected String equipmentPrefix;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "EquipmentSeal")
    protected List<EquipmentSealType> equipmentSeal;
    @XmlElement(name = "EquipmentDescriptionCode")
    protected String equipmentDescriptionCode;
    @XmlElement(name = "Weight")
    protected WeightType weight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "Length")
    protected LengthType length;
    @XmlElement(name = "Dunnage")
    protected GLogXMLWeightType dunnage;
    @XmlElement(name = "OwnershipCode")
    protected String ownershipCode;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the equipmentPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentPrefix() {
        return equipmentPrefix;
    }

    /**
     * Sets the value of the equipmentPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentPrefix(String value) {
        this.equipmentPrefix = value;
    }

    /**
     * Gets the value of the equipmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Sets the value of the equipmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the equipmentInitialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Sets the value of the equipmentInitialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Gets the value of the sEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Sets the value of the sEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the equipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSealType }
     * 
     * 
     */
    public List<EquipmentSealType> getEquipmentSeal() {
        if (equipmentSeal == null) {
            equipmentSeal = new ArrayList<EquipmentSealType>();
        }
        return this.equipmentSeal;
    }

    /**
     * Gets the value of the equipmentDescriptionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentDescriptionCode() {
        return equipmentDescriptionCode;
    }

    /**
     * Sets the value of the equipmentDescriptionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentDescriptionCode(String value) {
        this.equipmentDescriptionCode = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setWeight(WeightType value) {
        this.weight = value;
    }

    /**
     * Gets the value of the tareWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Sets the value of the tareWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link LengthType }
     *     
     */
    public LengthType getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthType }
     *     
     */
    public void setLength(LengthType value) {
        this.length = value;
    }

    /**
     * Gets the value of the dunnage property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getDunnage() {
        return dunnage;
    }

    /**
     * Sets the value of the dunnage property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setDunnage(GLogXMLWeightType value) {
        this.dunnage = value;
    }

    /**
     * Gets the value of the ownershipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnershipCode() {
        return ownershipCode;
    }

    /**
     * Sets the value of the ownershipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnershipCode(String value) {
        this.ownershipCode = value;
    }

}

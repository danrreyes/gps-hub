
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             A Location is a place where transportation related activities occur, such as loading and unloading freight.
 *             In addition, a location may represent a corporation, and/or a service provider.
 * 
 *             Note: The ChildOpreationalLocations element is supported on the outbound only.
 *          
 * 
 * <p>Java class for LocationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LocationName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Address" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AddressType"/&gt;
 *         &lt;element name="PostalSPLCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RailSPLCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RailStationCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RailJunctionCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ERPCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServingServprovProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RegionRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RegionRefType" minOccurs="0"/&gt;
 *         &lt;element name="LocationGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LocationRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LocationSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsTemporary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="Contact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="LocationRole" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRoleType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsMakeAppointmentBeforePlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Corporation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CorporationType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProvider" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderType" minOccurs="0"/&gt;
 *         &lt;element name="RateClassificationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsShipperKnown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SubstituteLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsAddressValid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumRowsInYard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumSlotsPerRowInYard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsLTLSplitable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApptObjectType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApptActivityType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApptSearchDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApptShowNumOfOptions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StandingApptCutoffWindow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SlotTimeInterval" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ExcludeFromRouteExec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UseApptPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SchedLowPriorityAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EnforceTimeWindowAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SchedInfeasibleAppt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AppointDisplayStartTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AppointDisplayDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OperationalLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OperationalLocationType" minOccurs="0"/&gt;
 *         &lt;element name="ChildOperationalLocations" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LoadUnloadPoint" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LoadUnloadPointName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *                   &lt;element name="IsLoad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="LoadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IsUnload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UnloadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LocationCapacityGroupInfo" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LocationCapacityGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                   &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *                   &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="LocationResourceType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationResourceTypeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LocationServProvPref" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationServProvPrefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AssetParentLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AssetParentLocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AllowDriverRest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsFixedAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryAddressLineSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsWMSFacility" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EruGrouping" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationType", propOrder = {
    "sendReason",
    "transactionCode",
    "replaceChildren",
    "intSavedQuery",
    "locationGid",
    "locationName",
    "description",
    "isTemplate",
    "address",
    "postalSPLCGid",
    "railSPLCGid",
    "railStationCodeGid",
    "railJunctionCodeGid",
    "erpcGid",
    "servingServprovProfileGid",
    "regionRef",
    "locationGroupGid",
    "locationRefnum",
    "remark",
    "locationSpecialService",
    "isTemporary",
    "contactGid",
    "contact",
    "locationRole",
    "accessorialCodeGid",
    "isMakeAppointmentBeforePlan",
    "corporation",
    "serviceProvider",
    "rateClassificationGid",
    "isShipperKnown",
    "substituteLocation",
    "serviceProviderProfileGid",
    "equipmentGroupProfileGid",
    "isAddressValid",
    "numRowsInYard",
    "numSlotsPerRowInYard",
    "isLTLSplitable",
    "apptObjectType",
    "apptActivityType",
    "apptSearchDays",
    "apptShowNumOfOptions",
    "standingApptCutoffWindow",
    "slotTimeInterval",
    "excludeFromRouteExec",
    "useApptPriority",
    "schedLowPriorityAppt",
    "enforceTimeWindowAppt",
    "schedInfeasibleAppt",
    "appointDisplayStartTime",
    "appointDisplayDuration",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "operationalLocation",
    "childOperationalLocations",
    "loadUnloadPoint",
    "locationCapacityGroupInfo",
    "locationResourceType",
    "status",
    "locationServProvPref",
    "assetParentLocation",
    "allowDriverRest",
    "isFixedAddress",
    "primaryAddressLineSeq",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "isActive",
    "isWMSFacility",
    "eruGrouping"
})
public class LocationType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "LocationName")
    protected String locationName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "IsTemplate")
    protected String isTemplate;
    @XmlElement(name = "Address", required = true)
    protected AddressType address;
    @XmlElement(name = "PostalSPLCGid")
    protected GLogXMLGidType postalSPLCGid;
    @XmlElement(name = "RailSPLCGid")
    protected GLogXMLGidType railSPLCGid;
    @XmlElement(name = "RailStationCodeGid")
    protected GLogXMLGidType railStationCodeGid;
    @XmlElement(name = "RailJunctionCodeGid")
    protected GLogXMLGidType railJunctionCodeGid;
    @XmlElement(name = "ERPCGid")
    protected GLogXMLGidType erpcGid;
    @XmlElement(name = "ServingServprovProfileGid")
    protected GLogXMLGidType servingServprovProfileGid;
    @XmlElement(name = "RegionRef")
    protected RegionRefType regionRef;
    @XmlElement(name = "LocationGroupGid")
    protected GLogXMLGidType locationGroupGid;
    @XmlElement(name = "LocationRefnum")
    protected List<LocationRefnumType> locationRefnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "LocationSpecialService")
    protected List<SpecialServiceType> locationSpecialService;
    @XmlElement(name = "IsTemporary")
    protected String isTemporary;
    @XmlElement(name = "ContactGid")
    protected List<GLogXMLGidType> contactGid;
    @XmlElement(name = "Contact")
    protected List<ContactType> contact;
    @XmlElement(name = "LocationRole")
    protected List<LocationRoleType> locationRole;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "IsMakeAppointmentBeforePlan")
    protected String isMakeAppointmentBeforePlan;
    @XmlElement(name = "Corporation")
    protected CorporationType corporation;
    @XmlElement(name = "ServiceProvider")
    protected ServiceProviderType serviceProvider;
    @XmlElement(name = "RateClassificationGid")
    protected GLogXMLGidType rateClassificationGid;
    @XmlElement(name = "IsShipperKnown")
    protected String isShipperKnown;
    @XmlElement(name = "SubstituteLocation")
    protected GLogXMLLocRefType substituteLocation;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "IsAddressValid")
    protected String isAddressValid;
    @XmlElement(name = "NumRowsInYard")
    protected String numRowsInYard;
    @XmlElement(name = "NumSlotsPerRowInYard")
    protected String numSlotsPerRowInYard;
    @XmlElement(name = "IsLTLSplitable")
    protected String isLTLSplitable;
    @XmlElement(name = "ApptObjectType")
    protected String apptObjectType;
    @XmlElement(name = "ApptActivityType")
    protected String apptActivityType;
    @XmlElement(name = "ApptSearchDays")
    protected String apptSearchDays;
    @XmlElement(name = "ApptShowNumOfOptions")
    protected String apptShowNumOfOptions;
    @XmlElement(name = "StandingApptCutoffWindow")
    protected String standingApptCutoffWindow;
    @XmlElement(name = "SlotTimeInterval")
    protected GLogXMLDurationType slotTimeInterval;
    @XmlElement(name = "ExcludeFromRouteExec")
    protected String excludeFromRouteExec;
    @XmlElement(name = "UseApptPriority")
    protected String useApptPriority;
    @XmlElement(name = "SchedLowPriorityAppt")
    protected String schedLowPriorityAppt;
    @XmlElement(name = "EnforceTimeWindowAppt")
    protected String enforceTimeWindowAppt;
    @XmlElement(name = "SchedInfeasibleAppt")
    protected String schedInfeasibleAppt;
    @XmlElement(name = "AppointDisplayStartTime")
    protected String appointDisplayStartTime;
    @XmlElement(name = "AppointDisplayDuration")
    protected GLogXMLDurationType appointDisplayDuration;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "OperationalLocation")
    protected OperationalLocationType operationalLocation;
    @XmlElement(name = "ChildOperationalLocations")
    protected LocationType.ChildOperationalLocations childOperationalLocations;
    @XmlElement(name = "LoadUnloadPoint")
    protected List<LocationType.LoadUnloadPoint> loadUnloadPoint;
    @XmlElement(name = "LocationCapacityGroupInfo")
    protected List<LocationType.LocationCapacityGroupInfo> locationCapacityGroupInfo;
    @XmlElement(name = "LocationResourceType")
    protected List<LocationResourceTypeType> locationResourceType;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "LocationServProvPref")
    protected List<LocationServProvPrefType> locationServProvPref;
    @XmlElement(name = "AssetParentLocation")
    protected List<AssetParentLocationType> assetParentLocation;
    @XmlElement(name = "AllowDriverRest")
    protected String allowDriverRest;
    @XmlElement(name = "IsFixedAddress")
    protected String isFixedAddress;
    @XmlElement(name = "PrimaryAddressLineSeq")
    protected String primaryAddressLineSeq;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "IsActive")
    protected String isActive;
    @XmlElement(name = "IsWMSFacility")
    protected String isWMSFacility;
    @XmlElement(name = "EruGrouping")
    protected String eruGrouping;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the locationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Sets the value of the locationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Gets the value of the locationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Sets the value of the locationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationName(String value) {
        this.locationName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the isTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemplate() {
        return isTemplate;
    }

    /**
     * Sets the value of the isTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemplate(String value) {
        this.isTemplate = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link AddressType }
     *     
     */
    public AddressType getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressType }
     *     
     */
    public void setAddress(AddressType value) {
        this.address = value;
    }

    /**
     * Gets the value of the postalSPLCGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPostalSPLCGid() {
        return postalSPLCGid;
    }

    /**
     * Sets the value of the postalSPLCGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPostalSPLCGid(GLogXMLGidType value) {
        this.postalSPLCGid = value;
    }

    /**
     * Gets the value of the railSPLCGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailSPLCGid() {
        return railSPLCGid;
    }

    /**
     * Sets the value of the railSPLCGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailSPLCGid(GLogXMLGidType value) {
        this.railSPLCGid = value;
    }

    /**
     * Gets the value of the railStationCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailStationCodeGid() {
        return railStationCodeGid;
    }

    /**
     * Sets the value of the railStationCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailStationCodeGid(GLogXMLGidType value) {
        this.railStationCodeGid = value;
    }

    /**
     * Gets the value of the railJunctionCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailJunctionCodeGid() {
        return railJunctionCodeGid;
    }

    /**
     * Sets the value of the railJunctionCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailJunctionCodeGid(GLogXMLGidType value) {
        this.railJunctionCodeGid = value;
    }

    /**
     * Gets the value of the erpcGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getERPCGid() {
        return erpcGid;
    }

    /**
     * Sets the value of the erpcGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setERPCGid(GLogXMLGidType value) {
        this.erpcGid = value;
    }

    /**
     * Gets the value of the servingServprovProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServingServprovProfileGid() {
        return servingServprovProfileGid;
    }

    /**
     * Sets the value of the servingServprovProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServingServprovProfileGid(GLogXMLGidType value) {
        this.servingServprovProfileGid = value;
    }

    /**
     * Gets the value of the regionRef property.
     * 
     * @return
     *     possible object is
     *     {@link RegionRefType }
     *     
     */
    public RegionRefType getRegionRef() {
        return regionRef;
    }

    /**
     * Sets the value of the regionRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionRefType }
     *     
     */
    public void setRegionRef(RegionRefType value) {
        this.regionRef = value;
    }

    /**
     * Gets the value of the locationGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGroupGid() {
        return locationGroupGid;
    }

    /**
     * Sets the value of the locationGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGroupGid(GLogXMLGidType value) {
        this.locationGroupGid = value;
    }

    /**
     * Gets the value of the locationRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationRefnumType }
     * 
     * 
     */
    public List<LocationRefnumType> getLocationRefnum() {
        if (locationRefnum == null) {
            locationRefnum = new ArrayList<LocationRefnumType>();
        }
        return this.locationRefnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the locationSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getLocationSpecialService() {
        if (locationSpecialService == null) {
            locationSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.locationSpecialService;
    }

    /**
     * Gets the value of the isTemporary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemporary() {
        return isTemporary;
    }

    /**
     * Sets the value of the isTemporary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemporary(String value) {
        this.isTemporary = value;
    }

    /**
     * Gets the value of the contactGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the contactGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContactGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getContactGid() {
        if (contactGid == null) {
            contactGid = new ArrayList<GLogXMLGidType>();
        }
        return this.contactGid;
    }

    /**
     * Gets the value of the contact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the contact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContactType }
     * 
     * 
     */
    public List<ContactType> getContact() {
        if (contact == null) {
            contact = new ArrayList<ContactType>();
        }
        return this.contact;
    }

    /**
     * Gets the value of the locationRole property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationRole property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationRoleType }
     * 
     * 
     */
    public List<LocationRoleType> getLocationRole() {
        if (locationRole == null) {
            locationRole = new ArrayList<LocationRoleType>();
        }
        return this.locationRole;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the isMakeAppointmentBeforePlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMakeAppointmentBeforePlan() {
        return isMakeAppointmentBeforePlan;
    }

    /**
     * Sets the value of the isMakeAppointmentBeforePlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMakeAppointmentBeforePlan(String value) {
        this.isMakeAppointmentBeforePlan = value;
    }

    /**
     * Gets the value of the corporation property.
     * 
     * @return
     *     possible object is
     *     {@link CorporationType }
     *     
     */
    public CorporationType getCorporation() {
        return corporation;
    }

    /**
     * Sets the value of the corporation property.
     * 
     * @param value
     *     allowed object is
     *     {@link CorporationType }
     *     
     */
    public void setCorporation(CorporationType value) {
        this.corporation = value;
    }

    /**
     * Gets the value of the serviceProvider property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderType }
     *     
     */
    public ServiceProviderType getServiceProvider() {
        return serviceProvider;
    }

    /**
     * Sets the value of the serviceProvider property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderType }
     *     
     */
    public void setServiceProvider(ServiceProviderType value) {
        this.serviceProvider = value;
    }

    /**
     * Gets the value of the rateClassificationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateClassificationGid() {
        return rateClassificationGid;
    }

    /**
     * Sets the value of the rateClassificationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateClassificationGid(GLogXMLGidType value) {
        this.rateClassificationGid = value;
    }

    /**
     * Gets the value of the isShipperKnown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShipperKnown() {
        return isShipperKnown;
    }

    /**
     * Sets the value of the isShipperKnown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShipperKnown(String value) {
        this.isShipperKnown = value;
    }

    /**
     * Gets the value of the substituteLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSubstituteLocation() {
        return substituteLocation;
    }

    /**
     * Sets the value of the substituteLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSubstituteLocation(GLogXMLLocRefType value) {
        this.substituteLocation = value;
    }

    /**
     * Gets the value of the serviceProviderProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Sets the value of the serviceProviderProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the isAddressValid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAddressValid() {
        return isAddressValid;
    }

    /**
     * Sets the value of the isAddressValid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAddressValid(String value) {
        this.isAddressValid = value;
    }

    /**
     * Gets the value of the numRowsInYard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRowsInYard() {
        return numRowsInYard;
    }

    /**
     * Sets the value of the numRowsInYard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRowsInYard(String value) {
        this.numRowsInYard = value;
    }

    /**
     * Gets the value of the numSlotsPerRowInYard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSlotsPerRowInYard() {
        return numSlotsPerRowInYard;
    }

    /**
     * Sets the value of the numSlotsPerRowInYard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSlotsPerRowInYard(String value) {
        this.numSlotsPerRowInYard = value;
    }

    /**
     * Gets the value of the isLTLSplitable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLTLSplitable() {
        return isLTLSplitable;
    }

    /**
     * Sets the value of the isLTLSplitable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLTLSplitable(String value) {
        this.isLTLSplitable = value;
    }

    /**
     * Gets the value of the apptObjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptObjectType() {
        return apptObjectType;
    }

    /**
     * Sets the value of the apptObjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptObjectType(String value) {
        this.apptObjectType = value;
    }

    /**
     * Gets the value of the apptActivityType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptActivityType() {
        return apptActivityType;
    }

    /**
     * Sets the value of the apptActivityType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptActivityType(String value) {
        this.apptActivityType = value;
    }

    /**
     * Gets the value of the apptSearchDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptSearchDays() {
        return apptSearchDays;
    }

    /**
     * Sets the value of the apptSearchDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptSearchDays(String value) {
        this.apptSearchDays = value;
    }

    /**
     * Gets the value of the apptShowNumOfOptions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApptShowNumOfOptions() {
        return apptShowNumOfOptions;
    }

    /**
     * Sets the value of the apptShowNumOfOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApptShowNumOfOptions(String value) {
        this.apptShowNumOfOptions = value;
    }

    /**
     * Gets the value of the standingApptCutoffWindow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStandingApptCutoffWindow() {
        return standingApptCutoffWindow;
    }

    /**
     * Sets the value of the standingApptCutoffWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStandingApptCutoffWindow(String value) {
        this.standingApptCutoffWindow = value;
    }

    /**
     * Gets the value of the slotTimeInterval property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getSlotTimeInterval() {
        return slotTimeInterval;
    }

    /**
     * Sets the value of the slotTimeInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setSlotTimeInterval(GLogXMLDurationType value) {
        this.slotTimeInterval = value;
    }

    /**
     * Gets the value of the excludeFromRouteExec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExcludeFromRouteExec() {
        return excludeFromRouteExec;
    }

    /**
     * Sets the value of the excludeFromRouteExec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcludeFromRouteExec(String value) {
        this.excludeFromRouteExec = value;
    }

    /**
     * Gets the value of the useApptPriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseApptPriority() {
        return useApptPriority;
    }

    /**
     * Sets the value of the useApptPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseApptPriority(String value) {
        this.useApptPriority = value;
    }

    /**
     * Gets the value of the schedLowPriorityAppt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchedLowPriorityAppt() {
        return schedLowPriorityAppt;
    }

    /**
     * Sets the value of the schedLowPriorityAppt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchedLowPriorityAppt(String value) {
        this.schedLowPriorityAppt = value;
    }

    /**
     * Gets the value of the enforceTimeWindowAppt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnforceTimeWindowAppt() {
        return enforceTimeWindowAppt;
    }

    /**
     * Sets the value of the enforceTimeWindowAppt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnforceTimeWindowAppt(String value) {
        this.enforceTimeWindowAppt = value;
    }

    /**
     * Gets the value of the schedInfeasibleAppt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchedInfeasibleAppt() {
        return schedInfeasibleAppt;
    }

    /**
     * Sets the value of the schedInfeasibleAppt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchedInfeasibleAppt(String value) {
        this.schedInfeasibleAppt = value;
    }

    /**
     * Gets the value of the appointDisplayStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppointDisplayStartTime() {
        return appointDisplayStartTime;
    }

    /**
     * Sets the value of the appointDisplayStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppointDisplayStartTime(String value) {
        this.appointDisplayStartTime = value;
    }

    /**
     * Gets the value of the appointDisplayDuration property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getAppointDisplayDuration() {
        return appointDisplayDuration;
    }

    /**
     * Sets the value of the appointDisplayDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setAppointDisplayDuration(GLogXMLDurationType value) {
        this.appointDisplayDuration = value;
    }

    /**
     * Gets the value of the pickupRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Sets the value of the pickupRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Gets the value of the dropoffRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Sets the value of the dropoffRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Gets the value of the operationalLocation property.
     * 
     * @return
     *     possible object is
     *     {@link OperationalLocationType }
     *     
     */
    public OperationalLocationType getOperationalLocation() {
        return operationalLocation;
    }

    /**
     * Sets the value of the operationalLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperationalLocationType }
     *     
     */
    public void setOperationalLocation(OperationalLocationType value) {
        this.operationalLocation = value;
    }

    /**
     * Gets the value of the childOperationalLocations property.
     * 
     * @return
     *     possible object is
     *     {@link LocationType.ChildOperationalLocations }
     *     
     */
    public LocationType.ChildOperationalLocations getChildOperationalLocations() {
        return childOperationalLocations;
    }

    /**
     * Sets the value of the childOperationalLocations property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationType.ChildOperationalLocations }
     *     
     */
    public void setChildOperationalLocations(LocationType.ChildOperationalLocations value) {
        this.childOperationalLocations = value;
    }

    /**
     * Gets the value of the loadUnloadPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the loadUnloadPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoadUnloadPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType.LoadUnloadPoint }
     * 
     * 
     */
    public List<LocationType.LoadUnloadPoint> getLoadUnloadPoint() {
        if (loadUnloadPoint == null) {
            loadUnloadPoint = new ArrayList<LocationType.LoadUnloadPoint>();
        }
        return this.loadUnloadPoint;
    }

    /**
     * Gets the value of the locationCapacityGroupInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationCapacityGroupInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationCapacityGroupInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType.LocationCapacityGroupInfo }
     * 
     * 
     */
    public List<LocationType.LocationCapacityGroupInfo> getLocationCapacityGroupInfo() {
        if (locationCapacityGroupInfo == null) {
            locationCapacityGroupInfo = new ArrayList<LocationType.LocationCapacityGroupInfo>();
        }
        return this.locationCapacityGroupInfo;
    }

    /**
     * Gets the value of the locationResourceType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationResourceType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationResourceType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationResourceTypeType }
     * 
     * 
     */
    public List<LocationResourceTypeType> getLocationResourceType() {
        if (locationResourceType == null) {
            locationResourceType = new ArrayList<LocationResourceTypeType>();
        }
        return this.locationResourceType;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the locationServProvPref property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationServProvPref property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationServProvPref().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationServProvPrefType }
     * 
     * 
     */
    public List<LocationServProvPrefType> getLocationServProvPref() {
        if (locationServProvPref == null) {
            locationServProvPref = new ArrayList<LocationServProvPrefType>();
        }
        return this.locationServProvPref;
    }

    /**
     * Gets the value of the assetParentLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the assetParentLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssetParentLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AssetParentLocationType }
     * 
     * 
     */
    public List<AssetParentLocationType> getAssetParentLocation() {
        if (assetParentLocation == null) {
            assetParentLocation = new ArrayList<AssetParentLocationType>();
        }
        return this.assetParentLocation;
    }

    /**
     * Gets the value of the allowDriverRest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowDriverRest() {
        return allowDriverRest;
    }

    /**
     * Sets the value of the allowDriverRest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowDriverRest(String value) {
        this.allowDriverRest = value;
    }

    /**
     * Gets the value of the isFixedAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedAddress() {
        return isFixedAddress;
    }

    /**
     * Sets the value of the isFixedAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedAddress(String value) {
        this.isFixedAddress = value;
    }

    /**
     * Gets the value of the primaryAddressLineSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryAddressLineSeq() {
        return primaryAddressLineSeq;
    }

    /**
     * Sets the value of the primaryAddressLineSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryAddressLineSeq(String value) {
        this.primaryAddressLineSeq = value;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the isWMSFacility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsWMSFacility() {
        return isWMSFacility;
    }

    /**
     * Sets the value of the isWMSFacility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsWMSFacility(String value) {
        this.isWMSFacility = value;
    }

    /**
     * Gets the value of the eruGrouping property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEruGrouping() {
        return eruGrouping;
    }

    /**
     * Sets the value of the eruGrouping property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEruGrouping(String value) {
        this.eruGrouping = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class ChildOperationalLocations {

        @XmlElement(name = "Location")
        protected List<LocationType> location;

        /**
         * Gets the value of the location property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the location property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LocationType }
         * 
         * 
         */
        public List<LocationType> getLocation() {
            if (location == null) {
                location = new ArrayList<LocationType>();
            }
            return this.location;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LoadUnloadPointName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
     *         &lt;element name="IsLoad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LoadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IsUnload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UnloadSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "loadUnloadPointName",
        "description",
        "equipmentGroupProfileGid",
        "isLoad",
        "loadSequence",
        "isUnload",
        "unloadSequence"
    })
    public static class LoadUnloadPoint {

        @XmlElement(name = "LoadUnloadPointName", required = true)
        protected String loadUnloadPointName;
        @XmlElement(name = "Description")
        protected String description;
        @XmlElement(name = "EquipmentGroupProfileGid")
        protected GLogXMLGidType equipmentGroupProfileGid;
        @XmlElement(name = "IsLoad")
        protected String isLoad;
        @XmlElement(name = "LoadSequence")
        protected String loadSequence;
        @XmlElement(name = "IsUnload")
        protected String isUnload;
        @XmlElement(name = "UnloadSequence")
        protected String unloadSequence;

        /**
         * Gets the value of the loadUnloadPointName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoadUnloadPointName() {
            return loadUnloadPointName;
        }

        /**
         * Sets the value of the loadUnloadPointName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoadUnloadPointName(String value) {
            this.loadUnloadPointName = value;
        }

        /**
         * Gets the value of the description property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescription() {
            return description;
        }

        /**
         * Sets the value of the description property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescription(String value) {
            this.description = value;
        }

        /**
         * Gets the value of the equipmentGroupProfileGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getEquipmentGroupProfileGid() {
            return equipmentGroupProfileGid;
        }

        /**
         * Sets the value of the equipmentGroupProfileGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
            this.equipmentGroupProfileGid = value;
        }

        /**
         * Gets the value of the isLoad property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsLoad() {
            return isLoad;
        }

        /**
         * Sets the value of the isLoad property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsLoad(String value) {
            this.isLoad = value;
        }

        /**
         * Gets the value of the loadSequence property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoadSequence() {
            return loadSequence;
        }

        /**
         * Sets the value of the loadSequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoadSequence(String value) {
            this.loadSequence = value;
        }

        /**
         * Gets the value of the isUnload property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIsUnload() {
            return isUnload;
        }

        /**
         * Sets the value of the isUnload property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIsUnload(String value) {
            this.isUnload = value;
        }

        /**
         * Gets the value of the unloadSequence property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUnloadSequence() {
            return unloadSequence;
        }

        /**
         * Sets the value of the unloadSequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUnloadSequence(String value) {
            this.unloadSequence = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LocationCapacityGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
     *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationCapacityGroupGid",
        "effectiveDate",
        "expirationDate"
    })
    public static class LocationCapacityGroupInfo {

        @XmlElement(name = "LocationCapacityGroupGid", required = true)
        protected GLogXMLGidType locationCapacityGroupGid;
        @XmlElement(name = "EffectiveDate", required = true)
        protected GLogDateTimeType effectiveDate;
        @XmlElement(name = "ExpirationDate", required = true)
        protected GLogDateTimeType expirationDate;

        /**
         * Gets the value of the locationCapacityGroupGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationCapacityGroupGid() {
            return locationCapacityGroupGid;
        }

        /**
         * Sets the value of the locationCapacityGroupGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationCapacityGroupGid(GLogXMLGidType value) {
            this.locationCapacityGroupGid = value;
        }

        /**
         * Gets the value of the effectiveDate property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Sets the value of the effectiveDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setEffectiveDate(GLogDateTimeType value) {
            this.effectiveDate = value;
        }

        /**
         * Gets the value of the expirationDate property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getExpirationDate() {
            return expirationDate;
        }

        /**
         * Sets the value of the expirationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setExpirationDate(GLogDateTimeType value) {
            this.expirationDate = value;
        }

    }

}

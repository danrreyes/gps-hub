
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * The Location THU capacity is a structure specifying the capacity that a transport handling unit may hold when it is being delivered to this location for
 *             this role. If defined the maximum values here override those that are specified on the Ship Unit Specification table that defines the transport handling unit.
 *          
 * 
 * <p>Java class for LocationTHUCapacityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationTHUCapacityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransportHandlingUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="MaxLength" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/&gt;
 *         &lt;element name="MaxWidth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWidthType" minOccurs="0"/&gt;
 *         &lt;element name="MaxHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLHeightType" minOccurs="0"/&gt;
 *         &lt;element name="MaxWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="MaxVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationTHUCapacityType", propOrder = {
    "transportHandlingUnitGid",
    "maxLength",
    "maxWidth",
    "maxHeight",
    "maxWeight",
    "maxVolume"
})
public class LocationTHUCapacityType {

    @XmlElement(name = "TransportHandlingUnitGid")
    protected GLogXMLGidType transportHandlingUnitGid;
    @XmlElement(name = "MaxLength")
    protected GLogXMLLengthType maxLength;
    @XmlElement(name = "MaxWidth")
    protected GLogXMLWidthType maxWidth;
    @XmlElement(name = "MaxHeight")
    protected GLogXMLHeightType maxHeight;
    @XmlElement(name = "MaxWeight")
    protected GLogXMLWeightType maxWeight;
    @XmlElement(name = "MaxVolume")
    protected GLogXMLVolumeType maxVolume;

    /**
     * Gets the value of the transportHandlingUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportHandlingUnitGid() {
        return transportHandlingUnitGid;
    }

    /**
     * Sets the value of the transportHandlingUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportHandlingUnitGid(GLogXMLGidType value) {
        this.transportHandlingUnitGid = value;
    }

    /**
     * Gets the value of the maxLength property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getMaxLength() {
        return maxLength;
    }

    /**
     * Sets the value of the maxLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setMaxLength(GLogXMLLengthType value) {
        this.maxLength = value;
    }

    /**
     * Gets the value of the maxWidth property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public GLogXMLWidthType getMaxWidth() {
        return maxWidth;
    }

    /**
     * Sets the value of the maxWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public void setMaxWidth(GLogXMLWidthType value) {
        this.maxWidth = value;
    }

    /**
     * Gets the value of the maxHeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public GLogXMLHeightType getMaxHeight() {
        return maxHeight;
    }

    /**
     * Sets the value of the maxHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public void setMaxHeight(GLogXMLHeightType value) {
        this.maxHeight = value;
    }

    /**
     * Gets the value of the maxWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxWeight() {
        return maxWeight;
    }

    /**
     * Sets the value of the maxWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxWeight(GLogXMLWeightType value) {
        this.maxWeight = value;
    }

    /**
     * Gets the value of the maxVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getMaxVolume() {
        return maxVolume;
    }

    /**
     * Sets the value of the maxVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setMaxVolume(GLogXMLVolumeType value) {
        this.maxVolume = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Refnum is a reference number for an object. It consists of a qualifier and a value.
 * 
 * <p>Java class for RefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefnumType", propOrder = {
    "refnumQualifierGid",
    "refnumValue"
})
public class RefnumType {

    @XmlElement(name = "RefnumQualifierGid", required = true)
    protected GLogXMLGidType refnumQualifierGid;
    @XmlElement(name = "RefnumValue", required = true)
    protected String refnumValue;

    /**
     * Gets the value of the refnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRefnumQualifierGid() {
        return refnumQualifierGid;
    }

    /**
     * Sets the value of the refnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRefnumQualifierGid(GLogXMLGidType value) {
        this.refnumQualifierGid = value;
    }

    /**
     * Gets the value of the refnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefnumValue() {
        return refnumValue;
    }

    /**
     * Sets the value of the refnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefnumValue(String value) {
        this.refnumValue = value;
    }

}

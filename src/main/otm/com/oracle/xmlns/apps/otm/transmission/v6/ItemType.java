
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Item is used to transmit item master data to GLog.
 *          
 * 
 * <p>Java class for ItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="ItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ItemName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="CommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CommodityName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CommodityProtectiveService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommodityProtectiveServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NMFCArticleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="NMFCClassGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="STCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="HTSGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SITCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UDCClassListGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PreviousItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BrandName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManufacturedCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsDrawback" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IATAScrCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ItemFeature" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemFeatureType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PricePerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PricePerUnitType" minOccurs="0"/&gt;
 *         &lt;element name="Item" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ChildItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GtmItemClassification" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemClassificationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GtmItemDescription" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemDescriptionType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GtmItemCountryOfOrigin" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemCountryOfOriginType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GtmItemUomConversion" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmItemUomConversionType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="ItemOrigin" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemOriginType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PartnerItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PartnerSiteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ItemTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CountryOfOriginGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ItemValueSet" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemValueSetType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemType", propOrder = {
    "sendReason",
    "transactionCode",
    "replaceChildren",
    "itemGid",
    "itemName",
    "description",
    "effectiveDate",
    "expirationDate",
    "commodityGid",
    "commodityName",
    "commodityProtectiveService",
    "nmfcArticleGid",
    "nmfcClassGid",
    "stccGid",
    "htsGid",
    "sitcGid",
    "userDefinedCommodityGid",
    "udcClassListGid",
    "previousItemGid",
    "brandName",
    "manufacturedCountryCode3Gid",
    "isDrawback",
    "iataScrCodeGid",
    "accessorialCodeGid",
    "specialServiceGid",
    "remark",
    "itemFeature",
    "refnum",
    "text",
    "unitOfMeasure",
    "pricePerUnit",
    "item",
    "childItemCount",
    "gtmItemClassification",
    "gtmItemDescription",
    "gtmItemCountryOfOrigin",
    "gtmItemUomConversion",
    "priority",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "itemOrigin",
    "partnerItemGid",
    "partnerSiteGid",
    "itemTypeGid",
    "countryOfOriginGid",
    "itemValueSet"
})
public class ItemType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "ItemGid", required = true)
    protected GLogXMLGidType itemGid;
    @XmlElement(name = "ItemName")
    protected String itemName;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "CommodityGid")
    protected GLogXMLGidType commodityGid;
    @XmlElement(name = "CommodityName")
    protected String commodityName;
    @XmlElement(name = "CommodityProtectiveService")
    protected List<CommodityProtectiveServiceType> commodityProtectiveService;
    @XmlElement(name = "NMFCArticleGid")
    protected GLogXMLGidType nmfcArticleGid;
    @XmlElement(name = "NMFCClassGid")
    protected GLogXMLGidType nmfcClassGid;
    @XmlElement(name = "STCCGid")
    protected GLogXMLGidType stccGid;
    @XmlElement(name = "HTSGid")
    protected GLogXMLGidType htsGid;
    @XmlElement(name = "SITCGid")
    protected GLogXMLGidType sitcGid;
    @XmlElement(name = "UserDefinedCommodityGid")
    protected GLogXMLGidType userDefinedCommodityGid;
    @XmlElement(name = "UDCClassListGid")
    protected GLogXMLGidType udcClassListGid;
    @XmlElement(name = "PreviousItemGid")
    protected GLogXMLGidType previousItemGid;
    @XmlElement(name = "BrandName")
    protected String brandName;
    @XmlElement(name = "ManufacturedCountryCode3Gid")
    protected GLogXMLGidType manufacturedCountryCode3Gid;
    @XmlElement(name = "IsDrawback")
    protected String isDrawback;
    @XmlElement(name = "IATAScrCodeGid")
    protected GLogXMLGidType iataScrCodeGid;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "SpecialServiceGid")
    protected List<GLogXMLGidType> specialServiceGid;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ItemFeature")
    protected List<ItemFeatureType> itemFeature;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "UnitOfMeasure")
    protected String unitOfMeasure;
    @XmlElement(name = "PricePerUnit")
    protected PricePerUnitType pricePerUnit;
    @XmlElement(name = "Item")
    protected List<ItemType> item;
    @XmlElement(name = "ChildItemCount")
    protected String childItemCount;
    @XmlElement(name = "GtmItemClassification")
    protected List<GtmItemClassificationType> gtmItemClassification;
    @XmlElement(name = "GtmItemDescription")
    protected List<GtmItemDescriptionType> gtmItemDescription;
    @XmlElement(name = "GtmItemCountryOfOrigin")
    protected List<GtmItemCountryOfOriginType> gtmItemCountryOfOrigin;
    @XmlElement(name = "GtmItemUomConversion")
    protected List<GtmItemUomConversionType> gtmItemUomConversion;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "ItemOrigin")
    protected List<ItemOriginType> itemOrigin;
    @XmlElement(name = "PartnerItemGid")
    protected List<GLogXMLGidType> partnerItemGid;
    @XmlElement(name = "PartnerSiteGid")
    protected List<GLogXMLGidType> partnerSiteGid;
    @XmlElement(name = "ItemTypeGid")
    protected GLogXMLGidType itemTypeGid;
    @XmlElement(name = "CountryOfOriginGid")
    protected GLogXMLGidType countryOfOriginGid;
    @XmlElement(name = "ItemValueSet")
    protected List<ItemValueSetType> itemValueSet;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the itemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemGid() {
        return itemGid;
    }

    /**
     * Sets the value of the itemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemGid(GLogXMLGidType value) {
        this.itemGid = value;
    }

    /**
     * Gets the value of the itemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * Sets the value of the itemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemName(String value) {
        this.itemName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the commodityGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommodityGid() {
        return commodityGid;
    }

    /**
     * Sets the value of the commodityGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommodityGid(GLogXMLGidType value) {
        this.commodityGid = value;
    }

    /**
     * Gets the value of the commodityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommodityName() {
        return commodityName;
    }

    /**
     * Sets the value of the commodityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommodityName(String value) {
        this.commodityName = value;
    }

    /**
     * Gets the value of the commodityProtectiveService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the commodityProtectiveService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommodityProtectiveService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommodityProtectiveServiceType }
     * 
     * 
     */
    public List<CommodityProtectiveServiceType> getCommodityProtectiveService() {
        if (commodityProtectiveService == null) {
            commodityProtectiveService = new ArrayList<CommodityProtectiveServiceType>();
        }
        return this.commodityProtectiveService;
    }

    /**
     * Gets the value of the nmfcArticleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNMFCArticleGid() {
        return nmfcArticleGid;
    }

    /**
     * Sets the value of the nmfcArticleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNMFCArticleGid(GLogXMLGidType value) {
        this.nmfcArticleGid = value;
    }

    /**
     * Gets the value of the nmfcClassGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNMFCClassGid() {
        return nmfcClassGid;
    }

    /**
     * Sets the value of the nmfcClassGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNMFCClassGid(GLogXMLGidType value) {
        this.nmfcClassGid = value;
    }

    /**
     * Gets the value of the stccGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSTCCGid() {
        return stccGid;
    }

    /**
     * Sets the value of the stccGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSTCCGid(GLogXMLGidType value) {
        this.stccGid = value;
    }

    /**
     * Gets the value of the htsGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHTSGid() {
        return htsGid;
    }

    /**
     * Sets the value of the htsGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHTSGid(GLogXMLGidType value) {
        this.htsGid = value;
    }

    /**
     * Gets the value of the sitcGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSITCGid() {
        return sitcGid;
    }

    /**
     * Sets the value of the sitcGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSITCGid(GLogXMLGidType value) {
        this.sitcGid = value;
    }

    /**
     * Gets the value of the userDefinedCommodityGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCommodityGid() {
        return userDefinedCommodityGid;
    }

    /**
     * Sets the value of the userDefinedCommodityGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCommodityGid(GLogXMLGidType value) {
        this.userDefinedCommodityGid = value;
    }

    /**
     * Gets the value of the udcClassListGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUDCClassListGid() {
        return udcClassListGid;
    }

    /**
     * Sets the value of the udcClassListGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUDCClassListGid(GLogXMLGidType value) {
        this.udcClassListGid = value;
    }

    /**
     * Gets the value of the previousItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPreviousItemGid() {
        return previousItemGid;
    }

    /**
     * Sets the value of the previousItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPreviousItemGid(GLogXMLGidType value) {
        this.previousItemGid = value;
    }

    /**
     * Gets the value of the brandName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * Sets the value of the brandName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandName(String value) {
        this.brandName = value;
    }

    /**
     * Gets the value of the manufacturedCountryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getManufacturedCountryCode3Gid() {
        return manufacturedCountryCode3Gid;
    }

    /**
     * Sets the value of the manufacturedCountryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setManufacturedCountryCode3Gid(GLogXMLGidType value) {
        this.manufacturedCountryCode3Gid = value;
    }

    /**
     * Gets the value of the isDrawback property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDrawback() {
        return isDrawback;
    }

    /**
     * Sets the value of the isDrawback property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDrawback(String value) {
        this.isDrawback = value;
    }

    /**
     * Gets the value of the iataScrCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIATAScrCodeGid() {
        return iataScrCodeGid;
    }

    /**
     * Sets the value of the iataScrCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIATAScrCodeGid(GLogXMLGidType value) {
        this.iataScrCodeGid = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the specialServiceGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecialServiceGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getSpecialServiceGid() {
        if (specialServiceGid == null) {
            specialServiceGid = new ArrayList<GLogXMLGidType>();
        }
        return this.specialServiceGid;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the itemFeature property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemFeature property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemFeature().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemFeatureType }
     * 
     * 
     */
    public List<ItemFeatureType> getItemFeature() {
        if (itemFeature == null) {
            itemFeature = new ArrayList<ItemFeatureType>();
        }
        return this.itemFeature;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Gets the value of the unitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the value of the unitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasure(String value) {
        this.unitOfMeasure = value;
    }

    /**
     * Gets the value of the pricePerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link PricePerUnitType }
     *     
     */
    public PricePerUnitType getPricePerUnit() {
        return pricePerUnit;
    }

    /**
     * Sets the value of the pricePerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricePerUnitType }
     *     
     */
    public void setPricePerUnit(PricePerUnitType value) {
        this.pricePerUnit = value;
    }

    /**
     * Gets the value of the item property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the item property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemType }
     * 
     * 
     */
    public List<ItemType> getItem() {
        if (item == null) {
            item = new ArrayList<ItemType>();
        }
        return this.item;
    }

    /**
     * Gets the value of the childItemCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChildItemCount() {
        return childItemCount;
    }

    /**
     * Sets the value of the childItemCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChildItemCount(String value) {
        this.childItemCount = value;
    }

    /**
     * Gets the value of the gtmItemClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemClassificationType }
     * 
     * 
     */
    public List<GtmItemClassificationType> getGtmItemClassification() {
        if (gtmItemClassification == null) {
            gtmItemClassification = new ArrayList<GtmItemClassificationType>();
        }
        return this.gtmItemClassification;
    }

    /**
     * Gets the value of the gtmItemDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemDescriptionType }
     * 
     * 
     */
    public List<GtmItemDescriptionType> getGtmItemDescription() {
        if (gtmItemDescription == null) {
            gtmItemDescription = new ArrayList<GtmItemDescriptionType>();
        }
        return this.gtmItemDescription;
    }

    /**
     * Gets the value of the gtmItemCountryOfOrigin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemCountryOfOrigin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemCountryOfOrigin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemCountryOfOriginType }
     * 
     * 
     */
    public List<GtmItemCountryOfOriginType> getGtmItemCountryOfOrigin() {
        if (gtmItemCountryOfOrigin == null) {
            gtmItemCountryOfOrigin = new ArrayList<GtmItemCountryOfOriginType>();
        }
        return this.gtmItemCountryOfOrigin;
    }

    /**
     * Gets the value of the gtmItemUomConversion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmItemUomConversion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmItemUomConversion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmItemUomConversionType }
     * 
     * 
     */
    public List<GtmItemUomConversionType> getGtmItemUomConversion() {
        if (gtmItemUomConversion == null) {
            gtmItemUomConversion = new ArrayList<GtmItemUomConversionType>();
        }
        return this.gtmItemUomConversion;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the itemOrigin property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemOrigin property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemOrigin().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemOriginType }
     * 
     * 
     */
    public List<ItemOriginType> getItemOrigin() {
        if (itemOrigin == null) {
            itemOrigin = new ArrayList<ItemOriginType>();
        }
        return this.itemOrigin;
    }

    /**
     * Gets the value of the partnerItemGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the partnerItemGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnerItemGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getPartnerItemGid() {
        if (partnerItemGid == null) {
            partnerItemGid = new ArrayList<GLogXMLGidType>();
        }
        return this.partnerItemGid;
    }

    /**
     * Gets the value of the partnerSiteGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the partnerSiteGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartnerSiteGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getPartnerSiteGid() {
        if (partnerSiteGid == null) {
            partnerSiteGid = new ArrayList<GLogXMLGidType>();
        }
        return this.partnerSiteGid;
    }

    /**
     * Gets the value of the itemTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemTypeGid() {
        return itemTypeGid;
    }

    /**
     * Sets the value of the itemTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemTypeGid(GLogXMLGidType value) {
        this.itemTypeGid = value;
    }

    /**
     * Gets the value of the countryOfOriginGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryOfOriginGid() {
        return countryOfOriginGid;
    }

    /**
     * Sets the value of the countryOfOriginGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryOfOriginGid(GLogXMLGidType value) {
        this.countryOfOriginGid = value;
    }

    /**
     * Gets the value of the itemValueSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemValueSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemValueSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemValueSetType }
     * 
     * 
     */
    public List<ItemValueSetType> getItemValueSet() {
        if (itemValueSet == null) {
            itemValueSet = new ArrayList<ItemValueSetType>();
        }
        return this.itemValueSet;
    }

}

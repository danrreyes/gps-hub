
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Provides the fields that are used to match the affected ship units.
 * 
 * <p>Java class for ShipModViaOrderSUMatchType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipModViaOrderSUMatchType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransOrderShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseInstrSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;element name="SEquipmentGidQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentGidQueryType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipModViaOrderSUMatchType", propOrder = {
    "transOrderGid",
    "transOrderShipUnitGid",
    "releaseInstrSeq",
    "releaseGid",
    "releaseShipUnitGid",
    "transportHandlingUnitGid",
    "sEquipmentGid",
    "sEquipmentGidQuery"
})
public class ShipModViaOrderSUMatchType {

    @XmlElement(name = "TransOrderGid")
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "TransOrderShipUnitGid")
    protected GLogXMLGidType transOrderShipUnitGid;
    @XmlElement(name = "ReleaseInstrSeq")
    protected String releaseInstrSeq;
    @XmlElement(name = "ReleaseGid")
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "ReleaseShipUnitGid")
    protected GLogXMLGidType releaseShipUnitGid;
    @XmlElement(name = "TransportHandlingUnitGid")
    protected GLogXMLGidType transportHandlingUnitGid;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "SEquipmentGidQuery")
    protected SEquipmentGidQueryType sEquipmentGidQuery;

    /**
     * Gets the value of the transOrderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Sets the value of the transOrderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Gets the value of the transOrderShipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderShipUnitGid() {
        return transOrderShipUnitGid;
    }

    /**
     * Sets the value of the transOrderShipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderShipUnitGid(GLogXMLGidType value) {
        this.transOrderShipUnitGid = value;
    }

    /**
     * Gets the value of the releaseInstrSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseInstrSeq() {
        return releaseInstrSeq;
    }

    /**
     * Sets the value of the releaseInstrSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseInstrSeq(String value) {
        this.releaseInstrSeq = value;
    }

    /**
     * Gets the value of the releaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Sets the value of the releaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Gets the value of the releaseShipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseShipUnitGid() {
        return releaseShipUnitGid;
    }

    /**
     * Sets the value of the releaseShipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseShipUnitGid(GLogXMLGidType value) {
        this.releaseShipUnitGid = value;
    }

    /**
     * Gets the value of the transportHandlingUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportHandlingUnitGid() {
        return transportHandlingUnitGid;
    }

    /**
     * Sets the value of the transportHandlingUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportHandlingUnitGid(GLogXMLGidType value) {
        this.transportHandlingUnitGid = value;
    }

    /**
     * Gets the value of the sEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Sets the value of the sEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the sEquipmentGidQuery property.
     * 
     * @return
     *     possible object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public SEquipmentGidQueryType getSEquipmentGidQuery() {
        return sEquipmentGidQuery;
    }

    /**
     * Sets the value of the sEquipmentGidQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public void setSEquipmentGidQuery(SEquipmentGidQueryType value) {
        this.sEquipmentGidQuery = value;
    }

}

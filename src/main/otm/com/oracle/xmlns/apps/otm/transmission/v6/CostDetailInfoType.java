
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies additional cost details information for the RIQ Query Result.
 * 
 * <p>Java class for CostDetailInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CostDetailInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RateGeoCostGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoCostSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateUnitBreakGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/&gt;
 *         &lt;element name="RATE_GEO_COST_UNIT_BREAK" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_UNIT_BREAK_TYPE" minOccurs="0"/&gt;
 *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostDetailInfoType", propOrder = {
    "rateGeoCostGroupGid",
    "rateGeoCostSeq",
    "rateUnitBreakGid",
    "rategeocost",
    "rategeocostunitbreak",
    "accessorialcost"
})
public class CostDetailInfoType {

    @XmlElement(name = "RateGeoCostGroupGid")
    protected GLogXMLGidType rateGeoCostGroupGid;
    @XmlElement(name = "RateGeoCostSeq")
    protected String rateGeoCostSeq;
    @XmlElement(name = "RateUnitBreakGid")
    protected GLogXMLGidType rateUnitBreakGid;
    @XmlElement(name = "RATE_GEO_COST")
    protected RATEGEOCOSTTYPE rategeocost;
    @XmlElement(name = "RATE_GEO_COST_UNIT_BREAK")
    protected RATEGEOCOSTUNITBREAKTYPE rategeocostunitbreak;
    @XmlElement(name = "ACCESSORIAL_COST")
    protected ACCESSORIALCOSTTYPE accessorialcost;

    /**
     * Gets the value of the rateGeoCostGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoCostGroupGid() {
        return rateGeoCostGroupGid;
    }

    /**
     * Sets the value of the rateGeoCostGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoCostGroupGid(GLogXMLGidType value) {
        this.rateGeoCostGroupGid = value;
    }

    /**
     * Gets the value of the rateGeoCostSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateGeoCostSeq() {
        return rateGeoCostSeq;
    }

    /**
     * Sets the value of the rateGeoCostSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateGeoCostSeq(String value) {
        this.rateGeoCostSeq = value;
    }

    /**
     * Gets the value of the rateUnitBreakGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateUnitBreakGid() {
        return rateUnitBreakGid;
    }

    /**
     * Sets the value of the rateUnitBreakGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateUnitBreakGid(GLogXMLGidType value) {
        this.rateUnitBreakGid = value;
    }

    /**
     * Gets the value of the rategeocost property.
     * 
     * @return
     *     possible object is
     *     {@link RATEGEOCOSTTYPE }
     *     
     */
    public RATEGEOCOSTTYPE getRATEGEOCOST() {
        return rategeocost;
    }

    /**
     * Sets the value of the rategeocost property.
     * 
     * @param value
     *     allowed object is
     *     {@link RATEGEOCOSTTYPE }
     *     
     */
    public void setRATEGEOCOST(RATEGEOCOSTTYPE value) {
        this.rategeocost = value;
    }

    /**
     * Gets the value of the rategeocostunitbreak property.
     * 
     * @return
     *     possible object is
     *     {@link RATEGEOCOSTUNITBREAKTYPE }
     *     
     */
    public RATEGEOCOSTUNITBREAKTYPE getRATEGEOCOSTUNITBREAK() {
        return rategeocostunitbreak;
    }

    /**
     * Sets the value of the rategeocostunitbreak property.
     * 
     * @param value
     *     allowed object is
     *     {@link RATEGEOCOSTUNITBREAKTYPE }
     *     
     */
    public void setRATEGEOCOSTUNITBREAK(RATEGEOCOSTUNITBREAKTYPE value) {
        this.rategeocostunitbreak = value;
    }

    /**
     * Gets the value of the accessorialcost property.
     * 
     * @return
     *     possible object is
     *     {@link ACCESSORIALCOSTTYPE }
     *     
     */
    public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
        return accessorialcost;
    }

    /**
     * Sets the value of the accessorialcost property.
     * 
     * @param value
     *     allowed object is
     *     {@link ACCESSORIALCOSTTYPE }
     *     
     */
    public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
        this.accessorialcost = value;
    }

}

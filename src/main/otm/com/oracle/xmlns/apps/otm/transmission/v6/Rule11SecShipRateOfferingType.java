
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains the information from the RATE_OFFERING for the second Shipment in a Rail Rule 11 move.
 * 
 * <p>Java class for Rule11SecShipRateOfferingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Rule11SecShipRateOfferingType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TariffRefnumQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffPubAuthority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffRegAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffAgencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffIssuingCarrierId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rule11SecShipRateOfferingType", propOrder = {
    "tariffRefnumQualifier",
    "tariffRefnumValue",
    "tariffName",
    "tariffPubAuthority",
    "tariffRegAgencyCode",
    "tariffAgencyCode",
    "tariffIssuingCarrierId"
})
public class Rule11SecShipRateOfferingType {

    @XmlElement(name = "TariffRefnumQualifier")
    protected String tariffRefnumQualifier;
    @XmlElement(name = "TariffRefnumValue")
    protected String tariffRefnumValue;
    @XmlElement(name = "TariffName")
    protected String tariffName;
    @XmlElement(name = "TariffPubAuthority")
    protected String tariffPubAuthority;
    @XmlElement(name = "TariffRegAgencyCode")
    protected String tariffRegAgencyCode;
    @XmlElement(name = "TariffAgencyCode")
    protected String tariffAgencyCode;
    @XmlElement(name = "TariffIssuingCarrierId")
    protected String tariffIssuingCarrierId;

    /**
     * Gets the value of the tariffRefnumQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumQualifier() {
        return tariffRefnumQualifier;
    }

    /**
     * Sets the value of the tariffRefnumQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumQualifier(String value) {
        this.tariffRefnumQualifier = value;
    }

    /**
     * Gets the value of the tariffRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumValue() {
        return tariffRefnumValue;
    }

    /**
     * Sets the value of the tariffRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumValue(String value) {
        this.tariffRefnumValue = value;
    }

    /**
     * Gets the value of the tariffName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffName() {
        return tariffName;
    }

    /**
     * Sets the value of the tariffName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffName(String value) {
        this.tariffName = value;
    }

    /**
     * Gets the value of the tariffPubAuthority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffPubAuthority() {
        return tariffPubAuthority;
    }

    /**
     * Sets the value of the tariffPubAuthority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffPubAuthority(String value) {
        this.tariffPubAuthority = value;
    }

    /**
     * Gets the value of the tariffRegAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRegAgencyCode() {
        return tariffRegAgencyCode;
    }

    /**
     * Sets the value of the tariffRegAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRegAgencyCode(String value) {
        this.tariffRegAgencyCode = value;
    }

    /**
     * Gets the value of the tariffAgencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffAgencyCode() {
        return tariffAgencyCode;
    }

    /**
     * Sets the value of the tariffAgencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffAgencyCode(String value) {
        this.tariffAgencyCode = value;
    }

    /**
     * Gets the value of the tariffIssuingCarrierId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffIssuingCarrierId() {
        return tariffIssuingCarrierId;
    }

    /**
     * Sets the value of the tariffIssuingCarrierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffIssuingCarrierId(String value) {
        this.tariffIssuingCarrierId = value;
    }

}

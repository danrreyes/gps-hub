
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             EquipmentRefnum is an alternate method for identifying an Equipment. It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for EquipmentRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EquipmentRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EquipmentRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="EquipmentRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentRefnumType", propOrder = {
    "equipmentRefnumQualifierGid",
    "equipmentRefnumValue"
})
public class EquipmentRefnumType {

    @XmlElement(name = "EquipmentRefnumQualifierGid", required = true)
    protected GLogXMLGidType equipmentRefnumQualifierGid;
    @XmlElement(name = "EquipmentRefnumValue", required = true)
    protected String equipmentRefnumValue;

    /**
     * Gets the value of the equipmentRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentRefnumQualifierGid() {
        return equipmentRefnumQualifierGid;
    }

    /**
     * Sets the value of the equipmentRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentRefnumQualifierGid(GLogXMLGidType value) {
        this.equipmentRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the equipmentRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentRefnumValue() {
        return equipmentRefnumValue;
    }

    /**
     * Sets the value of the equipmentRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentRefnumValue(String value) {
        this.equipmentRefnumValue = value;
    }

}

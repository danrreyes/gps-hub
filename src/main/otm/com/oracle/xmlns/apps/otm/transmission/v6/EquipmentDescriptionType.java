
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Equipment type. Characterizes the equipment.
 * 
 * <p>Java class for EquipmentDescriptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EquipmentDescriptionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="EquipTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipDescCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipDescCodeDef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ISOEquipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ISOEquipCodeDef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="NominalWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="InteriorVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="Metric1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Metric2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InteriorLWH" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/&gt;
 *         &lt;element name="ExteriorLWH" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/&gt;
 *         &lt;element name="IsTemperatureControl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TemperatureControlGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TagInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TagInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EquipTypeSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipTypeSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentDescriptionType", propOrder = {
    "equipmentTypeGid",
    "equipTypeName",
    "equipDesc",
    "equipDescCode",
    "equipDescCodeDef",
    "isoEquipCode",
    "isoEquipCodeDef",
    "maxWeight",
    "tareWeight",
    "nominalWeight",
    "interiorVolume",
    "metric1",
    "metric2",
    "interiorLWH",
    "exteriorLWH",
    "isTemperatureControl",
    "temperatureControlGid",
    "tagInfo",
    "remark",
    "equipTypeSpecialService"
})
public class EquipmentDescriptionType {

    @XmlElement(name = "EquipmentTypeGid", required = true)
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipTypeName")
    protected String equipTypeName;
    @XmlElement(name = "EquipDesc")
    protected String equipDesc;
    @XmlElement(name = "EquipDescCode")
    protected String equipDescCode;
    @XmlElement(name = "EquipDescCodeDef")
    protected String equipDescCodeDef;
    @XmlElement(name = "ISOEquipCode")
    protected String isoEquipCode;
    @XmlElement(name = "ISOEquipCodeDef")
    protected String isoEquipCodeDef;
    @XmlElement(name = "MaxWeight")
    protected GLogXMLWeightType maxWeight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "NominalWeight")
    protected GLogXMLWeightType nominalWeight;
    @XmlElement(name = "InteriorVolume")
    protected GLogXMLVolumeType interiorVolume;
    @XmlElement(name = "Metric1")
    protected String metric1;
    @XmlElement(name = "Metric2")
    protected String metric2;
    @XmlElement(name = "InteriorLWH")
    protected LengthWidthHeightType interiorLWH;
    @XmlElement(name = "ExteriorLWH")
    protected LengthWidthHeightType exteriorLWH;
    @XmlElement(name = "IsTemperatureControl")
    protected String isTemperatureControl;
    @XmlElement(name = "TemperatureControlGid")
    protected GLogXMLGidType temperatureControlGid;
    @XmlElement(name = "TagInfo")
    protected TagInfoType tagInfo;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "EquipTypeSpecialService")
    protected List<EquipTypeSpecialServiceType> equipTypeSpecialService;

    /**
     * Gets the value of the equipmentTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Sets the value of the equipmentTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Gets the value of the equipTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipTypeName() {
        return equipTypeName;
    }

    /**
     * Sets the value of the equipTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipTypeName(String value) {
        this.equipTypeName = value;
    }

    /**
     * Gets the value of the equipDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipDesc() {
        return equipDesc;
    }

    /**
     * Sets the value of the equipDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipDesc(String value) {
        this.equipDesc = value;
    }

    /**
     * Gets the value of the equipDescCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipDescCode() {
        return equipDescCode;
    }

    /**
     * Sets the value of the equipDescCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipDescCode(String value) {
        this.equipDescCode = value;
    }

    /**
     * Gets the value of the equipDescCodeDef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipDescCodeDef() {
        return equipDescCodeDef;
    }

    /**
     * Sets the value of the equipDescCodeDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipDescCodeDef(String value) {
        this.equipDescCodeDef = value;
    }

    /**
     * Gets the value of the isoEquipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOEquipCode() {
        return isoEquipCode;
    }

    /**
     * Sets the value of the isoEquipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOEquipCode(String value) {
        this.isoEquipCode = value;
    }

    /**
     * Gets the value of the isoEquipCodeDef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOEquipCodeDef() {
        return isoEquipCodeDef;
    }

    /**
     * Sets the value of the isoEquipCodeDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOEquipCodeDef(String value) {
        this.isoEquipCodeDef = value;
    }

    /**
     * Gets the value of the maxWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxWeight() {
        return maxWeight;
    }

    /**
     * Sets the value of the maxWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxWeight(GLogXMLWeightType value) {
        this.maxWeight = value;
    }

    /**
     * Gets the value of the tareWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Sets the value of the tareWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Gets the value of the nominalWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getNominalWeight() {
        return nominalWeight;
    }

    /**
     * Sets the value of the nominalWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setNominalWeight(GLogXMLWeightType value) {
        this.nominalWeight = value;
    }

    /**
     * Gets the value of the interiorVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getInteriorVolume() {
        return interiorVolume;
    }

    /**
     * Sets the value of the interiorVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setInteriorVolume(GLogXMLVolumeType value) {
        this.interiorVolume = value;
    }

    /**
     * Gets the value of the metric1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetric1() {
        return metric1;
    }

    /**
     * Sets the value of the metric1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetric1(String value) {
        this.metric1 = value;
    }

    /**
     * Gets the value of the metric2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMetric2() {
        return metric2;
    }

    /**
     * Sets the value of the metric2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMetric2(String value) {
        this.metric2 = value;
    }

    /**
     * Gets the value of the interiorLWH property.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getInteriorLWH() {
        return interiorLWH;
    }

    /**
     * Sets the value of the interiorLWH property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setInteriorLWH(LengthWidthHeightType value) {
        this.interiorLWH = value;
    }

    /**
     * Gets the value of the exteriorLWH property.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getExteriorLWH() {
        return exteriorLWH;
    }

    /**
     * Sets the value of the exteriorLWH property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setExteriorLWH(LengthWidthHeightType value) {
        this.exteriorLWH = value;
    }

    /**
     * Gets the value of the isTemperatureControl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemperatureControl() {
        return isTemperatureControl;
    }

    /**
     * Sets the value of the isTemperatureControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemperatureControl(String value) {
        this.isTemperatureControl = value;
    }

    /**
     * Gets the value of the temperatureControlGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTemperatureControlGid() {
        return temperatureControlGid;
    }

    /**
     * Sets the value of the temperatureControlGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTemperatureControlGid(GLogXMLGidType value) {
        this.temperatureControlGid = value;
    }

    /**
     * Gets the value of the tagInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TagInfoType }
     *     
     */
    public TagInfoType getTagInfo() {
        return tagInfo;
    }

    /**
     * Sets the value of the tagInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TagInfoType }
     *     
     */
    public void setTagInfo(TagInfoType value) {
        this.tagInfo = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the equipTypeSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipTypeSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipTypeSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipTypeSpecialServiceType }
     * 
     * 
     */
    public List<EquipTypeSpecialServiceType> getEquipTypeSpecialService() {
        if (equipTypeSpecialService == null) {
            equipTypeSpecialService = new ArrayList<EquipTypeSpecialServiceType>();
        }
        return this.equipTypeSpecialService;
    }

}

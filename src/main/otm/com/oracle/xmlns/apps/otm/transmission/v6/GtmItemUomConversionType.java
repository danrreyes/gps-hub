
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Holds item specific uom-to-uom converstion rates.
 * 
 * <p>Java class for GtmItemUomConversionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmItemUomConversionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FromUomCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ToUomCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ConversionRate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmItemUomConversionType", propOrder = {
    "sequenceNumber",
    "gtmProdClassTypeGid",
    "fromUomCode",
    "toUomCode",
    "conversionRate"
})
public class GtmItemUomConversionType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "GtmProdClassTypeGid")
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "FromUomCode", required = true)
    protected String fromUomCode;
    @XmlElement(name = "ToUomCode", required = true)
    protected String toUomCode;
    @XmlElement(name = "ConversionRate", required = true)
    protected String conversionRate;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the gtmProdClassTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Sets the value of the gtmProdClassTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Gets the value of the fromUomCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromUomCode() {
        return fromUomCode;
    }

    /**
     * Sets the value of the fromUomCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromUomCode(String value) {
        this.fromUomCode = value;
    }

    /**
     * Gets the value of the toUomCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToUomCode() {
        return toUomCode;
    }

    /**
     * Sets the value of the toUomCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToUomCode(String value) {
        this.toUomCode = value;
    }

    /**
     * Gets the value of the conversionRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConversionRate() {
        return conversionRate;
    }

    /**
     * Sets the value of the conversionRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConversionRate(String value) {
        this.conversionRate = value;
    }

}

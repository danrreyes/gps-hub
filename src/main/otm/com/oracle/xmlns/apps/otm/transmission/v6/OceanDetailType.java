
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The OceanDetail provides invoice data specific to ocean carriers.
 *          
 * 
 * <p>Java class for OceanDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OceanDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VesselInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VesselInfoType" minOccurs="0"/&gt;
 *         &lt;element name="InvoiceServiceCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="DeliveryDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="OceanEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OceanEquipmentType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="OceanLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OceanLineItemType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="Port" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PortType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="LetterofCredit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LetterOfCreditType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OceanDetailType", propOrder = {
    "vesselInfo",
    "invoiceServiceCodeGid",
    "deliveryDate",
    "oceanEquipment",
    "oceanLineItem",
    "port",
    "letterofCredit"
})
public class OceanDetailType {

    @XmlElement(name = "VesselInfo")
    protected VesselInfoType vesselInfo;
    @XmlElement(name = "InvoiceServiceCodeGid", required = true)
    protected GLogXMLGidType invoiceServiceCodeGid;
    @XmlElement(name = "DeliveryDate")
    protected GLogDateTimeType deliveryDate;
    @XmlElement(name = "OceanEquipment", required = true)
    protected List<OceanEquipmentType> oceanEquipment;
    @XmlElement(name = "OceanLineItem", required = true)
    protected List<OceanLineItemType> oceanLineItem;
    @XmlElement(name = "Port", required = true)
    protected List<PortType> port;
    @XmlElement(name = "LetterofCredit")
    protected LetterOfCreditType letterofCredit;

    /**
     * Gets the value of the vesselInfo property.
     * 
     * @return
     *     possible object is
     *     {@link VesselInfoType }
     *     
     */
    public VesselInfoType getVesselInfo() {
        return vesselInfo;
    }

    /**
     * Sets the value of the vesselInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselInfoType }
     *     
     */
    public void setVesselInfo(VesselInfoType value) {
        this.vesselInfo = value;
    }

    /**
     * Gets the value of the invoiceServiceCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceServiceCodeGid() {
        return invoiceServiceCodeGid;
    }

    /**
     * Sets the value of the invoiceServiceCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceServiceCodeGid(GLogXMLGidType value) {
        this.invoiceServiceCodeGid = value;
    }

    /**
     * Gets the value of the deliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Sets the value of the deliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryDate(GLogDateTimeType value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the oceanEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the oceanEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOceanEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OceanEquipmentType }
     * 
     * 
     */
    public List<OceanEquipmentType> getOceanEquipment() {
        if (oceanEquipment == null) {
            oceanEquipment = new ArrayList<OceanEquipmentType>();
        }
        return this.oceanEquipment;
    }

    /**
     * Gets the value of the oceanLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the oceanLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOceanLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OceanLineItemType }
     * 
     * 
     */
    public List<OceanLineItemType> getOceanLineItem() {
        if (oceanLineItem == null) {
            oceanLineItem = new ArrayList<OceanLineItemType>();
        }
        return this.oceanLineItem;
    }

    /**
     * Gets the value of the port property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the port property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPort().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortType }
     * 
     * 
     */
    public List<PortType> getPort() {
        if (port == null) {
            port = new ArrayList<PortType>();
        }
        return this.port;
    }

    /**
     * Gets the value of the letterofCredit property.
     * 
     * @return
     *     possible object is
     *     {@link LetterOfCreditType }
     *     
     */
    public LetterOfCreditType getLetterofCredit() {
        return letterofCredit;
    }

    /**
     * Sets the value of the letterofCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link LetterOfCreditType }
     *     
     */
    public void setLetterofCredit(LetterOfCreditType value) {
        this.letterofCredit = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Spefies the Value Added Tax Registration ID for the Corporation.
 * 
 * <p>Java class for VatRegistrationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VatRegistrationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VatRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="IsPrimaryRegistration" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustVatRegCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DefaultIntraEUVatCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VatRegistrationType", propOrder = {
    "vatRegistrationGid",
    "countryCode3Gid",
    "isPrimaryRegistration",
    "custVatRegCountryCode3Gid",
    "defaultIntraEUVatCodeGid"
})
public class VatRegistrationType {

    @XmlElement(name = "VatRegistrationGid", required = true)
    protected GLogXMLGidType vatRegistrationGid;
    @XmlElement(name = "CountryCode3Gid", required = true)
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "IsPrimaryRegistration")
    protected String isPrimaryRegistration;
    @XmlElement(name = "CustVatRegCountryCode3Gid")
    protected GLogXMLGidType custVatRegCountryCode3Gid;
    @XmlElement(name = "DefaultIntraEUVatCodeGid")
    protected GLogXMLGidType defaultIntraEUVatCodeGid;

    /**
     * Gets the value of the vatRegistrationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVatRegistrationGid() {
        return vatRegistrationGid;
    }

    /**
     * Sets the value of the vatRegistrationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVatRegistrationGid(GLogXMLGidType value) {
        this.vatRegistrationGid = value;
    }

    /**
     * Gets the value of the countryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Sets the value of the countryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Gets the value of the isPrimaryRegistration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimaryRegistration() {
        return isPrimaryRegistration;
    }

    /**
     * Sets the value of the isPrimaryRegistration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimaryRegistration(String value) {
        this.isPrimaryRegistration = value;
    }

    /**
     * Gets the value of the custVatRegCountryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCustVatRegCountryCode3Gid() {
        return custVatRegCountryCode3Gid;
    }

    /**
     * Sets the value of the custVatRegCountryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCustVatRegCountryCode3Gid(GLogXMLGidType value) {
        this.custVatRegCountryCode3Gid = value;
    }

    /**
     * Gets the value of the defaultIntraEUVatCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDefaultIntraEUVatCodeGid() {
        return defaultIntraEUVatCodeGid;
    }

    /**
     * Sets the value of the defaultIntraEUVatCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDefaultIntraEUVatCodeGid(GLogXMLGidType value) {
        this.defaultIntraEUVatCodeGid = value;
    }

}

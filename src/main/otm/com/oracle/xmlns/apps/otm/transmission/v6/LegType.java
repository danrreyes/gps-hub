
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             A Leg defines a destination for shipment activity on an itinerary and may establish requirements associated destination.
 *             For example, you can associate scheduling priorities, intermediate stops, service providers, costs, commodities, and
 *             modes of transport as requirements for a particular leg.
 *          
 * 
 * <p>Java class for LegType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LegType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="LegName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="SourceLocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRoleType" minOccurs="0"/&gt;
 *         &lt;element name="DestinationLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="DestLocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRoleType" minOccurs="0"/&gt;
 *         &lt;element name="DestLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="DestLocationRadius" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="DestRadiusEmphasis" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreateSingleShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ExpectedTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ExpectedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ArrangementResponsibility" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SchedulingPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentAssignmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CalculateContractedRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CalculateServiceTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="XDockLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="IntermediaryCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Rule11_Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RailInterModalPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CustomerRateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipGroupProfileSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderAssignmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxLocationCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AutoConsolidateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InterimPointOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ViaSourceLocProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="ViaDestLocProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="ViaSourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ViaDestLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PlanToOperationalLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTemporary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransArbSearchType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LegClassificationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UseConsol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LegInterimPoint" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LegInterimPointType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LegSchedule" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CapacityOverrideGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LegType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RoutingNetworkGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SourceRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="DestRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="LegConsolidationGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DepotProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AllowCrossConsolidation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LegType", propOrder = {
    "legGid",
    "transactionCode",
    "legName",
    "sourceLocationRef",
    "sourceLocationRoleGid",
    "destinationLocationRef",
    "destLocationRoleGid",
    "destLocationProfileGid",
    "destLocationRadius",
    "destRadiusEmphasis",
    "createSingleShipment",
    "isPrimary",
    "serviceProviderProfileGid",
    "equipmentGroupProfileGid",
    "modeProfileGid",
    "expectedTransitTime",
    "expectedCost",
    "arrangementResponsibility",
    "schedulingPriority",
    "equipmentAssignmentType",
    "calculateContractedRate",
    "calculateServiceTime",
    "rateOfferingGid",
    "rateGeoGid",
    "xDockLocation",
    "hazmatRegionGid",
    "intermediaryCorporationGid",
    "rule11Indicator",
    "railInterModalPlanGid",
    "customerRateCode",
    "cofctofc",
    "equipGroupProfileSetGid",
    "serviceProviderAssignmentType",
    "maxLocationCount",
    "autoConsolidateType",
    "interimPointOption",
    "rateServiceProfileGid",
    "paymentMethodCodeGid",
    "viaSourceLocProfileGid",
    "viaDestLocProfileGid",
    "viaSourceLocationRef",
    "viaDestLocationRef",
    "planToOperationalLocation",
    "isTemporary",
    "transArbSearchType",
    "legClassificationGid",
    "useConsol",
    "involvedParty",
    "legInterimPoint",
    "legSchedule",
    "capacityOverrideGid",
    "legType",
    "routingNetworkGid",
    "sourceRegionGid",
    "destRegionGid",
    "legConsolidationGroupGid",
    "depotProfileGid",
    "allowCrossConsolidation"
})
public class LegType {

    @XmlElement(name = "LegGid", required = true)
    protected GLogXMLGidType legGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "LegName")
    protected String legName;
    @XmlElement(name = "SourceLocationRef")
    protected GLogXMLLocRefType sourceLocationRef;
    @XmlElement(name = "SourceLocationRoleGid")
    protected GLogXMLLocRoleType sourceLocationRoleGid;
    @XmlElement(name = "DestinationLocationRef")
    protected GLogXMLLocRefType destinationLocationRef;
    @XmlElement(name = "DestLocationRoleGid")
    protected GLogXMLLocRoleType destLocationRoleGid;
    @XmlElement(name = "DestLocationProfileGid")
    protected GLogXMLLocProfileType destLocationProfileGid;
    @XmlElement(name = "DestLocationRadius")
    protected GLogXMLDistanceType destLocationRadius;
    @XmlElement(name = "DestRadiusEmphasis")
    protected String destRadiusEmphasis;
    @XmlElement(name = "CreateSingleShipment")
    protected String createSingleShipment;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "ModeProfileGid", required = true)
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "ExpectedTransitTime")
    protected GLogXMLDurationType expectedTransitTime;
    @XmlElement(name = "ExpectedCost")
    protected GLogXMLFinancialAmountType expectedCost;
    @XmlElement(name = "ArrangementResponsibility")
    protected String arrangementResponsibility;
    @XmlElement(name = "SchedulingPriority")
    protected String schedulingPriority;
    @XmlElement(name = "EquipmentAssignmentType")
    protected String equipmentAssignmentType;
    @XmlElement(name = "CalculateContractedRate")
    protected String calculateContractedRate;
    @XmlElement(name = "CalculateServiceTime")
    protected String calculateServiceTime;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "XDockLocation")
    protected GLogXMLLocRefType xDockLocation;
    @XmlElement(name = "HazmatRegionGid")
    protected GLogXMLRegionGidType hazmatRegionGid;
    @XmlElement(name = "IntermediaryCorporationGid")
    protected GLogXMLGidType intermediaryCorporationGid;
    @XmlElement(name = "Rule11_Indicator")
    protected String rule11Indicator;
    @XmlElement(name = "RailInterModalPlanGid")
    protected GLogXMLGidType railInterModalPlanGid;
    @XmlElement(name = "CustomerRateCode")
    protected String customerRateCode;
    @XmlElement(name = "COFC_TOFC")
    protected String cofctofc;
    @XmlElement(name = "EquipGroupProfileSetGid")
    protected GLogXMLGidType equipGroupProfileSetGid;
    @XmlElement(name = "ServiceProviderAssignmentType")
    protected String serviceProviderAssignmentType;
    @XmlElement(name = "MaxLocationCount")
    protected String maxLocationCount;
    @XmlElement(name = "AutoConsolidateType")
    protected String autoConsolidateType;
    @XmlElement(name = "InterimPointOption")
    protected String interimPointOption;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "ViaSourceLocProfileGid")
    protected GLogXMLLocProfileType viaSourceLocProfileGid;
    @XmlElement(name = "ViaDestLocProfileGid")
    protected GLogXMLLocProfileType viaDestLocProfileGid;
    @XmlElement(name = "ViaSourceLocationRef")
    protected GLogXMLLocRefType viaSourceLocationRef;
    @XmlElement(name = "ViaDestLocationRef")
    protected GLogXMLLocRefType viaDestLocationRef;
    @XmlElement(name = "PlanToOperationalLocation")
    protected String planToOperationalLocation;
    @XmlElement(name = "IsTemporary")
    protected String isTemporary;
    @XmlElement(name = "TransArbSearchType")
    protected String transArbSearchType;
    @XmlElement(name = "LegClassificationGid")
    protected GLogXMLGidType legClassificationGid;
    @XmlElement(name = "UseConsol")
    protected String useConsol;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "LegInterimPoint")
    protected List<LegInterimPointType> legInterimPoint;
    @XmlElement(name = "LegSchedule")
    protected List<LegType.LegSchedule> legSchedule;
    @XmlElement(name = "CapacityOverrideGid")
    protected List<GLogXMLGidType> capacityOverrideGid;
    @XmlElement(name = "LegType")
    protected String legType;
    @XmlElement(name = "RoutingNetworkGid")
    protected GLogXMLGidType routingNetworkGid;
    @XmlElement(name = "SourceRegionGid")
    protected GLogXMLRegionGidType sourceRegionGid;
    @XmlElement(name = "DestRegionGid")
    protected GLogXMLRegionGidType destRegionGid;
    @XmlElement(name = "LegConsolidationGroupGid")
    protected GLogXMLGidType legConsolidationGroupGid;
    @XmlElement(name = "DepotProfileGid")
    protected GLogXMLGidType depotProfileGid;
    @XmlElement(name = "AllowCrossConsolidation")
    protected String allowCrossConsolidation;

    /**
     * Gets the value of the legGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegGid() {
        return legGid;
    }

    /**
     * Sets the value of the legGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegGid(GLogXMLGidType value) {
        this.legGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the legName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegName() {
        return legName;
    }

    /**
     * Sets the value of the legName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegName(String value) {
        this.legName = value;
    }

    /**
     * Gets the value of the sourceLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourceLocationRef() {
        return sourceLocationRef;
    }

    /**
     * Sets the value of the sourceLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourceLocationRef(GLogXMLLocRefType value) {
        this.sourceLocationRef = value;
    }

    /**
     * Gets the value of the sourceLocationRoleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public GLogXMLLocRoleType getSourceLocationRoleGid() {
        return sourceLocationRoleGid;
    }

    /**
     * Sets the value of the sourceLocationRoleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public void setSourceLocationRoleGid(GLogXMLLocRoleType value) {
        this.sourceLocationRoleGid = value;
    }

    /**
     * Gets the value of the destinationLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestinationLocationRef() {
        return destinationLocationRef;
    }

    /**
     * Sets the value of the destinationLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestinationLocationRef(GLogXMLLocRefType value) {
        this.destinationLocationRef = value;
    }

    /**
     * Gets the value of the destLocationRoleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public GLogXMLLocRoleType getDestLocationRoleGid() {
        return destLocationRoleGid;
    }

    /**
     * Sets the value of the destLocationRoleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRoleType }
     *     
     */
    public void setDestLocationRoleGid(GLogXMLLocRoleType value) {
        this.destLocationRoleGid = value;
    }

    /**
     * Gets the value of the destLocationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getDestLocationProfileGid() {
        return destLocationProfileGid;
    }

    /**
     * Sets the value of the destLocationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setDestLocationProfileGid(GLogXMLLocProfileType value) {
        this.destLocationProfileGid = value;
    }

    /**
     * Gets the value of the destLocationRadius property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDestLocationRadius() {
        return destLocationRadius;
    }

    /**
     * Sets the value of the destLocationRadius property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDestLocationRadius(GLogXMLDistanceType value) {
        this.destLocationRadius = value;
    }

    /**
     * Gets the value of the destRadiusEmphasis property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestRadiusEmphasis() {
        return destRadiusEmphasis;
    }

    /**
     * Sets the value of the destRadiusEmphasis property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestRadiusEmphasis(String value) {
        this.destRadiusEmphasis = value;
    }

    /**
     * Gets the value of the createSingleShipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateSingleShipment() {
        return createSingleShipment;
    }

    /**
     * Sets the value of the createSingleShipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateSingleShipment(String value) {
        this.createSingleShipment = value;
    }

    /**
     * Gets the value of the isPrimary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Sets the value of the isPrimary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Gets the value of the serviceProviderProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Sets the value of the serviceProviderProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the expectedTransitTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getExpectedTransitTime() {
        return expectedTransitTime;
    }

    /**
     * Sets the value of the expectedTransitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setExpectedTransitTime(GLogXMLDurationType value) {
        this.expectedTransitTime = value;
    }

    /**
     * Gets the value of the expectedCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getExpectedCost() {
        return expectedCost;
    }

    /**
     * Sets the value of the expectedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setExpectedCost(GLogXMLFinancialAmountType value) {
        this.expectedCost = value;
    }

    /**
     * Gets the value of the arrangementResponsibility property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrangementResponsibility() {
        return arrangementResponsibility;
    }

    /**
     * Sets the value of the arrangementResponsibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrangementResponsibility(String value) {
        this.arrangementResponsibility = value;
    }

    /**
     * Gets the value of the schedulingPriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchedulingPriority() {
        return schedulingPriority;
    }

    /**
     * Sets the value of the schedulingPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchedulingPriority(String value) {
        this.schedulingPriority = value;
    }

    /**
     * Gets the value of the equipmentAssignmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentAssignmentType() {
        return equipmentAssignmentType;
    }

    /**
     * Sets the value of the equipmentAssignmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentAssignmentType(String value) {
        this.equipmentAssignmentType = value;
    }

    /**
     * Gets the value of the calculateContractedRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateContractedRate() {
        return calculateContractedRate;
    }

    /**
     * Sets the value of the calculateContractedRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateContractedRate(String value) {
        this.calculateContractedRate = value;
    }

    /**
     * Gets the value of the calculateServiceTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateServiceTime() {
        return calculateServiceTime;
    }

    /**
     * Sets the value of the calculateServiceTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateServiceTime(String value) {
        this.calculateServiceTime = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the rateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Sets the value of the rateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Gets the value of the xDockLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getXDockLocation() {
        return xDockLocation;
    }

    /**
     * Sets the value of the xDockLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setXDockLocation(GLogXMLLocRefType value) {
        this.xDockLocation = value;
    }

    /**
     * Gets the value of the hazmatRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getHazmatRegionGid() {
        return hazmatRegionGid;
    }

    /**
     * Sets the value of the hazmatRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setHazmatRegionGid(GLogXMLRegionGidType value) {
        this.hazmatRegionGid = value;
    }

    /**
     * Gets the value of the intermediaryCorporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntermediaryCorporationGid() {
        return intermediaryCorporationGid;
    }

    /**
     * Sets the value of the intermediaryCorporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntermediaryCorporationGid(GLogXMLGidType value) {
        this.intermediaryCorporationGid = value;
    }

    /**
     * Gets the value of the rule11Indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRule11Indicator() {
        return rule11Indicator;
    }

    /**
     * Sets the value of the rule11Indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRule11Indicator(String value) {
        this.rule11Indicator = value;
    }

    /**
     * Gets the value of the railInterModalPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailInterModalPlanGid() {
        return railInterModalPlanGid;
    }

    /**
     * Sets the value of the railInterModalPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailInterModalPlanGid(GLogXMLGidType value) {
        this.railInterModalPlanGid = value;
    }

    /**
     * Gets the value of the customerRateCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRateCode() {
        return customerRateCode;
    }

    /**
     * Sets the value of the customerRateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRateCode(String value) {
        this.customerRateCode = value;
    }

    /**
     * Gets the value of the cofctofc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOFCTOFC() {
        return cofctofc;
    }

    /**
     * Sets the value of the cofctofc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOFCTOFC(String value) {
        this.cofctofc = value;
    }

    /**
     * Gets the value of the equipGroupProfileSetGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipGroupProfileSetGid() {
        return equipGroupProfileSetGid;
    }

    /**
     * Sets the value of the equipGroupProfileSetGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipGroupProfileSetGid(GLogXMLGidType value) {
        this.equipGroupProfileSetGid = value;
    }

    /**
     * Gets the value of the serviceProviderAssignmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderAssignmentType() {
        return serviceProviderAssignmentType;
    }

    /**
     * Sets the value of the serviceProviderAssignmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderAssignmentType(String value) {
        this.serviceProviderAssignmentType = value;
    }

    /**
     * Gets the value of the maxLocationCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxLocationCount() {
        return maxLocationCount;
    }

    /**
     * Sets the value of the maxLocationCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxLocationCount(String value) {
        this.maxLocationCount = value;
    }

    /**
     * Gets the value of the autoConsolidateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoConsolidateType() {
        return autoConsolidateType;
    }

    /**
     * Sets the value of the autoConsolidateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoConsolidateType(String value) {
        this.autoConsolidateType = value;
    }

    /**
     * Gets the value of the interimPointOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterimPointOption() {
        return interimPointOption;
    }

    /**
     * Sets the value of the interimPointOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterimPointOption(String value) {
        this.interimPointOption = value;
    }

    /**
     * Gets the value of the rateServiceProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Sets the value of the rateServiceProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Gets the value of the paymentMethodCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Sets the value of the paymentMethodCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Gets the value of the viaSourceLocProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getViaSourceLocProfileGid() {
        return viaSourceLocProfileGid;
    }

    /**
     * Sets the value of the viaSourceLocProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setViaSourceLocProfileGid(GLogXMLLocProfileType value) {
        this.viaSourceLocProfileGid = value;
    }

    /**
     * Gets the value of the viaDestLocProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getViaDestLocProfileGid() {
        return viaDestLocProfileGid;
    }

    /**
     * Sets the value of the viaDestLocProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setViaDestLocProfileGid(GLogXMLLocProfileType value) {
        this.viaDestLocProfileGid = value;
    }

    /**
     * Gets the value of the viaSourceLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getViaSourceLocationRef() {
        return viaSourceLocationRef;
    }

    /**
     * Sets the value of the viaSourceLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setViaSourceLocationRef(GLogXMLLocRefType value) {
        this.viaSourceLocationRef = value;
    }

    /**
     * Gets the value of the viaDestLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getViaDestLocationRef() {
        return viaDestLocationRef;
    }

    /**
     * Sets the value of the viaDestLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setViaDestLocationRef(GLogXMLLocRefType value) {
        this.viaDestLocationRef = value;
    }

    /**
     * Gets the value of the planToOperationalLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanToOperationalLocation() {
        return planToOperationalLocation;
    }

    /**
     * Sets the value of the planToOperationalLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanToOperationalLocation(String value) {
        this.planToOperationalLocation = value;
    }

    /**
     * Gets the value of the isTemporary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemporary() {
        return isTemporary;
    }

    /**
     * Sets the value of the isTemporary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemporary(String value) {
        this.isTemporary = value;
    }

    /**
     * Gets the value of the transArbSearchType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransArbSearchType() {
        return transArbSearchType;
    }

    /**
     * Sets the value of the transArbSearchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransArbSearchType(String value) {
        this.transArbSearchType = value;
    }

    /**
     * Gets the value of the legClassificationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegClassificationGid() {
        return legClassificationGid;
    }

    /**
     * Sets the value of the legClassificationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegClassificationGid(GLogXMLGidType value) {
        this.legClassificationGid = value;
    }

    /**
     * Gets the value of the useConsol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseConsol() {
        return useConsol;
    }

    /**
     * Sets the value of the useConsol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseConsol(String value) {
        this.useConsol = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the legInterimPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the legInterimPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLegInterimPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LegInterimPointType }
     * 
     * 
     */
    public List<LegInterimPointType> getLegInterimPoint() {
        if (legInterimPoint == null) {
            legInterimPoint = new ArrayList<LegInterimPointType>();
        }
        return this.legInterimPoint;
    }

    /**
     * Gets the value of the legSchedule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the legSchedule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLegSchedule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LegType.LegSchedule }
     * 
     * 
     */
    public List<LegType.LegSchedule> getLegSchedule() {
        if (legSchedule == null) {
            legSchedule = new ArrayList<LegType.LegSchedule>();
        }
        return this.legSchedule;
    }

    /**
     * Gets the value of the capacityOverrideGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the capacityOverrideGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCapacityOverrideGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getCapacityOverrideGid() {
        if (capacityOverrideGid == null) {
            capacityOverrideGid = new ArrayList<GLogXMLGidType>();
        }
        return this.capacityOverrideGid;
    }

    /**
     * Gets the value of the legType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegType() {
        return legType;
    }

    /**
     * Sets the value of the legType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegType(String value) {
        this.legType = value;
    }

    /**
     * Gets the value of the routingNetworkGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRoutingNetworkGid() {
        return routingNetworkGid;
    }

    /**
     * Sets the value of the routingNetworkGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRoutingNetworkGid(GLogXMLGidType value) {
        this.routingNetworkGid = value;
    }

    /**
     * Gets the value of the sourceRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getSourceRegionGid() {
        return sourceRegionGid;
    }

    /**
     * Sets the value of the sourceRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setSourceRegionGid(GLogXMLRegionGidType value) {
        this.sourceRegionGid = value;
    }

    /**
     * Gets the value of the destRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestRegionGid() {
        return destRegionGid;
    }

    /**
     * Sets the value of the destRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestRegionGid(GLogXMLRegionGidType value) {
        this.destRegionGid = value;
    }

    /**
     * Gets the value of the legConsolidationGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLegConsolidationGroupGid() {
        return legConsolidationGroupGid;
    }

    /**
     * Sets the value of the legConsolidationGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLegConsolidationGroupGid(GLogXMLGidType value) {
        this.legConsolidationGroupGid = value;
    }

    /**
     * Gets the value of the depotProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDepotProfileGid() {
        return depotProfileGid;
    }

    /**
     * Sets the value of the depotProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDepotProfileGid(GLogXMLGidType value) {
        this.depotProfileGid = value;
    }

    /**
     * Gets the value of the allowCrossConsolidation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowCrossConsolidation() {
        return allowCrossConsolidation;
    }

    /**
     * Sets the value of the allowCrossConsolidation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowCrossConsolidation(String value) {
        this.allowCrossConsolidation = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "repetitionScheduleGid"
    })
    public static class LegSchedule {

        @XmlElement(name = "RepetitionScheduleGid", required = true)
        protected GLogXMLGidType repetitionScheduleGid;

        /**
         * Gets the value of the repetitionScheduleGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getRepetitionScheduleGid() {
            return repetitionScheduleGid;
        }

        /**
         * Sets the value of the repetitionScheduleGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setRepetitionScheduleGid(GLogXMLGidType value) {
            this.repetitionScheduleGid = value;
        }

    }

}

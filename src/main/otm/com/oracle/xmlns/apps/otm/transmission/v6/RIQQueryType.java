
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Rate Inquiry Input Information.
 *             For ShipUnit used within RIQQuery, please only specify information in ShipUnitSpecGid, ShipUnitCount,
 *             WeightVolume, LengthWidthHeight, DeclaredValue, FlexCommodityValue.
 * 
 *             The OutXmlProfileGid is used to apply an outbound profile when the IncludeRefInfo element is Y.
 *          
 * 
 * <p>Java class for RIQQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RIQQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RIQRequestType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SourceAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/&gt;
 *         &lt;element name="SourceLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="DestAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/&gt;
 *         &lt;element name="StopLocation" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="RIQQuerySpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQuerySpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OrderRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AvailableBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AvailableByType" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DeliveryByType" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OrigCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DelivCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPreferred" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UseRIQRoute" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomerDomain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ForceTieredRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EstTieredRateNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IncludeRefInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OutXmlProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IntPreferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CostCategorySetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="UseRIQRouteNR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryOptionDefinition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxPrimaryOptions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxSupportingOptions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsPrimaryOptionCentric" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxOptionsPerRoute" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RoutingConstraints" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PortOfLoadLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                   &lt;element name="PortOfDischargeLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQQueryType", propOrder = {
    "riqRequestType",
    "rateGroupGid",
    "sourceAddress",
    "sourceLocationProfileGid",
    "destAddress",
    "stopLocation",
    "transportModeGid",
    "serviceProviderGid",
    "accessorialCodeGid",
    "specialServiceGid",
    "riqQuerySpecialService",
    "equipmentGroupGid",
    "shipmentRefnum",
    "orderRefnum",
    "availableBy",
    "deliveryBy",
    "flexCommodityQualifierGid",
    "equipmentGroupProfileGid",
    "equipmentInitial",
    "equipmentNumber",
    "origCarrierGid",
    "delivCarrierGid",
    "routeCodeGid",
    "perspective",
    "isPreferred",
    "rateOfferingGid",
    "rateGeoGid",
    "rateServiceGid",
    "useRIQRoute",
    "customerDomain",
    "customerGid",
    "forceTieredRating",
    "estTieredRateNum",
    "incoTermGid",
    "includeRefInfo",
    "outXmlProfileGid",
    "intPreferenceGid",
    "costCategorySetGid",
    "shipUnit",
    "useRIQRouteNR",
    "portOfLoadLocationRef",
    "portOfDisLocationRef",
    "voyageGid",
    "primaryOptionDefinition",
    "maxPrimaryOptions",
    "maxSupportingOptions",
    "planningParamSetGid",
    "isPrimaryOptionCentric",
    "maxOptionsPerRoute",
    "itineraryProfileGid",
    "routingConstraints"
})
public class RIQQueryType {

    @XmlElement(name = "RIQRequestType")
    protected String riqRequestType;
    @XmlElement(name = "RateGroupGid")
    protected GLogXMLGidType rateGroupGid;
    @XmlElement(name = "SourceAddress", required = true)
    protected MileageCorporationType sourceAddress;
    @XmlElement(name = "SourceLocationProfileGid")
    protected GLogXMLLocProfileType sourceLocationProfileGid;
    @XmlElement(name = "DestAddress", required = true)
    protected MileageCorporationType destAddress;
    @XmlElement(name = "StopLocation")
    protected List<RIQQueryType.StopLocation> stopLocation;
    @XmlElement(name = "TransportModeGid")
    protected List<GLogXMLGidType> transportModeGid;
    @XmlElement(name = "ServiceProviderGid")
    protected List<GLogXMLGidType> serviceProviderGid;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "SpecialServiceGid")
    protected List<GLogXMLGidType> specialServiceGid;
    @XmlElement(name = "RIQQuerySpecialService")
    protected List<RIQQuerySpecialServiceType> riqQuerySpecialService;
    @XmlElement(name = "EquipmentGroupGid")
    protected List<GLogXMLGidType> equipmentGroupGid;
    @XmlElement(name = "ShipmentRefnum")
    protected List<ShipmentRefnumType> shipmentRefnum;
    @XmlElement(name = "OrderRefnum")
    protected List<OrderRefnumType> orderRefnum;
    @XmlElement(name = "AvailableBy")
    protected AvailableByType availableBy;
    @XmlElement(name = "DeliveryBy")
    protected DeliveryByType deliveryBy;
    @XmlElement(name = "FlexCommodityQualifierGid")
    protected GLogXMLGidType flexCommodityQualifierGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "OrigCarrierGid")
    protected GLogXMLGidType origCarrierGid;
    @XmlElement(name = "DelivCarrierGid")
    protected GLogXMLGidType delivCarrierGid;
    @XmlElement(name = "RouteCodeGid")
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "IsPreferred")
    protected String isPreferred;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "UseRIQRoute")
    protected String useRIQRoute;
    @XmlElement(name = "CustomerDomain")
    protected String customerDomain;
    @XmlElement(name = "CustomerGid")
    protected GLogXMLGidType customerGid;
    @XmlElement(name = "ForceTieredRating")
    protected String forceTieredRating;
    @XmlElement(name = "EstTieredRateNum")
    protected String estTieredRateNum;
    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "IncludeRefInfo")
    protected String includeRefInfo;
    @XmlElement(name = "OutXmlProfileGid")
    protected GLogXMLGidType outXmlProfileGid;
    @XmlElement(name = "IntPreferenceGid")
    protected GLogXMLGidType intPreferenceGid;
    @XmlElement(name = "CostCategorySetGid")
    protected GLogXMLGidType costCategorySetGid;
    @XmlElement(name = "ShipUnit", required = true)
    protected List<ShipUnitType> shipUnit;
    @XmlElement(name = "UseRIQRouteNR")
    protected String useRIQRouteNR;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "PrimaryOptionDefinition")
    protected String primaryOptionDefinition;
    @XmlElement(name = "MaxPrimaryOptions")
    protected String maxPrimaryOptions;
    @XmlElement(name = "MaxSupportingOptions")
    protected String maxSupportingOptions;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "IsPrimaryOptionCentric")
    protected String isPrimaryOptionCentric;
    @XmlElement(name = "MaxOptionsPerRoute")
    protected String maxOptionsPerRoute;
    @XmlElement(name = "ItineraryProfileGid")
    protected GLogXMLGidType itineraryProfileGid;
    @XmlElement(name = "RoutingConstraints")
    protected RIQQueryType.RoutingConstraints routingConstraints;

    /**
     * Gets the value of the riqRequestType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRIQRequestType() {
        return riqRequestType;
    }

    /**
     * Sets the value of the riqRequestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRIQRequestType(String value) {
        this.riqRequestType = value;
    }

    /**
     * Gets the value of the rateGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGroupGid() {
        return rateGroupGid;
    }

    /**
     * Sets the value of the rateGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGroupGid(GLogXMLGidType value) {
        this.rateGroupGid = value;
    }

    /**
     * Gets the value of the sourceAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Sets the value of the sourceAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setSourceAddress(MileageCorporationType value) {
        this.sourceAddress = value;
    }

    /**
     * Gets the value of the sourceLocationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getSourceLocationProfileGid() {
        return sourceLocationProfileGid;
    }

    /**
     * Sets the value of the sourceLocationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setSourceLocationProfileGid(GLogXMLLocProfileType value) {
        this.sourceLocationProfileGid = value;
    }

    /**
     * Gets the value of the destAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getDestAddress() {
        return destAddress;
    }

    /**
     * Sets the value of the destAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setDestAddress(MileageCorporationType value) {
        this.destAddress = value;
    }

    /**
     * Gets the value of the stopLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the stopLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQQueryType.StopLocation }
     * 
     * 
     */
    public List<RIQQueryType.StopLocation> getStopLocation() {
        if (stopLocation == null) {
            stopLocation = new ArrayList<RIQQueryType.StopLocation>();
        }
        return this.stopLocation;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transportModeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransportModeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getTransportModeGid() {
        if (transportModeGid == null) {
            transportModeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.transportModeGid;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the serviceProviderGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceProviderGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getServiceProviderGid() {
        if (serviceProviderGid == null) {
            serviceProviderGid = new ArrayList<GLogXMLGidType>();
        }
        return this.serviceProviderGid;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the specialServiceGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecialServiceGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getSpecialServiceGid() {
        if (specialServiceGid == null) {
            specialServiceGid = new ArrayList<GLogXMLGidType>();
        }
        return this.specialServiceGid;
    }

    /**
     * Gets the value of the riqQuerySpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the riqQuerySpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQQuerySpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQQuerySpecialServiceType }
     * 
     * 
     */
    public List<RIQQuerySpecialServiceType> getRIQQuerySpecialService() {
        if (riqQuerySpecialService == null) {
            riqQuerySpecialService = new ArrayList<RIQQuerySpecialServiceType>();
        }
        return this.riqQuerySpecialService;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentGroupGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentGroupGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getEquipmentGroupGid() {
        if (equipmentGroupGid == null) {
            equipmentGroupGid = new ArrayList<GLogXMLGidType>();
        }
        return this.equipmentGroupGid;
    }

    /**
     * Gets the value of the shipmentRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentRefnumType }
     * 
     * 
     */
    public List<ShipmentRefnumType> getShipmentRefnum() {
        if (shipmentRefnum == null) {
            shipmentRefnum = new ArrayList<ShipmentRefnumType>();
        }
        return this.shipmentRefnum;
    }

    /**
     * Gets the value of the orderRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orderRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderRefnumType }
     * 
     * 
     */
    public List<OrderRefnumType> getOrderRefnum() {
        if (orderRefnum == null) {
            orderRefnum = new ArrayList<OrderRefnumType>();
        }
        return this.orderRefnum;
    }

    /**
     * Gets the value of the availableBy property.
     * 
     * @return
     *     possible object is
     *     {@link AvailableByType }
     *     
     */
    public AvailableByType getAvailableBy() {
        return availableBy;
    }

    /**
     * Sets the value of the availableBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link AvailableByType }
     *     
     */
    public void setAvailableBy(AvailableByType value) {
        this.availableBy = value;
    }

    /**
     * Gets the value of the deliveryBy property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryByType }
     *     
     */
    public DeliveryByType getDeliveryBy() {
        return deliveryBy;
    }

    /**
     * Sets the value of the deliveryBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryByType }
     *     
     */
    public void setDeliveryBy(DeliveryByType value) {
        this.deliveryBy = value;
    }

    /**
     * Gets the value of the flexCommodityQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityQualifierGid() {
        return flexCommodityQualifierGid;
    }

    /**
     * Sets the value of the flexCommodityQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityQualifierGid(GLogXMLGidType value) {
        this.flexCommodityQualifierGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the equipmentInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Sets the value of the equipmentInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Gets the value of the equipmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Sets the value of the equipmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the origCarrierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigCarrierGid() {
        return origCarrierGid;
    }

    /**
     * Sets the value of the origCarrierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigCarrierGid(GLogXMLGidType value) {
        this.origCarrierGid = value;
    }

    /**
     * Gets the value of the delivCarrierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivCarrierGid() {
        return delivCarrierGid;
    }

    /**
     * Sets the value of the delivCarrierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivCarrierGid(GLogXMLGidType value) {
        this.delivCarrierGid = value;
    }

    /**
     * Gets the value of the routeCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Sets the value of the routeCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the isPreferred property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreferred() {
        return isPreferred;
    }

    /**
     * Sets the value of the isPreferred property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreferred(String value) {
        this.isPreferred = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the rateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Sets the value of the rateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Gets the value of the rateServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Sets the value of the rateServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Gets the value of the useRIQRoute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseRIQRoute() {
        return useRIQRoute;
    }

    /**
     * Sets the value of the useRIQRoute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseRIQRoute(String value) {
        this.useRIQRoute = value;
    }

    /**
     * Gets the value of the customerDomain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerDomain() {
        return customerDomain;
    }

    /**
     * Sets the value of the customerDomain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerDomain(String value) {
        this.customerDomain = value;
    }

    /**
     * Gets the value of the customerGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCustomerGid() {
        return customerGid;
    }

    /**
     * Sets the value of the customerGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCustomerGid(GLogXMLGidType value) {
        this.customerGid = value;
    }

    /**
     * Gets the value of the forceTieredRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForceTieredRating() {
        return forceTieredRating;
    }

    /**
     * Sets the value of the forceTieredRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForceTieredRating(String value) {
        this.forceTieredRating = value;
    }

    /**
     * Gets the value of the estTieredRateNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstTieredRateNum() {
        return estTieredRateNum;
    }

    /**
     * Sets the value of the estTieredRateNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstTieredRateNum(String value) {
        this.estTieredRateNum = value;
    }

    /**
     * Gets the value of the incoTermGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Sets the value of the incoTermGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Gets the value of the includeRefInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncludeRefInfo() {
        return includeRefInfo;
    }

    /**
     * Sets the value of the includeRefInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncludeRefInfo(String value) {
        this.includeRefInfo = value;
    }

    /**
     * Gets the value of the outXmlProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutXmlProfileGid() {
        return outXmlProfileGid;
    }

    /**
     * Sets the value of the outXmlProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutXmlProfileGid(GLogXMLGidType value) {
        this.outXmlProfileGid = value;
    }

    /**
     * Gets the value of the intPreferenceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntPreferenceGid() {
        return intPreferenceGid;
    }

    /**
     * Sets the value of the intPreferenceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntPreferenceGid(GLogXMLGidType value) {
        this.intPreferenceGid = value;
    }

    /**
     * Gets the value of the costCategorySetGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostCategorySetGid() {
        return costCategorySetGid;
    }

    /**
     * Sets the value of the costCategorySetGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostCategorySetGid(GLogXMLGidType value) {
        this.costCategorySetGid = value;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitType }
     * 
     * 
     */
    public List<ShipUnitType> getShipUnit() {
        if (shipUnit == null) {
            shipUnit = new ArrayList<ShipUnitType>();
        }
        return this.shipUnit;
    }

    /**
     * Gets the value of the useRIQRouteNR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseRIQRouteNR() {
        return useRIQRouteNR;
    }

    /**
     * Sets the value of the useRIQRouteNR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseRIQRouteNR(String value) {
        this.useRIQRouteNR = value;
    }

    /**
     * Gets the value of the portOfLoadLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Sets the value of the portOfLoadLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Gets the value of the portOfDisLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Sets the value of the portOfDisLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Gets the value of the voyageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Sets the value of the voyageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Gets the value of the primaryOptionDefinition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryOptionDefinition() {
        return primaryOptionDefinition;
    }

    /**
     * Sets the value of the primaryOptionDefinition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryOptionDefinition(String value) {
        this.primaryOptionDefinition = value;
    }

    /**
     * Gets the value of the maxPrimaryOptions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxPrimaryOptions() {
        return maxPrimaryOptions;
    }

    /**
     * Sets the value of the maxPrimaryOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxPrimaryOptions(String value) {
        this.maxPrimaryOptions = value;
    }

    /**
     * Gets the value of the maxSupportingOptions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxSupportingOptions() {
        return maxSupportingOptions;
    }

    /**
     * Sets the value of the maxSupportingOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxSupportingOptions(String value) {
        this.maxSupportingOptions = value;
    }

    /**
     * Gets the value of the planningParamSetGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Sets the value of the planningParamSetGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Gets the value of the isPrimaryOptionCentric property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimaryOptionCentric() {
        return isPrimaryOptionCentric;
    }

    /**
     * Sets the value of the isPrimaryOptionCentric property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimaryOptionCentric(String value) {
        this.isPrimaryOptionCentric = value;
    }

    /**
     * Gets the value of the maxOptionsPerRoute property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxOptionsPerRoute() {
        return maxOptionsPerRoute;
    }

    /**
     * Sets the value of the maxOptionsPerRoute property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxOptionsPerRoute(String value) {
        this.maxOptionsPerRoute = value;
    }

    /**
     * Gets the value of the itineraryProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryProfileGid() {
        return itineraryProfileGid;
    }

    /**
     * Sets the value of the itineraryProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryProfileGid(GLogXMLGidType value) {
        this.itineraryProfileGid = value;
    }

    /**
     * Gets the value of the routingConstraints property.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryType.RoutingConstraints }
     *     
     */
    public RIQQueryType.RoutingConstraints getRoutingConstraints() {
        return routingConstraints;
    }

    /**
     * Sets the value of the routingConstraints property.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryType.RoutingConstraints }
     *     
     */
    public void setRoutingConstraints(RIQQueryType.RoutingConstraints value) {
        this.routingConstraints = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PortOfLoadLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *         &lt;element name="PortOfDischargeLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "portOfLoadLocation",
        "portOfDischargeLocation"
    })
    public static class RoutingConstraints {

        @XmlElement(name = "PortOfLoadLocation")
        protected List<LocationRefType> portOfLoadLocation;
        @XmlElement(name = "PortOfDischargeLocation")
        protected List<LocationRefType> portOfDischargeLocation;

        /**
         * Gets the value of the portOfLoadLocation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the portOfLoadLocation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPortOfLoadLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LocationRefType }
         * 
         * 
         */
        public List<LocationRefType> getPortOfLoadLocation() {
            if (portOfLoadLocation == null) {
                portOfLoadLocation = new ArrayList<LocationRefType>();
            }
            return this.portOfLoadLocation;
        }

        /**
         * Gets the value of the portOfDischargeLocation property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the portOfDischargeLocation property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPortOfDischargeLocation().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link LocationRefType }
         * 
         * 
         */
        public List<LocationRefType> getPortOfDischargeLocation() {
            if (portOfDischargeLocation == null) {
                portOfDischargeLocation = new ArrayList<LocationRefType>();
            }
            return this.portOfDischargeLocation;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationGid"
    })
    public static class StopLocation {

        @XmlElement(name = "LocationGid")
        protected GLogXMLGidType locationGid;

        /**
         * Gets the value of the locationGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Sets the value of the locationGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

    }

}

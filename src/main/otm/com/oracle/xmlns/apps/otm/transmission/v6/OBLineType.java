
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * This is used to send out the TransOrderLine information. It is initiated via
 *             notification.
 *             This element is supported on the outbound only.
 *          
 * 
 * <p>Java class for OBLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OBLineType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="TransOrderLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderLineType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OBLineType", propOrder = {
    "sendReason",
    "transOrderLine"
})
public class OBLineType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransOrderLine", required = true)
    protected TransOrderLineType transOrderLine;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the transOrderLine property.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderLineType }
     *     
     */
    public TransOrderLineType getTransOrderLine() {
        return transOrderLine;
    }

    /**
     * Sets the value of the transOrderLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderLineType }
     *     
     */
    public void setTransOrderLine(TransOrderLineType value) {
        this.transOrderLine = value;
    }

}

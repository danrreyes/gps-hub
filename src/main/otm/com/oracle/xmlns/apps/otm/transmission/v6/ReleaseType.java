
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Release is a structure for describing an order release. A release respresents a shippable subset of a
 *             TransOrder.
 *             It corresponds to the TransOrderLines which share common transportation requirements.
 *             It includes a common source/destination, time window, and multiple release lines which
 *             share the source/destination and time window.
 *          
 * 
 * <p>Java class for ReleaseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="IntCommand" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntCommandType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseHeaderType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipToLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipToLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TimeWindow" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowType" minOccurs="0"/&gt;
 *         &lt;element name="DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLine" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLineType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PlanFromLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="PlanFromLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="PlanFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PlanToLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="PlanToLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="PlanToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PriLegSourceLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PriLegSourceLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfLoadLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfDisLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="PriLegDestLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PriLegDestLocOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="BulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SellBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PlanPartitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BestDirectBuyCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="BestDirectBuyRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BestDirectSellCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="BestDirectSellRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BuyGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SellGeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="TotalNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="TotalPackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalPackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="REquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}REquipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CommercialInvoice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseShipmentInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LatestEstDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *                   &lt;element name="EarliestEstPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ReleaseAllocationInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ReleaseAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TransOrder" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderType" minOccurs="0"/&gt;
 *         &lt;element name="OrStop" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrStopType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OrderMovement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderMovementType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AllocationGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseType", propOrder = {
    "sendReason",
    "intSavedQuery",
    "releaseGid",
    "transactionCode",
    "replaceChildren",
    "intCommand",
    "transOrderGid",
    "releaseHeader",
    "shipFromLocationRef",
    "shipFromLocOverrideRef",
    "shipFromLoadPoint",
    "shipToLocationRef",
    "shipToLocOverrideRef",
    "shipToUnloadPoint",
    "timeWindow",
    "declaredValue",
    "releaseLine",
    "shipUnit",
    "releaseTypeGid",
    "planFromLocationGid",
    "planFromLocOverrideRef",
    "planFromLoadPoint",
    "planToLocationGid",
    "planToLocOverrideRef",
    "planToUnloadPoint",
    "priLegSourceLocationRef",
    "priLegSourceLocOverrideRef",
    "portOfLoadLocationRef",
    "portOfLoadLocOverrideRef",
    "portOfDisLocationRef",
    "portOfDisLocOverrideRef",
    "priLegDestLocationRef",
    "priLegDestLocOverrideRef",
    "bulkPlanGid",
    "sellBulkPlanGid",
    "planPartitionGid",
    "bestDirectBuyCost",
    "bestDirectBuyRateOfferingGid",
    "bestDirectSellCost",
    "bestDirectSellRateOfferingGid",
    "buyGeneralLedgerGid",
    "sellGeneralLedgerGid",
    "totalWeightVolume",
    "totalNetWeightVolume",
    "totalPackagedItemSpecCount",
    "totalPackagedItemCount",
    "releaseRefnum",
    "releaseStatus",
    "remark",
    "rEquipment",
    "involvedParty",
    "commercialInvoice",
    "text",
    "releaseService",
    "releaseShipmentInfo",
    "releaseAllocationInfo",
    "transOrder",
    "orStop",
    "orderMovement",
    "allocationGroupGid"
})
public class ReleaseType
    extends OTMTransactionInOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ReleaseGid")
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "IntCommand")
    protected List<IntCommandType> intCommand;
    @XmlElement(name = "TransOrderGid")
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "ReleaseHeader")
    protected ReleaseHeaderType releaseHeader;
    @XmlElement(name = "ShipFromLocationRef")
    protected GLogXMLLocRefType shipFromLocationRef;
    @XmlElement(name = "ShipFromLocOverrideRef")
    protected GLogXMLLocOverrideRefType shipFromLocOverrideRef;
    @XmlElement(name = "ShipFromLoadPoint")
    protected String shipFromLoadPoint;
    @XmlElement(name = "ShipToLocationRef")
    protected GLogXMLLocRefType shipToLocationRef;
    @XmlElement(name = "ShipToLocOverrideRef")
    protected GLogXMLLocOverrideRefType shipToLocOverrideRef;
    @XmlElement(name = "ShipToUnloadPoint")
    protected String shipToUnloadPoint;
    @XmlElement(name = "TimeWindow")
    protected TimeWindowType timeWindow;
    @XmlElement(name = "DeclaredValue")
    protected GLogXMLFinancialAmountType declaredValue;
    @XmlElement(name = "ReleaseLine")
    protected List<ReleaseLineType> releaseLine;
    @XmlElement(name = "ShipUnit")
    protected List<ShipUnitType> shipUnit;
    @XmlElement(name = "ReleaseTypeGid")
    protected GLogXMLGidType releaseTypeGid;
    @XmlElement(name = "PlanFromLocationGid")
    protected GLogXMLLocGidType planFromLocationGid;
    @XmlElement(name = "PlanFromLocOverrideRef")
    protected GLogXMLLocOverrideRefType planFromLocOverrideRef;
    @XmlElement(name = "PlanFromLoadPoint")
    protected String planFromLoadPoint;
    @XmlElement(name = "PlanToLocationGid")
    protected GLogXMLLocGidType planToLocationGid;
    @XmlElement(name = "PlanToLocOverrideRef")
    protected GLogXMLLocOverrideRefType planToLocOverrideRef;
    @XmlElement(name = "PlanToUnloadPoint")
    protected String planToUnloadPoint;
    @XmlElement(name = "PriLegSourceLocationRef")
    protected GLogXMLLocRefType priLegSourceLocationRef;
    @XmlElement(name = "PriLegSourceLocOverrideRef")
    protected GLogXMLLocOverrideRefType priLegSourceLocOverrideRef;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfLoadLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfLoadLocOverrideRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "PortOfDisLocOverrideRef")
    protected GLogXMLLocOverrideRefType portOfDisLocOverrideRef;
    @XmlElement(name = "PriLegDestLocationRef")
    protected GLogXMLLocRefType priLegDestLocationRef;
    @XmlElement(name = "PriLegDestLocOverrideRef")
    protected GLogXMLLocOverrideRefType priLegDestLocOverrideRef;
    @XmlElement(name = "BulkPlanGid")
    protected GLogXMLGidType bulkPlanGid;
    @XmlElement(name = "SellBulkPlanGid")
    protected GLogXMLGidType sellBulkPlanGid;
    @XmlElement(name = "PlanPartitionGid")
    protected GLogXMLGidType planPartitionGid;
    @XmlElement(name = "BestDirectBuyCost")
    protected GLogXMLFinancialAmountType bestDirectBuyCost;
    @XmlElement(name = "BestDirectBuyRateOfferingGid")
    protected GLogXMLGidType bestDirectBuyRateOfferingGid;
    @XmlElement(name = "BestDirectSellCost")
    protected GLogXMLFinancialAmountType bestDirectSellCost;
    @XmlElement(name = "BestDirectSellRateOfferingGid")
    protected GLogXMLGidType bestDirectSellRateOfferingGid;
    @XmlElement(name = "BuyGeneralLedgerGid")
    protected GLogXMLGidType buyGeneralLedgerGid;
    @XmlElement(name = "SellGeneralLedgerGid")
    protected GLogXMLGidType sellGeneralLedgerGid;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TotalNetWeightVolume")
    protected WeightVolumeType totalNetWeightVolume;
    @XmlElement(name = "TotalPackagedItemSpecCount")
    protected String totalPackagedItemSpecCount;
    @XmlElement(name = "TotalPackagedItemCount")
    protected String totalPackagedItemCount;
    @XmlElement(name = "ReleaseRefnum")
    protected List<ReleaseRefnumType> releaseRefnum;
    @XmlElement(name = "ReleaseStatus")
    protected List<StatusType> releaseStatus;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "REquipment")
    protected List<REquipmentType> rEquipment;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "CommercialInvoice")
    protected List<CommercialInvoiceType> commercialInvoice;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "ReleaseService")
    protected List<ReleaseServiceType> releaseService;
    @XmlElement(name = "ReleaseShipmentInfo")
    protected ReleaseType.ReleaseShipmentInfo releaseShipmentInfo;
    @XmlElement(name = "ReleaseAllocationInfo")
    protected ReleaseType.ReleaseAllocationInfo releaseAllocationInfo;
    @XmlElement(name = "TransOrder")
    protected TransOrderType transOrder;
    @XmlElement(name = "OrStop")
    protected List<OrStopType> orStop;
    @XmlElement(name = "OrderMovement")
    protected List<OrderMovementType> orderMovement;
    @XmlElement(name = "AllocationGroupGid")
    protected GLogXMLGidType allocationGroupGid;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the releaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Sets the value of the releaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the intCommand property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the intCommand property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntCommand().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntCommandType }
     * 
     * 
     */
    public List<IntCommandType> getIntCommand() {
        if (intCommand == null) {
            intCommand = new ArrayList<IntCommandType>();
        }
        return this.intCommand;
    }

    /**
     * Gets the value of the transOrderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Sets the value of the transOrderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Gets the value of the releaseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseHeaderType }
     *     
     */
    public ReleaseHeaderType getReleaseHeader() {
        return releaseHeader;
    }

    /**
     * Sets the value of the releaseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseHeaderType }
     *     
     */
    public void setReleaseHeader(ReleaseHeaderType value) {
        this.releaseHeader = value;
    }

    /**
     * Gets the value of the shipFromLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipFromLocationRef() {
        return shipFromLocationRef;
    }

    /**
     * Sets the value of the shipFromLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipFromLocationRef(GLogXMLLocRefType value) {
        this.shipFromLocationRef = value;
    }

    /**
     * Gets the value of the shipFromLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getShipFromLocOverrideRef() {
        return shipFromLocOverrideRef;
    }

    /**
     * Sets the value of the shipFromLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setShipFromLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.shipFromLocOverrideRef = value;
    }

    /**
     * Gets the value of the shipFromLoadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipFromLoadPoint() {
        return shipFromLoadPoint;
    }

    /**
     * Sets the value of the shipFromLoadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipFromLoadPoint(String value) {
        this.shipFromLoadPoint = value;
    }

    /**
     * Gets the value of the shipToLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipToLocationRef() {
        return shipToLocationRef;
    }

    /**
     * Sets the value of the shipToLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipToLocationRef(GLogXMLLocRefType value) {
        this.shipToLocationRef = value;
    }

    /**
     * Gets the value of the shipToLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getShipToLocOverrideRef() {
        return shipToLocOverrideRef;
    }

    /**
     * Sets the value of the shipToLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setShipToLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.shipToLocOverrideRef = value;
    }

    /**
     * Gets the value of the shipToUnloadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToUnloadPoint() {
        return shipToUnloadPoint;
    }

    /**
     * Sets the value of the shipToUnloadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToUnloadPoint(String value) {
        this.shipToUnloadPoint = value;
    }

    /**
     * Gets the value of the timeWindow property.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowType }
     *     
     */
    public TimeWindowType getTimeWindow() {
        return timeWindow;
    }

    /**
     * Sets the value of the timeWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowType }
     *     
     */
    public void setTimeWindow(TimeWindowType value) {
        this.timeWindow = value;
    }

    /**
     * Gets the value of the declaredValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDeclaredValue() {
        return declaredValue;
    }

    /**
     * Sets the value of the declaredValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDeclaredValue(GLogXMLFinancialAmountType value) {
        this.declaredValue = value;
    }

    /**
     * Gets the value of the releaseLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseLineType }
     * 
     * 
     */
    public List<ReleaseLineType> getReleaseLine() {
        if (releaseLine == null) {
            releaseLine = new ArrayList<ReleaseLineType>();
        }
        return this.releaseLine;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitType }
     * 
     * 
     */
    public List<ShipUnitType> getShipUnit() {
        if (shipUnit == null) {
            shipUnit = new ArrayList<ShipUnitType>();
        }
        return this.shipUnit;
    }

    /**
     * Gets the value of the releaseTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseTypeGid() {
        return releaseTypeGid;
    }

    /**
     * Sets the value of the releaseTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseTypeGid(GLogXMLGidType value) {
        this.releaseTypeGid = value;
    }

    /**
     * Gets the value of the planFromLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanFromLocationGid() {
        return planFromLocationGid;
    }

    /**
     * Sets the value of the planFromLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanFromLocationGid(GLogXMLLocGidType value) {
        this.planFromLocationGid = value;
    }

    /**
     * Gets the value of the planFromLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPlanFromLocOverrideRef() {
        return planFromLocOverrideRef;
    }

    /**
     * Sets the value of the planFromLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPlanFromLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.planFromLocOverrideRef = value;
    }

    /**
     * Gets the value of the planFromLoadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFromLoadPoint() {
        return planFromLoadPoint;
    }

    /**
     * Sets the value of the planFromLoadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFromLoadPoint(String value) {
        this.planFromLoadPoint = value;
    }

    /**
     * Gets the value of the planToLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanToLocationGid() {
        return planToLocationGid;
    }

    /**
     * Sets the value of the planToLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanToLocationGid(GLogXMLLocGidType value) {
        this.planToLocationGid = value;
    }

    /**
     * Gets the value of the planToLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPlanToLocOverrideRef() {
        return planToLocOverrideRef;
    }

    /**
     * Sets the value of the planToLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPlanToLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.planToLocOverrideRef = value;
    }

    /**
     * Gets the value of the planToUnloadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanToUnloadPoint() {
        return planToUnloadPoint;
    }

    /**
     * Sets the value of the planToUnloadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanToUnloadPoint(String value) {
        this.planToUnloadPoint = value;
    }

    /**
     * Gets the value of the priLegSourceLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPriLegSourceLocationRef() {
        return priLegSourceLocationRef;
    }

    /**
     * Sets the value of the priLegSourceLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPriLegSourceLocationRef(GLogXMLLocRefType value) {
        this.priLegSourceLocationRef = value;
    }

    /**
     * Gets the value of the priLegSourceLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPriLegSourceLocOverrideRef() {
        return priLegSourceLocOverrideRef;
    }

    /**
     * Sets the value of the priLegSourceLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPriLegSourceLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.priLegSourceLocOverrideRef = value;
    }

    /**
     * Gets the value of the portOfLoadLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Sets the value of the portOfLoadLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Gets the value of the portOfLoadLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfLoadLocOverrideRef() {
        return portOfLoadLocOverrideRef;
    }

    /**
     * Sets the value of the portOfLoadLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfLoadLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfLoadLocOverrideRef = value;
    }

    /**
     * Gets the value of the portOfDisLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Sets the value of the portOfDisLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Gets the value of the portOfDisLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPortOfDisLocOverrideRef() {
        return portOfDisLocOverrideRef;
    }

    /**
     * Sets the value of the portOfDisLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPortOfDisLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.portOfDisLocOverrideRef = value;
    }

    /**
     * Gets the value of the priLegDestLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPriLegDestLocationRef() {
        return priLegDestLocationRef;
    }

    /**
     * Sets the value of the priLegDestLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPriLegDestLocationRef(GLogXMLLocRefType value) {
        this.priLegDestLocationRef = value;
    }

    /**
     * Gets the value of the priLegDestLocOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public GLogXMLLocOverrideRefType getPriLegDestLocOverrideRef() {
        return priLegDestLocOverrideRef;
    }

    /**
     * Sets the value of the priLegDestLocOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocOverrideRefType }
     *     
     */
    public void setPriLegDestLocOverrideRef(GLogXMLLocOverrideRefType value) {
        this.priLegDestLocOverrideRef = value;
    }

    /**
     * Gets the value of the bulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkPlanGid() {
        return bulkPlanGid;
    }

    /**
     * Sets the value of the bulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkPlanGid(GLogXMLGidType value) {
        this.bulkPlanGid = value;
    }

    /**
     * Gets the value of the sellBulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellBulkPlanGid() {
        return sellBulkPlanGid;
    }

    /**
     * Sets the value of the sellBulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellBulkPlanGid(GLogXMLGidType value) {
        this.sellBulkPlanGid = value;
    }

    /**
     * Gets the value of the planPartitionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanPartitionGid() {
        return planPartitionGid;
    }

    /**
     * Sets the value of the planPartitionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanPartitionGid(GLogXMLGidType value) {
        this.planPartitionGid = value;
    }

    /**
     * Gets the value of the bestDirectBuyCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getBestDirectBuyCost() {
        return bestDirectBuyCost;
    }

    /**
     * Sets the value of the bestDirectBuyCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setBestDirectBuyCost(GLogXMLFinancialAmountType value) {
        this.bestDirectBuyCost = value;
    }

    /**
     * Gets the value of the bestDirectBuyRateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBestDirectBuyRateOfferingGid() {
        return bestDirectBuyRateOfferingGid;
    }

    /**
     * Sets the value of the bestDirectBuyRateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBestDirectBuyRateOfferingGid(GLogXMLGidType value) {
        this.bestDirectBuyRateOfferingGid = value;
    }

    /**
     * Gets the value of the bestDirectSellCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getBestDirectSellCost() {
        return bestDirectSellCost;
    }

    /**
     * Sets the value of the bestDirectSellCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setBestDirectSellCost(GLogXMLFinancialAmountType value) {
        this.bestDirectSellCost = value;
    }

    /**
     * Gets the value of the bestDirectSellRateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBestDirectSellRateOfferingGid() {
        return bestDirectSellRateOfferingGid;
    }

    /**
     * Sets the value of the bestDirectSellRateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBestDirectSellRateOfferingGid(GLogXMLGidType value) {
        this.bestDirectSellRateOfferingGid = value;
    }

    /**
     * Gets the value of the buyGeneralLedgerGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBuyGeneralLedgerGid() {
        return buyGeneralLedgerGid;
    }

    /**
     * Sets the value of the buyGeneralLedgerGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBuyGeneralLedgerGid(GLogXMLGidType value) {
        this.buyGeneralLedgerGid = value;
    }

    /**
     * Gets the value of the sellGeneralLedgerGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellGeneralLedgerGid() {
        return sellGeneralLedgerGid;
    }

    /**
     * Sets the value of the sellGeneralLedgerGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellGeneralLedgerGid(GLogXMLGidType value) {
        this.sellGeneralLedgerGid = value;
    }

    /**
     * Gets the value of the totalWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Sets the value of the totalWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Gets the value of the totalNetWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getTotalNetWeightVolume() {
        return totalNetWeightVolume;
    }

    /**
     * Sets the value of the totalNetWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setTotalNetWeightVolume(WeightVolumeType value) {
        this.totalNetWeightVolume = value;
    }

    /**
     * Gets the value of the totalPackagedItemSpecCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemSpecCount() {
        return totalPackagedItemSpecCount;
    }

    /**
     * Sets the value of the totalPackagedItemSpecCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemSpecCount(String value) {
        this.totalPackagedItemSpecCount = value;
    }

    /**
     * Gets the value of the totalPackagedItemCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalPackagedItemCount() {
        return totalPackagedItemCount;
    }

    /**
     * Sets the value of the totalPackagedItemCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalPackagedItemCount(String value) {
        this.totalPackagedItemCount = value;
    }

    /**
     * Gets the value of the releaseRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseRefnumType }
     * 
     * 
     */
    public List<ReleaseRefnumType> getReleaseRefnum() {
        if (releaseRefnum == null) {
            releaseRefnum = new ArrayList<ReleaseRefnumType>();
        }
        return this.releaseRefnum;
    }

    /**
     * Gets the value of the releaseStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getReleaseStatus() {
        if (releaseStatus == null) {
            releaseStatus = new ArrayList<StatusType>();
        }
        return this.releaseStatus;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the rEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getREquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link REquipmentType }
     * 
     * 
     */
    public List<REquipmentType> getREquipment() {
        if (rEquipment == null) {
            rEquipment = new ArrayList<REquipmentType>();
        }
        return this.rEquipment;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the commercialInvoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the commercialInvoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommercialInvoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommercialInvoiceType }
     * 
     * 
     */
    public List<CommercialInvoiceType> getCommercialInvoice() {
        if (commercialInvoice == null) {
            commercialInvoice = new ArrayList<CommercialInvoiceType>();
        }
        return this.commercialInvoice;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Gets the value of the releaseService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseServiceType }
     * 
     * 
     */
    public List<ReleaseServiceType> getReleaseService() {
        if (releaseService == null) {
            releaseService = new ArrayList<ReleaseServiceType>();
        }
        return this.releaseService;
    }

    /**
     * Gets the value of the releaseShipmentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseType.ReleaseShipmentInfo }
     *     
     */
    public ReleaseType.ReleaseShipmentInfo getReleaseShipmentInfo() {
        return releaseShipmentInfo;
    }

    /**
     * Sets the value of the releaseShipmentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseType.ReleaseShipmentInfo }
     *     
     */
    public void setReleaseShipmentInfo(ReleaseType.ReleaseShipmentInfo value) {
        this.releaseShipmentInfo = value;
    }

    /**
     * Gets the value of the releaseAllocationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseType.ReleaseAllocationInfo }
     *     
     */
    public ReleaseType.ReleaseAllocationInfo getReleaseAllocationInfo() {
        return releaseAllocationInfo;
    }

    /**
     * Sets the value of the releaseAllocationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseType.ReleaseAllocationInfo }
     *     
     */
    public void setReleaseAllocationInfo(ReleaseType.ReleaseAllocationInfo value) {
        this.releaseAllocationInfo = value;
    }

    /**
     * Gets the value of the transOrder property.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderType }
     *     
     */
    public TransOrderType getTransOrder() {
        return transOrder;
    }

    /**
     * Sets the value of the transOrder property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderType }
     *     
     */
    public void setTransOrder(TransOrderType value) {
        this.transOrder = value;
    }

    /**
     * Gets the value of the orStop property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orStop property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrStop().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrStopType }
     * 
     * 
     */
    public List<OrStopType> getOrStop() {
        if (orStop == null) {
            orStop = new ArrayList<OrStopType>();
        }
        return this.orStop;
    }

    /**
     * Gets the value of the orderMovement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orderMovement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderMovement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderMovementType }
     * 
     * 
     */
    public List<OrderMovementType> getOrderMovement() {
        if (orderMovement == null) {
            orderMovement = new ArrayList<OrderMovementType>();
        }
        return this.orderMovement;
    }

    /**
     * Gets the value of the allocationGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocationGroupGid() {
        return allocationGroupGid;
    }

    /**
     * Sets the value of the allocationGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocationGroupGid(GLogXMLGidType value) {
        this.allocationGroupGid = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ReleaseAllocByType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "releaseAllocByType"
    })
    public static class ReleaseAllocationInfo {

        @XmlElement(name = "ReleaseAllocByType")
        protected List<ReleaseAllocType> releaseAllocByType;

        /**
         * Gets the value of the releaseAllocByType property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the releaseAllocByType property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getReleaseAllocByType().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ReleaseAllocType }
         * 
         * 
         */
        public List<ReleaseAllocType> getReleaseAllocByType() {
            if (releaseAllocByType == null) {
                releaseAllocByType = new ArrayList<ReleaseAllocType>();
            }
            return this.releaseAllocByType;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LatestEstDeliveryDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
     *         &lt;element name="EarliestEstPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "latestEstDeliveryDt",
        "earliestEstPickupDt"
    })
    public static class ReleaseShipmentInfo {

        @XmlElement(name = "LatestEstDeliveryDt", required = true)
        protected GLogDateTimeType latestEstDeliveryDt;
        @XmlElement(name = "EarliestEstPickupDt", required = true)
        protected GLogDateTimeType earliestEstPickupDt;

        /**
         * Gets the value of the latestEstDeliveryDt property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getLatestEstDeliveryDt() {
            return latestEstDeliveryDt;
        }

        /**
         * Sets the value of the latestEstDeliveryDt property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setLatestEstDeliveryDt(GLogDateTimeType value) {
            this.latestEstDeliveryDt = value;
        }

        /**
         * Gets the value of the earliestEstPickupDt property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getEarliestEstPickupDt() {
            return earliestEstPickupDt;
        }

        /**
         * Sets the value of the earliestEstPickupDt property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setEarliestEstPickupDt(GLogDateTimeType value) {
            this.earliestEstPickupDt = value;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LocationActivityTimeDefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationActivityTimeDefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="ActivityTimeDefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ActivityTimeDef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ActivityTimeDefType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationActivityTimeDefType", propOrder = {
    "activityTimeDefGid",
    "activityTimeDef"
})
public class LocationActivityTimeDefType {

    @XmlElement(name = "ActivityTimeDefGid")
    protected GLogXMLGidType activityTimeDefGid;
    @XmlElement(name = "ActivityTimeDef")
    protected ActivityTimeDefType activityTimeDef;

    /**
     * Gets the value of the activityTimeDefGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityTimeDefGid() {
        return activityTimeDefGid;
    }

    /**
     * Sets the value of the activityTimeDefGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityTimeDefGid(GLogXMLGidType value) {
        this.activityTimeDefGid = value;
    }

    /**
     * Gets the value of the activityTimeDef property.
     * 
     * @return
     *     possible object is
     *     {@link ActivityTimeDefType }
     *     
     */
    public ActivityTimeDefType getActivityTimeDef() {
        return activityTimeDef;
    }

    /**
     * Sets the value of the activityTimeDef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivityTimeDefType }
     *     
     */
    public void setActivityTimeDef(ActivityTimeDefType value) {
        this.activityTimeDef = value;
    }

}

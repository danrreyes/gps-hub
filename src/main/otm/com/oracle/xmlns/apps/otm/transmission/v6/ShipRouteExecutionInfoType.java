
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the route execution information.
 *             The OnRouteExecution element is outbound only.
 *             When the AssignShipmentToLeg element is Y, the RouteTemplateGid, RouteInstanceGid, RouteInstanceLegGid, and LegNum are used to determine the appropriate route instance
 *             leg to assign the shipment.
 *          
 * 
 * <p>Java class for ShipRouteExecutionInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipRouteExecutionInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AssignShipmentToLeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RouteTemplateGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteInstanceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteInstanceLegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LegNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipRouteExecutionInfoType", propOrder = {
    "assignShipmentToLeg",
    "routeTemplateGid",
    "routeInstanceGid",
    "routeInstanceLegGid",
    "legNum"
})
public class ShipRouteExecutionInfoType {

    @XmlElement(name = "AssignShipmentToLeg")
    protected String assignShipmentToLeg;
    @XmlElement(name = "RouteTemplateGid")
    protected GLogXMLGidType routeTemplateGid;
    @XmlElement(name = "RouteInstanceGid")
    protected GLogXMLGidType routeInstanceGid;
    @XmlElement(name = "RouteInstanceLegGid")
    protected GLogXMLGidType routeInstanceLegGid;
    @XmlElement(name = "LegNum")
    protected String legNum;

    /**
     * Gets the value of the assignShipmentToLeg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssignShipmentToLeg() {
        return assignShipmentToLeg;
    }

    /**
     * Sets the value of the assignShipmentToLeg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssignShipmentToLeg(String value) {
        this.assignShipmentToLeg = value;
    }

    /**
     * Gets the value of the routeTemplateGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteTemplateGid() {
        return routeTemplateGid;
    }

    /**
     * Sets the value of the routeTemplateGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteTemplateGid(GLogXMLGidType value) {
        this.routeTemplateGid = value;
    }

    /**
     * Gets the value of the routeInstanceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteInstanceGid() {
        return routeInstanceGid;
    }

    /**
     * Sets the value of the routeInstanceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteInstanceGid(GLogXMLGidType value) {
        this.routeInstanceGid = value;
    }

    /**
     * Gets the value of the routeInstanceLegGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteInstanceLegGid() {
        return routeInstanceLegGid;
    }

    /**
     * Sets the value of the routeInstanceLegGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteInstanceLegGid(GLogXMLGidType value) {
        this.routeInstanceLegGid = value;
    }

    /**
     * Gets the value of the legNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegNum() {
        return legNum;
    }

    /**
     * Sets the value of the legNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegNum(String value) {
        this.legNum = value;
    }

}

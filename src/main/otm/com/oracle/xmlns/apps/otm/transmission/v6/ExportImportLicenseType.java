
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Used to specify export and import licenses.
 *          
 * 
 * <p>Java class for ExportImportLicenseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ExportImportLicenseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExportLicense" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExportLicenseType" minOccurs="0"/&gt;
 *         &lt;element name="ImportLicense" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ImportLicenseType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExportImportLicenseType", propOrder = {
    "exportLicense",
    "importLicense"
})
public class ExportImportLicenseType {

    @XmlElement(name = "ExportLicense")
    protected ExportLicenseType exportLicense;
    @XmlElement(name = "ImportLicense")
    protected ImportLicenseType importLicense;

    /**
     * Gets the value of the exportLicense property.
     * 
     * @return
     *     possible object is
     *     {@link ExportLicenseType }
     *     
     */
    public ExportLicenseType getExportLicense() {
        return exportLicense;
    }

    /**
     * Sets the value of the exportLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExportLicenseType }
     *     
     */
    public void setExportLicense(ExportLicenseType value) {
        this.exportLicense = value;
    }

    /**
     * Gets the value of the importLicense property.
     * 
     * @return
     *     possible object is
     *     {@link ImportLicenseType }
     *     
     */
    public ImportLicenseType getImportLicense() {
        return importLicense;
    }

    /**
     * Sets the value of the importLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportLicenseType }
     *     
     */
    public void setImportLicense(ImportLicenseType value) {
        this.importLicense = value;
    }

}

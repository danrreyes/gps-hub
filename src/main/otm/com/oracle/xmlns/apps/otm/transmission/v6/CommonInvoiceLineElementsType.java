
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * CommonInvoiceLineElement is used to Used to convey commodity, tariff, remark, freight rate, and quantity information.
 * 
 * <p>Java class for CommonInvoiceLineElementsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommonInvoiceLineElementsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Commodity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommodityType" minOccurs="0"/&gt;
 *         &lt;element name="FP_Tariff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FPTariffType" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FreightRate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FreightRateType" minOccurs="0"/&gt;
 *         &lt;element name="BilledRated" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BilledRatedType" minOccurs="0"/&gt;
 *         &lt;element name="VatCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="GeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LineItemVat" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LineItemVatType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommonInvoiceLineElementsType", propOrder = {
    "commodity",
    "fpTariff",
    "remark",
    "freightRate",
    "billedRated",
    "vatCodeGid",
    "adjustmentReasonGid",
    "generalLedgerGid",
    "lineItemVat",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class CommonInvoiceLineElementsType {

    @XmlElement(name = "Commodity")
    protected CommodityType commodity;
    @XmlElement(name = "FP_Tariff")
    protected FPTariffType fpTariff;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "FreightRate")
    protected FreightRateType freightRate;
    @XmlElement(name = "BilledRated")
    protected BilledRatedType billedRated;
    @XmlElement(name = "VatCodeGid")
    protected GLogXMLGidType vatCodeGid;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "GeneralLedgerGid")
    protected GLogXMLGidType generalLedgerGid;
    @XmlElement(name = "LineItemVat")
    protected List<LineItemVatType> lineItemVat;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Gets the value of the commodity property.
     * 
     * @return
     *     possible object is
     *     {@link CommodityType }
     *     
     */
    public CommodityType getCommodity() {
        return commodity;
    }

    /**
     * Sets the value of the commodity property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommodityType }
     *     
     */
    public void setCommodity(CommodityType value) {
        this.commodity = value;
    }

    /**
     * Gets the value of the fpTariff property.
     * 
     * @return
     *     possible object is
     *     {@link FPTariffType }
     *     
     */
    public FPTariffType getFPTariff() {
        return fpTariff;
    }

    /**
     * Sets the value of the fpTariff property.
     * 
     * @param value
     *     allowed object is
     *     {@link FPTariffType }
     *     
     */
    public void setFPTariff(FPTariffType value) {
        this.fpTariff = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the freightRate property.
     * 
     * @return
     *     possible object is
     *     {@link FreightRateType }
     *     
     */
    public FreightRateType getFreightRate() {
        return freightRate;
    }

    /**
     * Sets the value of the freightRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link FreightRateType }
     *     
     */
    public void setFreightRate(FreightRateType value) {
        this.freightRate = value;
    }

    /**
     * Gets the value of the billedRated property.
     * 
     * @return
     *     possible object is
     *     {@link BilledRatedType }
     *     
     */
    public BilledRatedType getBilledRated() {
        return billedRated;
    }

    /**
     * Sets the value of the billedRated property.
     * 
     * @param value
     *     allowed object is
     *     {@link BilledRatedType }
     *     
     */
    public void setBilledRated(BilledRatedType value) {
        this.billedRated = value;
    }

    /**
     * Gets the value of the vatCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVatCodeGid() {
        return vatCodeGid;
    }

    /**
     * Sets the value of the vatCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVatCodeGid(GLogXMLGidType value) {
        this.vatCodeGid = value;
    }

    /**
     * Gets the value of the adjustmentReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Sets the value of the adjustmentReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Gets the value of the generalLedgerGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGeneralLedgerGid() {
        return generalLedgerGid;
    }

    /**
     * Sets the value of the generalLedgerGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGeneralLedgerGid(GLogXMLGidType value) {
        this.generalLedgerGid = value;
    }

    /**
     * Gets the value of the lineItemVat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemVat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemVat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemVatType }
     * 
     * 
     */
    public List<LineItemVatType> getLineItemVat() {
        if (lineItemVat == null) {
            lineItemVat = new ArrayList<LineItemVatType>();
        }
        return this.lineItemVat;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the flexFieldCurrencies property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Sets the value of the flexFieldCurrencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}

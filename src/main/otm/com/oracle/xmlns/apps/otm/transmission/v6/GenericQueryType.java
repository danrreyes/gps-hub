
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * The GenericQuery interface is used to query or generate the xml for an existing outbound
 *             interface (e.g. PlannedShipment).
 *             The element name indicates the interface to generate which will be one or more children of the
 *             GenericQueryReply element.
 *             The SqlQuery or IntSavedQuery is used to define the query that searches for the gids of the objects to be
 *             generated. The
 *             query should be limited to returning just the gid columns.
 *             The OutXMLProfileGid is used to apply an outbound profile when generating the xml.
 *          
 * 
 * <p>Java class for GenericQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ElementName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="SqlQuery" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="OutXmlProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IntPreferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericQueryType", propOrder = {
    "elementName",
    "sqlQuery",
    "intSavedQuery",
    "outXmlProfileGid",
    "intPreferenceGid"
})
public class GenericQueryType {

    @XmlElement(name = "ElementName", required = true)
    protected String elementName;
    @XmlElement(name = "SqlQuery")
    protected String sqlQuery;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "OutXmlProfileGid")
    protected GLogXMLGidType outXmlProfileGid;
    @XmlElement(name = "IntPreferenceGid")
    protected GLogXMLGidType intPreferenceGid;

    /**
     * Gets the value of the elementName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementName() {
        return elementName;
    }

    /**
     * Sets the value of the elementName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementName(String value) {
        this.elementName = value;
    }

    /**
     * Gets the value of the sqlQuery property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSqlQuery() {
        return sqlQuery;
    }

    /**
     * Sets the value of the sqlQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSqlQuery(String value) {
        this.sqlQuery = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the outXmlProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutXmlProfileGid() {
        return outXmlProfileGid;
    }

    /**
     * Sets the value of the outXmlProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutXmlProfileGid(GLogXMLGidType value) {
        this.outXmlProfileGid = value;
    }

    /**
     * Gets the value of the intPreferenceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntPreferenceGid() {
        return intPreferenceGid;
    }

    /**
     * Sets the value of the intPreferenceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntPreferenceGid(GLogXMLGidType value) {
        this.intPreferenceGid = value;
    }

}

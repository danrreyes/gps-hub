
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegionalHandlingTimeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegionalHandlingTimeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FixedHandlingTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType"/&gt;
 *         &lt;element name="SourceRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="DestRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegionalHandlingTimeType", propOrder = {
    "fixedHandlingTime",
    "sourceRegionGid",
    "destRegionGid"
})
public class RegionalHandlingTimeType {

    @XmlElement(name = "FixedHandlingTime", required = true)
    protected GLogXMLDurationType fixedHandlingTime;
    @XmlElement(name = "SourceRegionGid")
    protected GLogXMLRegionGidType sourceRegionGid;
    @XmlElement(name = "DestRegionGid")
    protected GLogXMLRegionGidType destRegionGid;

    /**
     * Gets the value of the fixedHandlingTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getFixedHandlingTime() {
        return fixedHandlingTime;
    }

    /**
     * Sets the value of the fixedHandlingTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setFixedHandlingTime(GLogXMLDurationType value) {
        this.fixedHandlingTime = value;
    }

    /**
     * Gets the value of the sourceRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getSourceRegionGid() {
        return sourceRegionGid;
    }

    /**
     * Sets the value of the sourceRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setSourceRegionGid(GLogXMLRegionGidType value) {
        this.sourceRegionGid = value;
    }

    /**
     * Gets the value of the destRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestRegionGid() {
        return destRegionGid;
    }

    /**
     * Sets the value of the destRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestRegionGid(GLogXMLRegionGidType value) {
        this.destRegionGid = value;
    }

}

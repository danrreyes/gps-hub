
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipmentRefnum is an alternate method for identifying a shipment. It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for ShipmentRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipmentRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipmentRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentRefnumType", propOrder = {
    "shipmentRefnumQualifierGid",
    "shipmentRefnumValue"
})
public class ShipmentRefnumType {

    @XmlElement(name = "ShipmentRefnumQualifierGid", required = true)
    protected GLogXMLGidType shipmentRefnumQualifierGid;
    @XmlElement(name = "ShipmentRefnumValue", required = true)
    protected String shipmentRefnumValue;

    /**
     * Gets the value of the shipmentRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentRefnumQualifierGid() {
        return shipmentRefnumQualifierGid;
    }

    /**
     * Sets the value of the shipmentRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentRefnumQualifierGid(GLogXMLGidType value) {
        this.shipmentRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the shipmentRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentRefnumValue() {
        return shipmentRefnumValue;
    }

    /**
     * Sets the value of the shipmentRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentRefnumValue(String value) {
        this.shipmentRefnumValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             RailStopOff is an element of RailDetail used to specify rail specific stop information on an invoice.
 *          
 * 
 * <p>Java class for RailStopOffType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RailStopOffType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="StopReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType"/&gt;
 *           &lt;element name="LocationRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefnumType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="StopRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailStopOffType", propOrder = {
    "stopSequence",
    "stopReasonGid",
    "locationRef",
    "locationRefnum",
    "stopRefnum"
})
public class RailStopOffType {

    @XmlElement(name = "StopSequence", required = true)
    protected String stopSequence;
    @XmlElement(name = "StopReasonGid")
    protected GLogXMLGidType stopReasonGid;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "LocationRefnum")
    protected LocationRefnumType locationRefnum;
    @XmlElement(name = "StopRefnum")
    protected List<StopRefnumType> stopRefnum;

    /**
     * Gets the value of the stopSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Sets the value of the stopSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Gets the value of the stopReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStopReasonGid() {
        return stopReasonGid;
    }

    /**
     * Sets the value of the stopReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStopReasonGid(GLogXMLGidType value) {
        this.stopReasonGid = value;
    }

    /**
     * Gets the value of the locationRef property.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Sets the value of the locationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Gets the value of the locationRefnum property.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefnumType }
     *     
     */
    public LocationRefnumType getLocationRefnum() {
        return locationRefnum;
    }

    /**
     * Sets the value of the locationRefnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefnumType }
     *     
     */
    public void setLocationRefnum(LocationRefnumType value) {
        this.locationRefnum = value;
    }

    /**
     * Gets the value of the stopRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the stopRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStopRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopRefnumType }
     * 
     * 
     */
    public List<StopRefnumType> getStopRefnum() {
        if (stopRefnum == null) {
            stopRefnum = new ArrayList<StopRefnumType>();
        }
        return this.stopRefnum;
    }

}

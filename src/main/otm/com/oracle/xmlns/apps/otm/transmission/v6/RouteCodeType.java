
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * A Route Code is used to specify a route where the path/route that the shipment will
 *             take is pre-defined and is part of the lane level offering. For example, a rail
 *             carrier may have several options to move freight from Philadelphia to Chicago.
 *             The various options correspond to various train schedules available as well as the
 *             junctions and carriers that might be involved in the route.
 *          
 * 
 * <p>Java class for RouteCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RouteCodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RouteCodeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OriginStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType" minOccurs="0"/&gt;
 *         &lt;element name="DestinationStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType" minOccurs="0"/&gt;
 *         &lt;element name="RailReturnLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="RailReturnRouteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailReturnRouteGidType" minOccurs="0"/&gt;
 *         &lt;element name="BorderCrossingLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteCodeDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteCodeType", propOrder = {
    "routeCodeGid",
    "routeCodeDesc",
    "originStation",
    "destinationStation",
    "railReturnLocationGid",
    "railReturnRouteGid",
    "borderCrossingLocationGid",
    "routeCodeDetail"
})
public class RouteCodeType {

    @XmlElement(name = "RouteCodeGid", required = true)
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "RouteCodeDesc")
    protected String routeCodeDesc;
    @XmlElement(name = "OriginStation")
    protected GLogXMLStationType originStation;
    @XmlElement(name = "DestinationStation")
    protected GLogXMLStationType destinationStation;
    @XmlElement(name = "RailReturnLocationGid")
    protected GLogXMLLocGidType railReturnLocationGid;
    @XmlElement(name = "RailReturnRouteGid")
    protected RailReturnRouteGidType railReturnRouteGid;
    @XmlElement(name = "BorderCrossingLocationGid")
    protected GLogXMLLocGidType borderCrossingLocationGid;
    @XmlElement(name = "RouteCodeDetail")
    protected List<RouteCodeDetailType> routeCodeDetail;

    /**
     * Gets the value of the routeCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Sets the value of the routeCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Gets the value of the routeCodeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRouteCodeDesc() {
        return routeCodeDesc;
    }

    /**
     * Sets the value of the routeCodeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRouteCodeDesc(String value) {
        this.routeCodeDesc = value;
    }

    /**
     * Gets the value of the originStation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getOriginStation() {
        return originStation;
    }

    /**
     * Sets the value of the originStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setOriginStation(GLogXMLStationType value) {
        this.originStation = value;
    }

    /**
     * Gets the value of the destinationStation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getDestinationStation() {
        return destinationStation;
    }

    /**
     * Sets the value of the destinationStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setDestinationStation(GLogXMLStationType value) {
        this.destinationStation = value;
    }

    /**
     * Gets the value of the railReturnLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getRailReturnLocationGid() {
        return railReturnLocationGid;
    }

    /**
     * Sets the value of the railReturnLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setRailReturnLocationGid(GLogXMLLocGidType value) {
        this.railReturnLocationGid = value;
    }

    /**
     * Gets the value of the railReturnRouteGid property.
     * 
     * @return
     *     possible object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public RailReturnRouteGidType getRailReturnRouteGid() {
        return railReturnRouteGid;
    }

    /**
     * Sets the value of the railReturnRouteGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public void setRailReturnRouteGid(RailReturnRouteGidType value) {
        this.railReturnRouteGid = value;
    }

    /**
     * Gets the value of the borderCrossingLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getBorderCrossingLocationGid() {
        return borderCrossingLocationGid;
    }

    /**
     * Sets the value of the borderCrossingLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setBorderCrossingLocationGid(GLogXMLLocGidType value) {
        this.borderCrossingLocationGid = value;
    }

    /**
     * Gets the value of the routeCodeDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the routeCodeDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteCodeDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteCodeDetailType }
     * 
     * 
     */
    public List<RouteCodeDetailType> getRouteCodeDetail() {
        if (routeCodeDetail == null) {
            routeCodeDetail = new ArrayList<RouteCodeDetailType>();
        }
        return this.routeCodeDetail;
    }

}

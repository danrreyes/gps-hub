
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * SSContact is a structure containing various contact elements, such as Contact Function Code, Contact Name, Remarks.
 * 
 * <p>Java class for SSContactType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSContactType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SSContactFunctionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SSContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSContactType", propOrder = {
    "ssContactFunctionCode",
    "ssContactName"
})
public class SSContactType {

    @XmlElement(name = "SSContactFunctionCode")
    protected String ssContactFunctionCode;
    @XmlElement(name = "SSContactName")
    protected String ssContactName;

    /**
     * Gets the value of the ssContactFunctionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSContactFunctionCode() {
        return ssContactFunctionCode;
    }

    /**
     * Sets the value of the ssContactFunctionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSContactFunctionCode(String value) {
        this.ssContactFunctionCode = value;
    }

    /**
     * Gets the value of the ssContactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSSContactName() {
        return ssContactName;
    }

    /**
     * Sets the value of the ssContactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSSContactName(String value) {
        this.ssContactName = value;
    }

}

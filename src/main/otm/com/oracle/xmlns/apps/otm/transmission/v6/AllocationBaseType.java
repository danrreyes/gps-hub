
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             When the AllocationBase is for a Voucher with a Parent Invoice, all of the AllocationBase for the Child Invoices
 *             will be contained in the ChildAllocationBases element. The AllocatedCost in the parent AllocationBase will correspond
 *             to the Voucher Cost.
 *          
 * 
 * <p>Java class for AllocationBaseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AllocationBaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="AllocSeqNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AllocTypeQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="AllocDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/&gt;
 *         &lt;element name="AllocatedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType" minOccurs="0"/&gt;
 *           &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Invoice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvoiceType" minOccurs="0"/&gt;
 *         &lt;element name="ParentInvoice" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ParentInvoiceType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="VoucherGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;element name="Voucher" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VoucherType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="AllocReleaseDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocReleaseDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AllocReleaseLineDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocReleaseLineDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="AllocTransOrderLineDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocTransOrderLineDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ChildAllocationBases" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="AllocationBase" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocationBaseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AllocationBaseType", propOrder = {
    "sendReason",
    "allocSeqNo",
    "allocTypeQualGid",
    "allocDate",
    "exchangeRateInfo",
    "allocatedCost",
    "shipment",
    "shipmentGid",
    "invoice",
    "parentInvoice",
    "voucherGid",
    "voucher",
    "allocReleaseDetail",
    "allocReleaseLineDetail",
    "allocTransOrderLineDetail",
    "childAllocationBases"
})
public class AllocationBaseType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "AllocSeqNo", required = true)
    protected String allocSeqNo;
    @XmlElement(name = "AllocTypeQualGid", required = true)
    protected GLogXMLGidType allocTypeQualGid;
    @XmlElement(name = "AllocDate", required = true)
    protected GLogDateTimeType allocDate;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "AllocatedCost")
    protected GLogXMLFinancialAmountType allocatedCost;
    @XmlElement(name = "Shipment")
    protected ShipmentType shipment;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "Invoice")
    protected InvoiceType invoice;
    @XmlElement(name = "ParentInvoice")
    protected ParentInvoiceType parentInvoice;
    @XmlElement(name = "VoucherGid")
    protected GLogXMLGidType voucherGid;
    @XmlElement(name = "Voucher")
    protected VoucherType voucher;
    @XmlElement(name = "AllocReleaseDetail")
    protected List<AllocReleaseDetailType> allocReleaseDetail;
    @XmlElement(name = "AllocReleaseLineDetail")
    protected List<AllocReleaseLineDetailType> allocReleaseLineDetail;
    @XmlElement(name = "AllocTransOrderLineDetail")
    protected List<AllocTransOrderLineDetailType> allocTransOrderLineDetail;
    @XmlElement(name = "ChildAllocationBases")
    protected AllocationBaseType.ChildAllocationBases childAllocationBases;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the allocSeqNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllocSeqNo() {
        return allocSeqNo;
    }

    /**
     * Sets the value of the allocSeqNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllocSeqNo(String value) {
        this.allocSeqNo = value;
    }

    /**
     * Gets the value of the allocTypeQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocTypeQualGid() {
        return allocTypeQualGid;
    }

    /**
     * Sets the value of the allocTypeQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocTypeQualGid(GLogXMLGidType value) {
        this.allocTypeQualGid = value;
    }

    /**
     * Gets the value of the allocDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAllocDate() {
        return allocDate;
    }

    /**
     * Sets the value of the allocDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAllocDate(GLogDateTimeType value) {
        this.allocDate = value;
    }

    /**
     * Gets the value of the exchangeRateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Sets the value of the exchangeRateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Gets the value of the allocatedCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAllocatedCost() {
        return allocatedCost;
    }

    /**
     * Sets the value of the allocatedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAllocatedCost(GLogXMLFinancialAmountType value) {
        this.allocatedCost = value;
    }

    /**
     * Gets the value of the shipment property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentType }
     *     
     */
    public ShipmentType getShipment() {
        return shipment;
    }

    /**
     * Sets the value of the shipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentType }
     *     
     */
    public void setShipment(ShipmentType value) {
        this.shipment = value;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the invoice property.
     * 
     * @return
     *     possible object is
     *     {@link InvoiceType }
     *     
     */
    public InvoiceType getInvoice() {
        return invoice;
    }

    /**
     * Sets the value of the invoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvoiceType }
     *     
     */
    public void setInvoice(InvoiceType value) {
        this.invoice = value;
    }

    /**
     * Gets the value of the parentInvoice property.
     * 
     * @return
     *     possible object is
     *     {@link ParentInvoiceType }
     *     
     */
    public ParentInvoiceType getParentInvoice() {
        return parentInvoice;
    }

    /**
     * Sets the value of the parentInvoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParentInvoiceType }
     *     
     */
    public void setParentInvoice(ParentInvoiceType value) {
        this.parentInvoice = value;
    }

    /**
     * Gets the value of the voucherGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoucherGid() {
        return voucherGid;
    }

    /**
     * Sets the value of the voucherGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoucherGid(GLogXMLGidType value) {
        this.voucherGid = value;
    }

    /**
     * Gets the value of the voucher property.
     * 
     * @return
     *     possible object is
     *     {@link VoucherType }
     *     
     */
    public VoucherType getVoucher() {
        return voucher;
    }

    /**
     * Sets the value of the voucher property.
     * 
     * @param value
     *     allowed object is
     *     {@link VoucherType }
     *     
     */
    public void setVoucher(VoucherType value) {
        this.voucher = value;
    }

    /**
     * Gets the value of the allocReleaseDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the allocReleaseDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocReleaseDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllocReleaseDetailType }
     * 
     * 
     */
    public List<AllocReleaseDetailType> getAllocReleaseDetail() {
        if (allocReleaseDetail == null) {
            allocReleaseDetail = new ArrayList<AllocReleaseDetailType>();
        }
        return this.allocReleaseDetail;
    }

    /**
     * Gets the value of the allocReleaseLineDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the allocReleaseLineDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocReleaseLineDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllocReleaseLineDetailType }
     * 
     * 
     */
    public List<AllocReleaseLineDetailType> getAllocReleaseLineDetail() {
        if (allocReleaseLineDetail == null) {
            allocReleaseLineDetail = new ArrayList<AllocReleaseLineDetailType>();
        }
        return this.allocReleaseLineDetail;
    }

    /**
     * Gets the value of the allocTransOrderLineDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the allocTransOrderLineDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllocTransOrderLineDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllocTransOrderLineDetailType }
     * 
     * 
     */
    public List<AllocTransOrderLineDetailType> getAllocTransOrderLineDetail() {
        if (allocTransOrderLineDetail == null) {
            allocTransOrderLineDetail = new ArrayList<AllocTransOrderLineDetailType>();
        }
        return this.allocTransOrderLineDetail;
    }

    /**
     * Gets the value of the childAllocationBases property.
     * 
     * @return
     *     possible object is
     *     {@link AllocationBaseType.ChildAllocationBases }
     *     
     */
    public AllocationBaseType.ChildAllocationBases getChildAllocationBases() {
        return childAllocationBases;
    }

    /**
     * Sets the value of the childAllocationBases property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllocationBaseType.ChildAllocationBases }
     *     
     */
    public void setChildAllocationBases(AllocationBaseType.ChildAllocationBases value) {
        this.childAllocationBases = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="AllocationBase" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocationBaseType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "allocationBase"
    })
    public static class ChildAllocationBases {

        @XmlElement(name = "AllocationBase")
        protected List<AllocationBaseType> allocationBase;

        /**
         * Gets the value of the allocationBase property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the allocationBase property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAllocationBase().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AllocationBaseType }
         * 
         * 
         */
        public List<AllocationBaseType> getAllocationBase() {
            if (allocationBase == null) {
                allocationBase = new ArrayList<AllocationBaseType>();
            }
            return this.allocationBase;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLFlexQuantityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLFlexQuantityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FlexQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexQuantityType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLFlexQuantityType", propOrder = {
    "flexQuantity"
})
public class GLogXMLFlexQuantityType {

    @XmlElement(name = "FlexQuantity", required = true)
    protected FlexQuantityType flexQuantity;

    /**
     * Gets the value of the flexQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link FlexQuantityType }
     *     
     */
    public FlexQuantityType getFlexQuantity() {
        return flexQuantity;
    }

    /**
     * Sets the value of the flexQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexQuantityType }
     *     
     */
    public void setFlexQuantity(FlexQuantityType value) {
        this.flexQuantity = value;
    }

}

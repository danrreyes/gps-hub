
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * RIQResult: Rate Inquiry Results.
 * 
 * <p>Java class for RIQResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RIQResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DeliveryByType"/&gt;
 *         &lt;element name="PickupBy" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PickupByType" minOccurs="0"/&gt;
 *         &lt;element name="TransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType"/&gt;
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="TotalActualCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightedCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/&gt;
 *         &lt;element name="CostDetails" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostDetailsType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsTimeFeasible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsOptimalResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DimWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Distance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DistanceType" minOccurs="0"/&gt;
 *         &lt;element name="OrigCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DelivCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RIQResultInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQResultInfoType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQResultType", propOrder = {
    "sourceLocationGid",
    "serviceProviderGid",
    "rateOfferingGid",
    "rateGeoGid",
    "rateServiceGid",
    "transportModeGid",
    "equipmentGroupProfileGid",
    "equipmentGroupGid",
    "deliveryBy",
    "pickupBy",
    "transitTime",
    "cost",
    "totalActualCostPerUOM",
    "totalWeightedCostPerUOM",
    "costDetails",
    "isTimeFeasible",
    "isOptimalResult",
    "dimWeight",
    "perspective",
    "distance",
    "origCarrierGid",
    "delivCarrierGid",
    "routeCodeGid",
    "riqResultInfo"
})
public class RIQResultType {

    @XmlElement(name = "SourceLocationGid", required = true)
    protected GLogXMLGidType sourceLocationGid;
    @XmlElement(name = "ServiceProviderGid", required = true)
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "RateOfferingGid", required = true)
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid", required = true)
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "TransportModeGid", required = true)
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "DeliveryBy", required = true)
    protected DeliveryByType deliveryBy;
    @XmlElement(name = "PickupBy")
    protected PickupByType pickupBy;
    @XmlElement(name = "TransitTime", required = true)
    protected GLogXMLDurationType transitTime;
    @XmlElement(name = "Cost", required = true)
    protected GLogXMLFinancialAmountType cost;
    @XmlElement(name = "TotalActualCostPerUOM")
    protected GLogXMLCostPerUOM totalActualCostPerUOM;
    @XmlElement(name = "TotalWeightedCostPerUOM")
    protected GLogXMLCostPerUOM totalWeightedCostPerUOM;
    @XmlElement(name = "CostDetails")
    protected List<CostDetailsType> costDetails;
    @XmlElement(name = "IsTimeFeasible")
    protected String isTimeFeasible;
    @XmlElement(name = "IsOptimalResult")
    protected String isOptimalResult;
    @XmlElement(name = "DimWeight")
    protected GLogXMLWeightType dimWeight;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "Distance")
    protected DistanceType distance;
    @XmlElement(name = "OrigCarrierGid")
    protected GLogXMLGidType origCarrierGid;
    @XmlElement(name = "DelivCarrierGid")
    protected GLogXMLGidType delivCarrierGid;
    @XmlElement(name = "RouteCodeGid")
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "RIQResultInfo")
    protected RIQResultInfoType riqResultInfo;

    /**
     * Gets the value of the sourceLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSourceLocationGid() {
        return sourceLocationGid;
    }

    /**
     * Sets the value of the sourceLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSourceLocationGid(GLogXMLGidType value) {
        this.sourceLocationGid = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the rateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Sets the value of the rateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Gets the value of the rateServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Sets the value of the rateServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Sets the value of the transportModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Sets the value of the equipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Gets the value of the deliveryBy property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryByType }
     *     
     */
    public DeliveryByType getDeliveryBy() {
        return deliveryBy;
    }

    /**
     * Sets the value of the deliveryBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryByType }
     *     
     */
    public void setDeliveryBy(DeliveryByType value) {
        this.deliveryBy = value;
    }

    /**
     * Gets the value of the pickupBy property.
     * 
     * @return
     *     possible object is
     *     {@link PickupByType }
     *     
     */
    public PickupByType getPickupBy() {
        return pickupBy;
    }

    /**
     * Sets the value of the pickupBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PickupByType }
     *     
     */
    public void setPickupBy(PickupByType value) {
        this.pickupBy = value;
    }

    /**
     * Gets the value of the transitTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTransitTime() {
        return transitTime;
    }

    /**
     * Sets the value of the transitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTransitTime(GLogXMLDurationType value) {
        this.transitTime = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

    /**
     * Gets the value of the totalActualCostPerUOM property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalActualCostPerUOM() {
        return totalActualCostPerUOM;
    }

    /**
     * Sets the value of the totalActualCostPerUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalActualCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalActualCostPerUOM = value;
    }

    /**
     * Gets the value of the totalWeightedCostPerUOM property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalWeightedCostPerUOM() {
        return totalWeightedCostPerUOM;
    }

    /**
     * Sets the value of the totalWeightedCostPerUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalWeightedCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalWeightedCostPerUOM = value;
    }

    /**
     * Gets the value of the costDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the costDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostDetailsType }
     * 
     * 
     */
    public List<CostDetailsType> getCostDetails() {
        if (costDetails == null) {
            costDetails = new ArrayList<CostDetailsType>();
        }
        return this.costDetails;
    }

    /**
     * Gets the value of the isTimeFeasible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTimeFeasible() {
        return isTimeFeasible;
    }

    /**
     * Sets the value of the isTimeFeasible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTimeFeasible(String value) {
        this.isTimeFeasible = value;
    }

    /**
     * Gets the value of the isOptimalResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOptimalResult() {
        return isOptimalResult;
    }

    /**
     * Sets the value of the isOptimalResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOptimalResult(String value) {
        this.isOptimalResult = value;
    }

    /**
     * Gets the value of the dimWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getDimWeight() {
        return dimWeight;
    }

    /**
     * Sets the value of the dimWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setDimWeight(GLogXMLWeightType value) {
        this.dimWeight = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDistance(DistanceType value) {
        this.distance = value;
    }

    /**
     * Gets the value of the origCarrierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigCarrierGid() {
        return origCarrierGid;
    }

    /**
     * Sets the value of the origCarrierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigCarrierGid(GLogXMLGidType value) {
        this.origCarrierGid = value;
    }

    /**
     * Gets the value of the delivCarrierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivCarrierGid() {
        return delivCarrierGid;
    }

    /**
     * Sets the value of the delivCarrierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivCarrierGid(GLogXMLGidType value) {
        this.delivCarrierGid = value;
    }

    /**
     * Gets the value of the routeCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Sets the value of the routeCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Gets the value of the riqResultInfo property.
     * 
     * @return
     *     possible object is
     *     {@link RIQResultInfoType }
     *     
     */
    public RIQResultInfoType getRIQResultInfo() {
        return riqResultInfo;
    }

    /**
     * Sets the value of the riqResultInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQResultInfoType }
     *     
     */
    public void setRIQResultInfo(RIQResultInfoType value) {
        this.riqResultInfo = value;
    }

}

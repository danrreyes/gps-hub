
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the load configuration set up
 * 
 * <p>Java class for LoadConfigSetupType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoadConfigSetupType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LoadConfigSetupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="StackingIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxStackingHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLHeightType" minOccurs="0"/&gt;
 *         &lt;element name="MaxStackingLayers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsStackableAbove" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsStackableBelow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxUnsupportedAreaPercent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PatternConfigProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StackingRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LoadConfigSetupOrientation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LoadConfigSetupOrientationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadConfigSetupType", propOrder = {
    "loadConfigSetupGid",
    "stackingIndex",
    "maxStackingHeight",
    "maxStackingLayers",
    "isStackableAbove",
    "isStackableBelow",
    "maxUnsupportedAreaPercent",
    "patternConfigProfileGid",
    "stackingRule",
    "loadConfigSetupOrientation"
})
public class LoadConfigSetupType {

    @XmlElement(name = "LoadConfigSetupGid", required = true)
    protected GLogXMLGidType loadConfigSetupGid;
    @XmlElement(name = "StackingIndex")
    protected String stackingIndex;
    @XmlElement(name = "MaxStackingHeight")
    protected GLogXMLHeightType maxStackingHeight;
    @XmlElement(name = "MaxStackingLayers")
    protected String maxStackingLayers;
    @XmlElement(name = "IsStackableAbove")
    protected String isStackableAbove;
    @XmlElement(name = "IsStackableBelow")
    protected String isStackableBelow;
    @XmlElement(name = "MaxUnsupportedAreaPercent")
    protected String maxUnsupportedAreaPercent;
    @XmlElement(name = "PatternConfigProfileGid")
    protected GLogXMLGidType patternConfigProfileGid;
    @XmlElement(name = "StackingRule")
    protected String stackingRule;
    @XmlElement(name = "LoadConfigSetupOrientation")
    protected List<LoadConfigSetupOrientationType> loadConfigSetupOrientation;

    /**
     * Gets the value of the loadConfigSetupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfigSetupGid() {
        return loadConfigSetupGid;
    }

    /**
     * Sets the value of the loadConfigSetupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfigSetupGid(GLogXMLGidType value) {
        this.loadConfigSetupGid = value;
    }

    /**
     * Gets the value of the stackingIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackingIndex() {
        return stackingIndex;
    }

    /**
     * Sets the value of the stackingIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackingIndex(String value) {
        this.stackingIndex = value;
    }

    /**
     * Gets the value of the maxStackingHeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public GLogXMLHeightType getMaxStackingHeight() {
        return maxStackingHeight;
    }

    /**
     * Sets the value of the maxStackingHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public void setMaxStackingHeight(GLogXMLHeightType value) {
        this.maxStackingHeight = value;
    }

    /**
     * Gets the value of the maxStackingLayers property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxStackingLayers() {
        return maxStackingLayers;
    }

    /**
     * Sets the value of the maxStackingLayers property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxStackingLayers(String value) {
        this.maxStackingLayers = value;
    }

    /**
     * Gets the value of the isStackableAbove property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsStackableAbove() {
        return isStackableAbove;
    }

    /**
     * Sets the value of the isStackableAbove property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsStackableAbove(String value) {
        this.isStackableAbove = value;
    }

    /**
     * Gets the value of the isStackableBelow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsStackableBelow() {
        return isStackableBelow;
    }

    /**
     * Sets the value of the isStackableBelow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsStackableBelow(String value) {
        this.isStackableBelow = value;
    }

    /**
     * Gets the value of the maxUnsupportedAreaPercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxUnsupportedAreaPercent() {
        return maxUnsupportedAreaPercent;
    }

    /**
     * Sets the value of the maxUnsupportedAreaPercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxUnsupportedAreaPercent(String value) {
        this.maxUnsupportedAreaPercent = value;
    }

    /**
     * Gets the value of the patternConfigProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPatternConfigProfileGid() {
        return patternConfigProfileGid;
    }

    /**
     * Sets the value of the patternConfigProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPatternConfigProfileGid(GLogXMLGidType value) {
        this.patternConfigProfileGid = value;
    }

    /**
     * Gets the value of the stackingRule property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackingRule() {
        return stackingRule;
    }

    /**
     * Sets the value of the stackingRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackingRule(String value) {
        this.stackingRule = value;
    }

    /**
     * Gets the value of the loadConfigSetupOrientation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the loadConfigSetupOrientation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLoadConfigSetupOrientation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LoadConfigSetupOrientationType }
     * 
     * 
     */
    public List<LoadConfigSetupOrientationType> getLoadConfigSetupOrientation() {
        if (loadConfigSetupOrientation == null) {
            loadConfigSetupOrientation = new ArrayList<LoadConfigSetupOrientationType>();
        }
        return this.loadConfigSetupOrientation;
    }

}

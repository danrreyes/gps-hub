
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RateOfferingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RateOfferingType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="RATE_OFFERING_ROW" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="RATE_OFFERING_XID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="RATE_OFFERING_TYPE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="RATE_OFFERING_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SERVPROV_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="TRANSPORT_MODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="USER_CLASSIFICATION1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USER_CLASSIFICATION2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USER_CLASSIFICATION3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USER_CLASSIFICATION4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_VERSION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MAX_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_DIST_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_DIST_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_SHIP_UNIT_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_SHIP_UNIT_WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_SHIP_UNIT_WEIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_DISTANCE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_CLASSIFICATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="STOPS_INCLUDED_IN_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SHIPPER_MIN_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_SHIP_UNIT_SPEC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXCHANGE_RATE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="COMMODITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="FAK_RATE_AS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="FAK_FLEX_COMMODITY_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RM_ABSOLUTE_MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RM_ABSOLUTE_MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RM_ABSOLUTE_MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="WEIGHT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MULTI_STOP_COST_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="VOLUME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PERSPECTIVE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CORPORATION_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CAPACITY_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CM_MAX_NUM_SHIPMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CM_IS_SAME_EQUIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CM_PREV_SHIPMENT_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CM_IS_PERCENT_OF_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_REFNUM_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_REFNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_PUB_AUTHORITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_REG_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_ISSUING_CARRIER_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_REFNUM_SUFFIX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_SUPPLEMENT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TARIFF_EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IS_ACTIVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="HANDLES_UNKNOWN_SHIPPER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USES_TIME_BASED_RATES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_DISTANCE_CONSTRNT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_DISTANCE_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="REGION_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IS_DEPOT_APPLICABLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RECALCULATE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="TRACK_CAPACITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IS_CONTRACT_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IS_DIRECT_ONLY_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USE_TACT_AS_DISPLAY_RATE_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USE_TACT_AS_DISPLAY_RATE_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="USE_TACT_AS_DISPLAY_RATE_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="IS_ROUTE_EXECUTION_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PACKAGE_WEIGHT_MIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PACKAGE_WEIGHT_MIN_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PACKAGE_WEIGHT_MIN_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_AVERAGE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_AVERAGE_PKG_WEIGH_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_AVERAGE_PKG_WEIGH_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PACKAGE_COUNT_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_SHIPUNIT_LINE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_SHIPUNIT_LINE_PKG_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_SHIPUNIT_LINE_PKG_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_NUM_OF_SHIPMENT_SEGMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_OFFERING_STOPS" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                             &lt;element name="RATE_OFFERING_STOPS_ROW" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="RATE_OFFERING_ACCESSORIAL" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="RATE_OFFERING_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="RATE_OFFERING_COMMENT" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                             &lt;element name="RATE_OFFERING_COMMENT_ROW" maxOccurs="unbounded"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="RATE_RULES_AND_TERMS" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="RATE_RULES_AND_TERMS_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="RATE_OFFERING_INV_PARTY" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="RATE_OFFERING_INV_PARTY_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                       &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                     &lt;/sequence&gt;
 *                                     &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateOfferingType", propOrder = {
    "sendReason",
    "rateofferingrow"
})
public class RateOfferingType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "RATE_OFFERING_ROW", required = true)
    protected List<RateOfferingType.RATEOFFERINGROW> rateofferingrow;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the rateofferingrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rateofferingrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEOFFERINGROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RateOfferingType.RATEOFFERINGROW }
     * 
     * 
     */
    public List<RateOfferingType.RATEOFFERINGROW> getRATEOFFERINGROW() {
        if (rateofferingrow == null) {
            rateofferingrow = new ArrayList<RateOfferingType.RATEOFFERINGROW>();
        }
        return this.rateofferingrow;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RATE_OFFERING_XID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RATE_OFFERING_TYPE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RATE_OFFERING_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SERVPROV_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="TRANSPORT_MODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="USER_CLASSIFICATION1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USER_CLASSIFICATION2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USER_CLASSIFICATION3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USER_CLASSIFICATION4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_VERSION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MAX_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_DIST_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_DIST_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_WEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_WEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_WEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_VOLUME_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_VOLUME_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_VOLUME_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_SHIP_UNIT_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_SHIP_UNIT_WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_SHIP_UNIT_WEIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_DISTANCE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_CLASSIFICATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXTERNAL_RATING_ENGINE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="STOPS_INCLUDED_IN_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHIPPER_MIN_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_SHIP_UNIT_SPEC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXCHANGE_RATE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="COMMODITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="FAK_RATE_AS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="FAK_FLEX_COMMODITY_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RM_ABSOLUTE_MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RM_ABSOLUTE_MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RM_ABSOLUTE_MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_LENGTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_LENGTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_LENGTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_WIDTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_WIDTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_WIDTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_HEIGHT_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_HEIGHT_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_HEIGHT_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_GIRTH_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_GIRTH_CONSTRAINT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_GIRTH_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="WEIGHT_BREAK_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MULTI_STOP_COST_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="WEIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="VOLUME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PERSPECTIVE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CORPORATION_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CAPACITY_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CM_MAX_NUM_SHIPMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CM_IS_SAME_EQUIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CM_PREV_SHIPMENT_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CM_IS_PERCENT_OF_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_REFNUM_QUALIFIER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_REFNUM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_PUB_AUTHORITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_REG_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_AGENCY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_ISSUING_CARRIER_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_REFNUM_SUFFIX" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_SUPPLEMENT_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TARIFF_EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_ACTIVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="HANDLES_UNKNOWN_SHIPPER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USES_TIME_BASED_RATES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_DISTANCE_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_DISTANCE_CONSTRNT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_DISTANCE_CONSTRAINT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="REGION_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_DEPOT_APPLICABLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RECALCULATE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TRACK_CAPACITY_USAGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_CONTRACT_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_DIRECT_ONLY_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USE_TACT_AS_DISPLAY_RATE_1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USE_TACT_AS_DISPLAY_RATE_2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="USE_TACT_AS_DISPLAY_RATE_3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_ROUTE_EXECUTION_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_TENDER_LEAD_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_TENDER_LEAD_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_TENDER_LEAD_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PACKAGE_WEIGHT_MIN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PACKAGE_WEIGHT_MIN_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PACKAGE_WEIGHT_MIN_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_AVERAGE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_AVERAGE_PKG_WEIGH_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_AVERAGE_PKG_WEIGH_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PACKAGE_COUNT_METHOD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_SHIPUNIT_LINE_PKG_WEIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_SHIPUNIT_LINE_PKG_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_SHIPUNIT_LINE_PKG_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_NUM_OF_SHIPMENT_SEGMENTS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXT_RE_FIELDSET_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_OFFERING_STOPS" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *                   &lt;element name="RATE_OFFERING_STOPS_ROW" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RATE_OFFERING_ACCESSORIAL" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="RATE_OFFERING_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RATE_OFFERING_COMMENT" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *                   &lt;element name="RATE_OFFERING_COMMENT_ROW" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RATE_RULES_AND_TERMS" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="RATE_RULES_AND_TERMS_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RATE_OFFERING_INV_PARTY" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="RATE_OFFERING_INV_PARTY_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rateofferinggid",
        "rateofferingxid",
        "rateofferingtypegid",
        "rateofferingdesc",
        "servprovgid",
        "rategroupgid",
        "currencygid",
        "transportmodegid",
        "userclassification1",
        "userclassification2",
        "userclassification3",
        "userclassification4",
        "equipmentgroupprofilegid",
        "rateservicegid",
        "rateversiongid",
        "maxdistanceconstraint",
        "maxdistconstraintuomcode",
        "maxdistconstraintbase",
        "minweightconstraint",
        "minweightconstraintuomcode",
        "minweightconstraintbase",
        "maxweightconstraint",
        "maxweightconstraintuomcode",
        "maxweightconstraintbase",
        "minvolumeconstraint",
        "minvolumeconstraintuomcode",
        "minvolumeconstraintbase",
        "maxvolumeconstraint",
        "maxvolumeconstraintuomcode",
        "maxvolumeconstraintbase",
        "maxshipunitweight",
        "maxshipunitweightuomcode",
        "maxshipunitweightbase",
        "tariffname",
        "mincost",
        "mincostgid",
        "mincostbase",
        "ratedistancegid",
        "ratequalitygid",
        "rateclassificationgid",
        "externalratingenginegid",
        "circuityallowancepercent",
        "totalstopsconstraint",
        "pickupstopsconstraint",
        "deliverystopsconstraint",
        "circuitydistancecost",
        "circuitydistancecostgid",
        "circuitydistancecostbase",
        "maxcircuitypercent",
        "maxcircuitydistance",
        "maxcircuitydistanceuomcode",
        "maxcircuitydistancebase",
        "stopsincludedinrate",
        "flexcommodityprofilegid",
        "shipperminvalue",
        "shipperminvaluegid",
        "shipperminvaluebase",
        "minshipunitspecgid",
        "exchangerategid",
        "domainname",
        "commodityusage",
        "fakrateas",
        "fakflexcommodityvalue",
        "rmabsolutemincost",
        "rmabsolutemincostgid",
        "rmabsolutemincostbase",
        "maxcost",
        "maxcostgid",
        "maxcostbase",
        "minstops",
        "shortlinecost",
        "shortlinecostgid",
        "shortlinecostbase",
        "minlengthconstraint",
        "minlengthconstraintuomcode",
        "minlengthconstraintbase",
        "maxlengthconstraint",
        "maxlengthconstraintuomcode",
        "maxlengthconstraintbase",
        "minwidthconstraint",
        "minwidthconstraintuomcode",
        "minwidthconstraintbase",
        "maxwidthconstraint",
        "maxwidthconstraintuomcode",
        "maxwidthconstraintbase",
        "minheightconstraint",
        "minheightconstraintuomcode",
        "minheightconstraintbase",
        "maxheightconstraint",
        "maxheightconstraintuomcode",
        "maxheightconstraintbase",
        "mingirthconstraint",
        "mingirthconstraintuomcode",
        "mingirthconstraintbase",
        "maxgirthconstraint",
        "maxgirthconstraintuomcode",
        "maxgirthconstraintbase",
        "weightbreakprofilegid",
        "multistopcostmethod",
        "weightuomcode",
        "volumeuomcode",
        "distanceuomcode",
        "dimratefactorgid",
        "perspective",
        "corporationprofilegid",
        "capacitygroupgid",
        "cmmaxnumshipments",
        "cmissameequip",
        "cmprevshipmentpercent",
        "cmispercentofdistance",
        "tariffrefnumqualifier",
        "tariffrefnum",
        "tariffpubauthority",
        "tariffregagencycode",
        "tariffagencycode",
        "tariffissuingcarrierid",
        "tariffrefnumsuffix",
        "tariffsupplementid",
        "tariffeffectivedate",
        "allowuncostedlineitems",
        "railintermodalplangid",
        "customerratecode",
        "cofctofc",
        "isactive",
        "handlesunknownshipper",
        "usestimebasedrates",
        "expiremarkid",
        "mindistanceconstraint",
        "mindistanceconstrntuomcode",
        "mindistanceconstraintbase",
        "regiongroupgid",
        "isdepotapplicable",
        "recalculatecost",
        "trackcapacityusage",
        "roundingtype",
        "roundinginterval",
        "roundingfieldslevel",
        "roundingapplication",
        "iscontractrate",
        "isdirectonlyrate",
        "hazardousratetype",
        "usetactasdisplayrate1",
        "usetactasdisplayrate2",
        "usetactasdisplayrate3",
        "domainprofilegid",
        "isrouteexecutionrate",
        "mintenderleadtime",
        "mintenderleadtimeuomcode",
        "mintenderleadtimebase",
        "maxtenderleadtime",
        "maxtenderleadtimeuomcode",
        "maxtenderleadtimebase",
        "packageweightmin",
        "packageweightminuomcode",
        "packageweightminbase",
        "maxaveragepkgweight",
        "maxaveragepkgweighuomcode",
        "maxaveragepkgweighbase",
        "packagecountmethod",
        "maxshipunitlinepkgweight",
        "maxshipunitlinepkguomcode",
        "maxshipunitlinepkgbase",
        "maxnumofshipmentsegments",
        "rotimeperioddefgid",
        "extrefieldsetgid",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate",
        "rateofferingstops",
        "rateofferingaccessorial",
        "rateofferingcomment",
        "raterulesandterms",
        "rospecialserviceaccessorial",
        "rateofferinginvparty",
        "attribute1",
        "attribute2",
        "attribute3",
        "attribute4",
        "attribute5",
        "attribute6",
        "attribute7",
        "attribute8",
        "attribute9",
        "attribute10",
        "attribute11",
        "attribute12",
        "attribute13",
        "attribute14",
        "attribute15",
        "attribute16",
        "attribute17",
        "attribute18",
        "attribute19",
        "attribute20",
        "attributenumber1",
        "attributenumber2",
        "attributenumber3",
        "attributenumber4",
        "attributenumber5",
        "attributenumber6",
        "attributenumber7",
        "attributenumber8",
        "attributenumber9",
        "attributenumber10",
        "attributedate1",
        "attributedate2",
        "attributedate3",
        "attributedate4",
        "attributedate5",
        "attributedate6",
        "attributedate7",
        "attributedate8",
        "attributedate9",
        "attributedate10"
    })
    public static class RATEOFFERINGROW {

        @XmlElement(name = "RATE_OFFERING_GID", required = true)
        protected String rateofferinggid;
        @XmlElement(name = "RATE_OFFERING_XID", required = true)
        protected String rateofferingxid;
        @XmlElement(name = "RATE_OFFERING_TYPE_GID", required = true)
        protected String rateofferingtypegid;
        @XmlElement(name = "RATE_OFFERING_DESC")
        protected String rateofferingdesc;
        @XmlElement(name = "SERVPROV_GID")
        protected String servprovgid;
        @XmlElement(name = "RATE_GROUP_GID")
        protected String rategroupgid;
        @XmlElement(name = "CURRENCY_GID", required = true)
        protected String currencygid;
        @XmlElement(name = "TRANSPORT_MODE_GID", required = true)
        protected String transportmodegid;
        @XmlElement(name = "USER_CLASSIFICATION1")
        protected String userclassification1;
        @XmlElement(name = "USER_CLASSIFICATION2")
        protected String userclassification2;
        @XmlElement(name = "USER_CLASSIFICATION3")
        protected String userclassification3;
        @XmlElement(name = "USER_CLASSIFICATION4")
        protected String userclassification4;
        @XmlElement(name = "EQUIPMENT_GROUP_PROFILE_GID")
        protected String equipmentgroupprofilegid;
        @XmlElement(name = "RATE_SERVICE_GID")
        protected String rateservicegid;
        @XmlElement(name = "RATE_VERSION_GID", required = true)
        protected String rateversiongid;
        @XmlElement(name = "MAX_DISTANCE_CONSTRAINT")
        protected String maxdistanceconstraint;
        @XmlElement(name = "MAX_DIST_CONSTRAINT_UOM_CODE")
        protected String maxdistconstraintuomcode;
        @XmlElement(name = "MAX_DIST_CONSTRAINT_BASE")
        protected String maxdistconstraintbase;
        @XmlElement(name = "MIN_WEIGHT_CONSTRAINT")
        protected String minweightconstraint;
        @XmlElement(name = "MIN_WEIGHT_CONSTRAINT_UOM_CODE")
        protected String minweightconstraintuomcode;
        @XmlElement(name = "MIN_WEIGHT_CONSTRAINT_BASE")
        protected String minweightconstraintbase;
        @XmlElement(name = "MAX_WEIGHT_CONSTRAINT")
        protected String maxweightconstraint;
        @XmlElement(name = "MAX_WEIGHT_CONSTRAINT_UOM_CODE")
        protected String maxweightconstraintuomcode;
        @XmlElement(name = "MAX_WEIGHT_CONSTRAINT_BASE")
        protected String maxweightconstraintbase;
        @XmlElement(name = "MIN_VOLUME_CONSTRAINT")
        protected String minvolumeconstraint;
        @XmlElement(name = "MIN_VOLUME_CONSTRAINT_UOM_CODE")
        protected String minvolumeconstraintuomcode;
        @XmlElement(name = "MIN_VOLUME_CONSTRAINT_BASE")
        protected String minvolumeconstraintbase;
        @XmlElement(name = "MAX_VOLUME_CONSTRAINT")
        protected String maxvolumeconstraint;
        @XmlElement(name = "MAX_VOLUME_CONSTRAINT_UOM_CODE")
        protected String maxvolumeconstraintuomcode;
        @XmlElement(name = "MAX_VOLUME_CONSTRAINT_BASE")
        protected String maxvolumeconstraintbase;
        @XmlElement(name = "MAX_SHIP_UNIT_WEIGHT")
        protected String maxshipunitweight;
        @XmlElement(name = "MAX_SHIP_UNIT_WEIGHT_UOM_CODE")
        protected String maxshipunitweightuomcode;
        @XmlElement(name = "MAX_SHIP_UNIT_WEIGHT_BASE")
        protected String maxshipunitweightbase;
        @XmlElement(name = "TARIFF_NAME")
        protected String tariffname;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_GID")
        protected String mincostgid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "RATE_DISTANCE_GID")
        protected String ratedistancegid;
        @XmlElement(name = "RATE_QUALITY_GID")
        protected String ratequalitygid;
        @XmlElement(name = "RATE_CLASSIFICATION_GID")
        protected String rateclassificationgid;
        @XmlElement(name = "EXTERNAL_RATING_ENGINE_GID")
        protected String externalratingenginegid;
        @XmlElement(name = "CIRCUITY_ALLOWANCE_PERCENT")
        protected String circuityallowancepercent;
        @XmlElement(name = "TOTAL_STOPS_CONSTRAINT")
        protected String totalstopsconstraint;
        @XmlElement(name = "PICKUP_STOPS_CONSTRAINT")
        protected String pickupstopsconstraint;
        @XmlElement(name = "DELIVERY_STOPS_CONSTRAINT")
        protected String deliverystopsconstraint;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST")
        protected String circuitydistancecost;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_GID")
        protected String circuitydistancecostgid;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_BASE")
        protected String circuitydistancecostbase;
        @XmlElement(name = "MAX_CIRCUITY_PERCENT")
        protected String maxcircuitypercent;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE")
        protected String maxcircuitydistance;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_UOM_CODE")
        protected String maxcircuitydistanceuomcode;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_BASE")
        protected String maxcircuitydistancebase;
        @XmlElement(name = "STOPS_INCLUDED_IN_RATE")
        protected String stopsincludedinrate;
        @XmlElement(name = "FLEX_COMMODITY_PROFILE_GID")
        protected String flexcommodityprofilegid;
        @XmlElement(name = "SHIPPER_MIN_VALUE")
        protected String shipperminvalue;
        @XmlElement(name = "SHIPPER_MIN_VALUE_GID")
        protected String shipperminvaluegid;
        @XmlElement(name = "SHIPPER_MIN_VALUE_BASE")
        protected String shipperminvaluebase;
        @XmlElement(name = "MIN_SHIP_UNIT_SPEC_GID")
        protected String minshipunitspecgid;
        @XmlElement(name = "EXCHANGE_RATE_GID", required = true)
        protected String exchangerategid;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "COMMODITY_USAGE", required = true)
        protected String commodityusage;
        @XmlElement(name = "FAK_RATE_AS", required = true)
        protected String fakrateas;
        @XmlElement(name = "FAK_FLEX_COMMODITY_VALUE")
        protected String fakflexcommodityvalue;
        @XmlElement(name = "RM_ABSOLUTE_MIN_COST")
        protected String rmabsolutemincost;
        @XmlElement(name = "RM_ABSOLUTE_MIN_COST_GID")
        protected String rmabsolutemincostgid;
        @XmlElement(name = "RM_ABSOLUTE_MIN_COST_BASE")
        protected String rmabsolutemincostbase;
        @XmlElement(name = "MAX_COST")
        protected String maxcost;
        @XmlElement(name = "MAX_COST_GID")
        protected String maxcostgid;
        @XmlElement(name = "MAX_COST_BASE")
        protected String maxcostbase;
        @XmlElement(name = "MIN_STOPS")
        protected String minstops;
        @XmlElement(name = "SHORT_LINE_COST")
        protected String shortlinecost;
        @XmlElement(name = "SHORT_LINE_COST_GID")
        protected String shortlinecostgid;
        @XmlElement(name = "SHORT_LINE_COST_BASE")
        protected String shortlinecostbase;
        @XmlElement(name = "MIN_LENGTH_CONSTRAINT")
        protected String minlengthconstraint;
        @XmlElement(name = "MIN_LENGTH_CONSTRAINT_UOM_CODE")
        protected String minlengthconstraintuomcode;
        @XmlElement(name = "MIN_LENGTH_CONSTRAINT_BASE")
        protected String minlengthconstraintbase;
        @XmlElement(name = "MAX_LENGTH_CONSTRAINT")
        protected String maxlengthconstraint;
        @XmlElement(name = "MAX_LENGTH_CONSTRAINT_UOM_CODE")
        protected String maxlengthconstraintuomcode;
        @XmlElement(name = "MAX_LENGTH_CONSTRAINT_BASE")
        protected String maxlengthconstraintbase;
        @XmlElement(name = "MIN_WIDTH_CONSTRAINT")
        protected String minwidthconstraint;
        @XmlElement(name = "MIN_WIDTH_CONSTRAINT_UOM_CODE")
        protected String minwidthconstraintuomcode;
        @XmlElement(name = "MIN_WIDTH_CONSTRAINT_BASE")
        protected String minwidthconstraintbase;
        @XmlElement(name = "MAX_WIDTH_CONSTRAINT")
        protected String maxwidthconstraint;
        @XmlElement(name = "MAX_WIDTH_CONSTRAINT_UOM_CODE")
        protected String maxwidthconstraintuomcode;
        @XmlElement(name = "MAX_WIDTH_CONSTRAINT_BASE")
        protected String maxwidthconstraintbase;
        @XmlElement(name = "MIN_HEIGHT_CONSTRAINT")
        protected String minheightconstraint;
        @XmlElement(name = "MIN_HEIGHT_CONSTRAINT_UOM_CODE")
        protected String minheightconstraintuomcode;
        @XmlElement(name = "MIN_HEIGHT_CONSTRAINT_BASE")
        protected String minheightconstraintbase;
        @XmlElement(name = "MAX_HEIGHT_CONSTRAINT")
        protected String maxheightconstraint;
        @XmlElement(name = "MAX_HEIGHT_CONSTRAINT_UOM_CODE")
        protected String maxheightconstraintuomcode;
        @XmlElement(name = "MAX_HEIGHT_CONSTRAINT_BASE")
        protected String maxheightconstraintbase;
        @XmlElement(name = "MIN_GIRTH_CONSTRAINT")
        protected String mingirthconstraint;
        @XmlElement(name = "MIN_GIRTH_CONSTRAINT_UOM_CODE")
        protected String mingirthconstraintuomcode;
        @XmlElement(name = "MIN_GIRTH_CONSTRAINT_BASE")
        protected String mingirthconstraintbase;
        @XmlElement(name = "MAX_GIRTH_CONSTRAINT")
        protected String maxgirthconstraint;
        @XmlElement(name = "MAX_GIRTH_CONSTRAINT_UOM_CODE")
        protected String maxgirthconstraintuomcode;
        @XmlElement(name = "MAX_GIRTH_CONSTRAINT_BASE")
        protected String maxgirthconstraintbase;
        @XmlElement(name = "WEIGHT_BREAK_PROFILE_GID")
        protected String weightbreakprofilegid;
        @XmlElement(name = "MULTI_STOP_COST_METHOD")
        protected String multistopcostmethod;
        @XmlElement(name = "WEIGHT_UOM_CODE")
        protected String weightuomcode;
        @XmlElement(name = "VOLUME_UOM_CODE")
        protected String volumeuomcode;
        @XmlElement(name = "DISTANCE_UOM_CODE")
        protected String distanceuomcode;
        @XmlElement(name = "DIM_RATE_FACTOR_GID")
        protected String dimratefactorgid;
        @XmlElement(name = "PERSPECTIVE", required = true)
        protected String perspective;
        @XmlElement(name = "CORPORATION_PROFILE_GID")
        protected String corporationprofilegid;
        @XmlElement(name = "CAPACITY_GROUP_GID")
        protected String capacitygroupgid;
        @XmlElement(name = "CM_MAX_NUM_SHIPMENTS")
        protected String cmmaxnumshipments;
        @XmlElement(name = "CM_IS_SAME_EQUIP")
        protected String cmissameequip;
        @XmlElement(name = "CM_PREV_SHIPMENT_PERCENT")
        protected String cmprevshipmentpercent;
        @XmlElement(name = "CM_IS_PERCENT_OF_DISTANCE")
        protected String cmispercentofdistance;
        @XmlElement(name = "TARIFF_REFNUM_QUALIFIER")
        protected String tariffrefnumqualifier;
        @XmlElement(name = "TARIFF_REFNUM")
        protected String tariffrefnum;
        @XmlElement(name = "TARIFF_PUB_AUTHORITY")
        protected String tariffpubauthority;
        @XmlElement(name = "TARIFF_REG_AGENCY_CODE")
        protected String tariffregagencycode;
        @XmlElement(name = "TARIFF_AGENCY_CODE")
        protected String tariffagencycode;
        @XmlElement(name = "TARIFF_ISSUING_CARRIER_ID")
        protected String tariffissuingcarrierid;
        @XmlElement(name = "TARIFF_REFNUM_SUFFIX")
        protected String tariffrefnumsuffix;
        @XmlElement(name = "TARIFF_SUPPLEMENT_ID")
        protected String tariffsupplementid;
        @XmlElement(name = "TARIFF_EFFECTIVE_DATE")
        protected String tariffeffectivedate;
        @XmlElement(name = "ALLOW_UNCOSTED_LINE_ITEMS", required = true)
        protected String allowuncostedlineitems;
        @XmlElement(name = "RAIL_INTER_MODAL_PLAN_GID")
        protected String railintermodalplangid;
        @XmlElement(name = "CUSTOMER_RATE_CODE")
        protected String customerratecode;
        @XmlElement(name = "COFC_TOFC")
        protected String cofctofc;
        @XmlElement(name = "IS_ACTIVE")
        protected String isactive;
        @XmlElement(name = "HANDLES_UNKNOWN_SHIPPER")
        protected String handlesunknownshipper;
        @XmlElement(name = "USES_TIME_BASED_RATES")
        protected String usestimebasedrates;
        @XmlElement(name = "EXPIRE_MARK_ID")
        protected String expiremarkid;
        @XmlElement(name = "MIN_DISTANCE_CONSTRAINT")
        protected String mindistanceconstraint;
        @XmlElement(name = "MIN_DISTANCE_CONSTRNT_UOM_CODE")
        protected String mindistanceconstrntuomcode;
        @XmlElement(name = "MIN_DISTANCE_CONSTRAINT_BASE")
        protected String mindistanceconstraintbase;
        @XmlElement(name = "REGION_GROUP_GID")
        protected String regiongroupgid;
        @XmlElement(name = "IS_DEPOT_APPLICABLE")
        protected String isdepotapplicable;
        @XmlElement(name = "RECALCULATE_COST")
        protected String recalculatecost;
        @XmlElement(name = "TRACK_CAPACITY_USAGE")
        protected String trackcapacityusage;
        @XmlElement(name = "ROUNDING_TYPE")
        protected String roundingtype;
        @XmlElement(name = "ROUNDING_INTERVAL")
        protected String roundinginterval;
        @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
        protected String roundingfieldslevel;
        @XmlElement(name = "ROUNDING_APPLICATION")
        protected String roundingapplication;
        @XmlElement(name = "IS_CONTRACT_RATE")
        protected String iscontractrate;
        @XmlElement(name = "IS_DIRECT_ONLY_RATE")
        protected String isdirectonlyrate;
        @XmlElement(name = "HAZARDOUS_RATE_TYPE")
        protected String hazardousratetype;
        @XmlElement(name = "USE_TACT_AS_DISPLAY_RATE_1")
        protected String usetactasdisplayrate1;
        @XmlElement(name = "USE_TACT_AS_DISPLAY_RATE_2")
        protected String usetactasdisplayrate2;
        @XmlElement(name = "USE_TACT_AS_DISPLAY_RATE_3")
        protected String usetactasdisplayrate3;
        @XmlElement(name = "DOMAIN_PROFILE_GID")
        protected String domainprofilegid;
        @XmlElement(name = "IS_ROUTE_EXECUTION_RATE")
        protected String isrouteexecutionrate;
        @XmlElement(name = "MIN_TENDER_LEAD_TIME")
        protected String mintenderleadtime;
        @XmlElement(name = "MIN_TENDER_LEAD_TIME_UOM_CODE")
        protected String mintenderleadtimeuomcode;
        @XmlElement(name = "MIN_TENDER_LEAD_TIME_BASE")
        protected String mintenderleadtimebase;
        @XmlElement(name = "MAX_TENDER_LEAD_TIME")
        protected String maxtenderleadtime;
        @XmlElement(name = "MAX_TENDER_LEAD_TIME_UOM_CODE")
        protected String maxtenderleadtimeuomcode;
        @XmlElement(name = "MAX_TENDER_LEAD_TIME_BASE")
        protected String maxtenderleadtimebase;
        @XmlElement(name = "PACKAGE_WEIGHT_MIN")
        protected String packageweightmin;
        @XmlElement(name = "PACKAGE_WEIGHT_MIN_UOM_CODE")
        protected String packageweightminuomcode;
        @XmlElement(name = "PACKAGE_WEIGHT_MIN_BASE")
        protected String packageweightminbase;
        @XmlElement(name = "MAX_AVERAGE_PKG_WEIGHT")
        protected String maxaveragepkgweight;
        @XmlElement(name = "MAX_AVERAGE_PKG_WEIGH_UOM_CODE")
        protected String maxaveragepkgweighuomcode;
        @XmlElement(name = "MAX_AVERAGE_PKG_WEIGH_BASE")
        protected String maxaveragepkgweighbase;
        @XmlElement(name = "PACKAGE_COUNT_METHOD")
        protected String packagecountmethod;
        @XmlElement(name = "MAX_SHIPUNIT_LINE_PKG_WEIGHT")
        protected String maxshipunitlinepkgweight;
        @XmlElement(name = "MAX_SHIPUNIT_LINE_PKG_UOM_CODE")
        protected String maxshipunitlinepkguomcode;
        @XmlElement(name = "MAX_SHIPUNIT_LINE_PKG_BASE")
        protected String maxshipunitlinepkgbase;
        @XmlElement(name = "MAX_NUM_OF_SHIPMENT_SEGMENTS")
        protected String maxnumofshipmentsegments;
        @XmlElement(name = "RO_TIME_PERIOD_DEF_GID")
        protected String rotimeperioddefgid;
        @XmlElement(name = "EXT_RE_FIELDSET_GID")
        protected String extrefieldsetgid;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlElement(name = "RATE_OFFERING_STOPS")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS rateofferingstops;
        @XmlElement(name = "RATE_OFFERING_ACCESSORIAL")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL rateofferingaccessorial;
        @XmlElement(name = "RATE_OFFERING_COMMENT")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT rateofferingcomment;
        @XmlElement(name = "RATE_RULES_AND_TERMS")
        protected RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS raterulesandterms;
        @XmlElement(name = "RO_SPECIAL_SERVICE_ACCESSORIAL")
        protected RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL rospecialserviceaccessorial;
        @XmlElement(name = "RATE_OFFERING_INV_PARTY")
        protected RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY rateofferinginvparty;
        @XmlElement(name = "ATTRIBUTE1")
        protected String attribute1;
        @XmlElement(name = "ATTRIBUTE2")
        protected String attribute2;
        @XmlElement(name = "ATTRIBUTE3")
        protected String attribute3;
        @XmlElement(name = "ATTRIBUTE4")
        protected String attribute4;
        @XmlElement(name = "ATTRIBUTE5")
        protected String attribute5;
        @XmlElement(name = "ATTRIBUTE6")
        protected String attribute6;
        @XmlElement(name = "ATTRIBUTE7")
        protected String attribute7;
        @XmlElement(name = "ATTRIBUTE8")
        protected String attribute8;
        @XmlElement(name = "ATTRIBUTE9")
        protected String attribute9;
        @XmlElement(name = "ATTRIBUTE10")
        protected String attribute10;
        @XmlElement(name = "ATTRIBUTE11")
        protected String attribute11;
        @XmlElement(name = "ATTRIBUTE12")
        protected String attribute12;
        @XmlElement(name = "ATTRIBUTE13")
        protected String attribute13;
        @XmlElement(name = "ATTRIBUTE14")
        protected String attribute14;
        @XmlElement(name = "ATTRIBUTE15")
        protected String attribute15;
        @XmlElement(name = "ATTRIBUTE16")
        protected String attribute16;
        @XmlElement(name = "ATTRIBUTE17")
        protected String attribute17;
        @XmlElement(name = "ATTRIBUTE18")
        protected String attribute18;
        @XmlElement(name = "ATTRIBUTE19")
        protected String attribute19;
        @XmlElement(name = "ATTRIBUTE20")
        protected String attribute20;
        @XmlElement(name = "ATTRIBUTE_NUMBER1")
        protected String attributenumber1;
        @XmlElement(name = "ATTRIBUTE_NUMBER2")
        protected String attributenumber2;
        @XmlElement(name = "ATTRIBUTE_NUMBER3")
        protected String attributenumber3;
        @XmlElement(name = "ATTRIBUTE_NUMBER4")
        protected String attributenumber4;
        @XmlElement(name = "ATTRIBUTE_NUMBER5")
        protected String attributenumber5;
        @XmlElement(name = "ATTRIBUTE_NUMBER6")
        protected String attributenumber6;
        @XmlElement(name = "ATTRIBUTE_NUMBER7")
        protected String attributenumber7;
        @XmlElement(name = "ATTRIBUTE_NUMBER8")
        protected String attributenumber8;
        @XmlElement(name = "ATTRIBUTE_NUMBER9")
        protected String attributenumber9;
        @XmlElement(name = "ATTRIBUTE_NUMBER10")
        protected String attributenumber10;
        @XmlElement(name = "ATTRIBUTE_DATE1")
        protected String attributedate1;
        @XmlElement(name = "ATTRIBUTE_DATE2")
        protected String attributedate2;
        @XmlElement(name = "ATTRIBUTE_DATE3")
        protected String attributedate3;
        @XmlElement(name = "ATTRIBUTE_DATE4")
        protected String attributedate4;
        @XmlElement(name = "ATTRIBUTE_DATE5")
        protected String attributedate5;
        @XmlElement(name = "ATTRIBUTE_DATE6")
        protected String attributedate6;
        @XmlElement(name = "ATTRIBUTE_DATE7")
        protected String attributedate7;
        @XmlElement(name = "ATTRIBUTE_DATE8")
        protected String attributedate8;
        @XmlElement(name = "ATTRIBUTE_DATE9")
        protected String attributedate9;
        @XmlElement(name = "ATTRIBUTE_DATE10")
        protected String attributedate10;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Gets the value of the rateofferinggid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGGID() {
            return rateofferinggid;
        }

        /**
         * Sets the value of the rateofferinggid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGGID(String value) {
            this.rateofferinggid = value;
        }

        /**
         * Gets the value of the rateofferingxid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGXID() {
            return rateofferingxid;
        }

        /**
         * Sets the value of the rateofferingxid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGXID(String value) {
            this.rateofferingxid = value;
        }

        /**
         * Gets the value of the rateofferingtypegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGTYPEGID() {
            return rateofferingtypegid;
        }

        /**
         * Sets the value of the rateofferingtypegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGTYPEGID(String value) {
            this.rateofferingtypegid = value;
        }

        /**
         * Gets the value of the rateofferingdesc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGDESC() {
            return rateofferingdesc;
        }

        /**
         * Sets the value of the rateofferingdesc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGDESC(String value) {
            this.rateofferingdesc = value;
        }

        /**
         * Gets the value of the servprovgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSERVPROVGID() {
            return servprovgid;
        }

        /**
         * Sets the value of the servprovgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSERVPROVGID(String value) {
            this.servprovgid = value;
        }

        /**
         * Gets the value of the rategroupgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGROUPGID() {
            return rategroupgid;
        }

        /**
         * Sets the value of the rategroupgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGROUPGID(String value) {
            this.rategroupgid = value;
        }

        /**
         * Gets the value of the currencygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCURRENCYGID() {
            return currencygid;
        }

        /**
         * Sets the value of the currencygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCURRENCYGID(String value) {
            this.currencygid = value;
        }

        /**
         * Gets the value of the transportmodegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRANSPORTMODEGID() {
            return transportmodegid;
        }

        /**
         * Sets the value of the transportmodegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRANSPORTMODEGID(String value) {
            this.transportmodegid = value;
        }

        /**
         * Gets the value of the userclassification1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION1() {
            return userclassification1;
        }

        /**
         * Sets the value of the userclassification1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION1(String value) {
            this.userclassification1 = value;
        }

        /**
         * Gets the value of the userclassification2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION2() {
            return userclassification2;
        }

        /**
         * Sets the value of the userclassification2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION2(String value) {
            this.userclassification2 = value;
        }

        /**
         * Gets the value of the userclassification3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION3() {
            return userclassification3;
        }

        /**
         * Sets the value of the userclassification3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION3(String value) {
            this.userclassification3 = value;
        }

        /**
         * Gets the value of the userclassification4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSERCLASSIFICATION4() {
            return userclassification4;
        }

        /**
         * Sets the value of the userclassification4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSERCLASSIFICATION4(String value) {
            this.userclassification4 = value;
        }

        /**
         * Gets the value of the equipmentgroupprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEQUIPMENTGROUPPROFILEGID() {
            return equipmentgroupprofilegid;
        }

        /**
         * Sets the value of the equipmentgroupprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEQUIPMENTGROUPPROFILEGID(String value) {
            this.equipmentgroupprofilegid = value;
        }

        /**
         * Gets the value of the rateservicegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATESERVICEGID() {
            return rateservicegid;
        }

        /**
         * Sets the value of the rateservicegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATESERVICEGID(String value) {
            this.rateservicegid = value;
        }

        /**
         * Gets the value of the rateversiongid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEVERSIONGID() {
            return rateversiongid;
        }

        /**
         * Sets the value of the rateversiongid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEVERSIONGID(String value) {
            this.rateversiongid = value;
        }

        /**
         * Gets the value of the maxdistanceconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXDISTANCECONSTRAINT() {
            return maxdistanceconstraint;
        }

        /**
         * Sets the value of the maxdistanceconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXDISTANCECONSTRAINT(String value) {
            this.maxdistanceconstraint = value;
        }

        /**
         * Gets the value of the maxdistconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXDISTCONSTRAINTUOMCODE() {
            return maxdistconstraintuomcode;
        }

        /**
         * Sets the value of the maxdistconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXDISTCONSTRAINTUOMCODE(String value) {
            this.maxdistconstraintuomcode = value;
        }

        /**
         * Gets the value of the maxdistconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXDISTCONSTRAINTBASE() {
            return maxdistconstraintbase;
        }

        /**
         * Sets the value of the maxdistconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXDISTCONSTRAINTBASE(String value) {
            this.maxdistconstraintbase = value;
        }

        /**
         * Gets the value of the minweightconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWEIGHTCONSTRAINT() {
            return minweightconstraint;
        }

        /**
         * Sets the value of the minweightconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWEIGHTCONSTRAINT(String value) {
            this.minweightconstraint = value;
        }

        /**
         * Gets the value of the minweightconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWEIGHTCONSTRAINTUOMCODE() {
            return minweightconstraintuomcode;
        }

        /**
         * Sets the value of the minweightconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWEIGHTCONSTRAINTUOMCODE(String value) {
            this.minweightconstraintuomcode = value;
        }

        /**
         * Gets the value of the minweightconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWEIGHTCONSTRAINTBASE() {
            return minweightconstraintbase;
        }

        /**
         * Sets the value of the minweightconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWEIGHTCONSTRAINTBASE(String value) {
            this.minweightconstraintbase = value;
        }

        /**
         * Gets the value of the maxweightconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWEIGHTCONSTRAINT() {
            return maxweightconstraint;
        }

        /**
         * Sets the value of the maxweightconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWEIGHTCONSTRAINT(String value) {
            this.maxweightconstraint = value;
        }

        /**
         * Gets the value of the maxweightconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWEIGHTCONSTRAINTUOMCODE() {
            return maxweightconstraintuomcode;
        }

        /**
         * Sets the value of the maxweightconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWEIGHTCONSTRAINTUOMCODE(String value) {
            this.maxweightconstraintuomcode = value;
        }

        /**
         * Gets the value of the maxweightconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWEIGHTCONSTRAINTBASE() {
            return maxweightconstraintbase;
        }

        /**
         * Sets the value of the maxweightconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWEIGHTCONSTRAINTBASE(String value) {
            this.maxweightconstraintbase = value;
        }

        /**
         * Gets the value of the minvolumeconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINVOLUMECONSTRAINT() {
            return minvolumeconstraint;
        }

        /**
         * Sets the value of the minvolumeconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINVOLUMECONSTRAINT(String value) {
            this.minvolumeconstraint = value;
        }

        /**
         * Gets the value of the minvolumeconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINVOLUMECONSTRAINTUOMCODE() {
            return minvolumeconstraintuomcode;
        }

        /**
         * Sets the value of the minvolumeconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINVOLUMECONSTRAINTUOMCODE(String value) {
            this.minvolumeconstraintuomcode = value;
        }

        /**
         * Gets the value of the minvolumeconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINVOLUMECONSTRAINTBASE() {
            return minvolumeconstraintbase;
        }

        /**
         * Sets the value of the minvolumeconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINVOLUMECONSTRAINTBASE(String value) {
            this.minvolumeconstraintbase = value;
        }

        /**
         * Gets the value of the maxvolumeconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXVOLUMECONSTRAINT() {
            return maxvolumeconstraint;
        }

        /**
         * Sets the value of the maxvolumeconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXVOLUMECONSTRAINT(String value) {
            this.maxvolumeconstraint = value;
        }

        /**
         * Gets the value of the maxvolumeconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXVOLUMECONSTRAINTUOMCODE() {
            return maxvolumeconstraintuomcode;
        }

        /**
         * Sets the value of the maxvolumeconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXVOLUMECONSTRAINTUOMCODE(String value) {
            this.maxvolumeconstraintuomcode = value;
        }

        /**
         * Gets the value of the maxvolumeconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXVOLUMECONSTRAINTBASE() {
            return maxvolumeconstraintbase;
        }

        /**
         * Sets the value of the maxvolumeconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXVOLUMECONSTRAINTBASE(String value) {
            this.maxvolumeconstraintbase = value;
        }

        /**
         * Gets the value of the maxshipunitweight property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITWEIGHT() {
            return maxshipunitweight;
        }

        /**
         * Sets the value of the maxshipunitweight property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITWEIGHT(String value) {
            this.maxshipunitweight = value;
        }

        /**
         * Gets the value of the maxshipunitweightuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITWEIGHTUOMCODE() {
            return maxshipunitweightuomcode;
        }

        /**
         * Sets the value of the maxshipunitweightuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITWEIGHTUOMCODE(String value) {
            this.maxshipunitweightuomcode = value;
        }

        /**
         * Gets the value of the maxshipunitweightbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITWEIGHTBASE() {
            return maxshipunitweightbase;
        }

        /**
         * Sets the value of the maxshipunitweightbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITWEIGHTBASE(String value) {
            this.maxshipunitweightbase = value;
        }

        /**
         * Gets the value of the tariffname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFNAME() {
            return tariffname;
        }

        /**
         * Sets the value of the tariffname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFNAME(String value) {
            this.tariffname = value;
        }

        /**
         * Gets the value of the mincost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Sets the value of the mincost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Gets the value of the mincostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTGID() {
            return mincostgid;
        }

        /**
         * Sets the value of the mincostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTGID(String value) {
            this.mincostgid = value;
        }

        /**
         * Gets the value of the mincostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Sets the value of the mincostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Gets the value of the ratedistancegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISTANCEGID() {
            return ratedistancegid;
        }

        /**
         * Sets the value of the ratedistancegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISTANCEGID(String value) {
            this.ratedistancegid = value;
        }

        /**
         * Gets the value of the ratequalitygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEQUALITYGID() {
            return ratequalitygid;
        }

        /**
         * Sets the value of the ratequalitygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEQUALITYGID(String value) {
            this.ratequalitygid = value;
        }

        /**
         * Gets the value of the rateclassificationgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATECLASSIFICATIONGID() {
            return rateclassificationgid;
        }

        /**
         * Sets the value of the rateclassificationgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATECLASSIFICATIONGID(String value) {
            this.rateclassificationgid = value;
        }

        /**
         * Gets the value of the externalratingenginegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTERNALRATINGENGINEGID() {
            return externalratingenginegid;
        }

        /**
         * Sets the value of the externalratingenginegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTERNALRATINGENGINEGID(String value) {
            this.externalratingenginegid = value;
        }

        /**
         * Gets the value of the circuityallowancepercent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYALLOWANCEPERCENT() {
            return circuityallowancepercent;
        }

        /**
         * Sets the value of the circuityallowancepercent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYALLOWANCEPERCENT(String value) {
            this.circuityallowancepercent = value;
        }

        /**
         * Gets the value of the totalstopsconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTOTALSTOPSCONSTRAINT() {
            return totalstopsconstraint;
        }

        /**
         * Sets the value of the totalstopsconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTOTALSTOPSCONSTRAINT(String value) {
            this.totalstopsconstraint = value;
        }

        /**
         * Gets the value of the pickupstopsconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPICKUPSTOPSCONSTRAINT() {
            return pickupstopsconstraint;
        }

        /**
         * Sets the value of the pickupstopsconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPICKUPSTOPSCONSTRAINT(String value) {
            this.pickupstopsconstraint = value;
        }

        /**
         * Gets the value of the deliverystopsconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDELIVERYSTOPSCONSTRAINT() {
            return deliverystopsconstraint;
        }

        /**
         * Sets the value of the deliverystopsconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDELIVERYSTOPSCONSTRAINT(String value) {
            this.deliverystopsconstraint = value;
        }

        /**
         * Gets the value of the circuitydistancecost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOST() {
            return circuitydistancecost;
        }

        /**
         * Sets the value of the circuitydistancecost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOST(String value) {
            this.circuitydistancecost = value;
        }

        /**
         * Gets the value of the circuitydistancecostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTGID() {
            return circuitydistancecostgid;
        }

        /**
         * Sets the value of the circuitydistancecostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTGID(String value) {
            this.circuitydistancecostgid = value;
        }

        /**
         * Gets the value of the circuitydistancecostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTBASE() {
            return circuitydistancecostbase;
        }

        /**
         * Sets the value of the circuitydistancecostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTBASE(String value) {
            this.circuitydistancecostbase = value;
        }

        /**
         * Gets the value of the maxcircuitypercent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYPERCENT() {
            return maxcircuitypercent;
        }

        /**
         * Sets the value of the maxcircuitypercent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYPERCENT(String value) {
            this.maxcircuitypercent = value;
        }

        /**
         * Gets the value of the maxcircuitydistance property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCE() {
            return maxcircuitydistance;
        }

        /**
         * Sets the value of the maxcircuitydistance property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCE(String value) {
            this.maxcircuitydistance = value;
        }

        /**
         * Gets the value of the maxcircuitydistanceuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEUOMCODE() {
            return maxcircuitydistanceuomcode;
        }

        /**
         * Sets the value of the maxcircuitydistanceuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEUOMCODE(String value) {
            this.maxcircuitydistanceuomcode = value;
        }

        /**
         * Gets the value of the maxcircuitydistancebase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEBASE() {
            return maxcircuitydistancebase;
        }

        /**
         * Sets the value of the maxcircuitydistancebase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEBASE(String value) {
            this.maxcircuitydistancebase = value;
        }

        /**
         * Gets the value of the stopsincludedinrate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTOPSINCLUDEDINRATE() {
            return stopsincludedinrate;
        }

        /**
         * Sets the value of the stopsincludedinrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTOPSINCLUDEDINRATE(String value) {
            this.stopsincludedinrate = value;
        }

        /**
         * Gets the value of the flexcommodityprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFLEXCOMMODITYPROFILEGID() {
            return flexcommodityprofilegid;
        }

        /**
         * Sets the value of the flexcommodityprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFLEXCOMMODITYPROFILEGID(String value) {
            this.flexcommodityprofilegid = value;
        }

        /**
         * Gets the value of the shipperminvalue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUE() {
            return shipperminvalue;
        }

        /**
         * Sets the value of the shipperminvalue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUE(String value) {
            this.shipperminvalue = value;
        }

        /**
         * Gets the value of the shipperminvaluegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUEGID() {
            return shipperminvaluegid;
        }

        /**
         * Sets the value of the shipperminvaluegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUEGID(String value) {
            this.shipperminvaluegid = value;
        }

        /**
         * Gets the value of the shipperminvaluebase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUEBASE() {
            return shipperminvaluebase;
        }

        /**
         * Sets the value of the shipperminvaluebase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUEBASE(String value) {
            this.shipperminvaluebase = value;
        }

        /**
         * Gets the value of the minshipunitspecgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINSHIPUNITSPECGID() {
            return minshipunitspecgid;
        }

        /**
         * Sets the value of the minshipunitspecgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINSHIPUNITSPECGID(String value) {
            this.minshipunitspecgid = value;
        }

        /**
         * Gets the value of the exchangerategid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXCHANGERATEGID() {
            return exchangerategid;
        }

        /**
         * Sets the value of the exchangerategid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXCHANGERATEGID(String value) {
            this.exchangerategid = value;
        }

        /**
         * Gets the value of the domainname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Sets the value of the domainname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Gets the value of the commodityusage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOMMODITYUSAGE() {
            return commodityusage;
        }

        /**
         * Sets the value of the commodityusage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOMMODITYUSAGE(String value) {
            this.commodityusage = value;
        }

        /**
         * Gets the value of the fakrateas property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFAKRATEAS() {
            return fakrateas;
        }

        /**
         * Sets the value of the fakrateas property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFAKRATEAS(String value) {
            this.fakrateas = value;
        }

        /**
         * Gets the value of the fakflexcommodityvalue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFAKFLEXCOMMODITYVALUE() {
            return fakflexcommodityvalue;
        }

        /**
         * Sets the value of the fakflexcommodityvalue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFAKFLEXCOMMODITYVALUE(String value) {
            this.fakflexcommodityvalue = value;
        }

        /**
         * Gets the value of the rmabsolutemincost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRMABSOLUTEMINCOST() {
            return rmabsolutemincost;
        }

        /**
         * Sets the value of the rmabsolutemincost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRMABSOLUTEMINCOST(String value) {
            this.rmabsolutemincost = value;
        }

        /**
         * Gets the value of the rmabsolutemincostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRMABSOLUTEMINCOSTGID() {
            return rmabsolutemincostgid;
        }

        /**
         * Sets the value of the rmabsolutemincostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRMABSOLUTEMINCOSTGID(String value) {
            this.rmabsolutemincostgid = value;
        }

        /**
         * Gets the value of the rmabsolutemincostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRMABSOLUTEMINCOSTBASE() {
            return rmabsolutemincostbase;
        }

        /**
         * Sets the value of the rmabsolutemincostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRMABSOLUTEMINCOSTBASE(String value) {
            this.rmabsolutemincostbase = value;
        }

        /**
         * Gets the value of the maxcost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOST() {
            return maxcost;
        }

        /**
         * Sets the value of the maxcost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOST(String value) {
            this.maxcost = value;
        }

        /**
         * Gets the value of the maxcostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTGID() {
            return maxcostgid;
        }

        /**
         * Sets the value of the maxcostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTGID(String value) {
            this.maxcostgid = value;
        }

        /**
         * Gets the value of the maxcostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTBASE() {
            return maxcostbase;
        }

        /**
         * Sets the value of the maxcostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTBASE(String value) {
            this.maxcostbase = value;
        }

        /**
         * Gets the value of the minstops property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINSTOPS() {
            return minstops;
        }

        /**
         * Sets the value of the minstops property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINSTOPS(String value) {
            this.minstops = value;
        }

        /**
         * Gets the value of the shortlinecost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOST() {
            return shortlinecost;
        }

        /**
         * Sets the value of the shortlinecost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOST(String value) {
            this.shortlinecost = value;
        }

        /**
         * Gets the value of the shortlinecostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTGID() {
            return shortlinecostgid;
        }

        /**
         * Sets the value of the shortlinecostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTGID(String value) {
            this.shortlinecostgid = value;
        }

        /**
         * Gets the value of the shortlinecostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTBASE() {
            return shortlinecostbase;
        }

        /**
         * Sets the value of the shortlinecostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTBASE(String value) {
            this.shortlinecostbase = value;
        }

        /**
         * Gets the value of the minlengthconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINLENGTHCONSTRAINT() {
            return minlengthconstraint;
        }

        /**
         * Sets the value of the minlengthconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINLENGTHCONSTRAINT(String value) {
            this.minlengthconstraint = value;
        }

        /**
         * Gets the value of the minlengthconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINLENGTHCONSTRAINTUOMCODE() {
            return minlengthconstraintuomcode;
        }

        /**
         * Sets the value of the minlengthconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINLENGTHCONSTRAINTUOMCODE(String value) {
            this.minlengthconstraintuomcode = value;
        }

        /**
         * Gets the value of the minlengthconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINLENGTHCONSTRAINTBASE() {
            return minlengthconstraintbase;
        }

        /**
         * Sets the value of the minlengthconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINLENGTHCONSTRAINTBASE(String value) {
            this.minlengthconstraintbase = value;
        }

        /**
         * Gets the value of the maxlengthconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXLENGTHCONSTRAINT() {
            return maxlengthconstraint;
        }

        /**
         * Sets the value of the maxlengthconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXLENGTHCONSTRAINT(String value) {
            this.maxlengthconstraint = value;
        }

        /**
         * Gets the value of the maxlengthconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXLENGTHCONSTRAINTUOMCODE() {
            return maxlengthconstraintuomcode;
        }

        /**
         * Sets the value of the maxlengthconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXLENGTHCONSTRAINTUOMCODE(String value) {
            this.maxlengthconstraintuomcode = value;
        }

        /**
         * Gets the value of the maxlengthconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXLENGTHCONSTRAINTBASE() {
            return maxlengthconstraintbase;
        }

        /**
         * Sets the value of the maxlengthconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXLENGTHCONSTRAINTBASE(String value) {
            this.maxlengthconstraintbase = value;
        }

        /**
         * Gets the value of the minwidthconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWIDTHCONSTRAINT() {
            return minwidthconstraint;
        }

        /**
         * Sets the value of the minwidthconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWIDTHCONSTRAINT(String value) {
            this.minwidthconstraint = value;
        }

        /**
         * Gets the value of the minwidthconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWIDTHCONSTRAINTUOMCODE() {
            return minwidthconstraintuomcode;
        }

        /**
         * Sets the value of the minwidthconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWIDTHCONSTRAINTUOMCODE(String value) {
            this.minwidthconstraintuomcode = value;
        }

        /**
         * Gets the value of the minwidthconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINWIDTHCONSTRAINTBASE() {
            return minwidthconstraintbase;
        }

        /**
         * Sets the value of the minwidthconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINWIDTHCONSTRAINTBASE(String value) {
            this.minwidthconstraintbase = value;
        }

        /**
         * Gets the value of the maxwidthconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWIDTHCONSTRAINT() {
            return maxwidthconstraint;
        }

        /**
         * Sets the value of the maxwidthconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWIDTHCONSTRAINT(String value) {
            this.maxwidthconstraint = value;
        }

        /**
         * Gets the value of the maxwidthconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWIDTHCONSTRAINTUOMCODE() {
            return maxwidthconstraintuomcode;
        }

        /**
         * Sets the value of the maxwidthconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWIDTHCONSTRAINTUOMCODE(String value) {
            this.maxwidthconstraintuomcode = value;
        }

        /**
         * Gets the value of the maxwidthconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXWIDTHCONSTRAINTBASE() {
            return maxwidthconstraintbase;
        }

        /**
         * Sets the value of the maxwidthconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXWIDTHCONSTRAINTBASE(String value) {
            this.maxwidthconstraintbase = value;
        }

        /**
         * Gets the value of the minheightconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINHEIGHTCONSTRAINT() {
            return minheightconstraint;
        }

        /**
         * Sets the value of the minheightconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINHEIGHTCONSTRAINT(String value) {
            this.minheightconstraint = value;
        }

        /**
         * Gets the value of the minheightconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINHEIGHTCONSTRAINTUOMCODE() {
            return minheightconstraintuomcode;
        }

        /**
         * Sets the value of the minheightconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINHEIGHTCONSTRAINTUOMCODE(String value) {
            this.minheightconstraintuomcode = value;
        }

        /**
         * Gets the value of the minheightconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINHEIGHTCONSTRAINTBASE() {
            return minheightconstraintbase;
        }

        /**
         * Sets the value of the minheightconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINHEIGHTCONSTRAINTBASE(String value) {
            this.minheightconstraintbase = value;
        }

        /**
         * Gets the value of the maxheightconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXHEIGHTCONSTRAINT() {
            return maxheightconstraint;
        }

        /**
         * Sets the value of the maxheightconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXHEIGHTCONSTRAINT(String value) {
            this.maxheightconstraint = value;
        }

        /**
         * Gets the value of the maxheightconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXHEIGHTCONSTRAINTUOMCODE() {
            return maxheightconstraintuomcode;
        }

        /**
         * Sets the value of the maxheightconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXHEIGHTCONSTRAINTUOMCODE(String value) {
            this.maxheightconstraintuomcode = value;
        }

        /**
         * Gets the value of the maxheightconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXHEIGHTCONSTRAINTBASE() {
            return maxheightconstraintbase;
        }

        /**
         * Sets the value of the maxheightconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXHEIGHTCONSTRAINTBASE(String value) {
            this.maxheightconstraintbase = value;
        }

        /**
         * Gets the value of the mingirthconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINGIRTHCONSTRAINT() {
            return mingirthconstraint;
        }

        /**
         * Sets the value of the mingirthconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINGIRTHCONSTRAINT(String value) {
            this.mingirthconstraint = value;
        }

        /**
         * Gets the value of the mingirthconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINGIRTHCONSTRAINTUOMCODE() {
            return mingirthconstraintuomcode;
        }

        /**
         * Sets the value of the mingirthconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINGIRTHCONSTRAINTUOMCODE(String value) {
            this.mingirthconstraintuomcode = value;
        }

        /**
         * Gets the value of the mingirthconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINGIRTHCONSTRAINTBASE() {
            return mingirthconstraintbase;
        }

        /**
         * Sets the value of the mingirthconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINGIRTHCONSTRAINTBASE(String value) {
            this.mingirthconstraintbase = value;
        }

        /**
         * Gets the value of the maxgirthconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXGIRTHCONSTRAINT() {
            return maxgirthconstraint;
        }

        /**
         * Sets the value of the maxgirthconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXGIRTHCONSTRAINT(String value) {
            this.maxgirthconstraint = value;
        }

        /**
         * Gets the value of the maxgirthconstraintuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXGIRTHCONSTRAINTUOMCODE() {
            return maxgirthconstraintuomcode;
        }

        /**
         * Sets the value of the maxgirthconstraintuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXGIRTHCONSTRAINTUOMCODE(String value) {
            this.maxgirthconstraintuomcode = value;
        }

        /**
         * Gets the value of the maxgirthconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXGIRTHCONSTRAINTBASE() {
            return maxgirthconstraintbase;
        }

        /**
         * Sets the value of the maxgirthconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXGIRTHCONSTRAINTBASE(String value) {
            this.maxgirthconstraintbase = value;
        }

        /**
         * Gets the value of the weightbreakprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWEIGHTBREAKPROFILEGID() {
            return weightbreakprofilegid;
        }

        /**
         * Sets the value of the weightbreakprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWEIGHTBREAKPROFILEGID(String value) {
            this.weightbreakprofilegid = value;
        }

        /**
         * Gets the value of the multistopcostmethod property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMULTISTOPCOSTMETHOD() {
            return multistopcostmethod;
        }

        /**
         * Sets the value of the multistopcostmethod property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMULTISTOPCOSTMETHOD(String value) {
            this.multistopcostmethod = value;
        }

        /**
         * Gets the value of the weightuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWEIGHTUOMCODE() {
            return weightuomcode;
        }

        /**
         * Sets the value of the weightuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWEIGHTUOMCODE(String value) {
            this.weightuomcode = value;
        }

        /**
         * Gets the value of the volumeuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVOLUMEUOMCODE() {
            return volumeuomcode;
        }

        /**
         * Sets the value of the volumeuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVOLUMEUOMCODE(String value) {
            this.volumeuomcode = value;
        }

        /**
         * Gets the value of the distanceuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDISTANCEUOMCODE() {
            return distanceuomcode;
        }

        /**
         * Sets the value of the distanceuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDISTANCEUOMCODE(String value) {
            this.distanceuomcode = value;
        }

        /**
         * Gets the value of the dimratefactorgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDIMRATEFACTORGID() {
            return dimratefactorgid;
        }

        /**
         * Sets the value of the dimratefactorgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDIMRATEFACTORGID(String value) {
            this.dimratefactorgid = value;
        }

        /**
         * Gets the value of the perspective property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPERSPECTIVE() {
            return perspective;
        }

        /**
         * Sets the value of the perspective property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPERSPECTIVE(String value) {
            this.perspective = value;
        }

        /**
         * Gets the value of the corporationprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCORPORATIONPROFILEGID() {
            return corporationprofilegid;
        }

        /**
         * Sets the value of the corporationprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCORPORATIONPROFILEGID(String value) {
            this.corporationprofilegid = value;
        }

        /**
         * Gets the value of the capacitygroupgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCAPACITYGROUPGID() {
            return capacitygroupgid;
        }

        /**
         * Sets the value of the capacitygroupgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCAPACITYGROUPGID(String value) {
            this.capacitygroupgid = value;
        }

        /**
         * Gets the value of the cmmaxnumshipments property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMMAXNUMSHIPMENTS() {
            return cmmaxnumshipments;
        }

        /**
         * Sets the value of the cmmaxnumshipments property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMMAXNUMSHIPMENTS(String value) {
            this.cmmaxnumshipments = value;
        }

        /**
         * Gets the value of the cmissameequip property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMISSAMEEQUIP() {
            return cmissameequip;
        }

        /**
         * Sets the value of the cmissameequip property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMISSAMEEQUIP(String value) {
            this.cmissameequip = value;
        }

        /**
         * Gets the value of the cmprevshipmentpercent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMPREVSHIPMENTPERCENT() {
            return cmprevshipmentpercent;
        }

        /**
         * Sets the value of the cmprevshipmentpercent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMPREVSHIPMENTPERCENT(String value) {
            this.cmprevshipmentpercent = value;
        }

        /**
         * Gets the value of the cmispercentofdistance property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCMISPERCENTOFDISTANCE() {
            return cmispercentofdistance;
        }

        /**
         * Sets the value of the cmispercentofdistance property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCMISPERCENTOFDISTANCE(String value) {
            this.cmispercentofdistance = value;
        }

        /**
         * Gets the value of the tariffrefnumqualifier property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREFNUMQUALIFIER() {
            return tariffrefnumqualifier;
        }

        /**
         * Sets the value of the tariffrefnumqualifier property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREFNUMQUALIFIER(String value) {
            this.tariffrefnumqualifier = value;
        }

        /**
         * Gets the value of the tariffrefnum property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREFNUM() {
            return tariffrefnum;
        }

        /**
         * Sets the value of the tariffrefnum property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREFNUM(String value) {
            this.tariffrefnum = value;
        }

        /**
         * Gets the value of the tariffpubauthority property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFPUBAUTHORITY() {
            return tariffpubauthority;
        }

        /**
         * Sets the value of the tariffpubauthority property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFPUBAUTHORITY(String value) {
            this.tariffpubauthority = value;
        }

        /**
         * Gets the value of the tariffregagencycode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREGAGENCYCODE() {
            return tariffregagencycode;
        }

        /**
         * Sets the value of the tariffregagencycode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREGAGENCYCODE(String value) {
            this.tariffregagencycode = value;
        }

        /**
         * Gets the value of the tariffagencycode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFAGENCYCODE() {
            return tariffagencycode;
        }

        /**
         * Sets the value of the tariffagencycode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFAGENCYCODE(String value) {
            this.tariffagencycode = value;
        }

        /**
         * Gets the value of the tariffissuingcarrierid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFISSUINGCARRIERID() {
            return tariffissuingcarrierid;
        }

        /**
         * Sets the value of the tariffissuingcarrierid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFISSUINGCARRIERID(String value) {
            this.tariffissuingcarrierid = value;
        }

        /**
         * Gets the value of the tariffrefnumsuffix property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFREFNUMSUFFIX() {
            return tariffrefnumsuffix;
        }

        /**
         * Sets the value of the tariffrefnumsuffix property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFREFNUMSUFFIX(String value) {
            this.tariffrefnumsuffix = value;
        }

        /**
         * Gets the value of the tariffsupplementid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFSUPPLEMENTID() {
            return tariffsupplementid;
        }

        /**
         * Sets the value of the tariffsupplementid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFSUPPLEMENTID(String value) {
            this.tariffsupplementid = value;
        }

        /**
         * Gets the value of the tariffeffectivedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTARIFFEFFECTIVEDATE() {
            return tariffeffectivedate;
        }

        /**
         * Sets the value of the tariffeffectivedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTARIFFEFFECTIVEDATE(String value) {
            this.tariffeffectivedate = value;
        }

        /**
         * Gets the value of the allowuncostedlineitems property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getALLOWUNCOSTEDLINEITEMS() {
            return allowuncostedlineitems;
        }

        /**
         * Sets the value of the allowuncostedlineitems property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setALLOWUNCOSTEDLINEITEMS(String value) {
            this.allowuncostedlineitems = value;
        }

        /**
         * Gets the value of the railintermodalplangid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRAILINTERMODALPLANGID() {
            return railintermodalplangid;
        }

        /**
         * Sets the value of the railintermodalplangid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRAILINTERMODALPLANGID(String value) {
            this.railintermodalplangid = value;
        }

        /**
         * Gets the value of the customerratecode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUSTOMERRATECODE() {
            return customerratecode;
        }

        /**
         * Sets the value of the customerratecode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUSTOMERRATECODE(String value) {
            this.customerratecode = value;
        }

        /**
         * Gets the value of the cofctofc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOFCTOFC() {
            return cofctofc;
        }

        /**
         * Sets the value of the cofctofc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOFCTOFC(String value) {
            this.cofctofc = value;
        }

        /**
         * Gets the value of the isactive property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISACTIVE() {
            return isactive;
        }

        /**
         * Sets the value of the isactive property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISACTIVE(String value) {
            this.isactive = value;
        }

        /**
         * Gets the value of the handlesunknownshipper property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHANDLESUNKNOWNSHIPPER() {
            return handlesunknownshipper;
        }

        /**
         * Sets the value of the handlesunknownshipper property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHANDLESUNKNOWNSHIPPER(String value) {
            this.handlesunknownshipper = value;
        }

        /**
         * Gets the value of the usestimebasedrates property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSESTIMEBASEDRATES() {
            return usestimebasedrates;
        }

        /**
         * Sets the value of the usestimebasedrates property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSESTIMEBASEDRATES(String value) {
            this.usestimebasedrates = value;
        }

        /**
         * Gets the value of the expiremarkid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIREMARKID() {
            return expiremarkid;
        }

        /**
         * Sets the value of the expiremarkid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIREMARKID(String value) {
            this.expiremarkid = value;
        }

        /**
         * Gets the value of the mindistanceconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINDISTANCECONSTRAINT() {
            return mindistanceconstraint;
        }

        /**
         * Sets the value of the mindistanceconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINDISTANCECONSTRAINT(String value) {
            this.mindistanceconstraint = value;
        }

        /**
         * Gets the value of the mindistanceconstrntuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINDISTANCECONSTRNTUOMCODE() {
            return mindistanceconstrntuomcode;
        }

        /**
         * Sets the value of the mindistanceconstrntuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINDISTANCECONSTRNTUOMCODE(String value) {
            this.mindistanceconstrntuomcode = value;
        }

        /**
         * Gets the value of the mindistanceconstraintbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINDISTANCECONSTRAINTBASE() {
            return mindistanceconstraintbase;
        }

        /**
         * Sets the value of the mindistanceconstraintbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINDISTANCECONSTRAINTBASE(String value) {
            this.mindistanceconstraintbase = value;
        }

        /**
         * Gets the value of the regiongroupgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getREGIONGROUPGID() {
            return regiongroupgid;
        }

        /**
         * Sets the value of the regiongroupgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setREGIONGROUPGID(String value) {
            this.regiongroupgid = value;
        }

        /**
         * Gets the value of the isdepotapplicable property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISDEPOTAPPLICABLE() {
            return isdepotapplicable;
        }

        /**
         * Sets the value of the isdepotapplicable property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISDEPOTAPPLICABLE(String value) {
            this.isdepotapplicable = value;
        }

        /**
         * Gets the value of the recalculatecost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRECALCULATECOST() {
            return recalculatecost;
        }

        /**
         * Sets the value of the recalculatecost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRECALCULATECOST(String value) {
            this.recalculatecost = value;
        }

        /**
         * Gets the value of the trackcapacityusage property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTRACKCAPACITYUSAGE() {
            return trackcapacityusage;
        }

        /**
         * Sets the value of the trackcapacityusage property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTRACKCAPACITYUSAGE(String value) {
            this.trackcapacityusage = value;
        }

        /**
         * Gets the value of the roundingtype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGTYPE() {
            return roundingtype;
        }

        /**
         * Sets the value of the roundingtype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGTYPE(String value) {
            this.roundingtype = value;
        }

        /**
         * Gets the value of the roundinginterval property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGINTERVAL() {
            return roundinginterval;
        }

        /**
         * Sets the value of the roundinginterval property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGINTERVAL(String value) {
            this.roundinginterval = value;
        }

        /**
         * Gets the value of the roundingfieldslevel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGFIELDSLEVEL() {
            return roundingfieldslevel;
        }

        /**
         * Sets the value of the roundingfieldslevel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGFIELDSLEVEL(String value) {
            this.roundingfieldslevel = value;
        }

        /**
         * Gets the value of the roundingapplication property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGAPPLICATION() {
            return roundingapplication;
        }

        /**
         * Sets the value of the roundingapplication property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGAPPLICATION(String value) {
            this.roundingapplication = value;
        }

        /**
         * Gets the value of the iscontractrate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISCONTRACTRATE() {
            return iscontractrate;
        }

        /**
         * Sets the value of the iscontractrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISCONTRACTRATE(String value) {
            this.iscontractrate = value;
        }

        /**
         * Gets the value of the isdirectonlyrate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISDIRECTONLYRATE() {
            return isdirectonlyrate;
        }

        /**
         * Sets the value of the isdirectonlyrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISDIRECTONLYRATE(String value) {
            this.isdirectonlyrate = value;
        }

        /**
         * Gets the value of the hazardousratetype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHAZARDOUSRATETYPE() {
            return hazardousratetype;
        }

        /**
         * Sets the value of the hazardousratetype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHAZARDOUSRATETYPE(String value) {
            this.hazardousratetype = value;
        }

        /**
         * Gets the value of the usetactasdisplayrate1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSETACTASDISPLAYRATE1() {
            return usetactasdisplayrate1;
        }

        /**
         * Sets the value of the usetactasdisplayrate1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSETACTASDISPLAYRATE1(String value) {
            this.usetactasdisplayrate1 = value;
        }

        /**
         * Gets the value of the usetactasdisplayrate2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSETACTASDISPLAYRATE2() {
            return usetactasdisplayrate2;
        }

        /**
         * Sets the value of the usetactasdisplayrate2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSETACTASDISPLAYRATE2(String value) {
            this.usetactasdisplayrate2 = value;
        }

        /**
         * Gets the value of the usetactasdisplayrate3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUSETACTASDISPLAYRATE3() {
            return usetactasdisplayrate3;
        }

        /**
         * Sets the value of the usetactasdisplayrate3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUSETACTASDISPLAYRATE3(String value) {
            this.usetactasdisplayrate3 = value;
        }

        /**
         * Gets the value of the domainprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINPROFILEGID() {
            return domainprofilegid;
        }

        /**
         * Sets the value of the domainprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINPROFILEGID(String value) {
            this.domainprofilegid = value;
        }

        /**
         * Gets the value of the isrouteexecutionrate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISROUTEEXECUTIONRATE() {
            return isrouteexecutionrate;
        }

        /**
         * Sets the value of the isrouteexecutionrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISROUTEEXECUTIONRATE(String value) {
            this.isrouteexecutionrate = value;
        }

        /**
         * Gets the value of the mintenderleadtime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINTENDERLEADTIME() {
            return mintenderleadtime;
        }

        /**
         * Sets the value of the mintenderleadtime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINTENDERLEADTIME(String value) {
            this.mintenderleadtime = value;
        }

        /**
         * Gets the value of the mintenderleadtimeuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINTENDERLEADTIMEUOMCODE() {
            return mintenderleadtimeuomcode;
        }

        /**
         * Sets the value of the mintenderleadtimeuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINTENDERLEADTIMEUOMCODE(String value) {
            this.mintenderleadtimeuomcode = value;
        }

        /**
         * Gets the value of the mintenderleadtimebase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINTENDERLEADTIMEBASE() {
            return mintenderleadtimebase;
        }

        /**
         * Sets the value of the mintenderleadtimebase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINTENDERLEADTIMEBASE(String value) {
            this.mintenderleadtimebase = value;
        }

        /**
         * Gets the value of the maxtenderleadtime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXTENDERLEADTIME() {
            return maxtenderleadtime;
        }

        /**
         * Sets the value of the maxtenderleadtime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXTENDERLEADTIME(String value) {
            this.maxtenderleadtime = value;
        }

        /**
         * Gets the value of the maxtenderleadtimeuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXTENDERLEADTIMEUOMCODE() {
            return maxtenderleadtimeuomcode;
        }

        /**
         * Sets the value of the maxtenderleadtimeuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXTENDERLEADTIMEUOMCODE(String value) {
            this.maxtenderleadtimeuomcode = value;
        }

        /**
         * Gets the value of the maxtenderleadtimebase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXTENDERLEADTIMEBASE() {
            return maxtenderleadtimebase;
        }

        /**
         * Sets the value of the maxtenderleadtimebase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXTENDERLEADTIMEBASE(String value) {
            this.maxtenderleadtimebase = value;
        }

        /**
         * Gets the value of the packageweightmin property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGEWEIGHTMIN() {
            return packageweightmin;
        }

        /**
         * Sets the value of the packageweightmin property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGEWEIGHTMIN(String value) {
            this.packageweightmin = value;
        }

        /**
         * Gets the value of the packageweightminuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGEWEIGHTMINUOMCODE() {
            return packageweightminuomcode;
        }

        /**
         * Sets the value of the packageweightminuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGEWEIGHTMINUOMCODE(String value) {
            this.packageweightminuomcode = value;
        }

        /**
         * Gets the value of the packageweightminbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGEWEIGHTMINBASE() {
            return packageweightminbase;
        }

        /**
         * Sets the value of the packageweightminbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGEWEIGHTMINBASE(String value) {
            this.packageweightminbase = value;
        }

        /**
         * Gets the value of the maxaveragepkgweight property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXAVERAGEPKGWEIGHT() {
            return maxaveragepkgweight;
        }

        /**
         * Sets the value of the maxaveragepkgweight property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXAVERAGEPKGWEIGHT(String value) {
            this.maxaveragepkgweight = value;
        }

        /**
         * Gets the value of the maxaveragepkgweighuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXAVERAGEPKGWEIGHUOMCODE() {
            return maxaveragepkgweighuomcode;
        }

        /**
         * Sets the value of the maxaveragepkgweighuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXAVERAGEPKGWEIGHUOMCODE(String value) {
            this.maxaveragepkgweighuomcode = value;
        }

        /**
         * Gets the value of the maxaveragepkgweighbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXAVERAGEPKGWEIGHBASE() {
            return maxaveragepkgweighbase;
        }

        /**
         * Sets the value of the maxaveragepkgweighbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXAVERAGEPKGWEIGHBASE(String value) {
            this.maxaveragepkgweighbase = value;
        }

        /**
         * Gets the value of the packagecountmethod property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPACKAGECOUNTMETHOD() {
            return packagecountmethod;
        }

        /**
         * Sets the value of the packagecountmethod property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPACKAGECOUNTMETHOD(String value) {
            this.packagecountmethod = value;
        }

        /**
         * Gets the value of the maxshipunitlinepkgweight property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITLINEPKGWEIGHT() {
            return maxshipunitlinepkgweight;
        }

        /**
         * Sets the value of the maxshipunitlinepkgweight property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITLINEPKGWEIGHT(String value) {
            this.maxshipunitlinepkgweight = value;
        }

        /**
         * Gets the value of the maxshipunitlinepkguomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITLINEPKGUOMCODE() {
            return maxshipunitlinepkguomcode;
        }

        /**
         * Sets the value of the maxshipunitlinepkguomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITLINEPKGUOMCODE(String value) {
            this.maxshipunitlinepkguomcode = value;
        }

        /**
         * Gets the value of the maxshipunitlinepkgbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXSHIPUNITLINEPKGBASE() {
            return maxshipunitlinepkgbase;
        }

        /**
         * Sets the value of the maxshipunitlinepkgbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXSHIPUNITLINEPKGBASE(String value) {
            this.maxshipunitlinepkgbase = value;
        }

        /**
         * Gets the value of the maxnumofshipmentsegments property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXNUMOFSHIPMENTSEGMENTS() {
            return maxnumofshipmentsegments;
        }

        /**
         * Sets the value of the maxnumofshipmentsegments property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXNUMOFSHIPMENTSEGMENTS(String value) {
            this.maxnumofshipmentsegments = value;
        }

        /**
         * Gets the value of the rotimeperioddefgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROTIMEPERIODDEFGID() {
            return rotimeperioddefgid;
        }

        /**
         * Sets the value of the rotimeperioddefgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROTIMEPERIODDEFGID(String value) {
            this.rotimeperioddefgid = value;
        }

        /**
         * Gets the value of the extrefieldsetgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXTREFIELDSETGID() {
            return extrefieldsetgid;
        }

        /**
         * Sets the value of the extrefieldsetgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXTREFIELDSETGID(String value) {
            this.extrefieldsetgid = value;
        }

        /**
         * Gets the value of the insertuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Sets the value of the insertuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Gets the value of the insertdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Sets the value of the insertdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Gets the value of the updateuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Sets the value of the updateuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Gets the value of the updatedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Sets the value of the updatedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Gets the value of the rateofferingstops property.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS getRATEOFFERINGSTOPS() {
            return rateofferingstops;
        }

        /**
         * Sets the value of the rateofferingstops property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS }
         *     
         */
        public void setRATEOFFERINGSTOPS(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS value) {
            this.rateofferingstops = value;
        }

        /**
         * Gets the value of the rateofferingaccessorial property.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL getRATEOFFERINGACCESSORIAL() {
            return rateofferingaccessorial;
        }

        /**
         * Sets the value of the rateofferingaccessorial property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL }
         *     
         */
        public void setRATEOFFERINGACCESSORIAL(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL value) {
            this.rateofferingaccessorial = value;
        }

        /**
         * Gets the value of the rateofferingcomment property.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT getRATEOFFERINGCOMMENT() {
            return rateofferingcomment;
        }

        /**
         * Sets the value of the rateofferingcomment property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT }
         *     
         */
        public void setRATEOFFERINGCOMMENT(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT value) {
            this.rateofferingcomment = value;
        }

        /**
         * Gets the value of the raterulesandterms property.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS getRATERULESANDTERMS() {
            return raterulesandterms;
        }

        /**
         * Sets the value of the raterulesandterms property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS }
         *     
         */
        public void setRATERULESANDTERMS(RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS value) {
            this.raterulesandterms = value;
        }

        /**
         * Gets the value of the rospecialserviceaccessorial property.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL getROSPECIALSERVICEACCESSORIAL() {
            return rospecialserviceaccessorial;
        }

        /**
         * Sets the value of the rospecialserviceaccessorial property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL }
         *     
         */
        public void setROSPECIALSERVICEACCESSORIAL(RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL value) {
            this.rospecialserviceaccessorial = value;
        }

        /**
         * Gets the value of the rateofferinginvparty property.
         * 
         * @return
         *     possible object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY }
         *     
         */
        public RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY getRATEOFFERINGINVPARTY() {
            return rateofferinginvparty;
        }

        /**
         * Sets the value of the rateofferinginvparty property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY }
         *     
         */
        public void setRATEOFFERINGINVPARTY(RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY value) {
            this.rateofferinginvparty = value;
        }

        /**
         * Gets the value of the attribute1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE1() {
            return attribute1;
        }

        /**
         * Sets the value of the attribute1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE1(String value) {
            this.attribute1 = value;
        }

        /**
         * Gets the value of the attribute2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE2() {
            return attribute2;
        }

        /**
         * Sets the value of the attribute2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE2(String value) {
            this.attribute2 = value;
        }

        /**
         * Gets the value of the attribute3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE3() {
            return attribute3;
        }

        /**
         * Sets the value of the attribute3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE3(String value) {
            this.attribute3 = value;
        }

        /**
         * Gets the value of the attribute4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE4() {
            return attribute4;
        }

        /**
         * Sets the value of the attribute4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE4(String value) {
            this.attribute4 = value;
        }

        /**
         * Gets the value of the attribute5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE5() {
            return attribute5;
        }

        /**
         * Sets the value of the attribute5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE5(String value) {
            this.attribute5 = value;
        }

        /**
         * Gets the value of the attribute6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE6() {
            return attribute6;
        }

        /**
         * Sets the value of the attribute6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE6(String value) {
            this.attribute6 = value;
        }

        /**
         * Gets the value of the attribute7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE7() {
            return attribute7;
        }

        /**
         * Sets the value of the attribute7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE7(String value) {
            this.attribute7 = value;
        }

        /**
         * Gets the value of the attribute8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE8() {
            return attribute8;
        }

        /**
         * Sets the value of the attribute8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE8(String value) {
            this.attribute8 = value;
        }

        /**
         * Gets the value of the attribute9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE9() {
            return attribute9;
        }

        /**
         * Sets the value of the attribute9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE9(String value) {
            this.attribute9 = value;
        }

        /**
         * Gets the value of the attribute10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE10() {
            return attribute10;
        }

        /**
         * Sets the value of the attribute10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE10(String value) {
            this.attribute10 = value;
        }

        /**
         * Gets the value of the attribute11 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE11() {
            return attribute11;
        }

        /**
         * Sets the value of the attribute11 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE11(String value) {
            this.attribute11 = value;
        }

        /**
         * Gets the value of the attribute12 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE12() {
            return attribute12;
        }

        /**
         * Sets the value of the attribute12 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE12(String value) {
            this.attribute12 = value;
        }

        /**
         * Gets the value of the attribute13 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE13() {
            return attribute13;
        }

        /**
         * Sets the value of the attribute13 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE13(String value) {
            this.attribute13 = value;
        }

        /**
         * Gets the value of the attribute14 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE14() {
            return attribute14;
        }

        /**
         * Sets the value of the attribute14 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE14(String value) {
            this.attribute14 = value;
        }

        /**
         * Gets the value of the attribute15 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE15() {
            return attribute15;
        }

        /**
         * Sets the value of the attribute15 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE15(String value) {
            this.attribute15 = value;
        }

        /**
         * Gets the value of the attribute16 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE16() {
            return attribute16;
        }

        /**
         * Sets the value of the attribute16 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE16(String value) {
            this.attribute16 = value;
        }

        /**
         * Gets the value of the attribute17 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE17() {
            return attribute17;
        }

        /**
         * Sets the value of the attribute17 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE17(String value) {
            this.attribute17 = value;
        }

        /**
         * Gets the value of the attribute18 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE18() {
            return attribute18;
        }

        /**
         * Sets the value of the attribute18 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE18(String value) {
            this.attribute18 = value;
        }

        /**
         * Gets the value of the attribute19 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE19() {
            return attribute19;
        }

        /**
         * Sets the value of the attribute19 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE19(String value) {
            this.attribute19 = value;
        }

        /**
         * Gets the value of the attribute20 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE20() {
            return attribute20;
        }

        /**
         * Sets the value of the attribute20 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE20(String value) {
            this.attribute20 = value;
        }

        /**
         * Gets the value of the attributenumber1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER1() {
            return attributenumber1;
        }

        /**
         * Sets the value of the attributenumber1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER1(String value) {
            this.attributenumber1 = value;
        }

        /**
         * Gets the value of the attributenumber2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER2() {
            return attributenumber2;
        }

        /**
         * Sets the value of the attributenumber2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER2(String value) {
            this.attributenumber2 = value;
        }

        /**
         * Gets the value of the attributenumber3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER3() {
            return attributenumber3;
        }

        /**
         * Sets the value of the attributenumber3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER3(String value) {
            this.attributenumber3 = value;
        }

        /**
         * Gets the value of the attributenumber4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER4() {
            return attributenumber4;
        }

        /**
         * Sets the value of the attributenumber4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER4(String value) {
            this.attributenumber4 = value;
        }

        /**
         * Gets the value of the attributenumber5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER5() {
            return attributenumber5;
        }

        /**
         * Sets the value of the attributenumber5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER5(String value) {
            this.attributenumber5 = value;
        }

        /**
         * Gets the value of the attributenumber6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER6() {
            return attributenumber6;
        }

        /**
         * Sets the value of the attributenumber6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER6(String value) {
            this.attributenumber6 = value;
        }

        /**
         * Gets the value of the attributenumber7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER7() {
            return attributenumber7;
        }

        /**
         * Sets the value of the attributenumber7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER7(String value) {
            this.attributenumber7 = value;
        }

        /**
         * Gets the value of the attributenumber8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER8() {
            return attributenumber8;
        }

        /**
         * Sets the value of the attributenumber8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER8(String value) {
            this.attributenumber8 = value;
        }

        /**
         * Gets the value of the attributenumber9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER9() {
            return attributenumber9;
        }

        /**
         * Sets the value of the attributenumber9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER9(String value) {
            this.attributenumber9 = value;
        }

        /**
         * Gets the value of the attributenumber10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER10() {
            return attributenumber10;
        }

        /**
         * Sets the value of the attributenumber10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER10(String value) {
            this.attributenumber10 = value;
        }

        /**
         * Gets the value of the attributedate1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE1() {
            return attributedate1;
        }

        /**
         * Sets the value of the attributedate1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE1(String value) {
            this.attributedate1 = value;
        }

        /**
         * Gets the value of the attributedate2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE2() {
            return attributedate2;
        }

        /**
         * Sets the value of the attributedate2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE2(String value) {
            this.attributedate2 = value;
        }

        /**
         * Gets the value of the attributedate3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE3() {
            return attributedate3;
        }

        /**
         * Sets the value of the attributedate3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE3(String value) {
            this.attributedate3 = value;
        }

        /**
         * Gets the value of the attributedate4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE4() {
            return attributedate4;
        }

        /**
         * Sets the value of the attributedate4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE4(String value) {
            this.attributedate4 = value;
        }

        /**
         * Gets the value of the attributedate5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE5() {
            return attributedate5;
        }

        /**
         * Sets the value of the attributedate5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE5(String value) {
            this.attributedate5 = value;
        }

        /**
         * Gets the value of the attributedate6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE6() {
            return attributedate6;
        }

        /**
         * Sets the value of the attributedate6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE6(String value) {
            this.attributedate6 = value;
        }

        /**
         * Gets the value of the attributedate7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE7() {
            return attributedate7;
        }

        /**
         * Sets the value of the attributedate7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE7(String value) {
            this.attributedate7 = value;
        }

        /**
         * Gets the value of the attributedate8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE8() {
            return attributedate8;
        }

        /**
         * Sets the value of the attributedate8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE8(String value) {
            this.attributedate8 = value;
        }

        /**
         * Gets the value of the attributedate9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE9() {
            return attributedate9;
        }

        /**
         * Sets the value of the attributedate9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE9(String value) {
            this.attributedate9 = value;
        }

        /**
         * Gets the value of the attributedate10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE10() {
            return attributedate10;
        }

        /**
         * Sets the value of the attributedate10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE10(String value) {
            this.attributedate10 = value;
        }

        /**
         * Gets the value of the num property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Sets the value of the num property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="RATE_OFFERING_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferingaccessorialrow"
        })
        public static class RATEOFFERINGACCESSORIAL {

            @XmlElement(name = "RATE_OFFERING_ACCESSORIAL_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW> rateofferingaccessorialrow;

            /**
             * Gets the value of the rateofferingaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferingaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW> getRATEOFFERINGACCESSORIALROW() {
                if (rateofferingaccessorialrow == null) {
                    rateofferingaccessorialrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGACCESSORIAL.RATEOFFERINGACCESSORIALROW>();
                }
                return this.rateofferingaccessorialrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rateofferinggid",
                "accessorialcodegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class RATEOFFERINGACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_OFFERING_GID", required = true)
                protected String rateofferinggid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the accessorialcostgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Sets the value of the accessorialcostgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Gets the value of the rateofferinggid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Sets the value of the rateofferinggid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Gets the value of the accessorialcodegid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Sets the value of the accessorialcodegid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the accessorialcost property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Sets the value of the accessorialcost property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
         *         &lt;element name="RATE_OFFERING_COMMENT_ROW" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferingcommentrow"
        })
        public static class RATEOFFERINGCOMMENT {

            @XmlElement(name = "RATE_OFFERING_COMMENT_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW> rateofferingcommentrow;

            /**
             * Gets the value of the rateofferingcommentrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferingcommentrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGCOMMENTROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW> getRATEOFFERINGCOMMENTROW() {
                if (rateofferingcommentrow == null) {
                    rateofferingcommentrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGCOMMENT.RATEOFFERINGCOMMENTROW>();
                }
                return this.rateofferingcommentrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="COMMENT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ENTERED_BY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="THE_COMMENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "commentdate",
                "enteredby",
                "thecomment",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEOFFERINGCOMMENTROW {

                @XmlElement(name = "RATE_OFFERING_GID")
                protected String rateofferinggid;
                @XmlElement(name = "COMMENT_DATE")
                protected String commentdate;
                @XmlElement(name = "ENTERED_BY")
                protected String enteredby;
                @XmlElement(name = "THE_COMMENT")
                protected String thecomment;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the rateofferinggid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Sets the value of the rateofferinggid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Gets the value of the commentdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCOMMENTDATE() {
                    return commentdate;
                }

                /**
                 * Sets the value of the commentdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCOMMENTDATE(String value) {
                    this.commentdate = value;
                }

                /**
                 * Gets the value of the enteredby property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getENTEREDBY() {
                    return enteredby;
                }

                /**
                 * Sets the value of the enteredby property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setENTEREDBY(String value) {
                    this.enteredby = value;
                }

                /**
                 * Gets the value of the thecomment property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTHECOMMENT() {
                    return thecomment;
                }

                /**
                 * Sets the value of the thecomment property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTHECOMMENT(String value) {
                    this.thecomment = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="RATE_OFFERING_INV_PARTY_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferinginvpartyrow"
        })
        public static class RATEOFFERINGINVPARTY {

            @XmlElement(name = "RATE_OFFERING_INV_PARTY_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW> rateofferinginvpartyrow;

            /**
             * Gets the value of the rateofferinginvpartyrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferinginvpartyrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGINVPARTYROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW> getRATEOFFERINGINVPARTYROW() {
                if (rateofferinginvpartyrow == null) {
                    rateofferinginvpartyrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGINVPARTY.RATEOFFERINGINVPARTYROW>();
                }
                return this.rateofferinginvpartyrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INVOLVED_PARTY_QUAL_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INVOLVED_PARTY_CONTACT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="COM_METHOD_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "involvedpartyqualgid",
                "involvedpartycontactgid",
                "commethodgid"
            })
            public static class RATEOFFERINGINVPARTYROW {

                @XmlElement(name = "RATE_OFFERING_GID", required = true)
                protected String rateofferinggid;
                @XmlElement(name = "INVOLVED_PARTY_QUAL_GID", required = true)
                protected String involvedpartyqualgid;
                @XmlElement(name = "INVOLVED_PARTY_CONTACT_GID", required = true)
                protected String involvedpartycontactgid;
                @XmlElement(name = "COM_METHOD_GID", required = true)
                protected String commethodgid;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the rateofferinggid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Sets the value of the rateofferinggid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Gets the value of the involvedpartyqualgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINVOLVEDPARTYQUALGID() {
                    return involvedpartyqualgid;
                }

                /**
                 * Sets the value of the involvedpartyqualgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINVOLVEDPARTYQUALGID(String value) {
                    this.involvedpartyqualgid = value;
                }

                /**
                 * Gets the value of the involvedpartycontactgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINVOLVEDPARTYCONTACTGID() {
                    return involvedpartycontactgid;
                }

                /**
                 * Sets the value of the involvedpartycontactgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINVOLVEDPARTYCONTACTGID(String value) {
                    this.involvedpartycontactgid = value;
                }

                /**
                 * Gets the value of the commethodgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCOMMETHODGID() {
                    return commethodgid;
                }

                /**
                 * Sets the value of the commethodgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCOMMETHODGID(String value) {
                    this.commethodgid = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
         *         &lt;element name="RATE_OFFERING_STOPS_ROW" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rateofferingstopsrow"
        })
        public static class RATEOFFERINGSTOPS {

            @XmlElement(name = "RATE_OFFERING_STOPS_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW> rateofferingstopsrow;

            /**
             * Gets the value of the rateofferingstopsrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rateofferingstopsrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEOFFERINGSTOPSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW> getRATEOFFERINGSTOPSROW() {
                if (rateofferingstopsrow == null) {
                    rateofferingstopsrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATEOFFERINGSTOPS.RATEOFFERINGSTOPSROW>();
                }
                return this.rateofferingstopsrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "lowstop",
                "highstop",
                "perstopcost",
                "perstopcostgid",
                "perstopcostbase",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEOFFERINGSTOPSROW {

                @XmlElement(name = "RATE_OFFERING_GID")
                protected String rateofferinggid;
                @XmlElement(name = "LOW_STOP")
                protected String lowstop;
                @XmlElement(name = "HIGH_STOP")
                protected String highstop;
                @XmlElement(name = "PER_STOP_COST")
                protected String perstopcost;
                @XmlElement(name = "PER_STOP_COST_GID")
                protected String perstopcostgid;
                @XmlElement(name = "PER_STOP_COST_BASE")
                protected String perstopcostbase;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the rateofferinggid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Sets the value of the rateofferinggid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Gets the value of the lowstop property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLOWSTOP() {
                    return lowstop;
                }

                /**
                 * Sets the value of the lowstop property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLOWSTOP(String value) {
                    this.lowstop = value;
                }

                /**
                 * Gets the value of the highstop property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHIGHSTOP() {
                    return highstop;
                }

                /**
                 * Sets the value of the highstop property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHIGHSTOP(String value) {
                    this.highstop = value;
                }

                /**
                 * Gets the value of the perstopcost property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOST() {
                    return perstopcost;
                }

                /**
                 * Sets the value of the perstopcost property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOST(String value) {
                    this.perstopcost = value;
                }

                /**
                 * Gets the value of the perstopcostgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTGID() {
                    return perstopcostgid;
                }

                /**
                 * Sets the value of the perstopcostgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTGID(String value) {
                    this.perstopcostgid = value;
                }

                /**
                 * Gets the value of the perstopcostbase property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTBASE() {
                    return perstopcostbase;
                }

                /**
                 * Sets the value of the perstopcostbase property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTBASE(String value) {
                    this.perstopcostbase = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="RATE_RULES_AND_TERMS_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "raterulesandtermsrow"
        })
        public static class RATERULESANDTERMS {

            @XmlElement(name = "RATE_RULES_AND_TERMS_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW> raterulesandtermsrow;

            /**
             * Gets the value of the raterulesandtermsrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the raterulesandtermsrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATERULESANDTERMSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW> getRATERULESANDTERMSROW() {
                if (raterulesandtermsrow == null) {
                    raterulesandtermsrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.RATERULESANDTERMS.RATERULESANDTERMSROW>();
                }
                return this.raterulesandtermsrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RULE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RULE_TITLE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RULE_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rateofferinggid",
                "rulenumber",
                "ruletitle",
                "ruledesc",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATERULESANDTERMSROW {

                @XmlElement(name = "RATE_OFFERING_GID")
                protected String rateofferinggid;
                @XmlElement(name = "RULE_NUMBER")
                protected String rulenumber;
                @XmlElement(name = "RULE_TITLE")
                protected String ruletitle;
                @XmlElement(name = "RULE_DESC")
                protected String ruledesc;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the rateofferinggid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Sets the value of the rateofferinggid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Gets the value of the rulenumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRULENUMBER() {
                    return rulenumber;
                }

                /**
                 * Sets the value of the rulenumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRULENUMBER(String value) {
                    this.rulenumber = value;
                }

                /**
                 * Gets the value of the ruletitle property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRULETITLE() {
                    return ruletitle;
                }

                /**
                 * Sets the value of the ruletitle property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRULETITLE(String value) {
                    this.ruletitle = value;
                }

                /**
                 * Gets the value of the ruledesc property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRULEDESC() {
                    return ruledesc;
                }

                /**
                 * Sets the value of the ruledesc property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRULEDESC(String value) {
                    this.ruledesc = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="RO_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rospecialserviceaccessorialrow"
        })
        public static class ROSPECIALSERVICEACCESSORIAL {

            @XmlElement(name = "RO_SPECIAL_SERVICE_ACCESSORIAL_ROW")
            protected List<RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW> rospecialserviceaccessorialrow;

            /**
             * Gets the value of the rospecialserviceaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rospecialserviceaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getROSPECIALSERVICEACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW }
             * 
             * 
             */
            public List<RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW> getROSPECIALSERVICEACCESSORIALROW() {
                if (rospecialserviceaccessorialrow == null) {
                    rospecialserviceaccessorialrow = new ArrayList<RateOfferingType.RATEOFFERINGROW.ROSPECIALSERVICEACCESSORIAL.ROSPECIALSERVICEACCESSORIALROW>();
                }
                return this.rospecialserviceaccessorialrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="SPECIAL_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rateofferinggid",
                "accessorialcodegid",
                "specialservicegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class ROSPECIALSERVICEACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_OFFERING_GID", required = true)
                protected String rateofferinggid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "SPECIAL_SERVICE_GID", required = true)
                protected String specialservicegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the accessorialcostgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Sets the value of the accessorialcostgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Gets the value of the rateofferinggid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEOFFERINGGID() {
                    return rateofferinggid;
                }

                /**
                 * Sets the value of the rateofferinggid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEOFFERINGGID(String value) {
                    this.rateofferinggid = value;
                }

                /**
                 * Gets the value of the accessorialcodegid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Sets the value of the accessorialcodegid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Gets the value of the specialservicegid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getSPECIALSERVICEGID() {
                    return specialservicegid;
                }

                /**
                 * Sets the value of the specialservicegid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setSPECIALSERVICEGID(String value) {
                    this.specialservicegid = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the accessorialcost property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Sets the value of the accessorialcost property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }

    }

}

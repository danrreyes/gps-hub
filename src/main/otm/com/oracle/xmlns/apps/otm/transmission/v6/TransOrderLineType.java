
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             A TransOrderLine is a request to move a specified quantity of a specified item from a
 *             given ShipFrom location to a given ShipTo location within a given time window.
 * 
 *             The ShipFromLocationRef and ShipToLocationRef elements are marked as optional for updates,
 *             but are requred on inserts.
 *          
 * 
 * <p>Java class for TransOrderLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransOrderLineType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransOrderLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemRefType" minOccurs="0"/&gt;
 *         &lt;element name="InitialItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipToLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemQuantityType" minOccurs="0"/&gt;
 *         &lt;element name="TimeWindow" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowType" minOccurs="0"/&gt;
 *         &lt;element name="ReleasedCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumLayersPerShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QuantityPerLayer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReleasedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="PlanFromLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="PlanFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PlanToLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="PlanToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RoundingInstruction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RoundTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BufferLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OrderLineRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderLineRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ItemAttributes" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemAttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OBLineEquipRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OBLinePackageRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackageRefUnittype" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="AllocationInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AllocationInfoType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransOrderLineType", propOrder = {
    "transOrderLineGid",
    "intSavedQuery",
    "transactionCode",
    "packagedItemRef",
    "initialItemGid",
    "packagedItemSpecRef",
    "packagedItemSpecCount",
    "transportHandlingUnitRef",
    "shipFromLocationRef",
    "shipFromLoadPoint",
    "shipToLocationRef",
    "shipToUnloadPoint",
    "itemQuantity",
    "timeWindow",
    "releasedCount",
    "numLayersPerShipUnit",
    "quantityPerLayer",
    "releasedWeightVolume",
    "planFromLocationGid",
    "planFromLoadPoint",
    "planToLocationGid",
    "planToUnloadPoint",
    "roundingInstruction",
    "roundTo",
    "bufferLocationGid",
    "shipUnitSpecProfileGid",
    "orderLineRefnum",
    "remark",
    "itemAttributes",
    "involvedParty",
    "obLineEquipRefUnit",
    "obLinePackageRefUnit",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "allocationInfo"
})
public class TransOrderLineType {

    @XmlElement(name = "TransOrderLineGid")
    protected GLogXMLGidType transOrderLineGid;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "PackagedItemRef")
    protected PackagedItemRefType packagedItemRef;
    @XmlElement(name = "InitialItemGid")
    protected GLogXMLGidType initialItemGid;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "PackagedItemSpecCount")
    protected String packagedItemSpecCount;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "ShipFromLocationRef")
    protected GLogXMLLocRefType shipFromLocationRef;
    @XmlElement(name = "ShipFromLoadPoint")
    protected String shipFromLoadPoint;
    @XmlElement(name = "ShipToLocationRef")
    protected GLogXMLLocRefType shipToLocationRef;
    @XmlElement(name = "ShipToUnloadPoint")
    protected String shipToUnloadPoint;
    @XmlElement(name = "ItemQuantity")
    protected ItemQuantityType itemQuantity;
    @XmlElement(name = "TimeWindow")
    protected TimeWindowType timeWindow;
    @XmlElement(name = "ReleasedCount")
    protected String releasedCount;
    @XmlElement(name = "NumLayersPerShipUnit")
    protected String numLayersPerShipUnit;
    @XmlElement(name = "QuantityPerLayer")
    protected String quantityPerLayer;
    @XmlElement(name = "ReleasedWeightVolume")
    protected WeightVolumeType releasedWeightVolume;
    @XmlElement(name = "PlanFromLocationGid")
    protected GLogXMLLocGidType planFromLocationGid;
    @XmlElement(name = "PlanFromLoadPoint")
    protected String planFromLoadPoint;
    @XmlElement(name = "PlanToLocationGid")
    protected GLogXMLLocGidType planToLocationGid;
    @XmlElement(name = "PlanToUnloadPoint")
    protected String planToUnloadPoint;
    @XmlElement(name = "RoundingInstruction")
    protected String roundingInstruction;
    @XmlElement(name = "RoundTo")
    protected String roundTo;
    @XmlElement(name = "BufferLocationGid")
    protected GLogXMLLocGidType bufferLocationGid;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "OrderLineRefnum")
    protected List<OrderLineRefnumType> orderLineRefnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ItemAttributes")
    protected List<ItemAttributeType> itemAttributes;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "OBLineEquipRefUnit")
    protected List<EquipmentRefUnitType> obLineEquipRefUnit;
    @XmlElement(name = "OBLinePackageRefUnit")
    protected List<PackageRefUnittype> obLinePackageRefUnit;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "AllocationInfo")
    protected AllocationInfoType allocationInfo;

    /**
     * Gets the value of the transOrderLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderLineGid() {
        return transOrderLineGid;
    }

    /**
     * Sets the value of the transOrderLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderLineGid(GLogXMLGidType value) {
        this.transOrderLineGid = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the packagedItemRef property.
     * 
     * @return
     *     possible object is
     *     {@link PackagedItemRefType }
     *     
     */
    public PackagedItemRefType getPackagedItemRef() {
        return packagedItemRef;
    }

    /**
     * Sets the value of the packagedItemRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagedItemRefType }
     *     
     */
    public void setPackagedItemRef(PackagedItemRefType value) {
        this.packagedItemRef = value;
    }

    /**
     * Gets the value of the initialItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInitialItemGid() {
        return initialItemGid;
    }

    /**
     * Sets the value of the initialItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInitialItemGid(GLogXMLGidType value) {
        this.initialItemGid = value;
    }

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Gets the value of the packagedItemSpecCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemSpecCount() {
        return packagedItemSpecCount;
    }

    /**
     * Sets the value of the packagedItemSpecCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemSpecCount(String value) {
        this.packagedItemSpecCount = value;
    }

    /**
     * Gets the value of the transportHandlingUnitRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Sets the value of the transportHandlingUnitRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Gets the value of the shipFromLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipFromLocationRef() {
        return shipFromLocationRef;
    }

    /**
     * Sets the value of the shipFromLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipFromLocationRef(GLogXMLLocRefType value) {
        this.shipFromLocationRef = value;
    }

    /**
     * Gets the value of the shipFromLoadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipFromLoadPoint() {
        return shipFromLoadPoint;
    }

    /**
     * Sets the value of the shipFromLoadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipFromLoadPoint(String value) {
        this.shipFromLoadPoint = value;
    }

    /**
     * Gets the value of the shipToLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipToLocationRef() {
        return shipToLocationRef;
    }

    /**
     * Sets the value of the shipToLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipToLocationRef(GLogXMLLocRefType value) {
        this.shipToLocationRef = value;
    }

    /**
     * Gets the value of the shipToUnloadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToUnloadPoint() {
        return shipToUnloadPoint;
    }

    /**
     * Sets the value of the shipToUnloadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToUnloadPoint(String value) {
        this.shipToUnloadPoint = value;
    }

    /**
     * Gets the value of the itemQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link ItemQuantityType }
     *     
     */
    public ItemQuantityType getItemQuantity() {
        return itemQuantity;
    }

    /**
     * Sets the value of the itemQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemQuantityType }
     *     
     */
    public void setItemQuantity(ItemQuantityType value) {
        this.itemQuantity = value;
    }

    /**
     * Gets the value of the timeWindow property.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowType }
     *     
     */
    public TimeWindowType getTimeWindow() {
        return timeWindow;
    }

    /**
     * Sets the value of the timeWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowType }
     *     
     */
    public void setTimeWindow(TimeWindowType value) {
        this.timeWindow = value;
    }

    /**
     * Gets the value of the releasedCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleasedCount() {
        return releasedCount;
    }

    /**
     * Sets the value of the releasedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleasedCount(String value) {
        this.releasedCount = value;
    }

    /**
     * Gets the value of the numLayersPerShipUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumLayersPerShipUnit() {
        return numLayersPerShipUnit;
    }

    /**
     * Sets the value of the numLayersPerShipUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumLayersPerShipUnit(String value) {
        this.numLayersPerShipUnit = value;
    }

    /**
     * Gets the value of the quantityPerLayer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantityPerLayer() {
        return quantityPerLayer;
    }

    /**
     * Sets the value of the quantityPerLayer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantityPerLayer(String value) {
        this.quantityPerLayer = value;
    }

    /**
     * Gets the value of the releasedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getReleasedWeightVolume() {
        return releasedWeightVolume;
    }

    /**
     * Sets the value of the releasedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setReleasedWeightVolume(WeightVolumeType value) {
        this.releasedWeightVolume = value;
    }

    /**
     * Gets the value of the planFromLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanFromLocationGid() {
        return planFromLocationGid;
    }

    /**
     * Sets the value of the planFromLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanFromLocationGid(GLogXMLLocGidType value) {
        this.planFromLocationGid = value;
    }

    /**
     * Gets the value of the planFromLoadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFromLoadPoint() {
        return planFromLoadPoint;
    }

    /**
     * Sets the value of the planFromLoadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFromLoadPoint(String value) {
        this.planFromLoadPoint = value;
    }

    /**
     * Gets the value of the planToLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanToLocationGid() {
        return planToLocationGid;
    }

    /**
     * Sets the value of the planToLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanToLocationGid(GLogXMLLocGidType value) {
        this.planToLocationGid = value;
    }

    /**
     * Gets the value of the planToUnloadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanToUnloadPoint() {
        return planToUnloadPoint;
    }

    /**
     * Sets the value of the planToUnloadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanToUnloadPoint(String value) {
        this.planToUnloadPoint = value;
    }

    /**
     * Gets the value of the roundingInstruction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoundingInstruction() {
        return roundingInstruction;
    }

    /**
     * Sets the value of the roundingInstruction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoundingInstruction(String value) {
        this.roundingInstruction = value;
    }

    /**
     * Gets the value of the roundTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoundTo() {
        return roundTo;
    }

    /**
     * Sets the value of the roundTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoundTo(String value) {
        this.roundTo = value;
    }

    /**
     * Gets the value of the bufferLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getBufferLocationGid() {
        return bufferLocationGid;
    }

    /**
     * Sets the value of the bufferLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setBufferLocationGid(GLogXMLLocGidType value) {
        this.bufferLocationGid = value;
    }

    /**
     * Gets the value of the shipUnitSpecProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Sets the value of the shipUnitSpecProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the orderLineRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the orderLineRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrderLineRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrderLineRefnumType }
     * 
     * 
     */
    public List<OrderLineRefnumType> getOrderLineRefnum() {
        if (orderLineRefnum == null) {
            orderLineRefnum = new ArrayList<OrderLineRefnumType>();
        }
        return this.orderLineRefnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the itemAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemAttributeType }
     * 
     * 
     */
    public List<ItemAttributeType> getItemAttributes() {
        if (itemAttributes == null) {
            itemAttributes = new ArrayList<ItemAttributeType>();
        }
        return this.itemAttributes;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the obLineEquipRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the obLineEquipRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOBLineEquipRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentRefUnitType }
     * 
     * 
     */
    public List<EquipmentRefUnitType> getOBLineEquipRefUnit() {
        if (obLineEquipRefUnit == null) {
            obLineEquipRefUnit = new ArrayList<EquipmentRefUnitType>();
        }
        return this.obLineEquipRefUnit;
    }

    /**
     * Gets the value of the obLinePackageRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the obLinePackageRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOBLinePackageRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackageRefUnittype }
     * 
     * 
     */
    public List<PackageRefUnittype> getOBLinePackageRefUnit() {
        if (obLinePackageRefUnit == null) {
            obLinePackageRefUnit = new ArrayList<PackageRefUnittype>();
        }
        return this.obLinePackageRefUnit;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the allocationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AllocationInfoType }
     *     
     */
    public AllocationInfoType getAllocationInfo() {
        return allocationInfo;
    }

    /**
     * Sets the value of the allocationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllocationInfoType }
     *     
     */
    public void setAllocationInfo(AllocationInfoType value) {
        this.allocationInfo = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * The ShipmentHeader2 element contains additional shipment header information.
 *             It is defined only to limit the size of the ShipmentHeader element.
 *          
 * 
 * <p>Java class for ShipmentHeader2Type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentHeader2Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IsAutoMergeConsolidate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ParentLegGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FlightInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IntermediaryCorporationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentAsWork" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FeasibilityCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CheckTimeConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CheckCostConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CheckCapacityConstraint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CmName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CmSequenceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CmCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="CmEmptyDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="CmTotalShipmentNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CmPrevDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="CmNextSourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="WeighCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Rule7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentReleased" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DimWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="ChargeableWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="Rail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailType" minOccurs="0"/&gt;
 *         &lt;element name="SecondaryChargeReference" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SecondaryChargeReferenceType" minOccurs="0"/&gt;
 *         &lt;element name="IsPreload" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsToBeHeld" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AutoGenerateRelease" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DriverAssignBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentAssignBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BulkContMoveGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InTrailerBuild" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastEventGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ScheduleType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WeightUtil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VolumeUtil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipRefUnitUtil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HasAppointments" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="JobGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsCreditNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NFRCRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ConsolGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DutyPaid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsMemoBL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsProfitSplit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAdvancedCharge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AppointmentPriorityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShippingAgentContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LoadConfigEngineTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FixedServiceDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PrevSightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrevSightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="NumEquipmentOrdered" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CombinationEquipmentGrpGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentGroupHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentGroupHeaderType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SrcArbLevelOfServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DestArbLevelOfServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentHeader2Type", propOrder = {
    "isAutoMergeConsolidate",
    "perspective",
    "isPrimary",
    "itineraryGid",
    "parentLegGid",
    "flightInstanceId",
    "intermediaryCorporationGid",
    "shipmentAsWork",
    "feasibilityCodeGid",
    "checkTimeConstraint",
    "checkCostConstraint",
    "checkCapacityConstraint",
    "cmName",
    "cmSequenceNum",
    "cmCost",
    "cmEmptyDistance",
    "cmTotalShipmentNum",
    "cmPrevDestLocationGid",
    "cmNextSourceLocationGid",
    "hazmatModeGid",
    "hazmatRegionGid",
    "weighCode",
    "rule7",
    "shipmentReleased",
    "shipmentTypeGid",
    "dimWeight",
    "chargeableWeight",
    "rail",
    "secondaryChargeReference",
    "isPreload",
    "isToBeHeld",
    "autoGenerateRelease",
    "bulkPlanGid",
    "driverAssignBulkPlanGid",
    "equipmentAssignBulkPlanGid",
    "bulkContMoveGid",
    "inTrailerBuild",
    "lastEventGroupGid",
    "repetitionScheduleGid",
    "scheduleType",
    "weightUtil",
    "volumeUtil",
    "equipRefUnitUtil",
    "hasAppointments",
    "jobGid",
    "isCreditNote",
    "nfrcRuleGid",
    "consolGid",
    "dutyPaid",
    "isMemoBL",
    "isProfitSplit",
    "isAdvancedCharge",
    "appointmentPriorityGid",
    "shippingAgentContactGid",
    "loadConfigEngineTypeGid",
    "fixedServiceDays",
    "sightingLocGid",
    "sightingDate",
    "prevSightingLocGid",
    "prevSightingDate",
    "numEquipmentOrdered",
    "combinationEquipmentGrpGid",
    "shipmentGroupHeader",
    "srcArbLevelOfServiceGid",
    "destArbLevelOfServiceGid"
})
public class ShipmentHeader2Type {

    @XmlElement(name = "IsAutoMergeConsolidate")
    protected String isAutoMergeConsolidate;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "ItineraryGid")
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "ParentLegGid")
    protected GLogXMLGidType parentLegGid;
    @XmlElement(name = "FlightInstanceId")
    protected String flightInstanceId;
    @XmlElement(name = "IntermediaryCorporationGid")
    protected GLogXMLGidType intermediaryCorporationGid;
    @XmlElement(name = "ShipmentAsWork")
    protected String shipmentAsWork;
    @XmlElement(name = "FeasibilityCodeGid")
    protected GLogXMLGidType feasibilityCodeGid;
    @XmlElement(name = "CheckTimeConstraint")
    protected String checkTimeConstraint;
    @XmlElement(name = "CheckCostConstraint")
    protected String checkCostConstraint;
    @XmlElement(name = "CheckCapacityConstraint")
    protected String checkCapacityConstraint;
    @XmlElement(name = "CmName")
    protected String cmName;
    @XmlElement(name = "CmSequenceNum")
    protected String cmSequenceNum;
    @XmlElement(name = "CmCost")
    protected GLogXMLFinancialAmountType cmCost;
    @XmlElement(name = "CmEmptyDistance")
    protected GLogXMLDistanceType cmEmptyDistance;
    @XmlElement(name = "CmTotalShipmentNum")
    protected String cmTotalShipmentNum;
    @XmlElement(name = "CmPrevDestLocationGid")
    protected GLogXMLLocGidType cmPrevDestLocationGid;
    @XmlElement(name = "CmNextSourceLocationGid")
    protected GLogXMLLocGidType cmNextSourceLocationGid;
    @XmlElement(name = "HazmatModeGid")
    protected GLogXMLGidType hazmatModeGid;
    @XmlElement(name = "HazmatRegionGid")
    protected GLogXMLRegionGidType hazmatRegionGid;
    @XmlElement(name = "WeighCode")
    protected String weighCode;
    @XmlElement(name = "Rule7")
    protected String rule7;
    @XmlElement(name = "ShipmentReleased")
    protected String shipmentReleased;
    @XmlElement(name = "ShipmentTypeGid")
    protected GLogXMLGidType shipmentTypeGid;
    @XmlElement(name = "DimWeight")
    protected GLogXMLWeightType dimWeight;
    @XmlElement(name = "ChargeableWeight")
    protected GLogXMLWeightType chargeableWeight;
    @XmlElement(name = "Rail")
    protected RailType rail;
    @XmlElement(name = "SecondaryChargeReference")
    protected SecondaryChargeReferenceType secondaryChargeReference;
    @XmlElement(name = "IsPreload")
    protected String isPreload;
    @XmlElement(name = "IsToBeHeld")
    protected String isToBeHeld;
    @XmlElement(name = "AutoGenerateRelease")
    protected String autoGenerateRelease;
    @XmlElement(name = "BulkPlanGid")
    protected GLogXMLGidType bulkPlanGid;
    @XmlElement(name = "DriverAssignBulkPlanGid")
    protected GLogXMLGidType driverAssignBulkPlanGid;
    @XmlElement(name = "EquipmentAssignBulkPlanGid")
    protected GLogXMLGidType equipmentAssignBulkPlanGid;
    @XmlElement(name = "BulkContMoveGid")
    protected GLogXMLGidType bulkContMoveGid;
    @XmlElement(name = "InTrailerBuild")
    protected String inTrailerBuild;
    @XmlElement(name = "LastEventGroupGid")
    protected GLogXMLGidType lastEventGroupGid;
    @XmlElement(name = "RepetitionScheduleGid")
    protected GLogXMLGidType repetitionScheduleGid;
    @XmlElement(name = "ScheduleType")
    protected String scheduleType;
    @XmlElement(name = "WeightUtil")
    protected String weightUtil;
    @XmlElement(name = "VolumeUtil")
    protected String volumeUtil;
    @XmlElement(name = "EquipRefUnitUtil")
    protected String equipRefUnitUtil;
    @XmlElement(name = "HasAppointments")
    protected String hasAppointments;
    @XmlElement(name = "JobGid")
    protected GLogXMLGidType jobGid;
    @XmlElement(name = "IsCreditNote")
    protected String isCreditNote;
    @XmlElement(name = "NFRCRuleGid")
    protected GLogXMLGidType nfrcRuleGid;
    @XmlElement(name = "ConsolGid")
    protected GLogXMLGidType consolGid;
    @XmlElement(name = "DutyPaid")
    protected String dutyPaid;
    @XmlElement(name = "IsMemoBL")
    protected String isMemoBL;
    @XmlElement(name = "IsProfitSplit")
    protected String isProfitSplit;
    @XmlElement(name = "IsAdvancedCharge")
    protected String isAdvancedCharge;
    @XmlElement(name = "AppointmentPriorityGid")
    protected GLogXMLGidType appointmentPriorityGid;
    @XmlElement(name = "ShippingAgentContactGid")
    protected GLogXMLGidType shippingAgentContactGid;
    @XmlElement(name = "LoadConfigEngineTypeGid")
    protected GLogXMLGidType loadConfigEngineTypeGid;
    @XmlElement(name = "FixedServiceDays")
    protected String fixedServiceDays;
    @XmlElement(name = "SightingLocGid")
    protected GLogXMLGidType sightingLocGid;
    @XmlElement(name = "SightingDate")
    protected GLogDateTimeType sightingDate;
    @XmlElement(name = "PrevSightingLocGid")
    protected GLogXMLGidType prevSightingLocGid;
    @XmlElement(name = "PrevSightingDate")
    protected GLogDateTimeType prevSightingDate;
    @XmlElement(name = "NumEquipmentOrdered")
    protected String numEquipmentOrdered;
    @XmlElement(name = "CombinationEquipmentGrpGid")
    protected GLogXMLGidType combinationEquipmentGrpGid;
    @XmlElement(name = "ShipmentGroupHeader")
    protected List<ShipmentGroupHeaderType> shipmentGroupHeader;
    @XmlElement(name = "SrcArbLevelOfServiceGid")
    protected GLogXMLGidType srcArbLevelOfServiceGid;
    @XmlElement(name = "DestArbLevelOfServiceGid")
    protected GLogXMLGidType destArbLevelOfServiceGid;

    /**
     * Gets the value of the isAutoMergeConsolidate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAutoMergeConsolidate() {
        return isAutoMergeConsolidate;
    }

    /**
     * Sets the value of the isAutoMergeConsolidate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAutoMergeConsolidate(String value) {
        this.isAutoMergeConsolidate = value;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the isPrimary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Sets the value of the isPrimary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Gets the value of the itineraryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Sets the value of the itineraryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Gets the value of the parentLegGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParentLegGid() {
        return parentLegGid;
    }

    /**
     * Sets the value of the parentLegGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParentLegGid(GLogXMLGidType value) {
        this.parentLegGid = value;
    }

    /**
     * Gets the value of the flightInstanceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightInstanceId() {
        return flightInstanceId;
    }

    /**
     * Sets the value of the flightInstanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightInstanceId(String value) {
        this.flightInstanceId = value;
    }

    /**
     * Gets the value of the intermediaryCorporationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntermediaryCorporationGid() {
        return intermediaryCorporationGid;
    }

    /**
     * Sets the value of the intermediaryCorporationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntermediaryCorporationGid(GLogXMLGidType value) {
        this.intermediaryCorporationGid = value;
    }

    /**
     * Gets the value of the shipmentAsWork property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentAsWork() {
        return shipmentAsWork;
    }

    /**
     * Sets the value of the shipmentAsWork property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentAsWork(String value) {
        this.shipmentAsWork = value;
    }

    /**
     * Gets the value of the feasibilityCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFeasibilityCodeGid() {
        return feasibilityCodeGid;
    }

    /**
     * Sets the value of the feasibilityCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFeasibilityCodeGid(GLogXMLGidType value) {
        this.feasibilityCodeGid = value;
    }

    /**
     * Gets the value of the checkTimeConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckTimeConstraint() {
        return checkTimeConstraint;
    }

    /**
     * Sets the value of the checkTimeConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckTimeConstraint(String value) {
        this.checkTimeConstraint = value;
    }

    /**
     * Gets the value of the checkCostConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckCostConstraint() {
        return checkCostConstraint;
    }

    /**
     * Sets the value of the checkCostConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckCostConstraint(String value) {
        this.checkCostConstraint = value;
    }

    /**
     * Gets the value of the checkCapacityConstraint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckCapacityConstraint() {
        return checkCapacityConstraint;
    }

    /**
     * Sets the value of the checkCapacityConstraint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckCapacityConstraint(String value) {
        this.checkCapacityConstraint = value;
    }

    /**
     * Gets the value of the cmName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmName() {
        return cmName;
    }

    /**
     * Sets the value of the cmName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmName(String value) {
        this.cmName = value;
    }

    /**
     * Gets the value of the cmSequenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmSequenceNum() {
        return cmSequenceNum;
    }

    /**
     * Sets the value of the cmSequenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmSequenceNum(String value) {
        this.cmSequenceNum = value;
    }

    /**
     * Gets the value of the cmCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCmCost() {
        return cmCost;
    }

    /**
     * Sets the value of the cmCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCmCost(GLogXMLFinancialAmountType value) {
        this.cmCost = value;
    }

    /**
     * Gets the value of the cmEmptyDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getCmEmptyDistance() {
        return cmEmptyDistance;
    }

    /**
     * Sets the value of the cmEmptyDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setCmEmptyDistance(GLogXMLDistanceType value) {
        this.cmEmptyDistance = value;
    }

    /**
     * Gets the value of the cmTotalShipmentNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmTotalShipmentNum() {
        return cmTotalShipmentNum;
    }

    /**
     * Sets the value of the cmTotalShipmentNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmTotalShipmentNum(String value) {
        this.cmTotalShipmentNum = value;
    }

    /**
     * Gets the value of the cmPrevDestLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getCmPrevDestLocationGid() {
        return cmPrevDestLocationGid;
    }

    /**
     * Sets the value of the cmPrevDestLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setCmPrevDestLocationGid(GLogXMLLocGidType value) {
        this.cmPrevDestLocationGid = value;
    }

    /**
     * Gets the value of the cmNextSourceLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getCmNextSourceLocationGid() {
        return cmNextSourceLocationGid;
    }

    /**
     * Sets the value of the cmNextSourceLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setCmNextSourceLocationGid(GLogXMLLocGidType value) {
        this.cmNextSourceLocationGid = value;
    }

    /**
     * Gets the value of the hazmatModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatModeGid() {
        return hazmatModeGid;
    }

    /**
     * Sets the value of the hazmatModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatModeGid(GLogXMLGidType value) {
        this.hazmatModeGid = value;
    }

    /**
     * Gets the value of the hazmatRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getHazmatRegionGid() {
        return hazmatRegionGid;
    }

    /**
     * Sets the value of the hazmatRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setHazmatRegionGid(GLogXMLRegionGidType value) {
        this.hazmatRegionGid = value;
    }

    /**
     * Gets the value of the weighCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeighCode() {
        return weighCode;
    }

    /**
     * Sets the value of the weighCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeighCode(String value) {
        this.weighCode = value;
    }

    /**
     * Gets the value of the rule7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRule7() {
        return rule7;
    }

    /**
     * Sets the value of the rule7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRule7(String value) {
        this.rule7 = value;
    }

    /**
     * Gets the value of the shipmentReleased property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentReleased() {
        return shipmentReleased;
    }

    /**
     * Sets the value of the shipmentReleased property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentReleased(String value) {
        this.shipmentReleased = value;
    }

    /**
     * Gets the value of the shipmentTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentTypeGid() {
        return shipmentTypeGid;
    }

    /**
     * Sets the value of the shipmentTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentTypeGid(GLogXMLGidType value) {
        this.shipmentTypeGid = value;
    }

    /**
     * Gets the value of the dimWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getDimWeight() {
        return dimWeight;
    }

    /**
     * Sets the value of the dimWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setDimWeight(GLogXMLWeightType value) {
        this.dimWeight = value;
    }

    /**
     * Gets the value of the chargeableWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getChargeableWeight() {
        return chargeableWeight;
    }

    /**
     * Sets the value of the chargeableWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setChargeableWeight(GLogXMLWeightType value) {
        this.chargeableWeight = value;
    }

    /**
     * Gets the value of the rail property.
     * 
     * @return
     *     possible object is
     *     {@link RailType }
     *     
     */
    public RailType getRail() {
        return rail;
    }

    /**
     * Sets the value of the rail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RailType }
     *     
     */
    public void setRail(RailType value) {
        this.rail = value;
    }

    /**
     * Gets the value of the secondaryChargeReference property.
     * 
     * @return
     *     possible object is
     *     {@link SecondaryChargeReferenceType }
     *     
     */
    public SecondaryChargeReferenceType getSecondaryChargeReference() {
        return secondaryChargeReference;
    }

    /**
     * Sets the value of the secondaryChargeReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link SecondaryChargeReferenceType }
     *     
     */
    public void setSecondaryChargeReference(SecondaryChargeReferenceType value) {
        this.secondaryChargeReference = value;
    }

    /**
     * Gets the value of the isPreload property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreload() {
        return isPreload;
    }

    /**
     * Sets the value of the isPreload property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreload(String value) {
        this.isPreload = value;
    }

    /**
     * Gets the value of the isToBeHeld property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsToBeHeld() {
        return isToBeHeld;
    }

    /**
     * Sets the value of the isToBeHeld property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsToBeHeld(String value) {
        this.isToBeHeld = value;
    }

    /**
     * Gets the value of the autoGenerateRelease property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoGenerateRelease() {
        return autoGenerateRelease;
    }

    /**
     * Sets the value of the autoGenerateRelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoGenerateRelease(String value) {
        this.autoGenerateRelease = value;
    }

    /**
     * Gets the value of the bulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkPlanGid() {
        return bulkPlanGid;
    }

    /**
     * Sets the value of the bulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkPlanGid(GLogXMLGidType value) {
        this.bulkPlanGid = value;
    }

    /**
     * Gets the value of the driverAssignBulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverAssignBulkPlanGid() {
        return driverAssignBulkPlanGid;
    }

    /**
     * Sets the value of the driverAssignBulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverAssignBulkPlanGid(GLogXMLGidType value) {
        this.driverAssignBulkPlanGid = value;
    }

    /**
     * Gets the value of the equipmentAssignBulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentAssignBulkPlanGid() {
        return equipmentAssignBulkPlanGid;
    }

    /**
     * Sets the value of the equipmentAssignBulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentAssignBulkPlanGid(GLogXMLGidType value) {
        this.equipmentAssignBulkPlanGid = value;
    }

    /**
     * Gets the value of the bulkContMoveGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkContMoveGid() {
        return bulkContMoveGid;
    }

    /**
     * Sets the value of the bulkContMoveGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkContMoveGid(GLogXMLGidType value) {
        this.bulkContMoveGid = value;
    }

    /**
     * Gets the value of the inTrailerBuild property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInTrailerBuild() {
        return inTrailerBuild;
    }

    /**
     * Sets the value of the inTrailerBuild property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInTrailerBuild(String value) {
        this.inTrailerBuild = value;
    }

    /**
     * Gets the value of the lastEventGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLastEventGroupGid() {
        return lastEventGroupGid;
    }

    /**
     * Sets the value of the lastEventGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLastEventGroupGid(GLogXMLGidType value) {
        this.lastEventGroupGid = value;
    }

    /**
     * Gets the value of the repetitionScheduleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRepetitionScheduleGid() {
        return repetitionScheduleGid;
    }

    /**
     * Sets the value of the repetitionScheduleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRepetitionScheduleGid(GLogXMLGidType value) {
        this.repetitionScheduleGid = value;
    }

    /**
     * Gets the value of the scheduleType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScheduleType() {
        return scheduleType;
    }

    /**
     * Sets the value of the scheduleType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScheduleType(String value) {
        this.scheduleType = value;
    }

    /**
     * Gets the value of the weightUtil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightUtil() {
        return weightUtil;
    }

    /**
     * Sets the value of the weightUtil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightUtil(String value) {
        this.weightUtil = value;
    }

    /**
     * Gets the value of the volumeUtil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVolumeUtil() {
        return volumeUtil;
    }

    /**
     * Sets the value of the volumeUtil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVolumeUtil(String value) {
        this.volumeUtil = value;
    }

    /**
     * Gets the value of the equipRefUnitUtil property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipRefUnitUtil() {
        return equipRefUnitUtil;
    }

    /**
     * Sets the value of the equipRefUnitUtil property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipRefUnitUtil(String value) {
        this.equipRefUnitUtil = value;
    }

    /**
     * Gets the value of the hasAppointments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasAppointments() {
        return hasAppointments;
    }

    /**
     * Sets the value of the hasAppointments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasAppointments(String value) {
        this.hasAppointments = value;
    }

    /**
     * Gets the value of the jobGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getJobGid() {
        return jobGid;
    }

    /**
     * Sets the value of the jobGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setJobGid(GLogXMLGidType value) {
        this.jobGid = value;
    }

    /**
     * Gets the value of the isCreditNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCreditNote() {
        return isCreditNote;
    }

    /**
     * Sets the value of the isCreditNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCreditNote(String value) {
        this.isCreditNote = value;
    }

    /**
     * Gets the value of the nfrcRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNFRCRuleGid() {
        return nfrcRuleGid;
    }

    /**
     * Sets the value of the nfrcRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNFRCRuleGid(GLogXMLGidType value) {
        this.nfrcRuleGid = value;
    }

    /**
     * Gets the value of the consolGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolGid() {
        return consolGid;
    }

    /**
     * Sets the value of the consolGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolGid(GLogXMLGidType value) {
        this.consolGid = value;
    }

    /**
     * Gets the value of the dutyPaid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyPaid() {
        return dutyPaid;
    }

    /**
     * Sets the value of the dutyPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyPaid(String value) {
        this.dutyPaid = value;
    }

    /**
     * Gets the value of the isMemoBL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMemoBL() {
        return isMemoBL;
    }

    /**
     * Sets the value of the isMemoBL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMemoBL(String value) {
        this.isMemoBL = value;
    }

    /**
     * Gets the value of the isProfitSplit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsProfitSplit() {
        return isProfitSplit;
    }

    /**
     * Sets the value of the isProfitSplit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsProfitSplit(String value) {
        this.isProfitSplit = value;
    }

    /**
     * Gets the value of the isAdvancedCharge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAdvancedCharge() {
        return isAdvancedCharge;
    }

    /**
     * Sets the value of the isAdvancedCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAdvancedCharge(String value) {
        this.isAdvancedCharge = value;
    }

    /**
     * Gets the value of the appointmentPriorityGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAppointmentPriorityGid() {
        return appointmentPriorityGid;
    }

    /**
     * Sets the value of the appointmentPriorityGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAppointmentPriorityGid(GLogXMLGidType value) {
        this.appointmentPriorityGid = value;
    }

    /**
     * Gets the value of the shippingAgentContactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactGid() {
        return shippingAgentContactGid;
    }

    /**
     * Sets the value of the shippingAgentContactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactGid(GLogXMLGidType value) {
        this.shippingAgentContactGid = value;
    }

    /**
     * Gets the value of the loadConfigEngineTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLoadConfigEngineTypeGid() {
        return loadConfigEngineTypeGid;
    }

    /**
     * Sets the value of the loadConfigEngineTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLoadConfigEngineTypeGid(GLogXMLGidType value) {
        this.loadConfigEngineTypeGid = value;
    }

    /**
     * Gets the value of the fixedServiceDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFixedServiceDays() {
        return fixedServiceDays;
    }

    /**
     * Sets the value of the fixedServiceDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFixedServiceDays(String value) {
        this.fixedServiceDays = value;
    }

    /**
     * Gets the value of the sightingLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSightingLocGid() {
        return sightingLocGid;
    }

    /**
     * Sets the value of the sightingLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSightingLocGid(GLogXMLGidType value) {
        this.sightingLocGid = value;
    }

    /**
     * Gets the value of the sightingDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSightingDate() {
        return sightingDate;
    }

    /**
     * Sets the value of the sightingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSightingDate(GLogDateTimeType value) {
        this.sightingDate = value;
    }

    /**
     * Gets the value of the prevSightingLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrevSightingLocGid() {
        return prevSightingLocGid;
    }

    /**
     * Sets the value of the prevSightingLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrevSightingLocGid(GLogXMLGidType value) {
        this.prevSightingLocGid = value;
    }

    /**
     * Gets the value of the prevSightingDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPrevSightingDate() {
        return prevSightingDate;
    }

    /**
     * Sets the value of the prevSightingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPrevSightingDate(GLogDateTimeType value) {
        this.prevSightingDate = value;
    }

    /**
     * Gets the value of the numEquipmentOrdered property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumEquipmentOrdered() {
        return numEquipmentOrdered;
    }

    /**
     * Sets the value of the numEquipmentOrdered property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumEquipmentOrdered(String value) {
        this.numEquipmentOrdered = value;
    }

    /**
     * Gets the value of the combinationEquipmentGrpGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCombinationEquipmentGrpGid() {
        return combinationEquipmentGrpGid;
    }

    /**
     * Sets the value of the combinationEquipmentGrpGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCombinationEquipmentGrpGid(GLogXMLGidType value) {
        this.combinationEquipmentGrpGid = value;
    }

    /**
     * Gets the value of the shipmentGroupHeader property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentGroupHeader property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentGroupHeader().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentGroupHeaderType }
     * 
     * 
     */
    public List<ShipmentGroupHeaderType> getShipmentGroupHeader() {
        if (shipmentGroupHeader == null) {
            shipmentGroupHeader = new ArrayList<ShipmentGroupHeaderType>();
        }
        return this.shipmentGroupHeader;
    }

    /**
     * Gets the value of the srcArbLevelOfServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSrcArbLevelOfServiceGid() {
        return srcArbLevelOfServiceGid;
    }

    /**
     * Sets the value of the srcArbLevelOfServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSrcArbLevelOfServiceGid(GLogXMLGidType value) {
        this.srcArbLevelOfServiceGid = value;
    }

    /**
     * Gets the value of the destArbLevelOfServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDestArbLevelOfServiceGid() {
        return destArbLevelOfServiceGid;
    }

    /**
     * Sets the value of the destArbLevelOfServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDestArbLevelOfServiceGid(GLogXMLGidType value) {
        this.destArbLevelOfServiceGid = value;
    }

}

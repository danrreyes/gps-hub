
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Note.
 * 
 * <p>Java class for DemurrageTransactionNoteType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionNoteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DmTransactionSummary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DmTransactionNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionNoteType", propOrder = {
    "dmTransactionSummary",
    "dmTransactionNote",
    "isSystemGenerated"
})
public class DemurrageTransactionNoteType {

    @XmlElement(name = "DmTransactionSummary")
    protected String dmTransactionSummary;
    @XmlElement(name = "DmTransactionNote")
    protected String dmTransactionNote;
    @XmlElement(name = "IsSystemGenerated")
    protected String isSystemGenerated;

    /**
     * Gets the value of the dmTransactionSummary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionSummary() {
        return dmTransactionSummary;
    }

    /**
     * Sets the value of the dmTransactionSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionSummary(String value) {
        this.dmTransactionSummary = value;
    }

    /**
     * Gets the value of the dmTransactionNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionNote() {
        return dmTransactionNote;
    }

    /**
     * Sets the value of the dmTransactionNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionNote(String value) {
        this.dmTransactionNote = value;
    }

    /**
     * Gets the value of the isSystemGenerated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Sets the value of the isSystemGenerated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

}

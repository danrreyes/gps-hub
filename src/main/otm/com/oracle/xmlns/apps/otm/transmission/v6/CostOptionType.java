
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies possible cost options saved with the quote.
 *          
 * 
 * <p>Java class for CostOptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CostOptionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="IsLate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsFeasible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="NonTransCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostOptionType", propOrder = {
    "transCost",
    "isLate",
    "isFeasible",
    "itineraryGid",
    "nonTransCost"
})
public class CostOptionType {

    @XmlElement(name = "TransCost", required = true)
    protected GLogXMLFinancialAmountType transCost;
    @XmlElement(name = "IsLate")
    protected String isLate;
    @XmlElement(name = "IsFeasible")
    protected String isFeasible;
    @XmlElement(name = "ItineraryGid")
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "NonTransCost")
    protected GLogXMLFinancialAmountType nonTransCost;

    /**
     * Gets the value of the transCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTransCost() {
        return transCost;
    }

    /**
     * Sets the value of the transCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTransCost(GLogXMLFinancialAmountType value) {
        this.transCost = value;
    }

    /**
     * Gets the value of the isLate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLate() {
        return isLate;
    }

    /**
     * Sets the value of the isLate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLate(String value) {
        this.isLate = value;
    }

    /**
     * Gets the value of the isFeasible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFeasible() {
        return isFeasible;
    }

    /**
     * Sets the value of the isFeasible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFeasible(String value) {
        this.isFeasible = value;
    }

    /**
     * Gets the value of the itineraryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Sets the value of the itineraryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Gets the value of the nonTransCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getNonTransCost() {
        return nonTransCost;
    }

    /**
     * Sets the value of the nonTransCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setNonTransCost(GLogXMLFinancialAmountType value) {
        this.nonTransCost = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * TransmissionHeader contains header level information for a Transmission, such as Transmission
 *             Type, Sender Reference ID.
 * 
 *             Deprecated 6.4.2: UserName, Password. See Integration Guide for full details.
 *          
 * 
 * <p>Java class for TransmissionHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransmissionHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransmissionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QueryReplyFormat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StagingInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StagingInfoType" minOccurs="0"/&gt;
 *         &lt;element name="TransmissionCreateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SenderHostName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReceiverHostName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SenderSystemID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Password" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PasswordType" minOccurs="0"/&gt;
 *         &lt;element name="SenderTransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReferenceTransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SuppressTransmissionAck" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AckSpec" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AckSpecType" minOccurs="0"/&gt;
 *         &lt;element name="IsProcessInSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StopProcessOnError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GLogXMLElementName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProcessGrouping" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProcessGroupingType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="NotifyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}NotifyInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DataQueuePriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DataQueueGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionHeaderType", propOrder = {
    "version",
    "transmissionType",
    "queryReplyFormat",
    "stagingInfo",
    "transmissionCreateDt",
    "transactionCount",
    "senderHostName",
    "receiverHostName",
    "senderSystemID",
    "userName",
    "password",
    "senderTransmissionNo",
    "referenceTransmissionNo",
    "suppressTransmissionAck",
    "ackSpec",
    "isProcessInSequence",
    "stopProcessOnError",
    "gLogXMLElementName",
    "processGrouping",
    "notifyInfo",
    "refnum",
    "dataQueuePriority",
    "dataQueueGid"
})
public class TransmissionHeaderType {

    @XmlElement(name = "Version")
    protected String version;
    @XmlElement(name = "TransmissionType")
    protected String transmissionType;
    @XmlElement(name = "QueryReplyFormat")
    protected String queryReplyFormat;
    @XmlElement(name = "StagingInfo")
    protected StagingInfoType stagingInfo;
    @XmlElement(name = "TransmissionCreateDt")
    protected GLogDateTimeType transmissionCreateDt;
    @XmlElement(name = "TransactionCount")
    protected String transactionCount;
    @XmlElement(name = "SenderHostName")
    protected String senderHostName;
    @XmlElement(name = "ReceiverHostName")
    protected String receiverHostName;
    @XmlElement(name = "SenderSystemID")
    protected String senderSystemID;
    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "Password")
    protected PasswordType password;
    @XmlElement(name = "SenderTransmissionNo")
    protected String senderTransmissionNo;
    @XmlElement(name = "ReferenceTransmissionNo")
    protected String referenceTransmissionNo;
    @XmlElement(name = "SuppressTransmissionAck")
    protected String suppressTransmissionAck;
    @XmlElement(name = "AckSpec")
    protected AckSpecType ackSpec;
    @XmlElement(name = "IsProcessInSequence")
    protected String isProcessInSequence;
    @XmlElement(name = "StopProcessOnError")
    protected String stopProcessOnError;
    @XmlElement(name = "GLogXMLElementName")
    protected String gLogXMLElementName;
    @XmlElement(name = "ProcessGrouping")
    protected List<ProcessGroupingType> processGrouping;
    @XmlElement(name = "NotifyInfo")
    protected NotifyInfoType notifyInfo;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "DataQueuePriority")
    protected String dataQueuePriority;
    @XmlElement(name = "DataQueueGid")
    protected GLogXMLGidType dataQueueGid;

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the transmissionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionType() {
        return transmissionType;
    }

    /**
     * Sets the value of the transmissionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionType(String value) {
        this.transmissionType = value;
    }

    /**
     * Gets the value of the queryReplyFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryReplyFormat() {
        return queryReplyFormat;
    }

    /**
     * Sets the value of the queryReplyFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryReplyFormat(String value) {
        this.queryReplyFormat = value;
    }

    /**
     * Gets the value of the stagingInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StagingInfoType }
     *     
     */
    public StagingInfoType getStagingInfo() {
        return stagingInfo;
    }

    /**
     * Sets the value of the stagingInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StagingInfoType }
     *     
     */
    public void setStagingInfo(StagingInfoType value) {
        this.stagingInfo = value;
    }

    /**
     * Gets the value of the transmissionCreateDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransmissionCreateDt() {
        return transmissionCreateDt;
    }

    /**
     * Sets the value of the transmissionCreateDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransmissionCreateDt(GLogDateTimeType value) {
        this.transmissionCreateDt = value;
    }

    /**
     * Gets the value of the transactionCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCount() {
        return transactionCount;
    }

    /**
     * Sets the value of the transactionCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCount(String value) {
        this.transactionCount = value;
    }

    /**
     * Gets the value of the senderHostName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderHostName() {
        return senderHostName;
    }

    /**
     * Sets the value of the senderHostName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderHostName(String value) {
        this.senderHostName = value;
    }

    /**
     * Gets the value of the receiverHostName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiverHostName() {
        return receiverHostName;
    }

    /**
     * Sets the value of the receiverHostName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiverHostName(String value) {
        this.receiverHostName = value;
    }

    /**
     * Gets the value of the senderSystemID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderSystemID() {
        return senderSystemID;
    }

    /**
     * Sets the value of the senderSystemID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderSystemID(String value) {
        this.senderSystemID = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link PasswordType }
     *     
     */
    public PasswordType getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordType }
     *     
     */
    public void setPassword(PasswordType value) {
        this.password = value;
    }

    /**
     * Gets the value of the senderTransmissionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransmissionNo() {
        return senderTransmissionNo;
    }

    /**
     * Sets the value of the senderTransmissionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransmissionNo(String value) {
        this.senderTransmissionNo = value;
    }

    /**
     * Gets the value of the referenceTransmissionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceTransmissionNo() {
        return referenceTransmissionNo;
    }

    /**
     * Sets the value of the referenceTransmissionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceTransmissionNo(String value) {
        this.referenceTransmissionNo = value;
    }

    /**
     * Gets the value of the suppressTransmissionAck property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuppressTransmissionAck() {
        return suppressTransmissionAck;
    }

    /**
     * Sets the value of the suppressTransmissionAck property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuppressTransmissionAck(String value) {
        this.suppressTransmissionAck = value;
    }

    /**
     * Gets the value of the ackSpec property.
     * 
     * @return
     *     possible object is
     *     {@link AckSpecType }
     *     
     */
    public AckSpecType getAckSpec() {
        return ackSpec;
    }

    /**
     * Sets the value of the ackSpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link AckSpecType }
     *     
     */
    public void setAckSpec(AckSpecType value) {
        this.ackSpec = value;
    }

    /**
     * Gets the value of the isProcessInSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsProcessInSequence() {
        return isProcessInSequence;
    }

    /**
     * Sets the value of the isProcessInSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsProcessInSequence(String value) {
        this.isProcessInSequence = value;
    }

    /**
     * Gets the value of the stopProcessOnError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopProcessOnError() {
        return stopProcessOnError;
    }

    /**
     * Sets the value of the stopProcessOnError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopProcessOnError(String value) {
        this.stopProcessOnError = value;
    }

    /**
     * Gets the value of the gLogXMLElementName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGLogXMLElementName() {
        return gLogXMLElementName;
    }

    /**
     * Sets the value of the gLogXMLElementName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGLogXMLElementName(String value) {
        this.gLogXMLElementName = value;
    }

    /**
     * Gets the value of the processGrouping property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the processGrouping property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProcessGrouping().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProcessGroupingType }
     * 
     * 
     */
    public List<ProcessGroupingType> getProcessGrouping() {
        if (processGrouping == null) {
            processGrouping = new ArrayList<ProcessGroupingType>();
        }
        return this.processGrouping;
    }

    /**
     * Gets the value of the notifyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link NotifyInfoType }
     *     
     */
    public NotifyInfoType getNotifyInfo() {
        return notifyInfo;
    }

    /**
     * Sets the value of the notifyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link NotifyInfoType }
     *     
     */
    public void setNotifyInfo(NotifyInfoType value) {
        this.notifyInfo = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the dataQueuePriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataQueuePriority() {
        return dataQueuePriority;
    }

    /**
     * Sets the value of the dataQueuePriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataQueuePriority(String value) {
        this.dataQueuePriority = value;
    }

    /**
     * Gets the value of the dataQueueGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueueGid() {
        return dataQueueGid;
    }

    /**
     * Sets the value of the dataQueueGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueueGid(GLogXMLGidType value) {
        this.dataQueueGid = value;
    }

}

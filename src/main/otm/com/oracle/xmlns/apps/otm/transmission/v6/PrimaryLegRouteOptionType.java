
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PrimaryLegRouteOptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PrimaryLegRouteOptionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RIQResultRoute" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQResultRouteType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrimaryLegRouteOptionType", propOrder = {
    "riqResultRoute"
})
public class PrimaryLegRouteOptionType {

    @XmlElement(name = "RIQResultRoute", required = true)
    protected List<RIQResultRouteType> riqResultRoute;

    /**
     * Gets the value of the riqResultRoute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the riqResultRoute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQResultRoute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQResultRouteType }
     * 
     * 
     */
    public List<RIQResultRouteType> getRIQResultRoute() {
        if (riqResultRoute == null) {
            riqResultRoute = new ArrayList<RIQResultRouteType>();
        }
        return this.riqResultRoute;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AccrualType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccrualType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="AccrualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/&gt;
 *         &lt;element name="TotalFreightCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AmountWithBaseType" minOccurs="0"/&gt;
 *         &lt;element name="TotalFreightSentCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AmountWithBaseType" minOccurs="0"/&gt;
 *         &lt;element name="DeltaCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AmountWithBaseType" minOccurs="0"/&gt;
 *         &lt;element name="AccruedDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="SentDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="IsReversal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AccrualReleaseInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccrualReleaseInfoType" minOccurs="0"/&gt;
 *         &lt;element name="AccrualShipmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccrualShipmentInfoType" minOccurs="0"/&gt;
 *         &lt;element name="AccrualTransOrderInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AccrualTransOrderInfoType" minOccurs="0"/&gt;
 *         &lt;element name="CostTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccrualType", propOrder = {
    "sendReason",
    "accrualGid",
    "releaseGid",
    "shipmentGid",
    "exchangeRateInfo",
    "totalFreightCost",
    "totalFreightSentCost",
    "deltaCost",
    "accruedDt",
    "sentDt",
    "isReversal",
    "accrualReleaseInfo",
    "accrualShipmentInfo",
    "accrualTransOrderInfo",
    "costTypeGid",
    "accessorialCodeGid"
})
public class AccrualType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "AccrualGid", required = true)
    protected GLogXMLGidType accrualGid;
    @XmlElement(name = "ReleaseGid", required = true)
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "TotalFreightCost")
    protected AmountWithBaseType totalFreightCost;
    @XmlElement(name = "TotalFreightSentCost")
    protected AmountWithBaseType totalFreightSentCost;
    @XmlElement(name = "DeltaCost")
    protected AmountWithBaseType deltaCost;
    @XmlElement(name = "AccruedDt")
    protected GLogDateTimeType accruedDt;
    @XmlElement(name = "SentDt")
    protected GLogDateTimeType sentDt;
    @XmlElement(name = "IsReversal")
    protected String isReversal;
    @XmlElement(name = "AccrualReleaseInfo")
    protected AccrualReleaseInfoType accrualReleaseInfo;
    @XmlElement(name = "AccrualShipmentInfo")
    protected AccrualShipmentInfoType accrualShipmentInfo;
    @XmlElement(name = "AccrualTransOrderInfo")
    protected AccrualTransOrderInfoType accrualTransOrderInfo;
    @XmlElement(name = "CostTypeGid")
    protected GLogXMLGidType costTypeGid;
    @XmlElement(name = "AccessorialCodeGid")
    protected GLogXMLGidType accessorialCodeGid;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the accrualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccrualGid() {
        return accrualGid;
    }

    /**
     * Sets the value of the accrualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccrualGid(GLogXMLGidType value) {
        this.accrualGid = value;
    }

    /**
     * Gets the value of the releaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Sets the value of the releaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the exchangeRateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Sets the value of the exchangeRateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Gets the value of the totalFreightCost property.
     * 
     * @return
     *     possible object is
     *     {@link AmountWithBaseType }
     *     
     */
    public AmountWithBaseType getTotalFreightCost() {
        return totalFreightCost;
    }

    /**
     * Sets the value of the totalFreightCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountWithBaseType }
     *     
     */
    public void setTotalFreightCost(AmountWithBaseType value) {
        this.totalFreightCost = value;
    }

    /**
     * Gets the value of the totalFreightSentCost property.
     * 
     * @return
     *     possible object is
     *     {@link AmountWithBaseType }
     *     
     */
    public AmountWithBaseType getTotalFreightSentCost() {
        return totalFreightSentCost;
    }

    /**
     * Sets the value of the totalFreightSentCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountWithBaseType }
     *     
     */
    public void setTotalFreightSentCost(AmountWithBaseType value) {
        this.totalFreightSentCost = value;
    }

    /**
     * Gets the value of the deltaCost property.
     * 
     * @return
     *     possible object is
     *     {@link AmountWithBaseType }
     *     
     */
    public AmountWithBaseType getDeltaCost() {
        return deltaCost;
    }

    /**
     * Sets the value of the deltaCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountWithBaseType }
     *     
     */
    public void setDeltaCost(AmountWithBaseType value) {
        this.deltaCost = value;
    }

    /**
     * Gets the value of the accruedDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAccruedDt() {
        return accruedDt;
    }

    /**
     * Sets the value of the accruedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAccruedDt(GLogDateTimeType value) {
        this.accruedDt = value;
    }

    /**
     * Gets the value of the sentDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSentDt() {
        return sentDt;
    }

    /**
     * Sets the value of the sentDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSentDt(GLogDateTimeType value) {
        this.sentDt = value;
    }

    /**
     * Gets the value of the isReversal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsReversal() {
        return isReversal;
    }

    /**
     * Sets the value of the isReversal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsReversal(String value) {
        this.isReversal = value;
    }

    /**
     * Gets the value of the accrualReleaseInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccrualReleaseInfoType }
     *     
     */
    public AccrualReleaseInfoType getAccrualReleaseInfo() {
        return accrualReleaseInfo;
    }

    /**
     * Sets the value of the accrualReleaseInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccrualReleaseInfoType }
     *     
     */
    public void setAccrualReleaseInfo(AccrualReleaseInfoType value) {
        this.accrualReleaseInfo = value;
    }

    /**
     * Gets the value of the accrualShipmentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccrualShipmentInfoType }
     *     
     */
    public AccrualShipmentInfoType getAccrualShipmentInfo() {
        return accrualShipmentInfo;
    }

    /**
     * Sets the value of the accrualShipmentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccrualShipmentInfoType }
     *     
     */
    public void setAccrualShipmentInfo(AccrualShipmentInfoType value) {
        this.accrualShipmentInfo = value;
    }

    /**
     * Gets the value of the accrualTransOrderInfo property.
     * 
     * @return
     *     possible object is
     *     {@link AccrualTransOrderInfoType }
     *     
     */
    public AccrualTransOrderInfoType getAccrualTransOrderInfo() {
        return accrualTransOrderInfo;
    }

    /**
     * Sets the value of the accrualTransOrderInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccrualTransOrderInfoType }
     *     
     */
    public void setAccrualTransOrderInfo(AccrualTransOrderInfoType value) {
        this.accrualTransOrderInfo = value;
    }

    /**
     * Gets the value of the costTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostTypeGid() {
        return costTypeGid;
    }

    /**
     * Sets the value of the costTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostTypeGid(GLogXMLGidType value) {
        this.costTypeGid = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCodeGid() {
        return accessorialCodeGid;
    }

    /**
     * Sets the value of the accessorialCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCodeGid(GLogXMLGidType value) {
        this.accessorialCodeGid = value;
    }

}

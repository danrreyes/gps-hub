
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DocumentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="DocumentDefinitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ContentManagementSystemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DocumentOwner" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DocumentOwnerType" minOccurs="0"/&gt;
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AddNewRevision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Annotation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DocumentContent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DocumentContentType" minOccurs="0"/&gt;
 *         &lt;element name="DocumentContentParam" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DocumentContentParamType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UsedAs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Context" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentType", propOrder = {
    "documentGid",
    "transactionCode",
    "replaceChildren",
    "documentDefinitionGid",
    "contentManagementSystemGid",
    "documentOwner",
    "indicator",
    "addNewRevision",
    "annotation",
    "status",
    "location",
    "documentContent",
    "documentContentParam",
    "usedAs",
    "context",
    "effectiveDate",
    "expirationDate"
})
public class DocumentType
    extends OTMTransactionInOut
{

    @XmlElement(name = "DocumentGid")
    protected GLogXMLGidType documentGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "DocumentDefinitionGid")
    protected GLogXMLGidType documentDefinitionGid;
    @XmlElement(name = "ContentManagementSystemGid")
    protected GLogXMLGidType contentManagementSystemGid;
    @XmlElement(name = "DocumentOwner")
    protected DocumentOwnerType documentOwner;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "AddNewRevision")
    protected String addNewRevision;
    @XmlElement(name = "Annotation")
    protected String annotation;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "DocumentContent")
    protected DocumentContentType documentContent;
    @XmlElement(name = "DocumentContentParam")
    protected List<DocumentContentParamType> documentContentParam;
    @XmlElement(name = "UsedAs")
    protected String usedAs;
    @XmlElement(name = "Context")
    protected List<ContextType> context;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;

    /**
     * Gets the value of the documentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentGid() {
        return documentGid;
    }

    /**
     * Sets the value of the documentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentGid(GLogXMLGidType value) {
        this.documentGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the documentDefinitionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentDefinitionGid() {
        return documentDefinitionGid;
    }

    /**
     * Sets the value of the documentDefinitionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentDefinitionGid(GLogXMLGidType value) {
        this.documentDefinitionGid = value;
    }

    /**
     * Gets the value of the contentManagementSystemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContentManagementSystemGid() {
        return contentManagementSystemGid;
    }

    /**
     * Sets the value of the contentManagementSystemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContentManagementSystemGid(GLogXMLGidType value) {
        this.contentManagementSystemGid = value;
    }

    /**
     * Gets the value of the documentOwner property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentOwnerType }
     *     
     */
    public DocumentOwnerType getDocumentOwner() {
        return documentOwner;
    }

    /**
     * Sets the value of the documentOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentOwnerType }
     *     
     */
    public void setDocumentOwner(DocumentOwnerType value) {
        this.documentOwner = value;
    }

    /**
     * Gets the value of the indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Sets the value of the indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Gets the value of the addNewRevision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddNewRevision() {
        return addNewRevision;
    }

    /**
     * Sets the value of the addNewRevision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddNewRevision(String value) {
        this.addNewRevision = value;
    }

    /**
     * Gets the value of the annotation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotation() {
        return annotation;
    }

    /**
     * Sets the value of the annotation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotation(String value) {
        this.annotation = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Gets the value of the documentContent property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentContentType }
     *     
     */
    public DocumentContentType getDocumentContent() {
        return documentContent;
    }

    /**
     * Sets the value of the documentContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentContentType }
     *     
     */
    public void setDocumentContent(DocumentContentType value) {
        this.documentContent = value;
    }

    /**
     * Gets the value of the documentContentParam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the documentContentParam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentContentParam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentContentParamType }
     * 
     * 
     */
    public List<DocumentContentParamType> getDocumentContentParam() {
        if (documentContentParam == null) {
            documentContentParam = new ArrayList<DocumentContentParamType>();
        }
        return this.documentContentParam;
    }

    /**
     * Gets the value of the usedAs property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsedAs() {
        return usedAs;
    }

    /**
     * Sets the value of the usedAs property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsedAs(String value) {
        this.usedAs = value;
    }

    /**
     * Gets the value of the context property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the context property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContext().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContextType }
     * 
     * 
     */
    public List<ContextType> getContext() {
        if (context == null) {
            context = new ArrayList<ContextType>();
        }
        return this.context;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Refnum.
 * 
 * <p>Java class for DemurrageTransactionRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DmTransactionRefnumQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DmTransactionRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionRefnumType", propOrder = {
    "dmTransactionRefnumQualGid",
    "dmTransactionRefnumValue"
})
public class DemurrageTransactionRefnumType {

    @XmlElement(name = "DmTransactionRefnumQualGid")
    protected GLogXMLGidType dmTransactionRefnumQualGid;
    @XmlElement(name = "DmTransactionRefnumValue")
    protected String dmTransactionRefnumValue;

    /**
     * Gets the value of the dmTransactionRefnumQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmTransactionRefnumQualGid() {
        return dmTransactionRefnumQualGid;
    }

    /**
     * Sets the value of the dmTransactionRefnumQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmTransactionRefnumQualGid(GLogXMLGidType value) {
        this.dmTransactionRefnumQualGid = value;
    }

    /**
     * Gets the value of the dmTransactionRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionRefnumValue() {
        return dmTransactionRefnumValue;
    }

    /**
     * Sets the value of the dmTransactionRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionRefnumValue(String value) {
        this.dmTransactionRefnumValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Marks is a repeating structure containing MarksNumberQualifierGid and MarksNumber elements
 * 
 * <p>Java class for MarksType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarksType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MarksNumberQualifier" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MarksNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarksType", propOrder = {
    "marksNumberQualifier",
    "marksNumber"
})
public class MarksType {

    @XmlElement(name = "MarksNumberQualifier", required = true)
    protected String marksNumberQualifier;
    @XmlElement(name = "MarksNumber", required = true)
    protected String marksNumber;

    /**
     * Gets the value of the marksNumberQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarksNumberQualifier() {
        return marksNumberQualifier;
    }

    /**
     * Sets the value of the marksNumberQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarksNumberQualifier(String value) {
        this.marksNumberQualifier = value;
    }

    /**
     * Gets the value of the marksNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarksNumber() {
        return marksNumber;
    }

    /**
     * Sets the value of the marksNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarksNumber(String value) {
        this.marksNumber = value;
    }

}

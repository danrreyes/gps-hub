
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the user defined icon info. Validation: References ICON table.
 * 
 * <p>Java class for UserDefIconInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserDefIconInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserDef1IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserDef2IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserDef3IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserDef4IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserDef5IconGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDefIconInfoType", propOrder = {
    "userDef1IconGid",
    "userDef2IconGid",
    "userDef3IconGid",
    "userDef4IconGid",
    "userDef5IconGid"
})
public class UserDefIconInfoType {

    @XmlElement(name = "UserDef1IconGid")
    protected GLogXMLGidType userDef1IconGid;
    @XmlElement(name = "UserDef2IconGid")
    protected GLogXMLGidType userDef2IconGid;
    @XmlElement(name = "UserDef3IconGid")
    protected GLogXMLGidType userDef3IconGid;
    @XmlElement(name = "UserDef4IconGid")
    protected GLogXMLGidType userDef4IconGid;
    @XmlElement(name = "UserDef5IconGid")
    protected GLogXMLGidType userDef5IconGid;

    /**
     * Gets the value of the userDef1IconGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef1IconGid() {
        return userDef1IconGid;
    }

    /**
     * Sets the value of the userDef1IconGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef1IconGid(GLogXMLGidType value) {
        this.userDef1IconGid = value;
    }

    /**
     * Gets the value of the userDef2IconGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef2IconGid() {
        return userDef2IconGid;
    }

    /**
     * Sets the value of the userDef2IconGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef2IconGid(GLogXMLGidType value) {
        this.userDef2IconGid = value;
    }

    /**
     * Gets the value of the userDef3IconGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef3IconGid() {
        return userDef3IconGid;
    }

    /**
     * Sets the value of the userDef3IconGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef3IconGid(GLogXMLGidType value) {
        this.userDef3IconGid = value;
    }

    /**
     * Gets the value of the userDef4IconGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef4IconGid() {
        return userDef4IconGid;
    }

    /**
     * Sets the value of the userDef4IconGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef4IconGid(GLogXMLGidType value) {
        this.userDef4IconGid = value;
    }

    /**
     * Gets the value of the userDef5IconGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDef5IconGid() {
        return userDef5IconGid;
    }

    /**
     * Sets the value of the userDef5IconGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDef5IconGid(GLogXMLGidType value) {
        this.userDef5IconGid = value;
    }

}

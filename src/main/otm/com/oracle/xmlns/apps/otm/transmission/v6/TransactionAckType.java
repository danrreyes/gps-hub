
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionAckType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionAckType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReferencedSendReason" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ProcessControlRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="SendReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                   &lt;element name="ObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SenderTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AckCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AckReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionAckType", propOrder = {
    "referencedSendReason",
    "iTransactionNo",
    "senderTransactionId",
    "ackCode",
    "ackReason",
    "refnum"
})
public class TransactionAckType
    extends OTMTransactionIn
{

    @XmlElement(name = "ReferencedSendReason")
    protected TransactionAckType.ReferencedSendReason referencedSendReason;
    @XmlElement(name = "ITransactionNo", required = true)
    protected String iTransactionNo;
    @XmlElement(name = "SenderTransactionId")
    protected String senderTransactionId;
    @XmlElement(name = "AckCode", required = true)
    protected String ackCode;
    @XmlElement(name = "AckReason")
    protected String ackReason;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Gets the value of the referencedSendReason property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionAckType.ReferencedSendReason }
     *     
     */
    public TransactionAckType.ReferencedSendReason getReferencedSendReason() {
        return referencedSendReason;
    }

    /**
     * Sets the value of the referencedSendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionAckType.ReferencedSendReason }
     *     
     */
    public void setReferencedSendReason(TransactionAckType.ReferencedSendReason value) {
        this.referencedSendReason = value;
    }

    /**
     * Gets the value of the iTransactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Sets the value of the iTransactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Gets the value of the senderTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransactionId() {
        return senderTransactionId;
    }

    /**
     * Sets the value of the senderTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransactionId(String value) {
        this.senderTransactionId = value;
    }

    /**
     * Gets the value of the ackCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAckCode() {
        return ackCode;
    }

    /**
     * Sets the value of the ackCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAckCode(String value) {
        this.ackCode = value;
    }

    /**
     * Gets the value of the ackReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAckReason() {
        return ackReason;
    }

    /**
     * Sets the value of the ackReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAckReason(String value) {
        this.ackReason = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ProcessControlRequestID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="SendReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="ObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "processControlRequestID",
        "sendReasonGid",
        "objectType"
    })
    public static class ReferencedSendReason {

        @XmlElement(name = "ProcessControlRequestID", required = true)
        protected String processControlRequestID;
        @XmlElement(name = "SendReasonGid", required = true)
        protected GLogXMLGidType sendReasonGid;
        @XmlElement(name = "ObjectType", required = true)
        protected String objectType;

        /**
         * Gets the value of the processControlRequestID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProcessControlRequestID() {
            return processControlRequestID;
        }

        /**
         * Sets the value of the processControlRequestID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProcessControlRequestID(String value) {
            this.processControlRequestID = value;
        }

        /**
         * Gets the value of the sendReasonGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getSendReasonGid() {
            return sendReasonGid;
        }

        /**
         * Sets the value of the sendReasonGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setSendReasonGid(GLogXMLGidType value) {
            this.sendReasonGid = value;
        }

        /**
         * Gets the value of the objectType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getObjectType() {
            return objectType;
        }

        /**
         * Sets the value of the objectType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setObjectType(String value) {
            this.objectType = value;
        }

    }

}

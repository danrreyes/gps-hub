
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the agent contact for a given agent.
 * 
 * <p>Java class for ShippingAgentContactType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShippingAgentContactType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShippingAgentContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShippingAgent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShippingAgentType"/&gt;
 *         &lt;element name="ShippingAgentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="InvolvedPartyLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ContactRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ContactRefType"/&gt;
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="IsActive" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DestLocationRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRegionGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipperLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="ConsigneeLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="IsPrimary" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsInternal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NFRCRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsAllowHouseCollect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxHouseCollectAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="MasterBLType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="MasterBLConsigneeInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MasterBLConsigneeInfoType" minOccurs="0"/&gt;
 *         &lt;element name="MasterBLNotifyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MasterBLNotifyInfoType" minOccurs="0"/&gt;
 *         &lt;element name="MasterBLAlsoNotifyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MasterBLNotifyInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ShippingAgentContactNote" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShippingAgentContactNoteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShippingAgentContactProfit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShippingAgentContactProfitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShippingAgentContactType", propOrder = {
    "shippingAgentContactGid",
    "shippingAgent",
    "shippingAgentTypeGid",
    "involvedPartyQualifierGid",
    "involvedPartyLocationRef",
    "contactRef",
    "comMethodGid",
    "isActive",
    "effectiveDate",
    "expirationDate",
    "modeProfileGid",
    "destLocationRegionGid",
    "shipperLocationProfileGid",
    "consigneeLocationProfileGid",
    "isPrimary",
    "isInternal",
    "nfrcRuleGid",
    "isAllowHouseCollect",
    "maxHouseCollectAmount",
    "masterBLType",
    "paymentMethodCodeGid",
    "masterBLConsigneeInfo",
    "masterBLNotifyInfo",
    "masterBLAlsoNotifyInfo",
    "shippingAgentContactNote",
    "shippingAgentContactProfit"
})
public class ShippingAgentContactType {

    @XmlElement(name = "ShippingAgentContactGid", required = true)
    protected GLogXMLGidType shippingAgentContactGid;
    @XmlElement(name = "ShippingAgent", required = true)
    protected ShippingAgentType shippingAgent;
    @XmlElement(name = "ShippingAgentTypeGid")
    protected GLogXMLGidType shippingAgentTypeGid;
    @XmlElement(name = "InvolvedPartyQualifierGid", required = true)
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "InvolvedPartyLocationRef")
    protected GLogXMLLocRefType involvedPartyLocationRef;
    @XmlElement(name = "ContactRef", required = true)
    protected ContactRefType contactRef;
    @XmlElement(name = "ComMethodGid", required = true)
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "IsActive", required = true)
    protected String isActive;
    @XmlElement(name = "EffectiveDate", required = true)
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate", required = true)
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "DestLocationRegionGid")
    protected GLogXMLRegionGidType destLocationRegionGid;
    @XmlElement(name = "ShipperLocationProfileGid")
    protected GLogXMLLocProfileType shipperLocationProfileGid;
    @XmlElement(name = "ConsigneeLocationProfileGid")
    protected GLogXMLLocProfileType consigneeLocationProfileGid;
    @XmlElement(name = "IsPrimary")
    protected String isPrimary;
    @XmlElement(name = "IsInternal")
    protected String isInternal;
    @XmlElement(name = "NFRCRuleGid")
    protected GLogXMLGidType nfrcRuleGid;
    @XmlElement(name = "IsAllowHouseCollect")
    protected String isAllowHouseCollect;
    @XmlElement(name = "MaxHouseCollectAmount")
    protected GLogXMLFinancialAmountType maxHouseCollectAmount;
    @XmlElement(name = "MasterBLType")
    protected String masterBLType;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "MasterBLConsigneeInfo")
    protected MasterBLConsigneeInfoType masterBLConsigneeInfo;
    @XmlElement(name = "MasterBLNotifyInfo")
    protected MasterBLNotifyInfoType masterBLNotifyInfo;
    @XmlElement(name = "MasterBLAlsoNotifyInfo")
    protected MasterBLNotifyInfoType masterBLAlsoNotifyInfo;
    @XmlElement(name = "ShippingAgentContactNote")
    protected List<ShippingAgentContactNoteType> shippingAgentContactNote;
    @XmlElement(name = "ShippingAgentContactProfit")
    protected List<ShippingAgentContactProfitType> shippingAgentContactProfit;

    /**
     * Gets the value of the shippingAgentContactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactGid() {
        return shippingAgentContactGid;
    }

    /**
     * Sets the value of the shippingAgentContactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactGid(GLogXMLGidType value) {
        this.shippingAgentContactGid = value;
    }

    /**
     * Gets the value of the shippingAgent property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingAgentType }
     *     
     */
    public ShippingAgentType getShippingAgent() {
        return shippingAgent;
    }

    /**
     * Sets the value of the shippingAgent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingAgentType }
     *     
     */
    public void setShippingAgent(ShippingAgentType value) {
        this.shippingAgent = value;
    }

    /**
     * Gets the value of the shippingAgentTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentTypeGid() {
        return shippingAgentTypeGid;
    }

    /**
     * Sets the value of the shippingAgentTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentTypeGid(GLogXMLGidType value) {
        this.shippingAgentTypeGid = value;
    }

    /**
     * Gets the value of the involvedPartyQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Sets the value of the involvedPartyQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Gets the value of the involvedPartyLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getInvolvedPartyLocationRef() {
        return involvedPartyLocationRef;
    }

    /**
     * Sets the value of the involvedPartyLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setInvolvedPartyLocationRef(GLogXMLLocRefType value) {
        this.involvedPartyLocationRef = value;
    }

    /**
     * Gets the value of the contactRef property.
     * 
     * @return
     *     possible object is
     *     {@link ContactRefType }
     *     
     */
    public ContactRefType getContactRef() {
        return contactRef;
    }

    /**
     * Sets the value of the contactRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactRefType }
     *     
     */
    public void setContactRef(ContactRefType value) {
        this.contactRef = value;
    }

    /**
     * Gets the value of the comMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Sets the value of the comMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Gets the value of the isActive property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsActive() {
        return isActive;
    }

    /**
     * Sets the value of the isActive property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsActive(String value) {
        this.isActive = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the destLocationRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public GLogXMLRegionGidType getDestLocationRegionGid() {
        return destLocationRegionGid;
    }

    /**
     * Sets the value of the destLocationRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRegionGidType }
     *     
     */
    public void setDestLocationRegionGid(GLogXMLRegionGidType value) {
        this.destLocationRegionGid = value;
    }

    /**
     * Gets the value of the shipperLocationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getShipperLocationProfileGid() {
        return shipperLocationProfileGid;
    }

    /**
     * Sets the value of the shipperLocationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setShipperLocationProfileGid(GLogXMLLocProfileType value) {
        this.shipperLocationProfileGid = value;
    }

    /**
     * Gets the value of the consigneeLocationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getConsigneeLocationProfileGid() {
        return consigneeLocationProfileGid;
    }

    /**
     * Sets the value of the consigneeLocationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setConsigneeLocationProfileGid(GLogXMLLocProfileType value) {
        this.consigneeLocationProfileGid = value;
    }

    /**
     * Gets the value of the isPrimary property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrimary() {
        return isPrimary;
    }

    /**
     * Sets the value of the isPrimary property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrimary(String value) {
        this.isPrimary = value;
    }

    /**
     * Gets the value of the isInternal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInternal() {
        return isInternal;
    }

    /**
     * Sets the value of the isInternal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInternal(String value) {
        this.isInternal = value;
    }

    /**
     * Gets the value of the nfrcRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNFRCRuleGid() {
        return nfrcRuleGid;
    }

    /**
     * Sets the value of the nfrcRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNFRCRuleGid(GLogXMLGidType value) {
        this.nfrcRuleGid = value;
    }

    /**
     * Gets the value of the isAllowHouseCollect property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowHouseCollect() {
        return isAllowHouseCollect;
    }

    /**
     * Sets the value of the isAllowHouseCollect property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowHouseCollect(String value) {
        this.isAllowHouseCollect = value;
    }

    /**
     * Gets the value of the maxHouseCollectAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getMaxHouseCollectAmount() {
        return maxHouseCollectAmount;
    }

    /**
     * Sets the value of the maxHouseCollectAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setMaxHouseCollectAmount(GLogXMLFinancialAmountType value) {
        this.maxHouseCollectAmount = value;
    }

    /**
     * Gets the value of the masterBLType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMasterBLType() {
        return masterBLType;
    }

    /**
     * Sets the value of the masterBLType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMasterBLType(String value) {
        this.masterBLType = value;
    }

    /**
     * Gets the value of the paymentMethodCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Sets the value of the paymentMethodCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Gets the value of the masterBLConsigneeInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MasterBLConsigneeInfoType }
     *     
     */
    public MasterBLConsigneeInfoType getMasterBLConsigneeInfo() {
        return masterBLConsigneeInfo;
    }

    /**
     * Sets the value of the masterBLConsigneeInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterBLConsigneeInfoType }
     *     
     */
    public void setMasterBLConsigneeInfo(MasterBLConsigneeInfoType value) {
        this.masterBLConsigneeInfo = value;
    }

    /**
     * Gets the value of the masterBLNotifyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public MasterBLNotifyInfoType getMasterBLNotifyInfo() {
        return masterBLNotifyInfo;
    }

    /**
     * Sets the value of the masterBLNotifyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public void setMasterBLNotifyInfo(MasterBLNotifyInfoType value) {
        this.masterBLNotifyInfo = value;
    }

    /**
     * Gets the value of the masterBLAlsoNotifyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public MasterBLNotifyInfoType getMasterBLAlsoNotifyInfo() {
        return masterBLAlsoNotifyInfo;
    }

    /**
     * Sets the value of the masterBLAlsoNotifyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MasterBLNotifyInfoType }
     *     
     */
    public void setMasterBLAlsoNotifyInfo(MasterBLNotifyInfoType value) {
        this.masterBLAlsoNotifyInfo = value;
    }

    /**
     * Gets the value of the shippingAgentContactNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shippingAgentContactNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShippingAgentContactNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShippingAgentContactNoteType }
     * 
     * 
     */
    public List<ShippingAgentContactNoteType> getShippingAgentContactNote() {
        if (shippingAgentContactNote == null) {
            shippingAgentContactNote = new ArrayList<ShippingAgentContactNoteType>();
        }
        return this.shippingAgentContactNote;
    }

    /**
     * Gets the value of the shippingAgentContactProfit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shippingAgentContactProfit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShippingAgentContactProfit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShippingAgentContactProfitType }
     * 
     * 
     */
    public List<ShippingAgentContactProfitType> getShippingAgentContactProfit() {
        if (shippingAgentContactProfit == null) {
            shippingAgentContactProfit = new ArrayList<ShippingAgentContactProfitType>();
        }
        return this.shippingAgentContactProfit;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * FinancialAmount includes a currency code and an amount of money in that currency.
 *             Note: Functional currency will only be set for a domain if and when the exchange rate gid/date is set.
 *          
 * 
 * <p>Java class for FinancialAmountType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialAmountType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GlobalCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RateToBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FuncCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FuncCurrencyAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialAmountType", propOrder = {
    "globalCurrencyCode",
    "monetaryAmount",
    "rateToBase",
    "funcCurrencyCode",
    "funcCurrencyAmount"
})
public class FinancialAmountType {

    @XmlElement(name = "GlobalCurrencyCode", required = true)
    protected String globalCurrencyCode;
    @XmlElement(name = "MonetaryAmount", required = true)
    protected String monetaryAmount;
    @XmlElement(name = "RateToBase")
    protected String rateToBase;
    @XmlElement(name = "FuncCurrencyCode")
    protected String funcCurrencyCode;
    @XmlElement(name = "FuncCurrencyAmount")
    protected String funcCurrencyAmount;

    /**
     * Gets the value of the globalCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    /**
     * Sets the value of the globalCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalCurrencyCode(String value) {
        this.globalCurrencyCode = value;
    }

    /**
     * Gets the value of the monetaryAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonetaryAmount() {
        return monetaryAmount;
    }

    /**
     * Sets the value of the monetaryAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonetaryAmount(String value) {
        this.monetaryAmount = value;
    }

    /**
     * Gets the value of the rateToBase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateToBase() {
        return rateToBase;
    }

    /**
     * Sets the value of the rateToBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateToBase(String value) {
        this.rateToBase = value;
    }

    /**
     * Gets the value of the funcCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuncCurrencyCode() {
        return funcCurrencyCode;
    }

    /**
     * Sets the value of the funcCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuncCurrencyCode(String value) {
        this.funcCurrencyCode = value;
    }

    /**
     * Gets the value of the funcCurrencyAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuncCurrencyAmount() {
        return funcCurrencyAmount;
    }

    /**
     * Sets the value of the funcCurrencyAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuncCurrencyAmount(String value) {
        this.funcCurrencyAmount = value;
    }

}

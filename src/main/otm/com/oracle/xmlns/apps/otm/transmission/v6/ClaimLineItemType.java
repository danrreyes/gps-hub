
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies a line item for the claim.
 *          
 * 
 * <p>Java class for ClaimLineItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimLineItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DamageTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="PricePerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PricePerUnitType" minOccurs="0"/&gt;
 *         &lt;element name="PricePerUnitUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UOMContextType" minOccurs="0"/&gt;
 *         &lt;element name="SizeOfLoss" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="DamagedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="DamagedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DamagedFraction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OriginalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="OriginalQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NewWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="NewQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RemovedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="RemovedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RemovedFraction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OrigCylinderDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/&gt;
 *         &lt;element name="OrigCylinderCoreDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/&gt;
 *         &lt;element name="RemovedCylinderDepth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/&gt;
 *         &lt;element name="NewCylinderDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/&gt;
 *         &lt;element name="DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="PackageStatusGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ClaimCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimCostType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimLineItemType", propOrder = {
    "sequenceNumber",
    "releaseLineGid",
    "serialNumber",
    "damageTypeGid",
    "transportHandlingUnitRef",
    "pricePerUnit",
    "pricePerUnitUOM",
    "sizeOfLoss",
    "damagedWeightVolume",
    "damagedQuantity",
    "damagedFraction",
    "originalWeightVolume",
    "originalQuantity",
    "newWeightVolume",
    "newQuantity",
    "removedWeightVolume",
    "removedQuantity",
    "removedFraction",
    "origCylinderDiameter",
    "origCylinderCoreDiameter",
    "removedCylinderDepth",
    "newCylinderDiameter",
    "declaredValue",
    "packageStatusGid",
    "claimCost",
    "refnum"
})
public class ClaimLineItemType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "ReleaseLineGid")
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "SerialNumber")
    protected String serialNumber;
    @XmlElement(name = "DamageTypeGid")
    protected GLogXMLGidType damageTypeGid;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "PricePerUnit")
    protected PricePerUnitType pricePerUnit;
    @XmlElement(name = "PricePerUnitUOM")
    protected UOMContextType pricePerUnitUOM;
    @XmlElement(name = "SizeOfLoss")
    protected GLogXMLFinancialAmountType sizeOfLoss;
    @XmlElement(name = "DamagedWeightVolume")
    protected WeightVolumeType damagedWeightVolume;
    @XmlElement(name = "DamagedQuantity")
    protected String damagedQuantity;
    @XmlElement(name = "DamagedFraction")
    protected String damagedFraction;
    @XmlElement(name = "OriginalWeightVolume")
    protected WeightVolumeType originalWeightVolume;
    @XmlElement(name = "OriginalQuantity")
    protected String originalQuantity;
    @XmlElement(name = "NewWeightVolume")
    protected WeightVolumeType newWeightVolume;
    @XmlElement(name = "NewQuantity")
    protected String newQuantity;
    @XmlElement(name = "RemovedWeightVolume")
    protected WeightVolumeType removedWeightVolume;
    @XmlElement(name = "RemovedQuantity")
    protected String removedQuantity;
    @XmlElement(name = "RemovedFraction")
    protected String removedFraction;
    @XmlElement(name = "OrigCylinderDiameter")
    protected GLogXMLDiameterType origCylinderDiameter;
    @XmlElement(name = "OrigCylinderCoreDiameter")
    protected GLogXMLDiameterType origCylinderCoreDiameter;
    @XmlElement(name = "RemovedCylinderDepth")
    protected GLogXMLLengthType removedCylinderDepth;
    @XmlElement(name = "NewCylinderDiameter")
    protected GLogXMLDiameterType newCylinderDiameter;
    @XmlElement(name = "DeclaredValue")
    protected GLogXMLFinancialAmountType declaredValue;
    @XmlElement(name = "PackageStatusGid")
    protected GLogXMLGidType packageStatusGid;
    @XmlElement(name = "ClaimCost")
    protected List<ClaimCostType> claimCost;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the releaseLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Sets the value of the releaseLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the damageTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageTypeGid() {
        return damageTypeGid;
    }

    /**
     * Sets the value of the damageTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageTypeGid(GLogXMLGidType value) {
        this.damageTypeGid = value;
    }

    /**
     * Gets the value of the transportHandlingUnitRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Sets the value of the transportHandlingUnitRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Gets the value of the pricePerUnit property.
     * 
     * @return
     *     possible object is
     *     {@link PricePerUnitType }
     *     
     */
    public PricePerUnitType getPricePerUnit() {
        return pricePerUnit;
    }

    /**
     * Sets the value of the pricePerUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link PricePerUnitType }
     *     
     */
    public void setPricePerUnit(PricePerUnitType value) {
        this.pricePerUnit = value;
    }

    /**
     * Gets the value of the pricePerUnitUOM property.
     * 
     * @return
     *     possible object is
     *     {@link UOMContextType }
     *     
     */
    public UOMContextType getPricePerUnitUOM() {
        return pricePerUnitUOM;
    }

    /**
     * Sets the value of the pricePerUnitUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link UOMContextType }
     *     
     */
    public void setPricePerUnitUOM(UOMContextType value) {
        this.pricePerUnitUOM = value;
    }

    /**
     * Gets the value of the sizeOfLoss property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getSizeOfLoss() {
        return sizeOfLoss;
    }

    /**
     * Sets the value of the sizeOfLoss property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setSizeOfLoss(GLogXMLFinancialAmountType value) {
        this.sizeOfLoss = value;
    }

    /**
     * Gets the value of the damagedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getDamagedWeightVolume() {
        return damagedWeightVolume;
    }

    /**
     * Sets the value of the damagedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setDamagedWeightVolume(WeightVolumeType value) {
        this.damagedWeightVolume = value;
    }

    /**
     * Gets the value of the damagedQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamagedQuantity() {
        return damagedQuantity;
    }

    /**
     * Sets the value of the damagedQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamagedQuantity(String value) {
        this.damagedQuantity = value;
    }

    /**
     * Gets the value of the damagedFraction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamagedFraction() {
        return damagedFraction;
    }

    /**
     * Sets the value of the damagedFraction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamagedFraction(String value) {
        this.damagedFraction = value;
    }

    /**
     * Gets the value of the originalWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getOriginalWeightVolume() {
        return originalWeightVolume;
    }

    /**
     * Sets the value of the originalWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setOriginalWeightVolume(WeightVolumeType value) {
        this.originalWeightVolume = value;
    }

    /**
     * Gets the value of the originalQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalQuantity() {
        return originalQuantity;
    }

    /**
     * Sets the value of the originalQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalQuantity(String value) {
        this.originalQuantity = value;
    }

    /**
     * Gets the value of the newWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getNewWeightVolume() {
        return newWeightVolume;
    }

    /**
     * Sets the value of the newWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setNewWeightVolume(WeightVolumeType value) {
        this.newWeightVolume = value;
    }

    /**
     * Gets the value of the newQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewQuantity() {
        return newQuantity;
    }

    /**
     * Sets the value of the newQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewQuantity(String value) {
        this.newQuantity = value;
    }

    /**
     * Gets the value of the removedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getRemovedWeightVolume() {
        return removedWeightVolume;
    }

    /**
     * Sets the value of the removedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setRemovedWeightVolume(WeightVolumeType value) {
        this.removedWeightVolume = value;
    }

    /**
     * Gets the value of the removedQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemovedQuantity() {
        return removedQuantity;
    }

    /**
     * Sets the value of the removedQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemovedQuantity(String value) {
        this.removedQuantity = value;
    }

    /**
     * Gets the value of the removedFraction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemovedFraction() {
        return removedFraction;
    }

    /**
     * Sets the value of the removedFraction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemovedFraction(String value) {
        this.removedFraction = value;
    }

    /**
     * Gets the value of the origCylinderDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getOrigCylinderDiameter() {
        return origCylinderDiameter;
    }

    /**
     * Sets the value of the origCylinderDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setOrigCylinderDiameter(GLogXMLDiameterType value) {
        this.origCylinderDiameter = value;
    }

    /**
     * Gets the value of the origCylinderCoreDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getOrigCylinderCoreDiameter() {
        return origCylinderCoreDiameter;
    }

    /**
     * Sets the value of the origCylinderCoreDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setOrigCylinderCoreDiameter(GLogXMLDiameterType value) {
        this.origCylinderCoreDiameter = value;
    }

    /**
     * Gets the value of the removedCylinderDepth property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getRemovedCylinderDepth() {
        return removedCylinderDepth;
    }

    /**
     * Sets the value of the removedCylinderDepth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setRemovedCylinderDepth(GLogXMLLengthType value) {
        this.removedCylinderDepth = value;
    }

    /**
     * Gets the value of the newCylinderDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getNewCylinderDiameter() {
        return newCylinderDiameter;
    }

    /**
     * Sets the value of the newCylinderDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setNewCylinderDiameter(GLogXMLDiameterType value) {
        this.newCylinderDiameter = value;
    }

    /**
     * Gets the value of the declaredValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDeclaredValue() {
        return declaredValue;
    }

    /**
     * Sets the value of the declaredValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDeclaredValue(GLogXMLFinancialAmountType value) {
        this.declaredValue = value;
    }

    /**
     * Gets the value of the packageStatusGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackageStatusGid() {
        return packageStatusGid;
    }

    /**
     * Sets the value of the packageStatusGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackageStatusGid(GLogXMLGidType value) {
        this.packageStatusGid = value;
    }

    /**
     * Gets the value of the claimCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the claimCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimCostType }
     * 
     * 
     */
    public List<ClaimCostType> getClaimCost() {
        if (claimCost == null) {
            claimCost = new ArrayList<ClaimCostType>();
        }
        return this.claimCost;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

}

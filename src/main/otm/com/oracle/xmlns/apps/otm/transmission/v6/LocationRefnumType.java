
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * LocationRefnum is a location reference number,
 *             which provides an alternate means for identifying a location.
 *             It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for LocationRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LocationRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="LocationRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationRefnumType", propOrder = {
    "locationRefnumQualifierGid",
    "locationRefnumValue"
})
public class LocationRefnumType {

    @XmlElement(name = "LocationRefnumQualifierGid", required = true)
    protected GLogXMLGidType locationRefnumQualifierGid;
    @XmlElement(name = "LocationRefnumValue", required = true)
    protected String locationRefnumValue;

    /**
     * Gets the value of the locationRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationRefnumQualifierGid() {
        return locationRefnumQualifierGid;
    }

    /**
     * Sets the value of the locationRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationRefnumQualifierGid(GLogXMLGidType value) {
        this.locationRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the locationRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationRefnumValue() {
        return locationRefnumValue;
    }

    /**
     * Sets the value of the locationRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationRefnumValue(String value) {
        this.locationRefnumValue = value;
    }

}

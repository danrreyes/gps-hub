
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLLWHType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLLWHType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LengthWidthHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLLWHType", propOrder = {
    "lengthWidthHeight"
})
public class GLogXMLLWHType {

    @XmlElement(name = "LengthWidthHeight", required = true)
    protected LengthWidthHeightType lengthWidthHeight;

    /**
     * Gets the value of the lengthWidthHeight property.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getLengthWidthHeight() {
        return lengthWidthHeight;
    }

    /**
     * Sets the value of the lengthWidthHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setLengthWidthHeight(LengthWidthHeightType value) {
        this.lengthWidthHeight = value;
    }

}

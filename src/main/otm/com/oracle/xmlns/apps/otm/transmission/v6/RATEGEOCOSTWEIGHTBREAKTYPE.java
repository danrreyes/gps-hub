
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RATE_GEO_COST_WEIGHT_BREAK_TYPE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RATE_GEO_COST_WEIGHT_BREAK_TYPE"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;element name="RATE_GEO_COST_WEIGHT_BREAK_ROW" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="WEIGHT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_DISCOUNT_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_DISCOUNT_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_DISCOUNT_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RATE_GEO_COST_WEIGHT_BREAK_TYPE", propOrder = {
    "rategeocostweightbreakrow"
})
public class RATEGEOCOSTWEIGHTBREAKTYPE {

    @XmlElement(name = "RATE_GEO_COST_WEIGHT_BREAK_ROW")
    protected List<RATEGEOCOSTWEIGHTBREAKTYPE.RATEGEOCOSTWEIGHTBREAKROW> rategeocostweightbreakrow;

    /**
     * Gets the value of the rategeocostweightbreakrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rategeocostweightbreakrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEGEOCOSTWEIGHTBREAKROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RATEGEOCOSTWEIGHTBREAKTYPE.RATEGEOCOSTWEIGHTBREAKROW }
     * 
     * 
     */
    public List<RATEGEOCOSTWEIGHTBREAKTYPE.RATEGEOCOSTWEIGHTBREAKROW> getRATEGEOCOSTWEIGHTBREAKROW() {
        if (rategeocostweightbreakrow == null) {
            rategeocostweightbreakrow = new ArrayList<RATEGEOCOSTWEIGHTBREAKTYPE.RATEGEOCOSTWEIGHTBREAKROW>();
        }
        return this.rategeocostweightbreakrow;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="WEIGHT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_DISCOUNT_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_DISCOUNT_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_DISCOUNT_VALUE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rategeocostseq",
        "weightbreakgid",
        "ratediscountvalue",
        "ratediscountvaluegid",
        "ratediscountvaluebase",
        "rategeocostgroupgid",
        "domainname",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate"
    })
    public static class RATEGEOCOSTWEIGHTBREAKROW {

        @XmlElement(name = "RATE_GEO_COST_SEQ")
        protected String rategeocostseq;
        @XmlElement(name = "WEIGHT_BREAK_GID")
        protected String weightbreakgid;
        @XmlElement(name = "RATE_DISCOUNT_VALUE")
        protected String ratediscountvalue;
        @XmlElement(name = "RATE_DISCOUNT_VALUE_GID")
        protected String ratediscountvaluegid;
        @XmlElement(name = "RATE_DISCOUNT_VALUE_BASE")
        protected String ratediscountvaluebase;
        @XmlElement(name = "RATE_GEO_COST_GROUP_GID")
        protected String rategeocostgroupgid;
        @XmlElement(name = "DOMAIN_NAME")
        protected String domainname;
        @XmlElement(name = "INSERT_USER")
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE")
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Gets the value of the rategeocostseq property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTSEQ() {
            return rategeocostseq;
        }

        /**
         * Sets the value of the rategeocostseq property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTSEQ(String value) {
            this.rategeocostseq = value;
        }

        /**
         * Gets the value of the weightbreakgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWEIGHTBREAKGID() {
            return weightbreakgid;
        }

        /**
         * Sets the value of the weightbreakgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWEIGHTBREAKGID(String value) {
            this.weightbreakgid = value;
        }

        /**
         * Gets the value of the ratediscountvalue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISCOUNTVALUE() {
            return ratediscountvalue;
        }

        /**
         * Sets the value of the ratediscountvalue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISCOUNTVALUE(String value) {
            this.ratediscountvalue = value;
        }

        /**
         * Gets the value of the ratediscountvaluegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISCOUNTVALUEGID() {
            return ratediscountvaluegid;
        }

        /**
         * Sets the value of the ratediscountvaluegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISCOUNTVALUEGID(String value) {
            this.ratediscountvaluegid = value;
        }

        /**
         * Gets the value of the ratediscountvaluebase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEDISCOUNTVALUEBASE() {
            return ratediscountvaluebase;
        }

        /**
         * Sets the value of the ratediscountvaluebase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEDISCOUNTVALUEBASE(String value) {
            this.ratediscountvaluebase = value;
        }

        /**
         * Gets the value of the rategeocostgroupgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTGROUPGID() {
            return rategeocostgroupgid;
        }

        /**
         * Sets the value of the rategeocostgroupgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTGROUPGID(String value) {
            this.rategeocostgroupgid = value;
        }

        /**
         * Gets the value of the domainname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Sets the value of the domainname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Gets the value of the insertuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Sets the value of the insertuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Gets the value of the insertdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Sets the value of the insertdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Gets the value of the updateuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Sets the value of the updateuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Gets the value of the updatedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Sets the value of the updatedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Gets the value of the num property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Sets the value of the num property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * FreightRate specifies rate and charges detail relative to a line item.
 * 
 * <p>Java class for FreightRateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FreightRateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FreightRateQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FreightRateValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FreightCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="PrepaidAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="SpecialCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialChargeType" minOccurs="0"/&gt;
 *         &lt;element name="FP_DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DeclaredValueType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FreightRateType", propOrder = {
    "freightRateQualifier",
    "freightRateValue",
    "paymentMethodCodeGid",
    "freightCharge",
    "prepaidAmount",
    "specialCharge",
    "fpDeclaredValue"
})
public class FreightRateType {

    @XmlElement(name = "FreightRateQualifier")
    protected String freightRateQualifier;
    @XmlElement(name = "FreightRateValue")
    protected String freightRateValue;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "FreightCharge")
    protected GLogXMLFinancialAmountType freightCharge;
    @XmlElement(name = "PrepaidAmount")
    protected GLogXMLFinancialAmountType prepaidAmount;
    @XmlElement(name = "SpecialCharge")
    protected SpecialChargeType specialCharge;
    @XmlElement(name = "FP_DeclaredValue")
    protected DeclaredValueType fpDeclaredValue;

    /**
     * Gets the value of the freightRateQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightRateQualifier() {
        return freightRateQualifier;
    }

    /**
     * Sets the value of the freightRateQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightRateQualifier(String value) {
        this.freightRateQualifier = value;
    }

    /**
     * Gets the value of the freightRateValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFreightRateValue() {
        return freightRateValue;
    }

    /**
     * Sets the value of the freightRateValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFreightRateValue(String value) {
        this.freightRateValue = value;
    }

    /**
     * Gets the value of the paymentMethodCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Sets the value of the paymentMethodCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Gets the value of the freightCharge property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getFreightCharge() {
        return freightCharge;
    }

    /**
     * Sets the value of the freightCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setFreightCharge(GLogXMLFinancialAmountType value) {
        this.freightCharge = value;
    }

    /**
     * Gets the value of the prepaidAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPrepaidAmount() {
        return prepaidAmount;
    }

    /**
     * Sets the value of the prepaidAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPrepaidAmount(GLogXMLFinancialAmountType value) {
        this.prepaidAmount = value;
    }

    /**
     * Gets the value of the specialCharge property.
     * 
     * @return
     *     possible object is
     *     {@link SpecialChargeType }
     *     
     */
    public SpecialChargeType getSpecialCharge() {
        return specialCharge;
    }

    /**
     * Sets the value of the specialCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpecialChargeType }
     *     
     */
    public void setSpecialCharge(SpecialChargeType value) {
        this.specialCharge = value;
    }

    /**
     * Gets the value of the fpDeclaredValue property.
     * 
     * @return
     *     possible object is
     *     {@link DeclaredValueType }
     *     
     */
    public DeclaredValueType getFPDeclaredValue() {
        return fpDeclaredValue;
    }

    /**
     * Sets the value of the fpDeclaredValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeclaredValueType }
     *     
     */
    public void setFPDeclaredValue(DeclaredValueType value) {
        this.fpDeclaredValue = value;
    }

}

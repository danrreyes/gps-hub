
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Inspection and survey information.
 * 
 * <p>Java class for InspectionAndSurveyInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InspectionAndSurveyInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InspectionNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InspectionRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InspectionScheduleDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="InspectionDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="SurveyLayCanDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InspectionAndSurveyInfoType", propOrder = {
    "inspectionNumber",
    "inspectionRequired",
    "inspectionScheduleDt",
    "inspectionDt",
    "surveyLayCanDt"
})
public class InspectionAndSurveyInfoType {

    @XmlElement(name = "InspectionNumber")
    protected String inspectionNumber;
    @XmlElement(name = "InspectionRequired")
    protected String inspectionRequired;
    @XmlElement(name = "InspectionScheduleDt")
    protected GLogDateTimeType inspectionScheduleDt;
    @XmlElement(name = "InspectionDt")
    protected GLogDateTimeType inspectionDt;
    @XmlElement(name = "SurveyLayCanDt")
    protected GLogDateTimeType surveyLayCanDt;

    /**
     * Gets the value of the inspectionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectionNumber() {
        return inspectionNumber;
    }

    /**
     * Sets the value of the inspectionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectionNumber(String value) {
        this.inspectionNumber = value;
    }

    /**
     * Gets the value of the inspectionRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInspectionRequired() {
        return inspectionRequired;
    }

    /**
     * Sets the value of the inspectionRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInspectionRequired(String value) {
        this.inspectionRequired = value;
    }

    /**
     * Gets the value of the inspectionScheduleDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInspectionScheduleDt() {
        return inspectionScheduleDt;
    }

    /**
     * Sets the value of the inspectionScheduleDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInspectionScheduleDt(GLogDateTimeType value) {
        this.inspectionScheduleDt = value;
    }

    /**
     * Gets the value of the inspectionDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInspectionDt() {
        return inspectionDt;
    }

    /**
     * Sets the value of the inspectionDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInspectionDt(GLogDateTimeType value) {
        this.inspectionDt = value;
    }

    /**
     * Gets the value of the surveyLayCanDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSurveyLayCanDt() {
        return surveyLayCanDt;
    }

    /**
     * Sets the value of the surveyLayCanDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSurveyLayCanDt(GLogDateTimeType value) {
        this.surveyLayCanDt = value;
    }

}

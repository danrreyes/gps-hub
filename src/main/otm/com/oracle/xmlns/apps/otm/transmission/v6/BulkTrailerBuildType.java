
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             This is used to provide statistics about shipments groups that were created during a given run of bulk trailer build.
 *          
 * 
 * <p>Java class for BulkTrailerBuildType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BulkTrailerBuildType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="BulkTrailerBuildGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="QueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentsSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentsBuiltToGroups" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentsNotInGroups" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentGroupsBuilt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BulkTrailerBuildType", propOrder = {
    "sendReason",
    "bulkTrailerBuildGid",
    "queryName",
    "startTime",
    "endTime",
    "numOfShipmentsSelected",
    "numOfShipmentsBuiltToGroups",
    "numOfShipmentsNotInGroups",
    "numOfShipmentGroupsBuilt"
})
public class BulkTrailerBuildType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "BulkTrailerBuildGid", required = true)
    protected GLogXMLGidType bulkTrailerBuildGid;
    @XmlElement(name = "QueryName")
    protected String queryName;
    @XmlElement(name = "StartTime")
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime")
    protected GLogDateTimeType endTime;
    @XmlElement(name = "NumOfShipmentsSelected")
    protected String numOfShipmentsSelected;
    @XmlElement(name = "NumOfShipmentsBuiltToGroups")
    protected String numOfShipmentsBuiltToGroups;
    @XmlElement(name = "NumOfShipmentsNotInGroups")
    protected String numOfShipmentsNotInGroups;
    @XmlElement(name = "NumOfShipmentGroupsBuilt")
    protected String numOfShipmentGroupsBuilt;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the bulkTrailerBuildGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkTrailerBuildGid() {
        return bulkTrailerBuildGid;
    }

    /**
     * Sets the value of the bulkTrailerBuildGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkTrailerBuildGid(GLogXMLGidType value) {
        this.bulkTrailerBuildGid = value;
    }

    /**
     * Gets the value of the queryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryName() {
        return queryName;
    }

    /**
     * Sets the value of the queryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryName(String value) {
        this.queryName = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the endTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Sets the value of the endTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Gets the value of the numOfShipmentsSelected property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsSelected() {
        return numOfShipmentsSelected;
    }

    /**
     * Sets the value of the numOfShipmentsSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsSelected(String value) {
        this.numOfShipmentsSelected = value;
    }

    /**
     * Gets the value of the numOfShipmentsBuiltToGroups property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsBuiltToGroups() {
        return numOfShipmentsBuiltToGroups;
    }

    /**
     * Sets the value of the numOfShipmentsBuiltToGroups property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsBuiltToGroups(String value) {
        this.numOfShipmentsBuiltToGroups = value;
    }

    /**
     * Gets the value of the numOfShipmentsNotInGroups property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsNotInGroups() {
        return numOfShipmentsNotInGroups;
    }

    /**
     * Sets the value of the numOfShipmentsNotInGroups property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsNotInGroups(String value) {
        this.numOfShipmentsNotInGroups = value;
    }

    /**
     * Gets the value of the numOfShipmentGroupsBuilt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentGroupsBuilt() {
        return numOfShipmentGroupsBuilt;
    }

    /**
     * Sets the value of the numOfShipmentGroupsBuilt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentGroupsBuilt(String value) {
        this.numOfShipmentGroupsBuilt = value;
    }

}

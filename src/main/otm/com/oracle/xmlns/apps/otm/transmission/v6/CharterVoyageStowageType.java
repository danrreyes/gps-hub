
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Represents a compartment within the vessel on a charter voyage. A ocean carrier specifies the available capacities
 *             on the vessel by stowage mode. The shippers provide their stowage mode preferences for their transport orders.
 *          
 * 
 * <p>Java class for CharterVoyageStowageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CharterVoyageStowageType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StowageModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SourceTerminalLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="DestTerminalLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="TerminalStartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="TerminalCloseTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="TerminalCloseTimeNonstuff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="MaterialAvailTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="MaterialAvailTimeDestuff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="BookingReferenceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Consol" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ConsolType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CharterVoyageStowageType", propOrder = {
    "stowageModeGid",
    "sourceTerminalLocationRef",
    "destTerminalLocationRef",
    "terminalStartTime",
    "terminalCloseTime",
    "terminalCloseTimeNonstuff",
    "materialAvailTime",
    "materialAvailTimeDestuff",
    "bookingReferenceNum",
    "consol"
})
public class CharterVoyageStowageType {

    @XmlElement(name = "StowageModeGid")
    protected GLogXMLGidType stowageModeGid;
    @XmlElement(name = "SourceTerminalLocationRef")
    protected GLogXMLLocRefType sourceTerminalLocationRef;
    @XmlElement(name = "DestTerminalLocationRef")
    protected GLogXMLLocRefType destTerminalLocationRef;
    @XmlElement(name = "TerminalStartTime")
    protected GLogXMLDurationType terminalStartTime;
    @XmlElement(name = "TerminalCloseTime")
    protected GLogXMLDurationType terminalCloseTime;
    @XmlElement(name = "TerminalCloseTimeNonstuff")
    protected GLogXMLDurationType terminalCloseTimeNonstuff;
    @XmlElement(name = "MaterialAvailTime")
    protected GLogXMLDurationType materialAvailTime;
    @XmlElement(name = "MaterialAvailTimeDestuff")
    protected GLogXMLDurationType materialAvailTimeDestuff;
    @XmlElement(name = "BookingReferenceNum")
    protected String bookingReferenceNum;
    @XmlElement(name = "Consol")
    protected List<ConsolType> consol;

    /**
     * Gets the value of the stowageModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStowageModeGid() {
        return stowageModeGid;
    }

    /**
     * Sets the value of the stowageModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStowageModeGid(GLogXMLGidType value) {
        this.stowageModeGid = value;
    }

    /**
     * Gets the value of the sourceTerminalLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getSourceTerminalLocationRef() {
        return sourceTerminalLocationRef;
    }

    /**
     * Sets the value of the sourceTerminalLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setSourceTerminalLocationRef(GLogXMLLocRefType value) {
        this.sourceTerminalLocationRef = value;
    }

    /**
     * Gets the value of the destTerminalLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestTerminalLocationRef() {
        return destTerminalLocationRef;
    }

    /**
     * Sets the value of the destTerminalLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestTerminalLocationRef(GLogXMLLocRefType value) {
        this.destTerminalLocationRef = value;
    }

    /**
     * Gets the value of the terminalStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTerminalStartTime() {
        return terminalStartTime;
    }

    /**
     * Sets the value of the terminalStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTerminalStartTime(GLogXMLDurationType value) {
        this.terminalStartTime = value;
    }

    /**
     * Gets the value of the terminalCloseTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTerminalCloseTime() {
        return terminalCloseTime;
    }

    /**
     * Sets the value of the terminalCloseTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTerminalCloseTime(GLogXMLDurationType value) {
        this.terminalCloseTime = value;
    }

    /**
     * Gets the value of the terminalCloseTimeNonstuff property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTerminalCloseTimeNonstuff() {
        return terminalCloseTimeNonstuff;
    }

    /**
     * Sets the value of the terminalCloseTimeNonstuff property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTerminalCloseTimeNonstuff(GLogXMLDurationType value) {
        this.terminalCloseTimeNonstuff = value;
    }

    /**
     * Gets the value of the materialAvailTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaterialAvailTime() {
        return materialAvailTime;
    }

    /**
     * Sets the value of the materialAvailTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaterialAvailTime(GLogXMLDurationType value) {
        this.materialAvailTime = value;
    }

    /**
     * Gets the value of the materialAvailTimeDestuff property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaterialAvailTimeDestuff() {
        return materialAvailTimeDestuff;
    }

    /**
     * Sets the value of the materialAvailTimeDestuff property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaterialAvailTimeDestuff(GLogXMLDurationType value) {
        this.materialAvailTimeDestuff = value;
    }

    /**
     * Gets the value of the bookingReferenceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBookingReferenceNum() {
        return bookingReferenceNum;
    }

    /**
     * Sets the value of the bookingReferenceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBookingReferenceNum(String value) {
        this.bookingReferenceNum = value;
    }

    /**
     * Gets the value of the consol property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the consol property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsol().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsolType }
     * 
     * 
     */
    public List<ConsolType> getConsol() {
        if (consol == null) {
            consol = new ArrayList<ConsolType>();
        }
        return this.consol;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to specify information modified by the service provider as part of the booking.
 *             Note: The PlannerValue is sent outbound in the TenderOffer and is not needed in the TenderResponse.
 *             The ServiceProviderValue is not sent outbound in the TenderOffer, but is required in the TenderResponse.
 *          
 * 
 * <p>Java class for CondBookingFieldInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CondBookingFieldInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConditionalBookingFieldGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PlannerValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UOMCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OrderSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StopNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationLongName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TimeQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransportStageQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LocationAlias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CondBookingFieldInfoType", propOrder = {
    "conditionalBookingFieldGid",
    "plannerValue",
    "serviceProviderValue",
    "uomCode",
    "orderSequence",
    "locationGid",
    "stopNumber",
    "locationLongName",
    "locationQualifierGid",
    "timeQualifierGid",
    "transportStageQualifierGid",
    "locationAlias"
})
public class CondBookingFieldInfoType {

    @XmlElement(name = "ConditionalBookingFieldGid", required = true)
    protected GLogXMLGidType conditionalBookingFieldGid;
    @XmlElement(name = "PlannerValue")
    protected String plannerValue;
    @XmlElement(name = "ServiceProviderValue")
    protected String serviceProviderValue;
    @XmlElement(name = "UOMCode")
    protected String uomCode;
    @XmlElement(name = "OrderSequence")
    protected String orderSequence;
    @XmlElement(name = "LocationGid")
    protected GLogXMLGidType locationGid;
    @XmlElement(name = "StopNumber")
    protected String stopNumber;
    @XmlElement(name = "LocationLongName")
    protected String locationLongName;
    @XmlElement(name = "LocationQualifierGid")
    protected GLogXMLGidType locationQualifierGid;
    @XmlElement(name = "TimeQualifierGid")
    protected GLogXMLGidType timeQualifierGid;
    @XmlElement(name = "TransportStageQualifierGid")
    protected GLogXMLGidType transportStageQualifierGid;
    @XmlElement(name = "LocationAlias")
    protected String locationAlias;

    /**
     * Gets the value of the conditionalBookingFieldGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConditionalBookingFieldGid() {
        return conditionalBookingFieldGid;
    }

    /**
     * Sets the value of the conditionalBookingFieldGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConditionalBookingFieldGid(GLogXMLGidType value) {
        this.conditionalBookingFieldGid = value;
    }

    /**
     * Gets the value of the plannerValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlannerValue() {
        return plannerValue;
    }

    /**
     * Sets the value of the plannerValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlannerValue(String value) {
        this.plannerValue = value;
    }

    /**
     * Gets the value of the serviceProviderValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderValue() {
        return serviceProviderValue;
    }

    /**
     * Sets the value of the serviceProviderValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderValue(String value) {
        this.serviceProviderValue = value;
    }

    /**
     * Gets the value of the uomCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOMCode() {
        return uomCode;
    }

    /**
     * Sets the value of the uomCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOMCode(String value) {
        this.uomCode = value;
    }

    /**
     * Gets the value of the orderSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderSequence() {
        return orderSequence;
    }

    /**
     * Sets the value of the orderSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderSequence(String value) {
        this.orderSequence = value;
    }

    /**
     * Gets the value of the locationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationGid() {
        return locationGid;
    }

    /**
     * Sets the value of the locationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationGid(GLogXMLGidType value) {
        this.locationGid = value;
    }

    /**
     * Gets the value of the stopNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopNumber() {
        return stopNumber;
    }

    /**
     * Sets the value of the stopNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopNumber(String value) {
        this.stopNumber = value;
    }

    /**
     * Gets the value of the locationLongName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationLongName() {
        return locationLongName;
    }

    /**
     * Sets the value of the locationLongName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationLongName(String value) {
        this.locationLongName = value;
    }

    /**
     * Gets the value of the locationQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationQualifierGid() {
        return locationQualifierGid;
    }

    /**
     * Sets the value of the locationQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationQualifierGid(GLogXMLGidType value) {
        this.locationQualifierGid = value;
    }

    /**
     * Gets the value of the timeQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeQualifierGid() {
        return timeQualifierGid;
    }

    /**
     * Sets the value of the timeQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeQualifierGid(GLogXMLGidType value) {
        this.timeQualifierGid = value;
    }

    /**
     * Gets the value of the transportStageQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportStageQualifierGid() {
        return transportStageQualifierGid;
    }

    /**
     * Sets the value of the transportStageQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportStageQualifierGid(GLogXMLGidType value) {
        this.transportStageQualifierGid = value;
    }

    /**
     * Gets the value of the locationAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationAlias() {
        return locationAlias;
    }

    /**
     * Sets the value of the locationAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationAlias(String value) {
        this.locationAlias = value;
    }

}

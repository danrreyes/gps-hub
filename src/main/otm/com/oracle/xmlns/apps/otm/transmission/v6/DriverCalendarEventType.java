
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Describes a calender event for a specific driver
 * 
 * <p>Java class for DriverCalendarEventType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverCalendarEventType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="DriverCalEventGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="EventStartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="EventEndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="EventLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CalendarEventTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverCalendarEventType", propOrder = {
    "intSavedQuery",
    "driverCalEventGid",
    "transactionCode",
    "driverGid",
    "eventStartTime",
    "eventEndTime",
    "eventLocGid",
    "calendarEventTypeGid"
})
public class DriverCalendarEventType
    extends OTMTransactionIn
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "DriverCalEventGid")
    protected GLogXMLGidType driverCalEventGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "DriverGid", required = true)
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "EventStartTime", required = true)
    protected GLogDateTimeType eventStartTime;
    @XmlElement(name = "EventEndTime", required = true)
    protected GLogDateTimeType eventEndTime;
    @XmlElement(name = "EventLocGid")
    protected GLogXMLGidType eventLocGid;
    @XmlElement(name = "CalendarEventTypeGid", required = true)
    protected GLogXMLGidType calendarEventTypeGid;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the driverCalEventGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverCalEventGid() {
        return driverCalEventGid;
    }

    /**
     * Sets the value of the driverCalEventGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverCalEventGid(GLogXMLGidType value) {
        this.driverCalEventGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the driverGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Sets the value of the driverGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Gets the value of the eventStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventStartTime() {
        return eventStartTime;
    }

    /**
     * Sets the value of the eventStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventStartTime(GLogDateTimeType value) {
        this.eventStartTime = value;
    }

    /**
     * Gets the value of the eventEndTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventEndTime() {
        return eventEndTime;
    }

    /**
     * Sets the value of the eventEndTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventEndTime(GLogDateTimeType value) {
        this.eventEndTime = value;
    }

    /**
     * Gets the value of the eventLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEventLocGid() {
        return eventLocGid;
    }

    /**
     * Sets the value of the eventLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEventLocGid(GLogXMLGidType value) {
        this.eventLocGid = value;
    }

    /**
     * Gets the value of the calendarEventTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarEventTypeGid() {
        return calendarEventTypeGid;
    }

    /**
     * Sets the value of the calendarEventTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarEventTypeGid(GLogXMLGidType value) {
        this.calendarEventTypeGid = value;
    }

}

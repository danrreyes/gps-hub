
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to take order related data and return order routing rule data.
 *             Note: The PackagedItemGid is required when the GetFreightCost = Y.
 *          
 * 
 * <p>Java class for OrderRoutingRuleQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderRoutingRuleQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SourceAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/&gt;
 *         &lt;element name="DestAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageCorporationType"/&gt;
 *         &lt;element name="EstDepartureDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EstArrivalDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType" minOccurs="0"/&gt;
 *         &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType" minOccurs="0"/&gt;
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderRoutingRuleQueryType", propOrder = {
    "sourceAddress",
    "destAddress",
    "estDepartureDt",
    "estArrivalDt",
    "weight",
    "volume",
    "paymentMethodCodeGid"
})
public class OrderRoutingRuleQueryType {

    @XmlElement(name = "SourceAddress", required = true)
    protected MileageCorporationType sourceAddress;
    @XmlElement(name = "DestAddress", required = true)
    protected MileageCorporationType destAddress;
    @XmlElement(name = "EstDepartureDt")
    protected GLogDateTimeType estDepartureDt;
    @XmlElement(name = "EstArrivalDt")
    protected GLogDateTimeType estArrivalDt;
    @XmlElement(name = "Weight")
    protected WeightType weight;
    @XmlElement(name = "Volume")
    protected VolumeType volume;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;

    /**
     * Gets the value of the sourceAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getSourceAddress() {
        return sourceAddress;
    }

    /**
     * Sets the value of the sourceAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setSourceAddress(MileageCorporationType value) {
        this.sourceAddress = value;
    }

    /**
     * Gets the value of the destAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MileageCorporationType }
     *     
     */
    public MileageCorporationType getDestAddress() {
        return destAddress;
    }

    /**
     * Sets the value of the destAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageCorporationType }
     *     
     */
    public void setDestAddress(MileageCorporationType value) {
        this.destAddress = value;
    }

    /**
     * Gets the value of the estDepartureDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstDepartureDt() {
        return estDepartureDt;
    }

    /**
     * Sets the value of the estDepartureDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstDepartureDt(GLogDateTimeType value) {
        this.estDepartureDt = value;
    }

    /**
     * Gets the value of the estArrivalDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstArrivalDt() {
        return estArrivalDt;
    }

    /**
     * Sets the value of the estArrivalDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstArrivalDt(GLogDateTimeType value) {
        this.estArrivalDt = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link WeightType }
     *     
     */
    public WeightType getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightType }
     *     
     */
    public void setWeight(WeightType value) {
        this.weight = value;
    }

    /**
     * Gets the value of the volume property.
     * 
     * @return
     *     possible object is
     *     {@link VolumeType }
     *     
     */
    public VolumeType getVolume() {
        return volume;
    }

    /**
     * Sets the value of the volume property.
     * 
     * @param value
     *     allowed object is
     *     {@link VolumeType }
     *     
     */
    public void setVolume(VolumeType value) {
        this.volume = value;
    }

    /**
     * Gets the value of the paymentMethodCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Sets the value of the paymentMethodCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

}

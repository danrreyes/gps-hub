
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies vessel information. The CountryCode3Gid element corresponds to the Vessel Country Code.
 *             The LocationRef element corresponds to the vessel port of registry.
 *          
 * 
 * <p>Java class for VesselType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VesselType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VesselGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="VesselName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Operator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" minOccurs="0"/&gt;
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OfficialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CallSign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IMONumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ClassSocietyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="YearBuilt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsInService" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VesselType", propOrder = {
    "vesselGid",
    "vesselName",
    "owner",
    "operator",
    "locationRef",
    "countryCode3Gid",
    "officialNumber",
    "callSign",
    "imoNumber",
    "classSocietyNumber",
    "yearBuilt",
    "isInService",
    "refnum"
})
public class VesselType {

    @XmlElement(name = "VesselGid", required = true)
    protected GLogXMLGidType vesselGid;
    @XmlElement(name = "VesselName", required = true)
    protected String vesselName;
    @XmlElement(name = "Owner")
    protected String owner;
    @XmlElement(name = "Operator")
    protected String operator;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "CountryCode3Gid")
    protected GLogXMLGidType countryCode3Gid;
    @XmlElement(name = "OfficialNumber")
    protected String officialNumber;
    @XmlElement(name = "CallSign")
    protected String callSign;
    @XmlElement(name = "IMONumber")
    protected String imoNumber;
    @XmlElement(name = "ClassSocietyNumber")
    protected String classSocietyNumber;
    @XmlElement(name = "YearBuilt")
    protected String yearBuilt;
    @XmlElement(name = "IsInService", required = true)
    protected String isInService;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Gets the value of the vesselGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVesselGid() {
        return vesselGid;
    }

    /**
     * Sets the value of the vesselGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVesselGid(GLogXMLGidType value) {
        this.vesselGid = value;
    }

    /**
     * Gets the value of the vesselName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVesselName() {
        return vesselName;
    }

    /**
     * Sets the value of the vesselName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVesselName(String value) {
        this.vesselName = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the operator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperator() {
        return operator;
    }

    /**
     * Sets the value of the operator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperator(String value) {
        this.operator = value;
    }

    /**
     * Gets the value of the locationRef property.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Sets the value of the locationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Gets the value of the countryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Sets the value of the countryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

    /**
     * Gets the value of the officialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficialNumber() {
        return officialNumber;
    }

    /**
     * Sets the value of the officialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficialNumber(String value) {
        this.officialNumber = value;
    }

    /**
     * Gets the value of the callSign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCallSign() {
        return callSign;
    }

    /**
     * Sets the value of the callSign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCallSign(String value) {
        this.callSign = value;
    }

    /**
     * Gets the value of the imoNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIMONumber() {
        return imoNumber;
    }

    /**
     * Sets the value of the imoNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIMONumber(String value) {
        this.imoNumber = value;
    }

    /**
     * Gets the value of the classSocietyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassSocietyNumber() {
        return classSocietyNumber;
    }

    /**
     * Sets the value of the classSocietyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassSocietyNumber(String value) {
        this.classSocietyNumber = value;
    }

    /**
     * Gets the value of the yearBuilt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYearBuilt() {
        return yearBuilt;
    }

    /**
     * Sets the value of the yearBuilt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYearBuilt(String value) {
        this.yearBuilt = value;
    }

    /**
     * Gets the value of the isInService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInService() {
        return isInService;
    }

    /**
     * Sets the value of the isInService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInService(String value) {
        this.isInService = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

}

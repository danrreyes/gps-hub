
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * This element is supported on the outbound only.
 * 
 * <p>Java class for OBShipUnitType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OBShipUnitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OBShipUnitType", propOrder = {
    "sendReason",
    "shipUnit"
})
public class OBShipUnitType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "ShipUnit", required = true)
    protected ShipUnitType shipUnit;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitType }
     *     
     */
    public ShipUnitType getShipUnit() {
        return shipUnit;
    }

    /**
     * Sets the value of the shipUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitType }
     *     
     */
    public void setShipUnit(ShipUnitType value) {
        this.shipUnit = value;
    }

}

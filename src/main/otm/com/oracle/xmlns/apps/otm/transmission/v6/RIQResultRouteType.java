
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Rate Inquiry results when the RIQ Route option is used.
 * 
 * <p>Java class for RIQResultRouteType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RIQResultRouteType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="StartTimestamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWithTimezoneType" minOccurs="0"/&gt;
 *         &lt;element name="EndTimestamp" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWithTimezoneType" minOccurs="0"/&gt;
 *         &lt;element name="TotalActualCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="IsFeasible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RIQRouteShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQRouteShipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Perspective" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsOptimalResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LeadTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="TotalActualCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightedCostPerUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLCostPerUOM" minOccurs="0"/&gt;
 *         &lt;element name="TotalWeightedCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TotalTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="OrigCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DelivCarrierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="VesselGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfLoadLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PortOfDisLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="PrimarySourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryTotalCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryTransitTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="CommitmentCountPlanned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CommitmentCountActual" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrimaryRateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipToLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RIQSecondaryCharges" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQSecondaryChargesType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQResultRouteType", propOrder = {
    "itineraryGid",
    "startTimestamp",
    "endTimestamp",
    "totalActualCost",
    "isFeasible",
    "riqRouteShipment",
    "perspective",
    "isOptimalResult",
    "leadTime",
    "totalActualCostPerUOM",
    "totalWeightedCostPerUOM",
    "totalWeightedCost",
    "transportModeGid",
    "serviceProviderGid",
    "totalTransitTime",
    "origCarrierGid",
    "delivCarrierGid",
    "equipmentGroupGid",
    "routeCodeGid",
    "vesselGid",
    "portOfLoadLocationRef",
    "portOfDisLocationRef",
    "primarySourceLocationGid",
    "primaryDestLocationGid",
    "primaryTotalCost",
    "voyageGid",
    "primaryTransitTime",
    "commitmentCountPlanned",
    "commitmentCountActual",
    "primaryRateOfferingGid",
    "primaryRateGeoGid",
    "commodityGid",
    "shipFromLocationGid",
    "shipToLocationGid",
    "riqSecondaryCharges"
})
public class RIQResultRouteType {

    @XmlElement(name = "ItineraryGid", required = true)
    protected GLogXMLGidType itineraryGid;
    @XmlElement(name = "StartTimestamp")
    protected TimeWithTimezoneType startTimestamp;
    @XmlElement(name = "EndTimestamp")
    protected TimeWithTimezoneType endTimestamp;
    @XmlElement(name = "TotalActualCost")
    protected GLogXMLFinancialAmountType totalActualCost;
    @XmlElement(name = "IsFeasible")
    protected String isFeasible;
    @XmlElement(name = "RIQRouteShipment")
    protected List<RIQRouteShipmentType> riqRouteShipment;
    @XmlElement(name = "Perspective")
    protected String perspective;
    @XmlElement(name = "IsOptimalResult")
    protected String isOptimalResult;
    @XmlElement(name = "LeadTime")
    protected GLogXMLDurationType leadTime;
    @XmlElement(name = "TotalActualCostPerUOM")
    protected GLogXMLCostPerUOM totalActualCostPerUOM;
    @XmlElement(name = "TotalWeightedCostPerUOM")
    protected GLogXMLCostPerUOM totalWeightedCostPerUOM;
    @XmlElement(name = "TotalWeightedCost")
    protected GLogXMLFinancialAmountType totalWeightedCost;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "TotalTransitTime")
    protected GLogXMLDurationType totalTransitTime;
    @XmlElement(name = "OrigCarrierGid")
    protected GLogXMLGidType origCarrierGid;
    @XmlElement(name = "DelivCarrierGid")
    protected GLogXMLGidType delivCarrierGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "RouteCodeGid")
    protected GLogXMLGidType routeCodeGid;
    @XmlElement(name = "VesselGid")
    protected GLogXMLGidType vesselGid;
    @XmlElement(name = "PortOfLoadLocationRef")
    protected GLogXMLLocRefType portOfLoadLocationRef;
    @XmlElement(name = "PortOfDisLocationRef")
    protected GLogXMLLocRefType portOfDisLocationRef;
    @XmlElement(name = "PrimarySourceLocationGid")
    protected GLogXMLGidType primarySourceLocationGid;
    @XmlElement(name = "PrimaryDestLocationGid")
    protected GLogXMLGidType primaryDestLocationGid;
    @XmlElement(name = "PrimaryTotalCost")
    protected GLogXMLFinancialAmountType primaryTotalCost;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "PrimaryTransitTime")
    protected GLogXMLDurationType primaryTransitTime;
    @XmlElement(name = "CommitmentCountPlanned")
    protected String commitmentCountPlanned;
    @XmlElement(name = "CommitmentCountActual")
    protected String commitmentCountActual;
    @XmlElement(name = "PrimaryRateOfferingGid")
    protected GLogXMLGidType primaryRateOfferingGid;
    @XmlElement(name = "PrimaryRateGeoGid")
    protected GLogXMLGidType primaryRateGeoGid;
    @XmlElement(name = "CommodityGid")
    protected GLogXMLGidType commodityGid;
    @XmlElement(name = "ShipFromLocationGid")
    protected GLogXMLGidType shipFromLocationGid;
    @XmlElement(name = "ShipToLocationGid")
    protected GLogXMLGidType shipToLocationGid;
    @XmlElement(name = "RIQSecondaryCharges")
    protected RIQSecondaryChargesType riqSecondaryCharges;

    /**
     * Gets the value of the itineraryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryGid() {
        return itineraryGid;
    }

    /**
     * Sets the value of the itineraryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryGid(GLogXMLGidType value) {
        this.itineraryGid = value;
    }

    /**
     * Gets the value of the startTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public TimeWithTimezoneType getStartTimestamp() {
        return startTimestamp;
    }

    /**
     * Sets the value of the startTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public void setStartTimestamp(TimeWithTimezoneType value) {
        this.startTimestamp = value;
    }

    /**
     * Gets the value of the endTimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public TimeWithTimezoneType getEndTimestamp() {
        return endTimestamp;
    }

    /**
     * Sets the value of the endTimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWithTimezoneType }
     *     
     */
    public void setEndTimestamp(TimeWithTimezoneType value) {
        this.endTimestamp = value;
    }

    /**
     * Gets the value of the totalActualCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalActualCost() {
        return totalActualCost;
    }

    /**
     * Sets the value of the totalActualCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalActualCost(GLogXMLFinancialAmountType value) {
        this.totalActualCost = value;
    }

    /**
     * Gets the value of the isFeasible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFeasible() {
        return isFeasible;
    }

    /**
     * Sets the value of the isFeasible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFeasible(String value) {
        this.isFeasible = value;
    }

    /**
     * Gets the value of the riqRouteShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the riqRouteShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRIQRouteShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RIQRouteShipmentType }
     * 
     * 
     */
    public List<RIQRouteShipmentType> getRIQRouteShipment() {
        if (riqRouteShipment == null) {
            riqRouteShipment = new ArrayList<RIQRouteShipmentType>();
        }
        return this.riqRouteShipment;
    }

    /**
     * Gets the value of the perspective property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerspective() {
        return perspective;
    }

    /**
     * Sets the value of the perspective property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerspective(String value) {
        this.perspective = value;
    }

    /**
     * Gets the value of the isOptimalResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOptimalResult() {
        return isOptimalResult;
    }

    /**
     * Sets the value of the isOptimalResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOptimalResult(String value) {
        this.isOptimalResult = value;
    }

    /**
     * Gets the value of the leadTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getLeadTime() {
        return leadTime;
    }

    /**
     * Sets the value of the leadTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setLeadTime(GLogXMLDurationType value) {
        this.leadTime = value;
    }

    /**
     * Gets the value of the totalActualCostPerUOM property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalActualCostPerUOM() {
        return totalActualCostPerUOM;
    }

    /**
     * Sets the value of the totalActualCostPerUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalActualCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalActualCostPerUOM = value;
    }

    /**
     * Gets the value of the totalWeightedCostPerUOM property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public GLogXMLCostPerUOM getTotalWeightedCostPerUOM() {
        return totalWeightedCostPerUOM;
    }

    /**
     * Sets the value of the totalWeightedCostPerUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLCostPerUOM }
     *     
     */
    public void setTotalWeightedCostPerUOM(GLogXMLCostPerUOM value) {
        this.totalWeightedCostPerUOM = value;
    }

    /**
     * Gets the value of the totalWeightedCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalWeightedCost() {
        return totalWeightedCost;
    }

    /**
     * Sets the value of the totalWeightedCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalWeightedCost(GLogXMLFinancialAmountType value) {
        this.totalWeightedCost = value;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Sets the value of the transportModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the totalTransitTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTotalTransitTime() {
        return totalTransitTime;
    }

    /**
     * Sets the value of the totalTransitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTotalTransitTime(GLogXMLDurationType value) {
        this.totalTransitTime = value;
    }

    /**
     * Gets the value of the origCarrierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOrigCarrierGid() {
        return origCarrierGid;
    }

    /**
     * Sets the value of the origCarrierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOrigCarrierGid(GLogXMLGidType value) {
        this.origCarrierGid = value;
    }

    /**
     * Gets the value of the delivCarrierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDelivCarrierGid() {
        return delivCarrierGid;
    }

    /**
     * Sets the value of the delivCarrierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDelivCarrierGid(GLogXMLGidType value) {
        this.delivCarrierGid = value;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Sets the value of the equipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Gets the value of the routeCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeGid() {
        return routeCodeGid;
    }

    /**
     * Sets the value of the routeCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeGid(GLogXMLGidType value) {
        this.routeCodeGid = value;
    }

    /**
     * Gets the value of the vesselGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVesselGid() {
        return vesselGid;
    }

    /**
     * Sets the value of the vesselGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVesselGid(GLogXMLGidType value) {
        this.vesselGid = value;
    }

    /**
     * Gets the value of the portOfLoadLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfLoadLocationRef() {
        return portOfLoadLocationRef;
    }

    /**
     * Sets the value of the portOfLoadLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfLoadLocationRef(GLogXMLLocRefType value) {
        this.portOfLoadLocationRef = value;
    }

    /**
     * Gets the value of the portOfDisLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getPortOfDisLocationRef() {
        return portOfDisLocationRef;
    }

    /**
     * Sets the value of the portOfDisLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setPortOfDisLocationRef(GLogXMLLocRefType value) {
        this.portOfDisLocationRef = value;
    }

    /**
     * Gets the value of the primarySourceLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimarySourceLocationGid() {
        return primarySourceLocationGid;
    }

    /**
     * Sets the value of the primarySourceLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimarySourceLocationGid(GLogXMLGidType value) {
        this.primarySourceLocationGid = value;
    }

    /**
     * Gets the value of the primaryDestLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimaryDestLocationGid() {
        return primaryDestLocationGid;
    }

    /**
     * Sets the value of the primaryDestLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimaryDestLocationGid(GLogXMLGidType value) {
        this.primaryDestLocationGid = value;
    }

    /**
     * Gets the value of the primaryTotalCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPrimaryTotalCost() {
        return primaryTotalCost;
    }

    /**
     * Sets the value of the primaryTotalCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPrimaryTotalCost(GLogXMLFinancialAmountType value) {
        this.primaryTotalCost = value;
    }

    /**
     * Gets the value of the voyageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Sets the value of the voyageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Gets the value of the primaryTransitTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getPrimaryTransitTime() {
        return primaryTransitTime;
    }

    /**
     * Sets the value of the primaryTransitTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setPrimaryTransitTime(GLogXMLDurationType value) {
        this.primaryTransitTime = value;
    }

    /**
     * Gets the value of the commitmentCountPlanned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommitmentCountPlanned() {
        return commitmentCountPlanned;
    }

    /**
     * Sets the value of the commitmentCountPlanned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommitmentCountPlanned(String value) {
        this.commitmentCountPlanned = value;
    }

    /**
     * Gets the value of the commitmentCountActual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommitmentCountActual() {
        return commitmentCountActual;
    }

    /**
     * Sets the value of the commitmentCountActual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommitmentCountActual(String value) {
        this.commitmentCountActual = value;
    }

    /**
     * Gets the value of the primaryRateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimaryRateOfferingGid() {
        return primaryRateOfferingGid;
    }

    /**
     * Sets the value of the primaryRateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimaryRateOfferingGid(GLogXMLGidType value) {
        this.primaryRateOfferingGid = value;
    }

    /**
     * Gets the value of the primaryRateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrimaryRateGeoGid() {
        return primaryRateGeoGid;
    }

    /**
     * Sets the value of the primaryRateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrimaryRateGeoGid(GLogXMLGidType value) {
        this.primaryRateGeoGid = value;
    }

    /**
     * Gets the value of the commodityGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommodityGid() {
        return commodityGid;
    }

    /**
     * Sets the value of the commodityGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommodityGid(GLogXMLGidType value) {
        this.commodityGid = value;
    }

    /**
     * Gets the value of the shipFromLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipFromLocationGid() {
        return shipFromLocationGid;
    }

    /**
     * Sets the value of the shipFromLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipFromLocationGid(GLogXMLGidType value) {
        this.shipFromLocationGid = value;
    }

    /**
     * Gets the value of the shipToLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipToLocationGid() {
        return shipToLocationGid;
    }

    /**
     * Sets the value of the shipToLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipToLocationGid(GLogXMLGidType value) {
        this.shipToLocationGid = value;
    }

    /**
     * Gets the value of the riqSecondaryCharges property.
     * 
     * @return
     *     possible object is
     *     {@link RIQSecondaryChargesType }
     *     
     */
    public RIQSecondaryChargesType getRIQSecondaryCharges() {
        return riqSecondaryCharges;
    }

    /**
     * Sets the value of the riqSecondaryCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQSecondaryChargesType }
     *     
     */
    public void setRIQSecondaryCharges(RIQSecondaryChargesType value) {
        this.riqSecondaryCharges = value;
    }

}

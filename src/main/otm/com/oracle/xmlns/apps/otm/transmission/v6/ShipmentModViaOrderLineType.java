
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Provides an alternative for updating actual quantites for a Shipment by referencing the order line.
 *             The ShipModViaOrderLineMatch element provides the fields that are used to match the affected ship units.
 *             The quantities on the ship units that are matched are updated according to the new value. Note that the action may
 *             delete existing ship units or add new ship units as needed to apply the change.
 *             Warning: Using this feature may impact or overwrite any changes made via the Shipment.ShipUnit element.
 * 
 *             The TransactionCode is used to specify if this is a modification, or the order line or ship unit should be added or removed.
 *          
 * 
 * <p>Java class for ShipmentModViaOrderLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentModViaOrderLineType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipModViaOrderLineMatch" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipModViaOrderLineMatchType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="AffectsCurrentLegOnly" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentModViaOrderLineType", propOrder = {
    "shipModViaOrderLineMatch",
    "transactionCode",
    "affectsCurrentLegOnly",
    "weightVolume",
    "packagedItemCount"
})
public class ShipmentModViaOrderLineType {

    @XmlElement(name = "ShipModViaOrderLineMatch", required = true)
    protected ShipModViaOrderLineMatchType shipModViaOrderLineMatch;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "AffectsCurrentLegOnly")
    protected String affectsCurrentLegOnly;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "PackagedItemCount")
    protected String packagedItemCount;

    /**
     * Gets the value of the shipModViaOrderLineMatch property.
     * 
     * @return
     *     possible object is
     *     {@link ShipModViaOrderLineMatchType }
     *     
     */
    public ShipModViaOrderLineMatchType getShipModViaOrderLineMatch() {
        return shipModViaOrderLineMatch;
    }

    /**
     * Sets the value of the shipModViaOrderLineMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipModViaOrderLineMatchType }
     *     
     */
    public void setShipModViaOrderLineMatch(ShipModViaOrderLineMatchType value) {
        this.shipModViaOrderLineMatch = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the affectsCurrentLegOnly property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAffectsCurrentLegOnly() {
        return affectsCurrentLegOnly;
    }

    /**
     * Sets the value of the affectsCurrentLegOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAffectsCurrentLegOnly(String value) {
        this.affectsCurrentLegOnly = value;
    }

    /**
     * Gets the value of the weightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Sets the value of the weightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Gets the value of the packagedItemCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemCount() {
        return packagedItemCount;
    }

    /**
     * Sets the value of the packagedItemCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemCount(String value) {
        this.packagedItemCount = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AckSpecType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AckSpecType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SmtpHost" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ServletURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AckOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AckSpecType", propOrder = {
    "comMethodGid",
    "emailAddress",
    "smtpHost",
    "servletURL",
    "ackOption",
    "contactGid"
})
public class AckSpecType {

    @XmlElement(name = "ComMethodGid", required = true)
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;
    @XmlElement(name = "SmtpHost")
    protected String smtpHost;
    @XmlElement(name = "ServletURL")
    protected String servletURL;
    @XmlElement(name = "AckOption")
    protected String ackOption;
    @XmlElement(name = "ContactGid")
    protected GLogXMLGidType contactGid;

    /**
     * Gets the value of the comMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Sets the value of the comMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the smtpHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmtpHost() {
        return smtpHost;
    }

    /**
     * Sets the value of the smtpHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmtpHost(String value) {
        this.smtpHost = value;
    }

    /**
     * Gets the value of the servletURL property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServletURL() {
        return servletURL;
    }

    /**
     * Sets the value of the servletURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServletURL(String value) {
        this.servletURL = value;
    }

    /**
     * Gets the value of the ackOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAckOption() {
        return ackOption;
    }

    /**
     * Sets the value of the ackOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAckOption(String value) {
        this.ackOption = value;
    }

    /**
     * Gets the value of the contactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Sets the value of the contactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

}

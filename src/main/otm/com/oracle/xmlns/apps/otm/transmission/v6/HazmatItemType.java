
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Used to identify hazardous materials information that is item centric, or related to an item.
 *          
 * 
 * <p>Java class for HazmatItemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HazmatItemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HazmatItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="HazmatModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="HazmatGenericGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatGeneric" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatGenericType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HotIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ResidueIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DotExemptionGid" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AdditionalTransportMsg1Gid" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="AdditionalTransportMsg2Gid" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="Stcc49CodeGid"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="STCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CompetentAuthorityCodeGid" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="HazmatCommonInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatCommonInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ProperShippingNamePrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPoison" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatItemType", propOrder = {
    "hazmatItemGid",
    "transactionCode",
    "packagedItemGid",
    "regionGid",
    "hazmatModeGid",
    "hazmatGenericGid",
    "hazmatGeneric",
    "hazmatIndicator",
    "hotIndicator",
    "residueIndicator",
    "dotExemptionGid",
    "additionalTransportMsg1Gid",
    "additionalTransportMsg2Gid",
    "stcc49CodeGid",
    "competentAuthorityCodeGid",
    "hazmatCommonInfo",
    "properShippingNamePrefix",
    "isPoison"
})
public class HazmatItemType
    extends OTMTransactionInOut
{

    @XmlElement(name = "HazmatItemGid", required = true)
    protected GLogXMLGidType hazmatItemGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;
    @XmlElement(name = "RegionGid", required = true)
    protected GLogXMLGidType regionGid;
    @XmlElement(name = "HazmatModeGid", required = true)
    protected GLogXMLGidType hazmatModeGid;
    @XmlElement(name = "HazmatGenericGid")
    protected GLogXMLGidType hazmatGenericGid;
    @XmlElement(name = "HazmatGeneric")
    protected HazmatGenericType hazmatGeneric;
    @XmlElement(name = "HazmatIndicator")
    protected String hazmatIndicator;
    @XmlElement(name = "HotIndicator")
    protected String hotIndicator;
    @XmlElement(name = "ResidueIndicator")
    protected String residueIndicator;
    @XmlElement(name = "DotExemptionGid")
    protected HazmatItemType.DotExemptionGid dotExemptionGid;
    @XmlElement(name = "AdditionalTransportMsg1Gid")
    protected HazmatItemType.AdditionalTransportMsg1Gid additionalTransportMsg1Gid;
    @XmlElement(name = "AdditionalTransportMsg2Gid")
    protected HazmatItemType.AdditionalTransportMsg2Gid additionalTransportMsg2Gid;
    @XmlElement(name = "Stcc49CodeGid", required = true)
    protected HazmatItemType.Stcc49CodeGid stcc49CodeGid;
    @XmlElement(name = "CompetentAuthorityCodeGid")
    protected HazmatItemType.CompetentAuthorityCodeGid competentAuthorityCodeGid;
    @XmlElement(name = "HazmatCommonInfo")
    protected HazmatCommonInfoType hazmatCommonInfo;
    @XmlElement(name = "ProperShippingNamePrefix")
    protected String properShippingNamePrefix;
    @XmlElement(name = "IsPoison")
    protected String isPoison;

    /**
     * Gets the value of the hazmatItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatItemGid() {
        return hazmatItemGid;
    }

    /**
     * Sets the value of the hazmatItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatItemGid(GLogXMLGidType value) {
        this.hazmatItemGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the packagedItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Sets the value of the packagedItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

    /**
     * Gets the value of the regionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRegionGid() {
        return regionGid;
    }

    /**
     * Sets the value of the regionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRegionGid(GLogXMLGidType value) {
        this.regionGid = value;
    }

    /**
     * Gets the value of the hazmatModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatModeGid() {
        return hazmatModeGid;
    }

    /**
     * Sets the value of the hazmatModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatModeGid(GLogXMLGidType value) {
        this.hazmatModeGid = value;
    }

    /**
     * Gets the value of the hazmatGenericGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatGenericGid() {
        return hazmatGenericGid;
    }

    /**
     * Sets the value of the hazmatGenericGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatGenericGid(GLogXMLGidType value) {
        this.hazmatGenericGid = value;
    }

    /**
     * Gets the value of the hazmatGeneric property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatGenericType }
     *     
     */
    public HazmatGenericType getHazmatGeneric() {
        return hazmatGeneric;
    }

    /**
     * Sets the value of the hazmatGeneric property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatGenericType }
     *     
     */
    public void setHazmatGeneric(HazmatGenericType value) {
        this.hazmatGeneric = value;
    }

    /**
     * Gets the value of the hazmatIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazmatIndicator() {
        return hazmatIndicator;
    }

    /**
     * Sets the value of the hazmatIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazmatIndicator(String value) {
        this.hazmatIndicator = value;
    }

    /**
     * Gets the value of the hotIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHotIndicator() {
        return hotIndicator;
    }

    /**
     * Sets the value of the hotIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHotIndicator(String value) {
        this.hotIndicator = value;
    }

    /**
     * Gets the value of the residueIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResidueIndicator() {
        return residueIndicator;
    }

    /**
     * Sets the value of the residueIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResidueIndicator(String value) {
        this.residueIndicator = value;
    }

    /**
     * Gets the value of the dotExemptionGid property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.DotExemptionGid }
     *     
     */
    public HazmatItemType.DotExemptionGid getDotExemptionGid() {
        return dotExemptionGid;
    }

    /**
     * Sets the value of the dotExemptionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.DotExemptionGid }
     *     
     */
    public void setDotExemptionGid(HazmatItemType.DotExemptionGid value) {
        this.dotExemptionGid = value;
    }

    /**
     * Gets the value of the additionalTransportMsg1Gid property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.AdditionalTransportMsg1Gid }
     *     
     */
    public HazmatItemType.AdditionalTransportMsg1Gid getAdditionalTransportMsg1Gid() {
        return additionalTransportMsg1Gid;
    }

    /**
     * Sets the value of the additionalTransportMsg1Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.AdditionalTransportMsg1Gid }
     *     
     */
    public void setAdditionalTransportMsg1Gid(HazmatItemType.AdditionalTransportMsg1Gid value) {
        this.additionalTransportMsg1Gid = value;
    }

    /**
     * Gets the value of the additionalTransportMsg2Gid property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.AdditionalTransportMsg2Gid }
     *     
     */
    public HazmatItemType.AdditionalTransportMsg2Gid getAdditionalTransportMsg2Gid() {
        return additionalTransportMsg2Gid;
    }

    /**
     * Sets the value of the additionalTransportMsg2Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.AdditionalTransportMsg2Gid }
     *     
     */
    public void setAdditionalTransportMsg2Gid(HazmatItemType.AdditionalTransportMsg2Gid value) {
        this.additionalTransportMsg2Gid = value;
    }

    /**
     * Gets the value of the stcc49CodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.Stcc49CodeGid }
     *     
     */
    public HazmatItemType.Stcc49CodeGid getStcc49CodeGid() {
        return stcc49CodeGid;
    }

    /**
     * Sets the value of the stcc49CodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.Stcc49CodeGid }
     *     
     */
    public void setStcc49CodeGid(HazmatItemType.Stcc49CodeGid value) {
        this.stcc49CodeGid = value;
    }

    /**
     * Gets the value of the competentAuthorityCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemType.CompetentAuthorityCodeGid }
     *     
     */
    public HazmatItemType.CompetentAuthorityCodeGid getCompetentAuthorityCodeGid() {
        return competentAuthorityCodeGid;
    }

    /**
     * Sets the value of the competentAuthorityCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemType.CompetentAuthorityCodeGid }
     *     
     */
    public void setCompetentAuthorityCodeGid(HazmatItemType.CompetentAuthorityCodeGid value) {
        this.competentAuthorityCodeGid = value;
    }

    /**
     * Gets the value of the hazmatCommonInfo property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public HazmatCommonInfoType getHazmatCommonInfo() {
        return hazmatCommonInfo;
    }

    /**
     * Sets the value of the hazmatCommonInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public void setHazmatCommonInfo(HazmatCommonInfoType value) {
        this.hazmatCommonInfo = value;
    }

    /**
     * Gets the value of the properShippingNamePrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProperShippingNamePrefix() {
        return properShippingNamePrefix;
    }

    /**
     * Sets the value of the properShippingNamePrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProperShippingNamePrefix(String value) {
        this.properShippingNamePrefix = value;
    }

    /**
     * Gets the value of the isPoison property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPoison() {
        return isPoison;
    }

    /**
     * Sets the value of the isPoison property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPoison(String value) {
        this.isPoison = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatTransportMsgGid"
    })
    public static class AdditionalTransportMsg1Gid {

        @XmlElement(name = "HazmatTransportMsgGid", required = true)
        protected GLogXMLGidType hazmatTransportMsgGid;

        /**
         * Gets the value of the hazmatTransportMsgGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatTransportMsgGid() {
            return hazmatTransportMsgGid;
        }

        /**
         * Sets the value of the hazmatTransportMsgGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatTransportMsgGid(GLogXMLGidType value) {
            this.hazmatTransportMsgGid = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="HazmatTransportMsgGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatTransportMsgGid"
    })
    public static class AdditionalTransportMsg2Gid {

        @XmlElement(name = "HazmatTransportMsgGid", required = true)
        protected GLogXMLGidType hazmatTransportMsgGid;

        /**
         * Gets the value of the hazmatTransportMsgGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatTransportMsgGid() {
            return hazmatTransportMsgGid;
        }

        /**
         * Sets the value of the hazmatTransportMsgGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatTransportMsgGid(GLogXMLGidType value) {
            this.hazmatTransportMsgGid = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatAprovalExemptionGid"
    })
    public static class CompetentAuthorityCodeGid {

        @XmlElement(name = "HazmatAprovalExemptionGid", required = true)
        protected GLogXMLGidType hazmatAprovalExemptionGid;

        /**
         * Gets the value of the hazmatAprovalExemptionGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatAprovalExemptionGid() {
            return hazmatAprovalExemptionGid;
        }

        /**
         * Sets the value of the hazmatAprovalExemptionGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatAprovalExemptionGid(GLogXMLGidType value) {
            this.hazmatAprovalExemptionGid = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="HazmatAprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "hazmatAprovalExemptionGid"
    })
    public static class DotExemptionGid {

        @XmlElement(name = "HazmatAprovalExemptionGid", required = true)
        protected GLogXMLGidType hazmatAprovalExemptionGid;

        /**
         * Gets the value of the hazmatAprovalExemptionGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getHazmatAprovalExemptionGid() {
            return hazmatAprovalExemptionGid;
        }

        /**
         * Sets the value of the hazmatAprovalExemptionGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setHazmatAprovalExemptionGid(GLogXMLGidType value) {
            this.hazmatAprovalExemptionGid = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="STCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stccGid"
    })
    public static class Stcc49CodeGid {

        @XmlElement(name = "STCCGid", required = true)
        protected GLogXMLGidType stccGid;

        /**
         * Gets the value of the stccGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getSTCCGid() {
            return stccGid;
        }

        /**
         * Sets the value of the stccGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setSTCCGid(GLogXMLGidType value) {
            this.stccGid = value;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * This element is used to identify item packaging information.
 * 
 *             Note: When the PackagedItemSpecRef element is specified in the Packaging element, it should not be specified
 *             again in the PackagingShipUnit element.
 * 
 *          
 * 
 * <p>Java class for PackagingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PackagingType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatPackageTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="InnerPackCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackageShipUnitWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="PackageShipUnitLWH" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/&gt;
 *         &lt;element name="PackageShipUnitDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DiameterType" minOccurs="0"/&gt;
 *         &lt;element name="CoreDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/&gt;
 *         &lt;element name="InnerPackInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InnerPackInfoType" minOccurs="0"/&gt;
 *         &lt;element name="IsDefaultPackaging" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackageItemTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LoadConfigRuleRank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PackagingShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagingShipUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PkgItemEquipRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PkgItemEquipRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="PackagingInnerPack" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagingInnerPackType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PkgItemPackageRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PkgItemPackageRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsAllowMixedFreight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BulkMixingFamilyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CompartmentTypeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CategoryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsNestable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IncrementalNestingHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HeightType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackagingType", propOrder = {
    "packagedItemGid",
    "hazmatPackageTypeGid",
    "description",
    "packagedItemSpecRef",
    "innerPackCount",
    "packageShipUnitWeightVolume",
    "packageShipUnitLWH",
    "packageShipUnitDiameter",
    "coreDiameter",
    "innerPackInfo",
    "isDefaultPackaging",
    "isHazardous",
    "packageItemTypeGid",
    "loadConfigRuleRank",
    "shipUnitSpecProfileGid",
    "refnum",
    "packagingShipUnit",
    "pkgItemEquipRefUnit",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "packagingInnerPack",
    "pkgItemPackageRefUnit",
    "isAllowMixedFreight",
    "bulkMixingFamilyGid",
    "compartmentTypeProfileGid",
    "priority",
    "categoryGid",
    "isNestable",
    "incrementalNestingHeight"
})
public class PackagingType {

    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;
    @XmlElement(name = "HazmatPackageTypeGid")
    protected GLogXMLGidType hazmatPackageTypeGid;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "InnerPackCount")
    protected String innerPackCount;
    @XmlElement(name = "PackageShipUnitWeightVolume")
    protected WeightVolumeType packageShipUnitWeightVolume;
    @XmlElement(name = "PackageShipUnitLWH")
    protected LengthWidthHeightType packageShipUnitLWH;
    @XmlElement(name = "PackageShipUnitDiameter")
    protected DiameterType packageShipUnitDiameter;
    @XmlElement(name = "CoreDiameter")
    protected GLogXMLDiameterType coreDiameter;
    @XmlElement(name = "InnerPackInfo")
    protected InnerPackInfoType innerPackInfo;
    @XmlElement(name = "IsDefaultPackaging")
    protected String isDefaultPackaging;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "PackageItemTypeGid")
    protected GLogXMLGidType packageItemTypeGid;
    @XmlElement(name = "LoadConfigRuleRank")
    protected String loadConfigRuleRank;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "PackagingShipUnit")
    protected List<PackagingShipUnitType> packagingShipUnit;
    @XmlElement(name = "PkgItemEquipRefUnit")
    protected List<PkgItemEquipRefUnitType> pkgItemEquipRefUnit;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "PackagingInnerPack")
    protected List<PackagingInnerPackType> packagingInnerPack;
    @XmlElement(name = "PkgItemPackageRefUnit")
    protected List<PkgItemPackageRefUnitType> pkgItemPackageRefUnit;
    @XmlElement(name = "IsAllowMixedFreight")
    protected String isAllowMixedFreight;
    @XmlElement(name = "BulkMixingFamilyGid")
    protected GLogXMLGidType bulkMixingFamilyGid;
    @XmlElement(name = "CompartmentTypeProfileGid")
    protected GLogXMLGidType compartmentTypeProfileGid;
    @XmlElement(name = "Priority")
    protected String priority;
    @XmlElement(name = "CategoryGid")
    protected GLogXMLGidType categoryGid;
    @XmlElement(name = "IsNestable")
    protected String isNestable;
    @XmlElement(name = "IncrementalNestingHeight")
    protected HeightType incrementalNestingHeight;

    /**
     * Gets the value of the packagedItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Sets the value of the packagedItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

    /**
     * Gets the value of the hazmatPackageTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatPackageTypeGid() {
        return hazmatPackageTypeGid;
    }

    /**
     * Sets the value of the hazmatPackageTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatPackageTypeGid(GLogXMLGidType value) {
        this.hazmatPackageTypeGid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Gets the value of the innerPackCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInnerPackCount() {
        return innerPackCount;
    }

    /**
     * Sets the value of the innerPackCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInnerPackCount(String value) {
        this.innerPackCount = value;
    }

    /**
     * Gets the value of the packageShipUnitWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getPackageShipUnitWeightVolume() {
        return packageShipUnitWeightVolume;
    }

    /**
     * Sets the value of the packageShipUnitWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setPackageShipUnitWeightVolume(WeightVolumeType value) {
        this.packageShipUnitWeightVolume = value;
    }

    /**
     * Gets the value of the packageShipUnitLWH property.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getPackageShipUnitLWH() {
        return packageShipUnitLWH;
    }

    /**
     * Sets the value of the packageShipUnitLWH property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setPackageShipUnitLWH(LengthWidthHeightType value) {
        this.packageShipUnitLWH = value;
    }

    /**
     * Gets the value of the packageShipUnitDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link DiameterType }
     *     
     */
    public DiameterType getPackageShipUnitDiameter() {
        return packageShipUnitDiameter;
    }

    /**
     * Sets the value of the packageShipUnitDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiameterType }
     *     
     */
    public void setPackageShipUnitDiameter(DiameterType value) {
        this.packageShipUnitDiameter = value;
    }

    /**
     * Gets the value of the coreDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getCoreDiameter() {
        return coreDiameter;
    }

    /**
     * Sets the value of the coreDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setCoreDiameter(GLogXMLDiameterType value) {
        this.coreDiameter = value;
    }

    /**
     * Gets the value of the innerPackInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InnerPackInfoType }
     *     
     */
    public InnerPackInfoType getInnerPackInfo() {
        return innerPackInfo;
    }

    /**
     * Sets the value of the innerPackInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InnerPackInfoType }
     *     
     */
    public void setInnerPackInfo(InnerPackInfoType value) {
        this.innerPackInfo = value;
    }

    /**
     * Gets the value of the isDefaultPackaging property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDefaultPackaging() {
        return isDefaultPackaging;
    }

    /**
     * Sets the value of the isDefaultPackaging property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDefaultPackaging(String value) {
        this.isDefaultPackaging = value;
    }

    /**
     * Gets the value of the isHazardous property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Sets the value of the isHazardous property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Gets the value of the packageItemTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackageItemTypeGid() {
        return packageItemTypeGid;
    }

    /**
     * Sets the value of the packageItemTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackageItemTypeGid(GLogXMLGidType value) {
        this.packageItemTypeGid = value;
    }

    /**
     * Gets the value of the loadConfigRuleRank property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoadConfigRuleRank() {
        return loadConfigRuleRank;
    }

    /**
     * Sets the value of the loadConfigRuleRank property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoadConfigRuleRank(String value) {
        this.loadConfigRuleRank = value;
    }

    /**
     * Gets the value of the shipUnitSpecProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Sets the value of the shipUnitSpecProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the packagingShipUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the packagingShipUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagingShipUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagingShipUnitType }
     * 
     * 
     */
    public List<PackagingShipUnitType> getPackagingShipUnit() {
        if (packagingShipUnit == null) {
            packagingShipUnit = new ArrayList<PackagingShipUnitType>();
        }
        return this.packagingShipUnit;
    }

    /**
     * Gets the value of the pkgItemEquipRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the pkgItemEquipRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPkgItemEquipRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PkgItemEquipRefUnitType }
     * 
     * 
     */
    public List<PkgItemEquipRefUnitType> getPkgItemEquipRefUnit() {
        if (pkgItemEquipRefUnit == null) {
            pkgItemEquipRefUnit = new ArrayList<PkgItemEquipRefUnitType>();
        }
        return this.pkgItemEquipRefUnit;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the packagingInnerPack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the packagingInnerPack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagingInnerPack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagingInnerPackType }
     * 
     * 
     */
    public List<PackagingInnerPackType> getPackagingInnerPack() {
        if (packagingInnerPack == null) {
            packagingInnerPack = new ArrayList<PackagingInnerPackType>();
        }
        return this.packagingInnerPack;
    }

    /**
     * Gets the value of the pkgItemPackageRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the pkgItemPackageRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPkgItemPackageRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PkgItemPackageRefUnitType }
     * 
     * 
     */
    public List<PkgItemPackageRefUnitType> getPkgItemPackageRefUnit() {
        if (pkgItemPackageRefUnit == null) {
            pkgItemPackageRefUnit = new ArrayList<PkgItemPackageRefUnitType>();
        }
        return this.pkgItemPackageRefUnit;
    }

    /**
     * Gets the value of the isAllowMixedFreight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowMixedFreight() {
        return isAllowMixedFreight;
    }

    /**
     * Sets the value of the isAllowMixedFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowMixedFreight(String value) {
        this.isAllowMixedFreight = value;
    }

    /**
     * Gets the value of the bulkMixingFamilyGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBulkMixingFamilyGid() {
        return bulkMixingFamilyGid;
    }

    /**
     * Sets the value of the bulkMixingFamilyGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBulkMixingFamilyGid(GLogXMLGidType value) {
        this.bulkMixingFamilyGid = value;
    }

    /**
     * Gets the value of the compartmentTypeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCompartmentTypeProfileGid() {
        return compartmentTypeProfileGid;
    }

    /**
     * Sets the value of the compartmentTypeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCompartmentTypeProfileGid(GLogXMLGidType value) {
        this.compartmentTypeProfileGid = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

    /**
     * Gets the value of the categoryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCategoryGid() {
        return categoryGid;
    }

    /**
     * Sets the value of the categoryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCategoryGid(GLogXMLGidType value) {
        this.categoryGid = value;
    }

    /**
     * Gets the value of the isNestable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNestable() {
        return isNestable;
    }

    /**
     * Sets the value of the isNestable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNestable(String value) {
        this.isNestable = value;
    }

    /**
     * Gets the value of the incrementalNestingHeight property.
     * 
     * @return
     *     possible object is
     *     {@link HeightType }
     *     
     */
    public HeightType getIncrementalNestingHeight() {
        return incrementalNestingHeight;
    }

    /**
     * Sets the value of the incrementalNestingHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeightType }
     *     
     */
    public void setIncrementalNestingHeight(HeightType value) {
        this.incrementalNestingHeight = value;
    }

}

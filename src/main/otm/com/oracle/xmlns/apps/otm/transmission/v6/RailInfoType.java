
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies rail road information related to the status message or CLM. The city, state, SPLC code, and date correspond to information related to the destination.
 *          
 * 
 * <p>Java class for RailInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RailInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CarrierScac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCarLoaded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EventCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EventState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SPLCCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EventCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ERPC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ETADt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HoursToRepair" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InterchangeETADt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="NextJunctionSPLC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailInfoType", propOrder = {
    "carrierScac",
    "isCarLoaded",
    "eventCity",
    "eventState",
    "splcCode",
    "eventCountry",
    "locationID",
    "erpc",
    "etaDt",
    "indicator",
    "hoursToRepair",
    "interchangeETADt",
    "nextJunctionSPLC"
})
public class RailInfoType {

    @XmlElement(name = "CarrierScac")
    protected String carrierScac;
    @XmlElement(name = "IsCarLoaded")
    protected String isCarLoaded;
    @XmlElement(name = "EventCity")
    protected String eventCity;
    @XmlElement(name = "EventState")
    protected String eventState;
    @XmlElement(name = "SPLCCode")
    protected String splcCode;
    @XmlElement(name = "EventCountry")
    protected String eventCountry;
    @XmlElement(name = "LocationID")
    protected String locationID;
    @XmlElement(name = "ERPC")
    protected String erpc;
    @XmlElement(name = "ETADt")
    protected GLogDateTimeType etaDt;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "HoursToRepair")
    protected String hoursToRepair;
    @XmlElement(name = "InterchangeETADt")
    protected GLogDateTimeType interchangeETADt;
    @XmlElement(name = "NextJunctionSPLC")
    protected String nextJunctionSPLC;

    /**
     * Gets the value of the carrierScac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierScac() {
        return carrierScac;
    }

    /**
     * Sets the value of the carrierScac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierScac(String value) {
        this.carrierScac = value;
    }

    /**
     * Gets the value of the isCarLoaded property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCarLoaded() {
        return isCarLoaded;
    }

    /**
     * Sets the value of the isCarLoaded property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCarLoaded(String value) {
        this.isCarLoaded = value;
    }

    /**
     * Gets the value of the eventCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventCity() {
        return eventCity;
    }

    /**
     * Sets the value of the eventCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventCity(String value) {
        this.eventCity = value;
    }

    /**
     * Gets the value of the eventState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventState() {
        return eventState;
    }

    /**
     * Sets the value of the eventState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventState(String value) {
        this.eventState = value;
    }

    /**
     * Gets the value of the splcCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSPLCCode() {
        return splcCode;
    }

    /**
     * Sets the value of the splcCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSPLCCode(String value) {
        this.splcCode = value;
    }

    /**
     * Gets the value of the eventCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventCountry() {
        return eventCountry;
    }

    /**
     * Sets the value of the eventCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventCountry(String value) {
        this.eventCountry = value;
    }

    /**
     * Gets the value of the locationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * Sets the value of the locationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocationID(String value) {
        this.locationID = value;
    }

    /**
     * Gets the value of the erpc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERPC() {
        return erpc;
    }

    /**
     * Sets the value of the erpc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERPC(String value) {
        this.erpc = value;
    }

    /**
     * Gets the value of the etaDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getETADt() {
        return etaDt;
    }

    /**
     * Sets the value of the etaDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setETADt(GLogDateTimeType value) {
        this.etaDt = value;
    }

    /**
     * Gets the value of the indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Sets the value of the indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Gets the value of the hoursToRepair property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHoursToRepair() {
        return hoursToRepair;
    }

    /**
     * Sets the value of the hoursToRepair property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHoursToRepair(String value) {
        this.hoursToRepair = value;
    }

    /**
     * Gets the value of the interchangeETADt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInterchangeETADt() {
        return interchangeETADt;
    }

    /**
     * Sets the value of the interchangeETADt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInterchangeETADt(GLogDateTimeType value) {
        this.interchangeETADt = value;
    }

    /**
     * Gets the value of the nextJunctionSPLC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextJunctionSPLC() {
        return nextJunctionSPLC;
    }

    /**
     * Sets the value of the nextJunctionSPLC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextJunctionSPLC(String value) {
        this.nextJunctionSPLC = value;
    }

}

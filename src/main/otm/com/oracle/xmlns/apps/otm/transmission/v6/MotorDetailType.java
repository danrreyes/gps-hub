
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             (inbound) MotorDetail provides invoice data specific to motor carriers.
 *          
 * 
 * <p>Java class for MotorDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MotorDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PickupDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="DeliveryDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="MotorStopOff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopOffType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="MotorEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MotorEquipmentType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="MotorLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MotorLineItemType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MotorDetailType", propOrder = {
    "pickupDate",
    "deliveryDate",
    "motorStopOff",
    "motorEquipment",
    "motorLineItem"
})
public class MotorDetailType {

    @XmlElement(name = "PickupDate", required = true)
    protected GLogDateTimeType pickupDate;
    @XmlElement(name = "DeliveryDate", required = true)
    protected GLogDateTimeType deliveryDate;
    @XmlElement(name = "MotorStopOff")
    protected List<StopOffType> motorStopOff;
    @XmlElement(name = "MotorEquipment", required = true)
    protected List<MotorEquipmentType> motorEquipment;
    @XmlElement(name = "MotorLineItem", required = true)
    protected List<MotorLineItemType> motorLineItem;

    /**
     * Gets the value of the pickupDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPickupDate() {
        return pickupDate;
    }

    /**
     * Sets the value of the pickupDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPickupDate(GLogDateTimeType value) {
        this.pickupDate = value;
    }

    /**
     * Gets the value of the deliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Sets the value of the deliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryDate(GLogDateTimeType value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the motorStopOff property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the motorStopOff property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotorStopOff().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopOffType }
     * 
     * 
     */
    public List<StopOffType> getMotorStopOff() {
        if (motorStopOff == null) {
            motorStopOff = new ArrayList<StopOffType>();
        }
        return this.motorStopOff;
    }

    /**
     * Gets the value of the motorEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the motorEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotorEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MotorEquipmentType }
     * 
     * 
     */
    public List<MotorEquipmentType> getMotorEquipment() {
        if (motorEquipment == null) {
            motorEquipment = new ArrayList<MotorEquipmentType>();
        }
        return this.motorEquipment;
    }

    /**
     * Gets the value of the motorLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the motorLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMotorLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MotorLineItemType }
     * 
     * 
     */
    public List<MotorLineItemType> getMotorLineItem() {
        if (motorLineItem == null) {
            motorLineItem = new ArrayList<MotorLineItemType>();
        }
        return this.motorLineItem;
    }

}

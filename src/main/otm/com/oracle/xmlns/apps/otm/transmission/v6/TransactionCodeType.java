
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionCodeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <pre>
 * &lt;simpleType name="TransactionCodeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="I"/&gt;
 *     &lt;enumeration value="U"/&gt;
 *     &lt;enumeration value="IU"/&gt;
 *     &lt;enumeration value="UI"/&gt;
 *     &lt;enumeration value="D"/&gt;
 *     &lt;enumeration value="RC"/&gt;
 *     &lt;enumeration value="RP"/&gt;
 *     &lt;enumeration value="R"/&gt;
 *     &lt;enumeration value="II"/&gt;
 *     &lt;enumeration value="DR"/&gt;
 *     &lt;enumeration value="NP"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "TransactionCodeType")
@XmlEnum
public enum TransactionCodeType {

    I,
    U,
    IU,
    UI,
    D,
    RC,
    RP,
    R,
    II,
    DR,
    NP;

    public String value() {
        return name();
    }

    public static TransactionCodeType fromValue(String v) {
        return valueOf(v);
    }

}

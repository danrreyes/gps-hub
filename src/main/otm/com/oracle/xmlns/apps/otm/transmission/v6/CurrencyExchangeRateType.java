
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the currency conversion between two currencies.
 *          
 * 
 * <p>Java class for CurrencyExchangeRateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CurrencyExchangeRateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CurrencyFrom" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType"/&gt;
 *         &lt;element name="CurrencyTo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType"/&gt;
 *         &lt;element name="TriangulationCurrency" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType" minOccurs="0"/&gt;
 *         &lt;element name="MaximumPrecision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FractionalDigits" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrencyExchangeRateType", propOrder = {
    "currencyFrom",
    "currencyTo",
    "triangulationCurrency",
    "maximumPrecision",
    "fractionalDigits",
    "exchangeRateValue"
})
public class CurrencyExchangeRateType {

    @XmlElement(name = "CurrencyFrom", required = true)
    protected CurrencyCodeType currencyFrom;
    @XmlElement(name = "CurrencyTo", required = true)
    protected CurrencyCodeType currencyTo;
    @XmlElement(name = "TriangulationCurrency")
    protected CurrencyCodeType triangulationCurrency;
    @XmlElement(name = "MaximumPrecision")
    protected String maximumPrecision;
    @XmlElement(name = "FractionalDigits")
    protected String fractionalDigits;
    @XmlElement(name = "ExchangeRateValue", required = true)
    protected String exchangeRateValue;

    /**
     * Gets the value of the currencyFrom property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getCurrencyFrom() {
        return currencyFrom;
    }

    /**
     * Sets the value of the currencyFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setCurrencyFrom(CurrencyCodeType value) {
        this.currencyFrom = value;
    }

    /**
     * Gets the value of the currencyTo property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getCurrencyTo() {
        return currencyTo;
    }

    /**
     * Sets the value of the currencyTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setCurrencyTo(CurrencyCodeType value) {
        this.currencyTo = value;
    }

    /**
     * Gets the value of the triangulationCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getTriangulationCurrency() {
        return triangulationCurrency;
    }

    /**
     * Sets the value of the triangulationCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setTriangulationCurrency(CurrencyCodeType value) {
        this.triangulationCurrency = value;
    }

    /**
     * Gets the value of the maximumPrecision property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumPrecision() {
        return maximumPrecision;
    }

    /**
     * Sets the value of the maximumPrecision property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumPrecision(String value) {
        this.maximumPrecision = value;
    }

    /**
     * Gets the value of the fractionalDigits property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFractionalDigits() {
        return fractionalDigits;
    }

    /**
     * Sets the value of the fractionalDigits property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFractionalDigits(String value) {
        this.fractionalDigits = value;
    }

    /**
     * Gets the value of the exchangeRateValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeRateValue() {
        return exchangeRateValue;
    }

    /**
     * Sets the value of the exchangeRateValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeRateValue(String value) {
        this.exchangeRateValue = value;
    }

}

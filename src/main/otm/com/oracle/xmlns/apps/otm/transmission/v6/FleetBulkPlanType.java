
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             This is used to provide statistics about the fleet resource assignments that were created to the planned shipments during a
 *             given run of fleet bulk plan.
 *          
 * 
 * <p>Java class for FleetBulkPlanType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FleetBulkPlanType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="FleetBulkPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipmentQueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ResourceQueryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EndTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentsSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfResourcesSelected" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ResourceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentsAssignable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfResourcesAssignable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfResourcesWithMultiAssign" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfShipmentsAssigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NumOfResourcesAssigned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PreShipCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AfterShipCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="BobTailDist" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="LoadedDist" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="DeadHeadDist" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="PlanningParamSetGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FleetBulkPlanCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FleetBulkPlanCostType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FleetBulkPlanType", propOrder = {
    "sendReason",
    "fleetBulkPlanGid",
    "shipmentQueryName",
    "resourceQueryName",
    "startTime",
    "endTime",
    "numOfShipmentsSelected",
    "numOfResourcesSelected",
    "resourceType",
    "numOfShipmentsAssignable",
    "numOfResourcesAssignable",
    "numOfResourcesWithMultiAssign",
    "numOfShipmentsAssigned",
    "numOfResourcesAssigned",
    "preShipCost",
    "afterShipCost",
    "bobTailDist",
    "loadedDist",
    "deadHeadDist",
    "planningParamSetGid",
    "fleetBulkPlanCost"
})
public class FleetBulkPlanType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "FleetBulkPlanGid", required = true)
    protected GLogXMLGidType fleetBulkPlanGid;
    @XmlElement(name = "ShipmentQueryName")
    protected String shipmentQueryName;
    @XmlElement(name = "ResourceQueryName")
    protected String resourceQueryName;
    @XmlElement(name = "StartTime")
    protected GLogDateTimeType startTime;
    @XmlElement(name = "EndTime")
    protected GLogDateTimeType endTime;
    @XmlElement(name = "NumOfShipmentsSelected")
    protected String numOfShipmentsSelected;
    @XmlElement(name = "NumOfResourcesSelected")
    protected String numOfResourcesSelected;
    @XmlElement(name = "ResourceType")
    protected String resourceType;
    @XmlElement(name = "NumOfShipmentsAssignable")
    protected String numOfShipmentsAssignable;
    @XmlElement(name = "NumOfResourcesAssignable")
    protected String numOfResourcesAssignable;
    @XmlElement(name = "NumOfResourcesWithMultiAssign")
    protected String numOfResourcesWithMultiAssign;
    @XmlElement(name = "NumOfShipmentsAssigned")
    protected String numOfShipmentsAssigned;
    @XmlElement(name = "NumOfResourcesAssigned")
    protected String numOfResourcesAssigned;
    @XmlElement(name = "PreShipCost")
    protected GLogXMLFinancialAmountType preShipCost;
    @XmlElement(name = "AfterShipCost")
    protected GLogXMLFinancialAmountType afterShipCost;
    @XmlElement(name = "BobTailDist")
    protected GLogXMLDistanceType bobTailDist;
    @XmlElement(name = "LoadedDist")
    protected GLogXMLDistanceType loadedDist;
    @XmlElement(name = "DeadHeadDist")
    protected GLogXMLDistanceType deadHeadDist;
    @XmlElement(name = "PlanningParamSetGid")
    protected GLogXMLGidType planningParamSetGid;
    @XmlElement(name = "FleetBulkPlanCost")
    protected List<FleetBulkPlanCostType> fleetBulkPlanCost;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the fleetBulkPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFleetBulkPlanGid() {
        return fleetBulkPlanGid;
    }

    /**
     * Sets the value of the fleetBulkPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFleetBulkPlanGid(GLogXMLGidType value) {
        this.fleetBulkPlanGid = value;
    }

    /**
     * Gets the value of the shipmentQueryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentQueryName() {
        return shipmentQueryName;
    }

    /**
     * Sets the value of the shipmentQueryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentQueryName(String value) {
        this.shipmentQueryName = value;
    }

    /**
     * Gets the value of the resourceQueryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceQueryName() {
        return resourceQueryName;
    }

    /**
     * Sets the value of the resourceQueryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceQueryName(String value) {
        this.resourceQueryName = value;
    }

    /**
     * Gets the value of the startTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setStartTime(GLogDateTimeType value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the endTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndTime() {
        return endTime;
    }

    /**
     * Sets the value of the endTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndTime(GLogDateTimeType value) {
        this.endTime = value;
    }

    /**
     * Gets the value of the numOfShipmentsSelected property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsSelected() {
        return numOfShipmentsSelected;
    }

    /**
     * Sets the value of the numOfShipmentsSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsSelected(String value) {
        this.numOfShipmentsSelected = value;
    }

    /**
     * Gets the value of the numOfResourcesSelected property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesSelected() {
        return numOfResourcesSelected;
    }

    /**
     * Sets the value of the numOfResourcesSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesSelected(String value) {
        this.numOfResourcesSelected = value;
    }

    /**
     * Gets the value of the resourceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Sets the value of the resourceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResourceType(String value) {
        this.resourceType = value;
    }

    /**
     * Gets the value of the numOfShipmentsAssignable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsAssignable() {
        return numOfShipmentsAssignable;
    }

    /**
     * Sets the value of the numOfShipmentsAssignable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsAssignable(String value) {
        this.numOfShipmentsAssignable = value;
    }

    /**
     * Gets the value of the numOfResourcesAssignable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesAssignable() {
        return numOfResourcesAssignable;
    }

    /**
     * Sets the value of the numOfResourcesAssignable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesAssignable(String value) {
        this.numOfResourcesAssignable = value;
    }

    /**
     * Gets the value of the numOfResourcesWithMultiAssign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesWithMultiAssign() {
        return numOfResourcesWithMultiAssign;
    }

    /**
     * Sets the value of the numOfResourcesWithMultiAssign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesWithMultiAssign(String value) {
        this.numOfResourcesWithMultiAssign = value;
    }

    /**
     * Gets the value of the numOfShipmentsAssigned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfShipmentsAssigned() {
        return numOfShipmentsAssigned;
    }

    /**
     * Sets the value of the numOfShipmentsAssigned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfShipmentsAssigned(String value) {
        this.numOfShipmentsAssigned = value;
    }

    /**
     * Gets the value of the numOfResourcesAssigned property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfResourcesAssigned() {
        return numOfResourcesAssigned;
    }

    /**
     * Sets the value of the numOfResourcesAssigned property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfResourcesAssigned(String value) {
        this.numOfResourcesAssigned = value;
    }

    /**
     * Gets the value of the preShipCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getPreShipCost() {
        return preShipCost;
    }

    /**
     * Sets the value of the preShipCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setPreShipCost(GLogXMLFinancialAmountType value) {
        this.preShipCost = value;
    }

    /**
     * Gets the value of the afterShipCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAfterShipCost() {
        return afterShipCost;
    }

    /**
     * Sets the value of the afterShipCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAfterShipCost(GLogXMLFinancialAmountType value) {
        this.afterShipCost = value;
    }

    /**
     * Gets the value of the bobTailDist property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getBobTailDist() {
        return bobTailDist;
    }

    /**
     * Sets the value of the bobTailDist property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setBobTailDist(GLogXMLDistanceType value) {
        this.bobTailDist = value;
    }

    /**
     * Gets the value of the loadedDist property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getLoadedDist() {
        return loadedDist;
    }

    /**
     * Sets the value of the loadedDist property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setLoadedDist(GLogXMLDistanceType value) {
        this.loadedDist = value;
    }

    /**
     * Gets the value of the deadHeadDist property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDeadHeadDist() {
        return deadHeadDist;
    }

    /**
     * Sets the value of the deadHeadDist property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDeadHeadDist(GLogXMLDistanceType value) {
        this.deadHeadDist = value;
    }

    /**
     * Gets the value of the planningParamSetGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningParamSetGid() {
        return planningParamSetGid;
    }

    /**
     * Sets the value of the planningParamSetGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningParamSetGid(GLogXMLGidType value) {
        this.planningParamSetGid = value;
    }

    /**
     * Gets the value of the fleetBulkPlanCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the fleetBulkPlanCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFleetBulkPlanCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FleetBulkPlanCostType }
     * 
     * 
     */
    public List<FleetBulkPlanCostType> getFleetBulkPlanCost() {
        if (fleetBulkPlanCost == null) {
            fleetBulkPlanCost = new ArrayList<FleetBulkPlanCostType>();
        }
        return this.fleetBulkPlanCost;
    }

}

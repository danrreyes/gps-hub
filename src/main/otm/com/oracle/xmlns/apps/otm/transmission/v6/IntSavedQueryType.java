
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Integration Saved Queries provide a means to query for Gid(s) using a combination of SQL and XPath
 *             expressions referencing information in the XML. A basic example of an Integration Saved Query that
 *             uses the ShipUnit Content Tracking Tag number in the XML to lookup the Gid in the database would resemble the following:
 * 
 *             select distinct s_ship_unit_gid from s_ship_unit_line where tracking_tag1 = '{ShipUnit/ShipUnitContent[1]/ItemQuantity/ItemTag1}'
 * 
 *             The Xpath expression in the braces, which referes to the ItemQuantity/ItemTag element in the first
 *             ShipUnitContent element of the ShipUnit, will be replaced with the value in the XML document before
 *             the query is initiated.
 *          
 * 
 * <p>Java class for IntSavedQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IntSavedQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQueryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="IntSavedQueryArg" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="IsMultiMatch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaximumMatch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NoDataFoundAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IntSavedQueryType", propOrder = {
    "intSavedQueryGid",
    "intSavedQueryArg",
    "isMultiMatch",
    "maximumMatch",
    "noDataFoundAction"
})
public class IntSavedQueryType {

    @XmlElement(name = "IntSavedQueryGid", required = true)
    protected GLogXMLGidType intSavedQueryGid;
    @XmlElement(name = "IntSavedQueryArg")
    protected List<IntSavedQueryType.IntSavedQueryArg> intSavedQueryArg;
    @XmlElement(name = "IsMultiMatch")
    protected String isMultiMatch;
    @XmlElement(name = "MaximumMatch")
    protected String maximumMatch;
    @XmlElement(name = "NoDataFoundAction")
    protected String noDataFoundAction;

    /**
     * Gets the value of the intSavedQueryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIntSavedQueryGid() {
        return intSavedQueryGid;
    }

    /**
     * Sets the value of the intSavedQueryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIntSavedQueryGid(GLogXMLGidType value) {
        this.intSavedQueryGid = value;
    }

    /**
     * Gets the value of the intSavedQueryArg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the intSavedQueryArg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntSavedQueryArg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntSavedQueryType.IntSavedQueryArg }
     * 
     * 
     */
    public List<IntSavedQueryType.IntSavedQueryArg> getIntSavedQueryArg() {
        if (intSavedQueryArg == null) {
            intSavedQueryArg = new ArrayList<IntSavedQueryType.IntSavedQueryArg>();
        }
        return this.intSavedQueryArg;
    }

    /**
     * Gets the value of the isMultiMatch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMultiMatch() {
        return isMultiMatch;
    }

    /**
     * Sets the value of the isMultiMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMultiMatch(String value) {
        this.isMultiMatch = value;
    }

    /**
     * Gets the value of the maximumMatch property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaximumMatch() {
        return maximumMatch;
    }

    /**
     * Sets the value of the maximumMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaximumMatch(String value) {
        this.maximumMatch = value;
    }

    /**
     * Gets the value of the noDataFoundAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNoDataFoundAction() {
        return noDataFoundAction;
    }

    /**
     * Sets the value of the noDataFoundAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNoDataFoundAction(String value) {
        this.noDataFoundAction = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ArgName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ArgValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "argName",
        "argValue"
    })
    public static class IntSavedQueryArg {

        @XmlElement(name = "ArgName", required = true)
        protected String argName;
        @XmlElement(name = "ArgValue", required = true)
        protected String argValue;

        /**
         * Gets the value of the argName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgName() {
            return argName;
        }

        /**
         * Sets the value of the argName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgName(String value) {
            this.argName = value;
        }

        /**
         * Gets the value of the argValue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getArgValue() {
            return argValue;
        }

        /**
         * Sets the value of the argValue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setArgValue(String value) {
            this.argValue = value;
        }

    }

}

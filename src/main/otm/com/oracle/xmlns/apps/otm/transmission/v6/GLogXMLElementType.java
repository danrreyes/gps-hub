
package com.oracle.xmlns.apps.otm.transmission.v6;

import com.oracle.xmlns.apps.gtm.transmission.v6.GtmBondType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmContactType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmDeclarationMessageType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmDeclarationType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmRegistrationType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmStructureType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmTransactionLineType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmTransactionType;
import com.oracle.xmlns.apps.gtm.transmission.v6.ServiceRequestType;
import com.oracle.xmlns.apps.gtm.transmission.v6.ServiceResponseType;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementRef;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             A GLogXMLElement represents an OTM or GTM Integration transaction.
 *             The GLogXMLTransaction element is used a Substitution Group head element. All valid Transaction interface elements will
 *             declare that they can substitute for the GLogXMLTransaction. For example, the TransOrder interface is a valid
 *             inbound and outbound transaction and can be used in place of the GLogXMLTransaction element:
 * 
 *             GLogXMLElement start tag
 *                TransactionHeader (optional)
 *                TransOrder
 *             GLogXMLElement end tag
 * 
 *          
 * 
 * <p>Java class for GLogXMLElementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLElementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionHeaderType" minOccurs="0"/&gt;
 *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransaction"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLElementType", propOrder = {
    "transactionHeader",
    "gLogXMLTransaction"
})
public class GLogXMLElementType {

    @XmlElement(name = "TransactionHeader")
    protected TransactionHeaderType transactionHeader;
    @XmlElementRef(name = "GLogXMLTransaction", namespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", type = JAXBElement.class)
    protected JAXBElement<? extends GLogXMLTransactionType> gLogXMLTransaction;

    /**
     * Gets the value of the transactionHeader property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionHeaderType }
     *     
     */
    public TransactionHeaderType getTransactionHeader() {
        return transactionHeader;
    }

    /**
     * Sets the value of the transactionHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionHeaderType }
     *     
     */
    public void setTransactionHeader(TransactionHeaderType value) {
        this.transactionHeader = value;
    }

    /**
     * Gets the value of the gLogXMLTransaction property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GtmBondType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationMessageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmRegistrationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmStructureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceRequestType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AccrualType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActivityTimeDefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActualShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AllocationBaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BillingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BookingLineAmendmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkContMoveType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkRatingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkTrailerBuildType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CSVDataLoadType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CharterVoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ClaimType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConsolType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CorporationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DataQuerySummaryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DemurrageTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DocumentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverCalendarEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverType }{@code >}
     *     {@link JAXBElement }{@code <}{@link EquipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ExchangeRateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FinancialSystemFeedType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FleetBulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GLogXMLTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GenericStatusUpdateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatGenericType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemMasterType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItineraryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link JobType }{@code >}
     *     {@link JAXBElement }{@code <}{@link LocationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MileageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OrderMovementType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PartnerItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PartySiteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PlannedShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PowerUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link QuoteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateGeoType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateOfferingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseInstructionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryReplyType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RouteTemplateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceTimeType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipStopType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupTenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TenderResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TopicType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransactionAckType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransmissionReportType }{@code >}
     *     {@link JAXBElement }{@code <}{@link UserType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoucherType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WorkInvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link XLaneType }{@code >}
     *     
     */
    public JAXBElement<? extends GLogXMLTransactionType> getGLogXMLTransaction() {
        return gLogXMLTransaction;
    }

    /**
     * Sets the value of the gLogXMLTransaction property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GtmBondType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationMessageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmDeclarationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmRegistrationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmStructureType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GtmTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceRequestType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AccrualType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActivityTimeDefType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ActualShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link AllocationBaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BillingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BookingLineAmendmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkContMoveType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkRatingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link BulkTrailerBuildType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CSVDataLoadType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CharterVoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ClaimType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ConsolType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ContactType }{@code >}
     *     {@link JAXBElement }{@code <}{@link CorporationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DataQuerySummaryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DemurrageTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DeviceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DocumentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverCalendarEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link DriverType }{@code >}
     *     {@link JAXBElement }{@code <}{@link EquipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ExchangeRateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FinancialSystemFeedType }{@code >}
     *     {@link JAXBElement }{@code <}{@link FleetBulkPlanType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GLogXMLTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link GenericStatusUpdateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatGenericType }{@code >}
     *     {@link JAXBElement }{@code <}{@link HazmatItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link InvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemMasterType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ItineraryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link JobType }{@code >}
     *     {@link JAXBElement }{@code <}{@link LocationType }{@code >}
     *     {@link JAXBElement }{@code <}{@link MileageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBLineType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OBShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link OrderMovementType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PartnerItemType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PartySiteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PlannedShipmentType }{@code >}
     *     {@link JAXBElement }{@code <}{@link PowerUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link QuoteType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateGeoType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RateOfferingType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseInstructionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ReleaseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryReplyType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RemoteQueryType }{@code >}
     *     {@link JAXBElement }{@code <}{@link RouteTemplateType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SShipUnitType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ServiceTimeType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipStopType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupTenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentGroupType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link ShipmentStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuEventType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuTransactionType }{@code >}
     *     {@link JAXBElement }{@code <}{@link SkuType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TenderOfferType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TenderResponseType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TopicType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderLinkType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderStatusType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransOrderType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransactionAckType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TransmissionReportType }{@code >}
     *     {@link JAXBElement }{@code <}{@link UserType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoucherType }{@code >}
     *     {@link JAXBElement }{@code <}{@link VoyageType }{@code >}
     *     {@link JAXBElement }{@code <}{@link WorkInvoiceType }{@code >}
     *     {@link JAXBElement }{@code <}{@link XLaneType }{@code >}
     *     
     */
    public void setGLogXMLTransaction(JAXBElement<? extends GLogXMLTransactionType> value) {
        this.gLogXMLTransaction = value;
    }

}

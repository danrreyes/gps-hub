
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             To specify a ShipmentGid, the Prev/NextObjectType would be set to 'S'.
 *          
 * 
 * <p>Java class for ShipmentLinkType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentLinkType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="PrevObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PrevObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PrevShipmentStopNum" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="NextObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="NextObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="NextShipmentStopNum" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentLinkType", propOrder = {
    "transactionCode",
    "prevObjectType",
    "prevObjectGid",
    "prevShipmentStopNum",
    "nextObjectType",
    "nextObjectGid",
    "nextShipmentStopNum"
})
public class ShipmentLinkType
    extends OTMTransactionIn
{

    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "PrevObjectType", required = true)
    protected String prevObjectType;
    @XmlElement(name = "PrevObjectGid", required = true)
    protected GLogXMLGidType prevObjectGid;
    @XmlElement(name = "PrevShipmentStopNum")
    protected ShipmentLinkType.PrevShipmentStopNum prevShipmentStopNum;
    @XmlElement(name = "NextObjectType", required = true)
    protected String nextObjectType;
    @XmlElement(name = "NextObjectGid", required = true)
    protected GLogXMLGidType nextObjectGid;
    @XmlElement(name = "NextShipmentStopNum")
    protected ShipmentLinkType.NextShipmentStopNum nextShipmentStopNum;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the prevObjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevObjectType() {
        return prevObjectType;
    }

    /**
     * Sets the value of the prevObjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevObjectType(String value) {
        this.prevObjectType = value;
    }

    /**
     * Gets the value of the prevObjectGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrevObjectGid() {
        return prevObjectGid;
    }

    /**
     * Sets the value of the prevObjectGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrevObjectGid(GLogXMLGidType value) {
        this.prevObjectGid = value;
    }

    /**
     * Gets the value of the prevShipmentStopNum property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentLinkType.PrevShipmentStopNum }
     *     
     */
    public ShipmentLinkType.PrevShipmentStopNum getPrevShipmentStopNum() {
        return prevShipmentStopNum;
    }

    /**
     * Sets the value of the prevShipmentStopNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentLinkType.PrevShipmentStopNum }
     *     
     */
    public void setPrevShipmentStopNum(ShipmentLinkType.PrevShipmentStopNum value) {
        this.prevShipmentStopNum = value;
    }

    /**
     * Gets the value of the nextObjectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNextObjectType() {
        return nextObjectType;
    }

    /**
     * Sets the value of the nextObjectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNextObjectType(String value) {
        this.nextObjectType = value;
    }

    /**
     * Gets the value of the nextObjectGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNextObjectGid() {
        return nextObjectGid;
    }

    /**
     * Sets the value of the nextObjectGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNextObjectGid(GLogXMLGidType value) {
        this.nextObjectGid = value;
    }

    /**
     * Gets the value of the nextShipmentStopNum property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentLinkType.NextShipmentStopNum }
     *     
     */
    public ShipmentLinkType.NextShipmentStopNum getNextShipmentStopNum() {
        return nextShipmentStopNum;
    }

    /**
     * Sets the value of the nextShipmentStopNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentLinkType.NextShipmentStopNum }
     *     
     */
    public void setNextShipmentStopNum(ShipmentLinkType.NextShipmentStopNum value) {
        this.nextShipmentStopNum = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stopSequence"
    })
    public static class NextShipmentStopNum {

        @XmlElement(name = "StopSequence", required = true)
        protected String stopSequence;

        /**
         * Gets the value of the stopSequence property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStopSequence() {
            return stopSequence;
        }

        /**
         * Sets the value of the stopSequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStopSequence(String value) {
            this.stopSequence = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "stopSequence"
    })
    public static class PrevShipmentStopNum {

        @XmlElement(name = "StopSequence", required = true)
        protected String stopSequence;

        /**
         * Gets the value of the stopSequence property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStopSequence() {
            return stopSequence;
        }

        /**
         * Sets the value of the stopSequence property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStopSequence(String value) {
            this.stopSequence = value;
        }

    }

}

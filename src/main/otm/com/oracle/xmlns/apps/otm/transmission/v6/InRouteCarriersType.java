
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             InRouteCarriers identifies a particular carrier that is involved in the shipment/activity
 *          
 * 
 * <p>Java class for InRouteCarriersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InRouteCarriersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" minOccurs="0"/&gt;
 *         &lt;element name="InRouteCarrierSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InRouteTransportationMethod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InRouteCarriersType", propOrder = {
    "serviceProviderAlias",
    "inRouteCarrierSequence",
    "inRouteTransportationMethod"
})
public class InRouteCarriersType {

    @XmlElement(name = "ServiceProviderAlias")
    protected ServiceProviderAliasType serviceProviderAlias;
    @XmlElement(name = "InRouteCarrierSequence")
    protected String inRouteCarrierSequence;
    @XmlElement(name = "InRouteTransportationMethod")
    protected String inRouteTransportationMethod;

    /**
     * Gets the value of the serviceProviderAlias property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public ServiceProviderAliasType getServiceProviderAlias() {
        return serviceProviderAlias;
    }

    /**
     * Sets the value of the serviceProviderAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public void setServiceProviderAlias(ServiceProviderAliasType value) {
        this.serviceProviderAlias = value;
    }

    /**
     * Gets the value of the inRouteCarrierSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInRouteCarrierSequence() {
        return inRouteCarrierSequence;
    }

    /**
     * Sets the value of the inRouteCarrierSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInRouteCarrierSequence(String value) {
        this.inRouteCarrierSequence = value;
    }

    /**
     * Gets the value of the inRouteTransportationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInRouteTransportationMethod() {
        return inRouteTransportationMethod;
    }

    /**
     * Sets the value of the inRouteTransportationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInRouteTransportationMethod(String value) {
        this.inRouteTransportationMethod = value;
    }

}

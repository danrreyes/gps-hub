
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLXLaneNodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLXLaneNodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="XLaneNode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}XLaneNodeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLXLaneNodeType", propOrder = {
    "xLaneNode"
})
public class GLogXMLXLaneNodeType {

    @XmlElement(name = "XLaneNode", required = true)
    protected XLaneNodeType xLaneNode;

    /**
     * Gets the value of the xLaneNode property.
     * 
     * @return
     *     possible object is
     *     {@link XLaneNodeType }
     *     
     */
    public XLaneNodeType getXLaneNode() {
        return xLaneNode;
    }

    /**
     * Sets the value of the xLaneNode property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLaneNodeType }
     *     
     */
    public void setXLaneNode(XLaneNodeType value) {
        this.xLaneNode = value;
    }

}

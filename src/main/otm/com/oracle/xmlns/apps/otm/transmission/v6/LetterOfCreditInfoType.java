
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Letter of credit information.
 * 
 * <p>Java class for LetterOfCreditInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LetterOfCreditInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LetterOfCreditNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsLetterOfCreditRequired" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IssueDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExpireDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ReceivedDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LatestShippingDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="NegotiatedDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LetterOfCreditAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AdvisingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ConfirmingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsLetterOfCreditStale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LetterOfCreditInfoType", propOrder = {
    "letterOfCreditNum",
    "isLetterOfCreditRequired",
    "issueDt",
    "expireDt",
    "receivedDt",
    "latestShippingDt",
    "negotiatedDays",
    "letterOfCreditAmount",
    "advisingNumber",
    "confirmingNumber",
    "isLetterOfCreditStale"
})
public class LetterOfCreditInfoType {

    @XmlElement(name = "LetterOfCreditNum")
    protected String letterOfCreditNum;
    @XmlElement(name = "IsLetterOfCreditRequired")
    protected String isLetterOfCreditRequired;
    @XmlElement(name = "IssueDt")
    protected GLogDateTimeType issueDt;
    @XmlElement(name = "ExpireDt")
    protected GLogDateTimeType expireDt;
    @XmlElement(name = "ReceivedDt")
    protected GLogDateTimeType receivedDt;
    @XmlElement(name = "LatestShippingDt")
    protected GLogDateTimeType latestShippingDt;
    @XmlElement(name = "NegotiatedDays")
    protected String negotiatedDays;
    @XmlElement(name = "LetterOfCreditAmount")
    protected GLogXMLFinancialAmountType letterOfCreditAmount;
    @XmlElement(name = "AdvisingNumber")
    protected String advisingNumber;
    @XmlElement(name = "ConfirmingNumber")
    protected String confirmingNumber;
    @XmlElement(name = "IsLetterOfCreditStale")
    protected String isLetterOfCreditStale;

    /**
     * Gets the value of the letterOfCreditNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLetterOfCreditNum() {
        return letterOfCreditNum;
    }

    /**
     * Sets the value of the letterOfCreditNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLetterOfCreditNum(String value) {
        this.letterOfCreditNum = value;
    }

    /**
     * Gets the value of the isLetterOfCreditRequired property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLetterOfCreditRequired() {
        return isLetterOfCreditRequired;
    }

    /**
     * Sets the value of the isLetterOfCreditRequired property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLetterOfCreditRequired(String value) {
        this.isLetterOfCreditRequired = value;
    }

    /**
     * Gets the value of the issueDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIssueDt() {
        return issueDt;
    }

    /**
     * Sets the value of the issueDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIssueDt(GLogDateTimeType value) {
        this.issueDt = value;
    }

    /**
     * Gets the value of the expireDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpireDt() {
        return expireDt;
    }

    /**
     * Sets the value of the expireDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpireDt(GLogDateTimeType value) {
        this.expireDt = value;
    }

    /**
     * Gets the value of the receivedDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getReceivedDt() {
        return receivedDt;
    }

    /**
     * Sets the value of the receivedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setReceivedDt(GLogDateTimeType value) {
        this.receivedDt = value;
    }

    /**
     * Gets the value of the latestShippingDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLatestShippingDt() {
        return latestShippingDt;
    }

    /**
     * Sets the value of the latestShippingDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLatestShippingDt(GLogDateTimeType value) {
        this.latestShippingDt = value;
    }

    /**
     * Gets the value of the negotiatedDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegotiatedDays() {
        return negotiatedDays;
    }

    /**
     * Sets the value of the negotiatedDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegotiatedDays(String value) {
        this.negotiatedDays = value;
    }

    /**
     * Gets the value of the letterOfCreditAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getLetterOfCreditAmount() {
        return letterOfCreditAmount;
    }

    /**
     * Sets the value of the letterOfCreditAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setLetterOfCreditAmount(GLogXMLFinancialAmountType value) {
        this.letterOfCreditAmount = value;
    }

    /**
     * Gets the value of the advisingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdvisingNumber() {
        return advisingNumber;
    }

    /**
     * Sets the value of the advisingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdvisingNumber(String value) {
        this.advisingNumber = value;
    }

    /**
     * Gets the value of the confirmingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfirmingNumber() {
        return confirmingNumber;
    }

    /**
     * Sets the value of the confirmingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfirmingNumber(String value) {
        this.confirmingNumber = value;
    }

    /**
     * Gets the value of the isLetterOfCreditStale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLetterOfCreditStale() {
        return isLetterOfCreditStale;
    }

    /**
     * Sets the value of the isLetterOfCreditStale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLetterOfCreditStale(String value) {
        this.isLetterOfCreditStale = value;
    }

}

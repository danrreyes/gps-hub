
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RATE_GEO_COST_UNIT_BREAK_TYPE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RATE_GEO_COST_UNIT_BREAK_TYPE"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RATE_GEO_COST_UNIT_BREAK_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RATE_GEO_COST_UNIT_BREAK_TYPE", propOrder = {
    "rategeocostunitbreakrow"
})
public class RATEGEOCOSTUNITBREAKTYPE {

    @XmlElement(name = "RATE_GEO_COST_UNIT_BREAK_ROW")
    protected List<RATEGEOCOSTUNITBREAKTYPE.RATEGEOCOSTUNITBREAKROW> rategeocostunitbreakrow;

    /**
     * Gets the value of the rategeocostunitbreakrow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rategeocostunitbreakrow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEGEOCOSTUNITBREAKROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RATEGEOCOSTUNITBREAKTYPE.RATEGEOCOSTUNITBREAKROW }
     * 
     * 
     */
    public List<RATEGEOCOSTUNITBREAKTYPE.RATEGEOCOSTUNITBREAKROW> getRATEGEOCOSTUNITBREAKROW() {
        if (rategeocostunitbreakrow == null) {
            rategeocostunitbreakrow = new ArrayList<RATEGEOCOSTUNITBREAKTYPE.RATEGEOCOSTUNITBREAKROW>();
        }
        return this.rategeocostunitbreakrow;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RATE_GEO_COST_SEQ" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RATE_UNIT_BREAK_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CHARGE_AMOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_AMOUNT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_AMOUNT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CHARGE_DISCOUNT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST_CURRENCY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_UNIT_BREAK2_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rategeocostgroupgid",
        "rategeocostseq",
        "rateunitbreakgid",
        "chargeamount",
        "chargeamountgid",
        "chargeamountbase",
        "chargediscount",
        "mincost",
        "mincostcurrencygid",
        "mincostbase",
        "maxcost",
        "maxcostcurrencygid",
        "maxcostbase",
        "rateunitbreak2GID",
        "domainname",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate"
    })
    public static class RATEGEOCOSTUNITBREAKROW {

        @XmlElement(name = "RATE_GEO_COST_GROUP_GID", required = true)
        protected String rategeocostgroupgid;
        @XmlElement(name = "RATE_GEO_COST_SEQ", required = true)
        protected String rategeocostseq;
        @XmlElement(name = "RATE_UNIT_BREAK_GID", required = true)
        protected String rateunitbreakgid;
        @XmlElement(name = "CHARGE_AMOUNT")
        protected String chargeamount;
        @XmlElement(name = "CHARGE_AMOUNT_GID")
        protected String chargeamountgid;
        @XmlElement(name = "CHARGE_AMOUNT_BASE")
        protected String chargeamountbase;
        @XmlElement(name = "CHARGE_DISCOUNT")
        protected String chargediscount;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_CURRENCY_GID")
        protected String mincostcurrencygid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "MAX_COST")
        protected String maxcost;
        @XmlElement(name = "MAX_COST_CURRENCY_GID")
        protected String maxcostcurrencygid;
        @XmlElement(name = "MAX_COST_BASE")
        protected String maxcostbase;
        @XmlElement(name = "RATE_UNIT_BREAK2_GID")
        protected String rateunitbreak2GID;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Gets the value of the rategeocostgroupgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTGROUPGID() {
            return rategeocostgroupgid;
        }

        /**
         * Sets the value of the rategeocostgroupgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTGROUPGID(String value) {
            this.rategeocostgroupgid = value;
        }

        /**
         * Gets the value of the rategeocostseq property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOCOSTSEQ() {
            return rategeocostseq;
        }

        /**
         * Sets the value of the rategeocostseq property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOCOSTSEQ(String value) {
            this.rategeocostseq = value;
        }

        /**
         * Gets the value of the rateunitbreakgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAKGID() {
            return rateunitbreakgid;
        }

        /**
         * Sets the value of the rateunitbreakgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAKGID(String value) {
            this.rateunitbreakgid = value;
        }

        /**
         * Gets the value of the chargeamount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNT() {
            return chargeamount;
        }

        /**
         * Sets the value of the chargeamount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNT(String value) {
            this.chargeamount = value;
        }

        /**
         * Gets the value of the chargeamountgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNTGID() {
            return chargeamountgid;
        }

        /**
         * Sets the value of the chargeamountgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNTGID(String value) {
            this.chargeamountgid = value;
        }

        /**
         * Gets the value of the chargeamountbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEAMOUNTBASE() {
            return chargeamountbase;
        }

        /**
         * Sets the value of the chargeamountbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEAMOUNTBASE(String value) {
            this.chargeamountbase = value;
        }

        /**
         * Gets the value of the chargediscount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCHARGEDISCOUNT() {
            return chargediscount;
        }

        /**
         * Sets the value of the chargediscount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCHARGEDISCOUNT(String value) {
            this.chargediscount = value;
        }

        /**
         * Gets the value of the mincost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Sets the value of the mincost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Gets the value of the mincostcurrencygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTCURRENCYGID() {
            return mincostcurrencygid;
        }

        /**
         * Sets the value of the mincostcurrencygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTCURRENCYGID(String value) {
            this.mincostcurrencygid = value;
        }

        /**
         * Gets the value of the mincostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Sets the value of the mincostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Gets the value of the maxcost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOST() {
            return maxcost;
        }

        /**
         * Sets the value of the maxcost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOST(String value) {
            this.maxcost = value;
        }

        /**
         * Gets the value of the maxcostcurrencygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTCURRENCYGID() {
            return maxcostcurrencygid;
        }

        /**
         * Sets the value of the maxcostcurrencygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTCURRENCYGID(String value) {
            this.maxcostcurrencygid = value;
        }

        /**
         * Gets the value of the maxcostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCOSTBASE() {
            return maxcostbase;
        }

        /**
         * Sets the value of the maxcostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCOSTBASE(String value) {
            this.maxcostbase = value;
        }

        /**
         * Gets the value of the rateunitbreak2GID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEUNITBREAK2GID() {
            return rateunitbreak2GID;
        }

        /**
         * Sets the value of the rateunitbreak2GID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEUNITBREAK2GID(String value) {
            this.rateunitbreak2GID = value;
        }

        /**
         * Gets the value of the domainname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Sets the value of the domainname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Gets the value of the insertuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Sets the value of the insertuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Gets the value of the insertdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Sets the value of the insertdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Gets the value of the updateuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Sets the value of the updateuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Gets the value of the updatedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Sets the value of the updatedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Gets the value of the num property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Sets the value of the num property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }

    }

}

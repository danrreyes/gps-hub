
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * port associated with an ocean stop.
 * 
 * <p>Java class for PortType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PortType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PortName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PortLocationFunctionCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="PortIdentifier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PortIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="ProvinceCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PortType", propOrder = {
    "portName",
    "portLocationFunctionCode",
    "portIdentifier",
    "provinceCode",
    "countryCode3Gid"
})
public class PortType {

    @XmlElement(name = "PortName")
    protected String portName;
    @XmlElement(name = "PortLocationFunctionCode", required = true)
    protected String portLocationFunctionCode;
    @XmlElement(name = "PortIdentifier")
    protected PortIdentifierType portIdentifier;
    @XmlElement(name = "ProvinceCode")
    protected String provinceCode;
    @XmlElement(name = "CountryCode3Gid")
    protected GLogXMLGidType countryCode3Gid;

    /**
     * Gets the value of the portName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortName() {
        return portName;
    }

    /**
     * Sets the value of the portName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortName(String value) {
        this.portName = value;
    }

    /**
     * Gets the value of the portLocationFunctionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortLocationFunctionCode() {
        return portLocationFunctionCode;
    }

    /**
     * Sets the value of the portLocationFunctionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortLocationFunctionCode(String value) {
        this.portLocationFunctionCode = value;
    }

    /**
     * Gets the value of the portIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PortIdentifierType }
     *     
     */
    public PortIdentifierType getPortIdentifier() {
        return portIdentifier;
    }

    /**
     * Sets the value of the portIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PortIdentifierType }
     *     
     */
    public void setPortIdentifier(PortIdentifierType value) {
        this.portIdentifier = value;
    }

    /**
     * Gets the value of the provinceCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvinceCode() {
        return provinceCode;
    }

    /**
     * Sets the value of the provinceCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvinceCode(String value) {
        this.provinceCode = value;
    }

    /**
     * Gets the value of the countryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryCode3Gid() {
        return countryCode3Gid;
    }

    /**
     * Sets the value of the countryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryCode3Gid(GLogXMLGidType value) {
        this.countryCode3Gid = value;
    }

}

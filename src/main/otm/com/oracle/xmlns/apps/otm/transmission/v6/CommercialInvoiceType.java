
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The Commercial Invoice is one of the most common documents used in international trade.
 *             Primarily, the document contains details of what, how much, and the cost of what is being shipped.
 *             The cost of the goods can vary depending on payment terms.
 *          
 * 
 * <p>Java class for CommercialInvoiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialInvoiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CommercialInvoiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="CommercialInvoiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CommercialInvoiceNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InvoiceDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GlobalCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/&gt;
 *         &lt;element name="CurrencyFrom" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CurrencyCodeType" minOccurs="0"/&gt;
 *         &lt;element name="IsOverrideExchangeRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProductAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="IsCalcProductAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OtherCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="IsCalcTotalInvoiceAmt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TotalInvoiceAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialTermsType" minOccurs="0"/&gt;
 *         &lt;element name="FinalCommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinalCommercialTermsType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialInvoiceTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceTermsType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialInvoiceCharge" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceChargeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialInvoiceType", propOrder = {
    "commercialInvoiceGid",
    "transactionCode",
    "commercialInvoiceType",
    "commercialInvoiceNum",
    "invoiceDate",
    "exchangeRateValue",
    "globalCurrencyCode",
    "exchangeRateInfo",
    "currencyFrom",
    "isOverrideExchangeRate",
    "productAmount",
    "isCalcProductAmt",
    "otherCharge",
    "isCalcTotalInvoiceAmt",
    "totalInvoiceAmount",
    "commercialTerms",
    "finalCommercialTerms",
    "commercialInvoiceTerms",
    "commercialInvoiceCharge"
})
public class CommercialInvoiceType {

    @XmlElement(name = "CommercialInvoiceGid", required = true)
    protected GLogXMLGidType commercialInvoiceGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "CommercialInvoiceType")
    protected String commercialInvoiceType;
    @XmlElement(name = "CommercialInvoiceNum")
    protected String commercialInvoiceNum;
    @XmlElement(name = "InvoiceDate")
    protected GLogDateTimeType invoiceDate;
    @XmlElement(name = "ExchangeRateValue")
    protected String exchangeRateValue;
    @XmlElement(name = "GlobalCurrencyCode")
    protected String globalCurrencyCode;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;
    @XmlElement(name = "CurrencyFrom")
    protected CurrencyCodeType currencyFrom;
    @XmlElement(name = "IsOverrideExchangeRate")
    protected String isOverrideExchangeRate;
    @XmlElement(name = "ProductAmount", required = true)
    protected GLogXMLFinancialAmountType productAmount;
    @XmlElement(name = "IsCalcProductAmt")
    protected String isCalcProductAmt;
    @XmlElement(name = "OtherCharge")
    protected GLogXMLFinancialAmountType otherCharge;
    @XmlElement(name = "IsCalcTotalInvoiceAmt")
    protected String isCalcTotalInvoiceAmt;
    @XmlElement(name = "TotalInvoiceAmount")
    protected GLogXMLFinancialAmountType totalInvoiceAmount;
    @XmlElement(name = "CommercialTerms")
    protected CommercialTermsType commercialTerms;
    @XmlElement(name = "FinalCommercialTerms")
    protected FinalCommercialTermsType finalCommercialTerms;
    @XmlElement(name = "CommercialInvoiceTerms")
    protected CommercialInvoiceTermsType commercialInvoiceTerms;
    @XmlElement(name = "CommercialInvoiceCharge")
    protected List<CommercialInvoiceChargeType> commercialInvoiceCharge;

    /**
     * Gets the value of the commercialInvoiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommercialInvoiceGid() {
        return commercialInvoiceGid;
    }

    /**
     * Sets the value of the commercialInvoiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommercialInvoiceGid(GLogXMLGidType value) {
        this.commercialInvoiceGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the commercialInvoiceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialInvoiceType() {
        return commercialInvoiceType;
    }

    /**
     * Sets the value of the commercialInvoiceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialInvoiceType(String value) {
        this.commercialInvoiceType = value;
    }

    /**
     * Gets the value of the commercialInvoiceNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialInvoiceNum() {
        return commercialInvoiceNum;
    }

    /**
     * Sets the value of the commercialInvoiceNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialInvoiceNum(String value) {
        this.commercialInvoiceNum = value;
    }

    /**
     * Gets the value of the invoiceDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInvoiceDate() {
        return invoiceDate;
    }

    /**
     * Sets the value of the invoiceDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInvoiceDate(GLogDateTimeType value) {
        this.invoiceDate = value;
    }

    /**
     * Gets the value of the exchangeRateValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExchangeRateValue() {
        return exchangeRateValue;
    }

    /**
     * Sets the value of the exchangeRateValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExchangeRateValue(String value) {
        this.exchangeRateValue = value;
    }

    /**
     * Gets the value of the globalCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    /**
     * Sets the value of the globalCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGlobalCurrencyCode(String value) {
        this.globalCurrencyCode = value;
    }

    /**
     * Gets the value of the exchangeRateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Sets the value of the exchangeRateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

    /**
     * Gets the value of the currencyFrom property.
     * 
     * @return
     *     possible object is
     *     {@link CurrencyCodeType }
     *     
     */
    public CurrencyCodeType getCurrencyFrom() {
        return currencyFrom;
    }

    /**
     * Sets the value of the currencyFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link CurrencyCodeType }
     *     
     */
    public void setCurrencyFrom(CurrencyCodeType value) {
        this.currencyFrom = value;
    }

    /**
     * Gets the value of the isOverrideExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsOverrideExchangeRate() {
        return isOverrideExchangeRate;
    }

    /**
     * Sets the value of the isOverrideExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsOverrideExchangeRate(String value) {
        this.isOverrideExchangeRate = value;
    }

    /**
     * Gets the value of the productAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getProductAmount() {
        return productAmount;
    }

    /**
     * Sets the value of the productAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setProductAmount(GLogXMLFinancialAmountType value) {
        this.productAmount = value;
    }

    /**
     * Gets the value of the isCalcProductAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCalcProductAmt() {
        return isCalcProductAmt;
    }

    /**
     * Sets the value of the isCalcProductAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCalcProductAmt(String value) {
        this.isCalcProductAmt = value;
    }

    /**
     * Gets the value of the otherCharge property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getOtherCharge() {
        return otherCharge;
    }

    /**
     * Sets the value of the otherCharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setOtherCharge(GLogXMLFinancialAmountType value) {
        this.otherCharge = value;
    }

    /**
     * Gets the value of the isCalcTotalInvoiceAmt property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCalcTotalInvoiceAmt() {
        return isCalcTotalInvoiceAmt;
    }

    /**
     * Sets the value of the isCalcTotalInvoiceAmt property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCalcTotalInvoiceAmt(String value) {
        this.isCalcTotalInvoiceAmt = value;
    }

    /**
     * Gets the value of the totalInvoiceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalInvoiceAmount() {
        return totalInvoiceAmount;
    }

    /**
     * Sets the value of the totalInvoiceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalInvoiceAmount(GLogXMLFinancialAmountType value) {
        this.totalInvoiceAmount = value;
    }

    /**
     * Gets the value of the commercialTerms property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialTermsType }
     *     
     */
    public CommercialTermsType getCommercialTerms() {
        return commercialTerms;
    }

    /**
     * Sets the value of the commercialTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialTermsType }
     *     
     */
    public void setCommercialTerms(CommercialTermsType value) {
        this.commercialTerms = value;
    }

    /**
     * Gets the value of the finalCommercialTerms property.
     * 
     * @return
     *     possible object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public FinalCommercialTermsType getFinalCommercialTerms() {
        return finalCommercialTerms;
    }

    /**
     * Sets the value of the finalCommercialTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public void setFinalCommercialTerms(FinalCommercialTermsType value) {
        this.finalCommercialTerms = value;
    }

    /**
     * Gets the value of the commercialInvoiceTerms property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public CommercialInvoiceTermsType getCommercialInvoiceTerms() {
        return commercialInvoiceTerms;
    }

    /**
     * Sets the value of the commercialInvoiceTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public void setCommercialInvoiceTerms(CommercialInvoiceTermsType value) {
        this.commercialInvoiceTerms = value;
    }

    /**
     * Gets the value of the commercialInvoiceCharge property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the commercialInvoiceCharge property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCommercialInvoiceCharge().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CommercialInvoiceChargeType }
     * 
     * 
     */
    public List<CommercialInvoiceChargeType> getCommercialInvoiceCharge() {
        if (commercialInvoiceCharge == null) {
            commercialInvoiceCharge = new ArrayList<CommercialInvoiceChargeType>();
        }
        return this.commercialInvoiceCharge;
    }

}

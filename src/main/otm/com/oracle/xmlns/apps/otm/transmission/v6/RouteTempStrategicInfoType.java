
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Specifies the route template strategic info.
 *             The values are prepopulated from the cooperative route if the creation source is C.
 *          
 * 
 * <p>Java class for RouteTempStrategicInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RouteTempStrategicInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TotalDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="LoadedDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="DeadheadDistance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="DeadheadPct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SavingsPerInstance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="Duration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DurationType" minOccurs="0"/&gt;
 *         &lt;element name="ConfidenceFactor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTempStrategicInfoType", propOrder = {
    "totalDistance",
    "loadedDistance",
    "deadheadDistance",
    "deadheadPct",
    "savingsPerInstance",
    "duration",
    "confidenceFactor"
})
public class RouteTempStrategicInfoType {

    @XmlElement(name = "TotalDistance")
    protected GLogXMLDistanceType totalDistance;
    @XmlElement(name = "LoadedDistance")
    protected GLogXMLDistanceType loadedDistance;
    @XmlElement(name = "DeadheadDistance")
    protected GLogXMLDistanceType deadheadDistance;
    @XmlElement(name = "DeadheadPct")
    protected String deadheadPct;
    @XmlElement(name = "SavingsPerInstance")
    protected GLogXMLFinancialAmountType savingsPerInstance;
    @XmlElement(name = "Duration")
    protected DurationType duration;
    @XmlElement(name = "ConfidenceFactor")
    protected String confidenceFactor;

    /**
     * Gets the value of the totalDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getTotalDistance() {
        return totalDistance;
    }

    /**
     * Sets the value of the totalDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setTotalDistance(GLogXMLDistanceType value) {
        this.totalDistance = value;
    }

    /**
     * Gets the value of the loadedDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getLoadedDistance() {
        return loadedDistance;
    }

    /**
     * Sets the value of the loadedDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setLoadedDistance(GLogXMLDistanceType value) {
        this.loadedDistance = value;
    }

    /**
     * Gets the value of the deadheadDistance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDeadheadDistance() {
        return deadheadDistance;
    }

    /**
     * Sets the value of the deadheadDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDeadheadDistance(GLogXMLDistanceType value) {
        this.deadheadDistance = value;
    }

    /**
     * Gets the value of the deadheadPct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeadheadPct() {
        return deadheadPct;
    }

    /**
     * Sets the value of the deadheadPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeadheadPct(String value) {
        this.deadheadPct = value;
    }

    /**
     * Gets the value of the savingsPerInstance property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getSavingsPerInstance() {
        return savingsPerInstance;
    }

    /**
     * Sets the value of the savingsPerInstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setSavingsPerInstance(GLogXMLFinancialAmountType value) {
        this.savingsPerInstance = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link DurationType }
     *     
     */
    public DurationType getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationType }
     *     
     */
    public void setDuration(DurationType value) {
        this.duration = value;
    }

    /**
     * Gets the value of the confidenceFactor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConfidenceFactor() {
        return confidenceFactor;
    }

    /**
     * Sets the value of the confidenceFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConfidenceFactor(String value) {
        this.confidenceFactor = value;
    }

}

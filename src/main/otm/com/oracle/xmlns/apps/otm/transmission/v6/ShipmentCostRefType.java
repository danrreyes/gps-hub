
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Provides a breakdown of the shipment costs based on the shipment qualifiers.
 *             For example Item cost, order base cost, order release cost, order release line cost etc.
 *          
 * 
 * <p>Java class for ShipmentCostRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentCostRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CostReferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipmentCostQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="IsPickup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsDropoff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentCostRefType", propOrder = {
    "sequenceNumber",
    "costReferenceGid",
    "shipmentCostQualGid",
    "isPickup",
    "isDropoff"
})
public class ShipmentCostRefType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "CostReferenceGid", required = true)
    protected GLogXMLGidType costReferenceGid;
    @XmlElement(name = "ShipmentCostQualGid", required = true)
    protected GLogXMLGidType shipmentCostQualGid;
    @XmlElement(name = "IsPickup")
    protected String isPickup;
    @XmlElement(name = "IsDropoff")
    protected String isDropoff;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the costReferenceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostReferenceGid() {
        return costReferenceGid;
    }

    /**
     * Sets the value of the costReferenceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostReferenceGid(GLogXMLGidType value) {
        this.costReferenceGid = value;
    }

    /**
     * Gets the value of the shipmentCostQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentCostQualGid() {
        return shipmentCostQualGid;
    }

    /**
     * Sets the value of the shipmentCostQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentCostQualGid(GLogXMLGidType value) {
        this.shipmentCostQualGid = value;
    }

    /**
     * Gets the value of the isPickup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPickup() {
        return isPickup;
    }

    /**
     * Sets the value of the isPickup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPickup(String value) {
        this.isPickup = value;
    }

    /**
     * Gets the value of the isDropoff property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDropoff() {
        return isDropoff;
    }

    /**
     * Sets the value of the isDropoff property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDropoff(String value) {
        this.isDropoff = value;
    }

}

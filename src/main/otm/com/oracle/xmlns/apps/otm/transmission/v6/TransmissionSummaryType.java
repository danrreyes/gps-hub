
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * TransmissionSummary is the summary component of a TransmissionReport.
 * 
 * <p>Java class for TransmissionSummaryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransmissionSummaryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FirstTransactionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastTransactionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ApplicationReport" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="ApplicationName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="InsertCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="UpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="InsertUpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="DeleteCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ErrorCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionSummaryType", propOrder = {
    "transmissionNo",
    "firstTransactionNo",
    "lastTransactionNo",
    "applicationReport"
})
public class TransmissionSummaryType {

    @XmlElement(name = "TransmissionNo")
    protected String transmissionNo;
    @XmlElement(name = "FirstTransactionNo")
    protected String firstTransactionNo;
    @XmlElement(name = "LastTransactionNo")
    protected String lastTransactionNo;
    @XmlElement(name = "ApplicationReport")
    protected List<TransmissionSummaryType.ApplicationReport> applicationReport;

    /**
     * Gets the value of the transmissionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionNo() {
        return transmissionNo;
    }

    /**
     * Sets the value of the transmissionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionNo(String value) {
        this.transmissionNo = value;
    }

    /**
     * Gets the value of the firstTransactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstTransactionNo() {
        return firstTransactionNo;
    }

    /**
     * Sets the value of the firstTransactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstTransactionNo(String value) {
        this.firstTransactionNo = value;
    }

    /**
     * Gets the value of the lastTransactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastTransactionNo() {
        return lastTransactionNo;
    }

    /**
     * Sets the value of the lastTransactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastTransactionNo(String value) {
        this.lastTransactionNo = value;
    }

    /**
     * Gets the value of the applicationReport property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the applicationReport property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicationReport().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransmissionSummaryType.ApplicationReport }
     * 
     * 
     */
    public List<TransmissionSummaryType.ApplicationReport> getApplicationReport() {
        if (applicationReport == null) {
            applicationReport = new ArrayList<TransmissionSummaryType.ApplicationReport>();
        }
        return this.applicationReport;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ApplicationName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="InsertCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="UpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="InsertUpdateCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="DeleteCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ErrorCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "applicationName",
        "insertCount",
        "updateCount",
        "insertUpdateCount",
        "deleteCount",
        "errorCount"
    })
    public static class ApplicationReport {

        @XmlElement(name = "ApplicationName", required = true)
        protected String applicationName;
        @XmlElement(name = "InsertCount", required = true)
        protected String insertCount;
        @XmlElement(name = "UpdateCount", required = true)
        protected String updateCount;
        @XmlElement(name = "InsertUpdateCount", required = true)
        protected String insertUpdateCount;
        @XmlElement(name = "DeleteCount", required = true)
        protected String deleteCount;
        @XmlElement(name = "ErrorCount", required = true)
        protected String errorCount;

        /**
         * Gets the value of the applicationName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApplicationName() {
            return applicationName;
        }

        /**
         * Sets the value of the applicationName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApplicationName(String value) {
            this.applicationName = value;
        }

        /**
         * Gets the value of the insertCount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInsertCount() {
            return insertCount;
        }

        /**
         * Sets the value of the insertCount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInsertCount(String value) {
            this.insertCount = value;
        }

        /**
         * Gets the value of the updateCount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUpdateCount() {
            return updateCount;
        }

        /**
         * Sets the value of the updateCount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUpdateCount(String value) {
            this.updateCount = value;
        }

        /**
         * Gets the value of the insertUpdateCount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInsertUpdateCount() {
            return insertUpdateCount;
        }

        /**
         * Sets the value of the insertUpdateCount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInsertUpdateCount(String value) {
            this.insertUpdateCount = value;
        }

        /**
         * Gets the value of the deleteCount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDeleteCount() {
            return deleteCount;
        }

        /**
         * Sets the value of the deleteCount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDeleteCount(String value) {
            this.deleteCount = value;
        }

        /**
         * Gets the value of the errorCount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getErrorCount() {
            return errorCount;
        }

        /**
         * Sets the value of the errorCount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setErrorCount(String value) {
            this.errorCount = value;
        }

    }

}

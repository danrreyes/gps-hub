
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             TenderOffer is a wrapper around the Shipment element.
 *          
 * 
 * <p>Java class for TenderOfferType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TenderOfferType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType"/&gt;
 *         &lt;element name="ExpectedResponseDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ConditionalBooking" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ConditionalBookingType" minOccurs="0"/&gt;
 *         &lt;element name="TenderType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TenderOfferType", propOrder = {
    "shipment",
    "expectedResponseDt",
    "conditionalBooking",
    "tenderType",
    "flexCommodityQualifierGid",
    "flexCommodityValue"
})
public class TenderOfferType
    extends OTMTransactionOut
{

    @XmlElement(name = "Shipment", required = true)
    protected ShipmentType shipment;
    @XmlElement(name = "ExpectedResponseDt")
    protected GLogDateTimeType expectedResponseDt;
    @XmlElement(name = "ConditionalBooking")
    protected ConditionalBookingType conditionalBooking;
    @XmlElement(name = "TenderType")
    protected String tenderType;
    @XmlElement(name = "FlexCommodityQualifierGid")
    protected GLogXMLGidType flexCommodityQualifierGid;
    @XmlElement(name = "FlexCommodityValue")
    protected String flexCommodityValue;

    /**
     * Gets the value of the shipment property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentType }
     *     
     */
    public ShipmentType getShipment() {
        return shipment;
    }

    /**
     * Sets the value of the shipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentType }
     *     
     */
    public void setShipment(ShipmentType value) {
        this.shipment = value;
    }

    /**
     * Gets the value of the expectedResponseDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpectedResponseDt() {
        return expectedResponseDt;
    }

    /**
     * Sets the value of the expectedResponseDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpectedResponseDt(GLogDateTimeType value) {
        this.expectedResponseDt = value;
    }

    /**
     * Gets the value of the conditionalBooking property.
     * 
     * @return
     *     possible object is
     *     {@link ConditionalBookingType }
     *     
     */
    public ConditionalBookingType getConditionalBooking() {
        return conditionalBooking;
    }

    /**
     * Sets the value of the conditionalBooking property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionalBookingType }
     *     
     */
    public void setConditionalBooking(ConditionalBookingType value) {
        this.conditionalBooking = value;
    }

    /**
     * Gets the value of the tenderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTenderType() {
        return tenderType;
    }

    /**
     * Sets the value of the tenderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTenderType(String value) {
        this.tenderType = value;
    }

    /**
     * Gets the value of the flexCommodityQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityQualifierGid() {
        return flexCommodityQualifierGid;
    }

    /**
     * Sets the value of the flexCommodityQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityQualifierGid(GLogXMLGidType value) {
        this.flexCommodityQualifierGid = value;
    }

    /**
     * Gets the value of the flexCommodityValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexCommodityValue() {
        return flexCommodityValue;
    }

    /**
     * Sets the value of the flexCommodityValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexCommodityValue(String value) {
        this.flexCommodityValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionDocumentType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EchoedTransmissionHeader"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TransmissionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionHeaderType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="TransmissionAckStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransmissionAckReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StackTrace" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransmissionText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="QueryResultInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "echoedTransmissionHeader",
    "transmissionAckStatus",
    "transmissionAckReason",
    "stackTrace",
    "transmissionText",
    "refnum",
    "queryResultInfo"
})
@XmlRootElement(name = "TransmissionAck")
public class TransmissionAck
    extends TransmissionDocumentType
{

    @XmlElement(name = "EchoedTransmissionHeader", required = true)
    protected TransmissionAck.EchoedTransmissionHeader echoedTransmissionHeader;
    @XmlElement(name = "TransmissionAckStatus")
    protected String transmissionAckStatus;
    @XmlElement(name = "TransmissionAckReason")
    protected String transmissionAckReason;
    @XmlElement(name = "StackTrace")
    protected String stackTrace;
    @XmlElement(name = "TransmissionText")
    protected String transmissionText;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "QueryResultInfo")
    protected TransmissionAck.QueryResultInfo queryResultInfo;
    @XmlAttribute(name = "Version")
    protected String version;

    /**
     * Gets the value of the echoedTransmissionHeader property.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionAck.EchoedTransmissionHeader }
     *     
     */
    public TransmissionAck.EchoedTransmissionHeader getEchoedTransmissionHeader() {
        return echoedTransmissionHeader;
    }

    /**
     * Sets the value of the echoedTransmissionHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionAck.EchoedTransmissionHeader }
     *     
     */
    public void setEchoedTransmissionHeader(TransmissionAck.EchoedTransmissionHeader value) {
        this.echoedTransmissionHeader = value;
    }

    /**
     * Gets the value of the transmissionAckStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionAckStatus() {
        return transmissionAckStatus;
    }

    /**
     * Sets the value of the transmissionAckStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionAckStatus(String value) {
        this.transmissionAckStatus = value;
    }

    /**
     * Gets the value of the transmissionAckReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionAckReason() {
        return transmissionAckReason;
    }

    /**
     * Sets the value of the transmissionAckReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionAckReason(String value) {
        this.transmissionAckReason = value;
    }

    /**
     * Gets the value of the stackTrace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStackTrace() {
        return stackTrace;
    }

    /**
     * Sets the value of the stackTrace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStackTrace(String value) {
        this.stackTrace = value;
    }

    /**
     * Gets the value of the transmissionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionText() {
        return transmissionText;
    }

    /**
     * Sets the value of the transmissionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionText(String value) {
        this.transmissionText = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the queryResultInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionAck.QueryResultInfo }
     *     
     */
    public TransmissionAck.QueryResultInfo getQueryResultInfo() {
        return queryResultInfo;
    }

    /**
     * Sets the value of the queryResultInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionAck.QueryResultInfo }
     *     
     */
    public void setQueryResultInfo(TransmissionAck.QueryResultInfo value) {
        this.queryResultInfo = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TransmissionHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionHeaderType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transmissionHeader"
    })
    public static class EchoedTransmissionHeader {

        @XmlElement(name = "TransmissionHeader", required = true)
        protected TransmissionHeaderType transmissionHeader;

        /**
         * Gets the value of the transmissionHeader property.
         * 
         * @return
         *     possible object is
         *     {@link TransmissionHeaderType }
         *     
         */
        public TransmissionHeaderType getTransmissionHeader() {
            return transmissionHeader;
        }

        /**
         * Sets the value of the transmissionHeader property.
         * 
         * @param value
         *     allowed object is
         *     {@link TransmissionHeaderType }
         *     
         */
        public void setTransmissionHeader(TransmissionHeaderType value) {
            this.transmissionHeader = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="GLogXMLElement" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLElementType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "gLogXMLElement"
    })
    public static class QueryResultInfo {

        @XmlElement(name = "GLogXMLElement")
        protected List<GLogXMLElementType> gLogXMLElement;

        /**
         * Gets the value of the gLogXMLElement property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the gLogXMLElement property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGLogXMLElement().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLElementType }
         * 
         * 
         */
        public List<GLogXMLElementType> getGLogXMLElement() {
            if (gLogXMLElement == null) {
                gLogXMLElement = new ArrayList<GLogXMLElementType>();
            }
            return this.gLogXMLElement;
        }

    }

}

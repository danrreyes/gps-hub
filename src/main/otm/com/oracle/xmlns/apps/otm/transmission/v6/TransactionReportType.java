
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * A TransactionReport contains a transaction number followed by 0 or more integration log messages.
 * 
 * <p>Java class for TransactionReportType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionReportType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ITransactionNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IntegrationLogMessage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntegrationLogMessageType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionReportType", propOrder = {
    "iTransactionNo",
    "integrationLogMessage"
})
public class TransactionReportType {

    @XmlElement(name = "ITransactionNo", required = true)
    protected String iTransactionNo;
    @XmlElement(name = "IntegrationLogMessage")
    protected List<IntegrationLogMessageType> integrationLogMessage;

    /**
     * Gets the value of the iTransactionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getITransactionNo() {
        return iTransactionNo;
    }

    /**
     * Sets the value of the iTransactionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setITransactionNo(String value) {
        this.iTransactionNo = value;
    }

    /**
     * Gets the value of the integrationLogMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the integrationLogMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntegrationLogMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntegrationLogMessageType }
     * 
     * 
     */
    public List<IntegrationLogMessageType> getIntegrationLogMessage() {
        if (integrationLogMessage == null) {
            integrationLogMessage = new ArrayList<IntegrationLogMessageType>();
        }
        return this.integrationLogMessage;
    }

}

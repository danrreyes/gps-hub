
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The property glog.integration.Claim.Location.includeContactGroupLocations controls whether the location(s) for the
 *             ContactGroup Contacts should be included
 *          
 * 
 * <p>Java class for ClaimType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ClaimType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ClaimGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="DamageInformation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReportDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="IncidentDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DamageNotifyPointGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="DamageCauseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DamageNatureGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DamageValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="IsDamageValueFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SizeOfLoss" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="IsSizeOfLossFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="NotificationDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="NotifyPartyContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/&gt;
 *         &lt;element name="LiablePartyContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/&gt;
 *         &lt;element name="IsSelfRecondition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ClaimCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimCostType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ClaimLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimLineItemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ClaimNote" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ClaimNoteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClaimType", propOrder = {
    "claimGid",
    "transactionCode",
    "replaceChildren",
    "damageInformation",
    "reportDt",
    "incidentDt",
    "damageNotifyPointGid",
    "damageCauseGid",
    "damageNatureGid",
    "damageValue",
    "isDamageValueFixed",
    "sizeOfLoss",
    "isSizeOfLossFixed",
    "shipmentGid",
    "notificationDt",
    "notifyPartyContact",
    "liablePartyContact",
    "isSelfRecondition",
    "involvedParty",
    "claimCost",
    "claimLineItem",
    "claimNote",
    "refnum",
    "remark",
    "status",
    "location",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies"
})
public class ClaimType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ClaimGid", required = true)
    protected GLogXMLGidType claimGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "DamageInformation")
    protected String damageInformation;
    @XmlElement(name = "ReportDt")
    protected GLogDateTimeType reportDt;
    @XmlElement(name = "IncidentDt")
    protected GLogDateTimeType incidentDt;
    @XmlElement(name = "DamageNotifyPointGid", required = true)
    protected GLogXMLGidType damageNotifyPointGid;
    @XmlElement(name = "DamageCauseGid")
    protected GLogXMLGidType damageCauseGid;
    @XmlElement(name = "DamageNatureGid")
    protected GLogXMLGidType damageNatureGid;
    @XmlElement(name = "DamageValue")
    protected GLogXMLFinancialAmountType damageValue;
    @XmlElement(name = "IsDamageValueFixed")
    protected String isDamageValueFixed;
    @XmlElement(name = "SizeOfLoss")
    protected GLogXMLFinancialAmountType sizeOfLoss;
    @XmlElement(name = "IsSizeOfLossFixed")
    protected String isSizeOfLossFixed;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "NotificationDt")
    protected GLogDateTimeType notificationDt;
    @XmlElement(name = "NotifyPartyContact")
    protected GLogXMLContactRefType notifyPartyContact;
    @XmlElement(name = "LiablePartyContact")
    protected GLogXMLContactRefType liablePartyContact;
    @XmlElement(name = "IsSelfRecondition")
    protected String isSelfRecondition;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "ClaimCost")
    protected List<ClaimCostType> claimCost;
    @XmlElement(name = "ClaimLineItem")
    protected List<ClaimLineItemType> claimLineItem;
    @XmlElement(name = "ClaimNote")
    protected List<ClaimNoteType> claimNote;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;

    /**
     * Gets the value of the claimGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getClaimGid() {
        return claimGid;
    }

    /**
     * Sets the value of the claimGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setClaimGid(GLogXMLGidType value) {
        this.claimGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the damageInformation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDamageInformation() {
        return damageInformation;
    }

    /**
     * Sets the value of the damageInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDamageInformation(String value) {
        this.damageInformation = value;
    }

    /**
     * Gets the value of the reportDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getReportDt() {
        return reportDt;
    }

    /**
     * Sets the value of the reportDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setReportDt(GLogDateTimeType value) {
        this.reportDt = value;
    }

    /**
     * Gets the value of the incidentDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIncidentDt() {
        return incidentDt;
    }

    /**
     * Sets the value of the incidentDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIncidentDt(GLogDateTimeType value) {
        this.incidentDt = value;
    }

    /**
     * Gets the value of the damageNotifyPointGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageNotifyPointGid() {
        return damageNotifyPointGid;
    }

    /**
     * Sets the value of the damageNotifyPointGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageNotifyPointGid(GLogXMLGidType value) {
        this.damageNotifyPointGid = value;
    }

    /**
     * Gets the value of the damageCauseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageCauseGid() {
        return damageCauseGid;
    }

    /**
     * Sets the value of the damageCauseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageCauseGid(GLogXMLGidType value) {
        this.damageCauseGid = value;
    }

    /**
     * Gets the value of the damageNatureGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDamageNatureGid() {
        return damageNatureGid;
    }

    /**
     * Sets the value of the damageNatureGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDamageNatureGid(GLogXMLGidType value) {
        this.damageNatureGid = value;
    }

    /**
     * Gets the value of the damageValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDamageValue() {
        return damageValue;
    }

    /**
     * Sets the value of the damageValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDamageValue(GLogXMLFinancialAmountType value) {
        this.damageValue = value;
    }

    /**
     * Gets the value of the isDamageValueFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDamageValueFixed() {
        return isDamageValueFixed;
    }

    /**
     * Sets the value of the isDamageValueFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDamageValueFixed(String value) {
        this.isDamageValueFixed = value;
    }

    /**
     * Gets the value of the sizeOfLoss property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getSizeOfLoss() {
        return sizeOfLoss;
    }

    /**
     * Sets the value of the sizeOfLoss property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setSizeOfLoss(GLogXMLFinancialAmountType value) {
        this.sizeOfLoss = value;
    }

    /**
     * Gets the value of the isSizeOfLossFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSizeOfLossFixed() {
        return isSizeOfLossFixed;
    }

    /**
     * Sets the value of the isSizeOfLossFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSizeOfLossFixed(String value) {
        this.isSizeOfLossFixed = value;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the notificationDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getNotificationDt() {
        return notificationDt;
    }

    /**
     * Sets the value of the notificationDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setNotificationDt(GLogDateTimeType value) {
        this.notificationDt = value;
    }

    /**
     * Gets the value of the notifyPartyContact property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getNotifyPartyContact() {
        return notifyPartyContact;
    }

    /**
     * Sets the value of the notifyPartyContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setNotifyPartyContact(GLogXMLContactRefType value) {
        this.notifyPartyContact = value;
    }

    /**
     * Gets the value of the liablePartyContact property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getLiablePartyContact() {
        return liablePartyContact;
    }

    /**
     * Sets the value of the liablePartyContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setLiablePartyContact(GLogXMLContactRefType value) {
        this.liablePartyContact = value;
    }

    /**
     * Gets the value of the isSelfRecondition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSelfRecondition() {
        return isSelfRecondition;
    }

    /**
     * Sets the value of the isSelfRecondition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSelfRecondition(String value) {
        this.isSelfRecondition = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the claimCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the claimCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimCostType }
     * 
     * 
     */
    public List<ClaimCostType> getClaimCost() {
        if (claimCost == null) {
            claimCost = new ArrayList<ClaimCostType>();
        }
        return this.claimCost;
    }

    /**
     * Gets the value of the claimLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the claimLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimLineItemType }
     * 
     * 
     */
    public List<ClaimLineItemType> getClaimLineItem() {
        if (claimLineItem == null) {
            claimLineItem = new ArrayList<ClaimLineItemType>();
        }
        return this.claimLineItem;
    }

    /**
     * Gets the value of the claimNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the claimNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClaimNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClaimNoteType }
     * 
     * 
     */
    public List<ClaimNoteType> getClaimNote() {
        if (claimNote == null) {
            claimNote = new ArrayList<ClaimNoteType>();
        }
        return this.claimNote;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the flexFieldCurrencies property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Sets the value of the flexFieldCurrencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             (Outbound) Contains details of an Item Origin's qualification for a Trade Agreement.
 *         
 * 
 * <p>Java class for ItemQualificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ItemQualificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ItemQualificationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TradeAgreement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IsQualified" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QualPrefCriteriaGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="QualRvcMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TradeProgram" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Producer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="QualificationStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ItemQualificationRefnums" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ItemQualificationRemarks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ItemQualificationValues" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GtmValueType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ItemQualificationRequiredDocuments" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemQualificationRequiredDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ItemQualificationType", propOrder = {
    "itemQualificationGid",
    "description",
    "tradeAgreement",
    "gtmProdClassTypeGid",
    "classificationCode",
    "isQualified",
    "qualPrefCriteriaGid",
    "qualRvcMethodGid",
    "tradeProgram",
    "producer",
    "qualificationStatus",
    "effectiveDate",
    "expirationDate",
    "itemQualificationRefnums",
    "itemQualificationRemarks",
    "itemQualificationValues",
    "itemQualificationRequiredDocuments",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class ItemQualificationType {

    @XmlElement(name = "ItemQualificationGid", required = true)
    protected GLogXMLGidType itemQualificationGid;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "TradeAgreement")
    protected String tradeAgreement;
    @XmlElement(name = "GtmProdClassTypeGid", required = true)
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "ClassificationCode", required = true)
    protected String classificationCode;
    @XmlElement(name = "IsQualified")
    protected String isQualified;
    @XmlElement(name = "QualPrefCriteriaGid")
    protected GLogXMLGidType qualPrefCriteriaGid;
    @XmlElement(name = "QualRvcMethodGid")
    protected GLogXMLGidType qualRvcMethodGid;
    @XmlElement(name = "TradeProgram", required = true)
    protected String tradeProgram;
    @XmlElement(name = "Producer")
    protected String producer;
    @XmlElement(name = "QualificationStatus", required = true)
    protected String qualificationStatus;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "ItemQualificationRefnums")
    protected List<RefnumType> itemQualificationRefnums;
    @XmlElement(name = "ItemQualificationRemarks")
    protected List<RemarkType> itemQualificationRemarks;
    @XmlElement(name = "ItemQualificationValues")
    protected List<GtmValueType> itemQualificationValues;
    @XmlElement(name = "ItemQualificationRequiredDocuments")
    protected List<ItemQualificationRequiredDocumentType> itemQualificationRequiredDocuments;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Gets the value of the itemQualificationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemQualificationGid() {
        return itemQualificationGid;
    }

    /**
     * Sets the value of the itemQualificationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemQualificationGid(GLogXMLGidType value) {
        this.itemQualificationGid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the tradeAgreement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeAgreement() {
        return tradeAgreement;
    }

    /**
     * Sets the value of the tradeAgreement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeAgreement(String value) {
        this.tradeAgreement = value;
    }

    /**
     * Gets the value of the gtmProdClassTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Sets the value of the gtmProdClassTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Gets the value of the classificationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Sets the value of the classificationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Gets the value of the isQualified property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsQualified() {
        return isQualified;
    }

    /**
     * Sets the value of the isQualified property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsQualified(String value) {
        this.isQualified = value;
    }

    /**
     * Gets the value of the qualPrefCriteriaGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQualPrefCriteriaGid() {
        return qualPrefCriteriaGid;
    }

    /**
     * Sets the value of the qualPrefCriteriaGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQualPrefCriteriaGid(GLogXMLGidType value) {
        this.qualPrefCriteriaGid = value;
    }

    /**
     * Gets the value of the qualRvcMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQualRvcMethodGid() {
        return qualRvcMethodGid;
    }

    /**
     * Sets the value of the qualRvcMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQualRvcMethodGid(GLogXMLGidType value) {
        this.qualRvcMethodGid = value;
    }

    /**
     * Gets the value of the tradeProgram property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeProgram() {
        return tradeProgram;
    }

    /**
     * Sets the value of the tradeProgram property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeProgram(String value) {
        this.tradeProgram = value;
    }

    /**
     * Gets the value of the producer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducer() {
        return producer;
    }

    /**
     * Sets the value of the producer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducer(String value) {
        this.producer = value;
    }

    /**
     * Gets the value of the qualificationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualificationStatus() {
        return qualificationStatus;
    }

    /**
     * Sets the value of the qualificationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualificationStatus(String value) {
        this.qualificationStatus = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the itemQualificationRefnums property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemQualificationRefnums property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemQualificationRefnums().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getItemQualificationRefnums() {
        if (itemQualificationRefnums == null) {
            itemQualificationRefnums = new ArrayList<RefnumType>();
        }
        return this.itemQualificationRefnums;
    }

    /**
     * Gets the value of the itemQualificationRemarks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemQualificationRemarks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemQualificationRemarks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getItemQualificationRemarks() {
        if (itemQualificationRemarks == null) {
            itemQualificationRemarks = new ArrayList<RemarkType>();
        }
        return this.itemQualificationRemarks;
    }

    /**
     * Gets the value of the itemQualificationValues property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemQualificationValues property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemQualificationValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmValueType }
     * 
     * 
     */
    public List<GtmValueType> getItemQualificationValues() {
        if (itemQualificationValues == null) {
            itemQualificationValues = new ArrayList<GtmValueType>();
        }
        return this.itemQualificationValues;
    }

    /**
     * Gets the value of the itemQualificationRequiredDocuments property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemQualificationRequiredDocuments property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemQualificationRequiredDocuments().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemQualificationRequiredDocumentType }
     * 
     * 
     */
    public List<ItemQualificationRequiredDocumentType> getItemQualificationRequiredDocuments() {
        if (itemQualificationRequiredDocuments == null) {
            itemQualificationRequiredDocuments = new ArrayList<ItemQualificationRequiredDocumentType>();
        }
        return this.itemQualificationRequiredDocuments;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}

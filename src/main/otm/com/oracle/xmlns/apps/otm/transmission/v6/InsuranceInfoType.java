
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the insurance information.
 * 
 * <p>Java class for InsuranceInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InsuranceInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InsurancePolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InsuranceAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="InsurancePremium" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="BillOfLadingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BillOfLadingIssuanceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InsuranceInfoType", propOrder = {
    "insurancePolicyNumber",
    "insuranceAmount",
    "insurancePremium",
    "billOfLadingType",
    "billOfLadingIssuanceType"
})
public class InsuranceInfoType {

    @XmlElement(name = "InsurancePolicyNumber")
    protected String insurancePolicyNumber;
    @XmlElement(name = "InsuranceAmount")
    protected GLogXMLFinancialAmountType insuranceAmount;
    @XmlElement(name = "InsurancePremium")
    protected GLogXMLFinancialAmountType insurancePremium;
    @XmlElement(name = "BillOfLadingType")
    protected String billOfLadingType;
    @XmlElement(name = "BillOfLadingIssuanceType")
    protected String billOfLadingIssuanceType;

    /**
     * Gets the value of the insurancePolicyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurancePolicyNumber() {
        return insurancePolicyNumber;
    }

    /**
     * Sets the value of the insurancePolicyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurancePolicyNumber(String value) {
        this.insurancePolicyNumber = value;
    }

    /**
     * Gets the value of the insuranceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getInsuranceAmount() {
        return insuranceAmount;
    }

    /**
     * Sets the value of the insuranceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setInsuranceAmount(GLogXMLFinancialAmountType value) {
        this.insuranceAmount = value;
    }

    /**
     * Gets the value of the insurancePremium property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getInsurancePremium() {
        return insurancePremium;
    }

    /**
     * Sets the value of the insurancePremium property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setInsurancePremium(GLogXMLFinancialAmountType value) {
        this.insurancePremium = value;
    }

    /**
     * Gets the value of the billOfLadingType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillOfLadingType() {
        return billOfLadingType;
    }

    /**
     * Sets the value of the billOfLadingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillOfLadingType(String value) {
        this.billOfLadingType = value;
    }

    /**
     * Gets the value of the billOfLadingIssuanceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBillOfLadingIssuanceType() {
        return billOfLadingIssuanceType;
    }

    /**
     * Sets the value of the billOfLadingIssuanceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBillOfLadingIssuanceType(String value) {
        this.billOfLadingIssuanceType = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the Booking Line Amendment Changes for a ContainerGroup.
 * 
 * <p>Java class for BookLineAmendDtlContGroupType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookLineAmendDtlContGroupType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChangeAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ContainerGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="BookLineAmendDtlSEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookLineAmendDtlSEquipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookLineAmendDtlContGroupType", propOrder = {
    "changeAction",
    "containerGroupGid",
    "weightVolume",
    "bookLineAmendDtlSEquipment"
})
public class BookLineAmendDtlContGroupType {

    @XmlElement(name = "ChangeAction")
    protected String changeAction;
    @XmlElement(name = "ContainerGroupGid")
    protected GLogXMLGidType containerGroupGid;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "BookLineAmendDtlSEquipment")
    protected List<BookLineAmendDtlSEquipmentType> bookLineAmendDtlSEquipment;

    /**
     * Gets the value of the changeAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeAction() {
        return changeAction;
    }

    /**
     * Sets the value of the changeAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeAction(String value) {
        this.changeAction = value;
    }

    /**
     * Gets the value of the containerGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContainerGroupGid() {
        return containerGroupGid;
    }

    /**
     * Sets the value of the containerGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContainerGroupGid(GLogXMLGidType value) {
        this.containerGroupGid = value;
    }

    /**
     * Gets the value of the weightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Sets the value of the weightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Gets the value of the bookLineAmendDtlSEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bookLineAmendDtlSEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookLineAmendDtlSEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookLineAmendDtlSEquipmentType }
     * 
     * 
     */
    public List<BookLineAmendDtlSEquipmentType> getBookLineAmendDtlSEquipment() {
        if (bookLineAmendDtlSEquipment == null) {
            bookLineAmendDtlSEquipment = new ArrayList<BookLineAmendDtlSEquipmentType>();
        }
        return this.bookLineAmendDtlSEquipment;
    }

}

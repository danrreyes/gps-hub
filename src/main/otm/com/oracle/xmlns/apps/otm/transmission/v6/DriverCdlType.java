
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Defines Driver's Commercial driver license information.
 * 
 * <p>Java class for DriverCdlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverCdlType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="CdlClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CdlNum" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CdlExpDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="CdlIssueCountryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CdlIssueState" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverCdlType", propOrder = {
    "transactionCode",
    "cdlClass",
    "cdlNum",
    "cdlExpDate",
    "cdlIssueCountryGid",
    "cdlIssueState"
})
public class DriverCdlType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "CdlClass")
    protected String cdlClass;
    @XmlElement(name = "CdlNum", required = true)
    protected String cdlNum;
    @XmlElement(name = "CdlExpDate", required = true)
    protected GLogDateTimeType cdlExpDate;
    @XmlElement(name = "CdlIssueCountryGid")
    protected GLogXMLGidType cdlIssueCountryGid;
    @XmlElement(name = "CdlIssueState", required = true)
    protected String cdlIssueState;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the cdlClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdlClass() {
        return cdlClass;
    }

    /**
     * Sets the value of the cdlClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdlClass(String value) {
        this.cdlClass = value;
    }

    /**
     * Gets the value of the cdlNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdlNum() {
        return cdlNum;
    }

    /**
     * Sets the value of the cdlNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdlNum(String value) {
        this.cdlNum = value;
    }

    /**
     * Gets the value of the cdlExpDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getCdlExpDate() {
        return cdlExpDate;
    }

    /**
     * Sets the value of the cdlExpDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setCdlExpDate(GLogDateTimeType value) {
        this.cdlExpDate = value;
    }

    /**
     * Gets the value of the cdlIssueCountryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCdlIssueCountryGid() {
        return cdlIssueCountryGid;
    }

    /**
     * Sets the value of the cdlIssueCountryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCdlIssueCountryGid(GLogXMLGidType value) {
        this.cdlIssueCountryGid = value;
    }

    /**
     * Gets the value of the cdlIssueState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCdlIssueState() {
        return cdlIssueState;
    }

    /**
     * Sets the value of the cdlIssueState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCdlIssueState(String value) {
        this.cdlIssueState = value;
    }

}

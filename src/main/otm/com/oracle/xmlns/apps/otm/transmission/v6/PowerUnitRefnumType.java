
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             PowerUnitRefnum is a qualifier/value pair used to refer to a power unit.
 *          
 * 
 * <p>Java class for PowerUnitRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PowerUnitRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PURefnumQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PURefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PowerUnitRefnumType", propOrder = {
    "puRefnumQualGid",
    "puRefnumValue"
})
public class PowerUnitRefnumType {

    @XmlElement(name = "PURefnumQualGid", required = true)
    protected GLogXMLGidType puRefnumQualGid;
    @XmlElement(name = "PURefnumValue", required = true)
    protected String puRefnumValue;

    /**
     * Gets the value of the puRefnumQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPURefnumQualGid() {
        return puRefnumQualGid;
    }

    /**
     * Sets the value of the puRefnumQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPURefnumQualGid(GLogXMLGidType value) {
        this.puRefnumQualGid = value;
    }

    /**
     * Gets the value of the puRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPURefnumValue() {
        return puRefnumValue;
    }

    /**
     * Sets the value of the puRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPURefnumValue(String value) {
        this.puRefnumValue = value;
    }

}

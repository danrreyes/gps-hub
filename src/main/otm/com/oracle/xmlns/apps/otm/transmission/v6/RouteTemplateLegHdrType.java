
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The RouteTemplateLegHdr contains general information about the route template leg.
 *             Note: When creating the RouteTemplateLeg, the StartRegionGid and EndRegionGid are required.
 *          
 * 
 * <p>Java class for RouteTemplateLegHdrType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RouteTemplateLegHdrType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StartRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StartDistThreshold" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="EndRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EndDistThreshold" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="TimeWindowSec" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowSecType" minOccurs="0"/&gt;
 *         &lt;element name="TimePhasedPlanThreshold" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="MinTimeToNextLeg" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="MaxTimeToNextLeg" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="MatchShipToLeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WarnForMissingLeg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DomainProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RouteTemplateLegHdrType", propOrder = {
    "sequenceNumber",
    "equipmentGroupProfileGid",
    "startRegionGid",
    "startDistThreshold",
    "endRegionGid",
    "endDistThreshold",
    "timeWindowSec",
    "timePhasedPlanThreshold",
    "minTimeToNextLeg",
    "maxTimeToNextLeg",
    "matchShipToLeg",
    "warnForMissingLeg",
    "domainProfileGid"
})
public class RouteTemplateLegHdrType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "StartRegionGid")
    protected GLogXMLGidType startRegionGid;
    @XmlElement(name = "StartDistThreshold")
    protected GLogXMLDistanceType startDistThreshold;
    @XmlElement(name = "EndRegionGid")
    protected GLogXMLGidType endRegionGid;
    @XmlElement(name = "EndDistThreshold")
    protected GLogXMLDistanceType endDistThreshold;
    @XmlElement(name = "TimeWindowSec")
    protected TimeWindowSecType timeWindowSec;
    @XmlElement(name = "TimePhasedPlanThreshold")
    protected GLogXMLDurationType timePhasedPlanThreshold;
    @XmlElement(name = "MinTimeToNextLeg")
    protected GLogXMLDurationType minTimeToNextLeg;
    @XmlElement(name = "MaxTimeToNextLeg")
    protected GLogXMLDurationType maxTimeToNextLeg;
    @XmlElement(name = "MatchShipToLeg")
    protected String matchShipToLeg;
    @XmlElement(name = "WarnForMissingLeg")
    protected String warnForMissingLeg;
    @XmlElement(name = "DomainProfileGid")
    protected GLogXMLGidType domainProfileGid;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the startRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartRegionGid() {
        return startRegionGid;
    }

    /**
     * Sets the value of the startRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartRegionGid(GLogXMLGidType value) {
        this.startRegionGid = value;
    }

    /**
     * Gets the value of the startDistThreshold property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getStartDistThreshold() {
        return startDistThreshold;
    }

    /**
     * Sets the value of the startDistThreshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setStartDistThreshold(GLogXMLDistanceType value) {
        this.startDistThreshold = value;
    }

    /**
     * Gets the value of the endRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndRegionGid() {
        return endRegionGid;
    }

    /**
     * Sets the value of the endRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndRegionGid(GLogXMLGidType value) {
        this.endRegionGid = value;
    }

    /**
     * Gets the value of the endDistThreshold property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getEndDistThreshold() {
        return endDistThreshold;
    }

    /**
     * Sets the value of the endDistThreshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setEndDistThreshold(GLogXMLDistanceType value) {
        this.endDistThreshold = value;
    }

    /**
     * Gets the value of the timeWindowSec property.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowSecType }
     *     
     */
    public TimeWindowSecType getTimeWindowSec() {
        return timeWindowSec;
    }

    /**
     * Sets the value of the timeWindowSec property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowSecType }
     *     
     */
    public void setTimeWindowSec(TimeWindowSecType value) {
        this.timeWindowSec = value;
    }

    /**
     * Gets the value of the timePhasedPlanThreshold property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTimePhasedPlanThreshold() {
        return timePhasedPlanThreshold;
    }

    /**
     * Sets the value of the timePhasedPlanThreshold property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTimePhasedPlanThreshold(GLogXMLDurationType value) {
        this.timePhasedPlanThreshold = value;
    }

    /**
     * Gets the value of the minTimeToNextLeg property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMinTimeToNextLeg() {
        return minTimeToNextLeg;
    }

    /**
     * Sets the value of the minTimeToNextLeg property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMinTimeToNextLeg(GLogXMLDurationType value) {
        this.minTimeToNextLeg = value;
    }

    /**
     * Gets the value of the maxTimeToNextLeg property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getMaxTimeToNextLeg() {
        return maxTimeToNextLeg;
    }

    /**
     * Sets the value of the maxTimeToNextLeg property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setMaxTimeToNextLeg(GLogXMLDurationType value) {
        this.maxTimeToNextLeg = value;
    }

    /**
     * Gets the value of the matchShipToLeg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchShipToLeg() {
        return matchShipToLeg;
    }

    /**
     * Sets the value of the matchShipToLeg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchShipToLeg(String value) {
        this.matchShipToLeg = value;
    }

    /**
     * Gets the value of the warnForMissingLeg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarnForMissingLeg() {
        return warnForMissingLeg;
    }

    /**
     * Sets the value of the warnForMissingLeg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarnForMissingLeg(String value) {
        this.warnForMissingLeg = value;
    }

    /**
     * Gets the value of the domainProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDomainProfileGid() {
        return domainProfileGid;
    }

    /**
     * Sets the value of the domainProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDomainProfileGid(GLogXMLGidType value) {
        this.domainProfileGid = value;
    }

}

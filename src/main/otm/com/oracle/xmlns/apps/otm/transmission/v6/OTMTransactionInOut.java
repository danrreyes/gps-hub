
package com.oracle.xmlns.apps.otm.transmission.v6;

import com.oracle.xmlns.apps.gtm.transmission.v6.GtmBondType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmContactType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmDeclarationMessageType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmDeclarationType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmRegistrationType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmTransactionLineType;
import com.oracle.xmlns.apps.gtm.transmission.v6.GtmTransactionType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlSeeAlso;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OTMTransactionInOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OTMTransactionInOut"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLTransactionType"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OTMTransactionInOut")
@XmlSeeAlso({
    TransmissionReportType.class,
    LocationType.class,
    ContactType.class,
    CorporationType.class,
    ActivityTimeDefType.class,
    PartySiteType.class,
    ItemMasterType.class,
    ItemType.class,
    HazmatItemType.class,
    HazmatGenericType.class,
    SkuType.class,
    SkuTransactionType.class,
    SkuEventType.class,
    PartnerItemType.class,
    XLaneType.class,
    CSVDataLoadType.class,
    TransOrderType.class,
    ReleaseType.class,
    OrderMovementType.class,
    QuoteType.class,
    DocumentType.class,
    ShipmentStatusType.class,
    EquipmentType.class,
    ShipmentGroupType.class,
    ShipmentGroupTenderOfferType.class,
    CharterVoyageType.class,
    ConsolType.class,
    DriverType.class,
    PowerUnitType.class,
    DeviceType.class,
    InvoiceType.class,
    ClaimType.class,
    ExchangeRateType.class,
    WorkInvoiceType.class,
    JobType.class,
    GtmContactType.class,
    GtmRegistrationType.class,
    GtmTransactionType.class,
    GtmTransactionLineType.class,
    GtmDeclarationType.class,
    GtmDeclarationMessageType.class,
    GtmBondType.class
})
public abstract class OTMTransactionInOut
    extends GLogXMLTransactionType
{


}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Common type for tables which contain Flex Field columns for CURRENCY data type
 *             Note: Some tables may not have all the available columns. These types just 
 *             define the maximum number of allowed elements.
 *          
 * 
 * <p>Java class for FlexFieldCurrencyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexFieldCurrencyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AttributeCurrency1" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency2" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency3" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency4" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency5" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency6" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency7" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency8" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency9" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeCurrency10" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexFieldCurrencyType", propOrder = {
    "attributeCurrency1",
    "attributeCurrency2",
    "attributeCurrency3",
    "attributeCurrency4",
    "attributeCurrency5",
    "attributeCurrency6",
    "attributeCurrency7",
    "attributeCurrency8",
    "attributeCurrency9",
    "attributeCurrency10"
})
public class FlexFieldCurrencyType {

    @XmlElement(name = "AttributeCurrency1")
    protected GLogXMLFinancialAmountType attributeCurrency1;
    @XmlElement(name = "AttributeCurrency2")
    protected GLogXMLFinancialAmountType attributeCurrency2;
    @XmlElement(name = "AttributeCurrency3")
    protected GLogXMLFinancialAmountType attributeCurrency3;
    @XmlElement(name = "AttributeCurrency4")
    protected GLogXMLFinancialAmountType attributeCurrency4;
    @XmlElement(name = "AttributeCurrency5")
    protected GLogXMLFinancialAmountType attributeCurrency5;
    @XmlElement(name = "AttributeCurrency6")
    protected GLogXMLFinancialAmountType attributeCurrency6;
    @XmlElement(name = "AttributeCurrency7")
    protected GLogXMLFinancialAmountType attributeCurrency7;
    @XmlElement(name = "AttributeCurrency8")
    protected GLogXMLFinancialAmountType attributeCurrency8;
    @XmlElement(name = "AttributeCurrency9")
    protected GLogXMLFinancialAmountType attributeCurrency9;
    @XmlElement(name = "AttributeCurrency10")
    protected GLogXMLFinancialAmountType attributeCurrency10;

    /**
     * Gets the value of the attributeCurrency1 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency1() {
        return attributeCurrency1;
    }

    /**
     * Sets the value of the attributeCurrency1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency1(GLogXMLFinancialAmountType value) {
        this.attributeCurrency1 = value;
    }

    /**
     * Gets the value of the attributeCurrency2 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency2() {
        return attributeCurrency2;
    }

    /**
     * Sets the value of the attributeCurrency2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency2(GLogXMLFinancialAmountType value) {
        this.attributeCurrency2 = value;
    }

    /**
     * Gets the value of the attributeCurrency3 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency3() {
        return attributeCurrency3;
    }

    /**
     * Sets the value of the attributeCurrency3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency3(GLogXMLFinancialAmountType value) {
        this.attributeCurrency3 = value;
    }

    /**
     * Gets the value of the attributeCurrency4 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency4() {
        return attributeCurrency4;
    }

    /**
     * Sets the value of the attributeCurrency4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency4(GLogXMLFinancialAmountType value) {
        this.attributeCurrency4 = value;
    }

    /**
     * Gets the value of the attributeCurrency5 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency5() {
        return attributeCurrency5;
    }

    /**
     * Sets the value of the attributeCurrency5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency5(GLogXMLFinancialAmountType value) {
        this.attributeCurrency5 = value;
    }

    /**
     * Gets the value of the attributeCurrency6 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency6() {
        return attributeCurrency6;
    }

    /**
     * Sets the value of the attributeCurrency6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency6(GLogXMLFinancialAmountType value) {
        this.attributeCurrency6 = value;
    }

    /**
     * Gets the value of the attributeCurrency7 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency7() {
        return attributeCurrency7;
    }

    /**
     * Sets the value of the attributeCurrency7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency7(GLogXMLFinancialAmountType value) {
        this.attributeCurrency7 = value;
    }

    /**
     * Gets the value of the attributeCurrency8 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency8() {
        return attributeCurrency8;
    }

    /**
     * Sets the value of the attributeCurrency8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency8(GLogXMLFinancialAmountType value) {
        this.attributeCurrency8 = value;
    }

    /**
     * Gets the value of the attributeCurrency9 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency9() {
        return attributeCurrency9;
    }

    /**
     * Sets the value of the attributeCurrency9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency9(GLogXMLFinancialAmountType value) {
        this.attributeCurrency9 = value;
    }

    /**
     * Gets the value of the attributeCurrency10 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getAttributeCurrency10() {
        return attributeCurrency10;
    }

    /**
     * Sets the value of the attributeCurrency10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setAttributeCurrency10(GLogXMLFinancialAmountType value) {
        this.attributeCurrency10 = value;
    }

}

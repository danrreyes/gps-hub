
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) InvoiceRefnum is an alternative identifier for an invoice or bill.
 *             It consists of an InvoiceRefnumQualifierGid and an InvoiceRefnumValue.
 *          
 * 
 * <p>Java class for InvoiceRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvoiceRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InvoiceRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="InvoiceRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IssueDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvoiceRefnumType", propOrder = {
    "invoiceRefnumQualifierGid",
    "invoiceRefnumValue",
    "issueDt"
})
public class InvoiceRefnumType {

    @XmlElement(name = "InvoiceRefnumQualifierGid", required = true)
    protected GLogXMLGidType invoiceRefnumQualifierGid;
    @XmlElement(name = "InvoiceRefnumValue", required = true)
    protected String invoiceRefnumValue;
    @XmlElement(name = "IssueDt")
    protected GLogDateTimeType issueDt;

    /**
     * Gets the value of the invoiceRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceRefnumQualifierGid() {
        return invoiceRefnumQualifierGid;
    }

    /**
     * Sets the value of the invoiceRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceRefnumQualifierGid(GLogXMLGidType value) {
        this.invoiceRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the invoiceRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoiceRefnumValue() {
        return invoiceRefnumValue;
    }

    /**
     * Sets the value of the invoiceRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoiceRefnumValue(String value) {
        this.invoiceRefnumValue = value;
    }

    /**
     * Gets the value of the issueDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getIssueDt() {
        return issueDt;
    }

    /**
     * Sets the value of the issueDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setIssueDt(GLogDateTimeType value) {
        this.issueDt = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlightInstanceRowType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlightInstanceRowType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FLIGHT_INSTANCE_ID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LEAVE_TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ARRIVAL_TIMESTAMP" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SRC_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ELAPSED_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FLIGHT_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ELAPSED_TIME_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ELAPSED_TIME_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FLIGHT" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlightType" minOccurs="0"/&gt;
 *         &lt;element name="INTERIM_FLIGHT_INSTANCE" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InterimFlightInstanceType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightInstanceRowType", propOrder = {
    "flightinstanceid",
    "leavetimestamp",
    "arrivaltimestamp",
    "srclocationgid",
    "destlocationgid",
    "elapsedtime",
    "domainname",
    "flightgid",
    "elapsedtimeuomcode",
    "elapsedtimebase",
    "insertdate",
    "updatedate",
    "insertuser",
    "updateuser",
    "flight",
    "interimflightinstance"
})
public class FlightInstanceRowType {

    @XmlElement(name = "FLIGHT_INSTANCE_ID", required = true)
    protected String flightinstanceid;
    @XmlElement(name = "LEAVE_TIMESTAMP", required = true)
    protected String leavetimestamp;
    @XmlElement(name = "ARRIVAL_TIMESTAMP", required = true)
    protected String arrivaltimestamp;
    @XmlElement(name = "SRC_LOCATION_GID", required = true)
    protected String srclocationgid;
    @XmlElement(name = "DEST_LOCATION_GID", required = true)
    protected String destlocationgid;
    @XmlElement(name = "ELAPSED_TIME")
    protected String elapsedtime;
    @XmlElement(name = "DOMAIN_NAME", required = true)
    protected String domainname;
    @XmlElement(name = "FLIGHT_GID", required = true)
    protected String flightgid;
    @XmlElement(name = "ELAPSED_TIME_UOM_CODE")
    protected String elapsedtimeuomcode;
    @XmlElement(name = "ELAPSED_TIME_BASE")
    protected String elapsedtimebase;
    @XmlElement(name = "INSERT_DATE")
    protected String insertdate;
    @XmlElement(name = "UPDATE_DATE")
    protected String updatedate;
    @XmlElement(name = "INSERT_USER")
    protected String insertuser;
    @XmlElement(name = "UPDATE_USER")
    protected String updateuser;
    @XmlElement(name = "FLIGHT")
    protected FlightType flight;
    @XmlElement(name = "INTERIM_FLIGHT_INSTANCE")
    protected InterimFlightInstanceType interimflightinstance;
    @XmlAttribute(name = "num")
    protected String num;

    /**
     * Gets the value of the flightinstanceid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTINSTANCEID() {
        return flightinstanceid;
    }

    /**
     * Sets the value of the flightinstanceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTINSTANCEID(String value) {
        this.flightinstanceid = value;
    }

    /**
     * Gets the value of the leavetimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLEAVETIMESTAMP() {
        return leavetimestamp;
    }

    /**
     * Sets the value of the leavetimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLEAVETIMESTAMP(String value) {
        this.leavetimestamp = value;
    }

    /**
     * Gets the value of the arrivaltimestamp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARRIVALTIMESTAMP() {
        return arrivaltimestamp;
    }

    /**
     * Sets the value of the arrivaltimestamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARRIVALTIMESTAMP(String value) {
        this.arrivaltimestamp = value;
    }

    /**
     * Gets the value of the srclocationgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCLOCATIONGID() {
        return srclocationgid;
    }

    /**
     * Sets the value of the srclocationgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCLOCATIONGID(String value) {
        this.srclocationgid = value;
    }

    /**
     * Gets the value of the destlocationgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTLOCATIONGID() {
        return destlocationgid;
    }

    /**
     * Sets the value of the destlocationgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTLOCATIONGID(String value) {
        this.destlocationgid = value;
    }

    /**
     * Gets the value of the elapsedtime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIME() {
        return elapsedtime;
    }

    /**
     * Sets the value of the elapsedtime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIME(String value) {
        this.elapsedtime = value;
    }

    /**
     * Gets the value of the domainname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMAINNAME() {
        return domainname;
    }

    /**
     * Sets the value of the domainname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMAINNAME(String value) {
        this.domainname = value;
    }

    /**
     * Gets the value of the flightgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTGID() {
        return flightgid;
    }

    /**
     * Sets the value of the flightgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTGID(String value) {
        this.flightgid = value;
    }

    /**
     * Gets the value of the elapsedtimeuomcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIMEUOMCODE() {
        return elapsedtimeuomcode;
    }

    /**
     * Sets the value of the elapsedtimeuomcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIMEUOMCODE(String value) {
        this.elapsedtimeuomcode = value;
    }

    /**
     * Gets the value of the elapsedtimebase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIMEBASE() {
        return elapsedtimebase;
    }

    /**
     * Sets the value of the elapsedtimebase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIMEBASE(String value) {
        this.elapsedtimebase = value;
    }

    /**
     * Gets the value of the insertdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTDATE() {
        return insertdate;
    }

    /**
     * Sets the value of the insertdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTDATE(String value) {
        this.insertdate = value;
    }

    /**
     * Gets the value of the updatedate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEDATE() {
        return updatedate;
    }

    /**
     * Sets the value of the updatedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEDATE(String value) {
        this.updatedate = value;
    }

    /**
     * Gets the value of the insertuser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTUSER() {
        return insertuser;
    }

    /**
     * Sets the value of the insertuser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTUSER(String value) {
        this.insertuser = value;
    }

    /**
     * Gets the value of the updateuser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEUSER() {
        return updateuser;
    }

    /**
     * Sets the value of the updateuser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEUSER(String value) {
        this.updateuser = value;
    }

    /**
     * Gets the value of the flight property.
     * 
     * @return
     *     possible object is
     *     {@link FlightType }
     *     
     */
    public FlightType getFLIGHT() {
        return flight;
    }

    /**
     * Sets the value of the flight property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlightType }
     *     
     */
    public void setFLIGHT(FlightType value) {
        this.flight = value;
    }

    /**
     * Gets the value of the interimflightinstance property.
     * 
     * @return
     *     possible object is
     *     {@link InterimFlightInstanceType }
     *     
     */
    public InterimFlightInstanceType getINTERIMFLIGHTINSTANCE() {
        return interimflightinstance;
    }

    /**
     * Sets the value of the interimflightinstance property.
     * 
     * @param value
     *     allowed object is
     *     {@link InterimFlightInstanceType }
     *     
     */
    public void setINTERIMFLIGHTINSTANCE(InterimFlightInstanceType value) {
        this.interimflightinstance = value;
    }

    /**
     * Gets the value of the num property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Sets the value of the num property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

}

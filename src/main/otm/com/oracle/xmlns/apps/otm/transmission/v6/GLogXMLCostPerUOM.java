
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLCostPerUOM complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLCostPerUOM"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CostPerWeightVolumeUOM" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostPerWeightVolumeUOMType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLCostPerUOM", propOrder = {
    "costPerWeightVolumeUOM"
})
public class GLogXMLCostPerUOM {

    @XmlElement(name = "CostPerWeightVolumeUOM", required = true)
    protected CostPerWeightVolumeUOMType costPerWeightVolumeUOM;

    /**
     * Gets the value of the costPerWeightVolumeUOM property.
     * 
     * @return
     *     possible object is
     *     {@link CostPerWeightVolumeUOMType }
     *     
     */
    public CostPerWeightVolumeUOMType getCostPerWeightVolumeUOM() {
        return costPerWeightVolumeUOM;
    }

    /**
     * Sets the value of the costPerWeightVolumeUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link CostPerWeightVolumeUOMType }
     *     
     */
    public void setCostPerWeightVolumeUOM(CostPerWeightVolumeUOMType value) {
        this.costPerWeightVolumeUOM = value;
    }

}

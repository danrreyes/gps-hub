
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * GenericDetail provides a generic detail format to include data specific to motor, ocean, or rail carriers.
 * 
 * <p>Java class for GenericDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PickupDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="VesselInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VesselInfoType" minOccurs="0"/&gt;
 *         &lt;element name="InvoiceServiceCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Port" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PortType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="LetterofCredit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LetterOfCreditType" minOccurs="0"/&gt;
 *         &lt;element name="DestinationStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType" minOccurs="0"/&gt;
 *         &lt;element name="OriginStation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLStationType" minOccurs="0"/&gt;
 *         &lt;element name="ProtectiveSvc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProtectiveSvcType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GenericStopOff" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StopOffType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GenericEquipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericEquipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GenericLineItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericLineItemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericDetailType", propOrder = {
    "pickupDate",
    "deliveryDate",
    "vesselInfo",
    "invoiceServiceCodeGid",
    "port",
    "letterofCredit",
    "destinationStation",
    "originStation",
    "protectiveSvc",
    "genericStopOff",
    "genericEquipment",
    "genericLineItem"
})
public class GenericDetailType {

    @XmlElement(name = "PickupDate")
    protected GLogDateTimeType pickupDate;
    @XmlElement(name = "DeliveryDate")
    protected GLogDateTimeType deliveryDate;
    @XmlElement(name = "VesselInfo")
    protected VesselInfoType vesselInfo;
    @XmlElement(name = "InvoiceServiceCodeGid")
    protected GLogXMLGidType invoiceServiceCodeGid;
    @XmlElement(name = "Port")
    protected List<PortType> port;
    @XmlElement(name = "LetterofCredit")
    protected LetterOfCreditType letterofCredit;
    @XmlElement(name = "DestinationStation")
    protected GLogXMLStationType destinationStation;
    @XmlElement(name = "OriginStation")
    protected GLogXMLStationType originStation;
    @XmlElement(name = "ProtectiveSvc")
    protected List<ProtectiveSvcType> protectiveSvc;
    @XmlElement(name = "GenericStopOff")
    protected List<StopOffType> genericStopOff;
    @XmlElement(name = "GenericEquipment")
    protected List<GenericEquipmentType> genericEquipment;
    @XmlElement(name = "GenericLineItem")
    protected List<GenericLineItemType> genericLineItem;

    /**
     * Gets the value of the pickupDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPickupDate() {
        return pickupDate;
    }

    /**
     * Sets the value of the pickupDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPickupDate(GLogDateTimeType value) {
        this.pickupDate = value;
    }

    /**
     * Gets the value of the deliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDeliveryDate() {
        return deliveryDate;
    }

    /**
     * Sets the value of the deliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDeliveryDate(GLogDateTimeType value) {
        this.deliveryDate = value;
    }

    /**
     * Gets the value of the vesselInfo property.
     * 
     * @return
     *     possible object is
     *     {@link VesselInfoType }
     *     
     */
    public VesselInfoType getVesselInfo() {
        return vesselInfo;
    }

    /**
     * Sets the value of the vesselInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link VesselInfoType }
     *     
     */
    public void setVesselInfo(VesselInfoType value) {
        this.vesselInfo = value;
    }

    /**
     * Gets the value of the invoiceServiceCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvoiceServiceCodeGid() {
        return invoiceServiceCodeGid;
    }

    /**
     * Sets the value of the invoiceServiceCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvoiceServiceCodeGid(GLogXMLGidType value) {
        this.invoiceServiceCodeGid = value;
    }

    /**
     * Gets the value of the port property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the port property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPort().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortType }
     * 
     * 
     */
    public List<PortType> getPort() {
        if (port == null) {
            port = new ArrayList<PortType>();
        }
        return this.port;
    }

    /**
     * Gets the value of the letterofCredit property.
     * 
     * @return
     *     possible object is
     *     {@link LetterOfCreditType }
     *     
     */
    public LetterOfCreditType getLetterofCredit() {
        return letterofCredit;
    }

    /**
     * Sets the value of the letterofCredit property.
     * 
     * @param value
     *     allowed object is
     *     {@link LetterOfCreditType }
     *     
     */
    public void setLetterofCredit(LetterOfCreditType value) {
        this.letterofCredit = value;
    }

    /**
     * Gets the value of the destinationStation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getDestinationStation() {
        return destinationStation;
    }

    /**
     * Sets the value of the destinationStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setDestinationStation(GLogXMLStationType value) {
        this.destinationStation = value;
    }

    /**
     * Gets the value of the originStation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLStationType }
     *     
     */
    public GLogXMLStationType getOriginStation() {
        return originStation;
    }

    /**
     * Sets the value of the originStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLStationType }
     *     
     */
    public void setOriginStation(GLogXMLStationType value) {
        this.originStation = value;
    }

    /**
     * Gets the value of the protectiveSvc property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the protectiveSvc property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtectiveSvc().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtectiveSvcType }
     * 
     * 
     */
    public List<ProtectiveSvcType> getProtectiveSvc() {
        if (protectiveSvc == null) {
            protectiveSvc = new ArrayList<ProtectiveSvcType>();
        }
        return this.protectiveSvc;
    }

    /**
     * Gets the value of the genericStopOff property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the genericStopOff property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenericStopOff().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StopOffType }
     * 
     * 
     */
    public List<StopOffType> getGenericStopOff() {
        if (genericStopOff == null) {
            genericStopOff = new ArrayList<StopOffType>();
        }
        return this.genericStopOff;
    }

    /**
     * Gets the value of the genericEquipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the genericEquipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenericEquipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericEquipmentType }
     * 
     * 
     */
    public List<GenericEquipmentType> getGenericEquipment() {
        if (genericEquipment == null) {
            genericEquipment = new ArrayList<GenericEquipmentType>();
        }
        return this.genericEquipment;
    }

    /**
     * Gets the value of the genericLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the genericLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGenericLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericLineItemType }
     * 
     * 
     */
    public List<GenericLineItemType> getGenericLineItem() {
        if (genericLineItem == null) {
            genericLineItem = new ArrayList<GenericLineItemType>();
        }
        return this.genericLineItem;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FlightRowType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlightRowType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FLIGHT_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FLIGHT_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MAJOR_RECORD_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IS_AIR_CARGO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LEAVE_TIME_AFTER_MIDNIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ARRIVAL_TIME_AFTER_MIDNIGHT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ELAPSED_TIME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FLIGHT_CATEGORY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SRC_IATA_CITY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEST_IATA_CITY_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DAY_ADJUSTMENT_COUNTER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DISCONTINUE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FREQUENCY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TRAFFIC_RESTRICTION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SRC_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DEST_LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AR_TM_AFTER_MIDNIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AR_TM_AFTER_MIDNIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ELAPSED_TM_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ELAPSED_TM_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LV_TM_AFTER_MIDNIGHT_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LV_TM_AFTER_MIDNIGHT_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlightRowType", propOrder = {
    "flightgid",
    "flightxid",
    "majorrecordtype",
    "isaircargo",
    "leavetimeaftermidnight",
    "arrivaltimeaftermidnight",
    "elapsedtime",
    "flightcategory",
    "srciatacitycode",
    "destiatacitycode",
    "dayadjustmentcounter",
    "effectivedate",
    "discontinuedate",
    "frequency",
    "trafficrestriction",
    "srclocationgid",
    "destlocationgid",
    "domainname",
    "artmaftermidnightuomcode",
    "artmaftermidnightbase",
    "elapsedtmuomcode",
    "elapsedtmbase",
    "lvtmaftermidnightuomcode",
    "lvtmaftermidnightbase",
    "insertdate",
    "updatedate",
    "insertuser",
    "updateuser"
})
public class FlightRowType {

    @XmlElement(name = "FLIGHT_GID")
    protected String flightgid;
    @XmlElement(name = "FLIGHT_XID")
    protected String flightxid;
    @XmlElement(name = "MAJOR_RECORD_TYPE")
    protected String majorrecordtype;
    @XmlElement(name = "IS_AIR_CARGO")
    protected String isaircargo;
    @XmlElement(name = "LEAVE_TIME_AFTER_MIDNIGHT")
    protected String leavetimeaftermidnight;
    @XmlElement(name = "ARRIVAL_TIME_AFTER_MIDNIGHT")
    protected String arrivaltimeaftermidnight;
    @XmlElement(name = "ELAPSED_TIME")
    protected String elapsedtime;
    @XmlElement(name = "FLIGHT_CATEGORY")
    protected String flightcategory;
    @XmlElement(name = "SRC_IATA_CITY_CODE")
    protected String srciatacitycode;
    @XmlElement(name = "DEST_IATA_CITY_CODE")
    protected String destiatacitycode;
    @XmlElement(name = "DAY_ADJUSTMENT_COUNTER")
    protected String dayadjustmentcounter;
    @XmlElement(name = "EFFECTIVE_DATE")
    protected String effectivedate;
    @XmlElement(name = "DISCONTINUE_DATE")
    protected String discontinuedate;
    @XmlElement(name = "FREQUENCY")
    protected String frequency;
    @XmlElement(name = "TRAFFIC_RESTRICTION")
    protected String trafficrestriction;
    @XmlElement(name = "SRC_LOCATION_GID")
    protected String srclocationgid;
    @XmlElement(name = "DEST_LOCATION_GID")
    protected String destlocationgid;
    @XmlElement(name = "DOMAIN_NAME")
    protected String domainname;
    @XmlElement(name = "AR_TM_AFTER_MIDNIGHT_UOM_CODE")
    protected String artmaftermidnightuomcode;
    @XmlElement(name = "AR_TM_AFTER_MIDNIGHT_BASE")
    protected String artmaftermidnightbase;
    @XmlElement(name = "ELAPSED_TM_UOM_CODE")
    protected String elapsedtmuomcode;
    @XmlElement(name = "ELAPSED_TM_BASE")
    protected String elapsedtmbase;
    @XmlElement(name = "LV_TM_AFTER_MIDNIGHT_UOM_CODE")
    protected String lvtmaftermidnightuomcode;
    @XmlElement(name = "LV_TM_AFTER_MIDNIGHT_BASE")
    protected String lvtmaftermidnightbase;
    @XmlElement(name = "INSERT_DATE")
    protected String insertdate;
    @XmlElement(name = "UPDATE_DATE")
    protected String updatedate;
    @XmlElement(name = "INSERT_USER")
    protected String insertuser;
    @XmlElement(name = "UPDATE_USER")
    protected String updateuser;
    @XmlAttribute(name = "num")
    protected String num;

    /**
     * Gets the value of the flightgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTGID() {
        return flightgid;
    }

    /**
     * Sets the value of the flightgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTGID(String value) {
        this.flightgid = value;
    }

    /**
     * Gets the value of the flightxid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTXID() {
        return flightxid;
    }

    /**
     * Sets the value of the flightxid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTXID(String value) {
        this.flightxid = value;
    }

    /**
     * Gets the value of the majorrecordtype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAJORRECORDTYPE() {
        return majorrecordtype;
    }

    /**
     * Sets the value of the majorrecordtype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAJORRECORDTYPE(String value) {
        this.majorrecordtype = value;
    }

    /**
     * Gets the value of the isaircargo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISAIRCARGO() {
        return isaircargo;
    }

    /**
     * Sets the value of the isaircargo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISAIRCARGO(String value) {
        this.isaircargo = value;
    }

    /**
     * Gets the value of the leavetimeaftermidnight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLEAVETIMEAFTERMIDNIGHT() {
        return leavetimeaftermidnight;
    }

    /**
     * Sets the value of the leavetimeaftermidnight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLEAVETIMEAFTERMIDNIGHT(String value) {
        this.leavetimeaftermidnight = value;
    }

    /**
     * Gets the value of the arrivaltimeaftermidnight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARRIVALTIMEAFTERMIDNIGHT() {
        return arrivaltimeaftermidnight;
    }

    /**
     * Sets the value of the arrivaltimeaftermidnight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARRIVALTIMEAFTERMIDNIGHT(String value) {
        this.arrivaltimeaftermidnight = value;
    }

    /**
     * Gets the value of the elapsedtime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTIME() {
        return elapsedtime;
    }

    /**
     * Sets the value of the elapsedtime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTIME(String value) {
        this.elapsedtime = value;
    }

    /**
     * Gets the value of the flightcategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLIGHTCATEGORY() {
        return flightcategory;
    }

    /**
     * Sets the value of the flightcategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLIGHTCATEGORY(String value) {
        this.flightcategory = value;
    }

    /**
     * Gets the value of the srciatacitycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCIATACITYCODE() {
        return srciatacitycode;
    }

    /**
     * Sets the value of the srciatacitycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCIATACITYCODE(String value) {
        this.srciatacitycode = value;
    }

    /**
     * Gets the value of the destiatacitycode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTIATACITYCODE() {
        return destiatacitycode;
    }

    /**
     * Sets the value of the destiatacitycode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTIATACITYCODE(String value) {
        this.destiatacitycode = value;
    }

    /**
     * Gets the value of the dayadjustmentcounter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDAYADJUSTMENTCOUNTER() {
        return dayadjustmentcounter;
    }

    /**
     * Sets the value of the dayadjustmentcounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDAYADJUSTMENTCOUNTER(String value) {
        this.dayadjustmentcounter = value;
    }

    /**
     * Gets the value of the effectivedate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEFFECTIVEDATE() {
        return effectivedate;
    }

    /**
     * Sets the value of the effectivedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEFFECTIVEDATE(String value) {
        this.effectivedate = value;
    }

    /**
     * Gets the value of the discontinuedate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDISCONTINUEDATE() {
        return discontinuedate;
    }

    /**
     * Sets the value of the discontinuedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDISCONTINUEDATE(String value) {
        this.discontinuedate = value;
    }

    /**
     * Gets the value of the frequency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFREQUENCY() {
        return frequency;
    }

    /**
     * Sets the value of the frequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFREQUENCY(String value) {
        this.frequency = value;
    }

    /**
     * Gets the value of the trafficrestriction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTRAFFICRESTRICTION() {
        return trafficrestriction;
    }

    /**
     * Sets the value of the trafficrestriction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTRAFFICRESTRICTION(String value) {
        this.trafficrestriction = value;
    }

    /**
     * Gets the value of the srclocationgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSRCLOCATIONGID() {
        return srclocationgid;
    }

    /**
     * Sets the value of the srclocationgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSRCLOCATIONGID(String value) {
        this.srclocationgid = value;
    }

    /**
     * Gets the value of the destlocationgid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTLOCATIONGID() {
        return destlocationgid;
    }

    /**
     * Sets the value of the destlocationgid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTLOCATIONGID(String value) {
        this.destlocationgid = value;
    }

    /**
     * Gets the value of the domainname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDOMAINNAME() {
        return domainname;
    }

    /**
     * Sets the value of the domainname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDOMAINNAME(String value) {
        this.domainname = value;
    }

    /**
     * Gets the value of the artmaftermidnightuomcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARTMAFTERMIDNIGHTUOMCODE() {
        return artmaftermidnightuomcode;
    }

    /**
     * Sets the value of the artmaftermidnightuomcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARTMAFTERMIDNIGHTUOMCODE(String value) {
        this.artmaftermidnightuomcode = value;
    }

    /**
     * Gets the value of the artmaftermidnightbase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getARTMAFTERMIDNIGHTBASE() {
        return artmaftermidnightbase;
    }

    /**
     * Sets the value of the artmaftermidnightbase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setARTMAFTERMIDNIGHTBASE(String value) {
        this.artmaftermidnightbase = value;
    }

    /**
     * Gets the value of the elapsedtmuomcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTMUOMCODE() {
        return elapsedtmuomcode;
    }

    /**
     * Sets the value of the elapsedtmuomcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTMUOMCODE(String value) {
        this.elapsedtmuomcode = value;
    }

    /**
     * Gets the value of the elapsedtmbase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getELAPSEDTMBASE() {
        return elapsedtmbase;
    }

    /**
     * Sets the value of the elapsedtmbase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setELAPSEDTMBASE(String value) {
        this.elapsedtmbase = value;
    }

    /**
     * Gets the value of the lvtmaftermidnightuomcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLVTMAFTERMIDNIGHTUOMCODE() {
        return lvtmaftermidnightuomcode;
    }

    /**
     * Sets the value of the lvtmaftermidnightuomcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLVTMAFTERMIDNIGHTUOMCODE(String value) {
        this.lvtmaftermidnightuomcode = value;
    }

    /**
     * Gets the value of the lvtmaftermidnightbase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLVTMAFTERMIDNIGHTBASE() {
        return lvtmaftermidnightbase;
    }

    /**
     * Sets the value of the lvtmaftermidnightbase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLVTMAFTERMIDNIGHTBASE(String value) {
        this.lvtmaftermidnightbase = value;
    }

    /**
     * Gets the value of the insertdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTDATE() {
        return insertdate;
    }

    /**
     * Sets the value of the insertdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTDATE(String value) {
        this.insertdate = value;
    }

    /**
     * Gets the value of the updatedate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEDATE() {
        return updatedate;
    }

    /**
     * Sets the value of the updatedate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEDATE(String value) {
        this.updatedate = value;
    }

    /**
     * Gets the value of the insertuser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINSERTUSER() {
        return insertuser;
    }

    /**
     * Sets the value of the insertuser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINSERTUSER(String value) {
        this.insertuser = value;
    }

    /**
     * Gets the value of the updateuser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUPDATEUSER() {
        return updateuser;
    }

    /**
     * Sets the value of the updateuser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUPDATEUSER(String value) {
        this.updateuser = value;
    }

    /**
     * Gets the value of the num property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNum() {
        return num;
    }

    /**
     * Sets the value of the num property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNum(String value) {
        this.num = value;
    }

}

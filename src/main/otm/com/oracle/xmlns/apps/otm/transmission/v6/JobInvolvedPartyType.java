
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for JobInvolvedPartyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="JobInvolvedPartyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType"/&gt;
 *         &lt;element name="ShippingAgentContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "JobInvolvedPartyType", propOrder = {
    "involvedParty",
    "shippingAgentContactGid"
})
public class JobInvolvedPartyType {

    @XmlElement(name = "InvolvedParty", required = true)
    protected InvolvedPartyType involvedParty;
    @XmlElement(name = "ShippingAgentContactGid")
    protected GLogXMLGidType shippingAgentContactGid;

    /**
     * Gets the value of the involvedParty property.
     * 
     * @return
     *     possible object is
     *     {@link InvolvedPartyType }
     *     
     */
    public InvolvedPartyType getInvolvedParty() {
        return involvedParty;
    }

    /**
     * Sets the value of the involvedParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvolvedPartyType }
     *     
     */
    public void setInvolvedParty(InvolvedPartyType value) {
        this.involvedParty = value;
    }

    /**
     * Gets the value of the shippingAgentContactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactGid() {
        return shippingAgentContactGid;
    }

    /**
     * Sets the value of the shippingAgentContactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactGid(GLogXMLGidType value) {
        this.shippingAgentContactGid = value;
    }

}

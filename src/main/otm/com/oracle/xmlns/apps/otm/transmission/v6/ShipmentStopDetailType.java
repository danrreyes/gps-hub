
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ShipmentStopDetail specifies an activity (pickup, delivery, or other),
 *             and the ship unit that is being processed.
 *          
 * 
 * <p>Java class for ShipmentStopDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentStopDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ActivityDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="IsPermanent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitDwellTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentStopDetailType", propOrder = {
    "activity",
    "activityDuration",
    "shipUnitGid",
    "isPermanent",
    "involvedParty",
    "shipUnitDwellTime"
})
public class ShipmentStopDetailType {

    @XmlElement(name = "Activity", required = true)
    protected String activity;
    @XmlElement(name = "ActivityDuration")
    protected GLogXMLDurationType activityDuration;
    @XmlElement(name = "ShipUnitGid", required = true)
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "IsPermanent")
    protected String isPermanent;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "ShipUnitDwellTime")
    protected GLogXMLDurationType shipUnitDwellTime;

    /**
     * Gets the value of the activity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Sets the value of the activity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivity(String value) {
        this.activity = value;
    }

    /**
     * Gets the value of the activityDuration property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActivityDuration() {
        return activityDuration;
    }

    /**
     * Sets the value of the activityDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActivityDuration(GLogXMLDurationType value) {
        this.activityDuration = value;
    }

    /**
     * Gets the value of the shipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Sets the value of the shipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPermanent(String value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the shipUnitDwellTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getShipUnitDwellTime() {
        return shipUnitDwellTime;
    }

    /**
     * Sets the value of the shipUnitDwellTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setShipUnitDwellTime(GLogXMLDurationType value) {
        this.shipUnitDwellTime = value;
    }

}

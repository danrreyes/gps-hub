
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the Booking Line Amendment Changes for a SEquipment (container).
 * 
 * <p>Java class for BookLineAmendDtlSEquipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookLineAmendDtlSEquipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ChangeAction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookLineAmendDtlSEquipmentType", propOrder = {
    "changeAction",
    "sEquipmentGid",
    "weightVolume"
})
public class BookLineAmendDtlSEquipmentType {

    @XmlElement(name = "ChangeAction")
    protected String changeAction;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;

    /**
     * Gets the value of the changeAction property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChangeAction() {
        return changeAction;
    }

    /**
     * Sets the value of the changeAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChangeAction(String value) {
        this.changeAction = value;
    }

    /**
     * Gets the value of the sEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Sets the value of the sEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the weightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Sets the value of the weightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

}

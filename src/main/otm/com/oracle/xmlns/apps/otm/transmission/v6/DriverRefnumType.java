
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             DriverRefnum is a qualifier/value pair used to refer to a driver.
 *          
 * 
 * <p>Java class for DriverRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DriverRefnumQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="DriverRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverRefnumType", propOrder = {
    "driverRefnumQualGid",
    "driverRefnumValue"
})
public class DriverRefnumType {

    @XmlElement(name = "DriverRefnumQualGid", required = true)
    protected GLogXMLGidType driverRefnumQualGid;
    @XmlElement(name = "DriverRefnumValue", required = true)
    protected String driverRefnumValue;

    /**
     * Gets the value of the driverRefnumQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverRefnumQualGid() {
        return driverRefnumQualGid;
    }

    /**
     * Sets the value of the driverRefnumQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverRefnumQualGid(GLogXMLGidType value) {
        this.driverRefnumQualGid = value;
    }

    /**
     * Gets the value of the driverRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDriverRefnumValue() {
        return driverRefnumValue;
    }

    /**
     * Sets the value of the driverRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDriverRefnumValue(String value) {
        this.driverRefnumValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies a list of equipment marks.
 * 
 * <p>Java class for EquipMarkListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EquipMarkListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EquipMarkListGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipMarkGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipMarkListType", propOrder = {
    "equipMarkListGid",
    "description",
    "equipMarkGid"
})
public class EquipMarkListType {

    @XmlElement(name = "EquipMarkListGid", required = true)
    protected GLogXMLGidType equipMarkListGid;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "EquipMarkGid")
    protected List<GLogXMLGidType> equipMarkGid;

    /**
     * Gets the value of the equipMarkListGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipMarkListGid() {
        return equipMarkListGid;
    }

    /**
     * Sets the value of the equipMarkListGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipMarkListGid(GLogXMLGidType value) {
        this.equipMarkListGid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the equipMarkGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipMarkGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipMarkGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getEquipMarkGid() {
        if (equipMarkGid == null) {
            equipMarkGid = new ArrayList<GLogXMLGidType>();
        }
        return this.equipMarkGid;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * SpecialCharge is an element of FreightRate used to convey special charges.
 * 
 * <p>Java class for SpecialChargeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SpecialChargeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SpecialChargeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SpecialChargeDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SpecialChargeType", propOrder = {
    "specialChargeCode",
    "specialChargeDesc"
})
public class SpecialChargeType {

    @XmlElement(name = "SpecialChargeCode", required = true)
    protected String specialChargeCode;
    @XmlElement(name = "SpecialChargeDesc")
    protected String specialChargeDesc;

    /**
     * Gets the value of the specialChargeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialChargeCode() {
        return specialChargeCode;
    }

    /**
     * Sets the value of the specialChargeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialChargeCode(String value) {
        this.specialChargeCode = value;
    }

    /**
     * Gets the value of the specialChargeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialChargeDesc() {
        return specialChargeDesc;
    }

    /**
     * Sets the value of the specialChargeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialChargeDesc(String value) {
        this.specialChargeDesc = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Remark
 * 
 * <p>Java class for DemurrageTransactionRemarkType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionRemarkType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DmTransactionRemarkQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DmTransactionRemarkText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionRemarkType", propOrder = {
    "dmTransactionRemarkQualGid",
    "dmTransactionRemarkText"
})
public class DemurrageTransactionRemarkType {

    @XmlElement(name = "DmTransactionRemarkQualGid")
    protected GLogXMLGidType dmTransactionRemarkQualGid;
    @XmlElement(name = "DmTransactionRemarkText")
    protected String dmTransactionRemarkText;

    /**
     * Gets the value of the dmTransactionRemarkQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmTransactionRemarkQualGid() {
        return dmTransactionRemarkQualGid;
    }

    /**
     * Sets the value of the dmTransactionRemarkQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmTransactionRemarkQualGid(GLogXMLGidType value) {
        this.dmTransactionRemarkQualGid = value;
    }

    /**
     * Gets the value of the dmTransactionRemarkText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmTransactionRemarkText() {
        return dmTransactionRemarkText;
    }

    /**
     * Sets the value of the dmTransactionRemarkText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmTransactionRemarkText(String value) {
        this.dmTransactionRemarkText = value;
    }

}

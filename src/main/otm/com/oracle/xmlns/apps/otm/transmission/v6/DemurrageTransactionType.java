
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The demurrage trasaction is built based on events (like CLM, EDI322 etc) and help manage resouces (equipments) that
 *             are incuring costs. It includes charges (that are time based) called demurrage, detention and storage with no clear
 *             definition of terms.
 *          
 * 
 * <p>Java class for DemurrageTransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReportingScac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DmLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DmTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DmCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DmState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DmCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DmSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CpLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CpTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CpCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CpState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CpCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CpSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AARCarType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChassisInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChassisNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChassisInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartEventStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StartLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StartScacGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StartCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StartIsCarLoaded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndEventStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EndLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EndScacGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndTimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EndCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndSplc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndIsCarLoaded" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InSEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InSTCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InUserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InSourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InWeightUomCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InWeightBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InPackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InTHUGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutSEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutSTCCGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutUserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutSourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutDestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OutWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OutWeightUomCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OutWeightBase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OutPackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutTHUGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate1" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate2" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate3" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate4" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate5" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate6" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate7" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate8" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate9" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate10" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode1Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode2Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode4Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode5Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode6Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode7Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode8Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode9Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BsStatusCode10Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MarkedForPurge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ActivityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ModeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EndDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DemurrageTransactionEvent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionEventType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DemurrageTransactionInvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DemurrageTransactionLineitem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionLineitemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DemurrageTransactionNote" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionNoteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DemurrageTransactionRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DemurrageTransactionRemark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DemurrageTransactionRemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DemurrageTransactionStatus" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionType", propOrder = {
    "reportingScac",
    "dmLocationGid",
    "dmTimeZoneGid",
    "dmCity",
    "dmState",
    "dmCountry",
    "dmSplc",
    "cpLocationGid",
    "cpTimeZoneGid",
    "cpCity",
    "cpState",
    "cpCountry",
    "cpSplc",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentGid",
    "equipmentTypeGid",
    "equipmentRefnumQualifierGid",
    "equipmentRefnumValue",
    "aarCarType",
    "chassisInitial",
    "chassisNumber",
    "chassisInitialNumber",
    "startEventStatusCodeGid",
    "startLocationGid",
    "startScacGid",
    "startTimeZoneGid",
    "startCity",
    "startState",
    "startCountry",
    "startSplc",
    "startIsCarLoaded",
    "endEventStatusCodeGid",
    "endLocationGid",
    "endScacGid",
    "endTimeZoneGid",
    "endCity",
    "endState",
    "endCountry",
    "endSplc",
    "endIsCarLoaded",
    "inShipmentGid",
    "inSEquipmentGid",
    "inSTCCGid",
    "inUserDefinedCommodityGid",
    "inSourceLocationGid",
    "inDestLocationGid",
    "inShipUnitCount",
    "inWeight",
    "inWeightUomCode",
    "inWeightBase",
    "inPackagedItemGid",
    "inItemGid",
    "inTHUGid",
    "outShipmentGid",
    "outSEquipmentGid",
    "outSTCCGid",
    "outUserDefinedCommodityGid",
    "outSourceLocationGid",
    "outDestLocationGid",
    "outShipUnitCount",
    "outWeight",
    "outWeightUomCode",
    "outWeightBase",
    "outPackagedItemGid",
    "outItemGid",
    "outTHUGid",
    "dmEventDate1",
    "dmEventDate2",
    "dmEventDate3",
    "dmEventDate4",
    "dmEventDate5",
    "dmEventDate6",
    "dmEventDate7",
    "dmEventDate8",
    "dmEventDate9",
    "dmEventDate10",
    "bsStatusCode1Gid",
    "bsStatusCode2Gid",
    "bsStatusCode3Gid",
    "bsStatusCode4Gid",
    "bsStatusCode5Gid",
    "bsStatusCode6Gid",
    "bsStatusCode7Gid",
    "bsStatusCode8Gid",
    "bsStatusCode9Gid",
    "bsStatusCode10Gid",
    "isSystemGenerated",
    "markedForPurge",
    "activityTypeGid",
    "modeType",
    "endDate",
    "demurrageTransactionEvent",
    "demurrageTransactionInvolvedParty",
    "demurrageTransactionLineitem",
    "demurrageTransactionNote",
    "demurrageTransactionRefnum",
    "demurrageTransactionRemark",
    "demurrageTransactionStatus"
})
public class DemurrageTransactionType
    extends OTMTransactionOut
{

    @XmlElement(name = "ReportingScac")
    protected String reportingScac;
    @XmlElement(name = "DmLocationGid")
    protected GLogXMLGidType dmLocationGid;
    @XmlElement(name = "DmTimeZoneGid")
    protected GLogXMLGidType dmTimeZoneGid;
    @XmlElement(name = "DmCity")
    protected String dmCity;
    @XmlElement(name = "DmState")
    protected String dmState;
    @XmlElement(name = "DmCountry")
    protected String dmCountry;
    @XmlElement(name = "DmSplc")
    protected String dmSplc;
    @XmlElement(name = "CpLocationGid")
    protected GLogXMLGidType cpLocationGid;
    @XmlElement(name = "CpTimeZoneGid")
    protected GLogXMLGidType cpTimeZoneGid;
    @XmlElement(name = "CpCity")
    protected String cpCity;
    @XmlElement(name = "CpState")
    protected String cpState;
    @XmlElement(name = "CpCountry")
    protected String cpCountry;
    @XmlElement(name = "CpSplc")
    protected String cpSplc;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentRefnumQualifierGid")
    protected GLogXMLGidType equipmentRefnumQualifierGid;
    @XmlElement(name = "EquipmentRefnumValue")
    protected String equipmentRefnumValue;
    @XmlElement(name = "AARCarType")
    protected String aarCarType;
    @XmlElement(name = "ChassisInitial")
    protected String chassisInitial;
    @XmlElement(name = "ChassisNumber")
    protected String chassisNumber;
    @XmlElement(name = "ChassisInitialNumber")
    protected String chassisInitialNumber;
    @XmlElement(name = "StartEventStatusCodeGid")
    protected GLogXMLGidType startEventStatusCodeGid;
    @XmlElement(name = "StartLocationGid")
    protected GLogXMLGidType startLocationGid;
    @XmlElement(name = "StartScacGid")
    protected String startScacGid;
    @XmlElement(name = "StartTimeZoneGid")
    protected GLogXMLGidType startTimeZoneGid;
    @XmlElement(name = "StartCity")
    protected String startCity;
    @XmlElement(name = "StartState")
    protected String startState;
    @XmlElement(name = "StartCountry")
    protected String startCountry;
    @XmlElement(name = "StartSplc")
    protected String startSplc;
    @XmlElement(name = "StartIsCarLoaded")
    protected String startIsCarLoaded;
    @XmlElement(name = "EndEventStatusCodeGid")
    protected GLogXMLGidType endEventStatusCodeGid;
    @XmlElement(name = "EndLocationGid")
    protected GLogXMLGidType endLocationGid;
    @XmlElement(name = "EndScacGid")
    protected String endScacGid;
    @XmlElement(name = "EndTimeZoneGid")
    protected GLogXMLGidType endTimeZoneGid;
    @XmlElement(name = "EndCity")
    protected String endCity;
    @XmlElement(name = "EndState")
    protected String endState;
    @XmlElement(name = "EndCountry")
    protected String endCountry;
    @XmlElement(name = "EndSplc")
    protected String endSplc;
    @XmlElement(name = "EndIsCarLoaded")
    protected String endIsCarLoaded;
    @XmlElement(name = "InShipmentGid")
    protected GLogXMLGidType inShipmentGid;
    @XmlElement(name = "InSEquipmentGid")
    protected GLogXMLGidType inSEquipmentGid;
    @XmlElement(name = "InSTCCGid")
    protected GLogXMLGidType inSTCCGid;
    @XmlElement(name = "InUserDefinedCommodityGid")
    protected GLogXMLGidType inUserDefinedCommodityGid;
    @XmlElement(name = "InSourceLocationGid")
    protected GLogXMLGidType inSourceLocationGid;
    @XmlElement(name = "InDestLocationGid")
    protected GLogXMLGidType inDestLocationGid;
    @XmlElement(name = "InShipUnitCount")
    protected String inShipUnitCount;
    @XmlElement(name = "InWeight")
    protected String inWeight;
    @XmlElement(name = "InWeightUomCode")
    protected String inWeightUomCode;
    @XmlElement(name = "InWeightBase")
    protected String inWeightBase;
    @XmlElement(name = "InPackagedItemGid")
    protected GLogXMLGidType inPackagedItemGid;
    @XmlElement(name = "InItemGid")
    protected GLogXMLGidType inItemGid;
    @XmlElement(name = "InTHUGid")
    protected GLogXMLGidType inTHUGid;
    @XmlElement(name = "OutShipmentGid")
    protected GLogXMLGidType outShipmentGid;
    @XmlElement(name = "OutSEquipmentGid")
    protected GLogXMLGidType outSEquipmentGid;
    @XmlElement(name = "OutSTCCGid")
    protected GLogXMLGidType outSTCCGid;
    @XmlElement(name = "OutUserDefinedCommodityGid")
    protected GLogXMLGidType outUserDefinedCommodityGid;
    @XmlElement(name = "OutSourceLocationGid")
    protected GLogXMLGidType outSourceLocationGid;
    @XmlElement(name = "OutDestLocationGid")
    protected GLogXMLGidType outDestLocationGid;
    @XmlElement(name = "OutShipUnitCount")
    protected String outShipUnitCount;
    @XmlElement(name = "OutWeight")
    protected String outWeight;
    @XmlElement(name = "OutWeightUomCode")
    protected String outWeightUomCode;
    @XmlElement(name = "OutWeightBase")
    protected String outWeightBase;
    @XmlElement(name = "OutPackagedItemGid")
    protected GLogXMLGidType outPackagedItemGid;
    @XmlElement(name = "OutItemGid")
    protected GLogXMLGidType outItemGid;
    @XmlElement(name = "OutTHUGid")
    protected GLogXMLGidType outTHUGid;
    @XmlElement(name = "DmEventDate1")
    protected GLogDateTimeType dmEventDate1;
    @XmlElement(name = "DmEventDate2")
    protected GLogDateTimeType dmEventDate2;
    @XmlElement(name = "DmEventDate3")
    protected GLogDateTimeType dmEventDate3;
    @XmlElement(name = "DmEventDate4")
    protected GLogDateTimeType dmEventDate4;
    @XmlElement(name = "DmEventDate5")
    protected GLogDateTimeType dmEventDate5;
    @XmlElement(name = "DmEventDate6")
    protected GLogDateTimeType dmEventDate6;
    @XmlElement(name = "DmEventDate7")
    protected GLogDateTimeType dmEventDate7;
    @XmlElement(name = "DmEventDate8")
    protected GLogDateTimeType dmEventDate8;
    @XmlElement(name = "DmEventDate9")
    protected GLogDateTimeType dmEventDate9;
    @XmlElement(name = "DmEventDate10")
    protected GLogDateTimeType dmEventDate10;
    @XmlElement(name = "BsStatusCode1Gid")
    protected GLogXMLGidType bsStatusCode1Gid;
    @XmlElement(name = "BsStatusCode2Gid")
    protected GLogXMLGidType bsStatusCode2Gid;
    @XmlElement(name = "BsStatusCode3Gid")
    protected GLogXMLGidType bsStatusCode3Gid;
    @XmlElement(name = "BsStatusCode4Gid")
    protected GLogXMLGidType bsStatusCode4Gid;
    @XmlElement(name = "BsStatusCode5Gid")
    protected GLogXMLGidType bsStatusCode5Gid;
    @XmlElement(name = "BsStatusCode6Gid")
    protected GLogXMLGidType bsStatusCode6Gid;
    @XmlElement(name = "BsStatusCode7Gid")
    protected GLogXMLGidType bsStatusCode7Gid;
    @XmlElement(name = "BsStatusCode8Gid")
    protected GLogXMLGidType bsStatusCode8Gid;
    @XmlElement(name = "BsStatusCode9Gid")
    protected GLogXMLGidType bsStatusCode9Gid;
    @XmlElement(name = "BsStatusCode10Gid")
    protected GLogXMLGidType bsStatusCode10Gid;
    @XmlElement(name = "IsSystemGenerated")
    protected String isSystemGenerated;
    @XmlElement(name = "MarkedForPurge")
    protected String markedForPurge;
    @XmlElement(name = "ActivityTypeGid")
    protected GLogXMLGidType activityTypeGid;
    @XmlElement(name = "ModeType")
    protected String modeType;
    @XmlElement(name = "EndDate")
    protected GLogDateTimeType endDate;
    @XmlElement(name = "DemurrageTransactionEvent")
    protected List<DemurrageTransactionEventType> demurrageTransactionEvent;
    @XmlElement(name = "DemurrageTransactionInvolvedParty")
    protected List<DemurrageInvolvedPartyType> demurrageTransactionInvolvedParty;
    @XmlElement(name = "DemurrageTransactionLineitem")
    protected List<DemurrageTransactionLineitemType> demurrageTransactionLineitem;
    @XmlElement(name = "DemurrageTransactionNote")
    protected List<DemurrageTransactionNoteType> demurrageTransactionNote;
    @XmlElement(name = "DemurrageTransactionRefnum")
    protected List<DemurrageTransactionRefnumType> demurrageTransactionRefnum;
    @XmlElement(name = "DemurrageTransactionRemark")
    protected List<DemurrageTransactionRemarkType> demurrageTransactionRemark;
    @XmlElement(name = "DemurrageTransactionStatus")
    protected List<StatusType> demurrageTransactionStatus;

    /**
     * Gets the value of the reportingScac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingScac() {
        return reportingScac;
    }

    /**
     * Sets the value of the reportingScac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingScac(String value) {
        this.reportingScac = value;
    }

    /**
     * Gets the value of the dmLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmLocationGid() {
        return dmLocationGid;
    }

    /**
     * Sets the value of the dmLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmLocationGid(GLogXMLGidType value) {
        this.dmLocationGid = value;
    }

    /**
     * Gets the value of the dmTimeZoneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDmTimeZoneGid() {
        return dmTimeZoneGid;
    }

    /**
     * Sets the value of the dmTimeZoneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDmTimeZoneGid(GLogXMLGidType value) {
        this.dmTimeZoneGid = value;
    }

    /**
     * Gets the value of the dmCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmCity() {
        return dmCity;
    }

    /**
     * Sets the value of the dmCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmCity(String value) {
        this.dmCity = value;
    }

    /**
     * Gets the value of the dmState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmState() {
        return dmState;
    }

    /**
     * Sets the value of the dmState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmState(String value) {
        this.dmState = value;
    }

    /**
     * Gets the value of the dmCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmCountry() {
        return dmCountry;
    }

    /**
     * Sets the value of the dmCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmCountry(String value) {
        this.dmCountry = value;
    }

    /**
     * Gets the value of the dmSplc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDmSplc() {
        return dmSplc;
    }

    /**
     * Sets the value of the dmSplc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDmSplc(String value) {
        this.dmSplc = value;
    }

    /**
     * Gets the value of the cpLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCpLocationGid() {
        return cpLocationGid;
    }

    /**
     * Sets the value of the cpLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCpLocationGid(GLogXMLGidType value) {
        this.cpLocationGid = value;
    }

    /**
     * Gets the value of the cpTimeZoneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCpTimeZoneGid() {
        return cpTimeZoneGid;
    }

    /**
     * Sets the value of the cpTimeZoneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCpTimeZoneGid(GLogXMLGidType value) {
        this.cpTimeZoneGid = value;
    }

    /**
     * Gets the value of the cpCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpCity() {
        return cpCity;
    }

    /**
     * Sets the value of the cpCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpCity(String value) {
        this.cpCity = value;
    }

    /**
     * Gets the value of the cpState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpState() {
        return cpState;
    }

    /**
     * Sets the value of the cpState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpState(String value) {
        this.cpState = value;
    }

    /**
     * Gets the value of the cpCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpCountry() {
        return cpCountry;
    }

    /**
     * Sets the value of the cpCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpCountry(String value) {
        this.cpCountry = value;
    }

    /**
     * Gets the value of the cpSplc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpSplc() {
        return cpSplc;
    }

    /**
     * Sets the value of the cpSplc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpSplc(String value) {
        this.cpSplc = value;
    }

    /**
     * Gets the value of the equipmentInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Sets the value of the equipmentInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Gets the value of the equipmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Sets the value of the equipmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the equipmentInitialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Sets the value of the equipmentInitialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Gets the value of the equipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Sets the value of the equipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Gets the value of the equipmentTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Sets the value of the equipmentTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Gets the value of the equipmentRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentRefnumQualifierGid() {
        return equipmentRefnumQualifierGid;
    }

    /**
     * Sets the value of the equipmentRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentRefnumQualifierGid(GLogXMLGidType value) {
        this.equipmentRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the equipmentRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentRefnumValue() {
        return equipmentRefnumValue;
    }

    /**
     * Sets the value of the equipmentRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentRefnumValue(String value) {
        this.equipmentRefnumValue = value;
    }

    /**
     * Gets the value of the aarCarType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAARCarType() {
        return aarCarType;
    }

    /**
     * Sets the value of the aarCarType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAARCarType(String value) {
        this.aarCarType = value;
    }

    /**
     * Gets the value of the chassisInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitial() {
        return chassisInitial;
    }

    /**
     * Sets the value of the chassisInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitial(String value) {
        this.chassisInitial = value;
    }

    /**
     * Gets the value of the chassisNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisNumber() {
        return chassisNumber;
    }

    /**
     * Sets the value of the chassisNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisNumber(String value) {
        this.chassisNumber = value;
    }

    /**
     * Gets the value of the chassisInitialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitialNumber() {
        return chassisInitialNumber;
    }

    /**
     * Sets the value of the chassisInitialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitialNumber(String value) {
        this.chassisInitialNumber = value;
    }

    /**
     * Gets the value of the startEventStatusCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartEventStatusCodeGid() {
        return startEventStatusCodeGid;
    }

    /**
     * Sets the value of the startEventStatusCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartEventStatusCodeGid(GLogXMLGidType value) {
        this.startEventStatusCodeGid = value;
    }

    /**
     * Gets the value of the startLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartLocationGid() {
        return startLocationGid;
    }

    /**
     * Sets the value of the startLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartLocationGid(GLogXMLGidType value) {
        this.startLocationGid = value;
    }

    /**
     * Gets the value of the startScacGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartScacGid() {
        return startScacGid;
    }

    /**
     * Sets the value of the startScacGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartScacGid(String value) {
        this.startScacGid = value;
    }

    /**
     * Gets the value of the startTimeZoneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStartTimeZoneGid() {
        return startTimeZoneGid;
    }

    /**
     * Sets the value of the startTimeZoneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStartTimeZoneGid(GLogXMLGidType value) {
        this.startTimeZoneGid = value;
    }

    /**
     * Gets the value of the startCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartCity() {
        return startCity;
    }

    /**
     * Sets the value of the startCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartCity(String value) {
        this.startCity = value;
    }

    /**
     * Gets the value of the startState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartState() {
        return startState;
    }

    /**
     * Sets the value of the startState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartState(String value) {
        this.startState = value;
    }

    /**
     * Gets the value of the startCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartCountry() {
        return startCountry;
    }

    /**
     * Sets the value of the startCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartCountry(String value) {
        this.startCountry = value;
    }

    /**
     * Gets the value of the startSplc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartSplc() {
        return startSplc;
    }

    /**
     * Sets the value of the startSplc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartSplc(String value) {
        this.startSplc = value;
    }

    /**
     * Gets the value of the startIsCarLoaded property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStartIsCarLoaded() {
        return startIsCarLoaded;
    }

    /**
     * Sets the value of the startIsCarLoaded property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStartIsCarLoaded(String value) {
        this.startIsCarLoaded = value;
    }

    /**
     * Gets the value of the endEventStatusCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndEventStatusCodeGid() {
        return endEventStatusCodeGid;
    }

    /**
     * Sets the value of the endEventStatusCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndEventStatusCodeGid(GLogXMLGidType value) {
        this.endEventStatusCodeGid = value;
    }

    /**
     * Gets the value of the endLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndLocationGid() {
        return endLocationGid;
    }

    /**
     * Sets the value of the endLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndLocationGid(GLogXMLGidType value) {
        this.endLocationGid = value;
    }

    /**
     * Gets the value of the endScacGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndScacGid() {
        return endScacGid;
    }

    /**
     * Sets the value of the endScacGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndScacGid(String value) {
        this.endScacGid = value;
    }

    /**
     * Gets the value of the endTimeZoneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEndTimeZoneGid() {
        return endTimeZoneGid;
    }

    /**
     * Sets the value of the endTimeZoneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEndTimeZoneGid(GLogXMLGidType value) {
        this.endTimeZoneGid = value;
    }

    /**
     * Gets the value of the endCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndCity() {
        return endCity;
    }

    /**
     * Sets the value of the endCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndCity(String value) {
        this.endCity = value;
    }

    /**
     * Gets the value of the endState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndState() {
        return endState;
    }

    /**
     * Sets the value of the endState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndState(String value) {
        this.endState = value;
    }

    /**
     * Gets the value of the endCountry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndCountry() {
        return endCountry;
    }

    /**
     * Sets the value of the endCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndCountry(String value) {
        this.endCountry = value;
    }

    /**
     * Gets the value of the endSplc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndSplc() {
        return endSplc;
    }

    /**
     * Sets the value of the endSplc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndSplc(String value) {
        this.endSplc = value;
    }

    /**
     * Gets the value of the endIsCarLoaded property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndIsCarLoaded() {
        return endIsCarLoaded;
    }

    /**
     * Sets the value of the endIsCarLoaded property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndIsCarLoaded(String value) {
        this.endIsCarLoaded = value;
    }

    /**
     * Gets the value of the inShipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInShipmentGid() {
        return inShipmentGid;
    }

    /**
     * Sets the value of the inShipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInShipmentGid(GLogXMLGidType value) {
        this.inShipmentGid = value;
    }

    /**
     * Gets the value of the inSEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInSEquipmentGid() {
        return inSEquipmentGid;
    }

    /**
     * Sets the value of the inSEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInSEquipmentGid(GLogXMLGidType value) {
        this.inSEquipmentGid = value;
    }

    /**
     * Gets the value of the inSTCCGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInSTCCGid() {
        return inSTCCGid;
    }

    /**
     * Sets the value of the inSTCCGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInSTCCGid(GLogXMLGidType value) {
        this.inSTCCGid = value;
    }

    /**
     * Gets the value of the inUserDefinedCommodityGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInUserDefinedCommodityGid() {
        return inUserDefinedCommodityGid;
    }

    /**
     * Sets the value of the inUserDefinedCommodityGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInUserDefinedCommodityGid(GLogXMLGidType value) {
        this.inUserDefinedCommodityGid = value;
    }

    /**
     * Gets the value of the inSourceLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInSourceLocationGid() {
        return inSourceLocationGid;
    }

    /**
     * Sets the value of the inSourceLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInSourceLocationGid(GLogXMLGidType value) {
        this.inSourceLocationGid = value;
    }

    /**
     * Gets the value of the inDestLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInDestLocationGid() {
        return inDestLocationGid;
    }

    /**
     * Sets the value of the inDestLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInDestLocationGid(GLogXMLGidType value) {
        this.inDestLocationGid = value;
    }

    /**
     * Gets the value of the inShipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInShipUnitCount() {
        return inShipUnitCount;
    }

    /**
     * Sets the value of the inShipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInShipUnitCount(String value) {
        this.inShipUnitCount = value;
    }

    /**
     * Gets the value of the inWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInWeight() {
        return inWeight;
    }

    /**
     * Sets the value of the inWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInWeight(String value) {
        this.inWeight = value;
    }

    /**
     * Gets the value of the inWeightUomCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInWeightUomCode() {
        return inWeightUomCode;
    }

    /**
     * Sets the value of the inWeightUomCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInWeightUomCode(String value) {
        this.inWeightUomCode = value;
    }

    /**
     * Gets the value of the inWeightBase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInWeightBase() {
        return inWeightBase;
    }

    /**
     * Sets the value of the inWeightBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInWeightBase(String value) {
        this.inWeightBase = value;
    }

    /**
     * Gets the value of the inPackagedItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInPackagedItemGid() {
        return inPackagedItemGid;
    }

    /**
     * Sets the value of the inPackagedItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInPackagedItemGid(GLogXMLGidType value) {
        this.inPackagedItemGid = value;
    }

    /**
     * Gets the value of the inItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInItemGid() {
        return inItemGid;
    }

    /**
     * Sets the value of the inItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInItemGid(GLogXMLGidType value) {
        this.inItemGid = value;
    }

    /**
     * Gets the value of the inTHUGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInTHUGid() {
        return inTHUGid;
    }

    /**
     * Sets the value of the inTHUGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInTHUGid(GLogXMLGidType value) {
        this.inTHUGid = value;
    }

    /**
     * Gets the value of the outShipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutShipmentGid() {
        return outShipmentGid;
    }

    /**
     * Sets the value of the outShipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutShipmentGid(GLogXMLGidType value) {
        this.outShipmentGid = value;
    }

    /**
     * Gets the value of the outSEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutSEquipmentGid() {
        return outSEquipmentGid;
    }

    /**
     * Sets the value of the outSEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutSEquipmentGid(GLogXMLGidType value) {
        this.outSEquipmentGid = value;
    }

    /**
     * Gets the value of the outSTCCGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutSTCCGid() {
        return outSTCCGid;
    }

    /**
     * Sets the value of the outSTCCGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutSTCCGid(GLogXMLGidType value) {
        this.outSTCCGid = value;
    }

    /**
     * Gets the value of the outUserDefinedCommodityGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutUserDefinedCommodityGid() {
        return outUserDefinedCommodityGid;
    }

    /**
     * Sets the value of the outUserDefinedCommodityGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutUserDefinedCommodityGid(GLogXMLGidType value) {
        this.outUserDefinedCommodityGid = value;
    }

    /**
     * Gets the value of the outSourceLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutSourceLocationGid() {
        return outSourceLocationGid;
    }

    /**
     * Sets the value of the outSourceLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutSourceLocationGid(GLogXMLGidType value) {
        this.outSourceLocationGid = value;
    }

    /**
     * Gets the value of the outDestLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutDestLocationGid() {
        return outDestLocationGid;
    }

    /**
     * Sets the value of the outDestLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutDestLocationGid(GLogXMLGidType value) {
        this.outDestLocationGid = value;
    }

    /**
     * Gets the value of the outShipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutShipUnitCount() {
        return outShipUnitCount;
    }

    /**
     * Sets the value of the outShipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutShipUnitCount(String value) {
        this.outShipUnitCount = value;
    }

    /**
     * Gets the value of the outWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutWeight() {
        return outWeight;
    }

    /**
     * Sets the value of the outWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutWeight(String value) {
        this.outWeight = value;
    }

    /**
     * Gets the value of the outWeightUomCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutWeightUomCode() {
        return outWeightUomCode;
    }

    /**
     * Sets the value of the outWeightUomCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutWeightUomCode(String value) {
        this.outWeightUomCode = value;
    }

    /**
     * Gets the value of the outWeightBase property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutWeightBase() {
        return outWeightBase;
    }

    /**
     * Sets the value of the outWeightBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutWeightBase(String value) {
        this.outWeightBase = value;
    }

    /**
     * Gets the value of the outPackagedItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutPackagedItemGid() {
        return outPackagedItemGid;
    }

    /**
     * Sets the value of the outPackagedItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutPackagedItemGid(GLogXMLGidType value) {
        this.outPackagedItemGid = value;
    }

    /**
     * Gets the value of the outItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutItemGid() {
        return outItemGid;
    }

    /**
     * Sets the value of the outItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutItemGid(GLogXMLGidType value) {
        this.outItemGid = value;
    }

    /**
     * Gets the value of the outTHUGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutTHUGid() {
        return outTHUGid;
    }

    /**
     * Sets the value of the outTHUGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutTHUGid(GLogXMLGidType value) {
        this.outTHUGid = value;
    }

    /**
     * Gets the value of the dmEventDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate1() {
        return dmEventDate1;
    }

    /**
     * Sets the value of the dmEventDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate1(GLogDateTimeType value) {
        this.dmEventDate1 = value;
    }

    /**
     * Gets the value of the dmEventDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate2() {
        return dmEventDate2;
    }

    /**
     * Sets the value of the dmEventDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate2(GLogDateTimeType value) {
        this.dmEventDate2 = value;
    }

    /**
     * Gets the value of the dmEventDate3 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate3() {
        return dmEventDate3;
    }

    /**
     * Sets the value of the dmEventDate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate3(GLogDateTimeType value) {
        this.dmEventDate3 = value;
    }

    /**
     * Gets the value of the dmEventDate4 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate4() {
        return dmEventDate4;
    }

    /**
     * Sets the value of the dmEventDate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate4(GLogDateTimeType value) {
        this.dmEventDate4 = value;
    }

    /**
     * Gets the value of the dmEventDate5 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate5() {
        return dmEventDate5;
    }

    /**
     * Sets the value of the dmEventDate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate5(GLogDateTimeType value) {
        this.dmEventDate5 = value;
    }

    /**
     * Gets the value of the dmEventDate6 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate6() {
        return dmEventDate6;
    }

    /**
     * Sets the value of the dmEventDate6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate6(GLogDateTimeType value) {
        this.dmEventDate6 = value;
    }

    /**
     * Gets the value of the dmEventDate7 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate7() {
        return dmEventDate7;
    }

    /**
     * Sets the value of the dmEventDate7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate7(GLogDateTimeType value) {
        this.dmEventDate7 = value;
    }

    /**
     * Gets the value of the dmEventDate8 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate8() {
        return dmEventDate8;
    }

    /**
     * Sets the value of the dmEventDate8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate8(GLogDateTimeType value) {
        this.dmEventDate8 = value;
    }

    /**
     * Gets the value of the dmEventDate9 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate9() {
        return dmEventDate9;
    }

    /**
     * Sets the value of the dmEventDate9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate9(GLogDateTimeType value) {
        this.dmEventDate9 = value;
    }

    /**
     * Gets the value of the dmEventDate10 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate10() {
        return dmEventDate10;
    }

    /**
     * Sets the value of the dmEventDate10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate10(GLogDateTimeType value) {
        this.dmEventDate10 = value;
    }

    /**
     * Gets the value of the bsStatusCode1Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode1Gid() {
        return bsStatusCode1Gid;
    }

    /**
     * Sets the value of the bsStatusCode1Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode1Gid(GLogXMLGidType value) {
        this.bsStatusCode1Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode2Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode2Gid() {
        return bsStatusCode2Gid;
    }

    /**
     * Sets the value of the bsStatusCode2Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode2Gid(GLogXMLGidType value) {
        this.bsStatusCode2Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode3Gid() {
        return bsStatusCode3Gid;
    }

    /**
     * Sets the value of the bsStatusCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode3Gid(GLogXMLGidType value) {
        this.bsStatusCode3Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode4Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode4Gid() {
        return bsStatusCode4Gid;
    }

    /**
     * Sets the value of the bsStatusCode4Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode4Gid(GLogXMLGidType value) {
        this.bsStatusCode4Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode5Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode5Gid() {
        return bsStatusCode5Gid;
    }

    /**
     * Sets the value of the bsStatusCode5Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode5Gid(GLogXMLGidType value) {
        this.bsStatusCode5Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode6Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode6Gid() {
        return bsStatusCode6Gid;
    }

    /**
     * Sets the value of the bsStatusCode6Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode6Gid(GLogXMLGidType value) {
        this.bsStatusCode6Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode7Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode7Gid() {
        return bsStatusCode7Gid;
    }

    /**
     * Sets the value of the bsStatusCode7Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode7Gid(GLogXMLGidType value) {
        this.bsStatusCode7Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode8Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode8Gid() {
        return bsStatusCode8Gid;
    }

    /**
     * Sets the value of the bsStatusCode8Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode8Gid(GLogXMLGidType value) {
        this.bsStatusCode8Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode9Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode9Gid() {
        return bsStatusCode9Gid;
    }

    /**
     * Sets the value of the bsStatusCode9Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode9Gid(GLogXMLGidType value) {
        this.bsStatusCode9Gid = value;
    }

    /**
     * Gets the value of the bsStatusCode10Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBsStatusCode10Gid() {
        return bsStatusCode10Gid;
    }

    /**
     * Sets the value of the bsStatusCode10Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBsStatusCode10Gid(GLogXMLGidType value) {
        this.bsStatusCode10Gid = value;
    }

    /**
     * Gets the value of the isSystemGenerated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Sets the value of the isSystemGenerated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

    /**
     * Gets the value of the markedForPurge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarkedForPurge() {
        return markedForPurge;
    }

    /**
     * Sets the value of the markedForPurge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarkedForPurge(String value) {
        this.markedForPurge = value;
    }

    /**
     * Gets the value of the activityTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityTypeGid() {
        return activityTypeGid;
    }

    /**
     * Sets the value of the activityTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityTypeGid(GLogXMLGidType value) {
        this.activityTypeGid = value;
    }

    /**
     * Gets the value of the modeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeType() {
        return modeType;
    }

    /**
     * Sets the value of the modeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeType(String value) {
        this.modeType = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEndDate(GLogDateTimeType value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the demurrageTransactionEvent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionEvent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionEventType }
     * 
     * 
     */
    public List<DemurrageTransactionEventType> getDemurrageTransactionEvent() {
        if (demurrageTransactionEvent == null) {
            demurrageTransactionEvent = new ArrayList<DemurrageTransactionEventType>();
        }
        return this.demurrageTransactionEvent;
    }

    /**
     * Gets the value of the demurrageTransactionInvolvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionInvolvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageInvolvedPartyType }
     * 
     * 
     */
    public List<DemurrageInvolvedPartyType> getDemurrageTransactionInvolvedParty() {
        if (demurrageTransactionInvolvedParty == null) {
            demurrageTransactionInvolvedParty = new ArrayList<DemurrageInvolvedPartyType>();
        }
        return this.demurrageTransactionInvolvedParty;
    }

    /**
     * Gets the value of the demurrageTransactionLineitem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionLineitem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionLineitem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionLineitemType }
     * 
     * 
     */
    public List<DemurrageTransactionLineitemType> getDemurrageTransactionLineitem() {
        if (demurrageTransactionLineitem == null) {
            demurrageTransactionLineitem = new ArrayList<DemurrageTransactionLineitemType>();
        }
        return this.demurrageTransactionLineitem;
    }

    /**
     * Gets the value of the demurrageTransactionNote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionNote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionNoteType }
     * 
     * 
     */
    public List<DemurrageTransactionNoteType> getDemurrageTransactionNote() {
        if (demurrageTransactionNote == null) {
            demurrageTransactionNote = new ArrayList<DemurrageTransactionNoteType>();
        }
        return this.demurrageTransactionNote;
    }

    /**
     * Gets the value of the demurrageTransactionRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionRefnumType }
     * 
     * 
     */
    public List<DemurrageTransactionRefnumType> getDemurrageTransactionRefnum() {
        if (demurrageTransactionRefnum == null) {
            demurrageTransactionRefnum = new ArrayList<DemurrageTransactionRefnumType>();
        }
        return this.demurrageTransactionRefnum;
    }

    /**
     * Gets the value of the demurrageTransactionRemark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionRemark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DemurrageTransactionRemarkType }
     * 
     * 
     */
    public List<DemurrageTransactionRemarkType> getDemurrageTransactionRemark() {
        if (demurrageTransactionRemark == null) {
            demurrageTransactionRemark = new ArrayList<DemurrageTransactionRemarkType>();
        }
        return this.demurrageTransactionRemark;
    }

    /**
     * Gets the value of the demurrageTransactionStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the demurrageTransactionStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDemurrageTransactionStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getDemurrageTransactionStatus() {
        if (demurrageTransactionStatus == null) {
            demurrageTransactionStatus = new ArrayList<StatusType>();
        }
        return this.demurrageTransactionStatus;
    }

}

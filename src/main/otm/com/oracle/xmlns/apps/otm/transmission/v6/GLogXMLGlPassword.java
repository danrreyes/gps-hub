
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLGlPassword complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLGlPassword"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GlPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GlPasswordType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLGlPassword", propOrder = {
    "glPassword"
})
public class GLogXMLGlPassword {

    @XmlElement(name = "GlPassword", required = true)
    protected GlPasswordType glPassword;

    /**
     * Gets the value of the glPassword property.
     * 
     * @return
     *     possible object is
     *     {@link GlPasswordType }
     *     
     */
    public GlPasswordType getGlPassword() {
        return glPassword;
    }

    /**
     * Sets the value of the glPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link GlPasswordType }
     *     
     */
    public void setGlPassword(GlPasswordType value) {
        this.glPassword = value;
    }

}

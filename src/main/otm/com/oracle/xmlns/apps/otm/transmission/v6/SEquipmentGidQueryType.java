
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to query for SEquipmentGid to assign the Shipment.ShipUnit.
 * 
 * <p>Java class for SEquipmentGidQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SEquipmentGidQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="SEquipGidMatchOption" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SEquipmentGidQueryType", propOrder = {
    "sEquipGidMatchOption",
    "intSavedQuery"
})
public class SEquipmentGidQueryType {

    @XmlElement(name = "SEquipGidMatchOption")
    protected String sEquipGidMatchOption;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;

    /**
     * Gets the value of the sEquipGidMatchOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEquipGidMatchOption() {
        return sEquipGidMatchOption;
    }

    /**
     * Sets the value of the sEquipGidMatchOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEquipGidMatchOption(String value) {
        this.sEquipGidMatchOption = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * EquipmentAttribute is a generic qualifier/value pair used to specify an attribute associated with a piece of equipment.
 * 
 * <p>Java class for EquipmentAttributeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EquipmentAttributeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EquipmentAttributeQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="EquipmentAttributeValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentAttributeType", propOrder = {
    "equipmentAttributeQualifierGid",
    "equipmentAttributeValue"
})
public class EquipmentAttributeType {

    @XmlElement(name = "EquipmentAttributeQualifierGid", required = true)
    protected GLogXMLGidType equipmentAttributeQualifierGid;
    @XmlElement(name = "EquipmentAttributeValue", required = true)
    protected String equipmentAttributeValue;

    /**
     * Gets the value of the equipmentAttributeQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentAttributeQualifierGid() {
        return equipmentAttributeQualifierGid;
    }

    /**
     * Sets the value of the equipmentAttributeQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentAttributeQualifierGid(GLogXMLGidType value) {
        this.equipmentAttributeQualifierGid = value;
    }

    /**
     * Gets the value of the equipmentAttributeValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentAttributeValue() {
        return equipmentAttributeValue;
    }

    /**
     * Sets the value of the equipmentAttributeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentAttributeValue(String value) {
        this.equipmentAttributeValue = value;
    }

}

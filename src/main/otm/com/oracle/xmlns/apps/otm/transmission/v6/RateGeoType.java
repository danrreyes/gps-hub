
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RateGeoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RateGeoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;element name="RATE_GEO_ROW" maxOccurs="unbounded"&gt;
 *             &lt;complexType&gt;
 *               &lt;complexContent&gt;
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                   &lt;sequence&gt;
 *                     &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="RATE_GEO_XID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="STOPS_INCLUDED_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="RATE_ZONE_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ROUTE_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="MULTI_BASE_GROUPS_RULE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="VIA_SRC_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="VIA_DEST_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="VIA_SRC_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="VIA_DEST_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="BUY_SERVPROV_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="BUY_RATE_GEO_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="IS_MASTER_OVERRIDES_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="IS_QUOTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="RATE_GEO_STOPS" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;element name="RATE_GEO_STOPS_ROW" maxOccurs="unbounded"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;/sequence&gt;
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="RATE_GEO_ACCESSORIAL" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="RATE_GEO_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
 *                                       &lt;/sequence&gt;
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="RATE_GEO_COST_GROUP" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
 *                               &lt;element name="RATE_GEO_COST_GROUP_ROW" maxOccurs="unbounded"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/&gt;
 *                                       &lt;/sequence&gt;
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
 *                                       &lt;/sequence&gt;
 *                                       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                     &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}X_LANE" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="RATE_GEO_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;element name="RATE_GEO_REMARKS" minOccurs="0"&gt;
 *                       &lt;complexType&gt;
 *                         &lt;complexContent&gt;
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                             &lt;sequence&gt;
 *                               &lt;element name="RATE_GEO_REMARKS_ROW" maxOccurs="unbounded"&gt;
 *                                 &lt;complexType&gt;
 *                                   &lt;complexContent&gt;
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                       &lt;sequence&gt;
 *                                         &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                                         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                                       &lt;/sequence&gt;
 *                                     &lt;/restriction&gt;
 *                                   &lt;/complexContent&gt;
 *                                 &lt;/complexType&gt;
 *                               &lt;/element&gt;
 *                             &lt;/sequence&gt;
 *                           &lt;/restriction&gt;
 *                         &lt;/complexContent&gt;
 *                       &lt;/complexType&gt;
 *                     &lt;/element&gt;
 *                   &lt;/sequence&gt;
 *                   &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;/restriction&gt;
 *               &lt;/complexContent&gt;
 *             &lt;/complexType&gt;
 *           &lt;/element&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateGeoType", propOrder = {
    "sendReason",
    "rategeorow"
})
public class RateGeoType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "RATE_GEO_ROW")
    protected List<RateGeoType.RATEGEOROW> rategeorow;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the rategeorow property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rategeorow property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRATEGEOROW().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RateGeoType.RATEGEOROW }
     * 
     * 
     */
    public List<RateGeoType.RATEGEOROW> getRATEGEOROW() {
        if (rategeorow == null) {
            rategeorow = new ArrayList<RateGeoType.RATEGEOROW>();
        }
        return this.rategeorow;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RATE_GEO_XID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RATE_OFFERING_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="X_LANE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EQUIPMENT_GROUP_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_SERVICE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MIN_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="TOTAL_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PICKUP_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DELIVERY_STOPS_CONSTRAINT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_ALLOWANCE_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_DISTANCE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_DISTANCE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CIRCUITY_DISTANCE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_PERCENT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_DISTANCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_UOM_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MAX_CIRCUITY_DISTANCE_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="STOPS_INCLUDED_RATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="FLEX_COMMODITY_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_QUALITY_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHIPPER_MIN_VALUE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MIN_STOPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHORT_LINE_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHORT_LINE_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="SHORT_LINE_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_ZONE_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="LOCATION_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUTE_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DIM_RATE_FACTOR_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EFFECTIVE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXPIRATION_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ALLOW_UNCOSTED_LINE_ITEMS" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="SHIPPER_MIN_VALUE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="MULTI_BASE_GROUPS_RULE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="RAIL_INTER_MODAL_PLAN_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CUSTOMER_RATE_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="EXPIRE_MARK_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="VIA_SRC_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="VIA_DEST_LOC_PROF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="VIA_SRC_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="VIA_DEST_LOC_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="BUY_SERVPROV_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="BUY_RATE_GEO_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PAYMENT_METHOD_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_MASTER_OVERRIDES_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="HAZARDOUS_RATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="DOMAIN_PROFILE_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="IS_QUOTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RO_TIME_PERIOD_DEF_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_STOPS" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *                   &lt;element name="RATE_GEO_STOPS_ROW" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RATE_GEO_ACCESSORIAL" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="RATE_GEO_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RATE_GEO_COST_GROUP" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
     *                   &lt;element name="RATE_GEO_COST_GROUP_ROW" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                           &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element ref="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}X_LANE" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_NUMBER10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="ATTRIBUTE_DATE10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_DESC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="RATE_GEO_REMARKS" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="RATE_GEO_REMARKS_ROW" maxOccurs="unbounded"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                             &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                             &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "rategeogid",
        "rategeoxid",
        "rateofferinggid",
        "xlanegid",
        "equipmentgroupprofilegid",
        "rateservicegid",
        "mincost",
        "mincostgid",
        "mincostbase",
        "totalstopsconstraint",
        "pickupstopsconstraint",
        "deliverystopsconstraint",
        "circuityallowancepercent",
        "circuitydistancecost",
        "circuitydistancecostgid",
        "circuitydistancecostbase",
        "maxcircuitypercent",
        "maxcircuitydistance",
        "maxcircuitydistanceuomcode",
        "maxcircuitydistancebase",
        "stopsincludedrate",
        "flexcommodityprofilegid",
        "ratequalitygid",
        "shipperminvalue",
        "domainname",
        "minstops",
        "shortlinecost",
        "shortlinecostgid",
        "shortlinecostbase",
        "ratezoneprofilegid",
        "locationgid",
        "routecodegid",
        "dimratefactorgid",
        "effectivedate",
        "expirationdate",
        "allowuncostedlineitems",
        "shipperminvaluegid",
        "multibasegroupsrule",
        "railintermodalplangid",
        "customerratecode",
        "cofctofc",
        "expiremarkid",
        "viasrclocprofgid",
        "viadestlocprofgid",
        "viasrclocgid",
        "viadestlocgid",
        "roundingtype",
        "roundinginterval",
        "roundingfieldslevel",
        "roundingapplication",
        "buyservprovprofilegid",
        "buyrategeoprofilegid",
        "paymentmethodcodegid",
        "ismasteroverridesbase",
        "hazardousratetype",
        "domainprofilegid",
        "isquote",
        "rotimeperioddefgid",
        "insertuser",
        "insertdate",
        "updateuser",
        "updatedate",
        "rategeostops",
        "rategeoaccessorial",
        "rategeocostgroup",
        "rgspecialserviceaccessorial",
        "xlane",
        "attribute1",
        "attribute2",
        "attribute3",
        "attribute4",
        "attribute5",
        "attribute6",
        "attribute7",
        "attribute8",
        "attribute9",
        "attribute10",
        "attribute11",
        "attribute12",
        "attribute13",
        "attribute14",
        "attribute15",
        "attribute16",
        "attribute17",
        "attribute18",
        "attribute19",
        "attribute20",
        "attributenumber1",
        "attributenumber2",
        "attributenumber3",
        "attributenumber4",
        "attributenumber5",
        "attributenumber6",
        "attributenumber7",
        "attributenumber8",
        "attributenumber9",
        "attributenumber10",
        "attributedate1",
        "attributedate2",
        "attributedate3",
        "attributedate4",
        "attributedate5",
        "attributedate6",
        "attributedate7",
        "attributedate8",
        "attributedate9",
        "attributedate10",
        "rategeodesc",
        "rategeoremarks"
    })
    public static class RATEGEOROW {

        @XmlElement(name = "RATE_GEO_GID", required = true)
        protected String rategeogid;
        @XmlElement(name = "RATE_GEO_XID", required = true)
        protected String rategeoxid;
        @XmlElement(name = "RATE_OFFERING_GID", required = true)
        protected String rateofferinggid;
        @XmlElement(name = "X_LANE_GID")
        protected String xlanegid;
        @XmlElement(name = "EQUIPMENT_GROUP_PROFILE_GID")
        protected String equipmentgroupprofilegid;
        @XmlElement(name = "RATE_SERVICE_GID")
        protected String rateservicegid;
        @XmlElement(name = "MIN_COST")
        protected String mincost;
        @XmlElement(name = "MIN_COST_GID")
        protected String mincostgid;
        @XmlElement(name = "MIN_COST_BASE")
        protected String mincostbase;
        @XmlElement(name = "TOTAL_STOPS_CONSTRAINT")
        protected String totalstopsconstraint;
        @XmlElement(name = "PICKUP_STOPS_CONSTRAINT")
        protected String pickupstopsconstraint;
        @XmlElement(name = "DELIVERY_STOPS_CONSTRAINT")
        protected String deliverystopsconstraint;
        @XmlElement(name = "CIRCUITY_ALLOWANCE_PERCENT")
        protected String circuityallowancepercent;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST")
        protected String circuitydistancecost;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_GID")
        protected String circuitydistancecostgid;
        @XmlElement(name = "CIRCUITY_DISTANCE_COST_BASE")
        protected String circuitydistancecostbase;
        @XmlElement(name = "MAX_CIRCUITY_PERCENT")
        protected String maxcircuitypercent;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE")
        protected String maxcircuitydistance;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_UOM_CODE")
        protected String maxcircuitydistanceuomcode;
        @XmlElement(name = "MAX_CIRCUITY_DISTANCE_BASE")
        protected String maxcircuitydistancebase;
        @XmlElement(name = "STOPS_INCLUDED_RATE")
        protected String stopsincludedrate;
        @XmlElement(name = "FLEX_COMMODITY_PROFILE_GID")
        protected String flexcommodityprofilegid;
        @XmlElement(name = "RATE_QUALITY_GID")
        protected String ratequalitygid;
        @XmlElement(name = "SHIPPER_MIN_VALUE")
        protected String shipperminvalue;
        @XmlElement(name = "DOMAIN_NAME", required = true)
        protected String domainname;
        @XmlElement(name = "MIN_STOPS")
        protected String minstops;
        @XmlElement(name = "SHORT_LINE_COST")
        protected String shortlinecost;
        @XmlElement(name = "SHORT_LINE_COST_GID")
        protected String shortlinecostgid;
        @XmlElement(name = "SHORT_LINE_COST_BASE")
        protected String shortlinecostbase;
        @XmlElement(name = "RATE_ZONE_PROFILE_GID")
        protected String ratezoneprofilegid;
        @XmlElement(name = "LOCATION_GID")
        protected String locationgid;
        @XmlElement(name = "ROUTE_CODE_GID")
        protected String routecodegid;
        @XmlElement(name = "DIM_RATE_FACTOR_GID")
        protected String dimratefactorgid;
        @XmlElement(name = "EFFECTIVE_DATE")
        protected String effectivedate;
        @XmlElement(name = "EXPIRATION_DATE")
        protected String expirationdate;
        @XmlElement(name = "ALLOW_UNCOSTED_LINE_ITEMS", required = true)
        protected String allowuncostedlineitems;
        @XmlElement(name = "SHIPPER_MIN_VALUE_GID")
        protected String shipperminvaluegid;
        @XmlElement(name = "MULTI_BASE_GROUPS_RULE", required = true)
        protected String multibasegroupsrule;
        @XmlElement(name = "RAIL_INTER_MODAL_PLAN_GID")
        protected String railintermodalplangid;
        @XmlElement(name = "CUSTOMER_RATE_CODE")
        protected String customerratecode;
        @XmlElement(name = "COFC_TOFC")
        protected String cofctofc;
        @XmlElement(name = "EXPIRE_MARK_ID")
        protected String expiremarkid;
        @XmlElement(name = "VIA_SRC_LOC_PROF_GID")
        protected String viasrclocprofgid;
        @XmlElement(name = "VIA_DEST_LOC_PROF_GID")
        protected String viadestlocprofgid;
        @XmlElement(name = "VIA_SRC_LOC_GID")
        protected String viasrclocgid;
        @XmlElement(name = "VIA_DEST_LOC_GID")
        protected String viadestlocgid;
        @XmlElement(name = "ROUNDING_TYPE")
        protected String roundingtype;
        @XmlElement(name = "ROUNDING_INTERVAL")
        protected String roundinginterval;
        @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
        protected String roundingfieldslevel;
        @XmlElement(name = "ROUNDING_APPLICATION")
        protected String roundingapplication;
        @XmlElement(name = "BUY_SERVPROV_PROFILE_GID")
        protected String buyservprovprofilegid;
        @XmlElement(name = "BUY_RATE_GEO_PROFILE_GID")
        protected String buyrategeoprofilegid;
        @XmlElement(name = "PAYMENT_METHOD_CODE_GID")
        protected String paymentmethodcodegid;
        @XmlElement(name = "IS_MASTER_OVERRIDES_BASE")
        protected String ismasteroverridesbase;
        @XmlElement(name = "HAZARDOUS_RATE_TYPE")
        protected String hazardousratetype;
        @XmlElement(name = "DOMAIN_PROFILE_GID")
        protected String domainprofilegid;
        @XmlElement(name = "IS_QUOTE")
        protected String isquote;
        @XmlElement(name = "RO_TIME_PERIOD_DEF_GID")
        protected String rotimeperioddefgid;
        @XmlElement(name = "INSERT_USER", required = true)
        protected String insertuser;
        @XmlElement(name = "INSERT_DATE", required = true)
        protected String insertdate;
        @XmlElement(name = "UPDATE_USER")
        protected String updateuser;
        @XmlElement(name = "UPDATE_DATE")
        protected String updatedate;
        @XmlElement(name = "RATE_GEO_STOPS")
        protected RateGeoType.RATEGEOROW.RATEGEOSTOPS rategeostops;
        @XmlElement(name = "RATE_GEO_ACCESSORIAL")
        protected RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL rategeoaccessorial;
        @XmlElement(name = "RATE_GEO_COST_GROUP")
        protected RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP rategeocostgroup;
        @XmlElement(name = "RG_SPECIAL_SERVICE_ACCESSORIAL")
        protected RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL rgspecialserviceaccessorial;
        @XmlElement(name = "X_LANE")
        protected XLANE xlane;
        @XmlElement(name = "ATTRIBUTE1")
        protected String attribute1;
        @XmlElement(name = "ATTRIBUTE2")
        protected String attribute2;
        @XmlElement(name = "ATTRIBUTE3")
        protected String attribute3;
        @XmlElement(name = "ATTRIBUTE4")
        protected String attribute4;
        @XmlElement(name = "ATTRIBUTE5")
        protected String attribute5;
        @XmlElement(name = "ATTRIBUTE6")
        protected String attribute6;
        @XmlElement(name = "ATTRIBUTE7")
        protected String attribute7;
        @XmlElement(name = "ATTRIBUTE8")
        protected String attribute8;
        @XmlElement(name = "ATTRIBUTE9")
        protected String attribute9;
        @XmlElement(name = "ATTRIBUTE10")
        protected String attribute10;
        @XmlElement(name = "ATTRIBUTE11")
        protected String attribute11;
        @XmlElement(name = "ATTRIBUTE12")
        protected String attribute12;
        @XmlElement(name = "ATTRIBUTE13")
        protected String attribute13;
        @XmlElement(name = "ATTRIBUTE14")
        protected String attribute14;
        @XmlElement(name = "ATTRIBUTE15")
        protected String attribute15;
        @XmlElement(name = "ATTRIBUTE16")
        protected String attribute16;
        @XmlElement(name = "ATTRIBUTE17")
        protected String attribute17;
        @XmlElement(name = "ATTRIBUTE18")
        protected String attribute18;
        @XmlElement(name = "ATTRIBUTE19")
        protected String attribute19;
        @XmlElement(name = "ATTRIBUTE20")
        protected String attribute20;
        @XmlElement(name = "ATTRIBUTE_NUMBER1")
        protected String attributenumber1;
        @XmlElement(name = "ATTRIBUTE_NUMBER2")
        protected String attributenumber2;
        @XmlElement(name = "ATTRIBUTE_NUMBER3")
        protected String attributenumber3;
        @XmlElement(name = "ATTRIBUTE_NUMBER4")
        protected String attributenumber4;
        @XmlElement(name = "ATTRIBUTE_NUMBER5")
        protected String attributenumber5;
        @XmlElement(name = "ATTRIBUTE_NUMBER6")
        protected String attributenumber6;
        @XmlElement(name = "ATTRIBUTE_NUMBER7")
        protected String attributenumber7;
        @XmlElement(name = "ATTRIBUTE_NUMBER8")
        protected String attributenumber8;
        @XmlElement(name = "ATTRIBUTE_NUMBER9")
        protected String attributenumber9;
        @XmlElement(name = "ATTRIBUTE_NUMBER10")
        protected String attributenumber10;
        @XmlElement(name = "ATTRIBUTE_DATE1")
        protected String attributedate1;
        @XmlElement(name = "ATTRIBUTE_DATE2")
        protected String attributedate2;
        @XmlElement(name = "ATTRIBUTE_DATE3")
        protected String attributedate3;
        @XmlElement(name = "ATTRIBUTE_DATE4")
        protected String attributedate4;
        @XmlElement(name = "ATTRIBUTE_DATE5")
        protected String attributedate5;
        @XmlElement(name = "ATTRIBUTE_DATE6")
        protected String attributedate6;
        @XmlElement(name = "ATTRIBUTE_DATE7")
        protected String attributedate7;
        @XmlElement(name = "ATTRIBUTE_DATE8")
        protected String attributedate8;
        @XmlElement(name = "ATTRIBUTE_DATE9")
        protected String attributedate9;
        @XmlElement(name = "ATTRIBUTE_DATE10")
        protected String attributedate10;
        @XmlElement(name = "RATE_GEO_DESC")
        protected String rategeodesc;
        @XmlElement(name = "RATE_GEO_REMARKS")
        protected RateGeoType.RATEGEOROW.RATEGEOREMARKS rategeoremarks;
        @XmlAttribute(name = "num")
        protected String num;

        /**
         * Gets the value of the rategeogid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOGID() {
            return rategeogid;
        }

        /**
         * Sets the value of the rategeogid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOGID(String value) {
            this.rategeogid = value;
        }

        /**
         * Gets the value of the rategeoxid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEOXID() {
            return rategeoxid;
        }

        /**
         * Sets the value of the rategeoxid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEOXID(String value) {
            this.rategeoxid = value;
        }

        /**
         * Gets the value of the rateofferinggid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEOFFERINGGID() {
            return rateofferinggid;
        }

        /**
         * Sets the value of the rateofferinggid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEOFFERINGGID(String value) {
            this.rateofferinggid = value;
        }

        /**
         * Gets the value of the xlanegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getXLANEGID() {
            return xlanegid;
        }

        /**
         * Sets the value of the xlanegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setXLANEGID(String value) {
            this.xlanegid = value;
        }

        /**
         * Gets the value of the equipmentgroupprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEQUIPMENTGROUPPROFILEGID() {
            return equipmentgroupprofilegid;
        }

        /**
         * Sets the value of the equipmentgroupprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEQUIPMENTGROUPPROFILEGID(String value) {
            this.equipmentgroupprofilegid = value;
        }

        /**
         * Gets the value of the rateservicegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATESERVICEGID() {
            return rateservicegid;
        }

        /**
         * Sets the value of the rateservicegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATESERVICEGID(String value) {
            this.rateservicegid = value;
        }

        /**
         * Gets the value of the mincost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOST() {
            return mincost;
        }

        /**
         * Sets the value of the mincost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOST(String value) {
            this.mincost = value;
        }

        /**
         * Gets the value of the mincostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTGID() {
            return mincostgid;
        }

        /**
         * Sets the value of the mincostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTGID(String value) {
            this.mincostgid = value;
        }

        /**
         * Gets the value of the mincostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINCOSTBASE() {
            return mincostbase;
        }

        /**
         * Sets the value of the mincostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINCOSTBASE(String value) {
            this.mincostbase = value;
        }

        /**
         * Gets the value of the totalstopsconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTOTALSTOPSCONSTRAINT() {
            return totalstopsconstraint;
        }

        /**
         * Sets the value of the totalstopsconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTOTALSTOPSCONSTRAINT(String value) {
            this.totalstopsconstraint = value;
        }

        /**
         * Gets the value of the pickupstopsconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPICKUPSTOPSCONSTRAINT() {
            return pickupstopsconstraint;
        }

        /**
         * Sets the value of the pickupstopsconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPICKUPSTOPSCONSTRAINT(String value) {
            this.pickupstopsconstraint = value;
        }

        /**
         * Gets the value of the deliverystopsconstraint property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDELIVERYSTOPSCONSTRAINT() {
            return deliverystopsconstraint;
        }

        /**
         * Sets the value of the deliverystopsconstraint property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDELIVERYSTOPSCONSTRAINT(String value) {
            this.deliverystopsconstraint = value;
        }

        /**
         * Gets the value of the circuityallowancepercent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYALLOWANCEPERCENT() {
            return circuityallowancepercent;
        }

        /**
         * Sets the value of the circuityallowancepercent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYALLOWANCEPERCENT(String value) {
            this.circuityallowancepercent = value;
        }

        /**
         * Gets the value of the circuitydistancecost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOST() {
            return circuitydistancecost;
        }

        /**
         * Sets the value of the circuitydistancecost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOST(String value) {
            this.circuitydistancecost = value;
        }

        /**
         * Gets the value of the circuitydistancecostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTGID() {
            return circuitydistancecostgid;
        }

        /**
         * Sets the value of the circuitydistancecostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTGID(String value) {
            this.circuitydistancecostgid = value;
        }

        /**
         * Gets the value of the circuitydistancecostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCIRCUITYDISTANCECOSTBASE() {
            return circuitydistancecostbase;
        }

        /**
         * Sets the value of the circuitydistancecostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCIRCUITYDISTANCECOSTBASE(String value) {
            this.circuitydistancecostbase = value;
        }

        /**
         * Gets the value of the maxcircuitypercent property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYPERCENT() {
            return maxcircuitypercent;
        }

        /**
         * Sets the value of the maxcircuitypercent property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYPERCENT(String value) {
            this.maxcircuitypercent = value;
        }

        /**
         * Gets the value of the maxcircuitydistance property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCE() {
            return maxcircuitydistance;
        }

        /**
         * Sets the value of the maxcircuitydistance property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCE(String value) {
            this.maxcircuitydistance = value;
        }

        /**
         * Gets the value of the maxcircuitydistanceuomcode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEUOMCODE() {
            return maxcircuitydistanceuomcode;
        }

        /**
         * Sets the value of the maxcircuitydistanceuomcode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEUOMCODE(String value) {
            this.maxcircuitydistanceuomcode = value;
        }

        /**
         * Gets the value of the maxcircuitydistancebase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMAXCIRCUITYDISTANCEBASE() {
            return maxcircuitydistancebase;
        }

        /**
         * Sets the value of the maxcircuitydistancebase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMAXCIRCUITYDISTANCEBASE(String value) {
            this.maxcircuitydistancebase = value;
        }

        /**
         * Gets the value of the stopsincludedrate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTOPSINCLUDEDRATE() {
            return stopsincludedrate;
        }

        /**
         * Sets the value of the stopsincludedrate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTOPSINCLUDEDRATE(String value) {
            this.stopsincludedrate = value;
        }

        /**
         * Gets the value of the flexcommodityprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFLEXCOMMODITYPROFILEGID() {
            return flexcommodityprofilegid;
        }

        /**
         * Sets the value of the flexcommodityprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFLEXCOMMODITYPROFILEGID(String value) {
            this.flexcommodityprofilegid = value;
        }

        /**
         * Gets the value of the ratequalitygid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEQUALITYGID() {
            return ratequalitygid;
        }

        /**
         * Sets the value of the ratequalitygid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEQUALITYGID(String value) {
            this.ratequalitygid = value;
        }

        /**
         * Gets the value of the shipperminvalue property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUE() {
            return shipperminvalue;
        }

        /**
         * Sets the value of the shipperminvalue property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUE(String value) {
            this.shipperminvalue = value;
        }

        /**
         * Gets the value of the domainname property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINNAME() {
            return domainname;
        }

        /**
         * Sets the value of the domainname property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINNAME(String value) {
            this.domainname = value;
        }

        /**
         * Gets the value of the minstops property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMINSTOPS() {
            return minstops;
        }

        /**
         * Sets the value of the minstops property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMINSTOPS(String value) {
            this.minstops = value;
        }

        /**
         * Gets the value of the shortlinecost property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOST() {
            return shortlinecost;
        }

        /**
         * Sets the value of the shortlinecost property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOST(String value) {
            this.shortlinecost = value;
        }

        /**
         * Gets the value of the shortlinecostgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTGID() {
            return shortlinecostgid;
        }

        /**
         * Sets the value of the shortlinecostgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTGID(String value) {
            this.shortlinecostgid = value;
        }

        /**
         * Gets the value of the shortlinecostbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHORTLINECOSTBASE() {
            return shortlinecostbase;
        }

        /**
         * Sets the value of the shortlinecostbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHORTLINECOSTBASE(String value) {
            this.shortlinecostbase = value;
        }

        /**
         * Gets the value of the ratezoneprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEZONEPROFILEGID() {
            return ratezoneprofilegid;
        }

        /**
         * Sets the value of the ratezoneprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEZONEPROFILEGID(String value) {
            this.ratezoneprofilegid = value;
        }

        /**
         * Gets the value of the locationgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLOCATIONGID() {
            return locationgid;
        }

        /**
         * Sets the value of the locationgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLOCATIONGID(String value) {
            this.locationgid = value;
        }

        /**
         * Gets the value of the routecodegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUTECODEGID() {
            return routecodegid;
        }

        /**
         * Sets the value of the routecodegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUTECODEGID(String value) {
            this.routecodegid = value;
        }

        /**
         * Gets the value of the dimratefactorgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDIMRATEFACTORGID() {
            return dimratefactorgid;
        }

        /**
         * Sets the value of the dimratefactorgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDIMRATEFACTORGID(String value) {
            this.dimratefactorgid = value;
        }

        /**
         * Gets the value of the effectivedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEFFECTIVEDATE() {
            return effectivedate;
        }

        /**
         * Sets the value of the effectivedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEFFECTIVEDATE(String value) {
            this.effectivedate = value;
        }

        /**
         * Gets the value of the expirationdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIRATIONDATE() {
            return expirationdate;
        }

        /**
         * Sets the value of the expirationdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIRATIONDATE(String value) {
            this.expirationdate = value;
        }

        /**
         * Gets the value of the allowuncostedlineitems property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getALLOWUNCOSTEDLINEITEMS() {
            return allowuncostedlineitems;
        }

        /**
         * Sets the value of the allowuncostedlineitems property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setALLOWUNCOSTEDLINEITEMS(String value) {
            this.allowuncostedlineitems = value;
        }

        /**
         * Gets the value of the shipperminvaluegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSHIPPERMINVALUEGID() {
            return shipperminvaluegid;
        }

        /**
         * Sets the value of the shipperminvaluegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSHIPPERMINVALUEGID(String value) {
            this.shipperminvaluegid = value;
        }

        /**
         * Gets the value of the multibasegroupsrule property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMULTIBASEGROUPSRULE() {
            return multibasegroupsrule;
        }

        /**
         * Sets the value of the multibasegroupsrule property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMULTIBASEGROUPSRULE(String value) {
            this.multibasegroupsrule = value;
        }

        /**
         * Gets the value of the railintermodalplangid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRAILINTERMODALPLANGID() {
            return railintermodalplangid;
        }

        /**
         * Sets the value of the railintermodalplangid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRAILINTERMODALPLANGID(String value) {
            this.railintermodalplangid = value;
        }

        /**
         * Gets the value of the customerratecode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUSTOMERRATECODE() {
            return customerratecode;
        }

        /**
         * Sets the value of the customerratecode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUSTOMERRATECODE(String value) {
            this.customerratecode = value;
        }

        /**
         * Gets the value of the cofctofc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOFCTOFC() {
            return cofctofc;
        }

        /**
         * Sets the value of the cofctofc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOFCTOFC(String value) {
            this.cofctofc = value;
        }

        /**
         * Gets the value of the expiremarkid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXPIREMARKID() {
            return expiremarkid;
        }

        /**
         * Sets the value of the expiremarkid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXPIREMARKID(String value) {
            this.expiremarkid = value;
        }

        /**
         * Gets the value of the viasrclocprofgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIASRCLOCPROFGID() {
            return viasrclocprofgid;
        }

        /**
         * Sets the value of the viasrclocprofgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIASRCLOCPROFGID(String value) {
            this.viasrclocprofgid = value;
        }

        /**
         * Gets the value of the viadestlocprofgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIADESTLOCPROFGID() {
            return viadestlocprofgid;
        }

        /**
         * Sets the value of the viadestlocprofgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIADESTLOCPROFGID(String value) {
            this.viadestlocprofgid = value;
        }

        /**
         * Gets the value of the viasrclocgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIASRCLOCGID() {
            return viasrclocgid;
        }

        /**
         * Sets the value of the viasrclocgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIASRCLOCGID(String value) {
            this.viasrclocgid = value;
        }

        /**
         * Gets the value of the viadestlocgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVIADESTLOCGID() {
            return viadestlocgid;
        }

        /**
         * Sets the value of the viadestlocgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVIADESTLOCGID(String value) {
            this.viadestlocgid = value;
        }

        /**
         * Gets the value of the roundingtype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGTYPE() {
            return roundingtype;
        }

        /**
         * Sets the value of the roundingtype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGTYPE(String value) {
            this.roundingtype = value;
        }

        /**
         * Gets the value of the roundinginterval property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGINTERVAL() {
            return roundinginterval;
        }

        /**
         * Sets the value of the roundinginterval property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGINTERVAL(String value) {
            this.roundinginterval = value;
        }

        /**
         * Gets the value of the roundingfieldslevel property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGFIELDSLEVEL() {
            return roundingfieldslevel;
        }

        /**
         * Sets the value of the roundingfieldslevel property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGFIELDSLEVEL(String value) {
            this.roundingfieldslevel = value;
        }

        /**
         * Gets the value of the roundingapplication property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROUNDINGAPPLICATION() {
            return roundingapplication;
        }

        /**
         * Sets the value of the roundingapplication property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROUNDINGAPPLICATION(String value) {
            this.roundingapplication = value;
        }

        /**
         * Gets the value of the buyservprovprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBUYSERVPROVPROFILEGID() {
            return buyservprovprofilegid;
        }

        /**
         * Sets the value of the buyservprovprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBUYSERVPROVPROFILEGID(String value) {
            this.buyservprovprofilegid = value;
        }

        /**
         * Gets the value of the buyrategeoprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBUYRATEGEOPROFILEGID() {
            return buyrategeoprofilegid;
        }

        /**
         * Sets the value of the buyrategeoprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBUYRATEGEOPROFILEGID(String value) {
            this.buyrategeoprofilegid = value;
        }

        /**
         * Gets the value of the paymentmethodcodegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPAYMENTMETHODCODEGID() {
            return paymentmethodcodegid;
        }

        /**
         * Sets the value of the paymentmethodcodegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPAYMENTMETHODCODEGID(String value) {
            this.paymentmethodcodegid = value;
        }

        /**
         * Gets the value of the ismasteroverridesbase property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISMASTEROVERRIDESBASE() {
            return ismasteroverridesbase;
        }

        /**
         * Sets the value of the ismasteroverridesbase property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISMASTEROVERRIDESBASE(String value) {
            this.ismasteroverridesbase = value;
        }

        /**
         * Gets the value of the hazardousratetype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHAZARDOUSRATETYPE() {
            return hazardousratetype;
        }

        /**
         * Sets the value of the hazardousratetype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHAZARDOUSRATETYPE(String value) {
            this.hazardousratetype = value;
        }

        /**
         * Gets the value of the domainprofilegid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDOMAINPROFILEGID() {
            return domainprofilegid;
        }

        /**
         * Sets the value of the domainprofilegid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDOMAINPROFILEGID(String value) {
            this.domainprofilegid = value;
        }

        /**
         * Gets the value of the isquote property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getISQUOTE() {
            return isquote;
        }

        /**
         * Sets the value of the isquote property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setISQUOTE(String value) {
            this.isquote = value;
        }

        /**
         * Gets the value of the rotimeperioddefgid property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getROTIMEPERIODDEFGID() {
            return rotimeperioddefgid;
        }

        /**
         * Sets the value of the rotimeperioddefgid property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setROTIMEPERIODDEFGID(String value) {
            this.rotimeperioddefgid = value;
        }

        /**
         * Gets the value of the insertuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTUSER() {
            return insertuser;
        }

        /**
         * Sets the value of the insertuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTUSER(String value) {
            this.insertuser = value;
        }

        /**
         * Gets the value of the insertdate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINSERTDATE() {
            return insertdate;
        }

        /**
         * Sets the value of the insertdate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINSERTDATE(String value) {
            this.insertdate = value;
        }

        /**
         * Gets the value of the updateuser property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEUSER() {
            return updateuser;
        }

        /**
         * Sets the value of the updateuser property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEUSER(String value) {
            this.updateuser = value;
        }

        /**
         * Gets the value of the updatedate property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUPDATEDATE() {
            return updatedate;
        }

        /**
         * Sets the value of the updatedate property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUPDATEDATE(String value) {
            this.updatedate = value;
        }

        /**
         * Gets the value of the rategeostops property.
         * 
         * @return
         *     possible object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOSTOPS }
         *     
         */
        public RateGeoType.RATEGEOROW.RATEGEOSTOPS getRATEGEOSTOPS() {
            return rategeostops;
        }

        /**
         * Sets the value of the rategeostops property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOSTOPS }
         *     
         */
        public void setRATEGEOSTOPS(RateGeoType.RATEGEOROW.RATEGEOSTOPS value) {
            this.rategeostops = value;
        }

        /**
         * Gets the value of the rategeoaccessorial property.
         * 
         * @return
         *     possible object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL }
         *     
         */
        public RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL getRATEGEOACCESSORIAL() {
            return rategeoaccessorial;
        }

        /**
         * Sets the value of the rategeoaccessorial property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL }
         *     
         */
        public void setRATEGEOACCESSORIAL(RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL value) {
            this.rategeoaccessorial = value;
        }

        /**
         * Gets the value of the rategeocostgroup property.
         * 
         * @return
         *     possible object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP }
         *     
         */
        public RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP getRATEGEOCOSTGROUP() {
            return rategeocostgroup;
        }

        /**
         * Sets the value of the rategeocostgroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP }
         *     
         */
        public void setRATEGEOCOSTGROUP(RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP value) {
            this.rategeocostgroup = value;
        }

        /**
         * Gets the value of the rgspecialserviceaccessorial property.
         * 
         * @return
         *     possible object is
         *     {@link RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL }
         *     
         */
        public RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL getRGSPECIALSERVICEACCESSORIAL() {
            return rgspecialserviceaccessorial;
        }

        /**
         * Sets the value of the rgspecialserviceaccessorial property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL }
         *     
         */
        public void setRGSPECIALSERVICEACCESSORIAL(RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL value) {
            this.rgspecialserviceaccessorial = value;
        }

        /**
         * Gets the value of the xlane property.
         * 
         * @return
         *     possible object is
         *     {@link XLANE }
         *     
         */
        public XLANE getXLANE() {
            return xlane;
        }

        /**
         * Sets the value of the xlane property.
         * 
         * @param value
         *     allowed object is
         *     {@link XLANE }
         *     
         */
        public void setXLANE(XLANE value) {
            this.xlane = value;
        }

        /**
         * Gets the value of the attribute1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE1() {
            return attribute1;
        }

        /**
         * Sets the value of the attribute1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE1(String value) {
            this.attribute1 = value;
        }

        /**
         * Gets the value of the attribute2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE2() {
            return attribute2;
        }

        /**
         * Sets the value of the attribute2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE2(String value) {
            this.attribute2 = value;
        }

        /**
         * Gets the value of the attribute3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE3() {
            return attribute3;
        }

        /**
         * Sets the value of the attribute3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE3(String value) {
            this.attribute3 = value;
        }

        /**
         * Gets the value of the attribute4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE4() {
            return attribute4;
        }

        /**
         * Sets the value of the attribute4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE4(String value) {
            this.attribute4 = value;
        }

        /**
         * Gets the value of the attribute5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE5() {
            return attribute5;
        }

        /**
         * Sets the value of the attribute5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE5(String value) {
            this.attribute5 = value;
        }

        /**
         * Gets the value of the attribute6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE6() {
            return attribute6;
        }

        /**
         * Sets the value of the attribute6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE6(String value) {
            this.attribute6 = value;
        }

        /**
         * Gets the value of the attribute7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE7() {
            return attribute7;
        }

        /**
         * Sets the value of the attribute7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE7(String value) {
            this.attribute7 = value;
        }

        /**
         * Gets the value of the attribute8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE8() {
            return attribute8;
        }

        /**
         * Sets the value of the attribute8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE8(String value) {
            this.attribute8 = value;
        }

        /**
         * Gets the value of the attribute9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE9() {
            return attribute9;
        }

        /**
         * Sets the value of the attribute9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE9(String value) {
            this.attribute9 = value;
        }

        /**
         * Gets the value of the attribute10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE10() {
            return attribute10;
        }

        /**
         * Sets the value of the attribute10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE10(String value) {
            this.attribute10 = value;
        }

        /**
         * Gets the value of the attribute11 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE11() {
            return attribute11;
        }

        /**
         * Sets the value of the attribute11 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE11(String value) {
            this.attribute11 = value;
        }

        /**
         * Gets the value of the attribute12 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE12() {
            return attribute12;
        }

        /**
         * Sets the value of the attribute12 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE12(String value) {
            this.attribute12 = value;
        }

        /**
         * Gets the value of the attribute13 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE13() {
            return attribute13;
        }

        /**
         * Sets the value of the attribute13 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE13(String value) {
            this.attribute13 = value;
        }

        /**
         * Gets the value of the attribute14 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE14() {
            return attribute14;
        }

        /**
         * Sets the value of the attribute14 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE14(String value) {
            this.attribute14 = value;
        }

        /**
         * Gets the value of the attribute15 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE15() {
            return attribute15;
        }

        /**
         * Sets the value of the attribute15 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE15(String value) {
            this.attribute15 = value;
        }

        /**
         * Gets the value of the attribute16 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE16() {
            return attribute16;
        }

        /**
         * Sets the value of the attribute16 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE16(String value) {
            this.attribute16 = value;
        }

        /**
         * Gets the value of the attribute17 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE17() {
            return attribute17;
        }

        /**
         * Sets the value of the attribute17 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE17(String value) {
            this.attribute17 = value;
        }

        /**
         * Gets the value of the attribute18 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE18() {
            return attribute18;
        }

        /**
         * Sets the value of the attribute18 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE18(String value) {
            this.attribute18 = value;
        }

        /**
         * Gets the value of the attribute19 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE19() {
            return attribute19;
        }

        /**
         * Sets the value of the attribute19 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE19(String value) {
            this.attribute19 = value;
        }

        /**
         * Gets the value of the attribute20 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTE20() {
            return attribute20;
        }

        /**
         * Sets the value of the attribute20 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTE20(String value) {
            this.attribute20 = value;
        }

        /**
         * Gets the value of the attributenumber1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER1() {
            return attributenumber1;
        }

        /**
         * Sets the value of the attributenumber1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER1(String value) {
            this.attributenumber1 = value;
        }

        /**
         * Gets the value of the attributenumber2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER2() {
            return attributenumber2;
        }

        /**
         * Sets the value of the attributenumber2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER2(String value) {
            this.attributenumber2 = value;
        }

        /**
         * Gets the value of the attributenumber3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER3() {
            return attributenumber3;
        }

        /**
         * Sets the value of the attributenumber3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER3(String value) {
            this.attributenumber3 = value;
        }

        /**
         * Gets the value of the attributenumber4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER4() {
            return attributenumber4;
        }

        /**
         * Sets the value of the attributenumber4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER4(String value) {
            this.attributenumber4 = value;
        }

        /**
         * Gets the value of the attributenumber5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER5() {
            return attributenumber5;
        }

        /**
         * Sets the value of the attributenumber5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER5(String value) {
            this.attributenumber5 = value;
        }

        /**
         * Gets the value of the attributenumber6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER6() {
            return attributenumber6;
        }

        /**
         * Sets the value of the attributenumber6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER6(String value) {
            this.attributenumber6 = value;
        }

        /**
         * Gets the value of the attributenumber7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER7() {
            return attributenumber7;
        }

        /**
         * Sets the value of the attributenumber7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER7(String value) {
            this.attributenumber7 = value;
        }

        /**
         * Gets the value of the attributenumber8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER8() {
            return attributenumber8;
        }

        /**
         * Sets the value of the attributenumber8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER8(String value) {
            this.attributenumber8 = value;
        }

        /**
         * Gets the value of the attributenumber9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER9() {
            return attributenumber9;
        }

        /**
         * Sets the value of the attributenumber9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER9(String value) {
            this.attributenumber9 = value;
        }

        /**
         * Gets the value of the attributenumber10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTENUMBER10() {
            return attributenumber10;
        }

        /**
         * Sets the value of the attributenumber10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTENUMBER10(String value) {
            this.attributenumber10 = value;
        }

        /**
         * Gets the value of the attributedate1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE1() {
            return attributedate1;
        }

        /**
         * Sets the value of the attributedate1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE1(String value) {
            this.attributedate1 = value;
        }

        /**
         * Gets the value of the attributedate2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE2() {
            return attributedate2;
        }

        /**
         * Sets the value of the attributedate2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE2(String value) {
            this.attributedate2 = value;
        }

        /**
         * Gets the value of the attributedate3 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE3() {
            return attributedate3;
        }

        /**
         * Sets the value of the attributedate3 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE3(String value) {
            this.attributedate3 = value;
        }

        /**
         * Gets the value of the attributedate4 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE4() {
            return attributedate4;
        }

        /**
         * Sets the value of the attributedate4 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE4(String value) {
            this.attributedate4 = value;
        }

        /**
         * Gets the value of the attributedate5 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE5() {
            return attributedate5;
        }

        /**
         * Sets the value of the attributedate5 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE5(String value) {
            this.attributedate5 = value;
        }

        /**
         * Gets the value of the attributedate6 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE6() {
            return attributedate6;
        }

        /**
         * Sets the value of the attributedate6 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE6(String value) {
            this.attributedate6 = value;
        }

        /**
         * Gets the value of the attributedate7 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE7() {
            return attributedate7;
        }

        /**
         * Sets the value of the attributedate7 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE7(String value) {
            this.attributedate7 = value;
        }

        /**
         * Gets the value of the attributedate8 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE8() {
            return attributedate8;
        }

        /**
         * Sets the value of the attributedate8 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE8(String value) {
            this.attributedate8 = value;
        }

        /**
         * Gets the value of the attributedate9 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE9() {
            return attributedate9;
        }

        /**
         * Sets the value of the attributedate9 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE9(String value) {
            this.attributedate9 = value;
        }

        /**
         * Gets the value of the attributedate10 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getATTRIBUTEDATE10() {
            return attributedate10;
        }

        /**
         * Sets the value of the attributedate10 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setATTRIBUTEDATE10(String value) {
            this.attributedate10 = value;
        }

        /**
         * Gets the value of the rategeodesc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRATEGEODESC() {
            return rategeodesc;
        }

        /**
         * Sets the value of the rategeodesc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRATEGEODESC(String value) {
            this.rategeodesc = value;
        }

        /**
         * Gets the value of the rategeoremarks property.
         * 
         * @return
         *     possible object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOREMARKS }
         *     
         */
        public RateGeoType.RATEGEOROW.RATEGEOREMARKS getRATEGEOREMARKS() {
            return rategeoremarks;
        }

        /**
         * Sets the value of the rategeoremarks property.
         * 
         * @param value
         *     allowed object is
         *     {@link RateGeoType.RATEGEOROW.RATEGEOREMARKS }
         *     
         */
        public void setRATEGEOREMARKS(RateGeoType.RATEGEOROW.RATEGEOREMARKS value) {
            this.rategeoremarks = value;
        }

        /**
         * Gets the value of the num property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNum() {
            return num;
        }

        /**
         * Sets the value of the num property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNum(String value) {
            this.num = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="RATE_GEO_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeoaccessorialrow"
        })
        public static class RATEGEOACCESSORIAL {

            @XmlElement(name = "RATE_GEO_ACCESSORIAL_ROW")
            protected List<RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL.RATEGEOACCESSORIALROW> rategeoaccessorialrow;

            /**
             * Gets the value of the rategeoaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rategeoaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL.RATEGEOACCESSORIALROW }
             * 
             * 
             */
            public List<RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL.RATEGEOACCESSORIALROW> getRATEGEOACCESSORIALROW() {
                if (rategeoaccessorialrow == null) {
                    rategeoaccessorialrow = new ArrayList<RateGeoType.RATEGEOROW.RATEGEOACCESSORIAL.RATEGEOACCESSORIALROW>();
                }
                return this.rategeoaccessorialrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rategeogid",
                "accessorialcodegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class RATEGEOACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_GEO_GID", required = true)
                protected String rategeogid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the accessorialcostgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Sets the value of the accessorialcostgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Gets the value of the rategeogid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Sets the value of the rategeogid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Gets the value of the accessorialcodegid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Sets the value of the accessorialcodegid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the accessorialcost property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Sets the value of the accessorialcost property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
         *         &lt;element name="RATE_GEO_COST_GROUP_ROW" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeocostgrouprow"
        })
        public static class RATEGEOCOSTGROUP {

            @XmlElement(name = "RATE_GEO_COST_GROUP_ROW")
            protected List<RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP.RATEGEOCOSTGROUPROW> rategeocostgrouprow;

            /**
             * Gets the value of the rategeocostgrouprow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rategeocostgrouprow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOCOSTGROUPROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP.RATEGEOCOSTGROUPROW }
             * 
             * 
             */
            public List<RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP.RATEGEOCOSTGROUPROW> getRATEGEOCOSTGROUPROW() {
                if (rategeocostgrouprow == null) {
                    rategeocostgrouprow = new ArrayList<RateGeoType.RATEGEOROW.RATEGEOCOSTGROUP.RATEGEOCOSTGROUPROW>();
                }
                return this.rategeocostgrouprow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="RATE_GEO_COST_GROUP_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RATE_GEO_COST_GROUP_XID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RATE_GEO_COST_GROUP_SEQ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="GROUP_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="USE_DEFICIT_CALCULATIONS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="MULTI_RATES_RULE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RATE_GROUP_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ROUNDING_TYPE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ROUNDING_INTERVAL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ROUNDING_FIELDS_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ROUNDING_APPLICATION" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="RATE_GEO_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RATE_GEO_COST_TYPE" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rategeocostgroupgid",
                "rategeocostgroupxid",
                "rategeogid",
                "rategeocostgroupseq",
                "groupname",
                "usedeficitcalculations",
                "multiratesrule",
                "rategrouptype",
                "roundingtype",
                "roundinginterval",
                "roundingfieldslevel",
                "roundingapplication",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "rategeocost"
            })
            public static class RATEGEOCOSTGROUPROW {

                @XmlElement(name = "RATE_GEO_COST_GROUP_GID")
                protected String rategeocostgroupgid;
                @XmlElement(name = "RATE_GEO_COST_GROUP_XID")
                protected String rategeocostgroupxid;
                @XmlElement(name = "RATE_GEO_GID")
                protected String rategeogid;
                @XmlElement(name = "RATE_GEO_COST_GROUP_SEQ")
                protected String rategeocostgroupseq;
                @XmlElement(name = "GROUP_NAME")
                protected String groupname;
                @XmlElement(name = "USE_DEFICIT_CALCULATIONS")
                protected String usedeficitcalculations;
                @XmlElement(name = "MULTI_RATES_RULE")
                protected String multiratesrule;
                @XmlElement(name = "RATE_GROUP_TYPE")
                protected String rategrouptype;
                @XmlElement(name = "ROUNDING_TYPE")
                protected String roundingtype;
                @XmlElement(name = "ROUNDING_INTERVAL")
                protected String roundinginterval;
                @XmlElement(name = "ROUNDING_FIELDS_LEVEL")
                protected String roundingfieldslevel;
                @XmlElement(name = "ROUNDING_APPLICATION")
                protected String roundingapplication;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "RATE_GEO_COST")
                protected RATEGEOCOSTTYPE rategeocost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the rategeocostgroupgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOCOSTGROUPGID() {
                    return rategeocostgroupgid;
                }

                /**
                 * Sets the value of the rategeocostgroupgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOCOSTGROUPGID(String value) {
                    this.rategeocostgroupgid = value;
                }

                /**
                 * Gets the value of the rategeocostgroupxid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOCOSTGROUPXID() {
                    return rategeocostgroupxid;
                }

                /**
                 * Sets the value of the rategeocostgroupxid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOCOSTGROUPXID(String value) {
                    this.rategeocostgroupxid = value;
                }

                /**
                 * Gets the value of the rategeogid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Sets the value of the rategeogid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Gets the value of the rategeocostgroupseq property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOCOSTGROUPSEQ() {
                    return rategeocostgroupseq;
                }

                /**
                 * Sets the value of the rategeocostgroupseq property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOCOSTGROUPSEQ(String value) {
                    this.rategeocostgroupseq = value;
                }

                /**
                 * Gets the value of the groupname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getGROUPNAME() {
                    return groupname;
                }

                /**
                 * Sets the value of the groupname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setGROUPNAME(String value) {
                    this.groupname = value;
                }

                /**
                 * Gets the value of the usedeficitcalculations property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUSEDEFICITCALCULATIONS() {
                    return usedeficitcalculations;
                }

                /**
                 * Sets the value of the usedeficitcalculations property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUSEDEFICITCALCULATIONS(String value) {
                    this.usedeficitcalculations = value;
                }

                /**
                 * Gets the value of the multiratesrule property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getMULTIRATESRULE() {
                    return multiratesrule;
                }

                /**
                 * Sets the value of the multiratesrule property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setMULTIRATESRULE(String value) {
                    this.multiratesrule = value;
                }

                /**
                 * Gets the value of the rategrouptype property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGROUPTYPE() {
                    return rategrouptype;
                }

                /**
                 * Sets the value of the rategrouptype property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGROUPTYPE(String value) {
                    this.rategrouptype = value;
                }

                /**
                 * Gets the value of the roundingtype property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGTYPE() {
                    return roundingtype;
                }

                /**
                 * Sets the value of the roundingtype property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGTYPE(String value) {
                    this.roundingtype = value;
                }

                /**
                 * Gets the value of the roundinginterval property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGINTERVAL() {
                    return roundinginterval;
                }

                /**
                 * Sets the value of the roundinginterval property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGINTERVAL(String value) {
                    this.roundinginterval = value;
                }

                /**
                 * Gets the value of the roundingfieldslevel property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGFIELDSLEVEL() {
                    return roundingfieldslevel;
                }

                /**
                 * Sets the value of the roundingfieldslevel property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGFIELDSLEVEL(String value) {
                    this.roundingfieldslevel = value;
                }

                /**
                 * Gets the value of the roundingapplication property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getROUNDINGAPPLICATION() {
                    return roundingapplication;
                }

                /**
                 * Sets the value of the roundingapplication property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setROUNDINGAPPLICATION(String value) {
                    this.roundingapplication = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the rategeocost property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RATEGEOCOSTTYPE }
                 *     
                 */
                public RATEGEOCOSTTYPE getRATEGEOCOST() {
                    return rategeocost;
                }

                /**
                 * Sets the value of the rategeocost property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RATEGEOCOSTTYPE }
                 *     
                 */
                public void setRATEGEOCOST(RATEGEOCOSTTYPE value) {
                    this.rategeocost = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="RATE_GEO_REMARKS_ROW" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeoremarksrow"
        })
        public static class RATEGEOREMARKS {

            @XmlElement(name = "RATE_GEO_REMARKS_ROW", required = true)
            protected List<RateGeoType.RATEGEOROW.RATEGEOREMARKS.RATEGEOREMARKSROW> rategeoremarksrow;

            /**
             * Gets the value of the rategeoremarksrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rategeoremarksrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOREMARKSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateGeoType.RATEGEOROW.RATEGEOREMARKS.RATEGEOREMARKSROW }
             * 
             * 
             */
            public List<RateGeoType.RATEGEOROW.RATEGEOREMARKS.RATEGEOREMARKSROW> getRATEGEOREMARKSROW() {
                if (rategeoremarksrow == null) {
                    rategeoremarksrow = new ArrayList<RateGeoType.RATEGEOROW.RATEGEOREMARKS.RATEGEOREMARKSROW>();
                }
                return this.rategeoremarksrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="TRANSACTION_CODE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="REMARK_SEQUENCE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="REMARK_QUALIFIER_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="REMARK_LEVEL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="REMARK_TEXT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "transactioncode",
                "remarksequence",
                "remarkqualifiergid",
                "remarklevel",
                "remarktext",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEGEOREMARKSROW {

                @XmlElement(name = "TRANSACTION_CODE")
                protected String transactioncode;
                @XmlElement(name = "REMARK_SEQUENCE")
                protected String remarksequence;
                @XmlElement(name = "REMARK_QUALIFIER_GID")
                protected String remarkqualifiergid;
                @XmlElement(name = "REMARK_LEVEL")
                protected String remarklevel;
                @XmlElement(name = "REMARK_TEXT")
                protected String remarktext;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;

                /**
                 * Gets the value of the transactioncode property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getTRANSACTIONCODE() {
                    return transactioncode;
                }

                /**
                 * Sets the value of the transactioncode property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setTRANSACTIONCODE(String value) {
                    this.transactioncode = value;
                }

                /**
                 * Gets the value of the remarksequence property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKSEQUENCE() {
                    return remarksequence;
                }

                /**
                 * Sets the value of the remarksequence property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKSEQUENCE(String value) {
                    this.remarksequence = value;
                }

                /**
                 * Gets the value of the remarkqualifiergid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKQUALIFIERGID() {
                    return remarkqualifiergid;
                }

                /**
                 * Sets the value of the remarkqualifiergid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKQUALIFIERGID(String value) {
                    this.remarkqualifiergid = value;
                }

                /**
                 * Gets the value of the remarklevel property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKLEVEL() {
                    return remarklevel;
                }

                /**
                 * Sets the value of the remarklevel property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKLEVEL(String value) {
                    this.remarklevel = value;
                }

                /**
                 * Gets the value of the remarktext property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getREMARKTEXT() {
                    return remarktext;
                }

                /**
                 * Sets the value of the remarktext property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setREMARKTEXT(String value) {
                    this.remarktext = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence maxOccurs="unbounded" minOccurs="0"&gt;
         *         &lt;element name="RATE_GEO_STOPS_ROW" maxOccurs="unbounded"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rategeostopsrow"
        })
        public static class RATEGEOSTOPS {

            @XmlElement(name = "RATE_GEO_STOPS_ROW")
            protected List<RateGeoType.RATEGEOROW.RATEGEOSTOPS.RATEGEOSTOPSROW> rategeostopsrow;

            /**
             * Gets the value of the rategeostopsrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rategeostopsrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRATEGEOSTOPSROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateGeoType.RATEGEOROW.RATEGEOSTOPS.RATEGEOSTOPSROW }
             * 
             * 
             */
            public List<RateGeoType.RATEGEOROW.RATEGEOSTOPS.RATEGEOSTOPSROW> getRATEGEOSTOPSROW() {
                if (rategeostopsrow == null) {
                    rategeostopsrow = new ArrayList<RateGeoType.RATEGEOROW.RATEGEOSTOPS.RATEGEOSTOPSROW>();
                }
                return this.rategeostopsrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="LOW_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="HIGH_STOP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="PER_STOP_COST" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="PER_STOP_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="PER_STOP_COST_BASE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "rategeogid",
                "lowstop",
                "highstop",
                "perstopcost",
                "perstopcostgid",
                "perstopcostbase",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate"
            })
            public static class RATEGEOSTOPSROW {

                @XmlElement(name = "RATE_GEO_GID")
                protected String rategeogid;
                @XmlElement(name = "LOW_STOP")
                protected String lowstop;
                @XmlElement(name = "HIGH_STOP")
                protected String highstop;
                @XmlElement(name = "PER_STOP_COST")
                protected String perstopcost;
                @XmlElement(name = "PER_STOP_COST_GID")
                protected String perstopcostgid;
                @XmlElement(name = "PER_STOP_COST_BASE")
                protected String perstopcostbase;
                @XmlElement(name = "DOMAIN_NAME")
                protected String domainname;
                @XmlElement(name = "INSERT_USER")
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE")
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the rategeogid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Sets the value of the rategeogid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Gets the value of the lowstop property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLOWSTOP() {
                    return lowstop;
                }

                /**
                 * Sets the value of the lowstop property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLOWSTOP(String value) {
                    this.lowstop = value;
                }

                /**
                 * Gets the value of the highstop property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getHIGHSTOP() {
                    return highstop;
                }

                /**
                 * Sets the value of the highstop property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setHIGHSTOP(String value) {
                    this.highstop = value;
                }

                /**
                 * Gets the value of the perstopcost property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOST() {
                    return perstopcost;
                }

                /**
                 * Sets the value of the perstopcost property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOST(String value) {
                    this.perstopcost = value;
                }

                /**
                 * Gets the value of the perstopcostgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTGID() {
                    return perstopcostgid;
                }

                /**
                 * Sets the value of the perstopcostgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTGID(String value) {
                    this.perstopcostgid = value;
                }

                /**
                 * Gets the value of the perstopcostbase property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getPERSTOPCOSTBASE() {
                    return perstopcostbase;
                }

                /**
                 * Sets the value of the perstopcostbase property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setPERSTOPCOSTBASE(String value) {
                    this.perstopcostbase = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="RG_SPECIAL_SERVICE_ACCESSORIAL_ROW" maxOccurs="unbounded" minOccurs="0"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *                   &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *                   &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
         *                 &lt;/sequence&gt;
         *                 &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "rgspecialserviceaccessorialrow"
        })
        public static class RGSPECIALSERVICEACCESSORIAL {

            @XmlElement(name = "RG_SPECIAL_SERVICE_ACCESSORIAL_ROW")
            protected List<RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL.RGSPECIALSERVICEACCESSORIALROW> rgspecialserviceaccessorialrow;

            /**
             * Gets the value of the rgspecialserviceaccessorialrow property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the Jakarta XML Binding object.
             * This is why there is not a <CODE>set</CODE> method for the rgspecialserviceaccessorialrow property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getRGSPECIALSERVICEACCESSORIALROW().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL.RGSPECIALSERVICEACCESSORIALROW }
             * 
             * 
             */
            public List<RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL.RGSPECIALSERVICEACCESSORIALROW> getRGSPECIALSERVICEACCESSORIALROW() {
                if (rgspecialserviceaccessorialrow == null) {
                    rgspecialserviceaccessorialrow = new ArrayList<RateGeoType.RATEGEOROW.RGSPECIALSERVICEACCESSORIAL.RGSPECIALSERVICEACCESSORIALROW>();
                }
                return this.rgspecialserviceaccessorialrow;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="ACCESSORIAL_COST_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="RATE_GEO_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="ACCESSORIAL_CODE_GID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="DOMAIN_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_USER" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="INSERT_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
             *         &lt;element name="UPDATE_USER" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="UPDATE_DATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
             *         &lt;element name="ACCESSORIAL_COST" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ACCESSORIAL_COST_TYPE" minOccurs="0"/&gt;
             *       &lt;/sequence&gt;
             *       &lt;attribute name="num" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "accessorialcostgid",
                "rategeogid",
                "accessorialcodegid",
                "domainname",
                "insertuser",
                "insertdate",
                "updateuser",
                "updatedate",
                "accessorialcost"
            })
            public static class RGSPECIALSERVICEACCESSORIALROW {

                @XmlElement(name = "ACCESSORIAL_COST_GID", required = true)
                protected String accessorialcostgid;
                @XmlElement(name = "RATE_GEO_GID", required = true)
                protected String rategeogid;
                @XmlElement(name = "ACCESSORIAL_CODE_GID", required = true)
                protected String accessorialcodegid;
                @XmlElement(name = "DOMAIN_NAME", required = true)
                protected String domainname;
                @XmlElement(name = "INSERT_USER", required = true)
                protected String insertuser;
                @XmlElement(name = "INSERT_DATE", required = true)
                protected String insertdate;
                @XmlElement(name = "UPDATE_USER")
                protected String updateuser;
                @XmlElement(name = "UPDATE_DATE")
                protected String updatedate;
                @XmlElement(name = "ACCESSORIAL_COST")
                protected ACCESSORIALCOSTTYPE accessorialcost;
                @XmlAttribute(name = "num")
                protected String num;

                /**
                 * Gets the value of the accessorialcostgid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCOSTGID() {
                    return accessorialcostgid;
                }

                /**
                 * Sets the value of the accessorialcostgid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCOSTGID(String value) {
                    this.accessorialcostgid = value;
                }

                /**
                 * Gets the value of the rategeogid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getRATEGEOGID() {
                    return rategeogid;
                }

                /**
                 * Sets the value of the rategeogid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setRATEGEOGID(String value) {
                    this.rategeogid = value;
                }

                /**
                 * Gets the value of the accessorialcodegid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getACCESSORIALCODEGID() {
                    return accessorialcodegid;
                }

                /**
                 * Sets the value of the accessorialcodegid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setACCESSORIALCODEGID(String value) {
                    this.accessorialcodegid = value;
                }

                /**
                 * Gets the value of the domainname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDOMAINNAME() {
                    return domainname;
                }

                /**
                 * Sets the value of the domainname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDOMAINNAME(String value) {
                    this.domainname = value;
                }

                /**
                 * Gets the value of the insertuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTUSER() {
                    return insertuser;
                }

                /**
                 * Sets the value of the insertuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTUSER(String value) {
                    this.insertuser = value;
                }

                /**
                 * Gets the value of the insertdate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getINSERTDATE() {
                    return insertdate;
                }

                /**
                 * Sets the value of the insertdate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setINSERTDATE(String value) {
                    this.insertdate = value;
                }

                /**
                 * Gets the value of the updateuser property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEUSER() {
                    return updateuser;
                }

                /**
                 * Sets the value of the updateuser property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEUSER(String value) {
                    this.updateuser = value;
                }

                /**
                 * Gets the value of the updatedate property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUPDATEDATE() {
                    return updatedate;
                }

                /**
                 * Sets the value of the updatedate property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUPDATEDATE(String value) {
                    this.updatedate = value;
                }

                /**
                 * Gets the value of the accessorialcost property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public ACCESSORIALCOSTTYPE getACCESSORIALCOST() {
                    return accessorialcost;
                }

                /**
                 * Sets the value of the accessorialcost property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link ACCESSORIALCOSTTYPE }
                 *     
                 */
                public void setACCESSORIALCOST(ACCESSORIALCOSTTYPE value) {
                    this.accessorialcost = value;
                }

                /**
                 * Gets the value of the num property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNum() {
                    return num;
                }

                /**
                 * Sets the value of the num property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNum(String value) {
                    this.num = value;
                }

            }

        }

    }

}

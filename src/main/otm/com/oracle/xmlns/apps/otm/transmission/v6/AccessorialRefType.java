
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the relationship between the Order Release Lines and the Shipment Accessorial.
 *             When used in the context of ShipmentHeader, maps to the shipment_accessorial_ref table.
 *          
 * 
 * <p>Java class for AccessorialRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccessorialRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CostReferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CostQualGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessorialRefType", propOrder = {
    "costReferenceGid",
    "costQualGid"
})
public class AccessorialRefType {

    @XmlElement(name = "CostReferenceGid", required = true)
    protected GLogXMLGidType costReferenceGid;
    @XmlElement(name = "CostQualGid", required = true)
    protected GLogXMLGidType costQualGid;

    /**
     * Gets the value of the costReferenceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostReferenceGid() {
        return costReferenceGid;
    }

    /**
     * Sets the value of the costReferenceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostReferenceGid(GLogXMLGidType value) {
        this.costReferenceGid = value;
    }

    /**
     * Gets the value of the costQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostQualGid() {
        return costQualGid;
    }

    /**
     * Sets the value of the costQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostQualGid(GLogXMLGidType value) {
        this.costQualGid = value;
    }

}

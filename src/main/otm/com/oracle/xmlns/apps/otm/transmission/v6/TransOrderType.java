
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             A TransOrder consists of header level information and detail information.
 *             The detail information consists of either line items, or ship units, but not both.
 *          
 * 
 * <p>Java class for TransOrderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransOrderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransOrderHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderHeaderType"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="TransOrderLineDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransOrderLineDetailType" minOccurs="0"/&gt;
 *           &lt;element name="ShipUnitDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitDetailType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransOrderType", propOrder = {
    "transOrderHeader",
    "transOrderLineDetail",
    "shipUnitDetail",
    "location",
    "packagedItem"
})
public class TransOrderType
    extends OTMTransactionInOut
{

    @XmlElement(name = "TransOrderHeader", required = true)
    protected TransOrderHeaderType transOrderHeader;
    @XmlElement(name = "TransOrderLineDetail")
    protected TransOrderLineDetailType transOrderLineDetail;
    @XmlElement(name = "ShipUnitDetail")
    protected ShipUnitDetailType shipUnitDetail;
    @XmlElement(name = "Location")
    protected List<LocationType> location;
    @XmlElement(name = "PackagedItem")
    protected List<PackagedItemType> packagedItem;

    /**
     * Gets the value of the transOrderHeader property.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderHeaderType }
     *     
     */
    public TransOrderHeaderType getTransOrderHeader() {
        return transOrderHeader;
    }

    /**
     * Sets the value of the transOrderHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderHeaderType }
     *     
     */
    public void setTransOrderHeader(TransOrderHeaderType value) {
        this.transOrderHeader = value;
    }

    /**
     * Gets the value of the transOrderLineDetail property.
     * 
     * @return
     *     possible object is
     *     {@link TransOrderLineDetailType }
     *     
     */
    public TransOrderLineDetailType getTransOrderLineDetail() {
        return transOrderLineDetail;
    }

    /**
     * Sets the value of the transOrderLineDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransOrderLineDetailType }
     *     
     */
    public void setTransOrderLineDetail(TransOrderLineDetailType value) {
        this.transOrderLineDetail = value;
    }

    /**
     * Gets the value of the shipUnitDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitDetailType }
     *     
     */
    public ShipUnitDetailType getShipUnitDetail() {
        return shipUnitDetail;
    }

    /**
     * Sets the value of the shipUnitDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitDetailType }
     *     
     */
    public void setShipUnitDetail(ShipUnitDetailType value) {
        this.shipUnitDetail = value;
    }

    /**
     * Gets the value of the location property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the location property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationType }
     * 
     * 
     */
    public List<LocationType> getLocation() {
        if (location == null) {
            location = new ArrayList<LocationType>();
        }
        return this.location;
    }

    /**
     * Gets the value of the packagedItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the packagedItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackagedItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PackagedItemType }
     * 
     * 
     */
    public List<PackagedItemType> getPackagedItem() {
        if (packagedItem == null) {
            packagedItem = new ArrayList<PackagedItemType>();
        }
        return this.packagedItem;
    }

}

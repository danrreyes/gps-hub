
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An EventTime consists of a PlannedTime, an ActualTime, and a IsPlannedTimeFixed flag,
 *             indicating if the PlannedTime cannot be changed.
 *          
 * 
 * <p>Java class for EventTimeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EventTimeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PlannedTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EstimatedTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ActualTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="IsPlannedTimeFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventTimeType", propOrder = {
    "plannedTime",
    "estimatedTime",
    "actualTime",
    "isPlannedTimeFixed"
})
public class EventTimeType {

    @XmlElement(name = "PlannedTime")
    protected GLogDateTimeType plannedTime;
    @XmlElement(name = "EstimatedTime")
    protected GLogDateTimeType estimatedTime;
    @XmlElement(name = "ActualTime")
    protected GLogDateTimeType actualTime;
    @XmlElement(name = "IsPlannedTimeFixed")
    protected String isPlannedTimeFixed;

    /**
     * Gets the value of the plannedTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPlannedTime() {
        return plannedTime;
    }

    /**
     * Sets the value of the plannedTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPlannedTime(GLogDateTimeType value) {
        this.plannedTime = value;
    }

    /**
     * Gets the value of the estimatedTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEstimatedTime() {
        return estimatedTime;
    }

    /**
     * Sets the value of the estimatedTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEstimatedTime(GLogDateTimeType value) {
        this.estimatedTime = value;
    }

    /**
     * Gets the value of the actualTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getActualTime() {
        return actualTime;
    }

    /**
     * Sets the value of the actualTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setActualTime(GLogDateTimeType value) {
        this.actualTime = value;
    }

    /**
     * Gets the value of the isPlannedTimeFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPlannedTimeFixed() {
        return isPlannedTimeFixed;
    }

    /**
     * Sets the value of the isPlannedTimeFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPlannedTimeFixed(String value) {
        this.isPlannedTimeFixed = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Equipment is a structure representing the equipment assigned to a shipment.
 * 
 * <p>Java class for EquipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EquipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSealType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentAttribute" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentAttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OwnershipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OwnerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AARCarType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WeightQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScaleWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="ScaleLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScaleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScaleTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IntermodalEquipLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LesseeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LicenseState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LicensePlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTag2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTag4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DateBuilt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PurchasedDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentRegistrationNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentRegistrationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="SightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PrevSightingLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PrevSightingDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="InterchangeRecvLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InterchangeRecvDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ParkLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsContainer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChassisInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChassisNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastOutgateLocGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LastOutgateTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LastIngateTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="UserDefIconInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserDefIconInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EquipmentType", propOrder = {
    "intSavedQuery",
    "equipmentGid",
    "transactionCode",
    "replaceChildren",
    "equipmentName",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentTypeGid",
    "equipmentGroupGid",
    "equipmentSeal",
    "equipmentAttribute",
    "ownershipCode",
    "equipmentOwner",
    "ownerType",
    "aarCarType",
    "weightQualifier",
    "scaleWeight",
    "tareWeight",
    "scaleLocation",
    "scaleName",
    "scaleTicket",
    "intermodalEquipLength",
    "lesseeGid",
    "licenseState",
    "licensePlate",
    "equipmentTag1",
    "equipmentTag2",
    "equipmentTag3",
    "equipmentTag4",
    "dateBuilt",
    "purchasedDate",
    "equipmentRegistrationNum",
    "equipmentRegistrationDate",
    "sightingLocGid",
    "sightingDate",
    "prevSightingLocGid",
    "prevSightingDate",
    "interchangeRecvLocGid",
    "interchangeRecvDate",
    "parkLocationGid",
    "indicator",
    "isContainer",
    "chassisInitial",
    "chassisNumber",
    "lastOutgateLocGid",
    "lastOutgateTime",
    "lastIngateTime",
    "userDefIconInfo",
    "refnum",
    "remark",
    "status",
    "equipmentSpecialService",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class EquipmentType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "EquipmentName")
    protected String equipmentName;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentSeal")
    protected List<EquipmentSealType> equipmentSeal;
    @XmlElement(name = "EquipmentAttribute")
    protected List<EquipmentAttributeType> equipmentAttribute;
    @XmlElement(name = "OwnershipCode")
    protected String ownershipCode;
    @XmlElement(name = "EquipmentOwner")
    protected String equipmentOwner;
    @XmlElement(name = "OwnerType")
    protected String ownerType;
    @XmlElement(name = "AARCarType")
    protected String aarCarType;
    @XmlElement(name = "WeightQualifier")
    protected String weightQualifier;
    @XmlElement(name = "ScaleWeight")
    protected GLogXMLWeightType scaleWeight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "ScaleLocation")
    protected String scaleLocation;
    @XmlElement(name = "ScaleName")
    protected String scaleName;
    @XmlElement(name = "ScaleTicket")
    protected String scaleTicket;
    @XmlElement(name = "IntermodalEquipLength")
    protected String intermodalEquipLength;
    @XmlElement(name = "LesseeGid")
    protected GLogXMLGidType lesseeGid;
    @XmlElement(name = "LicenseState")
    protected String licenseState;
    @XmlElement(name = "LicensePlate")
    protected String licensePlate;
    @XmlElement(name = "EquipmentTag1")
    protected String equipmentTag1;
    @XmlElement(name = "EquipmentTag2")
    protected String equipmentTag2;
    @XmlElement(name = "EquipmentTag3")
    protected String equipmentTag3;
    @XmlElement(name = "EquipmentTag4")
    protected String equipmentTag4;
    @XmlElement(name = "DateBuilt")
    protected GLogDateTimeType dateBuilt;
    @XmlElement(name = "PurchasedDate")
    protected GLogDateTimeType purchasedDate;
    @XmlElement(name = "EquipmentRegistrationNum")
    protected String equipmentRegistrationNum;
    @XmlElement(name = "EquipmentRegistrationDate")
    protected GLogDateTimeType equipmentRegistrationDate;
    @XmlElement(name = "SightingLocGid")
    protected GLogXMLGidType sightingLocGid;
    @XmlElement(name = "SightingDate")
    protected GLogDateTimeType sightingDate;
    @XmlElement(name = "PrevSightingLocGid")
    protected GLogXMLGidType prevSightingLocGid;
    @XmlElement(name = "PrevSightingDate")
    protected GLogDateTimeType prevSightingDate;
    @XmlElement(name = "InterchangeRecvLocGid")
    protected GLogXMLGidType interchangeRecvLocGid;
    @XmlElement(name = "InterchangeRecvDate")
    protected GLogDateTimeType interchangeRecvDate;
    @XmlElement(name = "ParkLocationGid")
    protected GLogXMLGidType parkLocationGid;
    @XmlElement(name = "Indicator")
    protected String indicator;
    @XmlElement(name = "IsContainer")
    protected String isContainer;
    @XmlElement(name = "ChassisInitial")
    protected String chassisInitial;
    @XmlElement(name = "ChassisNumber")
    protected String chassisNumber;
    @XmlElement(name = "LastOutgateLocGid")
    protected GLogXMLGidType lastOutgateLocGid;
    @XmlElement(name = "LastOutgateTime")
    protected GLogDateTimeType lastOutgateTime;
    @XmlElement(name = "LastIngateTime")
    protected GLogDateTimeType lastIngateTime;
    @XmlElement(name = "UserDefIconInfo")
    protected UserDefIconInfoType userDefIconInfo;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "EquipmentSpecialService")
    protected List<EquipmentSpecialServiceType> equipmentSpecialService;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the equipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Sets the value of the equipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the equipmentName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentName() {
        return equipmentName;
    }

    /**
     * Sets the value of the equipmentName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentName(String value) {
        this.equipmentName = value;
    }

    /**
     * Gets the value of the equipmentInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Sets the value of the equipmentInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Gets the value of the equipmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Sets the value of the equipmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the equipmentInitialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Sets the value of the equipmentInitialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Gets the value of the equipmentTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Sets the value of the equipmentTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Sets the value of the equipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Gets the value of the equipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSealType }
     * 
     * 
     */
    public List<EquipmentSealType> getEquipmentSeal() {
        if (equipmentSeal == null) {
            equipmentSeal = new ArrayList<EquipmentSealType>();
        }
        return this.equipmentSeal;
    }

    /**
     * Gets the value of the equipmentAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentAttributeType }
     * 
     * 
     */
    public List<EquipmentAttributeType> getEquipmentAttribute() {
        if (equipmentAttribute == null) {
            equipmentAttribute = new ArrayList<EquipmentAttributeType>();
        }
        return this.equipmentAttribute;
    }

    /**
     * Gets the value of the ownershipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnershipCode() {
        return ownershipCode;
    }

    /**
     * Sets the value of the ownershipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnershipCode(String value) {
        this.ownershipCode = value;
    }

    /**
     * Gets the value of the equipmentOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentOwner() {
        return equipmentOwner;
    }

    /**
     * Sets the value of the equipmentOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentOwner(String value) {
        this.equipmentOwner = value;
    }

    /**
     * Gets the value of the ownerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerType() {
        return ownerType;
    }

    /**
     * Sets the value of the ownerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerType(String value) {
        this.ownerType = value;
    }

    /**
     * Gets the value of the aarCarType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAARCarType() {
        return aarCarType;
    }

    /**
     * Sets the value of the aarCarType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAARCarType(String value) {
        this.aarCarType = value;
    }

    /**
     * Gets the value of the weightQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightQualifier() {
        return weightQualifier;
    }

    /**
     * Sets the value of the weightQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightQualifier(String value) {
        this.weightQualifier = value;
    }

    /**
     * Gets the value of the scaleWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getScaleWeight() {
        return scaleWeight;
    }

    /**
     * Sets the value of the scaleWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setScaleWeight(GLogXMLWeightType value) {
        this.scaleWeight = value;
    }

    /**
     * Gets the value of the tareWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Sets the value of the tareWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Gets the value of the scaleLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleLocation() {
        return scaleLocation;
    }

    /**
     * Sets the value of the scaleLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleLocation(String value) {
        this.scaleLocation = value;
    }

    /**
     * Gets the value of the scaleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleName() {
        return scaleName;
    }

    /**
     * Sets the value of the scaleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleName(String value) {
        this.scaleName = value;
    }

    /**
     * Gets the value of the scaleTicket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleTicket() {
        return scaleTicket;
    }

    /**
     * Sets the value of the scaleTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleTicket(String value) {
        this.scaleTicket = value;
    }

    /**
     * Gets the value of the intermodalEquipLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermodalEquipLength() {
        return intermodalEquipLength;
    }

    /**
     * Sets the value of the intermodalEquipLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermodalEquipLength(String value) {
        this.intermodalEquipLength = value;
    }

    /**
     * Gets the value of the lesseeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLesseeGid() {
        return lesseeGid;
    }

    /**
     * Sets the value of the lesseeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLesseeGid(GLogXMLGidType value) {
        this.lesseeGid = value;
    }

    /**
     * Gets the value of the licenseState property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseState() {
        return licenseState;
    }

    /**
     * Sets the value of the licenseState property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseState(String value) {
        this.licenseState = value;
    }

    /**
     * Gets the value of the licensePlate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlate() {
        return licensePlate;
    }

    /**
     * Sets the value of the licensePlate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlate(String value) {
        this.licensePlate = value;
    }

    /**
     * Gets the value of the equipmentTag1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag1() {
        return equipmentTag1;
    }

    /**
     * Sets the value of the equipmentTag1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag1(String value) {
        this.equipmentTag1 = value;
    }

    /**
     * Gets the value of the equipmentTag2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag2() {
        return equipmentTag2;
    }

    /**
     * Sets the value of the equipmentTag2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag2(String value) {
        this.equipmentTag2 = value;
    }

    /**
     * Gets the value of the equipmentTag3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag3() {
        return equipmentTag3;
    }

    /**
     * Sets the value of the equipmentTag3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag3(String value) {
        this.equipmentTag3 = value;
    }

    /**
     * Gets the value of the equipmentTag4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentTag4() {
        return equipmentTag4;
    }

    /**
     * Sets the value of the equipmentTag4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentTag4(String value) {
        this.equipmentTag4 = value;
    }

    /**
     * Gets the value of the dateBuilt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDateBuilt() {
        return dateBuilt;
    }

    /**
     * Sets the value of the dateBuilt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDateBuilt(GLogDateTimeType value) {
        this.dateBuilt = value;
    }

    /**
     * Gets the value of the purchasedDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPurchasedDate() {
        return purchasedDate;
    }

    /**
     * Sets the value of the purchasedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPurchasedDate(GLogDateTimeType value) {
        this.purchasedDate = value;
    }

    /**
     * Gets the value of the equipmentRegistrationNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentRegistrationNum() {
        return equipmentRegistrationNum;
    }

    /**
     * Sets the value of the equipmentRegistrationNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentRegistrationNum(String value) {
        this.equipmentRegistrationNum = value;
    }

    /**
     * Gets the value of the equipmentRegistrationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEquipmentRegistrationDate() {
        return equipmentRegistrationDate;
    }

    /**
     * Sets the value of the equipmentRegistrationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEquipmentRegistrationDate(GLogDateTimeType value) {
        this.equipmentRegistrationDate = value;
    }

    /**
     * Gets the value of the sightingLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSightingLocGid() {
        return sightingLocGid;
    }

    /**
     * Sets the value of the sightingLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSightingLocGid(GLogXMLGidType value) {
        this.sightingLocGid = value;
    }

    /**
     * Gets the value of the sightingDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getSightingDate() {
        return sightingDate;
    }

    /**
     * Sets the value of the sightingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setSightingDate(GLogDateTimeType value) {
        this.sightingDate = value;
    }

    /**
     * Gets the value of the prevSightingLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPrevSightingLocGid() {
        return prevSightingLocGid;
    }

    /**
     * Sets the value of the prevSightingLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPrevSightingLocGid(GLogXMLGidType value) {
        this.prevSightingLocGid = value;
    }

    /**
     * Gets the value of the prevSightingDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPrevSightingDate() {
        return prevSightingDate;
    }

    /**
     * Sets the value of the prevSightingDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPrevSightingDate(GLogDateTimeType value) {
        this.prevSightingDate = value;
    }

    /**
     * Gets the value of the interchangeRecvLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInterchangeRecvLocGid() {
        return interchangeRecvLocGid;
    }

    /**
     * Sets the value of the interchangeRecvLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInterchangeRecvLocGid(GLogXMLGidType value) {
        this.interchangeRecvLocGid = value;
    }

    /**
     * Gets the value of the interchangeRecvDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getInterchangeRecvDate() {
        return interchangeRecvDate;
    }

    /**
     * Sets the value of the interchangeRecvDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setInterchangeRecvDate(GLogDateTimeType value) {
        this.interchangeRecvDate = value;
    }

    /**
     * Gets the value of the parkLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParkLocationGid() {
        return parkLocationGid;
    }

    /**
     * Sets the value of the parkLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParkLocationGid(GLogXMLGidType value) {
        this.parkLocationGid = value;
    }

    /**
     * Gets the value of the indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndicator() {
        return indicator;
    }

    /**
     * Sets the value of the indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndicator(String value) {
        this.indicator = value;
    }

    /**
     * Gets the value of the isContainer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsContainer() {
        return isContainer;
    }

    /**
     * Sets the value of the isContainer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsContainer(String value) {
        this.isContainer = value;
    }

    /**
     * Gets the value of the chassisInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisInitial() {
        return chassisInitial;
    }

    /**
     * Sets the value of the chassisInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisInitial(String value) {
        this.chassisInitial = value;
    }

    /**
     * Gets the value of the chassisNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChassisNumber() {
        return chassisNumber;
    }

    /**
     * Sets the value of the chassisNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChassisNumber(String value) {
        this.chassisNumber = value;
    }

    /**
     * Gets the value of the lastOutgateLocGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLastOutgateLocGid() {
        return lastOutgateLocGid;
    }

    /**
     * Sets the value of the lastOutgateLocGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLastOutgateLocGid(GLogXMLGidType value) {
        this.lastOutgateLocGid = value;
    }

    /**
     * Gets the value of the lastOutgateTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLastOutgateTime() {
        return lastOutgateTime;
    }

    /**
     * Sets the value of the lastOutgateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLastOutgateTime(GLogDateTimeType value) {
        this.lastOutgateTime = value;
    }

    /**
     * Gets the value of the lastIngateTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLastIngateTime() {
        return lastIngateTime;
    }

    /**
     * Sets the value of the lastIngateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLastIngateTime(GLogDateTimeType value) {
        this.lastIngateTime = value;
    }

    /**
     * Gets the value of the userDefIconInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public UserDefIconInfoType getUserDefIconInfo() {
        return userDefIconInfo;
    }

    /**
     * Sets the value of the userDefIconInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefIconInfoType }
     *     
     */
    public void setUserDefIconInfo(UserDefIconInfoType value) {
        this.userDefIconInfo = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the equipmentSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentSpecialServiceType }
     * 
     * 
     */
    public List<EquipmentSpecialServiceType> getEquipmentSpecialService() {
        if (equipmentSpecialService == null) {
            equipmentSpecialService = new ArrayList<EquipmentSpecialServiceType>();
        }
        return this.equipmentSpecialService;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the services for the order release.
 * 
 * <p>Java class for ReleaseServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CustomerServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ExecutedDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="IsPriorityOverwrite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseServiceType", propOrder = {
    "customerServiceGid",
    "executedDt",
    "isPriorityOverwrite"
})
public class ReleaseServiceType {

    @XmlElement(name = "CustomerServiceGid", required = true)
    protected GLogXMLGidType customerServiceGid;
    @XmlElement(name = "ExecutedDt")
    protected GLogDateTimeType executedDt;
    @XmlElement(name = "IsPriorityOverwrite")
    protected String isPriorityOverwrite;

    /**
     * Gets the value of the customerServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCustomerServiceGid() {
        return customerServiceGid;
    }

    /**
     * Sets the value of the customerServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCustomerServiceGid(GLogXMLGidType value) {
        this.customerServiceGid = value;
    }

    /**
     * Gets the value of the executedDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExecutedDt() {
        return executedDt;
    }

    /**
     * Sets the value of the executedDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExecutedDt(GLogDateTimeType value) {
        this.executedDt = value;
    }

    /**
     * Gets the value of the isPriorityOverwrite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPriorityOverwrite() {
        return isPriorityOverwrite;
    }

    /**
     * Sets the value of the isPriorityOverwrite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPriorityOverwrite(String value) {
        this.isPriorityOverwrite = value;
    }

}

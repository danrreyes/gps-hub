
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the diameter. It includes sub-elements for diameter value and unit of measure.
 * 
 * <p>Java class for DiameterType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DiameterType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DiameterValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DiameterType", propOrder = {
    "diameterValue",
    "uomGid"
})
public class DiameterType {

    @XmlElement(name = "DiameterValue", required = true)
    protected String diameterValue;
    @XmlElement(name = "UOMGid", required = true)
    protected GLogXMLGidType uomGid;

    /**
     * Gets the value of the diameterValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiameterValue() {
        return diameterValue;
    }

    /**
     * Sets the value of the diameterValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiameterValue(String value) {
        this.diameterValue = value;
    }

    /**
     * Gets the value of the uomGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUOMGid() {
        return uomGid;
    }

    /**
     * Sets the value of the uomGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUOMGid(GLogXMLGidType value) {
        this.uomGid = value;
    }

}

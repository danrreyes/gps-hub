
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Duration is the amount of time spent doing an activity, such as a stop.
 *             Includes a duration value and unit of measure.
 *             Example: To specify 30 seconds you would specify DurationValue=30 and DurationUOMGid=S.
 *          
 * 
 * <p>Java class for DurationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DurationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DurationValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DurationUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DurationType", propOrder = {
    "durationValue",
    "durationUOMGid"
})
public class DurationType {

    @XmlElement(name = "DurationValue", required = true)
    protected String durationValue;
    @XmlElement(name = "DurationUOMGid", required = true)
    protected GLogXMLGidType durationUOMGid;

    /**
     * Gets the value of the durationValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDurationValue() {
        return durationValue;
    }

    /**
     * Sets the value of the durationValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDurationValue(String value) {
        this.durationValue = value;
    }

    /**
     * Gets the value of the durationUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDurationUOMGid() {
        return durationUOMGid;
    }

    /**
     * Sets the value of the durationUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDurationUOMGid(GLogXMLGidType value) {
        this.durationUOMGid = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies packaged item information.
 * 
 * <p>Java class for PackagedItemRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PackagedItemRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="PackagedItem" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemType"/&gt;
 *         &lt;element name="PackagedItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PackagedItemRefType", propOrder = {
    "packagedItem",
    "packagedItemGid"
})
public class PackagedItemRefType {

    @XmlElement(name = "PackagedItem")
    protected PackagedItemType packagedItem;
    @XmlElement(name = "PackagedItemGid")
    protected GLogXMLGidType packagedItemGid;

    /**
     * Gets the value of the packagedItem property.
     * 
     * @return
     *     possible object is
     *     {@link PackagedItemType }
     *     
     */
    public PackagedItemType getPackagedItem() {
        return packagedItem;
    }

    /**
     * Sets the value of the packagedItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagedItemType }
     *     
     */
    public void setPackagedItem(PackagedItemType value) {
        this.packagedItem = value;
    }

    /**
     * Gets the value of the packagedItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagedItemGid() {
        return packagedItemGid;
    }

    /**
     * Sets the value of the packagedItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagedItemGid(GLogXMLGidType value) {
        this.packagedItemGid = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Status of HOS Rule.
 *          
 * 
 * <p>Java class for HOSRuleState complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HOSRuleState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HOSRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ActivityTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ActivityTimeRemain" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="RuleBeginTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HOSRuleState", propOrder = {
    "hosRuleGid",
    "activityTime",
    "activityTimeRemain",
    "ruleBeginTime"
})
public class HOSRuleState {

    @XmlElement(name = "HOSRuleGid")
    protected GLogXMLGidType hosRuleGid;
    @XmlElement(name = "ActivityTime")
    protected GLogXMLDurationType activityTime;
    @XmlElement(name = "ActivityTimeRemain")
    protected GLogXMLDurationType activityTimeRemain;
    @XmlElement(name = "RuleBeginTime")
    protected GLogDateTimeType ruleBeginTime;

    /**
     * Gets the value of the hosRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHOSRuleGid() {
        return hosRuleGid;
    }

    /**
     * Sets the value of the hosRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHOSRuleGid(GLogXMLGidType value) {
        this.hosRuleGid = value;
    }

    /**
     * Gets the value of the activityTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActivityTime() {
        return activityTime;
    }

    /**
     * Sets the value of the activityTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActivityTime(GLogXMLDurationType value) {
        this.activityTime = value;
    }

    /**
     * Gets the value of the activityTimeRemain property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getActivityTimeRemain() {
        return activityTimeRemain;
    }

    /**
     * Sets the value of the activityTimeRemain property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setActivityTimeRemain(GLogXMLDurationType value) {
        this.activityTimeRemain = value;
    }

    /**
     * Gets the value of the ruleBeginTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getRuleBeginTime() {
        return ruleBeginTime;
    }

    /**
     * Sets the value of the ruleBeginTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setRuleBeginTime(GLogDateTimeType value) {
        this.ruleBeginTime = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Represents the attributes of the association of the SEquipment on the Shipment.
 * 
 * <p>Java class for ShipmentSEquipmentInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentSEquipmentInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CapacityUsageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PickupStopNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DropoffStopNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SEquipmentIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentSEquipmentInfoType", propOrder = {
    "capacityUsageGid",
    "pickupStopNum",
    "dropoffStopNum",
    "sEquipmentIndex"
})
public class ShipmentSEquipmentInfoType {

    @XmlElement(name = "CapacityUsageGid")
    protected GLogXMLGidType capacityUsageGid;
    @XmlElement(name = "PickupStopNum")
    protected String pickupStopNum;
    @XmlElement(name = "DropoffStopNum")
    protected String dropoffStopNum;
    @XmlElement(name = "SEquipmentIndex")
    protected String sEquipmentIndex;

    /**
     * Gets the value of the capacityUsageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCapacityUsageGid() {
        return capacityUsageGid;
    }

    /**
     * Sets the value of the capacityUsageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCapacityUsageGid(GLogXMLGidType value) {
        this.capacityUsageGid = value;
    }

    /**
     * Gets the value of the pickupStopNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPickupStopNum() {
        return pickupStopNum;
    }

    /**
     * Sets the value of the pickupStopNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPickupStopNum(String value) {
        this.pickupStopNum = value;
    }

    /**
     * Gets the value of the dropoffStopNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDropoffStopNum() {
        return dropoffStopNum;
    }

    /**
     * Sets the value of the dropoffStopNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDropoffStopNum(String value) {
        this.dropoffStopNum = value;
    }

    /**
     * Gets the value of the sEquipmentIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEquipmentIndex() {
        return sEquipmentIndex;
    }

    /**
     * Sets the value of the sEquipmentIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEquipmentIndex(String value) {
        this.sEquipmentIndex = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * provides information related to the quoted move, including cost breakdowns,mode(s),equipment,and remarks.
 * 
 * <p>Java class for QuoteCostOptionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuoteCostOptionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="SellCostOption"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="BuyCostOption"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CostOptionEquip" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionEquipType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CostOptionShip" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionShipType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteCostOptionType", propOrder = {
    "intSavedQuery",
    "sequenceNumber",
    "transactionCode",
    "sellCostOption",
    "buyCostOption",
    "costOptionEquip",
    "remark",
    "costOptionShip"
})
public class QuoteCostOptionType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "SellCostOption", required = true)
    protected QuoteCostOptionType.SellCostOption sellCostOption;
    @XmlElement(name = "BuyCostOption", required = true)
    protected QuoteCostOptionType.BuyCostOption buyCostOption;
    @XmlElement(name = "CostOptionEquip")
    protected List<CostOptionEquipType> costOptionEquip;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "CostOptionShip")
    protected List<CostOptionShipType> costOptionShip;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the sellCostOption property.
     * 
     * @return
     *     possible object is
     *     {@link QuoteCostOptionType.SellCostOption }
     *     
     */
    public QuoteCostOptionType.SellCostOption getSellCostOption() {
        return sellCostOption;
    }

    /**
     * Sets the value of the sellCostOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuoteCostOptionType.SellCostOption }
     *     
     */
    public void setSellCostOption(QuoteCostOptionType.SellCostOption value) {
        this.sellCostOption = value;
    }

    /**
     * Gets the value of the buyCostOption property.
     * 
     * @return
     *     possible object is
     *     {@link QuoteCostOptionType.BuyCostOption }
     *     
     */
    public QuoteCostOptionType.BuyCostOption getBuyCostOption() {
        return buyCostOption;
    }

    /**
     * Sets the value of the buyCostOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link QuoteCostOptionType.BuyCostOption }
     *     
     */
    public void setBuyCostOption(QuoteCostOptionType.BuyCostOption value) {
        this.buyCostOption = value;
    }

    /**
     * Gets the value of the costOptionEquip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the costOptionEquip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostOptionEquip().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostOptionEquipType }
     * 
     * 
     */
    public List<CostOptionEquipType> getCostOptionEquip() {
        if (costOptionEquip == null) {
            costOptionEquip = new ArrayList<CostOptionEquipType>();
        }
        return this.costOptionEquip;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the costOptionShip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the costOptionShip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostOptionShip().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CostOptionShipType }
     * 
     * 
     */
    public List<CostOptionShipType> getCostOptionShip() {
        if (costOptionShip == null) {
            costOptionShip = new ArrayList<CostOptionShipType>();
        }
        return this.costOptionShip;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "costOption"
    })
    public static class BuyCostOption {

        @XmlElement(name = "CostOption", required = true)
        protected CostOptionType costOption;

        /**
         * Gets the value of the costOption property.
         * 
         * @return
         *     possible object is
         *     {@link CostOptionType }
         *     
         */
        public CostOptionType getCostOption() {
            return costOption;
        }

        /**
         * Sets the value of the costOption property.
         * 
         * @param value
         *     allowed object is
         *     {@link CostOptionType }
         *     
         */
        public void setCostOption(CostOptionType value) {
            this.costOption = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="CostOption" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostOptionType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "costOption"
    })
    public static class SellCostOption {

        @XmlElement(name = "CostOption", required = true)
        protected CostOptionType costOption;

        /**
         * Gets the value of the costOption property.
         * 
         * @return
         *     possible object is
         *     {@link CostOptionType }
         *     
         */
        public CostOptionType getCostOption() {
            return costOption;
        }

        /**
         * Sets the value of the costOption property.
         * 
         * @param value
         *     allowed object is
         *     {@link CostOptionType }
         *     
         */
        public void setCostOption(CostOptionType value) {
            this.costOption = value;
        }

    }

}

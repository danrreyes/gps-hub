
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the debrief information about orders on shipment stops. It is used to indicate what was collected at each pickup stop.
 * 
 * <p>Java class for ShipStopDebriefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipStopDebriefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Activity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;element name="SEquipmentGidQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentGidQueryType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="NonConfReasonCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="MatchType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipStopDebriefType", propOrder = {
    "sequenceNumber",
    "activity",
    "shipUnitGid",
    "lineNumber",
    "releaseLineGid",
    "releaseShipUnitGid",
    "sEquipmentGid",
    "sEquipmentGidQuery",
    "packagedItemCount",
    "packagedItemSpecRef",
    "packagedItemSpecCount",
    "transportHandlingUnitRef",
    "transportHandlingUnitCount",
    "itemGid",
    "nonConfReasonCodeGid",
    "matchType"
})
public class ShipStopDebriefType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "Activity", required = true)
    protected String activity;
    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "ReleaseLineGid")
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "ReleaseShipUnitGid")
    protected GLogXMLGidType releaseShipUnitGid;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "SEquipmentGidQuery")
    protected SEquipmentGidQueryType sEquipmentGidQuery;
    @XmlElement(name = "PackagedItemCount")
    protected String packagedItemCount;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "PackagedItemSpecCount")
    protected String packagedItemSpecCount;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "TransportHandlingUnitCount")
    protected String transportHandlingUnitCount;
    @XmlElement(name = "ItemGid")
    protected GLogXMLGidType itemGid;
    @XmlElement(name = "NonConfReasonCodeGid")
    protected GLogXMLGidType nonConfReasonCodeGid;
    @XmlElement(name = "MatchType")
    protected String matchType;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the activity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Sets the value of the activity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivity(String value) {
        this.activity = value;
    }

    /**
     * Gets the value of the shipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Sets the value of the shipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the releaseLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Sets the value of the releaseLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Gets the value of the releaseShipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseShipUnitGid() {
        return releaseShipUnitGid;
    }

    /**
     * Sets the value of the releaseShipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseShipUnitGid(GLogXMLGidType value) {
        this.releaseShipUnitGid = value;
    }

    /**
     * Gets the value of the sEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Sets the value of the sEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the sEquipmentGidQuery property.
     * 
     * @return
     *     possible object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public SEquipmentGidQueryType getSEquipmentGidQuery() {
        return sEquipmentGidQuery;
    }

    /**
     * Sets the value of the sEquipmentGidQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public void setSEquipmentGidQuery(SEquipmentGidQueryType value) {
        this.sEquipmentGidQuery = value;
    }

    /**
     * Gets the value of the packagedItemCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemCount() {
        return packagedItemCount;
    }

    /**
     * Sets the value of the packagedItemCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemCount(String value) {
        this.packagedItemCount = value;
    }

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Gets the value of the packagedItemSpecCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemSpecCount() {
        return packagedItemSpecCount;
    }

    /**
     * Sets the value of the packagedItemSpecCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemSpecCount(String value) {
        this.packagedItemSpecCount = value;
    }

    /**
     * Gets the value of the transportHandlingUnitRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Sets the value of the transportHandlingUnitRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Gets the value of the transportHandlingUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransportHandlingUnitCount() {
        return transportHandlingUnitCount;
    }

    /**
     * Sets the value of the transportHandlingUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransportHandlingUnitCount(String value) {
        this.transportHandlingUnitCount = value;
    }

    /**
     * Gets the value of the itemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemGid() {
        return itemGid;
    }

    /**
     * Sets the value of the itemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemGid(GLogXMLGidType value) {
        this.itemGid = value;
    }

    /**
     * Gets the value of the nonConfReasonCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getNonConfReasonCodeGid() {
        return nonConfReasonCodeGid;
    }

    /**
     * Sets the value of the nonConfReasonCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setNonConfReasonCodeGid(GLogXMLGidType value) {
        this.nonConfReasonCodeGid = value;
    }

    /**
     * Gets the value of the matchType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMatchType() {
        return matchType;
    }

    /**
     * Sets the value of the matchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMatchType(String value) {
        this.matchType = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Hazardous material information for the release line.
 * 
 * <p>Java class for ReleaseLineHazmatInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseLineHazmatInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazmatGenericGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProperShippingName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazardousClass" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazardousClassType" minOccurs="0"/&gt;
 *         &lt;element name="PackagingGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SubsidiaryHazard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmergencyResponseInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PSASingaporeGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazmatCommonInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatCommonInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ERG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ERGAir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazCompatGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazSpecialProvisions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazApprovalExemptionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseLineHazmatInfoType", propOrder = {
    "isHazardous",
    "hazmatGenericGid",
    "hazmatItemGid",
    "identificationNumber",
    "properShippingName",
    "hazardousClass",
    "packagingGroup",
    "subsidiaryHazard",
    "emergencyResponseInfo",
    "psaSingaporeGroup",
    "hazmatCommonInfo",
    "erg",
    "ergAir",
    "ems",
    "hazCompatGroup",
    "hazSpecialProvisions",
    "hazApprovalExemptionGid"
})
public class ReleaseLineHazmatInfoType {

    @XmlElement(name = "IsHazardous")
    protected String isHazardous;
    @XmlElement(name = "HazmatGenericGid")
    protected GLogXMLGidType hazmatGenericGid;
    @XmlElement(name = "HazmatItemGid")
    protected GLogXMLGidType hazmatItemGid;
    @XmlElement(name = "IdentificationNumber")
    protected String identificationNumber;
    @XmlElement(name = "ProperShippingName")
    protected String properShippingName;
    @XmlElement(name = "HazardousClass")
    protected HazardousClassType hazardousClass;
    @XmlElement(name = "PackagingGroup")
    protected String packagingGroup;
    @XmlElement(name = "SubsidiaryHazard")
    protected String subsidiaryHazard;
    @XmlElement(name = "EmergencyResponseInfo")
    protected String emergencyResponseInfo;
    @XmlElement(name = "PSASingaporeGroup")
    protected String psaSingaporeGroup;
    @XmlElement(name = "HazmatCommonInfo")
    protected HazmatCommonInfoType hazmatCommonInfo;
    @XmlElement(name = "ERG")
    protected String erg;
    @XmlElement(name = "ERGAir")
    protected String ergAir;
    @XmlElement(name = "EMS")
    protected String ems;
    @XmlElement(name = "HazCompatGroup")
    protected String hazCompatGroup;
    @XmlElement(name = "HazSpecialProvisions")
    protected String hazSpecialProvisions;
    @XmlElement(name = "HazApprovalExemptionGid")
    protected GLogXMLGidType hazApprovalExemptionGid;

    /**
     * Gets the value of the isHazardous property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Sets the value of the isHazardous property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

    /**
     * Gets the value of the hazmatGenericGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatGenericGid() {
        return hazmatGenericGid;
    }

    /**
     * Sets the value of the hazmatGenericGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatGenericGid(GLogXMLGidType value) {
        this.hazmatGenericGid = value;
    }

    /**
     * Gets the value of the hazmatItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatItemGid() {
        return hazmatItemGid;
    }

    /**
     * Sets the value of the hazmatItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatItemGid(GLogXMLGidType value) {
        this.hazmatItemGid = value;
    }

    /**
     * Gets the value of the identificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    /**
     * Sets the value of the identificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationNumber(String value) {
        this.identificationNumber = value;
    }

    /**
     * Gets the value of the properShippingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProperShippingName() {
        return properShippingName;
    }

    /**
     * Sets the value of the properShippingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProperShippingName(String value) {
        this.properShippingName = value;
    }

    /**
     * Gets the value of the hazardousClass property.
     * 
     * @return
     *     possible object is
     *     {@link HazardousClassType }
     *     
     */
    public HazardousClassType getHazardousClass() {
        return hazardousClass;
    }

    /**
     * Sets the value of the hazardousClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazardousClassType }
     *     
     */
    public void setHazardousClass(HazardousClassType value) {
        this.hazardousClass = value;
    }

    /**
     * Gets the value of the packagingGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagingGroup() {
        return packagingGroup;
    }

    /**
     * Sets the value of the packagingGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagingGroup(String value) {
        this.packagingGroup = value;
    }

    /**
     * Gets the value of the subsidiaryHazard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubsidiaryHazard() {
        return subsidiaryHazard;
    }

    /**
     * Sets the value of the subsidiaryHazard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubsidiaryHazard(String value) {
        this.subsidiaryHazard = value;
    }

    /**
     * Gets the value of the emergencyResponseInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergencyResponseInfo() {
        return emergencyResponseInfo;
    }

    /**
     * Sets the value of the emergencyResponseInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergencyResponseInfo(String value) {
        this.emergencyResponseInfo = value;
    }

    /**
     * Gets the value of the psaSingaporeGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSASingaporeGroup() {
        return psaSingaporeGroup;
    }

    /**
     * Sets the value of the psaSingaporeGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSASingaporeGroup(String value) {
        this.psaSingaporeGroup = value;
    }

    /**
     * Gets the value of the hazmatCommonInfo property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public HazmatCommonInfoType getHazmatCommonInfo() {
        return hazmatCommonInfo;
    }

    /**
     * Sets the value of the hazmatCommonInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatCommonInfoType }
     *     
     */
    public void setHazmatCommonInfo(HazmatCommonInfoType value) {
        this.hazmatCommonInfo = value;
    }

    /**
     * Gets the value of the erg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERG() {
        return erg;
    }

    /**
     * Sets the value of the erg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERG(String value) {
        this.erg = value;
    }

    /**
     * Gets the value of the ergAir property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERGAir() {
        return ergAir;
    }

    /**
     * Sets the value of the ergAir property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERGAir(String value) {
        this.ergAir = value;
    }

    /**
     * Gets the value of the ems property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMS() {
        return ems;
    }

    /**
     * Sets the value of the ems property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMS(String value) {
        this.ems = value;
    }

    /**
     * Gets the value of the hazCompatGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazCompatGroup() {
        return hazCompatGroup;
    }

    /**
     * Sets the value of the hazCompatGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazCompatGroup(String value) {
        this.hazCompatGroup = value;
    }

    /**
     * Gets the value of the hazSpecialProvisions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazSpecialProvisions() {
        return hazSpecialProvisions;
    }

    /**
     * Sets the value of the hazSpecialProvisions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazSpecialProvisions(String value) {
        this.hazSpecialProvisions = value;
    }

    /**
     * Gets the value of the hazApprovalExemptionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazApprovalExemptionGid() {
        return hazApprovalExemptionGid;
    }

    /**
     * Sets the value of the hazApprovalExemptionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazApprovalExemptionGid(GLogXMLGidType value) {
        this.hazApprovalExemptionGid = value;
    }

}

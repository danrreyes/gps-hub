
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ShipmentGroupTenderOffer is a wrapper around the ShipmentGroup element. The interface currently
 *             does not support a response from the service provider for the offer.
 *          
 * 
 * <p>Java class for ShipmentGroupTenderOfferType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentGroupTenderOfferType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipmentGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentGroupType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentGroupTenderOfferType", propOrder = {
    "shipmentGroup"
})
public class ShipmentGroupTenderOfferType
    extends OTMTransactionInOut
{

    @XmlElement(name = "ShipmentGroup", required = true)
    protected ShipmentGroupType shipmentGroup;

    /**
     * Gets the value of the shipmentGroup property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentGroupType }
     *     
     */
    public ShipmentGroupType getShipmentGroup() {
        return shipmentGroup;
    }

    /**
     * Sets the value of the shipmentGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentGroupType }
     *     
     */
    public void setShipmentGroup(ShipmentGroupType value) {
        this.shipmentGroup = value;
    }

}

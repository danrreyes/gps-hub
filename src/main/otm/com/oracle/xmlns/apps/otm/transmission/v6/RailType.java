
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains elements used to support Rail functionality.
 * 
 * <p>Java class for RailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RouteCodeCombinationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RouteCodeCombination" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLRouteCodeType" minOccurs="0"/&gt;
 *         &lt;element name="RailInterModalPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CustomerRateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RailReturnLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *         &lt;element name="RailReturnRouteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RailReturnRouteGidType" minOccurs="0"/&gt;
 *         &lt;element name="Rule11_Indicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Rule11SecondShipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}Rule11SecondShipmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RailType", propOrder = {
    "routeCodeCombinationGid",
    "routeCodeCombination",
    "railInterModalPlanGid",
    "customerRateCode",
    "cofctofc",
    "railReturnLocationGid",
    "railReturnRouteGid",
    "rule11Indicator",
    "rule11SecondShipment"
})
public class RailType {

    @XmlElement(name = "RouteCodeCombinationGid")
    protected GLogXMLGidType routeCodeCombinationGid;
    @XmlElement(name = "RouteCodeCombination")
    protected GLogXMLRouteCodeType routeCodeCombination;
    @XmlElement(name = "RailInterModalPlanGid")
    protected GLogXMLGidType railInterModalPlanGid;
    @XmlElement(name = "CustomerRateCode")
    protected String customerRateCode;
    @XmlElement(name = "COFC_TOFC")
    protected String cofctofc;
    @XmlElement(name = "RailReturnLocationGid")
    protected GLogXMLLocGidType railReturnLocationGid;
    @XmlElement(name = "RailReturnRouteGid")
    protected RailReturnRouteGidType railReturnRouteGid;
    @XmlElement(name = "Rule11_Indicator")
    protected String rule11Indicator;
    @XmlElement(name = "Rule11SecondShipment")
    protected List<Rule11SecondShipmentType> rule11SecondShipment;

    /**
     * Gets the value of the routeCodeCombinationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRouteCodeCombinationGid() {
        return routeCodeCombinationGid;
    }

    /**
     * Sets the value of the routeCodeCombinationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRouteCodeCombinationGid(GLogXMLGidType value) {
        this.routeCodeCombinationGid = value;
    }

    /**
     * Gets the value of the routeCodeCombination property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLRouteCodeType }
     *     
     */
    public GLogXMLRouteCodeType getRouteCodeCombination() {
        return routeCodeCombination;
    }

    /**
     * Sets the value of the routeCodeCombination property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLRouteCodeType }
     *     
     */
    public void setRouteCodeCombination(GLogXMLRouteCodeType value) {
        this.routeCodeCombination = value;
    }

    /**
     * Gets the value of the railInterModalPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailInterModalPlanGid() {
        return railInterModalPlanGid;
    }

    /**
     * Sets the value of the railInterModalPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailInterModalPlanGid(GLogXMLGidType value) {
        this.railInterModalPlanGid = value;
    }

    /**
     * Gets the value of the customerRateCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRateCode() {
        return customerRateCode;
    }

    /**
     * Sets the value of the customerRateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRateCode(String value) {
        this.customerRateCode = value;
    }

    /**
     * Gets the value of the cofctofc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOFCTOFC() {
        return cofctofc;
    }

    /**
     * Sets the value of the cofctofc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOFCTOFC(String value) {
        this.cofctofc = value;
    }

    /**
     * Gets the value of the railReturnLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getRailReturnLocationGid() {
        return railReturnLocationGid;
    }

    /**
     * Sets the value of the railReturnLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setRailReturnLocationGid(GLogXMLLocGidType value) {
        this.railReturnLocationGid = value;
    }

    /**
     * Gets the value of the railReturnRouteGid property.
     * 
     * @return
     *     possible object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public RailReturnRouteGidType getRailReturnRouteGid() {
        return railReturnRouteGid;
    }

    /**
     * Sets the value of the railReturnRouteGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link RailReturnRouteGidType }
     *     
     */
    public void setRailReturnRouteGid(RailReturnRouteGidType value) {
        this.railReturnRouteGid = value;
    }

    /**
     * Gets the value of the rule11Indicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRule11Indicator() {
        return rule11Indicator;
    }

    /**
     * Sets the value of the rule11Indicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRule11Indicator(String value) {
        this.rule11Indicator = value;
    }

    /**
     * Gets the value of the rule11SecondShipment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the rule11SecondShipment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRule11SecondShipment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Rule11SecondShipmentType }
     * 
     * 
     */
    public List<Rule11SecondShipmentType> getRule11SecondShipment() {
        if (rule11SecondShipment == null) {
            rule11SecondShipment = new ArrayList<Rule11SecondShipmentType>();
        }
        return this.rule11SecondShipment;
    }

}

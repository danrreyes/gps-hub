
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies detail info for operational locations. Used as a lookup so that operational location may be automatically assigned.
 * 
 * <p>Java class for OperLocDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OperLocDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PriLegServProv" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PriLegServProvType"/&gt;
 *         &lt;element name="PriLegLocProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="ImportExportType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InclusionRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ExclusionRegionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OperLocDetailType", propOrder = {
    "sequenceNumber",
    "priLegServProv",
    "priLegLocProfileGid",
    "importExportType",
    "inclusionRegionGid",
    "exclusionRegionGid"
})
public class OperLocDetailType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "PriLegServProv", required = true)
    protected PriLegServProvType priLegServProv;
    @XmlElement(name = "PriLegLocProfileGid")
    protected GLogXMLLocProfileType priLegLocProfileGid;
    @XmlElement(name = "ImportExportType")
    protected String importExportType;
    @XmlElement(name = "InclusionRegionGid")
    protected GLogXMLGidType inclusionRegionGid;
    @XmlElement(name = "ExclusionRegionGid")
    protected GLogXMLGidType exclusionRegionGid;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the priLegServProv property.
     * 
     * @return
     *     possible object is
     *     {@link PriLegServProvType }
     *     
     */
    public PriLegServProvType getPriLegServProv() {
        return priLegServProv;
    }

    /**
     * Sets the value of the priLegServProv property.
     * 
     * @param value
     *     allowed object is
     *     {@link PriLegServProvType }
     *     
     */
    public void setPriLegServProv(PriLegServProvType value) {
        this.priLegServProv = value;
    }

    /**
     * Gets the value of the priLegLocProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getPriLegLocProfileGid() {
        return priLegLocProfileGid;
    }

    /**
     * Sets the value of the priLegLocProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setPriLegLocProfileGid(GLogXMLLocProfileType value) {
        this.priLegLocProfileGid = value;
    }

    /**
     * Gets the value of the importExportType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImportExportType() {
        return importExportType;
    }

    /**
     * Sets the value of the importExportType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImportExportType(String value) {
        this.importExportType = value;
    }

    /**
     * Gets the value of the inclusionRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInclusionRegionGid() {
        return inclusionRegionGid;
    }

    /**
     * Sets the value of the inclusionRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInclusionRegionGid(GLogXMLGidType value) {
        this.inclusionRegionGid = value;
    }

    /**
     * Gets the value of the exclusionRegionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getExclusionRegionGid() {
        return exclusionRegionGid;
    }

    /**
     * Sets the value of the exclusionRegionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setExclusionRegionGid(GLogXMLGidType value) {
        this.exclusionRegionGid = value;
    }

}

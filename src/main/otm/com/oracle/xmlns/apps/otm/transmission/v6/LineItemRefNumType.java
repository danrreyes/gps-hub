
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * LineItemRefNum is an element of RailLineItem used to identify the line item.
 * 
 * <p>Java class for LineItemRefNumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LineItemRefNumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LineItemRefNumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LineItemRefNumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemRefNumType", propOrder = {
    "lineItemRefNumValue",
    "lineItemRefNumQualifierGid"
})
public class LineItemRefNumType {

    @XmlElement(name = "LineItemRefNumValue", required = true)
    protected String lineItemRefNumValue;
    @XmlElement(name = "LineItemRefNumQualifierGid", required = true)
    protected GLogXMLGidType lineItemRefNumQualifierGid;

    /**
     * Gets the value of the lineItemRefNumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineItemRefNumValue() {
        return lineItemRefNumValue;
    }

    /**
     * Sets the value of the lineItemRefNumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineItemRefNumValue(String value) {
        this.lineItemRefNumValue = value;
    }

    /**
     * Gets the value of the lineItemRefNumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLineItemRefNumQualifierGid() {
        return lineItemRefNumQualifierGid;
    }

    /**
     * Sets the value of the lineItemRefNumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLineItemRefNumQualifierGid(GLogXMLGidType value) {
        this.lineItemRefNumQualifierGid = value;
    }

}

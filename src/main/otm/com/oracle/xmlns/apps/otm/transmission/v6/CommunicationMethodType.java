
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * CommunicationMethod is a structure for describing when and how to communicate
 *             with a business partner.
 *          
 * 
 * <p>Java class for CommunicationMethodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommunicationMethodType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ComMethodRank" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CalendarGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ExpectedResponseTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommunicationMethodType", propOrder = {
    "comMethodRank",
    "comMethodGid",
    "calendarGid",
    "expectedResponseTime"
})
public class CommunicationMethodType {

    @XmlElement(name = "ComMethodRank", required = true)
    protected String comMethodRank;
    @XmlElement(name = "ComMethodGid", required = true)
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "CalendarGid")
    protected GLogXMLGidType calendarGid;
    @XmlElement(name = "ExpectedResponseTime")
    protected GLogXMLDurationType expectedResponseTime;

    /**
     * Gets the value of the comMethodRank property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComMethodRank() {
        return comMethodRank;
    }

    /**
     * Sets the value of the comMethodRank property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComMethodRank(String value) {
        this.comMethodRank = value;
    }

    /**
     * Gets the value of the comMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Sets the value of the comMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Gets the value of the calendarGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalendarGid() {
        return calendarGid;
    }

    /**
     * Sets the value of the calendarGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalendarGid(GLogXMLGidType value) {
        this.calendarGid = value;
    }

    /**
     * Gets the value of the expectedResponseTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getExpectedResponseTime() {
        return expectedResponseTime;
    }

    /**
     * Sets the value of the expectedResponseTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setExpectedResponseTime(GLogXMLDurationType value) {
        this.expectedResponseTime = value;
    }

}

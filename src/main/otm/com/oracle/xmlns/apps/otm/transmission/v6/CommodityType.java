
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Commodity is an element of a invoice line used to identify the "stuff" being delivered.
 * 
 * <p>Java class for CommodityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommodityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FlexCommodityQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Marks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MarksType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommodityType", propOrder = {
    "flexCommodityQualifierGid",
    "flexCommodityValue",
    "description",
    "marks"
})
public class CommodityType {

    @XmlElement(name = "FlexCommodityQualifierGid")
    protected GLogXMLGidType flexCommodityQualifierGid;
    @XmlElement(name = "FlexCommodityValue")
    protected String flexCommodityValue;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "Marks")
    protected MarksType marks;

    /**
     * Gets the value of the flexCommodityQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityQualifierGid() {
        return flexCommodityQualifierGid;
    }

    /**
     * Sets the value of the flexCommodityQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityQualifierGid(GLogXMLGidType value) {
        this.flexCommodityQualifierGid = value;
    }

    /**
     * Gets the value of the flexCommodityValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexCommodityValue() {
        return flexCommodityValue;
    }

    /**
     * Sets the value of the flexCommodityValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexCommodityValue(String value) {
        this.flexCommodityValue = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the marks property.
     * 
     * @return
     *     possible object is
     *     {@link MarksType }
     *     
     */
    public MarksType getMarks() {
        return marks;
    }

    /**
     * Sets the value of the marks property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarksType }
     *     
     */
    public void setMarks(MarksType value) {
        this.marks = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains the information from the RATE_GEO for the second Shipment in a Rail Rule 11 move.
 * 
 * <p>Java class for Rule11SecShipRateGeoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Rule11SecShipRateGeoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RailInterModalPlanGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CustomerRateCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="COFC_TOFC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Rule11SecShipRateGeoType", propOrder = {
    "railInterModalPlanGid",
    "customerRateCode",
    "cofctofc"
})
public class Rule11SecShipRateGeoType {

    @XmlElement(name = "RailInterModalPlanGid")
    protected GLogXMLGidType railInterModalPlanGid;
    @XmlElement(name = "CustomerRateCode")
    protected String customerRateCode;
    @XmlElement(name = "COFC_TOFC")
    protected String cofctofc;

    /**
     * Gets the value of the railInterModalPlanGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailInterModalPlanGid() {
        return railInterModalPlanGid;
    }

    /**
     * Sets the value of the railInterModalPlanGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailInterModalPlanGid(GLogXMLGidType value) {
        this.railInterModalPlanGid = value;
    }

    /**
     * Gets the value of the customerRateCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerRateCode() {
        return customerRateCode;
    }

    /**
     * Sets the value of the customerRateCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerRateCode(String value) {
        this.customerRateCode = value;
    }

    /**
     * Gets the value of the cofctofc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOFCTOFC() {
        return cofctofc;
    }

    /**
     * Sets the value of the cofctofc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOFCTOFC(String value) {
        this.cofctofc = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Indicates a representation of a piece of equipment associated with a SHIPMENT.
 *             It can be, but is not required to be, associated with an actual EQUIPMENT record.
 * 
 *             Note that the Equipment and EquipmentType elements are supported outbound only.
 *          
 * 
 * <p>Java class for SEquipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SEquipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentInitialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SEquipmentSeal" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentSealType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentAttribute" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentAttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="WeightQualifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScaleWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="ScaleLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScaleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScaleTicket" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IntermodalEquipLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SubstituteEquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LoadConfigVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentSEquipmentInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentSEquipmentInfoType" minOccurs="0"/&gt;
 *         &lt;element name="Equipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentType" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentDescriptionType" minOccurs="0"/&gt;
 *         &lt;element name="IsFreight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CheckDigit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LicensePlate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FreightCGLength" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/&gt;
 *         &lt;element name="FreightCGWidth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWidthType" minOccurs="0"/&gt;
 *         &lt;element name="FreightCGHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLHeightType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SEquipmentType", propOrder = {
    "intSavedQuery",
    "sEquipmentGid",
    "transactionCode",
    "replaceChildren",
    "equipmentInitial",
    "equipmentNumber",
    "equipmentInitialNumber",
    "equipmentTypeGid",
    "equipmentGroupGid",
    "equipmentGid",
    "sEquipmentSeal",
    "equipmentAttribute",
    "weightQualifier",
    "scaleWeight",
    "tareWeight",
    "scaleLocation",
    "scaleName",
    "scaleTicket",
    "intermodalEquipLength",
    "substituteEquipmentGroupGid",
    "loadConfigVolume",
    "shipmentSEquipmentInfo",
    "equipment",
    "equipmentType",
    "isFreight",
    "checkDigit",
    "licensePlate",
    "freightCGLength",
    "freightCGWidth",
    "freightCGHeight"
})
public class SEquipmentType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "EquipmentInitial")
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber")
    protected String equipmentNumber;
    @XmlElement(name = "EquipmentInitialNumber")
    protected String equipmentInitialNumber;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentGid")
    protected GLogXMLGidType equipmentGid;
    @XmlElement(name = "SEquipmentSeal")
    protected List<SEquipmentSealType> sEquipmentSeal;
    @XmlElement(name = "EquipmentAttribute")
    protected List<EquipmentAttributeType> equipmentAttribute;
    @XmlElement(name = "WeightQualifier")
    protected String weightQualifier;
    @XmlElement(name = "ScaleWeight")
    protected GLogXMLWeightType scaleWeight;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "ScaleLocation")
    protected String scaleLocation;
    @XmlElement(name = "ScaleName")
    protected String scaleName;
    @XmlElement(name = "ScaleTicket")
    protected String scaleTicket;
    @XmlElement(name = "IntermodalEquipLength")
    protected String intermodalEquipLength;
    @XmlElement(name = "SubstituteEquipmentGroupGid")
    protected GLogXMLGidType substituteEquipmentGroupGid;
    @XmlElement(name = "LoadConfigVolume")
    protected GLogXMLVolumeType loadConfigVolume;
    @XmlElement(name = "ShipmentSEquipmentInfo")
    protected ShipmentSEquipmentInfoType shipmentSEquipmentInfo;
    @XmlElement(name = "Equipment")
    protected EquipmentType equipment;
    @XmlElement(name = "EquipmentType")
    protected EquipmentDescriptionType equipmentType;
    @XmlElement(name = "IsFreight")
    protected String isFreight;
    @XmlElement(name = "CheckDigit")
    protected String checkDigit;
    @XmlElement(name = "LicensePlate")
    protected String licensePlate;
    @XmlElement(name = "FreightCGLength")
    protected GLogXMLLengthType freightCGLength;
    @XmlElement(name = "FreightCGWidth")
    protected GLogXMLWidthType freightCGWidth;
    @XmlElement(name = "FreightCGHeight")
    protected GLogXMLHeightType freightCGHeight;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the sEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Sets the value of the sEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the equipmentInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Sets the value of the equipmentInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Gets the value of the equipmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Sets the value of the equipmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the equipmentInitialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitialNumber() {
        return equipmentInitialNumber;
    }

    /**
     * Sets the value of the equipmentInitialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitialNumber(String value) {
        this.equipmentInitialNumber = value;
    }

    /**
     * Gets the value of the equipmentTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Sets the value of the equipmentTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Sets the value of the equipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Gets the value of the equipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGid() {
        return equipmentGid;
    }

    /**
     * Sets the value of the equipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGid(GLogXMLGidType value) {
        this.equipmentGid = value;
    }

    /**
     * Gets the value of the sEquipmentSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the sEquipmentSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSEquipmentSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SEquipmentSealType }
     * 
     * 
     */
    public List<SEquipmentSealType> getSEquipmentSeal() {
        if (sEquipmentSeal == null) {
            sEquipmentSeal = new ArrayList<SEquipmentSealType>();
        }
        return this.sEquipmentSeal;
    }

    /**
     * Gets the value of the equipmentAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the equipmentAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEquipmentAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentAttributeType }
     * 
     * 
     */
    public List<EquipmentAttributeType> getEquipmentAttribute() {
        if (equipmentAttribute == null) {
            equipmentAttribute = new ArrayList<EquipmentAttributeType>();
        }
        return this.equipmentAttribute;
    }

    /**
     * Gets the value of the weightQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeightQualifier() {
        return weightQualifier;
    }

    /**
     * Sets the value of the weightQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeightQualifier(String value) {
        this.weightQualifier = value;
    }

    /**
     * Gets the value of the scaleWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getScaleWeight() {
        return scaleWeight;
    }

    /**
     * Sets the value of the scaleWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setScaleWeight(GLogXMLWeightType value) {
        this.scaleWeight = value;
    }

    /**
     * Gets the value of the tareWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Sets the value of the tareWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Gets the value of the scaleLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleLocation() {
        return scaleLocation;
    }

    /**
     * Sets the value of the scaleLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleLocation(String value) {
        this.scaleLocation = value;
    }

    /**
     * Gets the value of the scaleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleName() {
        return scaleName;
    }

    /**
     * Sets the value of the scaleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleName(String value) {
        this.scaleName = value;
    }

    /**
     * Gets the value of the scaleTicket property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaleTicket() {
        return scaleTicket;
    }

    /**
     * Sets the value of the scaleTicket property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaleTicket(String value) {
        this.scaleTicket = value;
    }

    /**
     * Gets the value of the intermodalEquipLength property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntermodalEquipLength() {
        return intermodalEquipLength;
    }

    /**
     * Sets the value of the intermodalEquipLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntermodalEquipLength(String value) {
        this.intermodalEquipLength = value;
    }

    /**
     * Gets the value of the substituteEquipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSubstituteEquipmentGroupGid() {
        return substituteEquipmentGroupGid;
    }

    /**
     * Sets the value of the substituteEquipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSubstituteEquipmentGroupGid(GLogXMLGidType value) {
        this.substituteEquipmentGroupGid = value;
    }

    /**
     * Gets the value of the loadConfigVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getLoadConfigVolume() {
        return loadConfigVolume;
    }

    /**
     * Sets the value of the loadConfigVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setLoadConfigVolume(GLogXMLVolumeType value) {
        this.loadConfigVolume = value;
    }

    /**
     * Gets the value of the shipmentSEquipmentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentSEquipmentInfoType }
     *     
     */
    public ShipmentSEquipmentInfoType getShipmentSEquipmentInfo() {
        return shipmentSEquipmentInfo;
    }

    /**
     * Sets the value of the shipmentSEquipmentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentSEquipmentInfoType }
     *     
     */
    public void setShipmentSEquipmentInfo(ShipmentSEquipmentInfoType value) {
        this.shipmentSEquipmentInfo = value;
    }

    /**
     * Gets the value of the equipment property.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentType }
     *     
     */
    public EquipmentType getEquipment() {
        return equipment;
    }

    /**
     * Sets the value of the equipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentType }
     *     
     */
    public void setEquipment(EquipmentType value) {
        this.equipment = value;
    }

    /**
     * Gets the value of the equipmentType property.
     * 
     * @return
     *     possible object is
     *     {@link EquipmentDescriptionType }
     *     
     */
    public EquipmentDescriptionType getEquipmentType() {
        return equipmentType;
    }

    /**
     * Sets the value of the equipmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipmentDescriptionType }
     *     
     */
    public void setEquipmentType(EquipmentDescriptionType value) {
        this.equipmentType = value;
    }

    /**
     * Gets the value of the isFreight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFreight() {
        return isFreight;
    }

    /**
     * Sets the value of the isFreight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFreight(String value) {
        this.isFreight = value;
    }

    /**
     * Gets the value of the checkDigit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckDigit() {
        return checkDigit;
    }

    /**
     * Sets the value of the checkDigit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckDigit(String value) {
        this.checkDigit = value;
    }

    /**
     * Gets the value of the licensePlate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicensePlate() {
        return licensePlate;
    }

    /**
     * Sets the value of the licensePlate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicensePlate(String value) {
        this.licensePlate = value;
    }

    /**
     * Gets the value of the freightCGLength property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getFreightCGLength() {
        return freightCGLength;
    }

    /**
     * Sets the value of the freightCGLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setFreightCGLength(GLogXMLLengthType value) {
        this.freightCGLength = value;
    }

    /**
     * Gets the value of the freightCGWidth property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public GLogXMLWidthType getFreightCGWidth() {
        return freightCGWidth;
    }

    /**
     * Sets the value of the freightCGWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public void setFreightCGWidth(GLogXMLWidthType value) {
        this.freightCGWidth = value;
    }

    /**
     * Gets the value of the freightCGHeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public GLogXMLHeightType getFreightCGHeight() {
        return freightCGHeight;
    }

    /**
     * Sets the value of the freightCGHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public void setFreightCGHeight(GLogXMLHeightType value) {
        this.freightCGHeight = value;
    }

}

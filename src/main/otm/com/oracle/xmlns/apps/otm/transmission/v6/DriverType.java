
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Defines Driver information
 * 
 * <p>Java class for DriverType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DriverType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="DriverGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="DriverHeader" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverHeaderType" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DriverTeam" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverTeamType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DriverSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DriverCdl" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverCdlType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DriverCalendar" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverCalendarType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DriverPowerUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverPowerUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DriverAssignment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DriverAssignmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DriverType", propOrder = {
    "intSavedQuery",
    "driverGid",
    "transactionCode",
    "replaceChildren",
    "driverHeader",
    "status",
    "driverTeam",
    "driverSpecialService",
    "refnum",
    "remark",
    "driverCdl",
    "involvedParty",
    "driverCalendar",
    "driverPowerUnit",
    "driverAssignment",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates"
})
public class DriverType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "DriverGid")
    protected GLogXMLGidType driverGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "DriverHeader")
    protected DriverHeaderType driverHeader;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "DriverTeam")
    protected List<DriverTeamType> driverTeam;
    @XmlElement(name = "DriverSpecialService")
    protected List<DriverSpecialServiceType> driverSpecialService;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "DriverCdl")
    protected List<DriverCdlType> driverCdl;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "DriverCalendar")
    protected List<DriverCalendarType> driverCalendar;
    @XmlElement(name = "DriverPowerUnit")
    protected List<DriverPowerUnitType> driverPowerUnit;
    @XmlElement(name = "DriverAssignment")
    protected List<DriverAssignmentType> driverAssignment;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the driverGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDriverGid() {
        return driverGid;
    }

    /**
     * Sets the value of the driverGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDriverGid(GLogXMLGidType value) {
        this.driverGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the driverHeader property.
     * 
     * @return
     *     possible object is
     *     {@link DriverHeaderType }
     *     
     */
    public DriverHeaderType getDriverHeader() {
        return driverHeader;
    }

    /**
     * Sets the value of the driverHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link DriverHeaderType }
     *     
     */
    public void setDriverHeader(DriverHeaderType value) {
        this.driverHeader = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the driverTeam property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the driverTeam property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverTeam().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DriverTeamType }
     * 
     * 
     */
    public List<DriverTeamType> getDriverTeam() {
        if (driverTeam == null) {
            driverTeam = new ArrayList<DriverTeamType>();
        }
        return this.driverTeam;
    }

    /**
     * Gets the value of the driverSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the driverSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DriverSpecialServiceType }
     * 
     * 
     */
    public List<DriverSpecialServiceType> getDriverSpecialService() {
        if (driverSpecialService == null) {
            driverSpecialService = new ArrayList<DriverSpecialServiceType>();
        }
        return this.driverSpecialService;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the driverCdl property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the driverCdl property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverCdl().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DriverCdlType }
     * 
     * 
     */
    public List<DriverCdlType> getDriverCdl() {
        if (driverCdl == null) {
            driverCdl = new ArrayList<DriverCdlType>();
        }
        return this.driverCdl;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the driverCalendar property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the driverCalendar property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverCalendar().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DriverCalendarType }
     * 
     * 
     */
    public List<DriverCalendarType> getDriverCalendar() {
        if (driverCalendar == null) {
            driverCalendar = new ArrayList<DriverCalendarType>();
        }
        return this.driverCalendar;
    }

    /**
     * Gets the value of the driverPowerUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the driverPowerUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverPowerUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DriverPowerUnitType }
     * 
     * 
     */
    public List<DriverPowerUnitType> getDriverPowerUnit() {
        if (driverPowerUnit == null) {
            driverPowerUnit = new ArrayList<DriverPowerUnitType>();
        }
        return this.driverPowerUnit;
    }

    /**
     * Gets the value of the driverAssignment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the driverAssignment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDriverAssignment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DriverAssignmentType }
     * 
     * 
     */
    public List<DriverAssignmentType> getDriverAssignment() {
        if (driverAssignment == null) {
            driverAssignment = new ArrayList<DriverAssignmentType>();
        }
        return this.driverAssignment;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             TariffRefnum is an element of FP_Tariff used to identify the tariff.
 *          
 * 
 * <p>Java class for TariffRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TariffRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TariffRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TariffRefnumQualifier" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ContractSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TariffRefnumType", propOrder = {
    "tariffRefnumValue",
    "tariffRefnumQualifier",
    "contractSuffix"
})
public class TariffRefnumType {

    @XmlElement(name = "TariffRefnumValue", required = true)
    protected String tariffRefnumValue;
    @XmlElement(name = "TariffRefnumQualifier", required = true)
    protected String tariffRefnumQualifier;
    @XmlElement(name = "ContractSuffix", required = true)
    protected String contractSuffix;

    /**
     * Gets the value of the tariffRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumValue() {
        return tariffRefnumValue;
    }

    /**
     * Sets the value of the tariffRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumValue(String value) {
        this.tariffRefnumValue = value;
    }

    /**
     * Gets the value of the tariffRefnumQualifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffRefnumQualifier() {
        return tariffRefnumQualifier;
    }

    /**
     * Sets the value of the tariffRefnumQualifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffRefnumQualifier(String value) {
        this.tariffRefnumQualifier = value;
    }

    /**
     * Gets the value of the contractSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractSuffix() {
        return contractSuffix;
    }

    /**
     * Sets the value of the contractSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractSuffix(String value) {
        this.contractSuffix = value;
    }

}

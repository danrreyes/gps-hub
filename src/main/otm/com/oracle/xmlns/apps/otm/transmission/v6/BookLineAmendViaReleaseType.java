
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the set of Booking Line Amendment Changes from a order centric view. This can be sent to notify the
 *             planner, or another interested party of an order, with the list of amendment changes.
 *          
 * 
 * <p>Java class for BookLineAmendViaReleaseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BookLineAmendViaReleaseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BookLineAmendDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}BookLineAmendDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BookLineAmendViaReleaseType", propOrder = {
    "releaseGid",
    "bookLineAmendDetail"
})
public class BookLineAmendViaReleaseType {

    @XmlElement(name = "ReleaseGid")
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "BookLineAmendDetail")
    protected List<BookLineAmendDetailType> bookLineAmendDetail;

    /**
     * Gets the value of the releaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Sets the value of the releaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Gets the value of the bookLineAmendDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the bookLineAmendDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBookLineAmendDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BookLineAmendDetailType }
     * 
     * 
     */
    public List<BookLineAmendDetailType> getBookLineAmendDetail() {
        if (bookLineAmendDetail == null) {
            bookLineAmendDetail = new ArrayList<BookLineAmendDetailType>();
        }
        return this.bookLineAmendDetail;
    }

}

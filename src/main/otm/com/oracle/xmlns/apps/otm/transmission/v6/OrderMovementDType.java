
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Order Movement Details. It contains all the s ship units in an order movement
 *          
 * 
 * <p>Java class for OrderMovementDType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderMovementDType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderMovementDType", propOrder = {
    "shipUnitGid",
    "shipUnit"
})
public class OrderMovementDType {

    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "ShipUnit")
    protected ShipUnitType shipUnit;

    /**
     * Gets the value of the shipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Sets the value of the shipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Gets the value of the shipUnit property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitType }
     *     
     */
    public ShipUnitType getShipUnit() {
        return shipUnit;
    }

    /**
     * Sets the value of the shipUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitType }
     *     
     */
    public void setShipUnit(ShipUnitType value) {
        this.shipUnit = value;
    }

}

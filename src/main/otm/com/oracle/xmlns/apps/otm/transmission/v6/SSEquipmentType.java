
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             SSEquipment is a structure containing various equipment elements
 *          
 * 
 * <p>Java class for SSEquipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSEquipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EquipmentPrefix" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentIdentificationNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentStatusCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ISOEquipmentTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSEquipmentType", propOrder = {
    "equipmentPrefix",
    "equipmentIdentificationNum",
    "equipmentStatusCode",
    "isoEquipmentTypeCode"
})
public class SSEquipmentType {

    @XmlElement(name = "EquipmentPrefix")
    protected String equipmentPrefix;
    @XmlElement(name = "EquipmentIdentificationNum")
    protected String equipmentIdentificationNum;
    @XmlElement(name = "EquipmentStatusCode")
    protected String equipmentStatusCode;
    @XmlElement(name = "ISOEquipmentTypeCode")
    protected String isoEquipmentTypeCode;

    /**
     * Gets the value of the equipmentPrefix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentPrefix() {
        return equipmentPrefix;
    }

    /**
     * Sets the value of the equipmentPrefix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentPrefix(String value) {
        this.equipmentPrefix = value;
    }

    /**
     * Gets the value of the equipmentIdentificationNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentIdentificationNum() {
        return equipmentIdentificationNum;
    }

    /**
     * Sets the value of the equipmentIdentificationNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentIdentificationNum(String value) {
        this.equipmentIdentificationNum = value;
    }

    /**
     * Gets the value of the equipmentStatusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentStatusCode() {
        return equipmentStatusCode;
    }

    /**
     * Sets the value of the equipmentStatusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentStatusCode(String value) {
        this.equipmentStatusCode = value;
    }

    /**
     * Gets the value of the isoEquipmentTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getISOEquipmentTypeCode() {
        return isoEquipmentTypeCode;
    }

    /**
     * Sets the value of the isoEquipmentTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setISOEquipmentTypeCode(String value) {
        this.isoEquipmentTypeCode = value;
    }

}

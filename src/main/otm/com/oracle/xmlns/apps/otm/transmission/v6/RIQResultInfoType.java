
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies additional rate details information for the RIQ Query Result.
 * 
 * <p>Java class for RIQResultInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RIQResultInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RATE_OFFERING" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RateOfferingType" minOccurs="0"/&gt;
 *         &lt;element name="RATE_GEO" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RateGeoType" minOccurs="0"/&gt;
 *         &lt;element name="RouteCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RouteCodeType" minOccurs="0"/&gt;
 *         &lt;element name="EquipMarks" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipMarksType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQResultInfoType", propOrder = {
    "rateoffering",
    "rategeo",
    "routeCode",
    "equipMarks"
})
public class RIQResultInfoType {

    @XmlElement(name = "RATE_OFFERING")
    protected RateOfferingType rateoffering;
    @XmlElement(name = "RATE_GEO")
    protected RateGeoType rategeo;
    @XmlElement(name = "RouteCode")
    protected RouteCodeType routeCode;
    @XmlElement(name = "EquipMarks")
    protected EquipMarksType equipMarks;

    /**
     * Gets the value of the rateoffering property.
     * 
     * @return
     *     possible object is
     *     {@link RateOfferingType }
     *     
     */
    public RateOfferingType getRATEOFFERING() {
        return rateoffering;
    }

    /**
     * Sets the value of the rateoffering property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateOfferingType }
     *     
     */
    public void setRATEOFFERING(RateOfferingType value) {
        this.rateoffering = value;
    }

    /**
     * Gets the value of the rategeo property.
     * 
     * @return
     *     possible object is
     *     {@link RateGeoType }
     *     
     */
    public RateGeoType getRATEGEO() {
        return rategeo;
    }

    /**
     * Sets the value of the rategeo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RateGeoType }
     *     
     */
    public void setRATEGEO(RateGeoType value) {
        this.rategeo = value;
    }

    /**
     * Gets the value of the routeCode property.
     * 
     * @return
     *     possible object is
     *     {@link RouteCodeType }
     *     
     */
    public RouteCodeType getRouteCode() {
        return routeCode;
    }

    /**
     * Sets the value of the routeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link RouteCodeType }
     *     
     */
    public void setRouteCode(RouteCodeType value) {
        this.routeCode = value;
    }

    /**
     * Gets the value of the equipMarks property.
     * 
     * @return
     *     possible object is
     *     {@link EquipMarksType }
     *     
     */
    public EquipMarksType getEquipMarks() {
        return equipMarks;
    }

    /**
     * Sets the value of the equipMarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipMarksType }
     *     
     */
    public void setEquipMarks(EquipMarksType value) {
        this.equipMarks = value;
    }

}

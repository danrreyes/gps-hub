
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Contains reference information from the Release for the Accrual.
 *          
 * 
 * <p>Java class for AccrualReleaseInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccrualReleaseInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EarlyPickupDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccrualReleaseInfoType", propOrder = {
    "earlyPickupDt",
    "releaseRefnum"
})
public class AccrualReleaseInfoType {

    @XmlElement(name = "EarlyPickupDt")
    protected GLogDateTimeType earlyPickupDt;
    @XmlElement(name = "ReleaseRefnum")
    protected List<ReleaseRefnumType> releaseRefnum;

    /**
     * Gets the value of the earlyPickupDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlyPickupDt() {
        return earlyPickupDt;
    }

    /**
     * Sets the value of the earlyPickupDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlyPickupDt(GLogDateTimeType value) {
        this.earlyPickupDt = value;
    }

    /**
     * Gets the value of the releaseRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseRefnumType }
     * 
     * 
     */
    public List<ReleaseRefnumType> getReleaseRefnum() {
        if (releaseRefnum == null) {
            releaseRefnum = new ArrayList<ReleaseRefnumType>();
        }
        return this.releaseRefnum;
    }

}

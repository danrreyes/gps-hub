
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Demurrage Transaction Lines. It captures the chargeable days and cost for demurrage transaction.
 *             Example include for the PVT car, its possible that there are both storage and detention charges
 *             applied for a demurrage transaction.
 *          
 * 
 * <p>Java class for DemurrageTransactionLineitemType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionLineitemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DwellDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CreditDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DebitDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsSystemGenerated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ChargeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SecondaryChargeRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionLineitemType", propOrder = {
    "specialServiceGid",
    "dwellDays",
    "creditDays",
    "debitDays",
    "adjustmentReasonGid",
    "isSystemGenerated",
    "rateOfferingGid",
    "rateGeoGid",
    "shipmentGid",
    "chargeType",
    "secondaryChargeRuleGid",
    "cost"
})
public class DemurrageTransactionLineitemType {

    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "DwellDays")
    protected String dwellDays;
    @XmlElement(name = "CreditDays")
    protected String creditDays;
    @XmlElement(name = "DebitDays")
    protected String debitDays;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "IsSystemGenerated")
    protected String isSystemGenerated;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "ShipmentGid")
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "ChargeType")
    protected String chargeType;
    @XmlElement(name = "SecondaryChargeRuleGid")
    protected GLogXMLGidType secondaryChargeRuleGid;
    @XmlElement(name = "Cost")
    protected GLogXMLFinancialAmountType cost;

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Sets the value of the specialServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Gets the value of the dwellDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDwellDays() {
        return dwellDays;
    }

    /**
     * Sets the value of the dwellDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDwellDays(String value) {
        this.dwellDays = value;
    }

    /**
     * Gets the value of the creditDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditDays() {
        return creditDays;
    }

    /**
     * Sets the value of the creditDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditDays(String value) {
        this.creditDays = value;
    }

    /**
     * Gets the value of the debitDays property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitDays() {
        return debitDays;
    }

    /**
     * Sets the value of the debitDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitDays(String value) {
        this.debitDays = value;
    }

    /**
     * Gets the value of the adjustmentReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Sets the value of the adjustmentReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Gets the value of the isSystemGenerated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSystemGenerated() {
        return isSystemGenerated;
    }

    /**
     * Sets the value of the isSystemGenerated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSystemGenerated(String value) {
        this.isSystemGenerated = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the rateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Sets the value of the rateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the chargeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the value of the chargeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeType(String value) {
        this.chargeType = value;
    }

    /**
     * Gets the value of the secondaryChargeRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSecondaryChargeRuleGid() {
        return secondaryChargeRuleGid;
    }

    /**
     * Sets the value of the secondaryChargeRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSecondaryChargeRuleGid(GLogXMLGidType value) {
        this.secondaryChargeRuleGid = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

}

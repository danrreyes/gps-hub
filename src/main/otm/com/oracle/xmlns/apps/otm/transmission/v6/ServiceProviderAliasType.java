
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ServiceProviderAlias is an alternate identifier for a service provider.
 * 
 * <p>Java class for ServiceProviderAliasType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceProviderAliasType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceProviderAliasQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ServiceProviderAliasValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderAliasType", propOrder = {
    "serviceProviderAliasQualifierGid",
    "serviceProviderAliasValue"
})
public class ServiceProviderAliasType {

    @XmlElement(name = "ServiceProviderAliasQualifierGid", required = true)
    protected GLogXMLGidType serviceProviderAliasQualifierGid;
    @XmlElement(name = "ServiceProviderAliasValue", required = true)
    protected String serviceProviderAliasValue;

    /**
     * Gets the value of the serviceProviderAliasQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderAliasQualifierGid() {
        return serviceProviderAliasQualifierGid;
    }

    /**
     * Sets the value of the serviceProviderAliasQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderAliasQualifierGid(GLogXMLGidType value) {
        this.serviceProviderAliasQualifierGid = value;
    }

    /**
     * Gets the value of the serviceProviderAliasValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceProviderAliasValue() {
        return serviceProviderAliasValue;
    }

    /**
     * Sets the value of the serviceProviderAliasValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceProviderAliasValue(String value) {
        this.serviceProviderAliasValue = value;
    }

}

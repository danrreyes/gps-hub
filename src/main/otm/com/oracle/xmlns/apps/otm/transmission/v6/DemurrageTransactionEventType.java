
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Demurrage Transaction Event. These includes all the events (notify, outgate, ingate,
 *             released, picked etc.) between start and end of a demurrage transaction that match based on
 *             asset, location and time window (Mandatory).
 *          
 * 
 * <p>Java class for DemurrageTransactionEventType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DemurrageTransactionEventType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="EventStatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DmEventDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DemurrageTransactionEventType", propOrder = {
    "eventStatusCodeGid",
    "dmEventDate"
})
public class DemurrageTransactionEventType {

    @XmlElement(name = "EventStatusCodeGid")
    protected GLogXMLGidType eventStatusCodeGid;
    @XmlElement(name = "DmEventDate")
    protected GLogDateTimeType dmEventDate;

    /**
     * Gets the value of the eventStatusCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEventStatusCodeGid() {
        return eventStatusCodeGid;
    }

    /**
     * Sets the value of the eventStatusCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEventStatusCodeGid(GLogXMLGidType value) {
        this.eventStatusCodeGid = value;
    }

    /**
     * Gets the value of the dmEventDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getDmEventDate() {
        return dmEventDate;
    }

    /**
     * Sets the value of the dmEventDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setDmEventDate(GLogDateTimeType value) {
        this.dmEventDate = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Volume is a group of fields for specifying volume.
 *             It includes sub-elements for specifying unit of measure and a volume value.
 *          
 * 
 * <p>Java class for VolumeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VolumeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VolumeValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="VolumeUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VolumeType", propOrder = {
    "volumeValue",
    "volumeUOMGid"
})
public class VolumeType {

    @XmlElement(name = "VolumeValue", required = true)
    protected String volumeValue;
    @XmlElement(name = "VolumeUOMGid", required = true)
    protected GLogXMLGidType volumeUOMGid;

    /**
     * Gets the value of the volumeValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVolumeValue() {
        return volumeValue;
    }

    /**
     * Sets the value of the volumeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVolumeValue(String value) {
        this.volumeValue = value;
    }

    /**
     * Gets the value of the volumeUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVolumeUOMGid() {
        return volumeUOMGid;
    }

    /**
     * Sets the value of the volumeUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVolumeUOMGid(GLogXMLGidType value) {
        this.volumeUOMGid = value;
    }

}

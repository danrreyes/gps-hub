
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies which documents should include a text template..
 * 
 * <p>Java class for TextDocumentDefinitionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TextDocumentDefinitionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DocumentDefinitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TextPatternOverride" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextDocumentDefinitionType", propOrder = {
    "documentDefinitionGid",
    "textPatternOverride"
})
public class TextDocumentDefinitionType {

    @XmlElement(name = "DocumentDefinitionGid", required = true)
    protected GLogXMLGidType documentDefinitionGid;
    @XmlElement(name = "TextPatternOverride")
    protected String textPatternOverride;

    /**
     * Gets the value of the documentDefinitionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentDefinitionGid() {
        return documentDefinitionGid;
    }

    /**
     * Sets the value of the documentDefinitionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentDefinitionGid(GLogXMLGidType value) {
        this.documentDefinitionGid = value;
    }

    /**
     * Gets the value of the textPatternOverride property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextPatternOverride() {
        return textPatternOverride;
    }

    /**
     * Sets the value of the textPatternOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextPatternOverride(String value) {
        this.textPatternOverride = value;
    }

}

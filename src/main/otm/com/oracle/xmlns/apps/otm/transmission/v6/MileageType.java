
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Use the Mileage element to specify the distance for a particular lane.
 *          
 * 
 * <p>Java class for MileageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MileageType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="XLaneRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}XLaneRefType"/&gt;
 *         &lt;element name="RateDistanceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="Distance" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DistanceType"/&gt;
 *         &lt;element name="ServiceTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MileageType", propOrder = {
    "sendReason",
    "xLaneRef",
    "rateDistanceGid",
    "transactionCode",
    "distance",
    "serviceTime"
})
public class MileageType
    extends OTMTransactionIn
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "XLaneRef", required = true)
    protected XLaneRefType xLaneRef;
    @XmlElement(name = "RateDistanceGid", required = true)
    protected GLogXMLGidType rateDistanceGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "Distance", required = true)
    protected DistanceType distance;
    @XmlElement(name = "ServiceTime")
    protected ServiceTimeType serviceTime;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the xLaneRef property.
     * 
     * @return
     *     possible object is
     *     {@link XLaneRefType }
     *     
     */
    public XLaneRefType getXLaneRef() {
        return xLaneRef;
    }

    /**
     * Sets the value of the xLaneRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link XLaneRefType }
     *     
     */
    public void setXLaneRef(XLaneRefType value) {
        this.xLaneRef = value;
    }

    /**
     * Gets the value of the rateDistanceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateDistanceGid() {
        return rateDistanceGid;
    }

    /**
     * Sets the value of the rateDistanceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateDistanceGid(GLogXMLGidType value) {
        this.rateDistanceGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the distance property.
     * 
     * @return
     *     possible object is
     *     {@link DistanceType }
     *     
     */
    public DistanceType getDistance() {
        return distance;
    }

    /**
     * Sets the value of the distance property.
     * 
     * @param value
     *     allowed object is
     *     {@link DistanceType }
     *     
     */
    public void setDistance(DistanceType value) {
        this.distance = value;
    }

    /**
     * Gets the value of the serviceTime property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceTimeType }
     *     
     */
    public ServiceTimeType getServiceTime() {
        return serviceTime;
    }

    /**
     * Sets the value of the serviceTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceTimeType }
     *     
     */
    public void setServiceTime(ServiceTimeType value) {
        this.serviceTime = value;
    }

}

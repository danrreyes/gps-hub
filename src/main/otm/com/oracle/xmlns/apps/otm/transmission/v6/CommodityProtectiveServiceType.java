
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * This element is used to define a Protective Service for a commodity.
 *             Protective service is used by the rail industry to communicate temperature
 *             requirements and is where the user communicates optimal temperature for the RR.
 * 
 *             In the Item element, this element is only supported on the outbound.
 *          
 * 
 * <p>Java class for CommodityProtectiveServiceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommodityProtectiveServiceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ProtectiveSvc" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProtectiveSvcType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommodityProtectiveServiceType", propOrder = {
    "sequenceNumber",
    "protectiveSvc"
})
public class CommodityProtectiveServiceType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "ProtectiveSvc", required = true)
    protected ProtectiveSvcType protectiveSvc;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the protectiveSvc property.
     * 
     * @return
     *     possible object is
     *     {@link ProtectiveSvcType }
     *     
     */
    public ProtectiveSvcType getProtectiveSvc() {
        return protectiveSvc;
    }

    /**
     * Sets the value of the protectiveSvc property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtectiveSvcType }
     *     
     */
    public void setProtectiveSvc(ProtectiveSvcType value) {
        this.protectiveSvc = value;
    }

}

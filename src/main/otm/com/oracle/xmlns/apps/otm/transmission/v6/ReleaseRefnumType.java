
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ReleaseRefnum is an alternate method for identifying a Release. It consists of a
 *             qualifier and a value.
 *          
 * 
 * <p>Java class for ReleaseRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReleaseRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ReleaseRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseRefnumType", propOrder = {
    "releaseRefnumQualifierGid",
    "releaseRefnumValue"
})
public class ReleaseRefnumType {

    @XmlElement(name = "ReleaseRefnumQualifierGid", required = true)
    protected GLogXMLGidType releaseRefnumQualifierGid;
    @XmlElement(name = "ReleaseRefnumValue", required = true)
    protected String releaseRefnumValue;

    /**
     * Gets the value of the releaseRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseRefnumQualifierGid() {
        return releaseRefnumQualifierGid;
    }

    /**
     * Sets the value of the releaseRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseRefnumQualifierGid(GLogXMLGidType value) {
        this.releaseRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the releaseRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseRefnumValue() {
        return releaseRefnumValue;
    }

    /**
     * Sets the value of the releaseRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseRefnumValue(String value) {
        this.releaseRefnumValue = value;
    }

}

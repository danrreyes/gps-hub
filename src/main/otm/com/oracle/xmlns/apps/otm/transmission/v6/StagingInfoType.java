
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StagingInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StagingInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StagingQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLIntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="StagingProcess" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLIntSavedQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StagingInfoType", propOrder = {
    "stagingQuery",
    "stagingProcess"
})
public class StagingInfoType {

    @XmlElement(name = "StagingQuery")
    protected GLogXMLIntSavedQueryType stagingQuery;
    @XmlElement(name = "StagingProcess")
    protected GLogXMLIntSavedQueryType stagingProcess;

    /**
     * Gets the value of the stagingQuery property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public GLogXMLIntSavedQueryType getStagingQuery() {
        return stagingQuery;
    }

    /**
     * Sets the value of the stagingQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public void setStagingQuery(GLogXMLIntSavedQueryType value) {
        this.stagingQuery = value;
    }

    /**
     * Gets the value of the stagingProcess property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public GLogXMLIntSavedQueryType getStagingProcess() {
        return stagingProcess;
    }

    /**
     * Sets the value of the stagingProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLIntSavedQueryType }
     *     
     */
    public void setStagingProcess(GLogXMLIntSavedQueryType value) {
        this.stagingProcess = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             The ReleaseHeader contains various order related attributes and control information.
 *          
 * 
 * <p>Java class for ReleaseHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReleaseName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExternalSystemId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="QuoteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="MovePerspectiveGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialTermsType" minOccurs="0"/&gt;
 *         &lt;element name="FinalCommercialTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinalCommercialTermsType" minOccurs="0"/&gt;
 *         &lt;element name="CommercialInvoiceTerms" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CommercialInvoiceTermsType" minOccurs="0"/&gt;
 *         &lt;element name="PlanningGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TimeWindowEmphasisGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="RateServiceProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="ServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="ServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="SellServiceProviderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="SellServiceProviderProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ShipWithGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TemplateType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SellRateOfferingGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SellRateGeoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FixedItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FixedSellItineraryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="MustShipDirect" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MustShipThruXDock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MustShipThruPool" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BundlingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsSplitAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsShipperKnown" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DimRateFactorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PickupRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DropoffRoutingSeqGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SellItineraryProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="InspectionAndSurveyInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InspectionAndSurveyInfoType" minOccurs="0"/&gt;
 *         &lt;element name="LetterOfCreditInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LetterOfCreditInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ImportLicenseInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ImportLicenseInfoType" minOccurs="0"/&gt;
 *         &lt;element name="ConsolidationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StuffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="DestuffLocation" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="StowageModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UnitizationConditionName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CustomerUnitizationRequest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsIgnoreLocationCalendar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsConsolidateREquipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UltimateDestCountryCode3Gid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BufferType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DutyPaid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OnRouteTempExec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPreEnteredPU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseSpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLegConstraint" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseLegConstraintType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OrderPriority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PickupRailCarrier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DeliveryRailCarrier" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RailRouteCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EmergPhoneNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldCurrencies" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldCurrencyType" minOccurs="0"/&gt;
 *         &lt;element name="EarlySailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LateSailDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseHeaderType", propOrder = {
    "releaseName",
    "externalSystemId",
    "releaseMethodGid",
    "quoteGid",
    "movePerspectiveGid",
    "commercialTerms",
    "finalCommercialTerms",
    "commercialInvoiceTerms",
    "planningGroupGid",
    "timeWindowEmphasisGid",
    "rateServiceGid",
    "rateServiceProfileGid",
    "serviceProviderGid",
    "serviceProviderProfileGid",
    "sellServiceProviderGid",
    "sellServiceProviderProfileGid",
    "transportModeGid",
    "modeProfileGid",
    "equipmentGroupGid",
    "equipmentGroupProfileGid",
    "shipWithGroup",
    "isTemplate",
    "templateType",
    "rateOfferingGid",
    "rateGeoGid",
    "sellRateOfferingGid",
    "sellRateGeoGid",
    "fixedItineraryGid",
    "fixedSellItineraryGid",
    "mustShipDirect",
    "mustShipThruXDock",
    "mustShipThruPool",
    "bundlingType",
    "isSplitAllowed",
    "isShipperKnown",
    "dimRateFactorGid",
    "pickupRoutingSeqGid",
    "dropoffRoutingSeqGid",
    "itineraryProfileGid",
    "sellItineraryProfileGid",
    "inspectionAndSurveyInfo",
    "letterOfCreditInfo",
    "importLicenseInfo",
    "consolidationTypeGid",
    "stuffLocation",
    "destuffLocation",
    "stowageModeGid",
    "unitizationConditionName",
    "customerUnitizationRequest",
    "isIgnoreLocationCalendar",
    "isConsolidateREquipment",
    "ultimateDestCountryCode3Gid",
    "bufferType",
    "dutyPaid",
    "onRouteTempExec",
    "isPreEnteredPU",
    "accessorialCodeGid",
    "releaseSpecialService",
    "releaseLegConstraint",
    "orderPriority",
    "equipmentTypeGid",
    "pickupRailCarrier",
    "deliveryRailCarrier",
    "railRouteCodeGid",
    "emergPhoneNum",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "flexFieldCurrencies",
    "earlySailDt",
    "lateSailDt",
    "voyageGid"
})
public class ReleaseHeaderType {

    @XmlElement(name = "ReleaseName")
    protected String releaseName;
    @XmlElement(name = "ExternalSystemId")
    protected String externalSystemId;
    @XmlElement(name = "ReleaseMethodGid")
    protected GLogXMLGidType releaseMethodGid;
    @XmlElement(name = "QuoteGid")
    protected GLogXMLGidType quoteGid;
    @XmlElement(name = "MovePerspectiveGid")
    protected GLogXMLGidType movePerspectiveGid;
    @XmlElement(name = "CommercialTerms")
    protected CommercialTermsType commercialTerms;
    @XmlElement(name = "FinalCommercialTerms")
    protected FinalCommercialTermsType finalCommercialTerms;
    @XmlElement(name = "CommercialInvoiceTerms")
    protected CommercialInvoiceTermsType commercialInvoiceTerms;
    @XmlElement(name = "PlanningGroupGid")
    protected GLogXMLGidType planningGroupGid;
    @XmlElement(name = "TimeWindowEmphasisGid")
    protected GLogXMLGidType timeWindowEmphasisGid;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "RateServiceProfileGid")
    protected GLogXMLGidType rateServiceProfileGid;
    @XmlElement(name = "ServiceProviderGid")
    protected GLogXMLGidType serviceProviderGid;
    @XmlElement(name = "ServiceProviderProfileGid")
    protected GLogXMLGidType serviceProviderProfileGid;
    @XmlElement(name = "SellServiceProviderGid")
    protected GLogXMLGidType sellServiceProviderGid;
    @XmlElement(name = "SellServiceProviderProfileGid")
    protected GLogXMLGidType sellServiceProviderProfileGid;
    @XmlElement(name = "TransportModeGid")
    protected GLogXMLGidType transportModeGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "EquipmentGroupGid")
    protected GLogXMLGidType equipmentGroupGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "ShipWithGroup")
    protected String shipWithGroup;
    @XmlElement(name = "IsTemplate")
    protected String isTemplate;
    @XmlElement(name = "TemplateType")
    protected String templateType;
    @XmlElement(name = "RateOfferingGid")
    protected GLogXMLGidType rateOfferingGid;
    @XmlElement(name = "RateGeoGid")
    protected GLogXMLGidType rateGeoGid;
    @XmlElement(name = "SellRateOfferingGid")
    protected GLogXMLGidType sellRateOfferingGid;
    @XmlElement(name = "SellRateGeoGid")
    protected GLogXMLGidType sellRateGeoGid;
    @XmlElement(name = "FixedItineraryGid")
    protected GLogXMLGidType fixedItineraryGid;
    @XmlElement(name = "FixedSellItineraryGid")
    protected GLogXMLGidType fixedSellItineraryGid;
    @XmlElement(name = "MustShipDirect")
    protected String mustShipDirect;
    @XmlElement(name = "MustShipThruXDock")
    protected String mustShipThruXDock;
    @XmlElement(name = "MustShipThruPool")
    protected String mustShipThruPool;
    @XmlElement(name = "BundlingType")
    protected String bundlingType;
    @XmlElement(name = "IsSplitAllowed")
    protected String isSplitAllowed;
    @XmlElement(name = "IsShipperKnown")
    protected String isShipperKnown;
    @XmlElement(name = "DimRateFactorGid")
    protected GLogXMLGidType dimRateFactorGid;
    @XmlElement(name = "PickupRoutingSeqGid")
    protected GLogXMLGidType pickupRoutingSeqGid;
    @XmlElement(name = "DropoffRoutingSeqGid")
    protected GLogXMLGidType dropoffRoutingSeqGid;
    @XmlElement(name = "ItineraryProfileGid")
    protected GLogXMLGidType itineraryProfileGid;
    @XmlElement(name = "SellItineraryProfileGid")
    protected GLogXMLGidType sellItineraryProfileGid;
    @XmlElement(name = "InspectionAndSurveyInfo")
    protected InspectionAndSurveyInfoType inspectionAndSurveyInfo;
    @XmlElement(name = "LetterOfCreditInfo")
    protected LetterOfCreditInfoType letterOfCreditInfo;
    @XmlElement(name = "ImportLicenseInfo")
    protected ImportLicenseInfoType importLicenseInfo;
    @XmlElement(name = "ConsolidationTypeGid")
    protected GLogXMLGidType consolidationTypeGid;
    @XmlElement(name = "StuffLocation")
    protected GLogXMLLocRefType stuffLocation;
    @XmlElement(name = "DestuffLocation")
    protected GLogXMLLocRefType destuffLocation;
    @XmlElement(name = "StowageModeGid")
    protected GLogXMLGidType stowageModeGid;
    @XmlElement(name = "UnitizationConditionName")
    protected String unitizationConditionName;
    @XmlElement(name = "CustomerUnitizationRequest")
    protected String customerUnitizationRequest;
    @XmlElement(name = "IsIgnoreLocationCalendar")
    protected String isIgnoreLocationCalendar;
    @XmlElement(name = "IsConsolidateREquipment")
    protected String isConsolidateREquipment;
    @XmlElement(name = "UltimateDestCountryCode3Gid")
    protected GLogXMLGidType ultimateDestCountryCode3Gid;
    @XmlElement(name = "BufferType")
    protected String bufferType;
    @XmlElement(name = "DutyPaid")
    protected String dutyPaid;
    @XmlElement(name = "OnRouteTempExec")
    protected String onRouteTempExec;
    @XmlElement(name = "IsPreEnteredPU")
    protected String isPreEnteredPU;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "ReleaseSpecialService")
    protected List<ReleaseSpecialServiceType> releaseSpecialService;
    @XmlElement(name = "ReleaseLegConstraint")
    protected List<ReleaseLegConstraintType> releaseLegConstraint;
    @XmlElement(name = "OrderPriority")
    protected String orderPriority;
    @XmlElement(name = "EquipmentTypeGid")
    protected GLogXMLGidType equipmentTypeGid;
    @XmlElement(name = "PickupRailCarrier")
    protected GLogXMLGidType pickupRailCarrier;
    @XmlElement(name = "DeliveryRailCarrier")
    protected GLogXMLGidType deliveryRailCarrier;
    @XmlElement(name = "RailRouteCodeGid")
    protected GLogXMLGidType railRouteCodeGid;
    @XmlElement(name = "EmergPhoneNum")
    protected String emergPhoneNum;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "FlexFieldCurrencies")
    protected FlexFieldCurrencyType flexFieldCurrencies;
    @XmlElement(name = "EarlySailDt")
    protected GLogDateTimeType earlySailDt;
    @XmlElement(name = "LateSailDt")
    protected GLogDateTimeType lateSailDt;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;

    /**
     * Gets the value of the releaseName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseName() {
        return releaseName;
    }

    /**
     * Sets the value of the releaseName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseName(String value) {
        this.releaseName = value;
    }

    /**
     * Gets the value of the externalSystemId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalSystemId() {
        return externalSystemId;
    }

    /**
     * Sets the value of the externalSystemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalSystemId(String value) {
        this.externalSystemId = value;
    }

    /**
     * Gets the value of the releaseMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseMethodGid() {
        return releaseMethodGid;
    }

    /**
     * Sets the value of the releaseMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseMethodGid(GLogXMLGidType value) {
        this.releaseMethodGid = value;
    }

    /**
     * Gets the value of the quoteGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuoteGid() {
        return quoteGid;
    }

    /**
     * Sets the value of the quoteGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuoteGid(GLogXMLGidType value) {
        this.quoteGid = value;
    }

    /**
     * Gets the value of the movePerspectiveGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMovePerspectiveGid() {
        return movePerspectiveGid;
    }

    /**
     * Sets the value of the movePerspectiveGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMovePerspectiveGid(GLogXMLGidType value) {
        this.movePerspectiveGid = value;
    }

    /**
     * Gets the value of the commercialTerms property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialTermsType }
     *     
     */
    public CommercialTermsType getCommercialTerms() {
        return commercialTerms;
    }

    /**
     * Sets the value of the commercialTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialTermsType }
     *     
     */
    public void setCommercialTerms(CommercialTermsType value) {
        this.commercialTerms = value;
    }

    /**
     * Gets the value of the finalCommercialTerms property.
     * 
     * @return
     *     possible object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public FinalCommercialTermsType getFinalCommercialTerms() {
        return finalCommercialTerms;
    }

    /**
     * Sets the value of the finalCommercialTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinalCommercialTermsType }
     *     
     */
    public void setFinalCommercialTerms(FinalCommercialTermsType value) {
        this.finalCommercialTerms = value;
    }

    /**
     * Gets the value of the commercialInvoiceTerms property.
     * 
     * @return
     *     possible object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public CommercialInvoiceTermsType getCommercialInvoiceTerms() {
        return commercialInvoiceTerms;
    }

    /**
     * Sets the value of the commercialInvoiceTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommercialInvoiceTermsType }
     *     
     */
    public void setCommercialInvoiceTerms(CommercialInvoiceTermsType value) {
        this.commercialInvoiceTerms = value;
    }

    /**
     * Gets the value of the planningGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPlanningGroupGid() {
        return planningGroupGid;
    }

    /**
     * Sets the value of the planningGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPlanningGroupGid(GLogXMLGidType value) {
        this.planningGroupGid = value;
    }

    /**
     * Gets the value of the timeWindowEmphasisGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeWindowEmphasisGid() {
        return timeWindowEmphasisGid;
    }

    /**
     * Sets the value of the timeWindowEmphasisGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeWindowEmphasisGid(GLogXMLGidType value) {
        this.timeWindowEmphasisGid = value;
    }

    /**
     * Gets the value of the rateServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Sets the value of the rateServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Gets the value of the rateServiceProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceProfileGid() {
        return rateServiceProfileGid;
    }

    /**
     * Sets the value of the rateServiceProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceProfileGid(GLogXMLGidType value) {
        this.rateServiceProfileGid = value;
    }

    /**
     * Gets the value of the serviceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderGid() {
        return serviceProviderGid;
    }

    /**
     * Sets the value of the serviceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderGid(GLogXMLGidType value) {
        this.serviceProviderGid = value;
    }

    /**
     * Gets the value of the serviceProviderProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServiceProviderProfileGid() {
        return serviceProviderProfileGid;
    }

    /**
     * Sets the value of the serviceProviderProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServiceProviderProfileGid(GLogXMLGidType value) {
        this.serviceProviderProfileGid = value;
    }

    /**
     * Gets the value of the sellServiceProviderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellServiceProviderGid() {
        return sellServiceProviderGid;
    }

    /**
     * Sets the value of the sellServiceProviderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellServiceProviderGid(GLogXMLGidType value) {
        this.sellServiceProviderGid = value;
    }

    /**
     * Gets the value of the sellServiceProviderProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellServiceProviderProfileGid() {
        return sellServiceProviderProfileGid;
    }

    /**
     * Sets the value of the sellServiceProviderProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellServiceProviderProfileGid(GLogXMLGidType value) {
        this.sellServiceProviderProfileGid = value;
    }

    /**
     * Gets the value of the transportModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportModeGid() {
        return transportModeGid;
    }

    /**
     * Sets the value of the transportModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportModeGid(GLogXMLGidType value) {
        this.transportModeGid = value;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the equipmentGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupGid() {
        return equipmentGroupGid;
    }

    /**
     * Sets the value of the equipmentGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupGid(GLogXMLGidType value) {
        this.equipmentGroupGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the shipWithGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipWithGroup() {
        return shipWithGroup;
    }

    /**
     * Sets the value of the shipWithGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipWithGroup(String value) {
        this.shipWithGroup = value;
    }

    /**
     * Gets the value of the isTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsTemplate() {
        return isTemplate;
    }

    /**
     * Sets the value of the isTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsTemplate(String value) {
        this.isTemplate = value;
    }

    /**
     * Gets the value of the templateType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateType() {
        return templateType;
    }

    /**
     * Sets the value of the templateType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateType(String value) {
        this.templateType = value;
    }

    /**
     * Gets the value of the rateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateOfferingGid() {
        return rateOfferingGid;
    }

    /**
     * Sets the value of the rateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateOfferingGid(GLogXMLGidType value) {
        this.rateOfferingGid = value;
    }

    /**
     * Gets the value of the rateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoGid() {
        return rateGeoGid;
    }

    /**
     * Sets the value of the rateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoGid(GLogXMLGidType value) {
        this.rateGeoGid = value;
    }

    /**
     * Gets the value of the sellRateOfferingGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellRateOfferingGid() {
        return sellRateOfferingGid;
    }

    /**
     * Sets the value of the sellRateOfferingGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellRateOfferingGid(GLogXMLGidType value) {
        this.sellRateOfferingGid = value;
    }

    /**
     * Gets the value of the sellRateGeoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellRateGeoGid() {
        return sellRateGeoGid;
    }

    /**
     * Sets the value of the sellRateGeoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellRateGeoGid(GLogXMLGidType value) {
        this.sellRateGeoGid = value;
    }

    /**
     * Gets the value of the fixedItineraryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFixedItineraryGid() {
        return fixedItineraryGid;
    }

    /**
     * Sets the value of the fixedItineraryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFixedItineraryGid(GLogXMLGidType value) {
        this.fixedItineraryGid = value;
    }

    /**
     * Gets the value of the fixedSellItineraryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFixedSellItineraryGid() {
        return fixedSellItineraryGid;
    }

    /**
     * Sets the value of the fixedSellItineraryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFixedSellItineraryGid(GLogXMLGidType value) {
        this.fixedSellItineraryGid = value;
    }

    /**
     * Gets the value of the mustShipDirect property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipDirect() {
        return mustShipDirect;
    }

    /**
     * Sets the value of the mustShipDirect property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipDirect(String value) {
        this.mustShipDirect = value;
    }

    /**
     * Gets the value of the mustShipThruXDock property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipThruXDock() {
        return mustShipThruXDock;
    }

    /**
     * Sets the value of the mustShipThruXDock property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipThruXDock(String value) {
        this.mustShipThruXDock = value;
    }

    /**
     * Gets the value of the mustShipThruPool property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMustShipThruPool() {
        return mustShipThruPool;
    }

    /**
     * Sets the value of the mustShipThruPool property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMustShipThruPool(String value) {
        this.mustShipThruPool = value;
    }

    /**
     * Gets the value of the bundlingType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBundlingType() {
        return bundlingType;
    }

    /**
     * Sets the value of the bundlingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBundlingType(String value) {
        this.bundlingType = value;
    }

    /**
     * Gets the value of the isSplitAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSplitAllowed() {
        return isSplitAllowed;
    }

    /**
     * Sets the value of the isSplitAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSplitAllowed(String value) {
        this.isSplitAllowed = value;
    }

    /**
     * Gets the value of the isShipperKnown property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShipperKnown() {
        return isShipperKnown;
    }

    /**
     * Sets the value of the isShipperKnown property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShipperKnown(String value) {
        this.isShipperKnown = value;
    }

    /**
     * Gets the value of the dimRateFactorGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDimRateFactorGid() {
        return dimRateFactorGid;
    }

    /**
     * Sets the value of the dimRateFactorGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDimRateFactorGid(GLogXMLGidType value) {
        this.dimRateFactorGid = value;
    }

    /**
     * Gets the value of the pickupRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRoutingSeqGid() {
        return pickupRoutingSeqGid;
    }

    /**
     * Sets the value of the pickupRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRoutingSeqGid(GLogXMLGidType value) {
        this.pickupRoutingSeqGid = value;
    }

    /**
     * Gets the value of the dropoffRoutingSeqGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffRoutingSeqGid() {
        return dropoffRoutingSeqGid;
    }

    /**
     * Sets the value of the dropoffRoutingSeqGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffRoutingSeqGid(GLogXMLGidType value) {
        this.dropoffRoutingSeqGid = value;
    }

    /**
     * Gets the value of the itineraryProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItineraryProfileGid() {
        return itineraryProfileGid;
    }

    /**
     * Sets the value of the itineraryProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItineraryProfileGid(GLogXMLGidType value) {
        this.itineraryProfileGid = value;
    }

    /**
     * Gets the value of the sellItineraryProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSellItineraryProfileGid() {
        return sellItineraryProfileGid;
    }

    /**
     * Sets the value of the sellItineraryProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSellItineraryProfileGid(GLogXMLGidType value) {
        this.sellItineraryProfileGid = value;
    }

    /**
     * Gets the value of the inspectionAndSurveyInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InspectionAndSurveyInfoType }
     *     
     */
    public InspectionAndSurveyInfoType getInspectionAndSurveyInfo() {
        return inspectionAndSurveyInfo;
    }

    /**
     * Sets the value of the inspectionAndSurveyInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InspectionAndSurveyInfoType }
     *     
     */
    public void setInspectionAndSurveyInfo(InspectionAndSurveyInfoType value) {
        this.inspectionAndSurveyInfo = value;
    }

    /**
     * Gets the value of the letterOfCreditInfo property.
     * 
     * @return
     *     possible object is
     *     {@link LetterOfCreditInfoType }
     *     
     */
    public LetterOfCreditInfoType getLetterOfCreditInfo() {
        return letterOfCreditInfo;
    }

    /**
     * Sets the value of the letterOfCreditInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LetterOfCreditInfoType }
     *     
     */
    public void setLetterOfCreditInfo(LetterOfCreditInfoType value) {
        this.letterOfCreditInfo = value;
    }

    /**
     * Gets the value of the importLicenseInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ImportLicenseInfoType }
     *     
     */
    public ImportLicenseInfoType getImportLicenseInfo() {
        return importLicenseInfo;
    }

    /**
     * Sets the value of the importLicenseInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ImportLicenseInfoType }
     *     
     */
    public void setImportLicenseInfo(ImportLicenseInfoType value) {
        this.importLicenseInfo = value;
    }

    /**
     * Gets the value of the consolidationTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getConsolidationTypeGid() {
        return consolidationTypeGid;
    }

    /**
     * Sets the value of the consolidationTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setConsolidationTypeGid(GLogXMLGidType value) {
        this.consolidationTypeGid = value;
    }

    /**
     * Gets the value of the stuffLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getStuffLocation() {
        return stuffLocation;
    }

    /**
     * Sets the value of the stuffLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setStuffLocation(GLogXMLLocRefType value) {
        this.stuffLocation = value;
    }

    /**
     * Gets the value of the destuffLocation property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getDestuffLocation() {
        return destuffLocation;
    }

    /**
     * Sets the value of the destuffLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setDestuffLocation(GLogXMLLocRefType value) {
        this.destuffLocation = value;
    }

    /**
     * Gets the value of the stowageModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStowageModeGid() {
        return stowageModeGid;
    }

    /**
     * Sets the value of the stowageModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStowageModeGid(GLogXMLGidType value) {
        this.stowageModeGid = value;
    }

    /**
     * Gets the value of the unitizationConditionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitizationConditionName() {
        return unitizationConditionName;
    }

    /**
     * Sets the value of the unitizationConditionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitizationConditionName(String value) {
        this.unitizationConditionName = value;
    }

    /**
     * Gets the value of the customerUnitizationRequest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerUnitizationRequest() {
        return customerUnitizationRequest;
    }

    /**
     * Sets the value of the customerUnitizationRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerUnitizationRequest(String value) {
        this.customerUnitizationRequest = value;
    }

    /**
     * Gets the value of the isIgnoreLocationCalendar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsIgnoreLocationCalendar() {
        return isIgnoreLocationCalendar;
    }

    /**
     * Sets the value of the isIgnoreLocationCalendar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsIgnoreLocationCalendar(String value) {
        this.isIgnoreLocationCalendar = value;
    }

    /**
     * Gets the value of the isConsolidateREquipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsConsolidateREquipment() {
        return isConsolidateREquipment;
    }

    /**
     * Sets the value of the isConsolidateREquipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsConsolidateREquipment(String value) {
        this.isConsolidateREquipment = value;
    }

    /**
     * Gets the value of the ultimateDestCountryCode3Gid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUltimateDestCountryCode3Gid() {
        return ultimateDestCountryCode3Gid;
    }

    /**
     * Sets the value of the ultimateDestCountryCode3Gid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUltimateDestCountryCode3Gid(GLogXMLGidType value) {
        this.ultimateDestCountryCode3Gid = value;
    }

    /**
     * Gets the value of the bufferType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBufferType() {
        return bufferType;
    }

    /**
     * Sets the value of the bufferType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBufferType(String value) {
        this.bufferType = value;
    }

    /**
     * Gets the value of the dutyPaid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDutyPaid() {
        return dutyPaid;
    }

    /**
     * Sets the value of the dutyPaid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDutyPaid(String value) {
        this.dutyPaid = value;
    }

    /**
     * Gets the value of the onRouteTempExec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOnRouteTempExec() {
        return onRouteTempExec;
    }

    /**
     * Sets the value of the onRouteTempExec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOnRouteTempExec(String value) {
        this.onRouteTempExec = value;
    }

    /**
     * Gets the value of the isPreEnteredPU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPreEnteredPU() {
        return isPreEnteredPU;
    }

    /**
     * Sets the value of the isPreEnteredPU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPreEnteredPU(String value) {
        this.isPreEnteredPU = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the releaseSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseSpecialServiceType }
     * 
     * 
     */
    public List<ReleaseSpecialServiceType> getReleaseSpecialService() {
        if (releaseSpecialService == null) {
            releaseSpecialService = new ArrayList<ReleaseSpecialServiceType>();
        }
        return this.releaseSpecialService;
    }

    /**
     * Gets the value of the releaseLegConstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseLegConstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseLegConstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseLegConstraintType }
     * 
     * 
     */
    public List<ReleaseLegConstraintType> getReleaseLegConstraint() {
        if (releaseLegConstraint == null) {
            releaseLegConstraint = new ArrayList<ReleaseLegConstraintType>();
        }
        return this.releaseLegConstraint;
    }

    /**
     * Gets the value of the orderPriority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderPriority() {
        return orderPriority;
    }

    /**
     * Sets the value of the orderPriority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderPriority(String value) {
        this.orderPriority = value;
    }

    /**
     * Gets the value of the equipmentTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentTypeGid() {
        return equipmentTypeGid;
    }

    /**
     * Sets the value of the equipmentTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentTypeGid(GLogXMLGidType value) {
        this.equipmentTypeGid = value;
    }

    /**
     * Gets the value of the pickupRailCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupRailCarrier() {
        return pickupRailCarrier;
    }

    /**
     * Sets the value of the pickupRailCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupRailCarrier(GLogXMLGidType value) {
        this.pickupRailCarrier = value;
    }

    /**
     * Gets the value of the deliveryRailCarrier property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDeliveryRailCarrier() {
        return deliveryRailCarrier;
    }

    /**
     * Sets the value of the deliveryRailCarrier property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDeliveryRailCarrier(GLogXMLGidType value) {
        this.deliveryRailCarrier = value;
    }

    /**
     * Gets the value of the railRouteCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRailRouteCodeGid() {
        return railRouteCodeGid;
    }

    /**
     * Sets the value of the railRouteCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRailRouteCodeGid(GLogXMLGidType value) {
        this.railRouteCodeGid = value;
    }

    /**
     * Gets the value of the emergPhoneNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergPhoneNum() {
        return emergPhoneNum;
    }

    /**
     * Sets the value of the emergPhoneNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergPhoneNum(String value) {
        this.emergPhoneNum = value;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the flexFieldCurrencies property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public FlexFieldCurrencyType getFlexFieldCurrencies() {
        return flexFieldCurrencies;
    }

    /**
     * Sets the value of the flexFieldCurrencies property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldCurrencyType }
     *     
     */
    public void setFlexFieldCurrencies(FlexFieldCurrencyType value) {
        this.flexFieldCurrencies = value;
    }

    /**
     * Gets the value of the earlySailDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEarlySailDt() {
        return earlySailDt;
    }

    /**
     * Sets the value of the earlySailDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEarlySailDt(GLogDateTimeType value) {
        this.earlySailDt = value;
    }

    /**
     * Gets the value of the lateSailDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLateSailDt() {
        return lateSailDt;
    }

    /**
     * Sets the value of the lateSailDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLateSailDt(GLogDateTimeType value) {
        this.lateSailDt = value;
    }

    /**
     * Gets the value of the voyageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Sets the value of the voyageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

}

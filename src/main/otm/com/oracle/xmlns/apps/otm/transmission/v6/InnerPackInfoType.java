
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the information for the inner pack.
 * 
 * <p>Java class for InnerPackInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InnerPackInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="InnerPackSize" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFlexQuantityType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InnerPackInfoType", propOrder = {
    "packagedItemSpecRef",
    "innerPackSize"
})
public class InnerPackInfoType {

    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "InnerPackSize")
    protected GLogXMLFlexQuantityType innerPackSize;

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Gets the value of the innerPackSize property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public GLogXMLFlexQuantityType getInnerPackSize() {
        return innerPackSize;
    }

    /**
     * Sets the value of the innerPackSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFlexQuantityType }
     *     
     */
    public void setInnerPackSize(GLogXMLFlexQuantityType value) {
        this.innerPackSize = value;
    }

}

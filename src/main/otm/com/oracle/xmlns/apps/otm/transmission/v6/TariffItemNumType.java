
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * TariffItemNum is an element of FP_Tariff used to identify the tariff.
 * 
 * <p>Java class for TariffItemNumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TariffItemNumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TariffItemNumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ContractSuffix" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TariffItemNumType", propOrder = {
    "tariffItemNumValue",
    "contractSuffix"
})
public class TariffItemNumType {

    @XmlElement(name = "TariffItemNumValue", required = true)
    protected String tariffItemNumValue;
    @XmlElement(name = "ContractSuffix", required = true)
    protected String contractSuffix;

    /**
     * Gets the value of the tariffItemNumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTariffItemNumValue() {
        return tariffItemNumValue;
    }

    /**
     * Sets the value of the tariffItemNumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTariffItemNumValue(String value) {
        this.tariffItemNumValue = value;
    }

    /**
     * Gets the value of the contractSuffix property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContractSuffix() {
        return contractSuffix;
    }

    /**
     * Sets the value of the contractSuffix property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContractSuffix(String value) {
        this.contractSuffix = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             When the interface is to apply to order release status event, the StatusLevel element must be set to
 *             'RELEASE'
 *             otherwise the interface is assumed to apply to an order base status event.
 *          
 * 
 * <p>Java class for TransOrderStatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransOrderStatusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/&gt;
 *           &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StatusLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="OrderRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRefnumType" minOccurs="0"/&gt;
 *         &lt;element name="StatusCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="StatusReasonCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EventDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="SSContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SSContactType" minOccurs="0"/&gt;
 *         &lt;element name="SSRemarks" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="EventGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EventGroupType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="StatusGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusGroupType" minOccurs="0"/&gt;
 *         &lt;element name="ReasonGroup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReasonGroupType" minOccurs="0"/&gt;
 *         &lt;element name="QuickCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ResponsiblePartyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReportingUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SLine" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransOrderStatusType", propOrder = {
    "sendReason",
    "serviceProviderAlias",
    "intSavedQuery",
    "transOrderGid",
    "releaseGid",
    "description",
    "statusLevel",
    "orderRefnum",
    "statusCodeGid",
    "statusReasonCodeGid",
    "timeZoneGid",
    "eventDt",
    "ssContact",
    "ssRemarks",
    "eventGroup",
    "statusGroup",
    "reasonGroup",
    "quickCodeGid",
    "responsiblePartyGid",
    "reportingUser",
    "sLine"
})
public class TransOrderStatusType
    extends OTMTransactionIn
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "ServiceProviderAlias")
    protected ServiceProviderAliasType serviceProviderAlias;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransOrderGid")
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "ReleaseGid")
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "StatusLevel")
    protected String statusLevel;
    @XmlElement(name = "OrderRefnum")
    protected OrderRefnumType orderRefnum;
    @XmlElement(name = "StatusCodeGid")
    protected GLogXMLGidType statusCodeGid;
    @XmlElement(name = "StatusReasonCodeGid")
    protected GLogXMLGidType statusReasonCodeGid;
    @XmlElement(name = "TimeZoneGid")
    protected GLogXMLGidType timeZoneGid;
    @XmlElement(name = "EventDt")
    protected GLogDateTimeType eventDt;
    @XmlElement(name = "SSContact")
    protected SSContactType ssContact;
    @XmlElement(name = "SSRemarks")
    protected List<String> ssRemarks;
    @XmlElement(name = "EventGroup")
    protected List<EventGroupType> eventGroup;
    @XmlElement(name = "StatusGroup")
    protected StatusGroupType statusGroup;
    @XmlElement(name = "ReasonGroup")
    protected ReasonGroupType reasonGroup;
    @XmlElement(name = "QuickCodeGid")
    protected GLogXMLGidType quickCodeGid;
    @XmlElement(name = "ResponsiblePartyGid")
    protected GLogXMLGidType responsiblePartyGid;
    @XmlElement(name = "ReportingUser")
    protected String reportingUser;
    @XmlElement(name = "SLine")
    protected List<TransOrderStatusType.SLine> sLine;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the serviceProviderAlias property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public ServiceProviderAliasType getServiceProviderAlias() {
        return serviceProviderAlias;
    }

    /**
     * Sets the value of the serviceProviderAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceProviderAliasType }
     *     
     */
    public void setServiceProviderAlias(ServiceProviderAliasType value) {
        this.serviceProviderAlias = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the transOrderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Sets the value of the transOrderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Gets the value of the releaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Sets the value of the releaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the statusLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusLevel() {
        return statusLevel;
    }

    /**
     * Sets the value of the statusLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusLevel(String value) {
        this.statusLevel = value;
    }

    /**
     * Gets the value of the orderRefnum property.
     * 
     * @return
     *     possible object is
     *     {@link OrderRefnumType }
     *     
     */
    public OrderRefnumType getOrderRefnum() {
        return orderRefnum;
    }

    /**
     * Sets the value of the orderRefnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderRefnumType }
     *     
     */
    public void setOrderRefnum(OrderRefnumType value) {
        this.orderRefnum = value;
    }

    /**
     * Gets the value of the statusCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusCodeGid() {
        return statusCodeGid;
    }

    /**
     * Sets the value of the statusCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusCodeGid(GLogXMLGidType value) {
        this.statusCodeGid = value;
    }

    /**
     * Gets the value of the statusReasonCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStatusReasonCodeGid() {
        return statusReasonCodeGid;
    }

    /**
     * Sets the value of the statusReasonCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStatusReasonCodeGid(GLogXMLGidType value) {
        this.statusReasonCodeGid = value;
    }

    /**
     * Gets the value of the timeZoneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeZoneGid() {
        return timeZoneGid;
    }

    /**
     * Sets the value of the timeZoneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeZoneGid(GLogXMLGidType value) {
        this.timeZoneGid = value;
    }

    /**
     * Gets the value of the eventDt property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEventDt() {
        return eventDt;
    }

    /**
     * Sets the value of the eventDt property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEventDt(GLogDateTimeType value) {
        this.eventDt = value;
    }

    /**
     * Gets the value of the ssContact property.
     * 
     * @return
     *     possible object is
     *     {@link SSContactType }
     *     
     */
    public SSContactType getSSContact() {
        return ssContact;
    }

    /**
     * Sets the value of the ssContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link SSContactType }
     *     
     */
    public void setSSContact(SSContactType value) {
        this.ssContact = value;
    }

    /**
     * Gets the value of the ssRemarks property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the ssRemarks property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSSRemarks().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSSRemarks() {
        if (ssRemarks == null) {
            ssRemarks = new ArrayList<String>();
        }
        return this.ssRemarks;
    }

    /**
     * Gets the value of the eventGroup property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the eventGroup property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventGroup().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventGroupType }
     * 
     * 
     */
    public List<EventGroupType> getEventGroup() {
        if (eventGroup == null) {
            eventGroup = new ArrayList<EventGroupType>();
        }
        return this.eventGroup;
    }

    /**
     * Gets the value of the statusGroup property.
     * 
     * @return
     *     possible object is
     *     {@link StatusGroupType }
     *     
     */
    public StatusGroupType getStatusGroup() {
        return statusGroup;
    }

    /**
     * Sets the value of the statusGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusGroupType }
     *     
     */
    public void setStatusGroup(StatusGroupType value) {
        this.statusGroup = value;
    }

    /**
     * Gets the value of the reasonGroup property.
     * 
     * @return
     *     possible object is
     *     {@link ReasonGroupType }
     *     
     */
    public ReasonGroupType getReasonGroup() {
        return reasonGroup;
    }

    /**
     * Sets the value of the reasonGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReasonGroupType }
     *     
     */
    public void setReasonGroup(ReasonGroupType value) {
        this.reasonGroup = value;
    }

    /**
     * Gets the value of the quickCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuickCodeGid() {
        return quickCodeGid;
    }

    /**
     * Sets the value of the quickCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuickCodeGid(GLogXMLGidType value) {
        this.quickCodeGid = value;
    }

    /**
     * Gets the value of the responsiblePartyGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getResponsiblePartyGid() {
        return responsiblePartyGid;
    }

    /**
     * Sets the value of the responsiblePartyGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setResponsiblePartyGid(GLogXMLGidType value) {
        this.responsiblePartyGid = value;
    }

    /**
     * Gets the value of the reportingUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingUser() {
        return reportingUser;
    }

    /**
     * Sets the value of the reportingUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingUser(String value) {
        this.reportingUser = value;
    }

    /**
     * Gets the value of the sLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the sLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransOrderStatusType.SLine }
     * 
     * 
     */
    public List<TransOrderStatusType.SLine> getSLine() {
        if (sLine == null) {
            sLine = new ArrayList<TransOrderStatusType.SLine>();
        }
        return this.sLine;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "lineGid"
    })
    public static class SLine {

        @XmlElement(name = "LineGid", required = true)
        protected GLogXMLGidType lineGid;

        /**
         * Gets the value of the lineGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLineGid() {
            return lineGid;
        }

        /**
         * Sets the value of the lineGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLineGid(GLogXMLGidType value) {
            this.lineGid = value;
        }

    }

}

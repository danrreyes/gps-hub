
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RemoteQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RemoteQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="RIQQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RIQQueryType"/&gt;
 *         &lt;element name="ShipmentQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentQueryType"/&gt;
 *         &lt;element name="TransmissionReportQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionReportQueryType"/&gt;
 *         &lt;element name="GenericQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GenericQueryType"/&gt;
 *         &lt;element name="AppointmentQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentQueryType"/&gt;
 *         &lt;element name="OrderRoutingRuleQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OrderRoutingRuleQueryType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RemoteQueryType", propOrder = {
    "riqQuery",
    "shipmentQuery",
    "transmissionReportQuery",
    "genericQuery",
    "appointmentQuery",
    "orderRoutingRuleQuery"
})
public class RemoteQueryType
    extends OTMTransactionIn
{

    @XmlElement(name = "RIQQuery")
    protected RIQQueryType riqQuery;
    @XmlElement(name = "ShipmentQuery")
    protected ShipmentQueryType shipmentQuery;
    @XmlElement(name = "TransmissionReportQuery")
    protected TransmissionReportQueryType transmissionReportQuery;
    @XmlElement(name = "GenericQuery")
    protected GenericQueryType genericQuery;
    @XmlElement(name = "AppointmentQuery")
    protected AppointmentQueryType appointmentQuery;
    @XmlElement(name = "OrderRoutingRuleQuery")
    protected OrderRoutingRuleQueryType orderRoutingRuleQuery;

    /**
     * Gets the value of the riqQuery property.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryType }
     *     
     */
    public RIQQueryType getRIQQuery() {
        return riqQuery;
    }

    /**
     * Sets the value of the riqQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryType }
     *     
     */
    public void setRIQQuery(RIQQueryType value) {
        this.riqQuery = value;
    }

    /**
     * Gets the value of the shipmentQuery property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentQueryType }
     *     
     */
    public ShipmentQueryType getShipmentQuery() {
        return shipmentQuery;
    }

    /**
     * Sets the value of the shipmentQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentQueryType }
     *     
     */
    public void setShipmentQuery(ShipmentQueryType value) {
        this.shipmentQuery = value;
    }

    /**
     * Gets the value of the transmissionReportQuery property.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionReportQueryType }
     *     
     */
    public TransmissionReportQueryType getTransmissionReportQuery() {
        return transmissionReportQuery;
    }

    /**
     * Sets the value of the transmissionReportQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionReportQueryType }
     *     
     */
    public void setTransmissionReportQuery(TransmissionReportQueryType value) {
        this.transmissionReportQuery = value;
    }

    /**
     * Gets the value of the genericQuery property.
     * 
     * @return
     *     possible object is
     *     {@link GenericQueryType }
     *     
     */
    public GenericQueryType getGenericQuery() {
        return genericQuery;
    }

    /**
     * Sets the value of the genericQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenericQueryType }
     *     
     */
    public void setGenericQuery(GenericQueryType value) {
        this.genericQuery = value;
    }

    /**
     * Gets the value of the appointmentQuery property.
     * 
     * @return
     *     possible object is
     *     {@link AppointmentQueryType }
     *     
     */
    public AppointmentQueryType getAppointmentQuery() {
        return appointmentQuery;
    }

    /**
     * Sets the value of the appointmentQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link AppointmentQueryType }
     *     
     */
    public void setAppointmentQuery(AppointmentQueryType value) {
        this.appointmentQuery = value;
    }

    /**
     * Gets the value of the orderRoutingRuleQuery property.
     * 
     * @return
     *     possible object is
     *     {@link OrderRoutingRuleQueryType }
     *     
     */
    public OrderRoutingRuleQueryType getOrderRoutingRuleQuery() {
        return orderRoutingRuleQuery;
    }

    /**
     * Sets the value of the orderRoutingRuleQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrderRoutingRuleQueryType }
     *     
     */
    public void setOrderRoutingRuleQuery(OrderRoutingRuleQueryType value) {
        this.orderRoutingRuleQuery = value;
    }

}

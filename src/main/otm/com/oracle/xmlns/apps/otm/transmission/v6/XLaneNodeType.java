
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An XLaneNode is either a mileage address or region reference.
 * 
 * <p>Java class for XLaneNodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="XLaneNodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice&gt;
 *         &lt;element name="MileageAddress" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}MileageAddressType"/&gt;
 *         &lt;element name="RegionRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RegionRefType"/&gt;
 *       &lt;/choice&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "XLaneNodeType", propOrder = {
    "mileageAddress",
    "regionRef"
})
public class XLaneNodeType {

    @XmlElement(name = "MileageAddress")
    protected MileageAddressType mileageAddress;
    @XmlElement(name = "RegionRef")
    protected RegionRefType regionRef;

    /**
     * Gets the value of the mileageAddress property.
     * 
     * @return
     *     possible object is
     *     {@link MileageAddressType }
     *     
     */
    public MileageAddressType getMileageAddress() {
        return mileageAddress;
    }

    /**
     * Sets the value of the mileageAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link MileageAddressType }
     *     
     */
    public void setMileageAddress(MileageAddressType value) {
        this.mileageAddress = value;
    }

    /**
     * Gets the value of the regionRef property.
     * 
     * @return
     *     possible object is
     *     {@link RegionRefType }
     *     
     */
    public RegionRefType getRegionRef() {
        return regionRef;
    }

    /**
     * Sets the value of the regionRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegionRefType }
     *     
     */
    public void setRegionRef(RegionRefType value) {
        this.regionRef = value;
    }

}

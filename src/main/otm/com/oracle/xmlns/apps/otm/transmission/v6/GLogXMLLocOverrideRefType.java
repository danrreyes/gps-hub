
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GLogXMLLocOverrideRefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GLogXMLLocOverrideRefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LocationOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideRefType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GLogXMLLocOverrideRefType", propOrder = {
    "locationOverrideRef"
})
public class GLogXMLLocOverrideRefType {

    @XmlElement(name = "LocationOverrideRef", required = true)
    protected LocationOverrideRefType locationOverrideRef;

    /**
     * Gets the value of the locationOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public LocationOverrideRefType getLocationOverrideRef() {
        return locationOverrideRef;
    }

    /**
     * Sets the value of the locationOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public void setLocationOverrideRef(LocationOverrideRefType value) {
        this.locationOverrideRef = value;
    }

}

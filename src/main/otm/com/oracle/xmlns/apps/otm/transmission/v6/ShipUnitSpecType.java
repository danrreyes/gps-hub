
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ShipUnitSpec is an element containing information about ship unit.
 *          
 * 
 * <p>Java class for ShipUnitSpecType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitSpecType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipUnitSpecGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ShipUnitSpecName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UnitType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsInOnMax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TareWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="MaxWeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightType" minOccurs="0"/&gt;
 *         &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MaxVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="Length" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthType" minOccurs="0"/&gt;
 *         &lt;element name="Width" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WidthType" minOccurs="0"/&gt;
 *         &lt;element name="Height" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HeightType" minOccurs="0"/&gt;
 *         &lt;element name="Diameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DiameterType" minOccurs="0"/&gt;
 *         &lt;element name="CoreDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/&gt;
 *         &lt;element name="IsCylindrical" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PackagingFormCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsNestable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MaxNestingCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NestingVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="MaxOnLength" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLengthType" minOccurs="0"/&gt;
 *         &lt;element name="MaxOnWidth" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWidthType" minOccurs="0"/&gt;
 *         &lt;element name="MaxOnHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLHeightType" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="THUCapacityPackageRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}THUCapacityPackageRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitSpecType", propOrder = {
    "shipUnitSpecGid",
    "shipUnitSpecName",
    "unitType",
    "isInOnMax",
    "tareWeight",
    "maxWeight",
    "volume",
    "maxVolume",
    "length",
    "width",
    "height",
    "diameter",
    "coreDiameter",
    "isCylindrical",
    "packagingFormCodeGid",
    "isNestable",
    "maxNestingCount",
    "nestingVolume",
    "maxOnLength",
    "maxOnWidth",
    "maxOnHeight",
    "refnum",
    "thuCapacityPackageRefUnit",
    "priority"
})
public class ShipUnitSpecType {

    @XmlElement(name = "ShipUnitSpecGid", required = true)
    protected GLogXMLGidType shipUnitSpecGid;
    @XmlElement(name = "ShipUnitSpecName")
    protected String shipUnitSpecName;
    @XmlElement(name = "UnitType")
    protected String unitType;
    @XmlElement(name = "IsInOnMax")
    protected String isInOnMax;
    @XmlElement(name = "TareWeight")
    protected GLogXMLWeightType tareWeight;
    @XmlElement(name = "MaxWeight")
    protected GLogXMLWeightType maxWeight;
    @XmlElement(name = "Volume")
    protected VolumeType volume;
    @XmlElement(name = "MaxVolume")
    protected GLogXMLVolumeType maxVolume;
    @XmlElement(name = "Length")
    protected LengthType length;
    @XmlElement(name = "Width")
    protected WidthType width;
    @XmlElement(name = "Height")
    protected HeightType height;
    @XmlElement(name = "Diameter")
    protected DiameterType diameter;
    @XmlElement(name = "CoreDiameter")
    protected GLogXMLDiameterType coreDiameter;
    @XmlElement(name = "IsCylindrical")
    protected String isCylindrical;
    @XmlElement(name = "PackagingFormCodeGid")
    protected GLogXMLGidType packagingFormCodeGid;
    @XmlElement(name = "IsNestable")
    protected String isNestable;
    @XmlElement(name = "MaxNestingCount")
    protected String maxNestingCount;
    @XmlElement(name = "NestingVolume")
    protected GLogXMLVolumeType nestingVolume;
    @XmlElement(name = "MaxOnLength")
    protected GLogXMLLengthType maxOnLength;
    @XmlElement(name = "MaxOnWidth")
    protected GLogXMLWidthType maxOnWidth;
    @XmlElement(name = "MaxOnHeight")
    protected GLogXMLHeightType maxOnHeight;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "THUCapacityPackageRefUnit")
    protected List<THUCapacityPackageRefUnitType> thuCapacityPackageRefUnit;
    @XmlElement(name = "Priority")
    protected String priority;

    /**
     * Gets the value of the shipUnitSpecGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecGid() {
        return shipUnitSpecGid;
    }

    /**
     * Sets the value of the shipUnitSpecGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecGid(GLogXMLGidType value) {
        this.shipUnitSpecGid = value;
    }

    /**
     * Gets the value of the shipUnitSpecName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitSpecName() {
        return shipUnitSpecName;
    }

    /**
     * Sets the value of the shipUnitSpecName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitSpecName(String value) {
        this.shipUnitSpecName = value;
    }

    /**
     * Gets the value of the unitType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitType() {
        return unitType;
    }

    /**
     * Sets the value of the unitType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitType(String value) {
        this.unitType = value;
    }

    /**
     * Gets the value of the isInOnMax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInOnMax() {
        return isInOnMax;
    }

    /**
     * Sets the value of the isInOnMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInOnMax(String value) {
        this.isInOnMax = value;
    }

    /**
     * Gets the value of the tareWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getTareWeight() {
        return tareWeight;
    }

    /**
     * Sets the value of the tareWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setTareWeight(GLogXMLWeightType value) {
        this.tareWeight = value;
    }

    /**
     * Gets the value of the maxWeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public GLogXMLWeightType getMaxWeight() {
        return maxWeight;
    }

    /**
     * Sets the value of the maxWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightType }
     *     
     */
    public void setMaxWeight(GLogXMLWeightType value) {
        this.maxWeight = value;
    }

    /**
     * Gets the value of the volume property.
     * 
     * @return
     *     possible object is
     *     {@link VolumeType }
     *     
     */
    public VolumeType getVolume() {
        return volume;
    }

    /**
     * Sets the value of the volume property.
     * 
     * @param value
     *     allowed object is
     *     {@link VolumeType }
     *     
     */
    public void setVolume(VolumeType value) {
        this.volume = value;
    }

    /**
     * Gets the value of the maxVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getMaxVolume() {
        return maxVolume;
    }

    /**
     * Sets the value of the maxVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setMaxVolume(GLogXMLVolumeType value) {
        this.maxVolume = value;
    }

    /**
     * Gets the value of the length property.
     * 
     * @return
     *     possible object is
     *     {@link LengthType }
     *     
     */
    public LengthType getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthType }
     *     
     */
    public void setLength(LengthType value) {
        this.length = value;
    }

    /**
     * Gets the value of the width property.
     * 
     * @return
     *     possible object is
     *     {@link WidthType }
     *     
     */
    public WidthType getWidth() {
        return width;
    }

    /**
     * Sets the value of the width property.
     * 
     * @param value
     *     allowed object is
     *     {@link WidthType }
     *     
     */
    public void setWidth(WidthType value) {
        this.width = value;
    }

    /**
     * Gets the value of the height property.
     * 
     * @return
     *     possible object is
     *     {@link HeightType }
     *     
     */
    public HeightType getHeight() {
        return height;
    }

    /**
     * Sets the value of the height property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeightType }
     *     
     */
    public void setHeight(HeightType value) {
        this.height = value;
    }

    /**
     * Gets the value of the diameter property.
     * 
     * @return
     *     possible object is
     *     {@link DiameterType }
     *     
     */
    public DiameterType getDiameter() {
        return diameter;
    }

    /**
     * Sets the value of the diameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiameterType }
     *     
     */
    public void setDiameter(DiameterType value) {
        this.diameter = value;
    }

    /**
     * Gets the value of the coreDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getCoreDiameter() {
        return coreDiameter;
    }

    /**
     * Sets the value of the coreDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setCoreDiameter(GLogXMLDiameterType value) {
        this.coreDiameter = value;
    }

    /**
     * Gets the value of the isCylindrical property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCylindrical() {
        return isCylindrical;
    }

    /**
     * Sets the value of the isCylindrical property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCylindrical(String value) {
        this.isCylindrical = value;
    }

    /**
     * Gets the value of the packagingFormCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPackagingFormCodeGid() {
        return packagingFormCodeGid;
    }

    /**
     * Sets the value of the packagingFormCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPackagingFormCodeGid(GLogXMLGidType value) {
        this.packagingFormCodeGid = value;
    }

    /**
     * Gets the value of the isNestable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNestable() {
        return isNestable;
    }

    /**
     * Sets the value of the isNestable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNestable(String value) {
        this.isNestable = value;
    }

    /**
     * Gets the value of the maxNestingCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNestingCount() {
        return maxNestingCount;
    }

    /**
     * Sets the value of the maxNestingCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNestingCount(String value) {
        this.maxNestingCount = value;
    }

    /**
     * Gets the value of the nestingVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public GLogXMLVolumeType getNestingVolume() {
        return nestingVolume;
    }

    /**
     * Sets the value of the nestingVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLVolumeType }
     *     
     */
    public void setNestingVolume(GLogXMLVolumeType value) {
        this.nestingVolume = value;
    }

    /**
     * Gets the value of the maxOnLength property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public GLogXMLLengthType getMaxOnLength() {
        return maxOnLength;
    }

    /**
     * Sets the value of the maxOnLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLengthType }
     *     
     */
    public void setMaxOnLength(GLogXMLLengthType value) {
        this.maxOnLength = value;
    }

    /**
     * Gets the value of the maxOnWidth property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public GLogXMLWidthType getMaxOnWidth() {
        return maxOnWidth;
    }

    /**
     * Sets the value of the maxOnWidth property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWidthType }
     *     
     */
    public void setMaxOnWidth(GLogXMLWidthType value) {
        this.maxOnWidth = value;
    }

    /**
     * Gets the value of the maxOnHeight property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public GLogXMLHeightType getMaxOnHeight() {
        return maxOnHeight;
    }

    /**
     * Sets the value of the maxOnHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLHeightType }
     *     
     */
    public void setMaxOnHeight(GLogXMLHeightType value) {
        this.maxOnHeight = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the thuCapacityPackageRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the thuCapacityPackageRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTHUCapacityPackageRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link THUCapacityPackageRefUnitType }
     * 
     * 
     */
    public List<THUCapacityPackageRefUnitType> getTHUCapacityPackageRefUnit() {
        if (thuCapacityPackageRefUnit == null) {
            thuCapacityPackageRefUnit = new ArrayList<THUCapacityPackageRefUnitType>();
        }
        return this.thuCapacityPackageRefUnit;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }

}

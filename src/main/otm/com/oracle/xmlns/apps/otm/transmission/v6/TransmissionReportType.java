
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             TransmissionReport consists of a TransmissionNo followed by 0 or more IntegrationLogMessage elements,
 *             followed by a TransmissionSummary element.
 *          
 * 
 * <p>Java class for TransmissionReportType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransmissionReportType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Password" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PasswordType" minOccurs="0"/&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="TransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SenderTransmissionNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReportStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IntegrationLogMessage" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntegrationLogMessageType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransmissionSummary" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransmissionSummaryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransmissionReportType", propOrder = {
    "userName",
    "password",
    "sendReason",
    "transmissionNo",
    "senderTransmissionNo",
    "reportStatus",
    "refnum",
    "integrationLogMessage",
    "transmissionSummary"
})
public class TransmissionReportType
    extends OTMTransactionInOut
{

    @XmlElement(name = "UserName")
    protected String userName;
    @XmlElement(name = "Password")
    protected PasswordType password;
    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransmissionNo", required = true)
    protected String transmissionNo;
    @XmlElement(name = "SenderTransmissionNo")
    protected String senderTransmissionNo;
    @XmlElement(name = "ReportStatus")
    protected String reportStatus;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "IntegrationLogMessage")
    protected List<IntegrationLogMessageType> integrationLogMessage;
    @XmlElement(name = "TransmissionSummary")
    protected TransmissionSummaryType transmissionSummary;

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link PasswordType }
     *     
     */
    public PasswordType getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link PasswordType }
     *     
     */
    public void setPassword(PasswordType value) {
        this.password = value;
    }

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the transmissionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionNo() {
        return transmissionNo;
    }

    /**
     * Sets the value of the transmissionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionNo(String value) {
        this.transmissionNo = value;
    }

    /**
     * Gets the value of the senderTransmissionNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransmissionNo() {
        return senderTransmissionNo;
    }

    /**
     * Sets the value of the senderTransmissionNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransmissionNo(String value) {
        this.senderTransmissionNo = value;
    }

    /**
     * Gets the value of the reportStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportStatus() {
        return reportStatus;
    }

    /**
     * Sets the value of the reportStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportStatus(String value) {
        this.reportStatus = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the integrationLogMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the integrationLogMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIntegrationLogMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IntegrationLogMessageType }
     * 
     * 
     */
    public List<IntegrationLogMessageType> getIntegrationLogMessage() {
        if (integrationLogMessage == null) {
            integrationLogMessage = new ArrayList<IntegrationLogMessageType>();
        }
        return this.integrationLogMessage;
    }

    /**
     * Gets the value of the transmissionSummary property.
     * 
     * @return
     *     possible object is
     *     {@link TransmissionSummaryType }
     *     
     */
    public TransmissionSummaryType getTransmissionSummary() {
        return transmissionSummary;
    }

    /**
     * Sets the value of the transmissionSummary property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransmissionSummaryType }
     *     
     */
    public void setTransmissionSummary(TransmissionSummaryType value) {
        this.transmissionSummary = value;
    }

}

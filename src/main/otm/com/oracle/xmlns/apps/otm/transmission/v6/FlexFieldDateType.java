
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Common type for tables which contain Flex Field columns for DATE data type.
 *             Note: Some tables may not have all the available columns. These types just 
 *             define the maximum number of allowed elements.
 *          
 * 
 * <p>Java class for FlexFieldDateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FlexFieldDateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AttributeDate1" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate2" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate3" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate4" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate5" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate6" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate7" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate8" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate9" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate10" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate11" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate12" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate13" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate14" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate15" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate16" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate17" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate18" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate19" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AttributeDate20" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FlexFieldDateType", propOrder = {
    "attributeDate1",
    "attributeDate2",
    "attributeDate3",
    "attributeDate4",
    "attributeDate5",
    "attributeDate6",
    "attributeDate7",
    "attributeDate8",
    "attributeDate9",
    "attributeDate10",
    "attributeDate11",
    "attributeDate12",
    "attributeDate13",
    "attributeDate14",
    "attributeDate15",
    "attributeDate16",
    "attributeDate17",
    "attributeDate18",
    "attributeDate19",
    "attributeDate20"
})
public class FlexFieldDateType {

    @XmlElement(name = "AttributeDate1")
    protected GLogDateTimeType attributeDate1;
    @XmlElement(name = "AttributeDate2")
    protected GLogDateTimeType attributeDate2;
    @XmlElement(name = "AttributeDate3")
    protected GLogDateTimeType attributeDate3;
    @XmlElement(name = "AttributeDate4")
    protected GLogDateTimeType attributeDate4;
    @XmlElement(name = "AttributeDate5")
    protected GLogDateTimeType attributeDate5;
    @XmlElement(name = "AttributeDate6")
    protected GLogDateTimeType attributeDate6;
    @XmlElement(name = "AttributeDate7")
    protected GLogDateTimeType attributeDate7;
    @XmlElement(name = "AttributeDate8")
    protected GLogDateTimeType attributeDate8;
    @XmlElement(name = "AttributeDate9")
    protected GLogDateTimeType attributeDate9;
    @XmlElement(name = "AttributeDate10")
    protected GLogDateTimeType attributeDate10;
    @XmlElement(name = "AttributeDate11")
    protected GLogDateTimeType attributeDate11;
    @XmlElement(name = "AttributeDate12")
    protected GLogDateTimeType attributeDate12;
    @XmlElement(name = "AttributeDate13")
    protected GLogDateTimeType attributeDate13;
    @XmlElement(name = "AttributeDate14")
    protected GLogDateTimeType attributeDate14;
    @XmlElement(name = "AttributeDate15")
    protected GLogDateTimeType attributeDate15;
    @XmlElement(name = "AttributeDate16")
    protected GLogDateTimeType attributeDate16;
    @XmlElement(name = "AttributeDate17")
    protected GLogDateTimeType attributeDate17;
    @XmlElement(name = "AttributeDate18")
    protected GLogDateTimeType attributeDate18;
    @XmlElement(name = "AttributeDate19")
    protected GLogDateTimeType attributeDate19;
    @XmlElement(name = "AttributeDate20")
    protected GLogDateTimeType attributeDate20;

    /**
     * Gets the value of the attributeDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate1() {
        return attributeDate1;
    }

    /**
     * Sets the value of the attributeDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate1(GLogDateTimeType value) {
        this.attributeDate1 = value;
    }

    /**
     * Gets the value of the attributeDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate2() {
        return attributeDate2;
    }

    /**
     * Sets the value of the attributeDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate2(GLogDateTimeType value) {
        this.attributeDate2 = value;
    }

    /**
     * Gets the value of the attributeDate3 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate3() {
        return attributeDate3;
    }

    /**
     * Sets the value of the attributeDate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate3(GLogDateTimeType value) {
        this.attributeDate3 = value;
    }

    /**
     * Gets the value of the attributeDate4 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate4() {
        return attributeDate4;
    }

    /**
     * Sets the value of the attributeDate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate4(GLogDateTimeType value) {
        this.attributeDate4 = value;
    }

    /**
     * Gets the value of the attributeDate5 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate5() {
        return attributeDate5;
    }

    /**
     * Sets the value of the attributeDate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate5(GLogDateTimeType value) {
        this.attributeDate5 = value;
    }

    /**
     * Gets the value of the attributeDate6 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate6() {
        return attributeDate6;
    }

    /**
     * Sets the value of the attributeDate6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate6(GLogDateTimeType value) {
        this.attributeDate6 = value;
    }

    /**
     * Gets the value of the attributeDate7 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate7() {
        return attributeDate7;
    }

    /**
     * Sets the value of the attributeDate7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate7(GLogDateTimeType value) {
        this.attributeDate7 = value;
    }

    /**
     * Gets the value of the attributeDate8 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate8() {
        return attributeDate8;
    }

    /**
     * Sets the value of the attributeDate8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate8(GLogDateTimeType value) {
        this.attributeDate8 = value;
    }

    /**
     * Gets the value of the attributeDate9 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate9() {
        return attributeDate9;
    }

    /**
     * Sets the value of the attributeDate9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate9(GLogDateTimeType value) {
        this.attributeDate9 = value;
    }

    /**
     * Gets the value of the attributeDate10 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate10() {
        return attributeDate10;
    }

    /**
     * Sets the value of the attributeDate10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate10(GLogDateTimeType value) {
        this.attributeDate10 = value;
    }

    /**
     * Gets the value of the attributeDate11 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate11() {
        return attributeDate11;
    }

    /**
     * Sets the value of the attributeDate11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate11(GLogDateTimeType value) {
        this.attributeDate11 = value;
    }

    /**
     * Gets the value of the attributeDate12 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate12() {
        return attributeDate12;
    }

    /**
     * Sets the value of the attributeDate12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate12(GLogDateTimeType value) {
        this.attributeDate12 = value;
    }

    /**
     * Gets the value of the attributeDate13 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate13() {
        return attributeDate13;
    }

    /**
     * Sets the value of the attributeDate13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate13(GLogDateTimeType value) {
        this.attributeDate13 = value;
    }

    /**
     * Gets the value of the attributeDate14 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate14() {
        return attributeDate14;
    }

    /**
     * Sets the value of the attributeDate14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate14(GLogDateTimeType value) {
        this.attributeDate14 = value;
    }

    /**
     * Gets the value of the attributeDate15 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate15() {
        return attributeDate15;
    }

    /**
     * Sets the value of the attributeDate15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate15(GLogDateTimeType value) {
        this.attributeDate15 = value;
    }

    /**
     * Gets the value of the attributeDate16 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate16() {
        return attributeDate16;
    }

    /**
     * Sets the value of the attributeDate16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate16(GLogDateTimeType value) {
        this.attributeDate16 = value;
    }

    /**
     * Gets the value of the attributeDate17 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate17() {
        return attributeDate17;
    }

    /**
     * Sets the value of the attributeDate17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate17(GLogDateTimeType value) {
        this.attributeDate17 = value;
    }

    /**
     * Gets the value of the attributeDate18 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate18() {
        return attributeDate18;
    }

    /**
     * Sets the value of the attributeDate18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate18(GLogDateTimeType value) {
        this.attributeDate18 = value;
    }

    /**
     * Gets the value of the attributeDate19 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate19() {
        return attributeDate19;
    }

    /**
     * Sets the value of the attributeDate19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate19(GLogDateTimeType value) {
        this.attributeDate19 = value;
    }

    /**
     * Gets the value of the attributeDate20 property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAttributeDate20() {
        return attributeDate20;
    }

    /**
     * Sets the value of the attributeDate20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAttributeDate20(GLogDateTimeType value) {
        this.attributeDate20 = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinancialSystemFeedType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FinancialSystemFeedType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SellSideFinancials" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialsType" minOccurs="0"/&gt;
 *         &lt;element name="BuySideFinancials" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialsType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FinancialSystemFeedType", propOrder = {
    "sendReason",
    "transOrderGid",
    "sellSideFinancials",
    "buySideFinancials"
})
public class FinancialSystemFeedType
    extends OTMTransactionOut
{

    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "TransOrderGid")
    protected List<GLogXMLGidType> transOrderGid;
    @XmlElement(name = "SellSideFinancials")
    protected FinancialsType sellSideFinancials;
    @XmlElement(name = "BuySideFinancials")
    protected FinancialsType buySideFinancials;

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the transOrderGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transOrderGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransOrderGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getTransOrderGid() {
        if (transOrderGid == null) {
            transOrderGid = new ArrayList<GLogXMLGidType>();
        }
        return this.transOrderGid;
    }

    /**
     * Gets the value of the sellSideFinancials property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialsType }
     *     
     */
    public FinancialsType getSellSideFinancials() {
        return sellSideFinancials;
    }

    /**
     * Sets the value of the sellSideFinancials property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialsType }
     *     
     */
    public void setSellSideFinancials(FinancialsType value) {
        this.sellSideFinancials = value;
    }

    /**
     * Gets the value of the buySideFinancials property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialsType }
     *     
     */
    public FinancialsType getBuySideFinancials() {
        return buySideFinancials;
    }

    /**
     * Sets the value of the buySideFinancials property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialsType }
     *     
     */
    public void setBuySideFinancials(FinancialsType value) {
        this.buySideFinancials = value;
    }

}

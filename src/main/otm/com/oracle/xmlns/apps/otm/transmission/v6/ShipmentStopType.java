
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ShipmentStop is a structure representing a stop on a shipment.
 *             Note: The LocationRef element is required on insert of the ShipmentStop.
 *          
 * 
 * <p>Java class for ShipmentStopType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentStopType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QueryByExtStopSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StopSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExtStopSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="StopDuration" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="IsAppointment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationRefType" minOccurs="0"/&gt;
 *         &lt;element name="LocationOverrideRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideRefType" minOccurs="0"/&gt;
 *         &lt;element name="ParentLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ArbitraryType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DistFromPrevStop" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDistanceType" minOccurs="0"/&gt;
 *         &lt;element name="IsFixedDistance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="StopReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ArrivalTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLEventTimeType" minOccurs="0"/&gt;
 *         &lt;element name="DepartureTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLEventTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AppointmentPickup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AppointmentDelivery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="IsPermanent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsDepot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="StopRequirementType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="VoyageGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RepetitionScheduleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RepetitionSchedStopNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlightInstanceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsMotherVessel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RushHourTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentStopDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentStopDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipStopDebrief" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipStopDebriefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Appointment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}AppointmentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="StopType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="AppointmentWindowStart" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="AppointmentWindowEnd" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PredictedArrival" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PredictedArrivalLow" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="PredictedArrivalHigh" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentStopType", propOrder = {
    "queryByExtStopSeq",
    "stopSequence",
    "extStopSequence",
    "intSavedQuery",
    "transactionCode",
    "stopDuration",
    "isAppointment",
    "locationRef",
    "locationOverrideRef",
    "parentLocationRef",
    "arbitraryType",
    "locationRoleGid",
    "distFromPrevStop",
    "isFixedDistance",
    "stopReason",
    "arrivalTime",
    "departureTime",
    "appointmentPickup",
    "appointmentDelivery",
    "isPermanent",
    "isDepot",
    "accessorialTime",
    "stopRequirementType",
    "rateServiceGid",
    "voyageGid",
    "repetitionScheduleGid",
    "repetitionSchedStopNo",
    "flightInstanceId",
    "isMotherVessel",
    "rushHourTime",
    "shipmentStopDetail",
    "refnum",
    "remark",
    "involvedParty",
    "status",
    "shipStopDebrief",
    "appointment",
    "stopType",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "appointmentWindowStart",
    "appointmentWindowEnd",
    "predictedArrival",
    "predictedArrivalLow",
    "predictedArrivalHigh"
})
public class ShipmentStopType {

    @XmlElement(name = "QueryByExtStopSeq")
    protected String queryByExtStopSeq;
    @XmlElement(name = "StopSequence")
    protected String stopSequence;
    @XmlElement(name = "ExtStopSequence")
    protected String extStopSequence;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "StopDuration")
    protected GLogXMLDurationType stopDuration;
    @XmlElement(name = "IsAppointment")
    protected String isAppointment;
    @XmlElement(name = "LocationRef")
    protected LocationRefType locationRef;
    @XmlElement(name = "LocationOverrideRef")
    protected LocationOverrideRefType locationOverrideRef;
    @XmlElement(name = "ParentLocationRef")
    protected GLogXMLLocRefType parentLocationRef;
    @XmlElement(name = "ArbitraryType")
    protected String arbitraryType;
    @XmlElement(name = "LocationRoleGid")
    protected GLogXMLGidType locationRoleGid;
    @XmlElement(name = "DistFromPrevStop")
    protected GLogXMLDistanceType distFromPrevStop;
    @XmlElement(name = "IsFixedDistance")
    protected String isFixedDistance;
    @XmlElement(name = "StopReason")
    protected String stopReason;
    @XmlElement(name = "ArrivalTime")
    protected GLogXMLEventTimeType arrivalTime;
    @XmlElement(name = "DepartureTime")
    protected GLogXMLEventTimeType departureTime;
    @XmlElement(name = "AppointmentPickup")
    protected GLogDateTimeType appointmentPickup;
    @XmlElement(name = "AppointmentDelivery")
    protected GLogDateTimeType appointmentDelivery;
    @XmlElement(name = "IsPermanent")
    protected String isPermanent;
    @XmlElement(name = "IsDepot")
    protected String isDepot;
    @XmlElement(name = "AccessorialTime")
    protected GLogXMLDurationType accessorialTime;
    @XmlElement(name = "StopRequirementType")
    protected String stopRequirementType;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "VoyageGid")
    protected GLogXMLGidType voyageGid;
    @XmlElement(name = "RepetitionScheduleGid")
    protected GLogXMLGidType repetitionScheduleGid;
    @XmlElement(name = "RepetitionSchedStopNo")
    protected String repetitionSchedStopNo;
    @XmlElement(name = "FlightInstanceId")
    protected String flightInstanceId;
    @XmlElement(name = "IsMotherVessel")
    protected String isMotherVessel;
    @XmlElement(name = "RushHourTime")
    protected GLogXMLDurationType rushHourTime;
    @XmlElement(name = "ShipmentStopDetail")
    protected List<ShipmentStopDetailType> shipmentStopDetail;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "ShipStopDebrief")
    protected List<ShipStopDebriefType> shipStopDebrief;
    @XmlElement(name = "Appointment")
    protected List<AppointmentType> appointment;
    @XmlElement(name = "StopType")
    protected String stopType;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "AppointmentWindowStart")
    protected GLogDateTimeType appointmentWindowStart;
    @XmlElement(name = "AppointmentWindowEnd")
    protected GLogDateTimeType appointmentWindowEnd;
    @XmlElement(name = "PredictedArrival")
    protected GLogDateTimeType predictedArrival;
    @XmlElement(name = "PredictedArrivalLow")
    protected GLogDateTimeType predictedArrivalLow;
    @XmlElement(name = "PredictedArrivalHigh")
    protected GLogDateTimeType predictedArrivalHigh;

    /**
     * Gets the value of the queryByExtStopSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQueryByExtStopSeq() {
        return queryByExtStopSeq;
    }

    /**
     * Sets the value of the queryByExtStopSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQueryByExtStopSeq(String value) {
        this.queryByExtStopSeq = value;
    }

    /**
     * Gets the value of the stopSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopSequence() {
        return stopSequence;
    }

    /**
     * Sets the value of the stopSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopSequence(String value) {
        this.stopSequence = value;
    }

    /**
     * Gets the value of the extStopSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtStopSequence() {
        return extStopSequence;
    }

    /**
     * Sets the value of the extStopSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtStopSequence(String value) {
        this.extStopSequence = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the stopDuration property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getStopDuration() {
        return stopDuration;
    }

    /**
     * Sets the value of the stopDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setStopDuration(GLogXMLDurationType value) {
        this.stopDuration = value;
    }

    /**
     * Gets the value of the isAppointment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAppointment() {
        return isAppointment;
    }

    /**
     * Sets the value of the isAppointment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAppointment(String value) {
        this.isAppointment = value;
    }

    /**
     * Gets the value of the locationRef property.
     * 
     * @return
     *     possible object is
     *     {@link LocationRefType }
     *     
     */
    public LocationRefType getLocationRef() {
        return locationRef;
    }

    /**
     * Sets the value of the locationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationRefType }
     *     
     */
    public void setLocationRef(LocationRefType value) {
        this.locationRef = value;
    }

    /**
     * Gets the value of the locationOverrideRef property.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public LocationOverrideRefType getLocationOverrideRef() {
        return locationOverrideRef;
    }

    /**
     * Sets the value of the locationOverrideRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideRefType }
     *     
     */
    public void setLocationOverrideRef(LocationOverrideRefType value) {
        this.locationOverrideRef = value;
    }

    /**
     * Gets the value of the parentLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getParentLocationRef() {
        return parentLocationRef;
    }

    /**
     * Sets the value of the parentLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setParentLocationRef(GLogXMLLocRefType value) {
        this.parentLocationRef = value;
    }

    /**
     * Gets the value of the arbitraryType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArbitraryType() {
        return arbitraryType;
    }

    /**
     * Sets the value of the arbitraryType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArbitraryType(String value) {
        this.arbitraryType = value;
    }

    /**
     * Gets the value of the locationRoleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationRoleGid() {
        return locationRoleGid;
    }

    /**
     * Sets the value of the locationRoleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationRoleGid(GLogXMLGidType value) {
        this.locationRoleGid = value;
    }

    /**
     * Gets the value of the distFromPrevStop property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public GLogXMLDistanceType getDistFromPrevStop() {
        return distFromPrevStop;
    }

    /**
     * Sets the value of the distFromPrevStop property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDistanceType }
     *     
     */
    public void setDistFromPrevStop(GLogXMLDistanceType value) {
        this.distFromPrevStop = value;
    }

    /**
     * Gets the value of the isFixedDistance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixedDistance() {
        return isFixedDistance;
    }

    /**
     * Sets the value of the isFixedDistance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixedDistance(String value) {
        this.isFixedDistance = value;
    }

    /**
     * Gets the value of the stopReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopReason() {
        return stopReason;
    }

    /**
     * Sets the value of the stopReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopReason(String value) {
        this.stopReason = value;
    }

    /**
     * Gets the value of the arrivalTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public GLogXMLEventTimeType getArrivalTime() {
        return arrivalTime;
    }

    /**
     * Sets the value of the arrivalTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public void setArrivalTime(GLogXMLEventTimeType value) {
        this.arrivalTime = value;
    }

    /**
     * Gets the value of the departureTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public GLogXMLEventTimeType getDepartureTime() {
        return departureTime;
    }

    /**
     * Sets the value of the departureTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLEventTimeType }
     *     
     */
    public void setDepartureTime(GLogXMLEventTimeType value) {
        this.departureTime = value;
    }

    /**
     * Gets the value of the appointmentPickup property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentPickup() {
        return appointmentPickup;
    }

    /**
     * Sets the value of the appointmentPickup property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentPickup(GLogDateTimeType value) {
        this.appointmentPickup = value;
    }

    /**
     * Gets the value of the appointmentDelivery property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentDelivery() {
        return appointmentDelivery;
    }

    /**
     * Sets the value of the appointmentDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentDelivery(GLogDateTimeType value) {
        this.appointmentDelivery = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPermanent(String value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the isDepot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDepot() {
        return isDepot;
    }

    /**
     * Sets the value of the isDepot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDepot(String value) {
        this.isDepot = value;
    }

    /**
     * Gets the value of the accessorialTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getAccessorialTime() {
        return accessorialTime;
    }

    /**
     * Sets the value of the accessorialTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setAccessorialTime(GLogXMLDurationType value) {
        this.accessorialTime = value;
    }

    /**
     * Gets the value of the stopRequirementType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopRequirementType() {
        return stopRequirementType;
    }

    /**
     * Sets the value of the stopRequirementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopRequirementType(String value) {
        this.stopRequirementType = value;
    }

    /**
     * Gets the value of the rateServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Sets the value of the rateServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Gets the value of the voyageGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoyageGid() {
        return voyageGid;
    }

    /**
     * Sets the value of the voyageGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoyageGid(GLogXMLGidType value) {
        this.voyageGid = value;
    }

    /**
     * Gets the value of the repetitionScheduleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRepetitionScheduleGid() {
        return repetitionScheduleGid;
    }

    /**
     * Sets the value of the repetitionScheduleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRepetitionScheduleGid(GLogXMLGidType value) {
        this.repetitionScheduleGid = value;
    }

    /**
     * Gets the value of the repetitionSchedStopNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepetitionSchedStopNo() {
        return repetitionSchedStopNo;
    }

    /**
     * Sets the value of the repetitionSchedStopNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepetitionSchedStopNo(String value) {
        this.repetitionSchedStopNo = value;
    }

    /**
     * Gets the value of the flightInstanceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlightInstanceId() {
        return flightInstanceId;
    }

    /**
     * Sets the value of the flightInstanceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlightInstanceId(String value) {
        this.flightInstanceId = value;
    }

    /**
     * Gets the value of the isMotherVessel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMotherVessel() {
        return isMotherVessel;
    }

    /**
     * Sets the value of the isMotherVessel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMotherVessel(String value) {
        this.isMotherVessel = value;
    }

    /**
     * Gets the value of the rushHourTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getRushHourTime() {
        return rushHourTime;
    }

    /**
     * Sets the value of the rushHourTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setRushHourTime(GLogXMLDurationType value) {
        this.rushHourTime = value;
    }

    /**
     * Gets the value of the shipmentStopDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentStopDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentStopDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentStopDetailType }
     * 
     * 
     */
    public List<ShipmentStopDetailType> getShipmentStopDetail() {
        if (shipmentStopDetail == null) {
            shipmentStopDetail = new ArrayList<ShipmentStopDetailType>();
        }
        return this.shipmentStopDetail;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the shipStopDebrief property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipStopDebrief property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipStopDebrief().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipStopDebriefType }
     * 
     * 
     */
    public List<ShipStopDebriefType> getShipStopDebrief() {
        if (shipStopDebrief == null) {
            shipStopDebrief = new ArrayList<ShipStopDebriefType>();
        }
        return this.shipStopDebrief;
    }

    /**
     * Gets the value of the appointment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the appointment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAppointment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AppointmentType }
     * 
     * 
     */
    public List<AppointmentType> getAppointment() {
        if (appointment == null) {
            appointment = new ArrayList<AppointmentType>();
        }
        return this.appointment;
    }

    /**
     * Gets the value of the stopType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopType() {
        return stopType;
    }

    /**
     * Sets the value of the stopType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopType(String value) {
        this.stopType = value;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the appointmentWindowStart property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentWindowStart() {
        return appointmentWindowStart;
    }

    /**
     * Sets the value of the appointmentWindowStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentWindowStart(GLogDateTimeType value) {
        this.appointmentWindowStart = value;
    }

    /**
     * Gets the value of the appointmentWindowEnd property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAppointmentWindowEnd() {
        return appointmentWindowEnd;
    }

    /**
     * Sets the value of the appointmentWindowEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAppointmentWindowEnd(GLogDateTimeType value) {
        this.appointmentWindowEnd = value;
    }

    /**
     * Gets the value of the predictedArrival property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPredictedArrival() {
        return predictedArrival;
    }

    /**
     * Sets the value of the predictedArrival property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPredictedArrival(GLogDateTimeType value) {
        this.predictedArrival = value;
    }

    /**
     * Gets the value of the predictedArrivalLow property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPredictedArrivalLow() {
        return predictedArrivalLow;
    }

    /**
     * Sets the value of the predictedArrivalLow property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPredictedArrivalLow(GLogDateTimeType value) {
        this.predictedArrivalLow = value;
    }

    /**
     * Gets the value of the predictedArrivalHigh property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getPredictedArrivalHigh() {
        return predictedArrivalHigh;
    }

    /**
     * Sets the value of the predictedArrivalHigh property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setPredictedArrivalHigh(GLogDateTimeType value) {
        this.predictedArrivalHigh = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GlUserGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="Nickname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UserPassword" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}UserPasswordType" minOccurs="0"/&gt;
 *         &lt;element name="GlAccountPolicyGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserPreferenceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserMenuLayoutGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DocumentUseProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DefaultAAReport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsExternal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsObiee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="UserBIAppGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UserBIRoleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserType", propOrder = {
    "glUserGid",
    "transactionCode",
    "nickname",
    "userPassword",
    "glAccountPolicyGid",
    "userRoleGid",
    "userPreferenceGid",
    "userMenuLayoutGid",
    "documentUseProfileGid",
    "defaultAAReport",
    "isExternal",
    "isObiee",
    "effectiveDate",
    "expirationDate",
    "userBIAppGid",
    "userBIRoleGid",
    "firstName",
    "lastName",
    "emailAddress"
})
public class UserType
    extends OTMTransactionIn
{

    @XmlElement(name = "GlUserGid", required = true)
    protected GLogXMLGidType glUserGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "Nickname")
    protected String nickname;
    @XmlElement(name = "UserPassword")
    protected UserPasswordType userPassword;
    @XmlElement(name = "GlAccountPolicyGid")
    protected GLogXMLGidType glAccountPolicyGid;
    @XmlElement(name = "UserRoleGid")
    protected GLogXMLGidType userRoleGid;
    @XmlElement(name = "UserPreferenceGid")
    protected GLogXMLGidType userPreferenceGid;
    @XmlElement(name = "UserMenuLayoutGid")
    protected List<GLogXMLGidType> userMenuLayoutGid;
    @XmlElement(name = "DocumentUseProfileGid")
    protected GLogXMLGidType documentUseProfileGid;
    @XmlElement(name = "DefaultAAReport")
    protected String defaultAAReport;
    @XmlElement(name = "IsExternal")
    protected String isExternal;
    @XmlElement(name = "IsObiee")
    protected String isObiee;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "UserBIAppGid")
    protected List<GLogXMLGidType> userBIAppGid;
    @XmlElement(name = "UserBIRoleGid")
    protected List<GLogXMLGidType> userBIRoleGid;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "EmailAddress")
    protected String emailAddress;

    /**
     * Gets the value of the glUserGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGlUserGid() {
        return glUserGid;
    }

    /**
     * Sets the value of the glUserGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGlUserGid(GLogXMLGidType value) {
        this.glUserGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the nickname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets the value of the nickname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNickname(String value) {
        this.nickname = value;
    }

    /**
     * Gets the value of the userPassword property.
     * 
     * @return
     *     possible object is
     *     {@link UserPasswordType }
     *     
     */
    public UserPasswordType getUserPassword() {
        return userPassword;
    }

    /**
     * Sets the value of the userPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserPasswordType }
     *     
     */
    public void setUserPassword(UserPasswordType value) {
        this.userPassword = value;
    }

    /**
     * Gets the value of the glAccountPolicyGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGlAccountPolicyGid() {
        return glAccountPolicyGid;
    }

    /**
     * Sets the value of the glAccountPolicyGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGlAccountPolicyGid(GLogXMLGidType value) {
        this.glAccountPolicyGid = value;
    }

    /**
     * Gets the value of the userRoleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserRoleGid() {
        return userRoleGid;
    }

    /**
     * Sets the value of the userRoleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserRoleGid(GLogXMLGidType value) {
        this.userRoleGid = value;
    }

    /**
     * Gets the value of the userPreferenceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserPreferenceGid() {
        return userPreferenceGid;
    }

    /**
     * Sets the value of the userPreferenceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserPreferenceGid(GLogXMLGidType value) {
        this.userPreferenceGid = value;
    }

    /**
     * Gets the value of the userMenuLayoutGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the userMenuLayoutGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserMenuLayoutGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getUserMenuLayoutGid() {
        if (userMenuLayoutGid == null) {
            userMenuLayoutGid = new ArrayList<GLogXMLGidType>();
        }
        return this.userMenuLayoutGid;
    }

    /**
     * Gets the value of the documentUseProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentUseProfileGid() {
        return documentUseProfileGid;
    }

    /**
     * Sets the value of the documentUseProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentUseProfileGid(GLogXMLGidType value) {
        this.documentUseProfileGid = value;
    }

    /**
     * Gets the value of the defaultAAReport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultAAReport() {
        return defaultAAReport;
    }

    /**
     * Sets the value of the defaultAAReport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultAAReport(String value) {
        this.defaultAAReport = value;
    }

    /**
     * Gets the value of the isExternal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsExternal() {
        return isExternal;
    }

    /**
     * Sets the value of the isExternal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsExternal(String value) {
        this.isExternal = value;
    }

    /**
     * Gets the value of the isObiee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsObiee() {
        return isObiee;
    }

    /**
     * Sets the value of the isObiee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsObiee(String value) {
        this.isObiee = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the userBIAppGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the userBIAppGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserBIAppGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getUserBIAppGid() {
        if (userBIAppGid == null) {
            userBIAppGid = new ArrayList<GLogXMLGidType>();
        }
        return this.userBIAppGid;
    }

    /**
     * Gets the value of the userBIRoleGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the userBIRoleGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserBIRoleGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getUserBIRoleGid() {
        if (userBIRoleGid == null) {
            userBIRoleGid = new ArrayList<GLogXMLGidType>();
        }
        return this.userBIRoleGid;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

}

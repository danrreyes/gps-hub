
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * SendReason is used to indicate the reason the notification/data/information is being sent to the external system.
 * 
 * <p>Java class for SendReasonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SendReasonType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ProcessControlRequestID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SendReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ObjectType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EventReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EventReasonType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendReasonType", propOrder = {
    "remark",
    "processControlRequestID",
    "sendReasonGid",
    "objectType",
    "eventReason"
})
public class SendReasonType {

    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ProcessControlRequestID")
    protected String processControlRequestID;
    @XmlElement(name = "SendReasonGid", required = true)
    protected GLogXMLGidType sendReasonGid;
    @XmlElement(name = "ObjectType", required = true)
    protected String objectType;
    @XmlElement(name = "EventReason")
    protected EventReasonType eventReason;

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the processControlRequestID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessControlRequestID() {
        return processControlRequestID;
    }

    /**
     * Sets the value of the processControlRequestID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessControlRequestID(String value) {
        this.processControlRequestID = value;
    }

    /**
     * Gets the value of the sendReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSendReasonGid() {
        return sendReasonGid;
    }

    /**
     * Sets the value of the sendReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSendReasonGid(GLogXMLGidType value) {
        this.sendReasonGid = value;
    }

    /**
     * Gets the value of the objectType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObjectType() {
        return objectType;
    }

    /**
     * Sets the value of the objectType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObjectType(String value) {
        this.objectType = value;
    }

    /**
     * Gets the value of the eventReason property.
     * 
     * @return
     *     possible object is
     *     {@link EventReasonType }
     *     
     */
    public EventReasonType getEventReason() {
        return eventReason;
    }

    /**
     * Sets the value of the eventReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventReasonType }
     *     
     */
    public void setEventReason(EventReasonType value) {
        this.eventReason = value;
    }

}

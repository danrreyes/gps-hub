
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             TransactionHeader contains the header level information for the Transaction (GLogXMLElement).
 *             The TimeZoneGid is used as an overide for the times specified in that Transaction.
 *             Typically, the times are assumed to be in the time zone of the corresponding location.
 *          
 * 
 * <p>Java class for TransactionHeaderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SenderTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProcessInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ProcessInfoType" minOccurs="0"/&gt;
 *         &lt;element name="TimeZoneGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ObjectModInfo" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="InsertDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *                   &lt;element name="UpdateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="SendReason" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SendReasonType" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionHeaderType", propOrder = {
    "senderTransactionId",
    "processInfo",
    "timeZoneGid",
    "objectModInfo",
    "sendReason",
    "refnum"
})
public class TransactionHeaderType {

    @XmlElement(name = "SenderTransactionId")
    protected String senderTransactionId;
    @XmlElement(name = "ProcessInfo")
    protected ProcessInfoType processInfo;
    @XmlElement(name = "TimeZoneGid")
    protected GLogXMLGidType timeZoneGid;
    @XmlElement(name = "ObjectModInfo")
    protected TransactionHeaderType.ObjectModInfo objectModInfo;
    @XmlElement(name = "SendReason")
    protected SendReasonType sendReason;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;

    /**
     * Gets the value of the senderTransactionId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenderTransactionId() {
        return senderTransactionId;
    }

    /**
     * Sets the value of the senderTransactionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenderTransactionId(String value) {
        this.senderTransactionId = value;
    }

    /**
     * Gets the value of the processInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ProcessInfoType }
     *     
     */
    public ProcessInfoType getProcessInfo() {
        return processInfo;
    }

    /**
     * Sets the value of the processInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessInfoType }
     *     
     */
    public void setProcessInfo(ProcessInfoType value) {
        this.processInfo = value;
    }

    /**
     * Gets the value of the timeZoneGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTimeZoneGid() {
        return timeZoneGid;
    }

    /**
     * Sets the value of the timeZoneGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTimeZoneGid(GLogXMLGidType value) {
        this.timeZoneGid = value;
    }

    /**
     * Gets the value of the objectModInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionHeaderType.ObjectModInfo }
     *     
     */
    public TransactionHeaderType.ObjectModInfo getObjectModInfo() {
        return objectModInfo;
    }

    /**
     * Sets the value of the objectModInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionHeaderType.ObjectModInfo }
     *     
     */
    public void setObjectModInfo(TransactionHeaderType.ObjectModInfo value) {
        this.objectModInfo = value;
    }

    /**
     * Gets the value of the sendReason property.
     * 
     * @return
     *     possible object is
     *     {@link SendReasonType }
     *     
     */
    public SendReasonType getSendReason() {
        return sendReason;
    }

    /**
     * Sets the value of the sendReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link SendReasonType }
     *     
     */
    public void setSendReason(SendReasonType value) {
        this.sendReason = value;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="InsertDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
     *         &lt;element name="UpdateDt" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "insertDt",
        "updateDt"
    })
    public static class ObjectModInfo {

        @XmlElement(name = "InsertDt")
        protected GLogDateTimeType insertDt;
        @XmlElement(name = "UpdateDt")
        protected GLogDateTimeType updateDt;

        /**
         * Gets the value of the insertDt property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getInsertDt() {
            return insertDt;
        }

        /**
         * Sets the value of the insertDt property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setInsertDt(GLogDateTimeType value) {
            this.insertDt = value;
        }

        /**
         * Gets the value of the updateDt property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getUpdateDt() {
            return updateDt;
        }

        /**
         * Sets the value of the updateDt property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setUpdateDt(GLogDateTimeType value) {
            this.updateDt = value;
        }

    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies time window preferences for appointment on a location for a service provider.
 *             When adding a new LocationServProvPrefDetail, set the TransactionCode = I to auto generate a new sequence.
 *             When updating a LocationServProfPrefDetail, use the IntSavedQuery to search for the sequence to update.
 *          
 * 
 * <p>Java class for LocationServProvPrefDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LocationServProvPrefDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="DayofWeek" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BeginTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="MaxSlot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PreferenceLevel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DoorDuration" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IsStanding" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LocationResourceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LocationServProvPrefDetailType", propOrder = {
    "intSavedQuery",
    "sequenceNumber",
    "transactionCode",
    "dayofWeek",
    "beginTime",
    "maxSlot",
    "preferenceLevel",
    "doorDuration",
    "isStanding",
    "locationResourceGid"
})
public class LocationServProvPrefDetailType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "DayofWeek", required = true)
    protected String dayofWeek;
    @XmlElement(name = "BeginTime", required = true)
    protected String beginTime;
    @XmlElement(name = "MaxSlot")
    protected String maxSlot;
    @XmlElement(name = "PreferenceLevel", required = true)
    protected String preferenceLevel;
    @XmlElement(name = "DoorDuration", required = true)
    protected String doorDuration;
    @XmlElement(name = "IsStanding")
    protected String isStanding;
    @XmlElement(name = "LocationResourceGid")
    protected GLogXMLGidType locationResourceGid;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the dayofWeek property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDayofWeek() {
        return dayofWeek;
    }

    /**
     * Sets the value of the dayofWeek property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDayofWeek(String value) {
        this.dayofWeek = value;
    }

    /**
     * Gets the value of the beginTime property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeginTime() {
        return beginTime;
    }

    /**
     * Sets the value of the beginTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeginTime(String value) {
        this.beginTime = value;
    }

    /**
     * Gets the value of the maxSlot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxSlot() {
        return maxSlot;
    }

    /**
     * Sets the value of the maxSlot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxSlot(String value) {
        this.maxSlot = value;
    }

    /**
     * Gets the value of the preferenceLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferenceLevel() {
        return preferenceLevel;
    }

    /**
     * Sets the value of the preferenceLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferenceLevel(String value) {
        this.preferenceLevel = value;
    }

    /**
     * Gets the value of the doorDuration property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDoorDuration() {
        return doorDuration;
    }

    /**
     * Sets the value of the doorDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDoorDuration(String value) {
        this.doorDuration = value;
    }

    /**
     * Gets the value of the isStanding property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsStanding() {
        return isStanding;
    }

    /**
     * Sets the value of the isStanding property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsStanding(String value) {
        this.isStanding = value;
    }

    /**
     * Gets the value of the locationResourceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLocationResourceGid() {
        return locationResourceGid;
    }

    /**
     * Sets the value of the locationResourceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLocationResourceGid(GLogXMLGidType value) {
        this.locationResourceGid = value;
    }

}

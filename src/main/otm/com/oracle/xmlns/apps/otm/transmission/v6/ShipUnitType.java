
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             A ShipUnit represents a pallet, container, crate, box, etc. It has a defined source and
 *             destination, and delivery timing requirements. You can optionally specify the contents in terms of items.
 * 
 *             The ShipUnit element is a shared element within the TransOrder, Release, and Shipment.
 *             The multiple or'ed option is used to specify the fields that are relevant when within the appropriate
 *             parent element. The first branch specifies the elements relevant within the TransOrder.ShipUnitDetail
 *             element (e.g. IsShippable, ReleaseCount, etc.). The second branch specifies the elements relevant within
 *             the Release element (e.g. TransOrderShipUnitGid). The third branch specifies the elements relevant within
 *             the Shipment element (e.g. SEquipmentGid, ReleaseShipUnitGid, etc.).
 *          
 * 
 * <p>Java class for ShipUnitType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipToLocationRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocRefType" minOccurs="0"/&gt;
 *         &lt;element name="ShipToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TimeWindow" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowType" minOccurs="0"/&gt;
 *         &lt;element name="WeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="UnitNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="LengthWidthHeight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LengthWidthHeightType" minOccurs="0"/&gt;
 *         &lt;element name="TotalGrossWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="Diameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}DiameterType" minOccurs="0"/&gt;
 *         &lt;element name="CoreDiameter" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDiameterType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSeal" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="SealNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ShipUnitContent" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitContentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DeclaredValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsSplitAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCountSplittable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TagInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TagInfoType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;sequence minOccurs="0"&gt;
 *             &lt;element name="IsShippable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="PlanFromLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *             &lt;element name="PlanFromLoadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="PlanToLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *             &lt;element name="PlanToUnloadPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="ReleasedCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="BufferLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocGidType" minOccurs="0"/&gt;
 *             &lt;element name="SplittableBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *             &lt;element name="OBShipUnitSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence minOccurs="0"&gt;
 *             &lt;element name="TransOrderShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *             &lt;element name="ReleaseInstrSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="REquipmentSequence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="SecondaryUnitWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *             &lt;element name="SecondaryUnitNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *             &lt;element name="LoadConfigSetupRef" minOccurs="0"&gt;
 *               &lt;complexType&gt;
 *                 &lt;complexContent&gt;
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                     &lt;choice&gt;
 *                       &lt;element name="LoadConfigSetupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                       &lt;element name="LoadConfigSetup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LoadConfigSetupType"/&gt;
 *                     &lt;/choice&gt;
 *                   &lt;/restriction&gt;
 *                 &lt;/complexContent&gt;
 *               &lt;/complexType&gt;
 *             &lt;/element&gt;
 *             &lt;element name="ShipUnitSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *             &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence minOccurs="0"&gt;
 *             &lt;choice minOccurs="0"&gt;
 *               &lt;element name="SEquipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *               &lt;element name="SEquipmentGidQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentGidQueryType"/&gt;
 *               &lt;element name="SEquipmentSShipUnitInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SEquipmentSShipUnitInfoType"/&gt;
 *             &lt;/choice&gt;
 *             &lt;element name="ReleaseShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *             &lt;element name="ReceivedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *             &lt;element name="ReceivedNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *             &lt;element name="ReceivedShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="RangeStart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="RangeEnd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="ReleaseMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *             &lt;element name="SShipUnitSpecialService" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}SpecialServiceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *             &lt;element name="ShipUnitPiece" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitPieceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OBShipUnitEquipRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PickupOrStopGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DropoffOrStopGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SUEquipRefUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipmentRefUnitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsRepackAllowed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitLoadingSplit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitLoadingSplitType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="IsExclusiveUse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Priority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitType", propOrder = {
    "shipUnitGid",
    "transactionCode",
    "transportHandlingUnitRef",
    "shipFromLocationRef",
    "shipFromLoadPoint",
    "shipToLocationRef",
    "shipToUnloadPoint",
    "timeWindow",
    "weightVolume",
    "unitNetWeightVolume",
    "lengthWidthHeight",
    "totalGrossWeightVolume",
    "diameter",
    "coreDiameter",
    "shipUnitSeal",
    "shipUnitContent",
    "declaredValue",
    "flexCommodityQualifierGid",
    "flexCommodityValue",
    "isSplitAllowed",
    "isCountSplittable",
    "remark",
    "shipUnitCount",
    "tagInfo",
    "isShippable",
    "planFromLocationGid",
    "planFromLoadPoint",
    "planToLocationGid",
    "planToUnloadPoint",
    "releasedCount",
    "bufferLocationGid",
    "splittableBy",
    "accessorialCodeGid",
    "obShipUnitSpecialService",
    "transOrderShipUnitGid",
    "releaseInstrSeq",
    "rEquipmentSequence",
    "secondaryUnitWeightVolume",
    "secondaryUnitNetWeightVolume",
    "loadConfigSetupRef",
    "shipUnitSpecialService",
    "text",
    "sEquipmentGid",
    "sEquipmentGidQuery",
    "sEquipmentSShipUnitInfo",
    "releaseShipUnitGid",
    "receivedWeightVolume",
    "receivedNetWeightVolume",
    "receivedShipUnitCount",
    "rangeStart",
    "rangeEnd",
    "releaseMethodGid",
    "sShipUnitSpecialService",
    "shipUnitPiece",
    "status",
    "involvedParty",
    "obShipUnitEquipRefUnit",
    "shipUnitRefnum",
    "pickupOrStopGid",
    "dropoffOrStopGid",
    "suEquipRefUnit",
    "isRepackAllowed",
    "shipUnitLoadingSplit",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "isExclusiveUse",
    "priority"
})
public class ShipUnitType {

    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "TransportHandlingUnitRef")
    protected GLogXMLShipUnitSpecRefType transportHandlingUnitRef;
    @XmlElement(name = "ShipFromLocationRef")
    protected GLogXMLLocRefType shipFromLocationRef;
    @XmlElement(name = "ShipFromLoadPoint")
    protected String shipFromLoadPoint;
    @XmlElement(name = "ShipToLocationRef")
    protected GLogXMLLocRefType shipToLocationRef;
    @XmlElement(name = "ShipToUnloadPoint")
    protected String shipToUnloadPoint;
    @XmlElement(name = "TimeWindow")
    protected TimeWindowType timeWindow;
    @XmlElement(name = "WeightVolume")
    protected WeightVolumeType weightVolume;
    @XmlElement(name = "UnitNetWeightVolume")
    protected WeightVolumeType unitNetWeightVolume;
    @XmlElement(name = "LengthWidthHeight")
    protected LengthWidthHeightType lengthWidthHeight;
    @XmlElement(name = "TotalGrossWeightVolume")
    protected WeightVolumeType totalGrossWeightVolume;
    @XmlElement(name = "Diameter")
    protected DiameterType diameter;
    @XmlElement(name = "CoreDiameter")
    protected GLogXMLDiameterType coreDiameter;
    @XmlElement(name = "ShipUnitSeal")
    protected List<ShipUnitType.ShipUnitSeal> shipUnitSeal;
    @XmlElement(name = "ShipUnitContent")
    protected List<ShipUnitContentType> shipUnitContent;
    @XmlElement(name = "DeclaredValue")
    protected GLogXMLFinancialAmountType declaredValue;
    @XmlElement(name = "FlexCommodityQualifierGid")
    protected GLogXMLGidType flexCommodityQualifierGid;
    @XmlElement(name = "FlexCommodityValue")
    protected String flexCommodityValue;
    @XmlElement(name = "IsSplitAllowed")
    protected String isSplitAllowed;
    @XmlElement(name = "IsCountSplittable")
    protected String isCountSplittable;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ShipUnitCount")
    protected String shipUnitCount;
    @XmlElement(name = "TagInfo")
    protected TagInfoType tagInfo;
    @XmlElement(name = "IsShippable")
    protected String isShippable;
    @XmlElement(name = "PlanFromLocationGid")
    protected GLogXMLLocGidType planFromLocationGid;
    @XmlElement(name = "PlanFromLoadPoint")
    protected String planFromLoadPoint;
    @XmlElement(name = "PlanToLocationGid")
    protected GLogXMLLocGidType planToLocationGid;
    @XmlElement(name = "PlanToUnloadPoint")
    protected String planToUnloadPoint;
    @XmlElement(name = "ReleasedCount")
    protected String releasedCount;
    @XmlElement(name = "BufferLocationGid")
    protected GLogXMLLocGidType bufferLocationGid;
    @XmlElement(name = "SplittableBy")
    protected String splittableBy;
    @XmlElement(name = "AccessorialCodeGid")
    protected List<GLogXMLGidType> accessorialCodeGid;
    @XmlElement(name = "OBShipUnitSpecialService")
    protected List<SpecialServiceType> obShipUnitSpecialService;
    @XmlElement(name = "TransOrderShipUnitGid")
    protected GLogXMLGidType transOrderShipUnitGid;
    @XmlElement(name = "ReleaseInstrSeq")
    protected String releaseInstrSeq;
    @XmlElement(name = "REquipmentSequence")
    protected String rEquipmentSequence;
    @XmlElement(name = "SecondaryUnitWeightVolume")
    protected WeightVolumeType secondaryUnitWeightVolume;
    @XmlElement(name = "SecondaryUnitNetWeightVolume")
    protected WeightVolumeType secondaryUnitNetWeightVolume;
    @XmlElement(name = "LoadConfigSetupRef")
    protected ShipUnitType.LoadConfigSetupRef loadConfigSetupRef;
    @XmlElement(name = "ShipUnitSpecialService")
    protected List<SpecialServiceType> shipUnitSpecialService;
    @XmlElement(name = "Text")
    protected List<TextType> text;
    @XmlElement(name = "SEquipmentGid")
    protected GLogXMLGidType sEquipmentGid;
    @XmlElement(name = "SEquipmentGidQuery")
    protected SEquipmentGidQueryType sEquipmentGidQuery;
    @XmlElement(name = "SEquipmentSShipUnitInfo")
    protected SEquipmentSShipUnitInfoType sEquipmentSShipUnitInfo;
    @XmlElement(name = "ReleaseShipUnitGid")
    protected GLogXMLGidType releaseShipUnitGid;
    @XmlElement(name = "ReceivedWeightVolume")
    protected WeightVolumeType receivedWeightVolume;
    @XmlElement(name = "ReceivedNetWeightVolume")
    protected WeightVolumeType receivedNetWeightVolume;
    @XmlElement(name = "ReceivedShipUnitCount")
    protected String receivedShipUnitCount;
    @XmlElement(name = "RangeStart")
    protected String rangeStart;
    @XmlElement(name = "RangeEnd")
    protected String rangeEnd;
    @XmlElement(name = "ReleaseMethodGid")
    protected GLogXMLGidType releaseMethodGid;
    @XmlElement(name = "SShipUnitSpecialService")
    protected List<SpecialServiceType> sShipUnitSpecialService;
    @XmlElement(name = "ShipUnitPiece")
    protected List<ShipUnitPieceType> shipUnitPiece;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "OBShipUnitEquipRefUnit")
    protected List<EquipmentRefUnitType> obShipUnitEquipRefUnit;
    @XmlElement(name = "ShipUnitRefnum")
    protected List<ShipUnitRefnumType> shipUnitRefnum;
    @XmlElement(name = "PickupOrStopGid")
    protected GLogXMLGidType pickupOrStopGid;
    @XmlElement(name = "DropoffOrStopGid")
    protected GLogXMLGidType dropoffOrStopGid;
    @XmlElement(name = "SUEquipRefUnit")
    protected List<EquipmentRefUnitType> suEquipRefUnit;
    @XmlElement(name = "IsRepackAllowed")
    protected String isRepackAllowed;
    @XmlElement(name = "ShipUnitLoadingSplit")
    protected List<ShipUnitLoadingSplitType> shipUnitLoadingSplit;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "IsExclusiveUse")
    protected String isExclusiveUse;
    @XmlElement(name = "Priority")
    protected String priority;

    /**
     * Gets the value of the shipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Sets the value of the shipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transportHandlingUnitRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getTransportHandlingUnitRef() {
        return transportHandlingUnitRef;
    }

    /**
     * Sets the value of the transportHandlingUnitRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setTransportHandlingUnitRef(GLogXMLShipUnitSpecRefType value) {
        this.transportHandlingUnitRef = value;
    }

    /**
     * Gets the value of the shipFromLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipFromLocationRef() {
        return shipFromLocationRef;
    }

    /**
     * Sets the value of the shipFromLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipFromLocationRef(GLogXMLLocRefType value) {
        this.shipFromLocationRef = value;
    }

    /**
     * Gets the value of the shipFromLoadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipFromLoadPoint() {
        return shipFromLoadPoint;
    }

    /**
     * Sets the value of the shipFromLoadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipFromLoadPoint(String value) {
        this.shipFromLoadPoint = value;
    }

    /**
     * Gets the value of the shipToLocationRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public GLogXMLLocRefType getShipToLocationRef() {
        return shipToLocationRef;
    }

    /**
     * Sets the value of the shipToLocationRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocRefType }
     *     
     */
    public void setShipToLocationRef(GLogXMLLocRefType value) {
        this.shipToLocationRef = value;
    }

    /**
     * Gets the value of the shipToUnloadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToUnloadPoint() {
        return shipToUnloadPoint;
    }

    /**
     * Sets the value of the shipToUnloadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToUnloadPoint(String value) {
        this.shipToUnloadPoint = value;
    }

    /**
     * Gets the value of the timeWindow property.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowType }
     *     
     */
    public TimeWindowType getTimeWindow() {
        return timeWindow;
    }

    /**
     * Sets the value of the timeWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowType }
     *     
     */
    public void setTimeWindow(TimeWindowType value) {
        this.timeWindow = value;
    }

    /**
     * Gets the value of the weightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolume() {
        return weightVolume;
    }

    /**
     * Sets the value of the weightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolume(WeightVolumeType value) {
        this.weightVolume = value;
    }

    /**
     * Gets the value of the unitNetWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getUnitNetWeightVolume() {
        return unitNetWeightVolume;
    }

    /**
     * Sets the value of the unitNetWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setUnitNetWeightVolume(WeightVolumeType value) {
        this.unitNetWeightVolume = value;
    }

    /**
     * Gets the value of the lengthWidthHeight property.
     * 
     * @return
     *     possible object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public LengthWidthHeightType getLengthWidthHeight() {
        return lengthWidthHeight;
    }

    /**
     * Sets the value of the lengthWidthHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link LengthWidthHeightType }
     *     
     */
    public void setLengthWidthHeight(LengthWidthHeightType value) {
        this.lengthWidthHeight = value;
    }

    /**
     * Gets the value of the totalGrossWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getTotalGrossWeightVolume() {
        return totalGrossWeightVolume;
    }

    /**
     * Sets the value of the totalGrossWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setTotalGrossWeightVolume(WeightVolumeType value) {
        this.totalGrossWeightVolume = value;
    }

    /**
     * Gets the value of the diameter property.
     * 
     * @return
     *     possible object is
     *     {@link DiameterType }
     *     
     */
    public DiameterType getDiameter() {
        return diameter;
    }

    /**
     * Sets the value of the diameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link DiameterType }
     *     
     */
    public void setDiameter(DiameterType value) {
        this.diameter = value;
    }

    /**
     * Gets the value of the coreDiameter property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public GLogXMLDiameterType getCoreDiameter() {
        return coreDiameter;
    }

    /**
     * Sets the value of the coreDiameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDiameterType }
     *     
     */
    public void setCoreDiameter(GLogXMLDiameterType value) {
        this.coreDiameter = value;
    }

    /**
     * Gets the value of the shipUnitSeal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitSeal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitSeal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitType.ShipUnitSeal }
     * 
     * 
     */
    public List<ShipUnitType.ShipUnitSeal> getShipUnitSeal() {
        if (shipUnitSeal == null) {
            shipUnitSeal = new ArrayList<ShipUnitType.ShipUnitSeal>();
        }
        return this.shipUnitSeal;
    }

    /**
     * Gets the value of the shipUnitContent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitContent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitContentType }
     * 
     * 
     */
    public List<ShipUnitContentType> getShipUnitContent() {
        if (shipUnitContent == null) {
            shipUnitContent = new ArrayList<ShipUnitContentType>();
        }
        return this.shipUnitContent;
    }

    /**
     * Gets the value of the declaredValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getDeclaredValue() {
        return declaredValue;
    }

    /**
     * Sets the value of the declaredValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setDeclaredValue(GLogXMLFinancialAmountType value) {
        this.declaredValue = value;
    }

    /**
     * Gets the value of the flexCommodityQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityQualifierGid() {
        return flexCommodityQualifierGid;
    }

    /**
     * Sets the value of the flexCommodityQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityQualifierGid(GLogXMLGidType value) {
        this.flexCommodityQualifierGid = value;
    }

    /**
     * Gets the value of the flexCommodityValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlexCommodityValue() {
        return flexCommodityValue;
    }

    /**
     * Sets the value of the flexCommodityValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlexCommodityValue(String value) {
        this.flexCommodityValue = value;
    }

    /**
     * Gets the value of the isSplitAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSplitAllowed() {
        return isSplitAllowed;
    }

    /**
     * Sets the value of the isSplitAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSplitAllowed(String value) {
        this.isSplitAllowed = value;
    }

    /**
     * Gets the value of the isCountSplittable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCountSplittable() {
        return isCountSplittable;
    }

    /**
     * Sets the value of the isCountSplittable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCountSplittable(String value) {
        this.isCountSplittable = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the shipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitCount() {
        return shipUnitCount;
    }

    /**
     * Sets the value of the shipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitCount(String value) {
        this.shipUnitCount = value;
    }

    /**
     * Gets the value of the tagInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TagInfoType }
     *     
     */
    public TagInfoType getTagInfo() {
        return tagInfo;
    }

    /**
     * Sets the value of the tagInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TagInfoType }
     *     
     */
    public void setTagInfo(TagInfoType value) {
        this.tagInfo = value;
    }

    /**
     * Gets the value of the isShippable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShippable() {
        return isShippable;
    }

    /**
     * Sets the value of the isShippable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShippable(String value) {
        this.isShippable = value;
    }

    /**
     * Gets the value of the planFromLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanFromLocationGid() {
        return planFromLocationGid;
    }

    /**
     * Sets the value of the planFromLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanFromLocationGid(GLogXMLLocGidType value) {
        this.planFromLocationGid = value;
    }

    /**
     * Gets the value of the planFromLoadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFromLoadPoint() {
        return planFromLoadPoint;
    }

    /**
     * Sets the value of the planFromLoadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFromLoadPoint(String value) {
        this.planFromLoadPoint = value;
    }

    /**
     * Gets the value of the planToLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getPlanToLocationGid() {
        return planToLocationGid;
    }

    /**
     * Sets the value of the planToLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setPlanToLocationGid(GLogXMLLocGidType value) {
        this.planToLocationGid = value;
    }

    /**
     * Gets the value of the planToUnloadPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanToUnloadPoint() {
        return planToUnloadPoint;
    }

    /**
     * Sets the value of the planToUnloadPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanToUnloadPoint(String value) {
        this.planToUnloadPoint = value;
    }

    /**
     * Gets the value of the releasedCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleasedCount() {
        return releasedCount;
    }

    /**
     * Sets the value of the releasedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleasedCount(String value) {
        this.releasedCount = value;
    }

    /**
     * Gets the value of the bufferLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public GLogXMLLocGidType getBufferLocationGid() {
        return bufferLocationGid;
    }

    /**
     * Sets the value of the bufferLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocGidType }
     *     
     */
    public void setBufferLocationGid(GLogXMLLocGidType value) {
        this.bufferLocationGid = value;
    }

    /**
     * Gets the value of the splittableBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSplittableBy() {
        return splittableBy;
    }

    /**
     * Sets the value of the splittableBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSplittableBy(String value) {
        this.splittableBy = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the accessorialCodeGid property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccessorialCodeGid().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GLogXMLGidType }
     * 
     * 
     */
    public List<GLogXMLGidType> getAccessorialCodeGid() {
        if (accessorialCodeGid == null) {
            accessorialCodeGid = new ArrayList<GLogXMLGidType>();
        }
        return this.accessorialCodeGid;
    }

    /**
     * Gets the value of the obShipUnitSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the obShipUnitSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOBShipUnitSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getOBShipUnitSpecialService() {
        if (obShipUnitSpecialService == null) {
            obShipUnitSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.obShipUnitSpecialService;
    }

    /**
     * Gets the value of the transOrderShipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderShipUnitGid() {
        return transOrderShipUnitGid;
    }

    /**
     * Sets the value of the transOrderShipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderShipUnitGid(GLogXMLGidType value) {
        this.transOrderShipUnitGid = value;
    }

    /**
     * Gets the value of the releaseInstrSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseInstrSeq() {
        return releaseInstrSeq;
    }

    /**
     * Sets the value of the releaseInstrSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseInstrSeq(String value) {
        this.releaseInstrSeq = value;
    }

    /**
     * Gets the value of the rEquipmentSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREquipmentSequence() {
        return rEquipmentSequence;
    }

    /**
     * Sets the value of the rEquipmentSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREquipmentSequence(String value) {
        this.rEquipmentSequence = value;
    }

    /**
     * Gets the value of the secondaryUnitWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getSecondaryUnitWeightVolume() {
        return secondaryUnitWeightVolume;
    }

    /**
     * Sets the value of the secondaryUnitWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setSecondaryUnitWeightVolume(WeightVolumeType value) {
        this.secondaryUnitWeightVolume = value;
    }

    /**
     * Gets the value of the secondaryUnitNetWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getSecondaryUnitNetWeightVolume() {
        return secondaryUnitNetWeightVolume;
    }

    /**
     * Sets the value of the secondaryUnitNetWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setSecondaryUnitNetWeightVolume(WeightVolumeType value) {
        this.secondaryUnitNetWeightVolume = value;
    }

    /**
     * Gets the value of the loadConfigSetupRef property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitType.LoadConfigSetupRef }
     *     
     */
    public ShipUnitType.LoadConfigSetupRef getLoadConfigSetupRef() {
        return loadConfigSetupRef;
    }

    /**
     * Sets the value of the loadConfigSetupRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitType.LoadConfigSetupRef }
     *     
     */
    public void setLoadConfigSetupRef(ShipUnitType.LoadConfigSetupRef value) {
        this.loadConfigSetupRef = value;
    }

    /**
     * Gets the value of the shipUnitSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getShipUnitSpecialService() {
        if (shipUnitSpecialService == null) {
            shipUnitSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.shipUnitSpecialService;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

    /**
     * Gets the value of the sEquipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSEquipmentGid() {
        return sEquipmentGid;
    }

    /**
     * Sets the value of the sEquipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSEquipmentGid(GLogXMLGidType value) {
        this.sEquipmentGid = value;
    }

    /**
     * Gets the value of the sEquipmentGidQuery property.
     * 
     * @return
     *     possible object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public SEquipmentGidQueryType getSEquipmentGidQuery() {
        return sEquipmentGidQuery;
    }

    /**
     * Sets the value of the sEquipmentGidQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link SEquipmentGidQueryType }
     *     
     */
    public void setSEquipmentGidQuery(SEquipmentGidQueryType value) {
        this.sEquipmentGidQuery = value;
    }

    /**
     * Gets the value of the sEquipmentSShipUnitInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SEquipmentSShipUnitInfoType }
     *     
     */
    public SEquipmentSShipUnitInfoType getSEquipmentSShipUnitInfo() {
        return sEquipmentSShipUnitInfo;
    }

    /**
     * Sets the value of the sEquipmentSShipUnitInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SEquipmentSShipUnitInfoType }
     *     
     */
    public void setSEquipmentSShipUnitInfo(SEquipmentSShipUnitInfoType value) {
        this.sEquipmentSShipUnitInfo = value;
    }

    /**
     * Gets the value of the releaseShipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseShipUnitGid() {
        return releaseShipUnitGid;
    }

    /**
     * Sets the value of the releaseShipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseShipUnitGid(GLogXMLGidType value) {
        this.releaseShipUnitGid = value;
    }

    /**
     * Gets the value of the receivedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getReceivedWeightVolume() {
        return receivedWeightVolume;
    }

    /**
     * Sets the value of the receivedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setReceivedWeightVolume(WeightVolumeType value) {
        this.receivedWeightVolume = value;
    }

    /**
     * Gets the value of the receivedNetWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getReceivedNetWeightVolume() {
        return receivedNetWeightVolume;
    }

    /**
     * Sets the value of the receivedNetWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setReceivedNetWeightVolume(WeightVolumeType value) {
        this.receivedNetWeightVolume = value;
    }

    /**
     * Gets the value of the receivedShipUnitCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedShipUnitCount() {
        return receivedShipUnitCount;
    }

    /**
     * Sets the value of the receivedShipUnitCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedShipUnitCount(String value) {
        this.receivedShipUnitCount = value;
    }

    /**
     * Gets the value of the rangeStart property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRangeStart() {
        return rangeStart;
    }

    /**
     * Sets the value of the rangeStart property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRangeStart(String value) {
        this.rangeStart = value;
    }

    /**
     * Gets the value of the rangeEnd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRangeEnd() {
        return rangeEnd;
    }

    /**
     * Sets the value of the rangeEnd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRangeEnd(String value) {
        this.rangeEnd = value;
    }

    /**
     * Gets the value of the releaseMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseMethodGid() {
        return releaseMethodGid;
    }

    /**
     * Sets the value of the releaseMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseMethodGid(GLogXMLGidType value) {
        this.releaseMethodGid = value;
    }

    /**
     * Gets the value of the sShipUnitSpecialService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the sShipUnitSpecialService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSShipUnitSpecialService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpecialServiceType }
     * 
     * 
     */
    public List<SpecialServiceType> getSShipUnitSpecialService() {
        if (sShipUnitSpecialService == null) {
            sShipUnitSpecialService = new ArrayList<SpecialServiceType>();
        }
        return this.sShipUnitSpecialService;
    }

    /**
     * Gets the value of the shipUnitPiece property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitPiece property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitPiece().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitPieceType }
     * 
     * 
     */
    public List<ShipUnitPieceType> getShipUnitPiece() {
        if (shipUnitPiece == null) {
            shipUnitPiece = new ArrayList<ShipUnitPieceType>();
        }
        return this.shipUnitPiece;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the obShipUnitEquipRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the obShipUnitEquipRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOBShipUnitEquipRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentRefUnitType }
     * 
     * 
     */
    public List<EquipmentRefUnitType> getOBShipUnitEquipRefUnit() {
        if (obShipUnitEquipRefUnit == null) {
            obShipUnitEquipRefUnit = new ArrayList<EquipmentRefUnitType>();
        }
        return this.obShipUnitEquipRefUnit;
    }

    /**
     * Gets the value of the shipUnitRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitRefnumType }
     * 
     * 
     */
    public List<ShipUnitRefnumType> getShipUnitRefnum() {
        if (shipUnitRefnum == null) {
            shipUnitRefnum = new ArrayList<ShipUnitRefnumType>();
        }
        return this.shipUnitRefnum;
    }

    /**
     * Gets the value of the pickupOrStopGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPickupOrStopGid() {
        return pickupOrStopGid;
    }

    /**
     * Sets the value of the pickupOrStopGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPickupOrStopGid(GLogXMLGidType value) {
        this.pickupOrStopGid = value;
    }

    /**
     * Gets the value of the dropoffOrStopGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDropoffOrStopGid() {
        return dropoffOrStopGid;
    }

    /**
     * Sets the value of the dropoffOrStopGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDropoffOrStopGid(GLogXMLGidType value) {
        this.dropoffOrStopGid = value;
    }

    /**
     * Gets the value of the suEquipRefUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the suEquipRefUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSUEquipRefUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EquipmentRefUnitType }
     * 
     * 
     */
    public List<EquipmentRefUnitType> getSUEquipRefUnit() {
        if (suEquipRefUnit == null) {
            suEquipRefUnit = new ArrayList<EquipmentRefUnitType>();
        }
        return this.suEquipRefUnit;
    }

    /**
     * Gets the value of the isRepackAllowed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsRepackAllowed() {
        return isRepackAllowed;
    }

    /**
     * Sets the value of the isRepackAllowed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsRepackAllowed(String value) {
        this.isRepackAllowed = value;
    }

    /**
     * Gets the value of the shipUnitLoadingSplit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitLoadingSplit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitLoadingSplit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitLoadingSplitType }
     * 
     * 
     */
    public List<ShipUnitLoadingSplitType> getShipUnitLoadingSplit() {
        if (shipUnitLoadingSplit == null) {
            shipUnitLoadingSplit = new ArrayList<ShipUnitLoadingSplitType>();
        }
        return this.shipUnitLoadingSplit;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the isExclusiveUse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsExclusiveUse() {
        return isExclusiveUse;
    }

    /**
     * Sets the value of the isExclusiveUse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsExclusiveUse(String value) {
        this.isExclusiveUse = value;
    }

    /**
     * Gets the value of the priority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriority() {
        return priority;
    }

    /**
     * Sets the value of the priority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriority(String value) {
        this.priority = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="LoadConfigSetupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="LoadConfigSetup" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LoadConfigSetupType"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "loadConfigSetupGid",
        "loadConfigSetup"
    })
    public static class LoadConfigSetupRef {

        @XmlElement(name = "LoadConfigSetupGid")
        protected GLogXMLGidType loadConfigSetupGid;
        @XmlElement(name = "LoadConfigSetup")
        protected LoadConfigSetupType loadConfigSetup;

        /**
         * Gets the value of the loadConfigSetupGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLoadConfigSetupGid() {
            return loadConfigSetupGid;
        }

        /**
         * Sets the value of the loadConfigSetupGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLoadConfigSetupGid(GLogXMLGidType value) {
            this.loadConfigSetupGid = value;
        }

        /**
         * Gets the value of the loadConfigSetup property.
         * 
         * @return
         *     possible object is
         *     {@link LoadConfigSetupType }
         *     
         */
        public LoadConfigSetupType getLoadConfigSetup() {
            return loadConfigSetup;
        }

        /**
         * Sets the value of the loadConfigSetup property.
         * 
         * @param value
         *     allowed object is
         *     {@link LoadConfigSetupType }
         *     
         */
        public void setLoadConfigSetup(LoadConfigSetupType value) {
            this.loadConfigSetup = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="SealNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sealNumber"
    })
    public static class ShipUnitSeal {

        @XmlElement(name = "SealNumber", required = true)
        protected String sealNumber;

        /**
         * Gets the value of the sealNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSealNumber() {
            return sealNumber;
        }

        /**
         * Sets the value of the sealNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSealNumber(String value) {
            this.sealNumber = value;
        }

    }

}

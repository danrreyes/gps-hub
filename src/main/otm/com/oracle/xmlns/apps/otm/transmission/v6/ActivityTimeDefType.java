
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies fields for allowing multiple fixed and variable stop times for each location role based on transport handling unit and commodity.
 *          
 * 
 * <p>Java class for ActivityTimeDefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivityTimeDefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="ActivityTimeDefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitSpecProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FlexCommodityProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServprovProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="EquipmentGroupProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="FixedStopTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="VariableStopTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="VarStopQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VarStopWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivityTimeDefType", propOrder = {
    "intSavedQuery",
    "activityTimeDefGid",
    "transactionCode",
    "specialServiceGid",
    "shipUnitSpecProfileGid",
    "flexCommodityProfileGid",
    "servprovProfileGid",
    "equipmentGroupProfileGid",
    "fixedStopTime",
    "variableStopTime",
    "varStopQuantity",
    "varStopWeightVolume"
})
public class ActivityTimeDefType
    extends OTMTransactionInOut
{

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ActivityTimeDefGid")
    protected GLogXMLGidType activityTimeDefGid;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "ShipUnitSpecProfileGid")
    protected GLogXMLGidType shipUnitSpecProfileGid;
    @XmlElement(name = "FlexCommodityProfileGid")
    protected GLogXMLGidType flexCommodityProfileGid;
    @XmlElement(name = "ServprovProfileGid")
    protected GLogXMLGidType servprovProfileGid;
    @XmlElement(name = "EquipmentGroupProfileGid")
    protected GLogXMLGidType equipmentGroupProfileGid;
    @XmlElement(name = "FixedStopTime")
    protected GLogXMLDurationType fixedStopTime;
    @XmlElement(name = "VariableStopTime")
    protected GLogXMLDurationType variableStopTime;
    @XmlElement(name = "VarStopQuantity")
    protected String varStopQuantity;
    @XmlElement(name = "VarStopWeightVolume")
    protected WeightVolumeType varStopWeightVolume;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the activityTimeDefGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getActivityTimeDefGid() {
        return activityTimeDefGid;
    }

    /**
     * Sets the value of the activityTimeDefGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setActivityTimeDefGid(GLogXMLGidType value) {
        this.activityTimeDefGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Sets the value of the specialServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Gets the value of the shipUnitSpecProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitSpecProfileGid() {
        return shipUnitSpecProfileGid;
    }

    /**
     * Sets the value of the shipUnitSpecProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitSpecProfileGid(GLogXMLGidType value) {
        this.shipUnitSpecProfileGid = value;
    }

    /**
     * Gets the value of the flexCommodityProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getFlexCommodityProfileGid() {
        return flexCommodityProfileGid;
    }

    /**
     * Sets the value of the flexCommodityProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setFlexCommodityProfileGid(GLogXMLGidType value) {
        this.flexCommodityProfileGid = value;
    }

    /**
     * Gets the value of the servprovProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getServprovProfileGid() {
        return servprovProfileGid;
    }

    /**
     * Sets the value of the servprovProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setServprovProfileGid(GLogXMLGidType value) {
        this.servprovProfileGid = value;
    }

    /**
     * Gets the value of the equipmentGroupProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getEquipmentGroupProfileGid() {
        return equipmentGroupProfileGid;
    }

    /**
     * Sets the value of the equipmentGroupProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setEquipmentGroupProfileGid(GLogXMLGidType value) {
        this.equipmentGroupProfileGid = value;
    }

    /**
     * Gets the value of the fixedStopTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getFixedStopTime() {
        return fixedStopTime;
    }

    /**
     * Sets the value of the fixedStopTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setFixedStopTime(GLogXMLDurationType value) {
        this.fixedStopTime = value;
    }

    /**
     * Gets the value of the variableStopTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getVariableStopTime() {
        return variableStopTime;
    }

    /**
     * Sets the value of the variableStopTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setVariableStopTime(GLogXMLDurationType value) {
        this.variableStopTime = value;
    }

    /**
     * Gets the value of the varStopQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVarStopQuantity() {
        return varStopQuantity;
    }

    /**
     * Sets the value of the varStopQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVarStopQuantity(String value) {
        this.varStopQuantity = value;
    }

    /**
     * Gets the value of the varStopWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getVarStopWeightVolume() {
        return varStopWeightVolume;
    }

    /**
     * Sets the value of the varStopWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setVarStopWeightVolume(WeightVolumeType value) {
        this.varStopWeightVolume = value;
    }

}

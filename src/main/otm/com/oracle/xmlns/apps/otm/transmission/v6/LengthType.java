
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Length contains sub-elements for length value and length unit of measure.
 * 
 * <p>Java class for LengthType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LengthType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LengthValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LengthUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LengthType", propOrder = {
    "lengthValue",
    "lengthUOMGid"
})
public class LengthType {

    @XmlElement(name = "LengthValue", required = true)
    protected String lengthValue;
    @XmlElement(name = "LengthUOMGid", required = true)
    protected GLogXMLGidType lengthUOMGid;

    /**
     * Gets the value of the lengthValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLengthValue() {
        return lengthValue;
    }

    /**
     * Sets the value of the lengthValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLengthValue(String value) {
        this.lengthValue = value;
    }

    /**
     * Gets the value of the lengthUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLengthUOMGid() {
        return lengthUOMGid;
    }

    /**
     * Sets the value of the lengthUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLengthUOMGid(GLogXMLGidType value) {
        this.lengthUOMGid = value;
    }

}

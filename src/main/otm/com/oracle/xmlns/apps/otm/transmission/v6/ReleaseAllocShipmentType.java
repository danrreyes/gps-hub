
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the total allocation for an order release on a particular shipment.
 *          
 * 
 * <p>Java class for ReleaseAllocShipmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseAllocShipmentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShipmentGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TotalAllocCost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="ReleaseAllocShipmentDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseAllocShipmentDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseAllocShipmentType", propOrder = {
    "shipmentGid",
    "totalAllocCost",
    "releaseAllocShipmentDetail"
})
public class ReleaseAllocShipmentType {

    @XmlElement(name = "ShipmentGid", required = true)
    protected GLogXMLGidType shipmentGid;
    @XmlElement(name = "TotalAllocCost", required = true)
    protected GLogXMLFinancialAmountType totalAllocCost;
    @XmlElement(name = "ReleaseAllocShipmentDetail")
    protected List<ReleaseAllocShipmentDetailType> releaseAllocShipmentDetail;

    /**
     * Gets the value of the shipmentGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipmentGid() {
        return shipmentGid;
    }

    /**
     * Sets the value of the shipmentGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipmentGid(GLogXMLGidType value) {
        this.shipmentGid = value;
    }

    /**
     * Gets the value of the totalAllocCost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getTotalAllocCost() {
        return totalAllocCost;
    }

    /**
     * Sets the value of the totalAllocCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setTotalAllocCost(GLogXMLFinancialAmountType value) {
        this.totalAllocCost = value;
    }

    /**
     * Gets the value of the releaseAllocShipmentDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the releaseAllocShipmentDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReleaseAllocShipmentDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReleaseAllocShipmentDetailType }
     * 
     * 
     */
    public List<ReleaseAllocShipmentDetailType> getReleaseAllocShipmentDetail() {
        if (releaseAllocShipmentDetail == null) {
            releaseAllocShipmentDetail = new ArrayList<ReleaseAllocShipmentDetailType>();
        }
        return this.releaseAllocShipmentDetail;
    }

}

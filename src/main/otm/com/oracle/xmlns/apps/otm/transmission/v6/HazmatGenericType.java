
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to identify hazardous materials information that is proper shipping name centric and
 *             thus more generic in nature.
 *             Note: Within the HazmatItem, this element is outbound only.
 *          
 * 
 * <p>Java class for HazmatGenericType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HazmatGenericType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HazmatGenericGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType"/&gt;
 *         &lt;element name="ProperShippingName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IdentificationNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="HazardousClass" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazardousClassType" minOccurs="0"/&gt;
 *         &lt;element name="PackagingGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SubsidiaryHazard" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegulatoryAgencyDataSource" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SpecialProvisions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PSASingaporeGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SymbolExtraMeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ImdgEmsNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsNos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsMarinePollutant" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsToxicInhalation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InhalationHazardZone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsPassengerAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCommercialAircraftForbid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ERG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ERGAir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EMS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="HazCompatGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HazmatGenericType", propOrder = {
    "hazmatGenericGid",
    "transactionCode",
    "properShippingName",
    "identificationNumber",
    "hazardousClass",
    "packagingGroup",
    "subsidiaryHazard",
    "regulatoryAgencyDataSource",
    "specialProvisions",
    "psaSingaporeGroup",
    "symbolExtraMeaning",
    "imdgEmsNumber",
    "isNos",
    "isMarinePollutant",
    "isToxicInhalation",
    "inhalationHazardZone",
    "isPassengerAircraftForbid",
    "isCommercialAircraftForbid",
    "erg",
    "ergAir",
    "ems",
    "hazCompatGroup"
})
public class HazmatGenericType
    extends OTMTransactionInOut
{

    @XmlElement(name = "HazmatGenericGid", required = true)
    protected GLogXMLGidType hazmatGenericGid;
    @XmlElement(name = "TransactionCode", required = true)
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ProperShippingName", required = true)
    protected String properShippingName;
    @XmlElement(name = "IdentificationNumber", required = true)
    protected String identificationNumber;
    @XmlElement(name = "HazardousClass")
    protected HazardousClassType hazardousClass;
    @XmlElement(name = "PackagingGroup")
    protected String packagingGroup;
    @XmlElement(name = "SubsidiaryHazard")
    protected String subsidiaryHazard;
    @XmlElement(name = "RegulatoryAgencyDataSource")
    protected String regulatoryAgencyDataSource;
    @XmlElement(name = "SpecialProvisions")
    protected String specialProvisions;
    @XmlElement(name = "PSASingaporeGroup")
    protected String psaSingaporeGroup;
    @XmlElement(name = "SymbolExtraMeaning")
    protected String symbolExtraMeaning;
    @XmlElement(name = "ImdgEmsNumber")
    protected String imdgEmsNumber;
    @XmlElement(name = "IsNos")
    protected String isNos;
    @XmlElement(name = "IsMarinePollutant")
    protected String isMarinePollutant;
    @XmlElement(name = "IsToxicInhalation")
    protected String isToxicInhalation;
    @XmlElement(name = "InhalationHazardZone")
    protected String inhalationHazardZone;
    @XmlElement(name = "IsPassengerAircraftForbid")
    protected String isPassengerAircraftForbid;
    @XmlElement(name = "IsCommercialAircraftForbid")
    protected String isCommercialAircraftForbid;
    @XmlElement(name = "ERG")
    protected String erg;
    @XmlElement(name = "ERGAir")
    protected String ergAir;
    @XmlElement(name = "EMS")
    protected String ems;
    @XmlElement(name = "HazCompatGroup")
    protected String hazCompatGroup;

    /**
     * Gets the value of the hazmatGenericGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getHazmatGenericGid() {
        return hazmatGenericGid;
    }

    /**
     * Sets the value of the hazmatGenericGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setHazmatGenericGid(GLogXMLGidType value) {
        this.hazmatGenericGid = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the properShippingName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProperShippingName() {
        return properShippingName;
    }

    /**
     * Sets the value of the properShippingName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProperShippingName(String value) {
        this.properShippingName = value;
    }

    /**
     * Gets the value of the identificationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationNumber() {
        return identificationNumber;
    }

    /**
     * Sets the value of the identificationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationNumber(String value) {
        this.identificationNumber = value;
    }

    /**
     * Gets the value of the hazardousClass property.
     * 
     * @return
     *     possible object is
     *     {@link HazardousClassType }
     *     
     */
    public HazardousClassType getHazardousClass() {
        return hazardousClass;
    }

    /**
     * Sets the value of the hazardousClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazardousClassType }
     *     
     */
    public void setHazardousClass(HazardousClassType value) {
        this.hazardousClass = value;
    }

    /**
     * Gets the value of the packagingGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagingGroup() {
        return packagingGroup;
    }

    /**
     * Sets the value of the packagingGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagingGroup(String value) {
        this.packagingGroup = value;
    }

    /**
     * Gets the value of the subsidiaryHazard property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubsidiaryHazard() {
        return subsidiaryHazard;
    }

    /**
     * Sets the value of the subsidiaryHazard property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubsidiaryHazard(String value) {
        this.subsidiaryHazard = value;
    }

    /**
     * Gets the value of the regulatoryAgencyDataSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegulatoryAgencyDataSource() {
        return regulatoryAgencyDataSource;
    }

    /**
     * Sets the value of the regulatoryAgencyDataSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegulatoryAgencyDataSource(String value) {
        this.regulatoryAgencyDataSource = value;
    }

    /**
     * Gets the value of the specialProvisions property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialProvisions() {
        return specialProvisions;
    }

    /**
     * Sets the value of the specialProvisions property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialProvisions(String value) {
        this.specialProvisions = value;
    }

    /**
     * Gets the value of the psaSingaporeGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSASingaporeGroup() {
        return psaSingaporeGroup;
    }

    /**
     * Sets the value of the psaSingaporeGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSASingaporeGroup(String value) {
        this.psaSingaporeGroup = value;
    }

    /**
     * Gets the value of the symbolExtraMeaning property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSymbolExtraMeaning() {
        return symbolExtraMeaning;
    }

    /**
     * Sets the value of the symbolExtraMeaning property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSymbolExtraMeaning(String value) {
        this.symbolExtraMeaning = value;
    }

    /**
     * Gets the value of the imdgEmsNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImdgEmsNumber() {
        return imdgEmsNumber;
    }

    /**
     * Sets the value of the imdgEmsNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImdgEmsNumber(String value) {
        this.imdgEmsNumber = value;
    }

    /**
     * Gets the value of the isNos property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsNos() {
        return isNos;
    }

    /**
     * Sets the value of the isNos property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsNos(String value) {
        this.isNos = value;
    }

    /**
     * Gets the value of the isMarinePollutant property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMarinePollutant() {
        return isMarinePollutant;
    }

    /**
     * Sets the value of the isMarinePollutant property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMarinePollutant(String value) {
        this.isMarinePollutant = value;
    }

    /**
     * Gets the value of the isToxicInhalation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsToxicInhalation() {
        return isToxicInhalation;
    }

    /**
     * Sets the value of the isToxicInhalation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsToxicInhalation(String value) {
        this.isToxicInhalation = value;
    }

    /**
     * Gets the value of the inhalationHazardZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInhalationHazardZone() {
        return inhalationHazardZone;
    }

    /**
     * Sets the value of the inhalationHazardZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInhalationHazardZone(String value) {
        this.inhalationHazardZone = value;
    }

    /**
     * Gets the value of the isPassengerAircraftForbid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPassengerAircraftForbid() {
        return isPassengerAircraftForbid;
    }

    /**
     * Sets the value of the isPassengerAircraftForbid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPassengerAircraftForbid(String value) {
        this.isPassengerAircraftForbid = value;
    }

    /**
     * Gets the value of the isCommercialAircraftForbid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCommercialAircraftForbid() {
        return isCommercialAircraftForbid;
    }

    /**
     * Sets the value of the isCommercialAircraftForbid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCommercialAircraftForbid(String value) {
        this.isCommercialAircraftForbid = value;
    }

    /**
     * Gets the value of the erg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERG() {
        return erg;
    }

    /**
     * Sets the value of the erg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERG(String value) {
        this.erg = value;
    }

    /**
     * Gets the value of the ergAir property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getERGAir() {
        return ergAir;
    }

    /**
     * Sets the value of the ergAir property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setERGAir(String value) {
        this.ergAir = value;
    }

    /**
     * Gets the value of the ems property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMS() {
        return ems;
    }

    /**
     * Sets the value of the ems property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMS(String value) {
        this.ems = value;
    }

    /**
     * Gets the value of the hazCompatGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHazCompatGroup() {
        return hazCompatGroup;
    }

    /**
     * Sets the value of the hazCompatGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHazCompatGroup(String value) {
        this.hazCompatGroup = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * An VoucherRefnum is an alternate way of referring to a Voucher.
 * 
 * <p>Java class for VoucherRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VoucherRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="VoucherRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="VoucherRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VoucherRefnumType", propOrder = {
    "voucherRefnumQualifierGid",
    "voucherRefnumValue"
})
public class VoucherRefnumType {

    @XmlElement(name = "VoucherRefnumQualifierGid", required = true)
    protected GLogXMLGidType voucherRefnumQualifierGid;
    @XmlElement(name = "VoucherRefnumValue", required = true)
    protected String voucherRefnumValue;

    /**
     * Gets the value of the voucherRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getVoucherRefnumQualifierGid() {
        return voucherRefnumQualifierGid;
    }

    /**
     * Sets the value of the voucherRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setVoucherRefnumQualifierGid(GLogXMLGidType value) {
        this.voucherRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the voucherRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherRefnumValue() {
        return voucherRefnumValue;
    }

    /**
     * Sets the value of the voucherRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherRefnumValue(String value) {
        this.voucherRefnumValue = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Provides a breakdown of costs (including cost type, amount, and associated codes).
 *          
 * 
 * <p>Java class for CostDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CostDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CostType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="IsWeighted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="CostDetailInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CostDetailInfoType" minOccurs="0"/&gt;
 *         &lt;element name="CostCategoryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CostDetailsType", propOrder = {
    "costType",
    "cost",
    "isWeighted",
    "accessorialCodeGid",
    "costDetailInfo",
    "costCategoryGid",
    "specialServiceGid"
})
public class CostDetailsType {

    @XmlElement(name = "CostType", required = true)
    protected String costType;
    @XmlElement(name = "Cost", required = true)
    protected GLogXMLFinancialAmountType cost;
    @XmlElement(name = "IsWeighted")
    protected String isWeighted;
    @XmlElement(name = "AccessorialCodeGid")
    protected GLogXMLGidType accessorialCodeGid;
    @XmlElement(name = "CostDetailInfo")
    protected CostDetailInfoType costDetailInfo;
    @XmlElement(name = "CostCategoryGid")
    protected GLogXMLGidType costCategoryGid;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;

    /**
     * Gets the value of the costType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostType() {
        return costType;
    }

    /**
     * Sets the value of the costType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostType(String value) {
        this.costType = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

    /**
     * Gets the value of the isWeighted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsWeighted() {
        return isWeighted;
    }

    /**
     * Sets the value of the isWeighted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsWeighted(String value) {
        this.isWeighted = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCodeGid() {
        return accessorialCodeGid;
    }

    /**
     * Sets the value of the accessorialCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCodeGid(GLogXMLGidType value) {
        this.accessorialCodeGid = value;
    }

    /**
     * Gets the value of the costDetailInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CostDetailInfoType }
     *     
     */
    public CostDetailInfoType getCostDetailInfo() {
        return costDetailInfo;
    }

    /**
     * Sets the value of the costDetailInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CostDetailInfoType }
     *     
     */
    public void setCostDetailInfo(CostDetailInfoType value) {
        this.costDetailInfo = value;
    }

    /**
     * Gets the value of the costCategoryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCostCategoryGid() {
        return costCategoryGid;
    }

    /**
     * Sets the value of the costCategoryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCostCategoryGid(GLogXMLGidType value) {
        this.costCategoryGid = value;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Sets the value of the specialServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

}

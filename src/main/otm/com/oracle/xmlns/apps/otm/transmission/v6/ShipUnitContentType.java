
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ShipUnitContent specifies what is inside a particular ship unit in terms of items and quantity.
 * 
 * <p>Java class for ShipUnitContentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipUnitContentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PackagedItemRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}PackagedItemRefType" minOccurs="0"/&gt;
 *         &lt;element name="HazmatItemRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}HazmatItemRefType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType"/&gt;
 *           &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ItemQuantity" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemQuantityType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLShipUnitSpecRefType" minOccurs="0"/&gt;
 *         &lt;element name="PackagedItemSpecCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="WeightVolumePerShipUnit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="CountPerShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;sequence minOccurs="0"&gt;
 *             &lt;element name="InitialItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *             &lt;element name="ItemAttributes" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ItemAttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *             &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}InvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence minOccurs="0"&gt;
 *             &lt;element name="TransOrderLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *             &lt;element name="ReleaseInstrSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="NetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *             &lt;element name="SecondaryWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *             &lt;element name="SecondaryNetWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence minOccurs="0"&gt;
 *             &lt;element name="ReleaseShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *             &lt;element name="ReleaseShipUnitLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="ReceivedWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightVolumeType" minOccurs="0"/&gt;
 *             &lt;element name="ReceivedPackageItemCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *             &lt;element name="UserDefinedCommodityGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ShipUnitLineRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitLineRefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IsHazardous" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipUnitContentType", propOrder = {
    "packagedItemRef",
    "hazmatItemRef",
    "intSavedQuery",
    "lineNumber",
    "itemQuantity",
    "packagedItemSpecRef",
    "packagedItemSpecCount",
    "weightVolumePerShipUnit",
    "countPerShipUnit",
    "releaseGid",
    "releaseLineGid",
    "transOrderGid",
    "initialItemGid",
    "itemAttributes",
    "involvedParty",
    "transOrderLineGid",
    "releaseInstrSeq",
    "netWeightVolume",
    "secondaryWeightVolume",
    "secondaryNetWeightVolume",
    "releaseShipUnitGid",
    "releaseShipUnitLineNumber",
    "receivedWeightVolume",
    "receivedPackageItemCount",
    "userDefinedCommodityGid",
    "shipUnitLineRefnum",
    "remark",
    "status",
    "isHazardous"
})
public class ShipUnitContentType {

    @XmlElement(name = "PackagedItemRef")
    protected PackagedItemRefType packagedItemRef;
    @XmlElement(name = "HazmatItemRef")
    protected HazmatItemRefType hazmatItemRef;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "ItemQuantity")
    protected ItemQuantityType itemQuantity;
    @XmlElement(name = "PackagedItemSpecRef")
    protected GLogXMLShipUnitSpecRefType packagedItemSpecRef;
    @XmlElement(name = "PackagedItemSpecCount")
    protected String packagedItemSpecCount;
    @XmlElement(name = "WeightVolumePerShipUnit")
    protected WeightVolumeType weightVolumePerShipUnit;
    @XmlElement(name = "CountPerShipUnit")
    protected String countPerShipUnit;
    @XmlElement(name = "ReleaseGid")
    protected GLogXMLGidType releaseGid;
    @XmlElement(name = "ReleaseLineGid")
    protected GLogXMLGidType releaseLineGid;
    @XmlElement(name = "TransOrderGid")
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "InitialItemGid")
    protected GLogXMLGidType initialItemGid;
    @XmlElement(name = "ItemAttributes")
    protected List<ItemAttributeType> itemAttributes;
    @XmlElement(name = "InvolvedParty")
    protected List<InvolvedPartyType> involvedParty;
    @XmlElement(name = "TransOrderLineGid")
    protected GLogXMLGidType transOrderLineGid;
    @XmlElement(name = "ReleaseInstrSeq")
    protected String releaseInstrSeq;
    @XmlElement(name = "NetWeightVolume")
    protected WeightVolumeType netWeightVolume;
    @XmlElement(name = "SecondaryWeightVolume")
    protected WeightVolumeType secondaryWeightVolume;
    @XmlElement(name = "SecondaryNetWeightVolume")
    protected WeightVolumeType secondaryNetWeightVolume;
    @XmlElement(name = "ReleaseShipUnitGid")
    protected GLogXMLGidType releaseShipUnitGid;
    @XmlElement(name = "ReleaseShipUnitLineNumber")
    protected String releaseShipUnitLineNumber;
    @XmlElement(name = "ReceivedWeightVolume")
    protected WeightVolumeType receivedWeightVolume;
    @XmlElement(name = "ReceivedPackageItemCount")
    protected String receivedPackageItemCount;
    @XmlElement(name = "UserDefinedCommodityGid")
    protected GLogXMLGidType userDefinedCommodityGid;
    @XmlElement(name = "ShipUnitLineRefnum")
    protected List<ShipUnitLineRefnumType> shipUnitLineRefnum;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "IsHazardous")
    protected String isHazardous;

    /**
     * Gets the value of the packagedItemRef property.
     * 
     * @return
     *     possible object is
     *     {@link PackagedItemRefType }
     *     
     */
    public PackagedItemRefType getPackagedItemRef() {
        return packagedItemRef;
    }

    /**
     * Sets the value of the packagedItemRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagedItemRefType }
     *     
     */
    public void setPackagedItemRef(PackagedItemRefType value) {
        this.packagedItemRef = value;
    }

    /**
     * Gets the value of the hazmatItemRef property.
     * 
     * @return
     *     possible object is
     *     {@link HazmatItemRefType }
     *     
     */
    public HazmatItemRefType getHazmatItemRef() {
        return hazmatItemRef;
    }

    /**
     * Sets the value of the hazmatItemRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link HazmatItemRefType }
     *     
     */
    public void setHazmatItemRef(HazmatItemRefType value) {
        this.hazmatItemRef = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the itemQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link ItemQuantityType }
     *     
     */
    public ItemQuantityType getItemQuantity() {
        return itemQuantity;
    }

    /**
     * Sets the value of the itemQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link ItemQuantityType }
     *     
     */
    public void setItemQuantity(ItemQuantityType value) {
        this.itemQuantity = value;
    }

    /**
     * Gets the value of the packagedItemSpecRef property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public GLogXMLShipUnitSpecRefType getPackagedItemSpecRef() {
        return packagedItemSpecRef;
    }

    /**
     * Sets the value of the packagedItemSpecRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLShipUnitSpecRefType }
     *     
     */
    public void setPackagedItemSpecRef(GLogXMLShipUnitSpecRefType value) {
        this.packagedItemSpecRef = value;
    }

    /**
     * Gets the value of the packagedItemSpecCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackagedItemSpecCount() {
        return packagedItemSpecCount;
    }

    /**
     * Sets the value of the packagedItemSpecCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackagedItemSpecCount(String value) {
        this.packagedItemSpecCount = value;
    }

    /**
     * Gets the value of the weightVolumePerShipUnit property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getWeightVolumePerShipUnit() {
        return weightVolumePerShipUnit;
    }

    /**
     * Sets the value of the weightVolumePerShipUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setWeightVolumePerShipUnit(WeightVolumeType value) {
        this.weightVolumePerShipUnit = value;
    }

    /**
     * Gets the value of the countPerShipUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountPerShipUnit() {
        return countPerShipUnit;
    }

    /**
     * Sets the value of the countPerShipUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountPerShipUnit(String value) {
        this.countPerShipUnit = value;
    }

    /**
     * Gets the value of the releaseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseGid() {
        return releaseGid;
    }

    /**
     * Sets the value of the releaseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseGid(GLogXMLGidType value) {
        this.releaseGid = value;
    }

    /**
     * Gets the value of the releaseLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseLineGid() {
        return releaseLineGid;
    }

    /**
     * Sets the value of the releaseLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseLineGid(GLogXMLGidType value) {
        this.releaseLineGid = value;
    }

    /**
     * Gets the value of the transOrderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Sets the value of the transOrderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Gets the value of the initialItemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInitialItemGid() {
        return initialItemGid;
    }

    /**
     * Sets the value of the initialItemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInitialItemGid(GLogXMLGidType value) {
        this.initialItemGid = value;
    }

    /**
     * Gets the value of the itemAttributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the itemAttributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getItemAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ItemAttributeType }
     * 
     * 
     */
    public List<ItemAttributeType> getItemAttributes() {
        if (itemAttributes == null) {
            itemAttributes = new ArrayList<ItemAttributeType>();
        }
        return this.itemAttributes;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyType }
     * 
     * 
     */
    public List<InvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<InvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the transOrderLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderLineGid() {
        return transOrderLineGid;
    }

    /**
     * Sets the value of the transOrderLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderLineGid(GLogXMLGidType value) {
        this.transOrderLineGid = value;
    }

    /**
     * Gets the value of the releaseInstrSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseInstrSeq() {
        return releaseInstrSeq;
    }

    /**
     * Sets the value of the releaseInstrSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseInstrSeq(String value) {
        this.releaseInstrSeq = value;
    }

    /**
     * Gets the value of the netWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getNetWeightVolume() {
        return netWeightVolume;
    }

    /**
     * Sets the value of the netWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setNetWeightVolume(WeightVolumeType value) {
        this.netWeightVolume = value;
    }

    /**
     * Gets the value of the secondaryWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getSecondaryWeightVolume() {
        return secondaryWeightVolume;
    }

    /**
     * Sets the value of the secondaryWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setSecondaryWeightVolume(WeightVolumeType value) {
        this.secondaryWeightVolume = value;
    }

    /**
     * Gets the value of the secondaryNetWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getSecondaryNetWeightVolume() {
        return secondaryNetWeightVolume;
    }

    /**
     * Sets the value of the secondaryNetWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setSecondaryNetWeightVolume(WeightVolumeType value) {
        this.secondaryNetWeightVolume = value;
    }

    /**
     * Gets the value of the releaseShipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseShipUnitGid() {
        return releaseShipUnitGid;
    }

    /**
     * Sets the value of the releaseShipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseShipUnitGid(GLogXMLGidType value) {
        this.releaseShipUnitGid = value;
    }

    /**
     * Gets the value of the releaseShipUnitLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReleaseShipUnitLineNumber() {
        return releaseShipUnitLineNumber;
    }

    /**
     * Sets the value of the releaseShipUnitLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReleaseShipUnitLineNumber(String value) {
        this.releaseShipUnitLineNumber = value;
    }

    /**
     * Gets the value of the receivedWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link WeightVolumeType }
     *     
     */
    public WeightVolumeType getReceivedWeightVolume() {
        return receivedWeightVolume;
    }

    /**
     * Sets the value of the receivedWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeightVolumeType }
     *     
     */
    public void setReceivedWeightVolume(WeightVolumeType value) {
        this.receivedWeightVolume = value;
    }

    /**
     * Gets the value of the receivedPackageItemCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivedPackageItemCount() {
        return receivedPackageItemCount;
    }

    /**
     * Sets the value of the receivedPackageItemCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivedPackageItemCount(String value) {
        this.receivedPackageItemCount = value;
    }

    /**
     * Gets the value of the userDefinedCommodityGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCommodityGid() {
        return userDefinedCommodityGid;
    }

    /**
     * Sets the value of the userDefinedCommodityGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCommodityGid(GLogXMLGidType value) {
        this.userDefinedCommodityGid = value;
    }

    /**
     * Gets the value of the shipUnitLineRefnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipUnitLineRefnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipUnitLineRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipUnitLineRefnumType }
     * 
     * 
     */
    public List<ShipUnitLineRefnumType> getShipUnitLineRefnum() {
        if (shipUnitLineRefnum == null) {
            shipUnitLineRefnum = new ArrayList<ShipUnitLineRefnumType>();
        }
        return this.shipUnitLineRefnum;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the isHazardous property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardous() {
        return isHazardous;
    }

    /**
     * Sets the value of the isHazardous property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardous(String value) {
        this.isHazardous = value;
    }

}

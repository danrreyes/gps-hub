
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ReleaseInstruction is used to identify the line item or ship unit and the quantity to
 *             release in the base order.
 *             It provides further instructions for creating order releases.
 *          
 * 
 * <p>Java class for ReleaseInstructionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReleaseInstructionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;element name="ReleaseInstructionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="TransOrderGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;choice&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="TransOrderLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *             &lt;element name="QuantityToRelease"&gt;
 *               &lt;complexType&gt;
 *                 &lt;complexContent&gt;
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                     &lt;choice&gt;
 *                       &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType"/&gt;
 *                       &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType"/&gt;
 *                       &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                     &lt;/choice&gt;
 *                   &lt;/restriction&gt;
 *                 &lt;/complexContent&gt;
 *               &lt;/complexType&gt;
 *             &lt;/element&gt;
 *             &lt;element name="LineRelInstInfo" minOccurs="0"&gt;
 *               &lt;complexType&gt;
 *                 &lt;complexContent&gt;
 *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                     &lt;sequence&gt;
 *                       &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                     &lt;/sequence&gt;
 *                   &lt;/restriction&gt;
 *                 &lt;/complexContent&gt;
 *               &lt;/complexType&gt;
 *             &lt;/element&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *             &lt;element name="ShipUnitReleaseCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *             &lt;element name="ShipUnitRelInstInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipUnitRelInstInfoType" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="TotalWeightVolume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLWeightVolumeType" minOccurs="0"/&gt;
 *         &lt;element name="TransportHandlingUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="SourceLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="DestLocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TimeWindow" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TimeWindowType" minOccurs="0"/&gt;
 *         &lt;element name="ReleaseDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ItemTag4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReleaseInstructionType", propOrder = {
    "releaseInstructionGid",
    "sequenceNumber",
    "transactionCode",
    "transOrderGid",
    "transOrderLineGid",
    "quantityToRelease",
    "lineRelInstInfo",
    "shipUnitGid",
    "shipUnitReleaseCount",
    "shipUnitRelInstInfo",
    "totalWeightVolume",
    "transportHandlingUnitGid",
    "sourceLocationGid",
    "destLocationGid",
    "timeWindow",
    "releaseDate",
    "itemTag1",
    "itemTag2",
    "itemTag3",
    "itemTag4"
})
public class ReleaseInstructionType
    extends OTMTransactionIn
{

    @XmlElement(name = "ReleaseInstructionGid")
    protected GLogXMLGidType releaseInstructionGid;
    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "TransOrderGid", required = true)
    protected GLogXMLGidType transOrderGid;
    @XmlElement(name = "TransOrderLineGid")
    protected GLogXMLGidType transOrderLineGid;
    @XmlElement(name = "QuantityToRelease")
    protected ReleaseInstructionType.QuantityToRelease quantityToRelease;
    @XmlElement(name = "LineRelInstInfo")
    protected ReleaseInstructionType.LineRelInstInfo lineRelInstInfo;
    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "ShipUnitReleaseCount")
    protected String shipUnitReleaseCount;
    @XmlElement(name = "ShipUnitRelInstInfo")
    protected ShipUnitRelInstInfoType shipUnitRelInstInfo;
    @XmlElement(name = "TotalWeightVolume")
    protected GLogXMLWeightVolumeType totalWeightVolume;
    @XmlElement(name = "TransportHandlingUnitGid")
    protected GLogXMLGidType transportHandlingUnitGid;
    @XmlElement(name = "SourceLocationGid")
    protected GLogXMLGidType sourceLocationGid;
    @XmlElement(name = "DestLocationGid")
    protected GLogXMLGidType destLocationGid;
    @XmlElement(name = "TimeWindow")
    protected TimeWindowType timeWindow;
    @XmlElement(name = "ReleaseDate")
    protected GLogDateTimeType releaseDate;
    @XmlElement(name = "ItemTag1")
    protected String itemTag1;
    @XmlElement(name = "ItemTag2")
    protected String itemTag2;
    @XmlElement(name = "ItemTag3")
    protected String itemTag3;
    @XmlElement(name = "ItemTag4")
    protected String itemTag4;

    /**
     * Gets the value of the releaseInstructionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getReleaseInstructionGid() {
        return releaseInstructionGid;
    }

    /**
     * Sets the value of the releaseInstructionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setReleaseInstructionGid(GLogXMLGidType value) {
        this.releaseInstructionGid = value;
    }

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the transOrderGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderGid() {
        return transOrderGid;
    }

    /**
     * Sets the value of the transOrderGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderGid(GLogXMLGidType value) {
        this.transOrderGid = value;
    }

    /**
     * Gets the value of the transOrderLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransOrderLineGid() {
        return transOrderLineGid;
    }

    /**
     * Sets the value of the transOrderLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransOrderLineGid(GLogXMLGidType value) {
        this.transOrderLineGid = value;
    }

    /**
     * Gets the value of the quantityToRelease property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseInstructionType.QuantityToRelease }
     *     
     */
    public ReleaseInstructionType.QuantityToRelease getQuantityToRelease() {
        return quantityToRelease;
    }

    /**
     * Sets the value of the quantityToRelease property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseInstructionType.QuantityToRelease }
     *     
     */
    public void setQuantityToRelease(ReleaseInstructionType.QuantityToRelease value) {
        this.quantityToRelease = value;
    }

    /**
     * Gets the value of the lineRelInstInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseInstructionType.LineRelInstInfo }
     *     
     */
    public ReleaseInstructionType.LineRelInstInfo getLineRelInstInfo() {
        return lineRelInstInfo;
    }

    /**
     * Sets the value of the lineRelInstInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseInstructionType.LineRelInstInfo }
     *     
     */
    public void setLineRelInstInfo(ReleaseInstructionType.LineRelInstInfo value) {
        this.lineRelInstInfo = value;
    }

    /**
     * Gets the value of the shipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Sets the value of the shipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Gets the value of the shipUnitReleaseCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipUnitReleaseCount() {
        return shipUnitReleaseCount;
    }

    /**
     * Sets the value of the shipUnitReleaseCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipUnitReleaseCount(String value) {
        this.shipUnitReleaseCount = value;
    }

    /**
     * Gets the value of the shipUnitRelInstInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ShipUnitRelInstInfoType }
     *     
     */
    public ShipUnitRelInstInfoType getShipUnitRelInstInfo() {
        return shipUnitRelInstInfo;
    }

    /**
     * Sets the value of the shipUnitRelInstInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipUnitRelInstInfoType }
     *     
     */
    public void setShipUnitRelInstInfo(ShipUnitRelInstInfoType value) {
        this.shipUnitRelInstInfo = value;
    }

    /**
     * Gets the value of the totalWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public GLogXMLWeightVolumeType getTotalWeightVolume() {
        return totalWeightVolume;
    }

    /**
     * Sets the value of the totalWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLWeightVolumeType }
     *     
     */
    public void setTotalWeightVolume(GLogXMLWeightVolumeType value) {
        this.totalWeightVolume = value;
    }

    /**
     * Gets the value of the transportHandlingUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getTransportHandlingUnitGid() {
        return transportHandlingUnitGid;
    }

    /**
     * Sets the value of the transportHandlingUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setTransportHandlingUnitGid(GLogXMLGidType value) {
        this.transportHandlingUnitGid = value;
    }

    /**
     * Gets the value of the sourceLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSourceLocationGid() {
        return sourceLocationGid;
    }

    /**
     * Sets the value of the sourceLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSourceLocationGid(GLogXMLGidType value) {
        this.sourceLocationGid = value;
    }

    /**
     * Gets the value of the destLocationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDestLocationGid() {
        return destLocationGid;
    }

    /**
     * Sets the value of the destLocationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDestLocationGid(GLogXMLGidType value) {
        this.destLocationGid = value;
    }

    /**
     * Gets the value of the timeWindow property.
     * 
     * @return
     *     possible object is
     *     {@link TimeWindowType }
     *     
     */
    public TimeWindowType getTimeWindow() {
        return timeWindow;
    }

    /**
     * Sets the value of the timeWindow property.
     * 
     * @param value
     *     allowed object is
     *     {@link TimeWindowType }
     *     
     */
    public void setTimeWindow(TimeWindowType value) {
        this.timeWindow = value;
    }

    /**
     * Gets the value of the releaseDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getReleaseDate() {
        return releaseDate;
    }

    /**
     * Sets the value of the releaseDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setReleaseDate(GLogDateTimeType value) {
        this.releaseDate = value;
    }

    /**
     * Gets the value of the itemTag1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag1() {
        return itemTag1;
    }

    /**
     * Sets the value of the itemTag1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag1(String value) {
        this.itemTag1 = value;
    }

    /**
     * Gets the value of the itemTag2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag2() {
        return itemTag2;
    }

    /**
     * Sets the value of the itemTag2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag2(String value) {
        this.itemTag2 = value;
    }

    /**
     * Gets the value of the itemTag3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag3() {
        return itemTag3;
    }

    /**
     * Sets the value of the itemTag3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag3(String value) {
        this.itemTag3 = value;
    }

    /**
     * Gets the value of the itemTag4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemTag4() {
        return itemTag4;
    }

    /**
     * Sets the value of the itemTag4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemTag4(String value) {
        this.itemTag4 = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="ShipUnitCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "shipUnitCount"
    })
    public static class LineRelInstInfo {

        @XmlElement(name = "ShipUnitCount")
        protected String shipUnitCount;

        /**
         * Gets the value of the shipUnitCount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getShipUnitCount() {
            return shipUnitCount;
        }

        /**
         * Sets the value of the shipUnitCount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setShipUnitCount(String value) {
            this.shipUnitCount = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice&gt;
     *         &lt;element name="Weight" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}WeightType"/&gt;
     *         &lt;element name="Volume" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}VolumeType"/&gt;
     *         &lt;element name="PackagedItemCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "weight",
        "volume",
        "packagedItemCount"
    })
    public static class QuantityToRelease {

        @XmlElement(name = "Weight")
        protected WeightType weight;
        @XmlElement(name = "Volume")
        protected VolumeType volume;
        @XmlElement(name = "PackagedItemCount")
        protected String packagedItemCount;

        /**
         * Gets the value of the weight property.
         * 
         * @return
         *     possible object is
         *     {@link WeightType }
         *     
         */
        public WeightType getWeight() {
            return weight;
        }

        /**
         * Sets the value of the weight property.
         * 
         * @param value
         *     allowed object is
         *     {@link WeightType }
         *     
         */
        public void setWeight(WeightType value) {
            this.weight = value;
        }

        /**
         * Gets the value of the volume property.
         * 
         * @return
         *     possible object is
         *     {@link VolumeType }
         *     
         */
        public VolumeType getVolume() {
            return volume;
        }

        /**
         * Sets the value of the volume property.
         * 
         * @param value
         *     allowed object is
         *     {@link VolumeType }
         *     
         */
        public void setVolume(VolumeType value) {
            this.volume = value;
        }

        /**
         * Gets the value of the packagedItemCount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPackagedItemCount() {
            return packagedItemCount;
        }

        /**
         * Sets the value of the packagedItemCount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPackagedItemCount(String value) {
            this.packagedItemCount = value;
        }

    }

}

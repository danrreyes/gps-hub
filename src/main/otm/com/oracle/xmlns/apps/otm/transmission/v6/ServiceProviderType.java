
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * ServiceProvider is an optional structure specified only if a location represents a service provider.
 * 
 * <p>Java class for ServiceProviderType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceProviderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ScacGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ServiceProviderAlias" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ServiceProviderAliasType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ConditionalBookingProfile" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *                   &lt;element name="ConditionalBookingProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TenderResponseTime" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLDurationType" minOccurs="0"/&gt;
 *         &lt;element name="MatchRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="MatchValidRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AutoApproveRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AutoPaymentFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AllocationRuleProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsAllowTender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsDispatchByRegion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="OutXmlProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsAcceptSpotBids" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAcceptBroadcastTenders" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAcceptCondBooking" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsLocalizeBroadcastSpotContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SpotRateAdjustmentFactor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InvoicingProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsCopyInvDeltaToShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsAcceptByShipUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsInternalNVOCC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NVOCCDomainName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LineApproveTolProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsMinority" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Incumbent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Tier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScorecardValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EpaIndexValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BroadcastSpotContact" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLContactRefType" minOccurs="0"/&gt;
 *         &lt;element name="EquipMarkList" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}EquipMarkListType" minOccurs="0"/&gt;
 *         &lt;element name="ServprovService" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                   &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ServprovModeInfo" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *                   &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="ServprovPolicy" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PolicyType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *                   &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *                   &lt;element name="Issuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="CoverageAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="IsFleet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AllowSpotRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceProviderType", propOrder = {
    "scacGid",
    "serviceProviderAlias",
    "conditionalBookingProfile",
    "modeProfileGid",
    "tenderResponseTime",
    "matchRuleProfileGid",
    "matchValidRuleProfileGid",
    "autoApproveRuleProfileGid",
    "autoPaymentFlag",
    "allocationRuleProfileGid",
    "isAllowTender",
    "isDispatchByRegion",
    "rateServiceGid",
    "outXmlProfileGid",
    "isAcceptSpotBids",
    "isAcceptBroadcastTenders",
    "isAcceptCondBooking",
    "isLocalizeBroadcastSpotContact",
    "spotRateAdjustmentFactor",
    "invoicingProcess",
    "isCopyInvDeltaToShipment",
    "isAcceptByShipUnit",
    "isInternalNVOCC",
    "nvoccDomainName",
    "lineApproveTolProfileGid",
    "isMinority",
    "incumbent",
    "tier",
    "scorecardValue",
    "epaIndexValue",
    "broadcastSpotContact",
    "equipMarkList",
    "servprovService",
    "servprovModeInfo",
    "servprovPolicy",
    "isFleet",
    "allowSpotRating"
})
public class ServiceProviderType {

    @XmlElement(name = "ScacGid")
    protected GLogXMLGidType scacGid;
    @XmlElement(name = "ServiceProviderAlias")
    protected List<ServiceProviderAliasType> serviceProviderAlias;
    @XmlElement(name = "ConditionalBookingProfile")
    protected List<ServiceProviderType.ConditionalBookingProfile> conditionalBookingProfile;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "TenderResponseTime")
    protected GLogXMLDurationType tenderResponseTime;
    @XmlElement(name = "MatchRuleProfileGid")
    protected GLogXMLGidType matchRuleProfileGid;
    @XmlElement(name = "MatchValidRuleProfileGid")
    protected GLogXMLGidType matchValidRuleProfileGid;
    @XmlElement(name = "AutoApproveRuleProfileGid")
    protected GLogXMLGidType autoApproveRuleProfileGid;
    @XmlElement(name = "AutoPaymentFlag")
    protected String autoPaymentFlag;
    @XmlElement(name = "AllocationRuleProfileGid")
    protected GLogXMLGidType allocationRuleProfileGid;
    @XmlElement(name = "IsAllowTender")
    protected String isAllowTender;
    @XmlElement(name = "IsDispatchByRegion")
    protected String isDispatchByRegion;
    @XmlElement(name = "RateServiceGid")
    protected GLogXMLGidType rateServiceGid;
    @XmlElement(name = "OutXmlProfileGid")
    protected GLogXMLGidType outXmlProfileGid;
    @XmlElement(name = "IsAcceptSpotBids")
    protected String isAcceptSpotBids;
    @XmlElement(name = "IsAcceptBroadcastTenders")
    protected String isAcceptBroadcastTenders;
    @XmlElement(name = "IsAcceptCondBooking")
    protected String isAcceptCondBooking;
    @XmlElement(name = "IsLocalizeBroadcastSpotContact")
    protected String isLocalizeBroadcastSpotContact;
    @XmlElement(name = "SpotRateAdjustmentFactor")
    protected String spotRateAdjustmentFactor;
    @XmlElement(name = "InvoicingProcess")
    protected String invoicingProcess;
    @XmlElement(name = "IsCopyInvDeltaToShipment")
    protected String isCopyInvDeltaToShipment;
    @XmlElement(name = "IsAcceptByShipUnit")
    protected String isAcceptByShipUnit;
    @XmlElement(name = "IsInternalNVOCC")
    protected String isInternalNVOCC;
    @XmlElement(name = "NVOCCDomainName")
    protected String nvoccDomainName;
    @XmlElement(name = "LineApproveTolProfileGid")
    protected GLogXMLGidType lineApproveTolProfileGid;
    @XmlElement(name = "IsMinority")
    protected String isMinority;
    @XmlElement(name = "Incumbent")
    protected String incumbent;
    @XmlElement(name = "Tier")
    protected String tier;
    @XmlElement(name = "ScorecardValue")
    protected String scorecardValue;
    @XmlElement(name = "EpaIndexValue")
    protected String epaIndexValue;
    @XmlElement(name = "BroadcastSpotContact")
    protected GLogXMLContactRefType broadcastSpotContact;
    @XmlElement(name = "EquipMarkList")
    protected EquipMarkListType equipMarkList;
    @XmlElement(name = "ServprovService")
    protected List<ServiceProviderType.ServprovService> servprovService;
    @XmlElement(name = "ServprovModeInfo")
    protected List<ServiceProviderType.ServprovModeInfo> servprovModeInfo;
    @XmlElement(name = "ServprovPolicy")
    protected List<ServiceProviderType.ServprovPolicy> servprovPolicy;
    @XmlElement(name = "IsFleet")
    protected String isFleet;
    @XmlElement(name = "AllowSpotRating")
    protected String allowSpotRating;

    /**
     * Gets the value of the scacGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getScacGid() {
        return scacGid;
    }

    /**
     * Sets the value of the scacGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setScacGid(GLogXMLGidType value) {
        this.scacGid = value;
    }

    /**
     * Gets the value of the serviceProviderAlias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the serviceProviderAlias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceProviderAlias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderAliasType }
     * 
     * 
     */
    public List<ServiceProviderAliasType> getServiceProviderAlias() {
        if (serviceProviderAlias == null) {
            serviceProviderAlias = new ArrayList<ServiceProviderAliasType>();
        }
        return this.serviceProviderAlias;
    }

    /**
     * Gets the value of the conditionalBookingProfile property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the conditionalBookingProfile property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConditionalBookingProfile().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderType.ConditionalBookingProfile }
     * 
     * 
     */
    public List<ServiceProviderType.ConditionalBookingProfile> getConditionalBookingProfile() {
        if (conditionalBookingProfile == null) {
            conditionalBookingProfile = new ArrayList<ServiceProviderType.ConditionalBookingProfile>();
        }
        return this.conditionalBookingProfile;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the tenderResponseTime property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public GLogXMLDurationType getTenderResponseTime() {
        return tenderResponseTime;
    }

    /**
     * Sets the value of the tenderResponseTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLDurationType }
     *     
     */
    public void setTenderResponseTime(GLogXMLDurationType value) {
        this.tenderResponseTime = value;
    }

    /**
     * Gets the value of the matchRuleProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMatchRuleProfileGid() {
        return matchRuleProfileGid;
    }

    /**
     * Sets the value of the matchRuleProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMatchRuleProfileGid(GLogXMLGidType value) {
        this.matchRuleProfileGid = value;
    }

    /**
     * Gets the value of the matchValidRuleProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getMatchValidRuleProfileGid() {
        return matchValidRuleProfileGid;
    }

    /**
     * Sets the value of the matchValidRuleProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setMatchValidRuleProfileGid(GLogXMLGidType value) {
        this.matchValidRuleProfileGid = value;
    }

    /**
     * Gets the value of the autoApproveRuleProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAutoApproveRuleProfileGid() {
        return autoApproveRuleProfileGid;
    }

    /**
     * Sets the value of the autoApproveRuleProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAutoApproveRuleProfileGid(GLogXMLGidType value) {
        this.autoApproveRuleProfileGid = value;
    }

    /**
     * Gets the value of the autoPaymentFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAutoPaymentFlag() {
        return autoPaymentFlag;
    }

    /**
     * Sets the value of the autoPaymentFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAutoPaymentFlag(String value) {
        this.autoPaymentFlag = value;
    }

    /**
     * Gets the value of the allocationRuleProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAllocationRuleProfileGid() {
        return allocationRuleProfileGid;
    }

    /**
     * Sets the value of the allocationRuleProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAllocationRuleProfileGid(GLogXMLGidType value) {
        this.allocationRuleProfileGid = value;
    }

    /**
     * Gets the value of the isAllowTender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAllowTender() {
        return isAllowTender;
    }

    /**
     * Sets the value of the isAllowTender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAllowTender(String value) {
        this.isAllowTender = value;
    }

    /**
     * Gets the value of the isDispatchByRegion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDispatchByRegion() {
        return isDispatchByRegion;
    }

    /**
     * Sets the value of the isDispatchByRegion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDispatchByRegion(String value) {
        this.isDispatchByRegion = value;
    }

    /**
     * Gets the value of the rateServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateServiceGid() {
        return rateServiceGid;
    }

    /**
     * Sets the value of the rateServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateServiceGid(GLogXMLGidType value) {
        this.rateServiceGid = value;
    }

    /**
     * Gets the value of the outXmlProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getOutXmlProfileGid() {
        return outXmlProfileGid;
    }

    /**
     * Sets the value of the outXmlProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setOutXmlProfileGid(GLogXMLGidType value) {
        this.outXmlProfileGid = value;
    }

    /**
     * Gets the value of the isAcceptSpotBids property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptSpotBids() {
        return isAcceptSpotBids;
    }

    /**
     * Sets the value of the isAcceptSpotBids property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptSpotBids(String value) {
        this.isAcceptSpotBids = value;
    }

    /**
     * Gets the value of the isAcceptBroadcastTenders property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptBroadcastTenders() {
        return isAcceptBroadcastTenders;
    }

    /**
     * Sets the value of the isAcceptBroadcastTenders property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptBroadcastTenders(String value) {
        this.isAcceptBroadcastTenders = value;
    }

    /**
     * Gets the value of the isAcceptCondBooking property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptCondBooking() {
        return isAcceptCondBooking;
    }

    /**
     * Sets the value of the isAcceptCondBooking property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptCondBooking(String value) {
        this.isAcceptCondBooking = value;
    }

    /**
     * Gets the value of the isLocalizeBroadcastSpotContact property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLocalizeBroadcastSpotContact() {
        return isLocalizeBroadcastSpotContact;
    }

    /**
     * Sets the value of the isLocalizeBroadcastSpotContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLocalizeBroadcastSpotContact(String value) {
        this.isLocalizeBroadcastSpotContact = value;
    }

    /**
     * Gets the value of the spotRateAdjustmentFactor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpotRateAdjustmentFactor() {
        return spotRateAdjustmentFactor;
    }

    /**
     * Sets the value of the spotRateAdjustmentFactor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpotRateAdjustmentFactor(String value) {
        this.spotRateAdjustmentFactor = value;
    }

    /**
     * Gets the value of the invoicingProcess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicingProcess() {
        return invoicingProcess;
    }

    /**
     * Sets the value of the invoicingProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicingProcess(String value) {
        this.invoicingProcess = value;
    }

    /**
     * Gets the value of the isCopyInvDeltaToShipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCopyInvDeltaToShipment() {
        return isCopyInvDeltaToShipment;
    }

    /**
     * Sets the value of the isCopyInvDeltaToShipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCopyInvDeltaToShipment(String value) {
        this.isCopyInvDeltaToShipment = value;
    }

    /**
     * Gets the value of the isAcceptByShipUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsAcceptByShipUnit() {
        return isAcceptByShipUnit;
    }

    /**
     * Sets the value of the isAcceptByShipUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsAcceptByShipUnit(String value) {
        this.isAcceptByShipUnit = value;
    }

    /**
     * Gets the value of the isInternalNVOCC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsInternalNVOCC() {
        return isInternalNVOCC;
    }

    /**
     * Sets the value of the isInternalNVOCC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsInternalNVOCC(String value) {
        this.isInternalNVOCC = value;
    }

    /**
     * Gets the value of the nvoccDomainName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNVOCCDomainName() {
        return nvoccDomainName;
    }

    /**
     * Sets the value of the nvoccDomainName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNVOCCDomainName(String value) {
        this.nvoccDomainName = value;
    }

    /**
     * Gets the value of the lineApproveTolProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLineApproveTolProfileGid() {
        return lineApproveTolProfileGid;
    }

    /**
     * Sets the value of the lineApproveTolProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLineApproveTolProfileGid(GLogXMLGidType value) {
        this.lineApproveTolProfileGid = value;
    }

    /**
     * Gets the value of the isMinority property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsMinority() {
        return isMinority;
    }

    /**
     * Sets the value of the isMinority property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsMinority(String value) {
        this.isMinority = value;
    }

    /**
     * Gets the value of the incumbent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncumbent() {
        return incumbent;
    }

    /**
     * Sets the value of the incumbent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncumbent(String value) {
        this.incumbent = value;
    }

    /**
     * Gets the value of the tier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTier() {
        return tier;
    }

    /**
     * Sets the value of the tier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTier(String value) {
        this.tier = value;
    }

    /**
     * Gets the value of the scorecardValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScorecardValue() {
        return scorecardValue;
    }

    /**
     * Sets the value of the scorecardValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScorecardValue(String value) {
        this.scorecardValue = value;
    }

    /**
     * Gets the value of the epaIndexValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEpaIndexValue() {
        return epaIndexValue;
    }

    /**
     * Sets the value of the epaIndexValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEpaIndexValue(String value) {
        this.epaIndexValue = value;
    }

    /**
     * Gets the value of the broadcastSpotContact property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public GLogXMLContactRefType getBroadcastSpotContact() {
        return broadcastSpotContact;
    }

    /**
     * Sets the value of the broadcastSpotContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLContactRefType }
     *     
     */
    public void setBroadcastSpotContact(GLogXMLContactRefType value) {
        this.broadcastSpotContact = value;
    }

    /**
     * Gets the value of the equipMarkList property.
     * 
     * @return
     *     possible object is
     *     {@link EquipMarkListType }
     *     
     */
    public EquipMarkListType getEquipMarkList() {
        return equipMarkList;
    }

    /**
     * Sets the value of the equipMarkList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EquipMarkListType }
     *     
     */
    public void setEquipMarkList(EquipMarkListType value) {
        this.equipMarkList = value;
    }

    /**
     * Gets the value of the servprovService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the servprovService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServprovService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderType.ServprovService }
     * 
     * 
     */
    public List<ServiceProviderType.ServprovService> getServprovService() {
        if (servprovService == null) {
            servprovService = new ArrayList<ServiceProviderType.ServprovService>();
        }
        return this.servprovService;
    }

    /**
     * Gets the value of the servprovModeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the servprovModeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServprovModeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderType.ServprovModeInfo }
     * 
     * 
     */
    public List<ServiceProviderType.ServprovModeInfo> getServprovModeInfo() {
        if (servprovModeInfo == null) {
            servprovModeInfo = new ArrayList<ServiceProviderType.ServprovModeInfo>();
        }
        return this.servprovModeInfo;
    }

    /**
     * Gets the value of the servprovPolicy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the servprovPolicy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServprovPolicy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceProviderType.ServprovPolicy }
     * 
     * 
     */
    public List<ServiceProviderType.ServprovPolicy> getServprovPolicy() {
        if (servprovPolicy == null) {
            servprovPolicy = new ArrayList<ServiceProviderType.ServprovPolicy>();
        }
        return this.servprovPolicy;
    }

    /**
     * Gets the value of the isFleet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFleet() {
        return isFleet;
    }

    /**
     * Sets the value of the isFleet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFleet(String value) {
        this.isFleet = value;
    }

    /**
     * Gets the value of the allowSpotRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllowSpotRating() {
        return allowSpotRating;
    }

    /**
     * Sets the value of the allowSpotRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllowSpotRating(String value) {
        this.allowSpotRating = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
     *         &lt;element name="ConditionalBookingProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transportModeGid",
        "conditionalBookingProfileGid"
    })
    public static class ConditionalBookingProfile {

        @XmlElement(name = "TransportModeGid")
        protected GLogXMLGidType transportModeGid;
        @XmlElement(name = "ConditionalBookingProfileGid")
        protected GLogXMLGidType conditionalBookingProfileGid;

        /**
         * Gets the value of the transportModeGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getTransportModeGid() {
            return transportModeGid;
        }

        /**
         * Sets the value of the transportModeGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setTransportModeGid(GLogXMLGidType value) {
            this.transportModeGid = value;
        }

        /**
         * Gets the value of the conditionalBookingProfileGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getConditionalBookingProfileGid() {
            return conditionalBookingProfileGid;
        }

        /**
         * Sets the value of the conditionalBookingProfileGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setConditionalBookingProfileGid(GLogXMLGidType value) {
            this.conditionalBookingProfileGid = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="TransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="EquipmentGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transportModeGid",
        "equipmentGroupGid"
    })
    public static class ServprovModeInfo {

        @XmlElement(name = "TransportModeGid", required = true)
        protected GLogXMLGidType transportModeGid;
        @XmlElement(name = "EquipmentGroupGid")
        protected List<GLogXMLGidType> equipmentGroupGid;

        /**
         * Gets the value of the transportModeGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getTransportModeGid() {
            return transportModeGid;
        }

        /**
         * Sets the value of the transportModeGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setTransportModeGid(GLogXMLGidType value) {
            this.transportModeGid = value;
        }

        /**
         * Gets the value of the equipmentGroupGid property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the Jakarta XML Binding object.
         * This is why there is not a <CODE>set</CODE> method for the equipmentGroupGid property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEquipmentGroupGid().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GLogXMLGidType }
         * 
         * 
         */
        public List<GLogXMLGidType> getEquipmentGroupGid() {
            if (equipmentGroupGid == null) {
                equipmentGroupGid = new ArrayList<GLogXMLGidType>();
            }
            return this.equipmentGroupGid;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PolicyType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
     *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
     *         &lt;element name="Issuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="PolicyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="CoverageAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sequenceNumber",
        "policyType",
        "effectiveDate",
        "expirationDate",
        "issuer",
        "policyNumber",
        "coverageAmount"
    })
    public static class ServprovPolicy {

        @XmlElement(name = "SequenceNumber")
        protected String sequenceNumber;
        @XmlElement(name = "PolicyType", required = true)
        protected String policyType;
        @XmlElement(name = "EffectiveDate", required = true)
        protected GLogDateTimeType effectiveDate;
        @XmlElement(name = "ExpirationDate", required = true)
        protected GLogDateTimeType expirationDate;
        @XmlElement(name = "Issuer")
        protected String issuer;
        @XmlElement(name = "PolicyNumber")
        protected String policyNumber;
        @XmlElement(name = "CoverageAmount")
        protected GLogXMLFinancialAmountType coverageAmount;

        /**
         * Gets the value of the sequenceNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSequenceNumber() {
            return sequenceNumber;
        }

        /**
         * Sets the value of the sequenceNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSequenceNumber(String value) {
            this.sequenceNumber = value;
        }

        /**
         * Gets the value of the policyType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyType() {
            return policyType;
        }

        /**
         * Sets the value of the policyType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyType(String value) {
            this.policyType = value;
        }

        /**
         * Gets the value of the effectiveDate property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * Sets the value of the effectiveDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setEffectiveDate(GLogDateTimeType value) {
            this.effectiveDate = value;
        }

        /**
         * Gets the value of the expirationDate property.
         * 
         * @return
         *     possible object is
         *     {@link GLogDateTimeType }
         *     
         */
        public GLogDateTimeType getExpirationDate() {
            return expirationDate;
        }

        /**
         * Sets the value of the expirationDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogDateTimeType }
         *     
         */
        public void setExpirationDate(GLogDateTimeType value) {
            this.expirationDate = value;
        }

        /**
         * Gets the value of the issuer property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIssuer() {
            return issuer;
        }

        /**
         * Sets the value of the issuer property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIssuer(String value) {
            this.issuer = value;
        }

        /**
         * Gets the value of the policyNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPolicyNumber() {
            return policyNumber;
        }

        /**
         * Sets the value of the policyNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPolicyNumber(String value) {
            this.policyNumber = value;
        }

        /**
         * Gets the value of the coverageAmount property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLFinancialAmountType }
         *     
         */
        public GLogXMLFinancialAmountType getCoverageAmount() {
            return coverageAmount;
        }

        /**
         * Sets the value of the coverageAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLFinancialAmountType }
         *     
         */
        public void setCoverageAmount(GLogXMLFinancialAmountType value) {
            this.coverageAmount = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="LocationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *         &lt;element name="RateServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "locationGid",
        "rateServiceGid"
    })
    public static class ServprovService {

        @XmlElement(name = "LocationGid", required = true)
        protected GLogXMLGidType locationGid;
        @XmlElement(name = "RateServiceGid", required = true)
        protected GLogXMLGidType rateServiceGid;

        /**
         * Gets the value of the locationGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getLocationGid() {
            return locationGid;
        }

        /**
         * Sets the value of the locationGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setLocationGid(GLogXMLGidType value) {
            this.locationGid = value;
        }

        /**
         * Gets the value of the rateServiceGid property.
         * 
         * @return
         *     possible object is
         *     {@link GLogXMLGidType }
         *     
         */
        public GLogXMLGidType getRateServiceGid() {
            return rateServiceGid;
        }

        /**
         * Sets the value of the rateServiceGid property.
         * 
         * @param value
         *     allowed object is
         *     {@link GLogXMLGidType }
         *     
         */
        public void setRateServiceGid(GLogXMLGidType value) {
            this.rateServiceGid = value;
        }

    }

}

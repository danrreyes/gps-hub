
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Contains the breakdown of the cost associated with the shipment.
 *             Note: The ShipmentCostSeqno element is currently the primary key for the Shipment_Cost table, and therefore must be unique across all Shipments.
 *             When inserting a new ShipmentCost record for a Shipment, specifying a TransactionCode = 'I' will auto generate a new number.
 *             When updating an existing record, the IntSavedQuery element can be used to search for the ShipmentCostSeqno value.
 *             Use of the IntSavedQuery is limited to returning a single value, such that the IntSavedQuery/IsMultiMatch cannot be set to 'Y'.
 *          
 * 
 * <p>Java class for ShipmentCostType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentCostType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentCostSeqno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="CostType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Cost" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="AccessorialCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="AccessorialCostGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoCostGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="RateGeoCostSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SpecialServiceGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsCostFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProcessAsFlowThru" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="AdjustmentReasonGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="GeneralLedgerGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="PaymentMethodCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BillableIndicatorGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ShipUnitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsWeighted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentCostRef" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentCostRefType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ShipmentCostDetail" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentCostDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentCostType", propOrder = {
    "intSavedQuery",
    "shipmentCostSeqno",
    "transactionCode",
    "costType",
    "cost",
    "accessorialCodeGid",
    "accessorialCostGid",
    "rateGeoCostGroupGid",
    "rateGeoCostSeq",
    "specialServiceGid",
    "isCostFixed",
    "processAsFlowThru",
    "adjustmentReasonGid",
    "generalLedgerGid",
    "paymentMethodCodeGid",
    "billableIndicatorGid",
    "shipUnitGid",
    "lineNumber",
    "isWeighted",
    "trackingNumber",
    "shipmentCostRef",
    "remark",
    "shipmentCostDetail"
})
public class ShipmentCostType {

    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "ShipmentCostSeqno")
    protected String shipmentCostSeqno;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "CostType", required = true)
    protected String costType;
    @XmlElement(name = "Cost", required = true)
    protected GLogXMLFinancialAmountType cost;
    @XmlElement(name = "AccessorialCodeGid")
    protected GLogXMLGidType accessorialCodeGid;
    @XmlElement(name = "AccessorialCostGid")
    protected GLogXMLGidType accessorialCostGid;
    @XmlElement(name = "RateGeoCostGroupGid")
    protected GLogXMLGidType rateGeoCostGroupGid;
    @XmlElement(name = "RateGeoCostSeq")
    protected String rateGeoCostSeq;
    @XmlElement(name = "SpecialServiceGid")
    protected GLogXMLGidType specialServiceGid;
    @XmlElement(name = "IsCostFixed")
    protected String isCostFixed;
    @XmlElement(name = "ProcessAsFlowThru")
    protected String processAsFlowThru;
    @XmlElement(name = "AdjustmentReasonGid")
    protected GLogXMLGidType adjustmentReasonGid;
    @XmlElement(name = "GeneralLedgerGid")
    protected GLogXMLGidType generalLedgerGid;
    @XmlElement(name = "PaymentMethodCodeGid")
    protected GLogXMLGidType paymentMethodCodeGid;
    @XmlElement(name = "BillableIndicatorGid")
    protected GLogXMLGidType billableIndicatorGid;
    @XmlElement(name = "ShipUnitGid")
    protected GLogXMLGidType shipUnitGid;
    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "IsWeighted")
    protected String isWeighted;
    @XmlElement(name = "TrackingNumber")
    protected String trackingNumber;
    @XmlElement(name = "ShipmentCostRef")
    protected List<ShipmentCostRefType> shipmentCostRef;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "ShipmentCostDetail")
    protected List<ShipmentCostDetailType> shipmentCostDetail;

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the shipmentCostSeqno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipmentCostSeqno() {
        return shipmentCostSeqno;
    }

    /**
     * Sets the value of the shipmentCostSeqno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipmentCostSeqno(String value) {
        this.shipmentCostSeqno = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the costType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCostType() {
        return costType;
    }

    /**
     * Sets the value of the costType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCostType(String value) {
        this.costType = value;
    }

    /**
     * Gets the value of the cost property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCost() {
        return cost;
    }

    /**
     * Sets the value of the cost property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCost(GLogXMLFinancialAmountType value) {
        this.cost = value;
    }

    /**
     * Gets the value of the accessorialCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCodeGid() {
        return accessorialCodeGid;
    }

    /**
     * Sets the value of the accessorialCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCodeGid(GLogXMLGidType value) {
        this.accessorialCodeGid = value;
    }

    /**
     * Gets the value of the accessorialCostGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAccessorialCostGid() {
        return accessorialCostGid;
    }

    /**
     * Sets the value of the accessorialCostGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAccessorialCostGid(GLogXMLGidType value) {
        this.accessorialCostGid = value;
    }

    /**
     * Gets the value of the rateGeoCostGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRateGeoCostGroupGid() {
        return rateGeoCostGroupGid;
    }

    /**
     * Sets the value of the rateGeoCostGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRateGeoCostGroupGid(GLogXMLGidType value) {
        this.rateGeoCostGroupGid = value;
    }

    /**
     * Gets the value of the rateGeoCostSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateGeoCostSeq() {
        return rateGeoCostSeq;
    }

    /**
     * Sets the value of the rateGeoCostSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateGeoCostSeq(String value) {
        this.rateGeoCostSeq = value;
    }

    /**
     * Gets the value of the specialServiceGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSpecialServiceGid() {
        return specialServiceGid;
    }

    /**
     * Sets the value of the specialServiceGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSpecialServiceGid(GLogXMLGidType value) {
        this.specialServiceGid = value;
    }

    /**
     * Gets the value of the isCostFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCostFixed() {
        return isCostFixed;
    }

    /**
     * Sets the value of the isCostFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCostFixed(String value) {
        this.isCostFixed = value;
    }

    /**
     * Gets the value of the processAsFlowThru property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessAsFlowThru() {
        return processAsFlowThru;
    }

    /**
     * Sets the value of the processAsFlowThru property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessAsFlowThru(String value) {
        this.processAsFlowThru = value;
    }

    /**
     * Gets the value of the adjustmentReasonGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdjustmentReasonGid() {
        return adjustmentReasonGid;
    }

    /**
     * Sets the value of the adjustmentReasonGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdjustmentReasonGid(GLogXMLGidType value) {
        this.adjustmentReasonGid = value;
    }

    /**
     * Gets the value of the generalLedgerGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGeneralLedgerGid() {
        return generalLedgerGid;
    }

    /**
     * Sets the value of the generalLedgerGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGeneralLedgerGid(GLogXMLGidType value) {
        this.generalLedgerGid = value;
    }

    /**
     * Gets the value of the paymentMethodCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodCodeGid() {
        return paymentMethodCodeGid;
    }

    /**
     * Sets the value of the paymentMethodCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodCodeGid(GLogXMLGidType value) {
        this.paymentMethodCodeGid = value;
    }

    /**
     * Gets the value of the billableIndicatorGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBillableIndicatorGid() {
        return billableIndicatorGid;
    }

    /**
     * Sets the value of the billableIndicatorGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBillableIndicatorGid(GLogXMLGidType value) {
        this.billableIndicatorGid = value;
    }

    /**
     * Gets the value of the shipUnitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShipUnitGid() {
        return shipUnitGid;
    }

    /**
     * Sets the value of the shipUnitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShipUnitGid(GLogXMLGidType value) {
        this.shipUnitGid = value;
    }

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the isWeighted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsWeighted() {
        return isWeighted;
    }

    /**
     * Sets the value of the isWeighted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsWeighted(String value) {
        this.isWeighted = value;
    }

    /**
     * Gets the value of the trackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * Sets the value of the trackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrackingNumber(String value) {
        this.trackingNumber = value;
    }

    /**
     * Gets the value of the shipmentCostRef property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentCostRef property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentCostRef().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentCostRefType }
     * 
     * 
     */
    public List<ShipmentCostRefType> getShipmentCostRef() {
        if (shipmentCostRef == null) {
            shipmentCostRef = new ArrayList<ShipmentCostRefType>();
        }
        return this.shipmentCostRef;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the shipmentCostDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentCostDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentCostDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentCostDetailType }
     * 
     * 
     */
    public List<ShipmentCostDetailType> getShipmentCostDetail() {
        if (shipmentCostDetail == null) {
            shipmentCostDetail = new ArrayList<ShipmentCostDetailType>();
        }
        return this.shipmentCostDetail;
    }

}

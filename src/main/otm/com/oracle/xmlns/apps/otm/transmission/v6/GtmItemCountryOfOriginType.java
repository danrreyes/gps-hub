
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Deprecated (20A): GtmItemCountryOfOriginType.
 *             This is deprecated and has been replaced by ItemOriginType in 19B. Any existing integrations using 
 *             this element should be updated as this element will be removed in the next major release.
 * 
 * <p>Java class for GtmItemCountryOfOriginType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmItemCountryOfOriginType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SupplierSiteGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CountryOfOriginGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ManufacturingCountryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsDefault" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManufacturingPartyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ManufacturingDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmItemCountryOfOriginType", propOrder = {
    "supplierSiteGid",
    "countryOfOriginGid",
    "manufacturingCountryGid",
    "isDefault",
    "manufacturingPartyNo",
    "manufacturingDesc"
})
public class GtmItemCountryOfOriginType {

    @XmlElement(name = "SupplierSiteGid", required = true)
    protected GLogXMLGidType supplierSiteGid;
    @XmlElement(name = "CountryOfOriginGid")
    protected GLogXMLGidType countryOfOriginGid;
    @XmlElement(name = "ManufacturingCountryGid")
    protected GLogXMLGidType manufacturingCountryGid;
    @XmlElement(name = "IsDefault")
    protected String isDefault;
    @XmlElement(name = "ManufacturingPartyNo")
    protected String manufacturingPartyNo;
    @XmlElement(name = "ManufacturingDesc")
    protected String manufacturingDesc;

    /**
     * Gets the value of the supplierSiteGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getSupplierSiteGid() {
        return supplierSiteGid;
    }

    /**
     * Sets the value of the supplierSiteGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setSupplierSiteGid(GLogXMLGidType value) {
        this.supplierSiteGid = value;
    }

    /**
     * Gets the value of the countryOfOriginGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCountryOfOriginGid() {
        return countryOfOriginGid;
    }

    /**
     * Sets the value of the countryOfOriginGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCountryOfOriginGid(GLogXMLGidType value) {
        this.countryOfOriginGid = value;
    }

    /**
     * Gets the value of the manufacturingCountryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getManufacturingCountryGid() {
        return manufacturingCountryGid;
    }

    /**
     * Sets the value of the manufacturingCountryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setManufacturingCountryGid(GLogXMLGidType value) {
        this.manufacturingCountryGid = value;
    }

    /**
     * Gets the value of the isDefault property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsDefault() {
        return isDefault;
    }

    /**
     * Sets the value of the isDefault property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsDefault(String value) {
        this.isDefault = value;
    }

    /**
     * Gets the value of the manufacturingPartyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturingPartyNo() {
        return manufacturingPartyNo;
    }

    /**
     * Sets the value of the manufacturingPartyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturingPartyNo(String value) {
        this.manufacturingPartyNo = value;
    }

    /**
     * Gets the value of the manufacturingDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManufacturingDesc() {
        return manufacturingDesc;
    }

    /**
     * Sets the value of the manufacturingDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManufacturingDesc(String value) {
        this.manufacturingDesc = value;
    }

}


package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies reference information from the RIQQuery. This includes the source and destination
 *             location details when the location gid is specified in the source and dest
 *             address.
 *          
 * 
 * <p>Java class for RIQQueryInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RIQQueryInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SourceLocRef" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="DestLocRef" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RIQQueryInfoType", propOrder = {
    "sourceLocRef",
    "destLocRef"
})
public class RIQQueryInfoType {

    @XmlElement(name = "SourceLocRef")
    protected RIQQueryInfoType.SourceLocRef sourceLocRef;
    @XmlElement(name = "DestLocRef")
    protected RIQQueryInfoType.DestLocRef destLocRef;

    /**
     * Gets the value of the sourceLocRef property.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryInfoType.SourceLocRef }
     *     
     */
    public RIQQueryInfoType.SourceLocRef getSourceLocRef() {
        return sourceLocRef;
    }

    /**
     * Sets the value of the sourceLocRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryInfoType.SourceLocRef }
     *     
     */
    public void setSourceLocRef(RIQQueryInfoType.SourceLocRef value) {
        this.sourceLocRef = value;
    }

    /**
     * Gets the value of the destLocRef property.
     * 
     * @return
     *     possible object is
     *     {@link RIQQueryInfoType.DestLocRef }
     *     
     */
    public RIQQueryInfoType.DestLocRef getDestLocRef() {
        return destLocRef;
    }

    /**
     * Sets the value of the destLocRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link RIQQueryInfoType.DestLocRef }
     *     
     */
    public void setDestLocRef(RIQQueryInfoType.DestLocRef value) {
        this.destLocRef = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class DestLocRef {

        @XmlElement(name = "Location", required = true)
        protected LocationType location;

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setLocation(LocationType value) {
            this.location = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Location" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationType"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class SourceLocRef {

        @XmlElement(name = "Location", required = true)
        protected LocationType location;

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link LocationType }
         *     
         */
        public LocationType getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link LocationType }
         *     
         */
        public void setLocation(LocationType value) {
            this.location = value;
        }

    }

}

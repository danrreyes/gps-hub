
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Used to define the declared value for the freight.
 *          
 * 
 * <p>Java class for DeclaredValueType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeclaredValueType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="FinancialAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FinancialAmountType"/&gt;
 *         &lt;element name="DeclaredValueQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeclaredValueType", propOrder = {
    "financialAmount",
    "declaredValueQualifierGid"
})
public class DeclaredValueType {

    @XmlElement(name = "FinancialAmount", required = true)
    protected FinancialAmountType financialAmount;
    @XmlElement(name = "DeclaredValueQualifierGid", required = true)
    protected GLogXMLGidType declaredValueQualifierGid;

    /**
     * Gets the value of the financialAmount property.
     * 
     * @return
     *     possible object is
     *     {@link FinancialAmountType }
     *     
     */
    public FinancialAmountType getFinancialAmount() {
        return financialAmount;
    }

    /**
     * Sets the value of the financialAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link FinancialAmountType }
     *     
     */
    public void setFinancialAmount(FinancialAmountType value) {
        this.financialAmount = value;
    }

    /**
     * Gets the value of the declaredValueQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDeclaredValueQualifierGid() {
        return declaredValueQualifierGid;
    }

    /**
     * Sets the value of the declaredValueQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDeclaredValueQualifierGid(GLogXMLGidType value) {
        this.declaredValueQualifierGid = value;
    }

}

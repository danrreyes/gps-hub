
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * StopRefnum is a qualifier/value pair used to refer to a particular stop.
 * 
 * <p>Java class for StopRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StopRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StopRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="StopRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StopRefnumType", propOrder = {
    "stopRefnumQualifierGid",
    "stopRefnumValue"
})
public class StopRefnumType {

    @XmlElement(name = "StopRefnumQualifierGid", required = true)
    protected GLogXMLGidType stopRefnumQualifierGid;
    @XmlElement(name = "StopRefnumValue", required = true)
    protected String stopRefnumValue;

    /**
     * Gets the value of the stopRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getStopRefnumQualifierGid() {
        return stopRefnumQualifierGid;
    }

    /**
     * Sets the value of the stopRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setStopRefnumQualifierGid(GLogXMLGidType value) {
        this.stopRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the stopRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopRefnumValue() {
        return stopRefnumValue;
    }

    /**
     * Sets the value of the stopRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopRefnumValue(String value) {
        this.stopRefnumValue = value;
    }

}

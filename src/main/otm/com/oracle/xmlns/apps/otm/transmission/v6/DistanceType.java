
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Distance is a structure for describing distance.
 *             It includes unit of measure and value.
 *          
 * 
 * <p>Java class for DistanceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DistanceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DistanceValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="DistanceUOMGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DistanceType", propOrder = {
    "distanceValue",
    "distanceUOMGid"
})
public class DistanceType {

    @XmlElement(name = "DistanceValue", required = true)
    protected String distanceValue;
    @XmlElement(name = "DistanceUOMGid", required = true)
    protected GLogXMLGidType distanceUOMGid;

    /**
     * Gets the value of the distanceValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistanceValue() {
        return distanceValue;
    }

    /**
     * Sets the value of the distanceValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistanceValue(String value) {
        this.distanceValue = value;
    }

    /**
     * Gets the value of the distanceUOMGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDistanceUOMGid() {
        return distanceUOMGid;
    }

    /**
     * Sets the value of the distanceUOMGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDistanceUOMGid(GLogXMLGidType value) {
        this.distanceUOMGid = value;
    }

}

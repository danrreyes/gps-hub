
package com.oracle.xmlns.apps.otm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * A SkuTransactionDescriptor provides additional detailed information for a particular
 *             SkuTransaction.
 *          
 * 
 * <p>Java class for SkuTransactionDescriptorType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SkuTransactionDescriptorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SkuDescriptorSeq" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SkuDescriptorType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SkuDescriptorValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Quantity1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Quantity2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ParentSkuDescriptorSeq" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="UserDefinedXml" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkuTransactionDescriptorType", propOrder = {
    "skuDescriptorSeq",
    "skuDescriptorType",
    "skuDescriptorValue",
    "quantity1",
    "quantity2",
    "parentSkuDescriptorSeq",
    "userDefinedXml",
    "remark"
})
public class SkuTransactionDescriptorType {

    @XmlElement(name = "SkuDescriptorSeq", required = true)
    protected String skuDescriptorSeq;
    @XmlElement(name = "SkuDescriptorType")
    protected String skuDescriptorType;
    @XmlElement(name = "SkuDescriptorValue")
    protected String skuDescriptorValue;
    @XmlElement(name = "Quantity1")
    protected String quantity1;
    @XmlElement(name = "Quantity2")
    protected String quantity2;
    @XmlElement(name = "ParentSkuDescriptorSeq")
    protected String parentSkuDescriptorSeq;
    @XmlElement(name = "UserDefinedXml")
    protected String userDefinedXml;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;

    /**
     * Gets the value of the skuDescriptorSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorSeq() {
        return skuDescriptorSeq;
    }

    /**
     * Sets the value of the skuDescriptorSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorSeq(String value) {
        this.skuDescriptorSeq = value;
    }

    /**
     * Gets the value of the skuDescriptorType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorType() {
        return skuDescriptorType;
    }

    /**
     * Sets the value of the skuDescriptorType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorType(String value) {
        this.skuDescriptorType = value;
    }

    /**
     * Gets the value of the skuDescriptorValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkuDescriptorValue() {
        return skuDescriptorValue;
    }

    /**
     * Sets the value of the skuDescriptorValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkuDescriptorValue(String value) {
        this.skuDescriptorValue = value;
    }

    /**
     * Gets the value of the quantity1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity1() {
        return quantity1;
    }

    /**
     * Sets the value of the quantity1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity1(String value) {
        this.quantity1 = value;
    }

    /**
     * Gets the value of the quantity2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity2() {
        return quantity2;
    }

    /**
     * Sets the value of the quantity2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity2(String value) {
        this.quantity2 = value;
    }

    /**
     * Gets the value of the parentSkuDescriptorSeq property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentSkuDescriptorSeq() {
        return parentSkuDescriptorSeq;
    }

    /**
     * Sets the value of the parentSkuDescriptorSeq property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentSkuDescriptorSeq(String value) {
        this.parentSkuDescriptorSeq = value;
    }

    /**
     * Gets the value of the userDefinedXml property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserDefinedXml() {
        return userDefinedXml;
    }

    /**
     * Sets the value of the userDefinedXml property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserDefinedXml(String value) {
        this.userDefinedXml = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

}

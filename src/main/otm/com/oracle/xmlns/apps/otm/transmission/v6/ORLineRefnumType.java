
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             ORLineRefnum is an alternate method for identifying a Release Line. It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for ORLineRefnumType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ORLineRefnumType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ORLineRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ORLineRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ORLineRefnumType", propOrder = {
    "orLineRefnumQualifierGid",
    "orLineRefnumValue"
})
public class ORLineRefnumType {

    @XmlElement(name = "ORLineRefnumQualifierGid", required = true)
    protected GLogXMLGidType orLineRefnumQualifierGid;
    @XmlElement(name = "ORLineRefnumValue", required = true)
    protected String orLineRefnumValue;

    /**
     * Gets the value of the orLineRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getORLineRefnumQualifierGid() {
        return orLineRefnumQualifierGid;
    }

    /**
     * Sets the value of the orLineRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setORLineRefnumQualifierGid(GLogXMLGidType value) {
        this.orLineRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the orLineRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORLineRefnumValue() {
        return orLineRefnumValue;
    }

    /**
     * Sets the value of the orLineRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORLineRefnumValue(String value) {
        this.orLineRefnumValue = value;
    }

}

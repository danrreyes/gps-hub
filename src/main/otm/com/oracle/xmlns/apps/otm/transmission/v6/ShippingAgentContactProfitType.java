
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Specifies the profit split for the agent role.
 * 
 * <p>Java class for ShippingAgentContactProfitType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShippingAgentContactProfitType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ShippingAgentContactProfitGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ModeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="JobType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ShipperLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="ConsigneeLocationProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *         &lt;element name="ProfitSplitPct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ProfitSplitAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="ConsolidationTypeProfileGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLLocProfileType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShippingAgentContactProfitType", propOrder = {
    "shippingAgentContactProfitGid",
    "modeProfileGid",
    "jobType",
    "shipperLocationProfileGid",
    "consigneeLocationProfileGid",
    "profitSplitPct",
    "profitSplitAmount",
    "consolidationTypeProfileGid"
})
public class ShippingAgentContactProfitType {

    @XmlElement(name = "ShippingAgentContactProfitGid", required = true)
    protected GLogXMLGidType shippingAgentContactProfitGid;
    @XmlElement(name = "ModeProfileGid")
    protected GLogXMLGidType modeProfileGid;
    @XmlElement(name = "JobType")
    protected String jobType;
    @XmlElement(name = "ShipperLocationProfileGid")
    protected GLogXMLLocProfileType shipperLocationProfileGid;
    @XmlElement(name = "ConsigneeLocationProfileGid")
    protected GLogXMLLocProfileType consigneeLocationProfileGid;
    @XmlElement(name = "ProfitSplitPct")
    protected String profitSplitPct;
    @XmlElement(name = "ProfitSplitAmount")
    protected GLogXMLFinancialAmountType profitSplitAmount;
    @XmlElement(name = "ConsolidationTypeProfileGid")
    protected GLogXMLLocProfileType consolidationTypeProfileGid;

    /**
     * Gets the value of the shippingAgentContactProfitGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getShippingAgentContactProfitGid() {
        return shippingAgentContactProfitGid;
    }

    /**
     * Sets the value of the shippingAgentContactProfitGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setShippingAgentContactProfitGid(GLogXMLGidType value) {
        this.shippingAgentContactProfitGid = value;
    }

    /**
     * Gets the value of the modeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getModeProfileGid() {
        return modeProfileGid;
    }

    /**
     * Sets the value of the modeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setModeProfileGid(GLogXMLGidType value) {
        this.modeProfileGid = value;
    }

    /**
     * Gets the value of the jobType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobType() {
        return jobType;
    }

    /**
     * Sets the value of the jobType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobType(String value) {
        this.jobType = value;
    }

    /**
     * Gets the value of the shipperLocationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getShipperLocationProfileGid() {
        return shipperLocationProfileGid;
    }

    /**
     * Sets the value of the shipperLocationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setShipperLocationProfileGid(GLogXMLLocProfileType value) {
        this.shipperLocationProfileGid = value;
    }

    /**
     * Gets the value of the consigneeLocationProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getConsigneeLocationProfileGid() {
        return consigneeLocationProfileGid;
    }

    /**
     * Sets the value of the consigneeLocationProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setConsigneeLocationProfileGid(GLogXMLLocProfileType value) {
        this.consigneeLocationProfileGid = value;
    }

    /**
     * Gets the value of the profitSplitPct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfitSplitPct() {
        return profitSplitPct;
    }

    /**
     * Sets the value of the profitSplitPct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfitSplitPct(String value) {
        this.profitSplitPct = value;
    }

    /**
     * Gets the value of the profitSplitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getProfitSplitAmount() {
        return profitSplitAmount;
    }

    /**
     * Sets the value of the profitSplitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setProfitSplitAmount(GLogXMLFinancialAmountType value) {
        this.profitSplitAmount = value;
    }

    /**
     * Gets the value of the consolidationTypeProfileGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public GLogXMLLocProfileType getConsolidationTypeProfileGid() {
        return consolidationTypeProfileGid;
    }

    /**
     * Sets the value of the consolidationTypeProfileGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLLocProfileType }
     *     
     */
    public void setConsolidationTypeProfileGid(GLogXMLLocProfileType value) {
        this.consolidationTypeProfileGid = value;
    }

}

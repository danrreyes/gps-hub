
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Used to specify additional/other charges for the commercial invoice.
 * 
 * <p>Java class for CommercialInvoiceChargeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CommercialInvoiceChargeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ChargeAmount" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="CommercialInvChargeCodeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ChargeActivity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CommercialInvoiceChargeType", propOrder = {
    "sequenceNumber",
    "chargeAmount",
    "commercialInvChargeCodeGid",
    "chargeActivity"
})
public class CommercialInvoiceChargeType {

    @XmlElement(name = "SequenceNumber")
    protected String sequenceNumber;
    @XmlElement(name = "ChargeAmount", required = true)
    protected GLogXMLFinancialAmountType chargeAmount;
    @XmlElement(name = "CommercialInvChargeCodeGid", required = true)
    protected GLogXMLGidType commercialInvChargeCodeGid;
    @XmlElement(name = "ChargeActivity", required = true)
    protected String chargeActivity;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the chargeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getChargeAmount() {
        return chargeAmount;
    }

    /**
     * Sets the value of the chargeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setChargeAmount(GLogXMLFinancialAmountType value) {
        this.chargeAmount = value;
    }

    /**
     * Gets the value of the commercialInvChargeCodeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCommercialInvChargeCodeGid() {
        return commercialInvChargeCodeGid;
    }

    /**
     * Sets the value of the commercialInvChargeCodeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCommercialInvChargeCodeGid(GLogXMLGidType value) {
        this.commercialInvChargeCodeGid = value;
    }

    /**
     * Gets the value of the chargeActivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChargeActivity() {
        return chargeActivity;
    }

    /**
     * Sets the value of the chargeActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChargeActivity(String value) {
        this.chargeActivity = value;
    }

}

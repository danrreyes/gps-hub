
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProcessGroupingType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProcessGroupingType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProcessGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ProcessGroupOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InSequence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="StopProcessOnError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessGroupingType", propOrder = {
    "processGroup",
    "processGroupOwner",
    "inSequence",
    "stopProcessOnError"
})
public class ProcessGroupingType {

    @XmlElement(name = "ProcessGroup", required = true)
    protected String processGroup;
    @XmlElement(name = "ProcessGroupOwner")
    protected String processGroupOwner;
    @XmlElement(name = "InSequence", required = true)
    protected String inSequence;
    @XmlElement(name = "StopProcessOnError")
    protected String stopProcessOnError;

    /**
     * Gets the value of the processGroup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessGroup() {
        return processGroup;
    }

    /**
     * Sets the value of the processGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessGroup(String value) {
        this.processGroup = value;
    }

    /**
     * Gets the value of the processGroupOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProcessGroupOwner() {
        return processGroupOwner;
    }

    /**
     * Sets the value of the processGroupOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProcessGroupOwner(String value) {
        this.processGroupOwner = value;
    }

    /**
     * Gets the value of the inSequence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInSequence() {
        return inSequence;
    }

    /**
     * Sets the value of the inSequence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInSequence(String value) {
        this.inSequence = value;
    }

    /**
     * Gets the value of the stopProcessOnError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStopProcessOnError() {
        return stopProcessOnError;
    }

    /**
     * Sets the value of the stopProcessOnError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStopProcessOnError(String value) {
        this.stopProcessOnError = value;
    }

}

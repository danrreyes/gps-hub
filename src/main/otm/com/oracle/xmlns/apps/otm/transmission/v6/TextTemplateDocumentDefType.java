
package com.oracle.xmlns.apps.otm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * Represents the use of a text template for a particular document type.
 * 
 * <p>Java class for TextTemplateDocumentDefType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TextTemplateDocumentDefType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DocumentDefinitionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TextTemplateIsMandatory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TextTemplateDocumentDefType", propOrder = {
    "documentDefinitionGid",
    "textTemplateIsMandatory"
})
public class TextTemplateDocumentDefType {

    @XmlElement(name = "DocumentDefinitionGid", required = true)
    protected GLogXMLGidType documentDefinitionGid;
    @XmlElement(name = "TextTemplateIsMandatory")
    protected String textTemplateIsMandatory;

    /**
     * Gets the value of the documentDefinitionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentDefinitionGid() {
        return documentDefinitionGid;
    }

    /**
     * Sets the value of the documentDefinitionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentDefinitionGid(GLogXMLGidType value) {
        this.documentDefinitionGid = value;
    }

    /**
     * Gets the value of the textTemplateIsMandatory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextTemplateIsMandatory() {
        return textTemplateIsMandatory;
    }

    /**
     * Sets the value of the textTemplateIsMandatory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextTemplateIsMandatory(String value) {
        this.textTemplateIsMandatory = value;
    }

}

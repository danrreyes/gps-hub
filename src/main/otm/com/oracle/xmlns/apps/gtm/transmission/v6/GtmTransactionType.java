
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.FlexFieldDateType;
import com.oracle.xmlns.apps.otm.transmission.v6.FlexFieldNumberType;
import com.oracle.xmlns.apps.otm.transmission.v6.FlexFieldStringType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import com.oracle.xmlns.apps.otm.transmission.v6.IntSavedQueryType;
import com.oracle.xmlns.apps.otm.transmission.v6.OTMTransactionInOut;
import com.oracle.xmlns.apps.otm.transmission.v6.RefnumType;
import com.oracle.xmlns.apps.otm.transmission.v6.ReleaseType;
import com.oracle.xmlns.apps.otm.transmission.v6.RemarkType;
import com.oracle.xmlns.apps.otm.transmission.v6.ReplaceChildrenType;
import com.oracle.xmlns.apps.otm.transmission.v6.ShipmentType;
import com.oracle.xmlns.apps.otm.transmission.v6.StatusType;
import com.oracle.xmlns.apps.otm.transmission.v6.TransactionCodeType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Trade Transaction and it's details. Trade Transaction includes Order Release or Shipment.
 *          
 * 
 * <p>Java class for GtmTransactionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmTransactionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmTransactionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="GtmTransactionLine" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmTransactionLineType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="DataQueryTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;choice minOccurs="0"&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="Release" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReleaseType" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *           &lt;sequence&gt;
 *             &lt;element name="Shipment" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ShipmentType" minOccurs="0"/&gt;
 *           &lt;/sequence&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IncoTermLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsHazardousCargo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GtmTransactionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GtmTransactionTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IsShipmentCreated" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionInvLocation" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransactionInvLocationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransDate" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransDateType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UserDefinedClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedClassificationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CarrierCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TransactionQuantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}QuantityType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCurrency" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CurrencyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionPolicy" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransactionPolicyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionRequiredDocument" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransactionRequiredDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PortInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}PortInfoType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="BorderTransportModeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="BorderConveyanceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BorderConveyanceFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffPreferenceType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TariffPreferenceTypeType" minOccurs="0"/&gt;
 *         &lt;element name="Conveyance" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ConveyanceType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmTransactionType", propOrder = {
    "gtmTransactionGid",
    "gtmTransactionLine",
    "intSavedQuery",
    "transactionCode",
    "replaceChildren",
    "dataQueryTypeGid",
    "objectGid",
    "release",
    "shipment",
    "incoTermGid",
    "incoTermLocation",
    "isHazardousCargo",
    "gtmTransactionType",
    "gtmTransactionTypeGid",
    "isShipmentCreated",
    "remark",
    "involvedParty",
    "transactionInvLocation",
    "refnum",
    "status",
    "transDate",
    "userDefinedClassification",
    "carrierCode",
    "transactionQuantity",
    "transactionCurrency",
    "transactionPolicy",
    "transactionRequiredDocument",
    "portInfo",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "borderTransportModeGid",
    "borderConveyanceName",
    "borderConveyanceFlag",
    "tariffPreferenceType",
    "conveyance"
})
public class GtmTransactionType
    extends OTMTransactionInOut
{

    @XmlElement(name = "GtmTransactionGid")
    protected GLogXMLGidType gtmTransactionGid;
    @XmlElement(name = "GtmTransactionLine")
    protected List<GtmTransactionLineType> gtmTransactionLine;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "DataQueryTypeGid")
    protected GLogXMLGidType dataQueryTypeGid;
    @XmlElement(name = "ObjectGid")
    protected GLogXMLGidType objectGid;
    @XmlElement(name = "Release")
    protected ReleaseType release;
    @XmlElement(name = "Shipment")
    protected ShipmentType shipment;
    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "IncoTermLocation")
    protected String incoTermLocation;
    @XmlElement(name = "IsHazardousCargo")
    protected String isHazardousCargo;
    @XmlElement(name = "GtmTransactionType")
    protected String gtmTransactionType;
    @XmlElement(name = "GtmTransactionTypeGid")
    protected GLogXMLGidType gtmTransactionTypeGid;
    @XmlElement(name = "IsShipmentCreated")
    protected String isShipmentCreated;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<GtmInvolvedPartyType> involvedParty;
    @XmlElement(name = "TransactionInvLocation")
    protected List<TransactionInvLocationType> transactionInvLocation;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "TransDate")
    protected List<TransDateType> transDate;
    @XmlElement(name = "UserDefinedClassification")
    protected List<UserDefinedClassificationType> userDefinedClassification;
    @XmlElement(name = "CarrierCode")
    protected String carrierCode;
    @XmlElement(name = "TransactionQuantity")
    protected List<QuantityType> transactionQuantity;
    @XmlElement(name = "TransactionCurrency")
    protected List<CurrencyType> transactionCurrency;
    @XmlElement(name = "TransactionPolicy")
    protected List<TransactionPolicyType> transactionPolicy;
    @XmlElement(name = "TransactionRequiredDocument")
    protected List<TransactionRequiredDocumentType> transactionRequiredDocument;
    @XmlElement(name = "PortInfo")
    protected List<PortInfoType> portInfo;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "BorderTransportModeGid")
    protected GLogXMLGidType borderTransportModeGid;
    @XmlElement(name = "BorderConveyanceName")
    protected String borderConveyanceName;
    @XmlElement(name = "BorderConveyanceFlag")
    protected String borderConveyanceFlag;
    @XmlElement(name = "TariffPreferenceType")
    protected TariffPreferenceTypeType tariffPreferenceType;
    @XmlElement(name = "Conveyance")
    protected List<ConveyanceType> conveyance;

    /**
     * Gets the value of the gtmTransactionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmTransactionGid() {
        return gtmTransactionGid;
    }

    /**
     * Sets the value of the gtmTransactionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmTransactionGid(GLogXMLGidType value) {
        this.gtmTransactionGid = value;
    }

    /**
     * Gets the value of the gtmTransactionLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransactionLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransactionLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmTransactionLineType }
     * 
     * 
     */
    public List<GtmTransactionLineType> getGtmTransactionLine() {
        if (gtmTransactionLine == null) {
            gtmTransactionLine = new ArrayList<GtmTransactionLineType>();
        }
        return this.gtmTransactionLine;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the dataQueryTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDataQueryTypeGid() {
        return dataQueryTypeGid;
    }

    /**
     * Sets the value of the dataQueryTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDataQueryTypeGid(GLogXMLGidType value) {
        this.dataQueryTypeGid = value;
    }

    /**
     * Gets the value of the objectGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getObjectGid() {
        return objectGid;
    }

    /**
     * Sets the value of the objectGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setObjectGid(GLogXMLGidType value) {
        this.objectGid = value;
    }

    /**
     * Gets the value of the release property.
     * 
     * @return
     *     possible object is
     *     {@link ReleaseType }
     *     
     */
    public ReleaseType getRelease() {
        return release;
    }

    /**
     * Sets the value of the release property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReleaseType }
     *     
     */
    public void setRelease(ReleaseType value) {
        this.release = value;
    }

    /**
     * Gets the value of the shipment property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentType }
     *     
     */
    public ShipmentType getShipment() {
        return shipment;
    }

    /**
     * Sets the value of the shipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentType }
     *     
     */
    public void setShipment(ShipmentType value) {
        this.shipment = value;
    }

    /**
     * Gets the value of the incoTermGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Sets the value of the incoTermGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Gets the value of the incoTermLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncoTermLocation() {
        return incoTermLocation;
    }

    /**
     * Sets the value of the incoTermLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncoTermLocation(String value) {
        this.incoTermLocation = value;
    }

    /**
     * Gets the value of the isHazardousCargo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsHazardousCargo() {
        return isHazardousCargo;
    }

    /**
     * Sets the value of the isHazardousCargo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsHazardousCargo(String value) {
        this.isHazardousCargo = value;
    }

    /**
     * Gets the value of the gtmTransactionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmTransactionType() {
        return gtmTransactionType;
    }

    /**
     * Sets the value of the gtmTransactionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmTransactionType(String value) {
        this.gtmTransactionType = value;
    }

    /**
     * Gets the value of the gtmTransactionTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmTransactionTypeGid() {
        return gtmTransactionTypeGid;
    }

    /**
     * Sets the value of the gtmTransactionTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmTransactionTypeGid(GLogXMLGidType value) {
        this.gtmTransactionTypeGid = value;
    }

    /**
     * Gets the value of the isShipmentCreated property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsShipmentCreated() {
        return isShipmentCreated;
    }

    /**
     * Sets the value of the isShipmentCreated property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsShipmentCreated(String value) {
        this.isShipmentCreated = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmInvolvedPartyType }
     * 
     * 
     */
    public List<GtmInvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<GtmInvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the transactionInvLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionInvLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionInvLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionInvLocationType }
     * 
     * 
     */
    public List<TransactionInvLocationType> getTransactionInvLocation() {
        if (transactionInvLocation == null) {
            transactionInvLocation = new ArrayList<TransactionInvLocationType>();
        }
        return this.transactionInvLocation;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the transDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransDateType }
     * 
     * 
     */
    public List<TransDateType> getTransDate() {
        if (transDate == null) {
            transDate = new ArrayList<TransDateType>();
        }
        return this.transDate;
    }

    /**
     * Gets the value of the userDefinedClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the userDefinedClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserDefinedClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDefinedClassificationType }
     * 
     * 
     */
    public List<UserDefinedClassificationType> getUserDefinedClassification() {
        if (userDefinedClassification == null) {
            userDefinedClassification = new ArrayList<UserDefinedClassificationType>();
        }
        return this.userDefinedClassification;
    }

    /**
     * Gets the value of the carrierCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierCode() {
        return carrierCode;
    }

    /**
     * Sets the value of the carrierCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierCode(String value) {
        this.carrierCode = value;
    }

    /**
     * Gets the value of the transactionQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuantityType }
     * 
     * 
     */
    public List<QuantityType> getTransactionQuantity() {
        if (transactionQuantity == null) {
            transactionQuantity = new ArrayList<QuantityType>();
        }
        return this.transactionQuantity;
    }

    /**
     * Gets the value of the transactionCurrency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionCurrency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionCurrency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CurrencyType }
     * 
     * 
     */
    public List<CurrencyType> getTransactionCurrency() {
        if (transactionCurrency == null) {
            transactionCurrency = new ArrayList<CurrencyType>();
        }
        return this.transactionCurrency;
    }

    /**
     * Gets the value of the transactionPolicy property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionPolicy property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionPolicy().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionPolicyType }
     * 
     * 
     */
    public List<TransactionPolicyType> getTransactionPolicy() {
        if (transactionPolicy == null) {
            transactionPolicy = new ArrayList<TransactionPolicyType>();
        }
        return this.transactionPolicy;
    }

    /**
     * Gets the value of the transactionRequiredDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionRequiredDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionRequiredDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionRequiredDocumentType }
     * 
     * 
     */
    public List<TransactionRequiredDocumentType> getTransactionRequiredDocument() {
        if (transactionRequiredDocument == null) {
            transactionRequiredDocument = new ArrayList<TransactionRequiredDocumentType>();
        }
        return this.transactionRequiredDocument;
    }

    /**
     * Gets the value of the portInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the portInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPortInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortInfoType }
     * 
     * 
     */
    public List<PortInfoType> getPortInfo() {
        if (portInfo == null) {
            portInfo = new ArrayList<PortInfoType>();
        }
        return this.portInfo;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the borderTransportModeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBorderTransportModeGid() {
        return borderTransportModeGid;
    }

    /**
     * Sets the value of the borderTransportModeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBorderTransportModeGid(GLogXMLGidType value) {
        this.borderTransportModeGid = value;
    }

    /**
     * Gets the value of the borderConveyanceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBorderConveyanceName() {
        return borderConveyanceName;
    }

    /**
     * Sets the value of the borderConveyanceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBorderConveyanceName(String value) {
        this.borderConveyanceName = value;
    }

    /**
     * Gets the value of the borderConveyanceFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBorderConveyanceFlag() {
        return borderConveyanceFlag;
    }

    /**
     * Sets the value of the borderConveyanceFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBorderConveyanceFlag(String value) {
        this.borderConveyanceFlag = value;
    }

    /**
     * Gets the value of the tariffPreferenceType property.
     * 
     * @return
     *     possible object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public TariffPreferenceTypeType getTariffPreferenceType() {
        return tariffPreferenceType;
    }

    /**
     * Sets the value of the tariffPreferenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public void setTariffPreferenceType(TariffPreferenceTypeType value) {
        this.tariffPreferenceType = value;
    }

    /**
     * Gets the value of the conveyance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the conveyance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConveyance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConveyanceType }
     * 
     * 
     */
    public List<ConveyanceType> getConveyance() {
        if (conveyance == null) {
            conveyance = new ArrayList<ConveyanceType>();
        }
        return this.conveyance;
    }

}

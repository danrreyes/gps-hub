
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Inbound) Remarks for the party involved.
 *          
 * 
 * <p>Java class for InvolvedPartyRemarkDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyRemarkDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PartyQualifierGidForRemark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PartyRemarkQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PartyRemarkText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyRemarkDetailType", propOrder = {
    "partyQualifierGidForRemark",
    "partyRemarkQualifierGid",
    "partyRemarkText"
})
public class InvolvedPartyRemarkDetailType {

    @XmlElement(name = "PartyQualifierGidForRemark", required = true)
    protected GLogXMLGidType partyQualifierGidForRemark;
    @XmlElement(name = "PartyRemarkQualifierGid", required = true)
    protected GLogXMLGidType partyRemarkQualifierGid;
    @XmlElement(name = "PartyRemarkText", required = true)
    protected String partyRemarkText;

    /**
     * Gets the value of the partyQualifierGidForRemark property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyQualifierGidForRemark() {
        return partyQualifierGidForRemark;
    }

    /**
     * Sets the value of the partyQualifierGidForRemark property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyQualifierGidForRemark(GLogXMLGidType value) {
        this.partyQualifierGidForRemark = value;
    }

    /**
     * Gets the value of the partyRemarkQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyRemarkQualifierGid() {
        return partyRemarkQualifierGid;
    }

    /**
     * Sets the value of the partyRemarkQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyRemarkQualifierGid(GLogXMLGidType value) {
        this.partyRemarkQualifierGid = value;
    }

    /**
     * Gets the value of the partyRemarkText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyRemarkText() {
        return partyRemarkText;
    }

    /**
     * Sets the value of the partyRemarkText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyRemarkText(String value) {
        this.partyRemarkText = value;
    }

}

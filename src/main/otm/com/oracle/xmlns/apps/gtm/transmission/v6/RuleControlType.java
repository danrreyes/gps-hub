
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.TextType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) RuleControl represents the controls applicable for the input data based on compliance screening  
 *          
 * 
 * <p>Java class for RuleControlType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RuleControlType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RuleId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ControlType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ControlCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Precedence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="DocumentDefGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegimeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedPartyQualGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ScreeningStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExceptionControlTypeGid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExceptionControlCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PassFail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Text" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RuleControlType", propOrder = {
    "ruleId",
    "controlType",
    "controlCode",
    "controlCategory",
    "precedence",
    "documentDefGid",
    "regimeId",
    "involvedPartyQualGid",
    "screeningStatus",
    "exceptionControlTypeGid",
    "exceptionControlCode",
    "passFail",
    "text"
})
public class RuleControlType {

    @XmlElement(name = "RuleId", required = true)
    protected String ruleId;
    @XmlElement(name = "ControlType")
    protected String controlType;
    @XmlElement(name = "ControlCode")
    protected String controlCode;
    @XmlElement(name = "ControlCategory")
    protected String controlCategory;
    @XmlElement(name = "Precedence")
    protected String precedence;
    @XmlElement(name = "DocumentDefGid")
    protected String documentDefGid;
    @XmlElement(name = "RegimeId")
    protected String regimeId;
    @XmlElement(name = "InvolvedPartyQualGid")
    protected String involvedPartyQualGid;
    @XmlElement(name = "ScreeningStatus")
    protected String screeningStatus;
    @XmlElement(name = "ExceptionControlTypeGid")
    protected String exceptionControlTypeGid;
    @XmlElement(name = "ExceptionControlCode")
    protected String exceptionControlCode;
    @XmlElement(name = "PassFail")
    protected String passFail;
    @XmlElement(name = "Text")
    protected List<TextType> text;

    /**
     * Gets the value of the ruleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleId() {
        return ruleId;
    }

    /**
     * Sets the value of the ruleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleId(String value) {
        this.ruleId = value;
    }

    /**
     * Gets the value of the controlType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlType() {
        return controlType;
    }

    /**
     * Sets the value of the controlType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlType(String value) {
        this.controlType = value;
    }

    /**
     * Gets the value of the controlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Sets the value of the controlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Gets the value of the controlCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCategory() {
        return controlCategory;
    }

    /**
     * Sets the value of the controlCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCategory(String value) {
        this.controlCategory = value;
    }

    /**
     * Gets the value of the precedence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrecedence() {
        return precedence;
    }

    /**
     * Sets the value of the precedence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrecedence(String value) {
        this.precedence = value;
    }

    /**
     * Gets the value of the documentDefGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDefGid() {
        return documentDefGid;
    }

    /**
     * Sets the value of the documentDefGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDefGid(String value) {
        this.documentDefGid = value;
    }

    /**
     * Gets the value of the regimeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeId() {
        return regimeId;
    }

    /**
     * Sets the value of the regimeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeId(String value) {
        this.regimeId = value;
    }

    /**
     * Gets the value of the involvedPartyQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvolvedPartyQualGid() {
        return involvedPartyQualGid;
    }

    /**
     * Sets the value of the involvedPartyQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvolvedPartyQualGid(String value) {
        this.involvedPartyQualGid = value;
    }

    /**
     * Gets the value of the screeningStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScreeningStatus() {
        return screeningStatus;
    }

    /**
     * Sets the value of the screeningStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScreeningStatus(String value) {
        this.screeningStatus = value;
    }

    /**
     * Gets the value of the exceptionControlTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionControlTypeGid() {
        return exceptionControlTypeGid;
    }

    /**
     * Sets the value of the exceptionControlTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionControlTypeGid(String value) {
        this.exceptionControlTypeGid = value;
    }

    /**
     * Gets the value of the exceptionControlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceptionControlCode() {
        return exceptionControlCode;
    }

    /**
     * Sets the value of the exceptionControlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceptionControlCode(String value) {
        this.exceptionControlCode = value;
    }

    /**
     * Gets the value of the passFail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassFail() {
        return passFail;
    }

    /**
     * Sets the value of the passFail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassFail(String value) {
        this.passFail = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the text property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextType }
     * 
     * 
     */
    public List<TextType> getText() {
        if (text == null) {
            text = new ArrayList<TextType>();
        }
        return this.text;
    }

}

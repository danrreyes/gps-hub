
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             (Both) General User Defined Code pertaining to business objects, including but not limited to: end use code,
 *             end user code, product type code, transaction code, etc.
 *          
 * 
 * <p>Java class for UserDefinedClassificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserDefinedClassificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UserDefinedCategoryGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="UserDefinedType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedTypeType"/&gt;
 *         &lt;element name="UserDefinedCode" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedCodeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserDefinedClassificationType", propOrder = {
    "userDefinedCategoryGid",
    "userDefinedType",
    "userDefinedCode"
})
public class UserDefinedClassificationType {

    @XmlElement(name = "UserDefinedCategoryGid")
    protected GLogXMLGidType userDefinedCategoryGid;
    @XmlElement(name = "UserDefinedType", required = true)
    protected UserDefinedTypeType userDefinedType;
    @XmlElement(name = "UserDefinedCode", required = true)
    protected UserDefinedCodeType userDefinedCode;

    /**
     * Gets the value of the userDefinedCategoryGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getUserDefinedCategoryGid() {
        return userDefinedCategoryGid;
    }

    /**
     * Sets the value of the userDefinedCategoryGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setUserDefinedCategoryGid(GLogXMLGidType value) {
        this.userDefinedCategoryGid = value;
    }

    /**
     * Gets the value of the userDefinedType property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefinedTypeType }
     *     
     */
    public UserDefinedTypeType getUserDefinedType() {
        return userDefinedType;
    }

    /**
     * Sets the value of the userDefinedType property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefinedTypeType }
     *     
     */
    public void setUserDefinedType(UserDefinedTypeType value) {
        this.userDefinedType = value;
    }

    /**
     * Gets the value of the userDefinedCode property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefinedCodeType }
     *     
     */
    public UserDefinedCodeType getUserDefinedCode() {
        return userDefinedCode;
    }

    /**
     * Sets the value of the userDefinedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefinedCodeType }
     *     
     */
    public void setUserDefinedCode(UserDefinedCodeType value) {
        this.userDefinedCode = value;
    }

}

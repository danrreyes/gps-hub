
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogDateTimeType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) Product classification code.
 * 
 *             Deprecated (6.4.3): TradeDirection. Functionality removed. Value will be ignored and field will be removed in a future release.
 *          
 * 
 * <p>Java class for ProductClassificationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProductClassificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ClassificationCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ClassificationCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TradeDirection" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}AttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Note" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProdClassCodeNoteType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UOM" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProdClassCodeUOMType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ClassificationDescription" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProdClassCodeDescType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="BindingRulingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BindingRulingEffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="BindingRulingExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductClassificationType", propOrder = {
    "classificationCategory",
    "gtmProdClassTypeGid",
    "classificationCode",
    "tradeDirection",
    "attribute",
    "note",
    "uom",
    "classificationDescription",
    "bindingRulingNumber",
    "bindingRulingEffectiveDate",
    "bindingRulingExpirationDate"
})
public class ProductClassificationType {

    @XmlElement(name = "ClassificationCategory")
    protected String classificationCategory;
    @XmlElement(name = "GtmProdClassTypeGid", required = true)
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "ClassificationCode", required = true)
    protected String classificationCode;
    @XmlElement(name = "TradeDirection")
    protected String tradeDirection;
    @XmlElement(name = "Attribute")
    protected List<AttributeType> attribute;
    @XmlElement(name = "Note")
    protected List<ProdClassCodeNoteType> note;
    @XmlElement(name = "UOM")
    protected List<ProdClassCodeUOMType> uom;
    @XmlElement(name = "ClassificationDescription")
    protected List<ProdClassCodeDescType> classificationDescription;
    @XmlElement(name = "BindingRulingNumber")
    protected String bindingRulingNumber;
    @XmlElement(name = "BindingRulingEffectiveDate")
    protected GLogDateTimeType bindingRulingEffectiveDate;
    @XmlElement(name = "BindingRulingExpirationDate")
    protected GLogDateTimeType bindingRulingExpirationDate;

    /**
     * Gets the value of the classificationCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCategory() {
        return classificationCategory;
    }

    /**
     * Sets the value of the classificationCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCategory(String value) {
        this.classificationCategory = value;
    }

    /**
     * Gets the value of the gtmProdClassTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Sets the value of the gtmProdClassTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Gets the value of the classificationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificationCode() {
        return classificationCode;
    }

    /**
     * Sets the value of the classificationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificationCode(String value) {
        this.classificationCode = value;
    }

    /**
     * Gets the value of the tradeDirection property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradeDirection() {
        return tradeDirection;
    }

    /**
     * Sets the value of the tradeDirection property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradeDirection(String value) {
        this.tradeDirection = value;
    }

    /**
     * Gets the value of the attribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the attribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeType }
     * 
     * 
     */
    public List<AttributeType> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<AttributeType>();
        }
        return this.attribute;
    }

    /**
     * Gets the value of the note property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the note property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdClassCodeNoteType }
     * 
     * 
     */
    public List<ProdClassCodeNoteType> getNote() {
        if (note == null) {
            note = new ArrayList<ProdClassCodeNoteType>();
        }
        return this.note;
    }

    /**
     * Gets the value of the uom property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the uom property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUOM().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdClassCodeUOMType }
     * 
     * 
     */
    public List<ProdClassCodeUOMType> getUOM() {
        if (uom == null) {
            uom = new ArrayList<ProdClassCodeUOMType>();
        }
        return this.uom;
    }

    /**
     * Gets the value of the classificationDescription property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the classificationDescription property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassificationDescription().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProdClassCodeDescType }
     * 
     * 
     */
    public List<ProdClassCodeDescType> getClassificationDescription() {
        if (classificationDescription == null) {
            classificationDescription = new ArrayList<ProdClassCodeDescType>();
        }
        return this.classificationDescription;
    }

    /**
     * Gets the value of the bindingRulingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBindingRulingNumber() {
        return bindingRulingNumber;
    }

    /**
     * Sets the value of the bindingRulingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBindingRulingNumber(String value) {
        this.bindingRulingNumber = value;
    }

    /**
     * Gets the value of the bindingRulingEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getBindingRulingEffectiveDate() {
        return bindingRulingEffectiveDate;
    }

    /**
     * Sets the value of the bindingRulingEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setBindingRulingEffectiveDate(GLogDateTimeType value) {
        this.bindingRulingEffectiveDate = value;
    }

    /**
     * Gets the value of the bindingRulingExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getBindingRulingExpirationDate() {
        return bindingRulingExpirationDate;
    }

    /**
     * Sets the value of the bindingRulingExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setBindingRulingExpirationDate(GLogDateTimeType value) {
        this.bindingRulingExpirationDate = value;
    }

}

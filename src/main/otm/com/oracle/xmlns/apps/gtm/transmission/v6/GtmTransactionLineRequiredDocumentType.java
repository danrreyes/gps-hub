
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) Required Documents of transaction line item or declaration line item.
 *          
 * 
 * <p>Java class for GtmTransactionLineRequiredDocumentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmTransactionLineRequiredDocumentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmTransactionLineDocumentRequiredText" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RequiredTextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="DocumentDefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmTransactionLineRequiredDocumentType", propOrder = {
    "gtmTransactionLineDocumentRequiredText",
    "documentDefGid",
    "complianceRuleGid",
    "complianceRuleGroupGid"
})
public class GtmTransactionLineRequiredDocumentType {

    @XmlElement(name = "GtmTransactionLineDocumentRequiredText")
    protected List<RequiredTextType> gtmTransactionLineDocumentRequiredText;
    @XmlElement(name = "DocumentDefGid", required = true)
    protected GLogXMLGidType documentDefGid;
    @XmlElement(name = "ComplianceRuleGid")
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid")
    protected GLogXMLGidType complianceRuleGroupGid;

    /**
     * Gets the value of the gtmTransactionLineDocumentRequiredText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransactionLineDocumentRequiredText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransactionLineDocumentRequiredText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequiredTextType }
     * 
     * 
     */
    public List<RequiredTextType> getGtmTransactionLineDocumentRequiredText() {
        if (gtmTransactionLineDocumentRequiredText == null) {
            gtmTransactionLineDocumentRequiredText = new ArrayList<RequiredTextType>();
        }
        return this.gtmTransactionLineDocumentRequiredText;
    }

    /**
     * Gets the value of the documentDefGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDocumentDefGid() {
        return documentDefGid;
    }

    /**
     * Sets the value of the documentDefGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDocumentDefGid(GLogXMLGidType value) {
        this.documentDefGid = value;
    }

    /**
     * Gets the value of the complianceRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Sets the value of the complianceRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Gets the value of the complianceRuleGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Sets the value of the complianceRuleGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import com.oracle.xmlns.apps.otm.transmission.v6.LocationOverrideInfoType;
import com.oracle.xmlns.apps.otm.transmission.v6.TransactionCodeType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GtmInvolvedPartyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmInvolvedPartyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="GtmContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *           &lt;element name="GtmContact" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmContactType" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LocationOverrideInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}LocationOverrideInfoType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmInvolvedPartyType", propOrder = {
    "transactionCode",
    "involvedPartyQualifierGid",
    "gtmContactGid",
    "gtmContact",
    "comMethodGid",
    "locationOverrideInfo"
})
public class GtmInvolvedPartyType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "InvolvedPartyQualifierGid", required = true)
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "GtmContactGid")
    protected GLogXMLGidType gtmContactGid;
    @XmlElement(name = "GtmContact")
    protected GtmContactType gtmContact;
    @XmlElement(name = "ComMethodGid")
    protected GLogXMLGidType comMethodGid;
    @XmlElement(name = "LocationOverrideInfo")
    protected LocationOverrideInfoType locationOverrideInfo;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the involvedPartyQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Sets the value of the involvedPartyQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Gets the value of the gtmContactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmContactGid() {
        return gtmContactGid;
    }

    /**
     * Sets the value of the gtmContactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmContactGid(GLogXMLGidType value) {
        this.gtmContactGid = value;
    }

    /**
     * Gets the value of the gtmContact property.
     * 
     * @return
     *     possible object is
     *     {@link GtmContactType }
     *     
     */
    public GtmContactType getGtmContact() {
        return gtmContact;
    }

    /**
     * Sets the value of the gtmContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link GtmContactType }
     *     
     */
    public void setGtmContact(GtmContactType value) {
        this.gtmContact = value;
    }

    /**
     * Gets the value of the comMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Sets the value of the comMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

    /**
     * Gets the value of the locationOverrideInfo property.
     * 
     * @return
     *     possible object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public LocationOverrideInfoType getLocationOverrideInfo() {
        return locationOverrideInfo;
    }

    /**
     * Sets the value of the locationOverrideInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationOverrideInfoType }
     *     
     */
    public void setLocationOverrideInfo(LocationOverrideInfoType value) {
        this.locationOverrideInfo = value;
    }

}

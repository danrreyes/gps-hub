
package com.oracle.xmlns.apps.gtm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) SanctionedTerritoryResponse is the embargo screening response sent for the inbound SanctionedTerritory screening request.
 *          
 * 
 * <p>Java class for SanctionedTerritoryResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SanctionedTerritoryResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RuleControl" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RuleControlType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SanctionedTerritoryResponseType", propOrder = {
    "ruleControl"
})
public class SanctionedTerritoryResponseType {

    @XmlElement(name = "RuleControl", required = true)
    protected RuleControlType ruleControl;

    /**
     * Gets the value of the ruleControl property.
     * 
     * @return
     *     possible object is
     *     {@link RuleControlType }
     *     
     */
    public RuleControlType getRuleControl() {
        return ruleControl;
    }

    /**
     * Sets the value of the ruleControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleControlType }
     *     
     */
    public void setRuleControl(RuleControlType value) {
        this.ruleControl = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) Controls applicable to line items for reporting purpose.
 *          
 * 
 * <p>Java class for ReportingResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReportingResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="GtmExceptionControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="GtmExceptionControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingResultType", propOrder = {
    "gtmControlTypeGid",
    "controlCode",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "gtmExceptionControlTypeGid",
    "gtmExceptionControlCode"
})
public class ReportingResultType {

    @XmlElement(name = "GtmControlTypeGid", required = true)
    protected GLogXMLGidType gtmControlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ComplianceRuleGid", required = true)
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid", required = true)
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "GtmExceptionControlTypeGid", required = true)
    protected GLogXMLGidType gtmExceptionControlTypeGid;
    @XmlElement(name = "GtmExceptionControlCode", required = true)
    protected String gtmExceptionControlCode;

    /**
     * Gets the value of the gtmControlTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmControlTypeGid() {
        return gtmControlTypeGid;
    }

    /**
     * Sets the value of the gtmControlTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmControlTypeGid(GLogXMLGidType value) {
        this.gtmControlTypeGid = value;
    }

    /**
     * Gets the value of the controlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Sets the value of the controlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Gets the value of the complianceRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Sets the value of the complianceRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Gets the value of the complianceRuleGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Sets the value of the complianceRuleGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Gets the value of the gtmExceptionControlTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmExceptionControlTypeGid() {
        return gtmExceptionControlTypeGid;
    }

    /**
     * Sets the value of the gtmExceptionControlTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmExceptionControlTypeGid(GLogXMLGidType value) {
        this.gtmExceptionControlTypeGid = value;
    }

    /**
     * Gets the value of the gtmExceptionControlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmExceptionControlCode() {
        return gtmExceptionControlCode;
    }

    /**
     * Sets the value of the gtmExceptionControlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmExceptionControlCode(String value) {
        this.gtmExceptionControlCode = value;
    }

}

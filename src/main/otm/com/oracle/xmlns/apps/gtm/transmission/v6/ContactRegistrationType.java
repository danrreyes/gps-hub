
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import com.oracle.xmlns.apps.otm.transmission.v6.TransactionCodeType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) Represents the registrations related to the contact.
 *          
 * 
 * <p>Java class for ContactRegistrationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactRegistrationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="GtmRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *           &lt;element name="GtmRegistration" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmRegistrationType"/&gt;
 *         &lt;/choice&gt;
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ComMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactRegistrationType", propOrder = {
    "transactionCode",
    "gtmRegistrationGid",
    "gtmRegistration",
    "involvedPartyQualifierGid",
    "comMethodGid"
})
public class ContactRegistrationType {

    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "GtmRegistrationGid")
    protected GLogXMLGidType gtmRegistrationGid;
    @XmlElement(name = "GtmRegistration")
    protected GtmRegistrationType gtmRegistration;
    @XmlElement(name = "InvolvedPartyQualifierGid", required = true)
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "ComMethodGid", required = true)
    protected GLogXMLGidType comMethodGid;

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the gtmRegistrationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationGid() {
        return gtmRegistrationGid;
    }

    /**
     * Sets the value of the gtmRegistrationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationGid(GLogXMLGidType value) {
        this.gtmRegistrationGid = value;
    }

    /**
     * Gets the value of the gtmRegistration property.
     * 
     * @return
     *     possible object is
     *     {@link GtmRegistrationType }
     *     
     */
    public GtmRegistrationType getGtmRegistration() {
        return gtmRegistration;
    }

    /**
     * Sets the value of the gtmRegistration property.
     * 
     * @param value
     *     allowed object is
     *     {@link GtmRegistrationType }
     *     
     */
    public void setGtmRegistration(GtmRegistrationType value) {
        this.gtmRegistration = value;
    }

    /**
     * Gets the value of the involvedPartyQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Sets the value of the involvedPartyQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Gets the value of the comMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComMethodGid() {
        return comMethodGid;
    }

    /**
     * Sets the value of the comMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComMethodGid(GLogXMLGidType value) {
        this.comMethodGid = value;
    }

}

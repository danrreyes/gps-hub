
package com.oracle.xmlns.apps.gtm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) Defines equipment and seal details for a conveyance.
 *          
 * 
 * <p>Java class for ConveyanceType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConveyanceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EquipmentInitial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EquipmentNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SealNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConveyanceType", propOrder = {
    "sequenceNumber",
    "equipmentInitial",
    "equipmentNumber",
    "sealNumber"
})
public class ConveyanceType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "EquipmentInitial", required = true)
    protected String equipmentInitial;
    @XmlElement(name = "EquipmentNumber", required = true)
    protected String equipmentNumber;
    @XmlElement(name = "SealNumber", required = true)
    protected String sealNumber;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the equipmentInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentInitial() {
        return equipmentInitial;
    }

    /**
     * Sets the value of the equipmentInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentInitial(String value) {
        this.equipmentInitial = value;
    }

    /**
     * Gets the value of the equipmentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEquipmentNumber() {
        return equipmentNumber;
    }

    /**
     * Sets the value of the equipmentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEquipmentNumber(String value) {
        this.equipmentNumber = value;
    }

    /**
     * Gets the value of the sealNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSealNumber() {
        return sealNumber;
    }

    /**
     * Sets the value of the sealNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSealNumber(String value) {
        this.sealNumber = value;
    }

}

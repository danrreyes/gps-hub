
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogDateTimeType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) LicenseDeterminationResponse is response for the license screening triggered via ServiceRequest integration interface.
 *          
 * 
 * <p>Java class for LicenseDeterminationResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LicenseDeterminationResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LicenseGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="LicenseNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LicenseLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="QuantityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="BalanceQuantityStr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CurrencyTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="BalanceValueStr" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LicenseDeterminationResponseType", propOrder = {
    "licenseGid",
    "licenseNumber",
    "licenseLineGid",
    "controlTypeGid",
    "controlCode",
    "expirationDate",
    "quantityTypeGid",
    "balanceQuantityStr",
    "currencyTypeGid",
    "balanceValueStr"
})
public class LicenseDeterminationResponseType {

    @XmlElement(name = "LicenseGid", required = true)
    protected GLogXMLGidType licenseGid;
    @XmlElement(name = "LicenseNumber", required = true)
    protected String licenseNumber;
    @XmlElement(name = "LicenseLineGid", required = true)
    protected GLogXMLGidType licenseLineGid;
    @XmlElement(name = "ControlTypeGid", required = true)
    protected GLogXMLGidType controlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ExpirationDate", required = true)
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "QuantityTypeGid", required = true)
    protected GLogXMLGidType quantityTypeGid;
    @XmlElement(name = "BalanceQuantityStr", required = true)
    protected String balanceQuantityStr;
    @XmlElement(name = "CurrencyTypeGid", required = true)
    protected GLogXMLGidType currencyTypeGid;
    @XmlElement(name = "BalanceValueStr", required = true)
    protected String balanceValueStr;

    /**
     * Gets the value of the licenseGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLicenseGid() {
        return licenseGid;
    }

    /**
     * Sets the value of the licenseGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLicenseGid(GLogXMLGidType value) {
        this.licenseGid = value;
    }

    /**
     * Gets the value of the licenseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseNumber() {
        return licenseNumber;
    }

    /**
     * Sets the value of the licenseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseNumber(String value) {
        this.licenseNumber = value;
    }

    /**
     * Gets the value of the licenseLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLicenseLineGid() {
        return licenseLineGid;
    }

    /**
     * Sets the value of the licenseLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLicenseLineGid(GLogXMLGidType value) {
        this.licenseLineGid = value;
    }

    /**
     * Gets the value of the controlTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getControlTypeGid() {
        return controlTypeGid;
    }

    /**
     * Sets the value of the controlTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setControlTypeGid(GLogXMLGidType value) {
        this.controlTypeGid = value;
    }

    /**
     * Gets the value of the controlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Sets the value of the controlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the quantityTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuantityTypeGid() {
        return quantityTypeGid;
    }

    /**
     * Sets the value of the quantityTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuantityTypeGid(GLogXMLGidType value) {
        this.quantityTypeGid = value;
    }

    /**
     * Gets the value of the balanceQuantityStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceQuantityStr() {
        return balanceQuantityStr;
    }

    /**
     * Sets the value of the balanceQuantityStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceQuantityStr(String value) {
        this.balanceQuantityStr = value;
    }

    /**
     * Gets the value of the currencyTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCurrencyTypeGid() {
        return currencyTypeGid;
    }

    /**
     * Sets the value of the currencyTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCurrencyTypeGid(GLogXMLGidType value) {
        this.currencyTypeGid = value;
    }

    /**
     * Gets the value of the balanceValueStr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceValueStr() {
        return balanceValueStr;
    }

    /**
     * Sets the value of the balanceValueStr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceValueStr(String value) {
        this.balanceValueStr = value;
    }

}

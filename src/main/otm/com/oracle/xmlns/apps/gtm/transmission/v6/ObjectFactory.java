
package com.oracle.xmlns.apps.gtm.transmission.v6;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.oracle.xmlns.apps.gtm.transmission.v6 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GtmContact_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmContact");
    private final static QName _GtmRegistration_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmRegistration");
    private final static QName _GtmTransaction_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmTransaction");
    private final static QName _GtmTransactionLine_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmTransactionLine");
    private final static QName _GtmStructure_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmStructure");
    private final static QName _GtmDeclaration_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmDeclaration");
    private final static QName _GtmDeclarationMessage_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmDeclarationMessage");
    private final static QName _GtmBond_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "GtmBond");
    private final static QName _ServiceRequest_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "ServiceRequest");
    private final static QName _ServiceResponse_QNAME = new QName("http://xmlns.oracle.com/apps/gtm/transmission/v6.4", "ServiceResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.oracle.xmlns.apps.gtm.transmission.v6
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GtmContactType }
     * 
     */
    public GtmContactType createGtmContactType() {
        return new GtmContactType();
    }

    /**
     * Create an instance of {@link GtmRegistrationType }
     * 
     */
    public GtmRegistrationType createGtmRegistrationType() {
        return new GtmRegistrationType();
    }

    /**
     * Create an instance of {@link GtmTransactionType }
     * 
     */
    public GtmTransactionType createGtmTransactionType() {
        return new GtmTransactionType();
    }

    /**
     * Create an instance of {@link GtmTransactionLineType }
     * 
     */
    public GtmTransactionLineType createGtmTransactionLineType() {
        return new GtmTransactionLineType();
    }

    /**
     * Create an instance of {@link GtmStructureType }
     * 
     */
    public GtmStructureType createGtmStructureType() {
        return new GtmStructureType();
    }

    /**
     * Create an instance of {@link GtmDeclarationType }
     * 
     */
    public GtmDeclarationType createGtmDeclarationType() {
        return new GtmDeclarationType();
    }

    /**
     * Create an instance of {@link GtmDeclarationMessageType }
     * 
     */
    public GtmDeclarationMessageType createGtmDeclarationMessageType() {
        return new GtmDeclarationMessageType();
    }

    /**
     * Create an instance of {@link GtmBondType }
     * 
     */
    public GtmBondType createGtmBondType() {
        return new GtmBondType();
    }

    /**
     * Create an instance of {@link ServiceRequestType }
     * 
     */
    public ServiceRequestType createServiceRequestType() {
        return new ServiceRequestType();
    }

    /**
     * Create an instance of {@link ServiceResponseType }
     * 
     */
    public ServiceResponseType createServiceResponseType() {
        return new ServiceResponseType();
    }

    /**
     * Create an instance of {@link ResponseInfoType }
     * 
     */
    public ResponseInfoType createResponseInfoType() {
        return new ResponseInfoType();
    }

    /**
     * Create an instance of {@link DeclarationTypeType }
     * 
     */
    public DeclarationTypeType createDeclarationTypeType() {
        return new DeclarationTypeType();
    }

    /**
     * Create an instance of {@link DeclarationSubTypeType }
     * 
     */
    public DeclarationSubTypeType createDeclarationSubTypeType() {
        return new DeclarationSubTypeType();
    }

    /**
     * Create an instance of {@link ValuationMethodType }
     * 
     */
    public ValuationMethodType createValuationMethodType() {
        return new ValuationMethodType();
    }

    /**
     * Create an instance of {@link TariffPreferenceTypeType }
     * 
     */
    public TariffPreferenceTypeType createTariffPreferenceTypeType() {
        return new TariffPreferenceTypeType();
    }

    /**
     * Create an instance of {@link ProcedureType }
     * 
     */
    public ProcedureType createProcedureType() {
        return new ProcedureType();
    }

    /**
     * Create an instance of {@link ProcedureCategoryType }
     * 
     */
    public ProcedureCategoryType createProcedureCategoryType() {
        return new ProcedureCategoryType();
    }

    /**
     * Create an instance of {@link ProcedureDetailType }
     * 
     */
    public ProcedureDetailType createProcedureDetailType() {
        return new ProcedureDetailType();
    }

    /**
     * Create an instance of {@link UserDefinedClassificationType }
     * 
     */
    public UserDefinedClassificationType createUserDefinedClassificationType() {
        return new UserDefinedClassificationType();
    }

    /**
     * Create an instance of {@link UserDefinedTypeType }
     * 
     */
    public UserDefinedTypeType createUserDefinedTypeType() {
        return new UserDefinedTypeType();
    }

    /**
     * Create an instance of {@link UserDefinedCodeType }
     * 
     */
    public UserDefinedCodeType createUserDefinedCodeType() {
        return new UserDefinedCodeType();
    }

    /**
     * Create an instance of {@link RestrictedPartyType }
     * 
     */
    public RestrictedPartyType createRestrictedPartyType() {
        return new RestrictedPartyType();
    }

    /**
     * Create an instance of {@link RestrictedPartyResponseType }
     * 
     */
    public RestrictedPartyResponseType createRestrictedPartyResponseType() {
        return new RestrictedPartyResponseType();
    }

    /**
     * Create an instance of {@link SanctionedTerritoryType }
     * 
     */
    public SanctionedTerritoryType createSanctionedTerritoryType() {
        return new SanctionedTerritoryType();
    }

    /**
     * Create an instance of {@link SanctionedTerritoryResponseType }
     * 
     */
    public SanctionedTerritoryResponseType createSanctionedTerritoryResponseType() {
        return new SanctionedTerritoryResponseType();
    }

    /**
     * Create an instance of {@link ClassificationType }
     * 
     */
    public ClassificationType createClassificationType() {
        return new ClassificationType();
    }

    /**
     * Create an instance of {@link ClassificationResponseType }
     * 
     */
    public ClassificationResponseType createClassificationResponseType() {
        return new ClassificationResponseType();
    }

    /**
     * Create an instance of {@link ClassCodeType }
     * 
     */
    public ClassCodeType createClassCodeType() {
        return new ClassCodeType();
    }

    /**
     * Create an instance of {@link ComplianceRuleType }
     * 
     */
    public ComplianceRuleType createComplianceRuleType() {
        return new ComplianceRuleType();
    }

    /**
     * Create an instance of {@link ComplianceRuleResponseType }
     * 
     */
    public ComplianceRuleResponseType createComplianceRuleResponseType() {
        return new ComplianceRuleResponseType();
    }

    /**
     * Create an instance of {@link LicenseDeterminationType }
     * 
     */
    public LicenseDeterminationType createLicenseDeterminationType() {
        return new LicenseDeterminationType();
    }

    /**
     * Create an instance of {@link LicenseDeterminationResponseType }
     * 
     */
    public LicenseDeterminationResponseType createLicenseDeterminationResponseType() {
        return new LicenseDeterminationResponseType();
    }

    /**
     * Create an instance of {@link RuleControlType }
     * 
     */
    public RuleControlType createRuleControlType() {
        return new RuleControlType();
    }

    /**
     * Create an instance of {@link ContactRegistrationType }
     * 
     */
    public ContactRegistrationType createContactRegistrationType() {
        return new ContactRegistrationType();
    }

    /**
     * Create an instance of {@link GtmRegistrationDateType }
     * 
     */
    public GtmRegistrationDateType createGtmRegistrationDateType() {
        return new GtmRegistrationDateType();
    }

    /**
     * Create an instance of {@link AttributeType }
     * 
     */
    public AttributeType createAttributeType() {
        return new AttributeType();
    }

    /**
     * Create an instance of {@link ConditionType }
     * 
     */
    public ConditionType createConditionType() {
        return new ConditionType();
    }

    /**
     * Create an instance of {@link ProductInfoType }
     * 
     */
    public ProductInfoType createProductInfoType() {
        return new ProductInfoType();
    }

    /**
     * Create an instance of {@link InvolvedPartyDetailType }
     * 
     */
    public InvolvedPartyDetailType createInvolvedPartyDetailType() {
        return new InvolvedPartyDetailType();
    }

    /**
     * Create an instance of {@link CategoryTypeCodeType }
     * 
     */
    public CategoryTypeCodeType createCategoryTypeCodeType() {
        return new CategoryTypeCodeType();
    }

    /**
     * Create an instance of {@link LocationInfoType }
     * 
     */
    public LocationInfoType createLocationInfoType() {
        return new LocationInfoType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedItemType }
     * 
     */
    public GtmRegRelatedItemType createGtmRegRelatedItemType() {
        return new GtmRegRelatedItemType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedCommodityType }
     * 
     */
    public GtmRegRelatedCommodityType createGtmRegRelatedCommodityType() {
        return new GtmRegRelatedCommodityType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedUDCommodityType }
     * 
     */
    public GtmRegRelatedUDCommodityType createGtmRegRelatedUDCommodityType() {
        return new GtmRegRelatedUDCommodityType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedLicenseType }
     * 
     */
    public GtmRegRelatedLicenseType createGtmRegRelatedLicenseType() {
        return new GtmRegRelatedLicenseType();
    }

    /**
     * Create an instance of {@link GtmRegRelatedRegistrationType }
     * 
     */
    public GtmRegRelatedRegistrationType createGtmRegRelatedRegistrationType() {
        return new GtmRegRelatedRegistrationType();
    }

    /**
     * Create an instance of {@link QuantityType }
     * 
     */
    public QuantityType createQuantityType() {
        return new QuantityType();
    }

    /**
     * Create an instance of {@link ReportingQuantityType }
     * 
     */
    public ReportingQuantityType createReportingQuantityType() {
        return new ReportingQuantityType();
    }

    /**
     * Create an instance of {@link CurrencyType }
     * 
     */
    public CurrencyType createCurrencyType() {
        return new CurrencyType();
    }

    /**
     * Create an instance of {@link TransactionPolicyType }
     * 
     */
    public TransactionPolicyType createTransactionPolicyType() {
        return new TransactionPolicyType();
    }

    /**
     * Create an instance of {@link TransactionRequiredDocumentType }
     * 
     */
    public TransactionRequiredDocumentType createTransactionRequiredDocumentType() {
        return new TransactionRequiredDocumentType();
    }

    /**
     * Create an instance of {@link PartyScreeningResultType }
     * 
     */
    public PartyScreeningResultType createPartyScreeningResultType() {
        return new PartyScreeningResultType();
    }

    /**
     * Create an instance of {@link CommentsType }
     * 
     */
    public CommentsType createCommentsType() {
        return new CommentsType();
    }

    /**
     * Create an instance of {@link GtmTransLineLicenseType }
     * 
     */
    public GtmTransLineLicenseType createGtmTransLineLicenseType() {
        return new GtmTransLineLicenseType();
    }

    /**
     * Create an instance of {@link GtmTransactionLineRequiredDocumentType }
     * 
     */
    public GtmTransactionLineRequiredDocumentType createGtmTransactionLineRequiredDocumentType() {
        return new GtmTransactionLineRequiredDocumentType();
    }

    /**
     * Create an instance of {@link RequiredTextType }
     * 
     */
    public RequiredTextType createRequiredTextType() {
        return new RequiredTextType();
    }

    /**
     * Create an instance of {@link ProductClassificationType }
     * 
     */
    public ProductClassificationType createProductClassificationType() {
        return new ProductClassificationType();
    }

    /**
     * Create an instance of {@link ProdClassCodeNoteType }
     * 
     */
    public ProdClassCodeNoteType createProdClassCodeNoteType() {
        return new ProdClassCodeNoteType();
    }

    /**
     * Create an instance of {@link ProdClassCodeUOMType }
     * 
     */
    public ProdClassCodeUOMType createProdClassCodeUOMType() {
        return new ProdClassCodeUOMType();
    }

    /**
     * Create an instance of {@link ProdClassCodeDescType }
     * 
     */
    public ProdClassCodeDescType createProdClassCodeDescType() {
        return new ProdClassCodeDescType();
    }

    /**
     * Create an instance of {@link ControlScreeningResultGenericType }
     * 
     */
    public ControlScreeningResultGenericType createControlScreeningResultGenericType() {
        return new ControlScreeningResultGenericType();
    }

    /**
     * Create an instance of {@link ControlScreeningResultType }
     * 
     */
    public ControlScreeningResultType createControlScreeningResultType() {
        return new ControlScreeningResultType();
    }

    /**
     * Create an instance of {@link SanctionControlScreeningResultType }
     * 
     */
    public SanctionControlScreeningResultType createSanctionControlScreeningResultType() {
        return new SanctionControlScreeningResultType();
    }

    /**
     * Create an instance of {@link ReportingResultType }
     * 
     */
    public ReportingResultType createReportingResultType() {
        return new ReportingResultType();
    }

    /**
     * Create an instance of {@link RestrictedPartyScreeningResultType }
     * 
     */
    public RestrictedPartyScreeningResultType createRestrictedPartyScreeningResultType() {
        return new RestrictedPartyScreeningResultType();
    }

    /**
     * Create an instance of {@link TerritoryInfoType }
     * 
     */
    public TerritoryInfoType createTerritoryInfoType() {
        return new TerritoryInfoType();
    }

    /**
     * Create an instance of {@link TransactionInvLocationType }
     * 
     */
    public TransactionInvLocationType createTransactionInvLocationType() {
        return new TransactionInvLocationType();
    }

    /**
     * Create an instance of {@link RegionInfoType }
     * 
     */
    public RegionInfoType createRegionInfoType() {
        return new RegionInfoType();
    }

    /**
     * Create an instance of {@link PortInfoType }
     * 
     */
    public PortInfoType createPortInfoType() {
        return new PortInfoType();
    }

    /**
     * Create an instance of {@link ControlTypeCodeType }
     * 
     */
    public ControlTypeCodeType createControlTypeCodeType() {
        return new ControlTypeCodeType();
    }

    /**
     * Create an instance of {@link RegionCountryInfoType }
     * 
     */
    public RegionCountryInfoType createRegionCountryInfoType() {
        return new RegionCountryInfoType();
    }

    /**
     * Create an instance of {@link TransDateType }
     * 
     */
    public TransDateType createTransDateType() {
        return new TransDateType();
    }

    /**
     * Create an instance of {@link CalculatedValueResultType }
     * 
     */
    public CalculatedValueResultType createCalculatedValueResultType() {
        return new CalculatedValueResultType();
    }

    /**
     * Create an instance of {@link GtmStructureComponentType }
     * 
     */
    public GtmStructureComponentType createGtmStructureComponentType() {
        return new GtmStructureComponentType();
    }

    /**
     * Create an instance of {@link PercentageType }
     * 
     */
    public PercentageType createPercentageType() {
        return new PercentageType();
    }

    /**
     * Create an instance of {@link GtmDeclarationLineType }
     * 
     */
    public GtmDeclarationLineType createGtmDeclarationLineType() {
        return new GtmDeclarationLineType();
    }

    /**
     * Create an instance of {@link AdditionalInfoType }
     * 
     */
    public AdditionalInfoType createAdditionalInfoType() {
        return new AdditionalInfoType();
    }

    /**
     * Create an instance of {@link ProducedDocumentType }
     * 
     */
    public ProducedDocumentType createProducedDocumentType() {
        return new ProducedDocumentType();
    }

    /**
     * Create an instance of {@link GtmInvolvedPartyType }
     * 
     */
    public GtmInvolvedPartyType createGtmInvolvedPartyType() {
        return new GtmInvolvedPartyType();
    }

    /**
     * Create an instance of {@link InvolvedPartyRefnumDetailType }
     * 
     */
    public InvolvedPartyRefnumDetailType createInvolvedPartyRefnumDetailType() {
        return new InvolvedPartyRefnumDetailType();
    }

    /**
     * Create an instance of {@link InvolvedPartyRemarkDetailType }
     * 
     */
    public InvolvedPartyRemarkDetailType createInvolvedPartyRemarkDetailType() {
        return new InvolvedPartyRemarkDetailType();
    }

    /**
     * Create an instance of {@link ConveyanceType }
     * 
     */
    public ConveyanceType createConveyanceType() {
        return new ConveyanceType();
    }

    /**
     * Create an instance of {@link GtmRemarksType }
     * 
     */
    public GtmRemarksType createGtmRemarksType() {
        return new GtmRemarksType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmContactType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmContactType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmContact", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmContactType> createGtmContact(GtmContactType value) {
        return new JAXBElement<GtmContactType>(_GtmContact_QNAME, GtmContactType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmRegistrationType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmRegistrationType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmRegistration", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmRegistrationType> createGtmRegistration(GtmRegistrationType value) {
        return new JAXBElement<GtmRegistrationType>(_GtmRegistration_QNAME, GtmRegistrationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmTransactionType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmTransactionType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmTransaction", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmTransactionType> createGtmTransaction(GtmTransactionType value) {
        return new JAXBElement<GtmTransactionType>(_GtmTransaction_QNAME, GtmTransactionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmTransactionLineType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmTransactionLineType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmTransactionLine", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmTransactionLineType> createGtmTransactionLine(GtmTransactionLineType value) {
        return new JAXBElement<GtmTransactionLineType>(_GtmTransactionLine_QNAME, GtmTransactionLineType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmStructureType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmStructureType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmStructure", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmStructureType> createGtmStructure(GtmStructureType value) {
        return new JAXBElement<GtmStructureType>(_GtmStructure_QNAME, GtmStructureType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmDeclarationType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmDeclarationType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmDeclaration", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmDeclarationType> createGtmDeclaration(GtmDeclarationType value) {
        return new JAXBElement<GtmDeclarationType>(_GtmDeclaration_QNAME, GtmDeclarationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmDeclarationMessageType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmDeclarationMessageType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmDeclarationMessage", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmDeclarationMessageType> createGtmDeclarationMessage(GtmDeclarationMessageType value) {
        return new JAXBElement<GtmDeclarationMessageType>(_GtmDeclarationMessage_QNAME, GtmDeclarationMessageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GtmBondType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GtmBondType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "GtmBond", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<GtmBondType> createGtmBond(GtmBondType value) {
        return new JAXBElement<GtmBondType>(_GtmBond_QNAME, GtmBondType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ServiceRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "ServiceRequest", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ServiceRequestType> createServiceRequest(ServiceRequestType value) {
        return new JAXBElement<ServiceRequestType>(_ServiceRequest_QNAME, ServiceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ServiceResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/gtm/transmission/v6.4", name = "ServiceResponse", substitutionHeadNamespace = "http://xmlns.oracle.com/apps/otm/transmission/v6.4", substitutionHeadName = "GLogXMLTransaction")
    public JAXBElement<ServiceResponseType> createServiceResponse(ServiceResponseType value) {
        return new JAXBElement<ServiceResponseType>(_ServiceResponse_QNAME, ServiceResponseType.class, null, value);
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.CountryCodeType;
import com.oracle.xmlns.apps.otm.transmission.v6.FlexFieldDateType;
import com.oracle.xmlns.apps.otm.transmission.v6.FlexFieldNumberType;
import com.oracle.xmlns.apps.otm.transmission.v6.FlexFieldStringType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import com.oracle.xmlns.apps.otm.transmission.v6.IntSavedQueryType;
import com.oracle.xmlns.apps.otm.transmission.v6.OTMTransactionInOut;
import com.oracle.xmlns.apps.otm.transmission.v6.RefnumType;
import com.oracle.xmlns.apps.otm.transmission.v6.RemarkType;
import com.oracle.xmlns.apps.otm.transmission.v6.StatusType;
import com.oracle.xmlns.apps.otm.transmission.v6.TransactionCodeType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Trade transaction information for an line item.
 *          
 * 
 * <p>Java class for GtmTransactionLineType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmTransactionLineType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmTransactionLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="GtmTransactionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ItemGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LineObjectGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ParentTransactionLineGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="TransLineType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CountryOfOrigin" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CountryCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ManufacturingCountry" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}CountryCodeType" minOccurs="0"/&gt;
 *         &lt;element name="IncoTermGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="IncoTermLocation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ProductClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ProductClassificationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="UserDefinedClassification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedClassificationType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ControlScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ControlScreeningResultType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReportingResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ReportingResultType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="RestrictedPartyScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RestrictedPartyScreeningResultType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Quantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}QuantityType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ReportingQuantity" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ReportingQuantityType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Currency" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CurrencyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Percentage" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}PercentageType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Status" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}StatusType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GtmTransLineLicense" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmTransLineLicenseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GtmTransactionLineRequiredDocument" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmTransactionLineRequiredDocumentType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GtmTransactionLineRequiredText" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RequiredTextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="CalculatedValueResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}CalculatedValueResultType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="SanctionControlScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}SanctionControlScreeningResultType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldStrings" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldStringType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldNumbers" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldNumberType" minOccurs="0"/&gt;
 *         &lt;element name="FlexFieldDates" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}FlexFieldDateType" minOccurs="0"/&gt;
 *         &lt;element name="GtmTrItemStructureGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LineDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="TariffPreferenceType" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TariffPreferenceTypeType" minOccurs="0"/&gt;
 *         &lt;element name="TransLineDate" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}TransDateType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OtherControlScreeningResult" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ControlScreeningResultGenericType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmTransactionLineType", propOrder = {
    "gtmTransactionLineGid",
    "intSavedQuery",
    "transactionCode",
    "gtmTransactionGid",
    "itemGid",
    "lineNumber",
    "lineObjectGid",
    "parentTransactionLineGid",
    "transLineType",
    "countryOfOrigin",
    "manufacturingCountry",
    "incoTermGid",
    "incoTermLocation",
    "remark",
    "involvedParty",
    "refnum",
    "productClassification",
    "userDefinedClassification",
    "controlScreeningResult",
    "reportingResult",
    "restrictedPartyScreeningResult",
    "quantity",
    "reportingQuantity",
    "currency",
    "percentage",
    "status",
    "gtmTransLineLicense",
    "gtmTransactionLineRequiredDocument",
    "gtmTransactionLineRequiredText",
    "calculatedValueResult",
    "sanctionControlScreeningResult",
    "flexFieldStrings",
    "flexFieldNumbers",
    "flexFieldDates",
    "gtmTrItemStructureGid",
    "lineDescription",
    "tariffPreferenceType",
    "transLineDate",
    "otherControlScreeningResult"
})
public class GtmTransactionLineType
    extends OTMTransactionInOut
{

    @XmlElement(name = "GtmTransactionLineGid")
    protected GLogXMLGidType gtmTransactionLineGid;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "GtmTransactionGid")
    protected GLogXMLGidType gtmTransactionGid;
    @XmlElement(name = "ItemGid")
    protected GLogXMLGidType itemGid;
    @XmlElement(name = "LineNumber")
    protected String lineNumber;
    @XmlElement(name = "LineObjectGid")
    protected GLogXMLGidType lineObjectGid;
    @XmlElement(name = "ParentTransactionLineGid")
    protected GLogXMLGidType parentTransactionLineGid;
    @XmlElement(name = "TransLineType")
    protected String transLineType;
    @XmlElement(name = "CountryOfOrigin")
    protected CountryCodeType countryOfOrigin;
    @XmlElement(name = "ManufacturingCountry")
    protected CountryCodeType manufacturingCountry;
    @XmlElement(name = "IncoTermGid")
    protected GLogXMLGidType incoTermGid;
    @XmlElement(name = "IncoTermLocation")
    protected String incoTermLocation;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "InvolvedParty")
    protected List<GtmInvolvedPartyType> involvedParty;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "ProductClassification")
    protected List<ProductClassificationType> productClassification;
    @XmlElement(name = "UserDefinedClassification")
    protected List<UserDefinedClassificationType> userDefinedClassification;
    @XmlElement(name = "ControlScreeningResult")
    protected List<ControlScreeningResultType> controlScreeningResult;
    @XmlElement(name = "ReportingResult")
    protected List<ReportingResultType> reportingResult;
    @XmlElement(name = "RestrictedPartyScreeningResult")
    protected List<RestrictedPartyScreeningResultType> restrictedPartyScreeningResult;
    @XmlElement(name = "Quantity")
    protected List<QuantityType> quantity;
    @XmlElement(name = "ReportingQuantity")
    protected List<ReportingQuantityType> reportingQuantity;
    @XmlElement(name = "Currency")
    protected List<CurrencyType> currency;
    @XmlElement(name = "Percentage")
    protected List<PercentageType> percentage;
    @XmlElement(name = "Status")
    protected List<StatusType> status;
    @XmlElement(name = "GtmTransLineLicense")
    protected List<GtmTransLineLicenseType> gtmTransLineLicense;
    @XmlElement(name = "GtmTransactionLineRequiredDocument")
    protected List<GtmTransactionLineRequiredDocumentType> gtmTransactionLineRequiredDocument;
    @XmlElement(name = "GtmTransactionLineRequiredText")
    protected List<RequiredTextType> gtmTransactionLineRequiredText;
    @XmlElement(name = "CalculatedValueResult")
    protected List<CalculatedValueResultType> calculatedValueResult;
    @XmlElement(name = "SanctionControlScreeningResult")
    protected List<SanctionControlScreeningResultType> sanctionControlScreeningResult;
    @XmlElement(name = "FlexFieldStrings")
    protected FlexFieldStringType flexFieldStrings;
    @XmlElement(name = "FlexFieldNumbers")
    protected FlexFieldNumberType flexFieldNumbers;
    @XmlElement(name = "FlexFieldDates")
    protected FlexFieldDateType flexFieldDates;
    @XmlElement(name = "GtmTrItemStructureGid")
    protected GLogXMLGidType gtmTrItemStructureGid;
    @XmlElement(name = "LineDescription")
    protected String lineDescription;
    @XmlElement(name = "TariffPreferenceType")
    protected TariffPreferenceTypeType tariffPreferenceType;
    @XmlElement(name = "TransLineDate")
    protected List<TransDateType> transLineDate;
    @XmlElement(name = "OtherControlScreeningResult")
    protected List<ControlScreeningResultGenericType> otherControlScreeningResult;

    /**
     * Gets the value of the gtmTransactionLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmTransactionLineGid() {
        return gtmTransactionLineGid;
    }

    /**
     * Sets the value of the gtmTransactionLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmTransactionLineGid(GLogXMLGidType value) {
        this.gtmTransactionLineGid = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the gtmTransactionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmTransactionGid() {
        return gtmTransactionGid;
    }

    /**
     * Sets the value of the gtmTransactionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmTransactionGid(GLogXMLGidType value) {
        this.gtmTransactionGid = value;
    }

    /**
     * Gets the value of the itemGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getItemGid() {
        return itemGid;
    }

    /**
     * Sets the value of the itemGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setItemGid(GLogXMLGidType value) {
        this.itemGid = value;
    }

    /**
     * Gets the value of the lineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineNumber() {
        return lineNumber;
    }

    /**
     * Sets the value of the lineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineNumber(String value) {
        this.lineNumber = value;
    }

    /**
     * Gets the value of the lineObjectGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getLineObjectGid() {
        return lineObjectGid;
    }

    /**
     * Sets the value of the lineObjectGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setLineObjectGid(GLogXMLGidType value) {
        this.lineObjectGid = value;
    }

    /**
     * Gets the value of the parentTransactionLineGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getParentTransactionLineGid() {
        return parentTransactionLineGid;
    }

    /**
     * Sets the value of the parentTransactionLineGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setParentTransactionLineGid(GLogXMLGidType value) {
        this.parentTransactionLineGid = value;
    }

    /**
     * Gets the value of the transLineType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransLineType() {
        return transLineType;
    }

    /**
     * Sets the value of the transLineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransLineType(String value) {
        this.transLineType = value;
    }

    /**
     * Gets the value of the countryOfOrigin property.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getCountryOfOrigin() {
        return countryOfOrigin;
    }

    /**
     * Sets the value of the countryOfOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setCountryOfOrigin(CountryCodeType value) {
        this.countryOfOrigin = value;
    }

    /**
     * Gets the value of the manufacturingCountry property.
     * 
     * @return
     *     possible object is
     *     {@link CountryCodeType }
     *     
     */
    public CountryCodeType getManufacturingCountry() {
        return manufacturingCountry;
    }

    /**
     * Sets the value of the manufacturingCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryCodeType }
     *     
     */
    public void setManufacturingCountry(CountryCodeType value) {
        this.manufacturingCountry = value;
    }

    /**
     * Gets the value of the incoTermGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getIncoTermGid() {
        return incoTermGid;
    }

    /**
     * Sets the value of the incoTermGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setIncoTermGid(GLogXMLGidType value) {
        this.incoTermGid = value;
    }

    /**
     * Gets the value of the incoTermLocation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncoTermLocation() {
        return incoTermLocation;
    }

    /**
     * Sets the value of the incoTermLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncoTermLocation(String value) {
        this.incoTermLocation = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmInvolvedPartyType }
     * 
     * 
     */
    public List<GtmInvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<GtmInvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the productClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the productClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductClassificationType }
     * 
     * 
     */
    public List<ProductClassificationType> getProductClassification() {
        if (productClassification == null) {
            productClassification = new ArrayList<ProductClassificationType>();
        }
        return this.productClassification;
    }

    /**
     * Gets the value of the userDefinedClassification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the userDefinedClassification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserDefinedClassification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDefinedClassificationType }
     * 
     * 
     */
    public List<UserDefinedClassificationType> getUserDefinedClassification() {
        if (userDefinedClassification == null) {
            userDefinedClassification = new ArrayList<UserDefinedClassificationType>();
        }
        return this.userDefinedClassification;
    }

    /**
     * Gets the value of the controlScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the controlScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getControlScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ControlScreeningResultType }
     * 
     * 
     */
    public List<ControlScreeningResultType> getControlScreeningResult() {
        if (controlScreeningResult == null) {
            controlScreeningResult = new ArrayList<ControlScreeningResultType>();
        }
        return this.controlScreeningResult;
    }

    /**
     * Gets the value of the reportingResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the reportingResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportingResultType }
     * 
     * 
     */
    public List<ReportingResultType> getReportingResult() {
        if (reportingResult == null) {
            reportingResult = new ArrayList<ReportingResultType>();
        }
        return this.reportingResult;
    }

    /**
     * Gets the value of the restrictedPartyScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the restrictedPartyScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRestrictedPartyScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestrictedPartyScreeningResultType }
     * 
     * 
     */
    public List<RestrictedPartyScreeningResultType> getRestrictedPartyScreeningResult() {
        if (restrictedPartyScreeningResult == null) {
            restrictedPartyScreeningResult = new ArrayList<RestrictedPartyScreeningResultType>();
        }
        return this.restrictedPartyScreeningResult;
    }

    /**
     * Gets the value of the quantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the quantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuantityType }
     * 
     * 
     */
    public List<QuantityType> getQuantity() {
        if (quantity == null) {
            quantity = new ArrayList<QuantityType>();
        }
        return this.quantity;
    }

    /**
     * Gets the value of the reportingQuantity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the reportingQuantity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportingQuantity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportingQuantityType }
     * 
     * 
     */
    public List<ReportingQuantityType> getReportingQuantity() {
        if (reportingQuantity == null) {
            reportingQuantity = new ArrayList<ReportingQuantityType>();
        }
        return this.reportingQuantity;
    }

    /**
     * Gets the value of the currency property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the currency property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCurrency().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CurrencyType }
     * 
     * 
     */
    public List<CurrencyType> getCurrency() {
        if (currency == null) {
            currency = new ArrayList<CurrencyType>();
        }
        return this.currency;
    }

    /**
     * Gets the value of the percentage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the percentage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPercentage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PercentageType }
     * 
     * 
     */
    public List<PercentageType> getPercentage() {
        if (percentage == null) {
            percentage = new ArrayList<PercentageType>();
        }
        return this.percentage;
    }

    /**
     * Gets the value of the status property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the status property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StatusType }
     * 
     * 
     */
    public List<StatusType> getStatus() {
        if (status == null) {
            status = new ArrayList<StatusType>();
        }
        return this.status;
    }

    /**
     * Gets the value of the gtmTransLineLicense property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransLineLicense property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransLineLicense().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmTransLineLicenseType }
     * 
     * 
     */
    public List<GtmTransLineLicenseType> getGtmTransLineLicense() {
        if (gtmTransLineLicense == null) {
            gtmTransLineLicense = new ArrayList<GtmTransLineLicenseType>();
        }
        return this.gtmTransLineLicense;
    }

    /**
     * Gets the value of the gtmTransactionLineRequiredDocument property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransactionLineRequiredDocument property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransactionLineRequiredDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmTransactionLineRequiredDocumentType }
     * 
     * 
     */
    public List<GtmTransactionLineRequiredDocumentType> getGtmTransactionLineRequiredDocument() {
        if (gtmTransactionLineRequiredDocument == null) {
            gtmTransactionLineRequiredDocument = new ArrayList<GtmTransactionLineRequiredDocumentType>();
        }
        return this.gtmTransactionLineRequiredDocument;
    }

    /**
     * Gets the value of the gtmTransactionLineRequiredText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the gtmTransactionLineRequiredText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGtmTransactionLineRequiredText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequiredTextType }
     * 
     * 
     */
    public List<RequiredTextType> getGtmTransactionLineRequiredText() {
        if (gtmTransactionLineRequiredText == null) {
            gtmTransactionLineRequiredText = new ArrayList<RequiredTextType>();
        }
        return this.gtmTransactionLineRequiredText;
    }

    /**
     * Gets the value of the calculatedValueResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the calculatedValueResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCalculatedValueResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CalculatedValueResultType }
     * 
     * 
     */
    public List<CalculatedValueResultType> getCalculatedValueResult() {
        if (calculatedValueResult == null) {
            calculatedValueResult = new ArrayList<CalculatedValueResultType>();
        }
        return this.calculatedValueResult;
    }

    /**
     * Gets the value of the sanctionControlScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the sanctionControlScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSanctionControlScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SanctionControlScreeningResultType }
     * 
     * 
     */
    public List<SanctionControlScreeningResultType> getSanctionControlScreeningResult() {
        if (sanctionControlScreeningResult == null) {
            sanctionControlScreeningResult = new ArrayList<SanctionControlScreeningResultType>();
        }
        return this.sanctionControlScreeningResult;
    }

    /**
     * Gets the value of the flexFieldStrings property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldStringType }
     *     
     */
    public FlexFieldStringType getFlexFieldStrings() {
        return flexFieldStrings;
    }

    /**
     * Sets the value of the flexFieldStrings property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldStringType }
     *     
     */
    public void setFlexFieldStrings(FlexFieldStringType value) {
        this.flexFieldStrings = value;
    }

    /**
     * Gets the value of the flexFieldNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public FlexFieldNumberType getFlexFieldNumbers() {
        return flexFieldNumbers;
    }

    /**
     * Sets the value of the flexFieldNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldNumberType }
     *     
     */
    public void setFlexFieldNumbers(FlexFieldNumberType value) {
        this.flexFieldNumbers = value;
    }

    /**
     * Gets the value of the flexFieldDates property.
     * 
     * @return
     *     possible object is
     *     {@link FlexFieldDateType }
     *     
     */
    public FlexFieldDateType getFlexFieldDates() {
        return flexFieldDates;
    }

    /**
     * Sets the value of the flexFieldDates property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlexFieldDateType }
     *     
     */
    public void setFlexFieldDates(FlexFieldDateType value) {
        this.flexFieldDates = value;
    }

    /**
     * Gets the value of the gtmTrItemStructureGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmTrItemStructureGid() {
        return gtmTrItemStructureGid;
    }

    /**
     * Sets the value of the gtmTrItemStructureGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmTrItemStructureGid(GLogXMLGidType value) {
        this.gtmTrItemStructureGid = value;
    }

    /**
     * Gets the value of the lineDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineDescription() {
        return lineDescription;
    }

    /**
     * Sets the value of the lineDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineDescription(String value) {
        this.lineDescription = value;
    }

    /**
     * Gets the value of the tariffPreferenceType property.
     * 
     * @return
     *     possible object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public TariffPreferenceTypeType getTariffPreferenceType() {
        return tariffPreferenceType;
    }

    /**
     * Sets the value of the tariffPreferenceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TariffPreferenceTypeType }
     *     
     */
    public void setTariffPreferenceType(TariffPreferenceTypeType value) {
        this.tariffPreferenceType = value;
    }

    /**
     * Gets the value of the transLineDate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transLineDate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransLineDate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransDateType }
     * 
     * 
     */
    public List<TransDateType> getTransLineDate() {
        if (transLineDate == null) {
            transLineDate = new ArrayList<TransDateType>();
        }
        return this.transLineDate;
    }

    /**
     * Gets the value of the otherControlScreeningResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the otherControlScreeningResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherControlScreeningResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ControlScreeningResultGenericType }
     * 
     * 
     */
    public List<ControlScreeningResultGenericType> getOtherControlScreeningResult() {
        if (otherControlScreeningResult == null) {
            otherControlScreeningResult = new ArrayList<ControlScreeningResultGenericType>();
        }
        return this.otherControlScreeningResult;
    }

}

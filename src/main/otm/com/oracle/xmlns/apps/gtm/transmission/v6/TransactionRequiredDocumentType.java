
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) Documents Required for the conduct of business transaction
 *          
 * 
 * <p>Java class for TransactionRequiredDocumentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionRequiredDocumentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DocumentDefGid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TransactionRequiredText" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RequiredTextType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionDocumentReviewer" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionDocumentSubscriber" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReviewStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionRequiredDocumentType", propOrder = {
    "documentDefGid",
    "transactionRequiredText",
    "transactionDocumentReviewer",
    "transactionDocumentSubscriber",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "reviewStatus"
})
public class TransactionRequiredDocumentType {

    @XmlElement(name = "DocumentDefGid", required = true)
    protected String documentDefGid;
    @XmlElement(name = "TransactionRequiredText")
    protected List<RequiredTextType> transactionRequiredText;
    @XmlElement(name = "TransactionDocumentReviewer")
    protected List<InvolvedPartyDetailType> transactionDocumentReviewer;
    @XmlElement(name = "TransactionDocumentSubscriber")
    protected List<InvolvedPartyDetailType> transactionDocumentSubscriber;
    @XmlElement(name = "ComplianceRuleGid")
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid")
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "ReviewStatus")
    protected String reviewStatus;

    /**
     * Gets the value of the documentDefGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentDefGid() {
        return documentDefGid;
    }

    /**
     * Sets the value of the documentDefGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentDefGid(String value) {
        this.documentDefGid = value;
    }

    /**
     * Gets the value of the transactionRequiredText property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionRequiredText property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionRequiredText().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RequiredTextType }
     * 
     * 
     */
    public List<RequiredTextType> getTransactionRequiredText() {
        if (transactionRequiredText == null) {
            transactionRequiredText = new ArrayList<RequiredTextType>();
        }
        return this.transactionRequiredText;
    }

    /**
     * Gets the value of the transactionDocumentReviewer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionDocumentReviewer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionDocumentReviewer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyDetailType }
     * 
     * 
     */
    public List<InvolvedPartyDetailType> getTransactionDocumentReviewer() {
        if (transactionDocumentReviewer == null) {
            transactionDocumentReviewer = new ArrayList<InvolvedPartyDetailType>();
        }
        return this.transactionDocumentReviewer;
    }

    /**
     * Gets the value of the transactionDocumentSubscriber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the transactionDocumentSubscriber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionDocumentSubscriber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyDetailType }
     * 
     * 
     */
    public List<InvolvedPartyDetailType> getTransactionDocumentSubscriber() {
        if (transactionDocumentSubscriber == null) {
            transactionDocumentSubscriber = new ArrayList<InvolvedPartyDetailType>();
        }
        return this.transactionDocumentSubscriber;
    }

    /**
     * Gets the value of the complianceRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Sets the value of the complianceRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Gets the value of the complianceRuleGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Sets the value of the complianceRuleGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Gets the value of the reviewStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReviewStatus() {
        return reviewStatus;
    }

    /**
     * Sets the value of the reviewStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReviewStatus(String value) {
        this.reviewStatus = value;
    }

}

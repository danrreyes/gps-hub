
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogDateTimeType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) Date on the business transaction.
 *          
 * 
 * <p>Java class for TransDateType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransDateType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DateQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="TransactionDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransDateType", propOrder = {
    "dateQualifierGid",
    "transactionDate"
})
public class TransDateType {

    @XmlElement(name = "DateQualifierGid", required = true)
    protected GLogXMLGidType dateQualifierGid;
    @XmlElement(name = "TransactionDate", required = true)
    protected GLogDateTimeType transactionDate;

    /**
     * Gets the value of the dateQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getDateQualifierGid() {
        return dateQualifierGid;
    }

    /**
     * Sets the value of the dateQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setDateQualifierGid(GLogXMLGidType value) {
        this.dateQualifierGid = value;
    }

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransactionDate(GLogDateTimeType value) {
        this.transactionDate = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.OTMTransactionOut;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Request/Reply style interface reply to the ServiceRequest.
 *          
 * 
 * <p>Java class for ServiceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceRequest" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ServiceRequestType" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="RestrictedPartyResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RestrictedPartyResponseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="SanctionedTerritoryResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}SanctionedTerritoryResponseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="ClassificationResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ClassificationResponseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="ComplianceRuleResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ComplianceRuleResponseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *           &lt;element name="LicenseDeterminationResponse" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}LicenseDeterminationResponseType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceResponseType", propOrder = {
    "serviceRequest",
    "restrictedPartyResponse",
    "sanctionedTerritoryResponse",
    "classificationResponse",
    "complianceRuleResponse",
    "licenseDeterminationResponse"
})
public class ServiceResponseType
    extends OTMTransactionOut
{

    @XmlElement(name = "ServiceRequest")
    protected ServiceRequestType serviceRequest;
    @XmlElement(name = "RestrictedPartyResponse")
    protected List<RestrictedPartyResponseType> restrictedPartyResponse;
    @XmlElement(name = "SanctionedTerritoryResponse")
    protected List<SanctionedTerritoryResponseType> sanctionedTerritoryResponse;
    @XmlElement(name = "ClassificationResponse")
    protected List<ClassificationResponseType> classificationResponse;
    @XmlElement(name = "ComplianceRuleResponse")
    protected List<ComplianceRuleResponseType> complianceRuleResponse;
    @XmlElement(name = "LicenseDeterminationResponse")
    protected List<LicenseDeterminationResponseType> licenseDeterminationResponse;

    /**
     * Gets the value of the serviceRequest property.
     * 
     * @return
     *     possible object is
     *     {@link ServiceRequestType }
     *     
     */
    public ServiceRequestType getServiceRequest() {
        return serviceRequest;
    }

    /**
     * Sets the value of the serviceRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceRequestType }
     *     
     */
    public void setServiceRequest(ServiceRequestType value) {
        this.serviceRequest = value;
    }

    /**
     * Gets the value of the restrictedPartyResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the restrictedPartyResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRestrictedPartyResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RestrictedPartyResponseType }
     * 
     * 
     */
    public List<RestrictedPartyResponseType> getRestrictedPartyResponse() {
        if (restrictedPartyResponse == null) {
            restrictedPartyResponse = new ArrayList<RestrictedPartyResponseType>();
        }
        return this.restrictedPartyResponse;
    }

    /**
     * Gets the value of the sanctionedTerritoryResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the sanctionedTerritoryResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSanctionedTerritoryResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SanctionedTerritoryResponseType }
     * 
     * 
     */
    public List<SanctionedTerritoryResponseType> getSanctionedTerritoryResponse() {
        if (sanctionedTerritoryResponse == null) {
            sanctionedTerritoryResponse = new ArrayList<SanctionedTerritoryResponseType>();
        }
        return this.sanctionedTerritoryResponse;
    }

    /**
     * Gets the value of the classificationResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the classificationResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassificationResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ClassificationResponseType }
     * 
     * 
     */
    public List<ClassificationResponseType> getClassificationResponse() {
        if (classificationResponse == null) {
            classificationResponse = new ArrayList<ClassificationResponseType>();
        }
        return this.classificationResponse;
    }

    /**
     * Gets the value of the complianceRuleResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the complianceRuleResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getComplianceRuleResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ComplianceRuleResponseType }
     * 
     * 
     */
    public List<ComplianceRuleResponseType> getComplianceRuleResponse() {
        if (complianceRuleResponse == null) {
            complianceRuleResponse = new ArrayList<ComplianceRuleResponseType>();
        }
        return this.complianceRuleResponse;
    }

    /**
     * Gets the value of the licenseDeterminationResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the licenseDeterminationResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLicenseDeterminationResponse().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LicenseDeterminationResponseType }
     * 
     * 
     */
    public List<LicenseDeterminationResponseType> getLicenseDeterminationResponse() {
        if (licenseDeterminationResponse == null) {
            licenseDeterminationResponse = new ArrayList<LicenseDeterminationResponseType>();
        }
        return this.licenseDeterminationResponse;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) Restricted party matches to the line party involved in business transaction.
 *          
 * 
 * <p>Java class for RestrictedPartyScreeningResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RestrictedPartyScreeningResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmContact" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmContactType"/&gt;
 *         &lt;element name="IsPassed" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RestrictedPartyScreeningResultType", propOrder = {
    "gtmContact",
    "isPassed"
})
public class RestrictedPartyScreeningResultType {

    @XmlElement(name = "GtmContact", required = true)
    protected GtmContactType gtmContact;
    @XmlElement(name = "IsPassed", required = true)
    protected String isPassed;

    /**
     * Gets the value of the gtmContact property.
     * 
     * @return
     *     possible object is
     *     {@link GtmContactType }
     *     
     */
    public GtmContactType getGtmContact() {
        return gtmContact;
    }

    /**
     * Sets the value of the gtmContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link GtmContactType }
     *     
     */
    public void setGtmContact(GtmContactType value) {
        this.gtmContact = value;
    }

    /**
     * Gets the value of the isPassed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPassed() {
        return isPassed;
    }

    /**
     * Sets the value of the isPassed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPassed(String value) {
        this.isPassed = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) This element maintains the additional information master data.
 *          
 * 
 * <p>Java class for AdditionalInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AdditionalInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="AdditionalInfoGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExportFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdditionalInfoType", propOrder = {
    "additionalInfoGid",
    "code",
    "text",
    "exportFrom"
})
public class AdditionalInfoType {

    @XmlElement(name = "AdditionalInfoGid")
    protected GLogXMLGidType additionalInfoGid;
    @XmlElement(name = "Code")
    protected String code;
    @XmlElement(name = "Text")
    protected String text;
    @XmlElement(name = "ExportFrom")
    protected String exportFrom;

    /**
     * Gets the value of the additionalInfoGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAdditionalInfoGid() {
        return additionalInfoGid;
    }

    /**
     * Sets the value of the additionalInfoGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAdditionalInfoGid(GLogXMLGidType value) {
        this.additionalInfoGid = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    /**
     * Gets the value of the exportFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExportFrom() {
        return exportFrom;
    }

    /**
     * Sets the value of the exportFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExportFrom(String value) {
        this.exportFrom = value;
    }

}

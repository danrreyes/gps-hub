
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogDateTimeType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLFinancialAmountType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import com.oracle.xmlns.apps.otm.transmission.v6.IntSavedQueryType;
import com.oracle.xmlns.apps.otm.transmission.v6.OTMTransactionInOut;
import com.oracle.xmlns.apps.otm.transmission.v6.RefnumType;
import com.oracle.xmlns.apps.otm.transmission.v6.RemarkType;
import com.oracle.xmlns.apps.otm.transmission.v6.ReplaceChildrenType;
import com.oracle.xmlns.apps.otm.transmission.v6.TransactionCodeType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlSchemaType;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             This element maintains bond or guarantee related information.
 *          
 * 
 * <p>Java class for GtmBondType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmBondType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionInOut"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="BondGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IntSavedQuery" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}IntSavedQueryType" minOccurs="0"/&gt;
 *         &lt;element name="TransactionCode" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}TransactionCodeType" minOccurs="0"/&gt;
 *         &lt;element name="ReplaceChildren" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ReplaceChildrenType" minOccurs="0"/&gt;
 *         &lt;element name="BondNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PriorBondNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="BondType1" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedClassificationType" minOccurs="0"/&gt;
 *         &lt;element name="BondType2" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}UserDefinedClassificationType" minOccurs="0"/&gt;
 *         &lt;element name="IsSupplemental" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExpirationDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="ExecutionDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="LiabilityLimit" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType" minOccurs="0"/&gt;
 *         &lt;element name="Remark" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RemarkType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="Refnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}RefnumType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedParty" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}GtmInvolvedPartyType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="PortInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}PortInfoType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmBondType", propOrder = {
    "bondGid",
    "description",
    "intSavedQuery",
    "transactionCode",
    "replaceChildren",
    "bondNumber",
    "priorBondNumber",
    "bondType1",
    "bondType2",
    "isSupplemental",
    "isExtension",
    "effectiveDate",
    "expirationDate",
    "executionDate",
    "liabilityLimit",
    "remark",
    "refnum",
    "involvedParty",
    "portInfo"
})
public class GtmBondType
    extends OTMTransactionInOut
{

    @XmlElement(name = "BondGid")
    protected GLogXMLGidType bondGid;
    @XmlElement(name = "Description")
    protected String description;
    @XmlElement(name = "IntSavedQuery")
    protected IntSavedQueryType intSavedQuery;
    @XmlElement(name = "TransactionCode")
    @XmlSchemaType(name = "string")
    protected TransactionCodeType transactionCode;
    @XmlElement(name = "ReplaceChildren")
    protected ReplaceChildrenType replaceChildren;
    @XmlElement(name = "BondNumber")
    protected String bondNumber;
    @XmlElement(name = "PriorBondNumber")
    protected String priorBondNumber;
    @XmlElement(name = "BondType1")
    protected UserDefinedClassificationType bondType1;
    @XmlElement(name = "BondType2")
    protected UserDefinedClassificationType bondType2;
    @XmlElement(name = "IsSupplemental")
    protected String isSupplemental;
    @XmlElement(name = "IsExtension")
    protected String isExtension;
    @XmlElement(name = "EffectiveDate")
    protected GLogDateTimeType effectiveDate;
    @XmlElement(name = "ExpirationDate")
    protected GLogDateTimeType expirationDate;
    @XmlElement(name = "ExecutionDate")
    protected GLogDateTimeType executionDate;
    @XmlElement(name = "LiabilityLimit")
    protected GLogXMLFinancialAmountType liabilityLimit;
    @XmlElement(name = "Remark")
    protected List<RemarkType> remark;
    @XmlElement(name = "Refnum")
    protected List<RefnumType> refnum;
    @XmlElement(name = "InvolvedParty")
    protected List<GtmInvolvedPartyType> involvedParty;
    @XmlElement(name = "PortInfo")
    protected List<PortInfoType> portInfo;

    /**
     * Gets the value of the bondGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getBondGid() {
        return bondGid;
    }

    /**
     * Sets the value of the bondGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setBondGid(GLogXMLGidType value) {
        this.bondGid = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the intSavedQuery property.
     * 
     * @return
     *     possible object is
     *     {@link IntSavedQueryType }
     *     
     */
    public IntSavedQueryType getIntSavedQuery() {
        return intSavedQuery;
    }

    /**
     * Sets the value of the intSavedQuery property.
     * 
     * @param value
     *     allowed object is
     *     {@link IntSavedQueryType }
     *     
     */
    public void setIntSavedQuery(IntSavedQueryType value) {
        this.intSavedQuery = value;
    }

    /**
     * Gets the value of the transactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link TransactionCodeType }
     *     
     */
    public TransactionCodeType getTransactionCode() {
        return transactionCode;
    }

    /**
     * Sets the value of the transactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionCodeType }
     *     
     */
    public void setTransactionCode(TransactionCodeType value) {
        this.transactionCode = value;
    }

    /**
     * Gets the value of the replaceChildren property.
     * 
     * @return
     *     possible object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public ReplaceChildrenType getReplaceChildren() {
        return replaceChildren;
    }

    /**
     * Sets the value of the replaceChildren property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReplaceChildrenType }
     *     
     */
    public void setReplaceChildren(ReplaceChildrenType value) {
        this.replaceChildren = value;
    }

    /**
     * Gets the value of the bondNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBondNumber() {
        return bondNumber;
    }

    /**
     * Sets the value of the bondNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBondNumber(String value) {
        this.bondNumber = value;
    }

    /**
     * Gets the value of the priorBondNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorBondNumber() {
        return priorBondNumber;
    }

    /**
     * Sets the value of the priorBondNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorBondNumber(String value) {
        this.priorBondNumber = value;
    }

    /**
     * Gets the value of the bondType1 property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefinedClassificationType }
     *     
     */
    public UserDefinedClassificationType getBondType1() {
        return bondType1;
    }

    /**
     * Sets the value of the bondType1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefinedClassificationType }
     *     
     */
    public void setBondType1(UserDefinedClassificationType value) {
        this.bondType1 = value;
    }

    /**
     * Gets the value of the bondType2 property.
     * 
     * @return
     *     possible object is
     *     {@link UserDefinedClassificationType }
     *     
     */
    public UserDefinedClassificationType getBondType2() {
        return bondType2;
    }

    /**
     * Sets the value of the bondType2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDefinedClassificationType }
     *     
     */
    public void setBondType2(UserDefinedClassificationType value) {
        this.bondType2 = value;
    }

    /**
     * Gets the value of the isSupplemental property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsSupplemental() {
        return isSupplemental;
    }

    /**
     * Sets the value of the isSupplemental property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsSupplemental(String value) {
        this.isSupplemental = value;
    }

    /**
     * Gets the value of the isExtension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsExtension() {
        return isExtension;
    }

    /**
     * Sets the value of the isExtension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsExtension(String value) {
        this.isExtension = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setEffectiveDate(GLogDateTimeType value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExpirationDate(GLogDateTimeType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the executionDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getExecutionDate() {
        return executionDate;
    }

    /**
     * Sets the value of the executionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setExecutionDate(GLogDateTimeType value) {
        this.executionDate = value;
    }

    /**
     * Gets the value of the liabilityLimit property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getLiabilityLimit() {
        return liabilityLimit;
    }

    /**
     * Sets the value of the liabilityLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setLiabilityLimit(GLogXMLFinancialAmountType value) {
        this.liabilityLimit = value;
    }

    /**
     * Gets the value of the remark property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the remark property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRemark().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RemarkType }
     * 
     * 
     */
    public List<RemarkType> getRemark() {
        if (remark == null) {
            remark = new ArrayList<RemarkType>();
        }
        return this.remark;
    }

    /**
     * Gets the value of the refnum property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the refnum property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefnum().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefnumType }
     * 
     * 
     */
    public List<RefnumType> getRefnum() {
        if (refnum == null) {
            refnum = new ArrayList<RefnumType>();
        }
        return this.refnum;
    }

    /**
     * Gets the value of the involvedParty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedParty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedParty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GtmInvolvedPartyType }
     * 
     * 
     */
    public List<GtmInvolvedPartyType> getInvolvedParty() {
        if (involvedParty == null) {
            involvedParty = new ArrayList<GtmInvolvedPartyType>();
        }
        return this.involvedParty;
    }

    /**
     * Gets the value of the portInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the portInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPortInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PortInfoType }
     * 
     * 
     */
    public List<PortInfoType> getPortInfo() {
        if (portInfo == null) {
            portInfo = new ArrayList<PortInfoType>();
        }
        return this.portInfo;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Deprecated (6.4.3): This element won't be populated from 6.4 onwards and will be removed in a future release.
 *             (Outbound) Base amounts calulated and assigned to the business transaction.
 *          
 * 
 * <p>Java class for CalculatedValueResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CalculatedValueResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CalculatedValueGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="RuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CalculatedValueResultType", propOrder = {
    "calculatedValueGid",
    "complianceRuleGid",
    "ruleGroupGid"
})
public class CalculatedValueResultType {

    @XmlElement(name = "CalculatedValueGid", required = true)
    protected GLogXMLGidType calculatedValueGid;
    @XmlElement(name = "ComplianceRuleGid", required = true)
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "RuleGroupGid", required = true)
    protected GLogXMLGidType ruleGroupGid;

    /**
     * Gets the value of the calculatedValueGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getCalculatedValueGid() {
        return calculatedValueGid;
    }

    /**
     * Sets the value of the calculatedValueGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setCalculatedValueGid(GLogXMLGidType value) {
        this.calculatedValueGid = value;
    }

    /**
     * Gets the value of the complianceRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Sets the value of the complianceRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Gets the value of the ruleGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRuleGroupGid() {
        return ruleGroupGid;
    }

    /**
     * Sets the value of the ruleGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRuleGroupGid(GLogXMLGidType value) {
        this.ruleGroupGid = value;
    }

}

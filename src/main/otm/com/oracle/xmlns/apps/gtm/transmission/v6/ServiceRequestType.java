
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.OTMTransactionIn;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             Request/Reply style interface request structure.
 *          
 * 
 * <p>Java class for ServiceRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServiceRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}OTMTransactionIn"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ServiceName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="RefId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Attribute" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}AttributeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="GtmServicePreferenceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;choice&gt;
 *           &lt;element name="RestrictedParty" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RestrictedPartyType"/&gt;
 *           &lt;element name="SanctionedTerritory" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}SanctionedTerritoryType"/&gt;
 *           &lt;element name="Classification" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ClassificationType"/&gt;
 *           &lt;element name="ComplianceRule" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}ComplianceRuleType"/&gt;
 *           &lt;element name="LicenseDetermination" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}LicenseDeterminationType"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServiceRequestType", propOrder = {
    "serviceName",
    "refId",
    "source",
    "attribute",
    "gtmServicePreferenceId",
    "restrictedParty",
    "sanctionedTerritory",
    "classification",
    "complianceRule",
    "licenseDetermination"
})
public class ServiceRequestType
    extends OTMTransactionIn
{

    @XmlElement(name = "ServiceName", required = true)
    protected String serviceName;
    @XmlElement(name = "RefId")
    protected String refId;
    @XmlElement(name = "Source")
    protected String source;
    @XmlElement(name = "Attribute")
    protected List<AttributeType> attribute;
    @XmlElement(name = "GtmServicePreferenceId")
    protected String gtmServicePreferenceId;
    @XmlElement(name = "RestrictedParty")
    protected RestrictedPartyType restrictedParty;
    @XmlElement(name = "SanctionedTerritory")
    protected SanctionedTerritoryType sanctionedTerritory;
    @XmlElement(name = "Classification")
    protected ClassificationType classification;
    @XmlElement(name = "ComplianceRule")
    protected ComplianceRuleType complianceRule;
    @XmlElement(name = "LicenseDetermination")
    protected LicenseDeterminationType licenseDetermination;

    /**
     * Gets the value of the serviceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the value of the serviceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceName(String value) {
        this.serviceName = value;
    }

    /**
     * Gets the value of the refId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefId() {
        return refId;
    }

    /**
     * Sets the value of the refId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefId(String value) {
        this.refId = value;
    }

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSource(String value) {
        this.source = value;
    }

    /**
     * Gets the value of the attribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the attribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttributeType }
     * 
     * 
     */
    public List<AttributeType> getAttribute() {
        if (attribute == null) {
            attribute = new ArrayList<AttributeType>();
        }
        return this.attribute;
    }

    /**
     * Gets the value of the gtmServicePreferenceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmServicePreferenceId() {
        return gtmServicePreferenceId;
    }

    /**
     * Sets the value of the gtmServicePreferenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmServicePreferenceId(String value) {
        this.gtmServicePreferenceId = value;
    }

    /**
     * Gets the value of the restrictedParty property.
     * 
     * @return
     *     possible object is
     *     {@link RestrictedPartyType }
     *     
     */
    public RestrictedPartyType getRestrictedParty() {
        return restrictedParty;
    }

    /**
     * Sets the value of the restrictedParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link RestrictedPartyType }
     *     
     */
    public void setRestrictedParty(RestrictedPartyType value) {
        this.restrictedParty = value;
    }

    /**
     * Gets the value of the sanctionedTerritory property.
     * 
     * @return
     *     possible object is
     *     {@link SanctionedTerritoryType }
     *     
     */
    public SanctionedTerritoryType getSanctionedTerritory() {
        return sanctionedTerritory;
    }

    /**
     * Sets the value of the sanctionedTerritory property.
     * 
     * @param value
     *     allowed object is
     *     {@link SanctionedTerritoryType }
     *     
     */
    public void setSanctionedTerritory(SanctionedTerritoryType value) {
        this.sanctionedTerritory = value;
    }

    /**
     * Gets the value of the classification property.
     * 
     * @return
     *     possible object is
     *     {@link ClassificationType }
     *     
     */
    public ClassificationType getClassification() {
        return classification;
    }

    /**
     * Sets the value of the classification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClassificationType }
     *     
     */
    public void setClassification(ClassificationType value) {
        this.classification = value;
    }

    /**
     * Gets the value of the complianceRule property.
     * 
     * @return
     *     possible object is
     *     {@link ComplianceRuleType }
     *     
     */
    public ComplianceRuleType getComplianceRule() {
        return complianceRule;
    }

    /**
     * Sets the value of the complianceRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link ComplianceRuleType }
     *     
     */
    public void setComplianceRule(ComplianceRuleType value) {
        this.complianceRule = value;
    }

    /**
     * Gets the value of the licenseDetermination property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseDeterminationType }
     *     
     */
    public LicenseDeterminationType getLicenseDetermination() {
        return licenseDetermination;
    }

    /**
     * Sets the value of the licenseDetermination property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseDeterminationType }
     *     
     */
    public void setLicenseDetermination(LicenseDeterminationType value) {
        this.licenseDetermination = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogDateTimeType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) Trade control screening result.
 *          
 * 
 * <p>Java class for ControlScreeningResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ControlScreeningResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="GtmregulationRefGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="GtmExceptionControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="GtmExceptionControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AuthorizationTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LicenseQuantity" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LicenseUom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LicenseAddedBy" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="LicenseAddedOn" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="InvolvedPartyQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ControlCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegimeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlScreeningResultType", propOrder = {
    "gtmControlTypeGid",
    "controlCode",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "gtmregulationRefGid",
    "gtmExceptionControlTypeGid",
    "gtmExceptionControlCode",
    "authorizationTypeGid",
    "authorization",
    "licenseQuantity",
    "licenseUom",
    "licenseAddedBy",
    "licenseAddedOn",
    "involvedPartyQualifierGid",
    "controlCodeDescription",
    "regimeId"
})
public class ControlScreeningResultType {

    @XmlElement(name = "GtmControlTypeGid", required = true)
    protected GLogXMLGidType gtmControlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ComplianceRuleGid", required = true)
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid", required = true)
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "GtmregulationRefGid", required = true)
    protected GLogXMLGidType gtmregulationRefGid;
    @XmlElement(name = "GtmExceptionControlTypeGid", required = true)
    protected GLogXMLGidType gtmExceptionControlTypeGid;
    @XmlElement(name = "GtmExceptionControlCode", required = true)
    protected String gtmExceptionControlCode;
    @XmlElement(name = "AuthorizationTypeGid")
    protected GLogXMLGidType authorizationTypeGid;
    @XmlElement(name = "Authorization", required = true)
    protected String authorization;
    @XmlElement(name = "LicenseQuantity", required = true)
    protected String licenseQuantity;
    @XmlElement(name = "LicenseUom", required = true)
    protected String licenseUom;
    @XmlElement(name = "LicenseAddedBy", required = true)
    protected String licenseAddedBy;
    @XmlElement(name = "LicenseAddedOn", required = true)
    protected GLogDateTimeType licenseAddedOn;
    @XmlElement(name = "InvolvedPartyQualifierGid")
    protected GLogXMLGidType involvedPartyQualifierGid;
    @XmlElement(name = "ControlCodeDescription")
    protected String controlCodeDescription;
    @XmlElement(name = "RegimeId")
    protected String regimeId;

    /**
     * Gets the value of the gtmControlTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmControlTypeGid() {
        return gtmControlTypeGid;
    }

    /**
     * Sets the value of the gtmControlTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmControlTypeGid(GLogXMLGidType value) {
        this.gtmControlTypeGid = value;
    }

    /**
     * Gets the value of the controlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Sets the value of the controlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Gets the value of the complianceRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Sets the value of the complianceRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Gets the value of the complianceRuleGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Sets the value of the complianceRuleGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Gets the value of the gtmregulationRefGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmregulationRefGid() {
        return gtmregulationRefGid;
    }

    /**
     * Sets the value of the gtmregulationRefGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmregulationRefGid(GLogXMLGidType value) {
        this.gtmregulationRefGid = value;
    }

    /**
     * Gets the value of the gtmExceptionControlTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmExceptionControlTypeGid() {
        return gtmExceptionControlTypeGid;
    }

    /**
     * Sets the value of the gtmExceptionControlTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmExceptionControlTypeGid(GLogXMLGidType value) {
        this.gtmExceptionControlTypeGid = value;
    }

    /**
     * Gets the value of the gtmExceptionControlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmExceptionControlCode() {
        return gtmExceptionControlCode;
    }

    /**
     * Sets the value of the gtmExceptionControlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmExceptionControlCode(String value) {
        this.gtmExceptionControlCode = value;
    }

    /**
     * Gets the value of the authorizationTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getAuthorizationTypeGid() {
        return authorizationTypeGid;
    }

    /**
     * Sets the value of the authorizationTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setAuthorizationTypeGid(GLogXMLGidType value) {
        this.authorizationTypeGid = value;
    }

    /**
     * Gets the value of the authorization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Sets the value of the authorization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

    /**
     * Gets the value of the licenseQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseQuantity() {
        return licenseQuantity;
    }

    /**
     * Sets the value of the licenseQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseQuantity(String value) {
        this.licenseQuantity = value;
    }

    /**
     * Gets the value of the licenseUom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseUom() {
        return licenseUom;
    }

    /**
     * Sets the value of the licenseUom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseUom(String value) {
        this.licenseUom = value;
    }

    /**
     * Gets the value of the licenseAddedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseAddedBy() {
        return licenseAddedBy;
    }

    /**
     * Sets the value of the licenseAddedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseAddedBy(String value) {
        this.licenseAddedBy = value;
    }

    /**
     * Gets the value of the licenseAddedOn property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getLicenseAddedOn() {
        return licenseAddedOn;
    }

    /**
     * Sets the value of the licenseAddedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setLicenseAddedOn(GLogDateTimeType value) {
        this.licenseAddedOn = value;
    }

    /**
     * Gets the value of the involvedPartyQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getInvolvedPartyQualifierGid() {
        return involvedPartyQualifierGid;
    }

    /**
     * Sets the value of the involvedPartyQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setInvolvedPartyQualifierGid(GLogXMLGidType value) {
        this.involvedPartyQualifierGid = value;
    }

    /**
     * Gets the value of the controlCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCodeDescription() {
        return controlCodeDescription;
    }

    /**
     * Sets the value of the controlCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCodeDescription(String value) {
        this.controlCodeDescription = value;
    }

    /**
     * Gets the value of the regimeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeId() {
        return regimeId;
    }

    /**
     * Sets the value of the regimeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeId(String value) {
        this.regimeId = value;
    }

}

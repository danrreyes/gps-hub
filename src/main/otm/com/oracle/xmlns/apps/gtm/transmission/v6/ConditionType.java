
package com.oracle.xmlns.apps.gtm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Inbound) This is not used any more.
 * 
 * <p>Java class for ConditionType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConditionType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ConditionName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ConditionValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConditionType", propOrder = {
    "conditionName",
    "conditionValue"
})
public class ConditionType {

    @XmlElement(name = "ConditionName", required = true)
    protected String conditionName;
    @XmlElement(name = "ConditionValue", required = true)
    protected String conditionValue;

    /**
     * Gets the value of the conditionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionName() {
        return conditionName;
    }

    /**
     * Sets the value of the conditionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionName(String value) {
        this.conditionName = value;
    }

    /**
     * Gets the value of the conditionValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionValue() {
        return conditionValue;
    }

    /**
     * Sets the value of the conditionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionValue(String value) {
        this.conditionValue = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) ComplianceRuleResponse is response for the compliance screening triggered via ServiceRequest integration interface.
 *          
 * 
 * <p>Java class for ComplianceRuleResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ComplianceRuleResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RuleControl" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}RuleControlType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ComplianceRuleResponseType", propOrder = {
    "ruleControl"
})
public class ComplianceRuleResponseType {

    @XmlElement(name = "RuleControl", required = true)
    protected RuleControlType ruleControl;

    /**
     * Gets the value of the ruleControl property.
     * 
     * @return
     *     possible object is
     *     {@link RuleControlType }
     *     
     */
    public RuleControlType getRuleControl() {
        return ruleControl;
    }

    /**
     * Sets the value of the ruleControl property.
     * 
     * @param value
     *     allowed object is
     *     {@link RuleControlType }
     *     
     */
    public void setRuleControl(RuleControlType value) {
        this.ruleControl = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogDateTimeType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) Sanction control screening result.
 *          
 * 
 * <p>Java class for SanctionControlScreeningResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SanctionControlScreeningResultType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmControlTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ControlCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ComplianceRuleGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="ComplianceRuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="Authorization" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AuthorizationAddedBy" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AuthorizationAddedOn" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType"/&gt;
 *         &lt;element name="ControlCodeDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RegimeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SanctionControlScreeningResultType", propOrder = {
    "gtmControlTypeGid",
    "controlCode",
    "complianceRuleGid",
    "complianceRuleGroupGid",
    "authorization",
    "authorizationAddedBy",
    "authorizationAddedOn",
    "controlCodeDescription",
    "regimeId"
})
public class SanctionControlScreeningResultType {

    @XmlElement(name = "GtmControlTypeGid", required = true)
    protected GLogXMLGidType gtmControlTypeGid;
    @XmlElement(name = "ControlCode", required = true)
    protected String controlCode;
    @XmlElement(name = "ComplianceRuleGid", required = true)
    protected GLogXMLGidType complianceRuleGid;
    @XmlElement(name = "ComplianceRuleGroupGid", required = true)
    protected GLogXMLGidType complianceRuleGroupGid;
    @XmlElement(name = "Authorization", required = true)
    protected String authorization;
    @XmlElement(name = "AuthorizationAddedBy", required = true)
    protected String authorizationAddedBy;
    @XmlElement(name = "AuthorizationAddedOn", required = true)
    protected GLogDateTimeType authorizationAddedOn;
    @XmlElement(name = "ControlCodeDescription")
    protected String controlCodeDescription;
    @XmlElement(name = "RegimeId")
    protected String regimeId;

    /**
     * Gets the value of the gtmControlTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmControlTypeGid() {
        return gtmControlTypeGid;
    }

    /**
     * Sets the value of the gtmControlTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmControlTypeGid(GLogXMLGidType value) {
        this.gtmControlTypeGid = value;
    }

    /**
     * Gets the value of the controlCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCode() {
        return controlCode;
    }

    /**
     * Sets the value of the controlCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCode(String value) {
        this.controlCode = value;
    }

    /**
     * Gets the value of the complianceRuleGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGid() {
        return complianceRuleGid;
    }

    /**
     * Sets the value of the complianceRuleGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGid(GLogXMLGidType value) {
        this.complianceRuleGid = value;
    }

    /**
     * Gets the value of the complianceRuleGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getComplianceRuleGroupGid() {
        return complianceRuleGroupGid;
    }

    /**
     * Sets the value of the complianceRuleGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setComplianceRuleGroupGid(GLogXMLGidType value) {
        this.complianceRuleGroupGid = value;
    }

    /**
     * Gets the value of the authorization property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Sets the value of the authorization property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

    /**
     * Gets the value of the authorizationAddedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationAddedBy() {
        return authorizationAddedBy;
    }

    /**
     * Sets the value of the authorizationAddedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationAddedBy(String value) {
        this.authorizationAddedBy = value;
    }

    /**
     * Gets the value of the authorizationAddedOn property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getAuthorizationAddedOn() {
        return authorizationAddedOn;
    }

    /**
     * Sets the value of the authorizationAddedOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setAuthorizationAddedOn(GLogDateTimeType value) {
        this.authorizationAddedOn = value;
    }

    /**
     * Gets the value of the controlCodeDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getControlCodeDescription() {
        return controlCodeDescription;
    }

    /**
     * Sets the value of the controlCodeDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setControlCodeDescription(String value) {
        this.controlCodeDescription = value;
    }

    /**
     * Gets the value of the regimeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegimeId() {
        return regimeId;
    }

    /**
     * Sets the value of the regimeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegimeId(String value) {
        this.regimeId = value;
    }

}

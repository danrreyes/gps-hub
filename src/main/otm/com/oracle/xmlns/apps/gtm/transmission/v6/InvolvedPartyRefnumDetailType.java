
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Inbound) Reference numbers for the party involved
 *          
 * 
 * <p>Java class for InvolvedPartyRefnumDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyRefnumDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PartyQualifierGidForRefnum" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PartyRefnumQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PartyRefnumValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyRefnumDetailType", propOrder = {
    "partyQualifierGidForRefnum",
    "partyRefnumQualifierGid",
    "partyRefnumValue"
})
public class InvolvedPartyRefnumDetailType {

    @XmlElement(name = "PartyQualifierGidForRefnum", required = true)
    protected GLogXMLGidType partyQualifierGidForRefnum;
    @XmlElement(name = "PartyRefnumQualifierGid", required = true)
    protected GLogXMLGidType partyRefnumQualifierGid;
    @XmlElement(name = "PartyRefnumValue", required = true)
    protected String partyRefnumValue;

    /**
     * Gets the value of the partyQualifierGidForRefnum property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyQualifierGidForRefnum() {
        return partyQualifierGidForRefnum;
    }

    /**
     * Sets the value of the partyQualifierGidForRefnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyQualifierGidForRefnum(GLogXMLGidType value) {
        this.partyQualifierGidForRefnum = value;
    }

    /**
     * Gets the value of the partyRefnumQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPartyRefnumQualifierGid() {
        return partyRefnumQualifierGid;
    }

    /**
     * Sets the value of the partyRefnumQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPartyRefnumQualifierGid(GLogXMLGidType value) {
        this.partyRefnumQualifierGid = value;
    }

    /**
     * Gets the value of the partyRefnumValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartyRefnumValue() {
        return partyRefnumValue;
    }

    /**
     * Sets the value of the partyRefnumValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartyRefnumValue(String value) {
        this.partyRefnumValue = value;
    }

}

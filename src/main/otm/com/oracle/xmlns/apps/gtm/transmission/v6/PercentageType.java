
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) Percentage information for an object. It consists of a qualifier and a value.
 *          
 * 
 * <p>Java class for PercentageType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PercentageType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="PercentageQualifierGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="PercentageValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PercentageType", propOrder = {
    "percentageQualifierGid",
    "percentageValue"
})
public class PercentageType {

    @XmlElement(name = "PercentageQualifierGid", required = true)
    protected GLogXMLGidType percentageQualifierGid;
    @XmlElement(name = "PercentageValue", required = true)
    protected String percentageValue;

    /**
     * Gets the value of the percentageQualifierGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPercentageQualifierGid() {
        return percentageQualifierGid;
    }

    /**
     * Sets the value of the percentageQualifierGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPercentageQualifierGid(GLogXMLGidType value) {
        this.percentageQualifierGid = value;
    }

    /**
     * Gets the value of the percentageValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentageValue() {
        return percentageValue;
    }

    /**
     * Sets the value of the percentageValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentageValue(String value) {
        this.percentageValue = value;
    }

}

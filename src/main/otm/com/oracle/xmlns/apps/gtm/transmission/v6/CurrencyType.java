
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.ExchangeRateInfoType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLFinancialAmountType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) Currency on the business object.
 *          
 * 
 * <p>Java class for CurrencyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CurrencyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmCurrencyTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="CurrencyValue" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLFinancialAmountType"/&gt;
 *         &lt;element name="GtmFormulaExpressionGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ValueNote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MethodOfCalculation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="IsFixed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PaymentMethodGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="ReportingCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ReportingMonetaryAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RateToReporting" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ExchangeRateInfo" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}ExchangeRateInfoType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CurrencyType", propOrder = {
    "gtmCurrencyTypeGid",
    "currencyValue",
    "gtmFormulaExpressionGid",
    "valueNote",
    "methodOfCalculation",
    "isFixed",
    "paymentMethodGid",
    "reportingCurrencyCode",
    "reportingMonetaryAmount",
    "rateToReporting",
    "exchangeRateInfo"
})
public class CurrencyType {

    @XmlElement(name = "GtmCurrencyTypeGid", required = true)
    protected GLogXMLGidType gtmCurrencyTypeGid;
    @XmlElement(name = "CurrencyValue", required = true)
    protected GLogXMLFinancialAmountType currencyValue;
    @XmlElement(name = "GtmFormulaExpressionGid")
    protected GLogXMLGidType gtmFormulaExpressionGid;
    @XmlElement(name = "ValueNote")
    protected String valueNote;
    @XmlElement(name = "MethodOfCalculation")
    protected String methodOfCalculation;
    @XmlElement(name = "IsFixed")
    protected String isFixed;
    @XmlElement(name = "PaymentMethodGid")
    protected GLogXMLGidType paymentMethodGid;
    @XmlElement(name = "ReportingCurrencyCode")
    protected String reportingCurrencyCode;
    @XmlElement(name = "ReportingMonetaryAmount")
    protected String reportingMonetaryAmount;
    @XmlElement(name = "RateToReporting")
    protected String rateToReporting;
    @XmlElement(name = "ExchangeRateInfo")
    protected ExchangeRateInfoType exchangeRateInfo;

    /**
     * Gets the value of the gtmCurrencyTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmCurrencyTypeGid() {
        return gtmCurrencyTypeGid;
    }

    /**
     * Sets the value of the gtmCurrencyTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmCurrencyTypeGid(GLogXMLGidType value) {
        this.gtmCurrencyTypeGid = value;
    }

    /**
     * Gets the value of the currencyValue property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public GLogXMLFinancialAmountType getCurrencyValue() {
        return currencyValue;
    }

    /**
     * Sets the value of the currencyValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLFinancialAmountType }
     *     
     */
    public void setCurrencyValue(GLogXMLFinancialAmountType value) {
        this.currencyValue = value;
    }

    /**
     * Gets the value of the gtmFormulaExpressionGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmFormulaExpressionGid() {
        return gtmFormulaExpressionGid;
    }

    /**
     * Sets the value of the gtmFormulaExpressionGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmFormulaExpressionGid(GLogXMLGidType value) {
        this.gtmFormulaExpressionGid = value;
    }

    /**
     * Gets the value of the valueNote property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueNote() {
        return valueNote;
    }

    /**
     * Sets the value of the valueNote property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueNote(String value) {
        this.valueNote = value;
    }

    /**
     * Gets the value of the methodOfCalculation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMethodOfCalculation() {
        return methodOfCalculation;
    }

    /**
     * Sets the value of the methodOfCalculation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMethodOfCalculation(String value) {
        this.methodOfCalculation = value;
    }

    /**
     * Gets the value of the isFixed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsFixed() {
        return isFixed;
    }

    /**
     * Sets the value of the isFixed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsFixed(String value) {
        this.isFixed = value;
    }

    /**
     * Gets the value of the paymentMethodGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getPaymentMethodGid() {
        return paymentMethodGid;
    }

    /**
     * Sets the value of the paymentMethodGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setPaymentMethodGid(GLogXMLGidType value) {
        this.paymentMethodGid = value;
    }

    /**
     * Gets the value of the reportingCurrencyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingCurrencyCode() {
        return reportingCurrencyCode;
    }

    /**
     * Sets the value of the reportingCurrencyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingCurrencyCode(String value) {
        this.reportingCurrencyCode = value;
    }

    /**
     * Gets the value of the reportingMonetaryAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportingMonetaryAmount() {
        return reportingMonetaryAmount;
    }

    /**
     * Sets the value of the reportingMonetaryAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportingMonetaryAmount(String value) {
        this.reportingMonetaryAmount = value;
    }

    /**
     * Gets the value of the rateToReporting property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateToReporting() {
        return rateToReporting;
    }

    /**
     * Sets the value of the rateToReporting property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateToReporting(String value) {
        this.rateToReporting = value;
    }

    /**
     * Gets the value of the exchangeRateInfo property.
     * 
     * @return
     *     possible object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public ExchangeRateInfoType getExchangeRateInfo() {
        return exchangeRateInfo;
    }

    /**
     * Sets the value of the exchangeRateInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExchangeRateInfoType }
     *     
     */
    public void setExchangeRateInfo(ExchangeRateInfoType value) {
        this.exchangeRateInfo = value;
    }

}

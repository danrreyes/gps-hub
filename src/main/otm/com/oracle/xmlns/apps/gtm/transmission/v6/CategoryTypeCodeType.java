
package com.oracle.xmlns.apps.gtm.transmission.v6;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) This represents user defined type and code for a business object
 *          
 * 
 * <p>Java class for CategoryTypeCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CategoryTypeCodeType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="GtmType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="GtmCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CategoryTypeCodeType", propOrder = {
    "gtmCategory",
    "gtmType",
    "gtmCode"
})
public class CategoryTypeCodeType {

    @XmlElement(name = "GtmCategory")
    protected String gtmCategory;
    @XmlElement(name = "GtmType", required = true)
    protected String gtmType;
    @XmlElement(name = "GtmCode", required = true)
    protected String gtmCode;

    /**
     * Gets the value of the gtmCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmCategory() {
        return gtmCategory;
    }

    /**
     * Sets the value of the gtmCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmCategory(String value) {
        this.gtmCategory = value;
    }

    /**
     * Gets the value of the gtmType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmType() {
        return gtmType;
    }

    /**
     * Sets the value of the gtmType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmType(String value) {
        this.gtmType = value;
    }

    /**
     * Gets the value of the gtmCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGtmCode() {
        return gtmCode;
    }

    /**
     * Sets the value of the gtmCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGtmCode(String value) {
        this.gtmCode = value;
    }

}

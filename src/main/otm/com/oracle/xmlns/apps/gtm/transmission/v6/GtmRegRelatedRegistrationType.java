
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) Registrations related to another Registration.
 *          
 * 
 * <p>Java class for GtmRegRelatedRegistrationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GtmRegRelatedRegistrationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="GtmRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="GtmRelatedRegistrationGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GtmRegRelatedRegistrationType", propOrder = {
    "gtmRegistrationGid",
    "gtmRelatedRegistrationGid"
})
public class GtmRegRelatedRegistrationType {

    @XmlElement(name = "GtmRegistrationGid")
    protected GLogXMLGidType gtmRegistrationGid;
    @XmlElement(name = "GtmRelatedRegistrationGid", required = true)
    protected GLogXMLGidType gtmRelatedRegistrationGid;

    /**
     * Gets the value of the gtmRegistrationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRegistrationGid() {
        return gtmRegistrationGid;
    }

    /**
     * Sets the value of the gtmRegistrationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRegistrationGid(GLogXMLGidType value) {
        this.gtmRegistrationGid = value;
    }

    /**
     * Gets the value of the gtmRelatedRegistrationGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmRelatedRegistrationGid() {
        return gtmRelatedRegistrationGid;
    }

    /**
     * Sets the value of the gtmRelatedRegistrationGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmRelatedRegistrationGid(GLogXMLGidType value) {
        this.gtmRelatedRegistrationGid = value;
    }

}

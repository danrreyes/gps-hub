
package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Both) This tag will hold details of party involved.
 *          
 * 
 * <p>Java class for InvolvedPartyDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvolvedPartyDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InvolvedPartyQualGid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ContactGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvolvedPartyDetailType", propOrder = {
    "involvedPartyQualGid",
    "contactGid"
})
public class InvolvedPartyDetailType {

    @XmlElement(name = "InvolvedPartyQualGid", required = true)
    protected String involvedPartyQualGid;
    @XmlElement(name = "ContactGid", required = true)
    protected GLogXMLGidType contactGid;

    /**
     * Gets the value of the involvedPartyQualGid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvolvedPartyQualGid() {
        return involvedPartyQualGid;
    }

    /**
     * Sets the value of the involvedPartyQualGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvolvedPartyQualGid(String value) {
        this.involvedPartyQualGid = value;
    }

    /**
     * Gets the value of the contactGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getContactGid() {
        return contactGid;
    }

    /**
     * Sets the value of the contactGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setContactGid(GLogXMLGidType value) {
        this.contactGid = value;
    }

}


package com.oracle.xmlns.apps.gtm.transmission.v6;

import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * (Outbound) Reporting Quantity represents the quantity in external unit of measure as needed by different regimes.
 *          
 * 
 * <p>Java class for ReportingQuantityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReportingQuantityType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="SequenceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="GtmProdClassTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="QuantityTypeGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType"/&gt;
 *         &lt;element name="QuantityValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AttrName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UOM" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportingQuantityType", propOrder = {
    "sequenceNumber",
    "gtmProdClassTypeGid",
    "quantityTypeGid",
    "quantityValue",
    "attrName",
    "uom"
})
public class ReportingQuantityType {

    @XmlElement(name = "SequenceNumber", required = true)
    protected String sequenceNumber;
    @XmlElement(name = "GtmProdClassTypeGid", required = true)
    protected GLogXMLGidType gtmProdClassTypeGid;
    @XmlElement(name = "QuantityTypeGid", required = true)
    protected GLogXMLGidType quantityTypeGid;
    @XmlElement(name = "QuantityValue", required = true)
    protected String quantityValue;
    @XmlElement(name = "AttrName", required = true)
    protected String attrName;
    @XmlElement(name = "UOM", required = true)
    protected String uom;

    /**
     * Gets the value of the sequenceNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the value of the sequenceNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceNumber(String value) {
        this.sequenceNumber = value;
    }

    /**
     * Gets the value of the gtmProdClassTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getGtmProdClassTypeGid() {
        return gtmProdClassTypeGid;
    }

    /**
     * Sets the value of the gtmProdClassTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setGtmProdClassTypeGid(GLogXMLGidType value) {
        this.gtmProdClassTypeGid = value;
    }

    /**
     * Gets the value of the quantityTypeGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getQuantityTypeGid() {
        return quantityTypeGid;
    }

    /**
     * Sets the value of the quantityTypeGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setQuantityTypeGid(GLogXMLGidType value) {
        this.quantityTypeGid = value;
    }

    /**
     * Gets the value of the quantityValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantityValue() {
        return quantityValue;
    }

    /**
     * Sets the value of the quantityValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantityValue(String value) {
        this.quantityValue = value;
    }

    /**
     * Gets the value of the attrName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttrName() {
        return attrName;
    }

    /**
     * Sets the value of the attrName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttrName(String value) {
        this.attrName = value;
    }

    /**
     * Gets the value of the uom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUOM() {
        return uom;
    }

    /**
     * Sets the value of the uom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUOM(String value) {
        this.uom = value;
    }

}

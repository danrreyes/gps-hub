
package com.oracle.xmlns.apps.gtm.transmission.v6;

import java.util.ArrayList;
import java.util.List;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogDateTimeType;
import com.oracle.xmlns.apps.otm.transmission.v6.GLogXMLGidType;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;


/**
 * 
 *             (Inbound) SanctionedTerritory is the embargo screening request triggered via ServiceRequest integration interface.
 *          
 * 
 * <p>Java class for SanctionedTerritoryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SanctionedTerritoryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RuleGroupGid" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogXMLGidType" minOccurs="0"/&gt;
 *         &lt;element name="LocationInfo" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}LocationInfoType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="TransactionDate" type="{http://xmlns.oracle.com/apps/otm/transmission/v6.4}GLogDateTimeType" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedPartyRefnumDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyRefnumDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="InvolvedPartyRemarkDetail" type="{http://xmlns.oracle.com/apps/gtm/transmission/v6.4}InvolvedPartyRemarkDetailType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SanctionedTerritoryType", propOrder = {
    "ruleGroupGid",
    "locationInfo",
    "transactionDate",
    "involvedPartyRefnumDetail",
    "involvedPartyRemarkDetail"
})
public class SanctionedTerritoryType {

    @XmlElement(name = "RuleGroupGid")
    protected GLogXMLGidType ruleGroupGid;
    @XmlElement(name = "LocationInfo")
    protected List<LocationInfoType> locationInfo;
    @XmlElement(name = "TransactionDate")
    protected GLogDateTimeType transactionDate;
    @XmlElement(name = "InvolvedPartyRefnumDetail")
    protected List<InvolvedPartyRefnumDetailType> involvedPartyRefnumDetail;
    @XmlElement(name = "InvolvedPartyRemarkDetail")
    protected List<InvolvedPartyRemarkDetailType> involvedPartyRemarkDetail;

    /**
     * Gets the value of the ruleGroupGid property.
     * 
     * @return
     *     possible object is
     *     {@link GLogXMLGidType }
     *     
     */
    public GLogXMLGidType getRuleGroupGid() {
        return ruleGroupGid;
    }

    /**
     * Sets the value of the ruleGroupGid property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogXMLGidType }
     *     
     */
    public void setRuleGroupGid(GLogXMLGidType value) {
        this.ruleGroupGid = value;
    }

    /**
     * Gets the value of the locationInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the locationInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLocationInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LocationInfoType }
     * 
     * 
     */
    public List<LocationInfoType> getLocationInfo() {
        if (locationInfo == null) {
            locationInfo = new ArrayList<LocationInfoType>();
        }
        return this.locationInfo;
    }

    /**
     * Gets the value of the transactionDate property.
     * 
     * @return
     *     possible object is
     *     {@link GLogDateTimeType }
     *     
     */
    public GLogDateTimeType getTransactionDate() {
        return transactionDate;
    }

    /**
     * Sets the value of the transactionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLogDateTimeType }
     *     
     */
    public void setTransactionDate(GLogDateTimeType value) {
        this.transactionDate = value;
    }

    /**
     * Gets the value of the involvedPartyRefnumDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyRefnumDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyRefnumDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyRefnumDetailType }
     * 
     * 
     */
    public List<InvolvedPartyRefnumDetailType> getInvolvedPartyRefnumDetail() {
        if (involvedPartyRefnumDetail == null) {
            involvedPartyRefnumDetail = new ArrayList<InvolvedPartyRefnumDetailType>();
        }
        return this.involvedPartyRefnumDetail;
    }

    /**
     * Gets the value of the involvedPartyRemarkDetail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the Jakarta XML Binding object.
     * This is why there is not a <CODE>set</CODE> method for the involvedPartyRemarkDetail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvolvedPartyRemarkDetail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvolvedPartyRemarkDetailType }
     * 
     * 
     */
    public List<InvolvedPartyRemarkDetailType> getInvolvedPartyRemarkDetail() {
        if (involvedPartyRemarkDetail == null) {
            involvedPartyRemarkDetail = new ArrayList<InvolvedPartyRemarkDetailType>();
        }
        return this.involvedPartyRemarkDetail;
    }

}

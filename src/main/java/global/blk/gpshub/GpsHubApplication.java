package global.blk.gpshub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:application.properties")
@PropertySource("file:/u01/data/blk/aplicaciones/config_app/gpshubws/application.properties")
public class GpsHubApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpsHubApplication.class, args);
    }

}

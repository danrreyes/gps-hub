package global.blk.gpshub.util.soap;

import jakarta.xml.ws.handler.Handler;
import jakarta.xml.ws.handler.HandlerResolver;
import jakarta.xml.ws.handler.PortInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class HeaderHandlerResolver implements HandlerResolver {

    private Logger LOGGER = LoggerFactory.getLogger(HeaderHandlerResolver.class);

    @Override
    public List<Handler> getHandlerChain(PortInfo portInfo) {
        List<Handler> handlerChain = new ArrayList<Handler>();
        HeaderHandlerWsse hh = new HeaderHandlerWsse();
        String nServerQname = portInfo.getServiceName().toString();
        String nameServer = nServerQname.substring(nServerQname.indexOf('}') + 1);
        //if ()
        handlerChain.add(hh);
        return handlerChain;
    }

}

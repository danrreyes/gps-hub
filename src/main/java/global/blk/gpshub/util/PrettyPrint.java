package global.blk.gpshub.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;

public class PrettyPrint {

    public static String prettyPrintJson(String json){
        ObjectMapper objectMapper= new ObjectMapper();
        String pPJ = "";

        try {
            Object object = objectMapper.readValue(json, Object.class);
            pPJ = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return pPJ;
    }

    public static String prettyPrintXml(String xml){
        String pPX = "";

        try {
            Source xmlInput = new StreamSource(new StringReader(xml));
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", 2);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(xmlInput, xmlOutput);
            pPX = xmlOutput.getWriter().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pPX;
    }

}

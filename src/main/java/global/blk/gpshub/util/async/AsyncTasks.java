package global.blk.gpshub.util.async;

import com.fasterxml.jackson.databind.JsonNode;
import com.oracle.xmlns.apps.otm.transmission.v6.*;
import com.oracle.xmlns.apps.otm.transmissionservice.TransmissionPortType;
import com.oracle.xmlns.apps.otm.transmissionservice.TransmissionService;
import global.blk.gpshub.config.OtmConfig;
import global.blk.gpshub.model.EventEntry;
import global.blk.gpshub.model.PlannedShipmentEvent;
import global.blk.gpshub.repository.EventEntryRepository;
import global.blk.gpshub.repository.PlannedShipmentEventRepository;
import global.blk.gpshub.repository.PlannedShipmentInfoRepository;
import global.blk.gpshub.repository.ShipmentStatusRepository;
import global.blk.gpshub.service.GpsHubService;
import global.blk.gpshub.util.PrettyPrint;
import global.blk.gpshub.util.soap.HeaderHandlerResolver;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.ws.BindingProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class AsyncTasks {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncTasks.class);
    /* OTM */
    private PlannedShipmentInfoRepository plannedShipmentInfoRepository;
    private PlannedShipmentEventRepository plannedShipmentEventRepository;
    private GpsHubService gpsHubService;
    /* FC */
    public String strAddr = "";
    private ShipmentStatusRepository shipmentStatusRepository;
    private EventEntryRepository eventEntryRepository;
    private OtmConfig otmConfig;
    @Autowired
    private Environment env;

    public AsyncTasks(PlannedShipmentInfoRepository plannedShipmentInfoRepository, PlannedShipmentEventRepository plannedShipmentEventRepository, GpsHubService gpsHubService, ShipmentStatusRepository shipmentStatusRepository, EventEntryRepository eventEntryRepository, OtmConfig otmConfig) {
        this.plannedShipmentInfoRepository = plannedShipmentInfoRepository;
        this.plannedShipmentEventRepository = plannedShipmentEventRepository;
        this.gpsHubService = gpsHubService;
        this.shipmentStatusRepository = shipmentStatusRepository;
        this.eventEntryRepository = eventEntryRepository;
        this.otmConfig = otmConfig;
    }

    @Async("processExecutor")
    public void processOtmRequest(com.oracle.xmlns.apps.otm.transmission.v6.PlannedShipmentType nPS, Transmission transmission, String payload) {
        String responseAction = "";

        try {
            List<global.blk.gpshub.model.PlannedShipmentInfo> shipments = plannedShipmentInfoRepository.findByShipmentId(nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid());
            global.blk.gpshub.model.PlannedShipmentInfo nShipment = null;
            String pSJson = "";
            String response = "";
            if (shipments.size() > 0) {
                LOGGER.info("Size: {} /// {}", nPS.getShipment().getShipmentStatus().size(), nPS.getShipment().getShipmentStatus().size() > 0);
                if (nPS.getShipment().getShipmentStatus().size() > 0) {
                    int totalShipmenStatus = nPS.getShipment().getShipmentStatus().size();
                    String updShipmentId = nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid();
                    if (nPS.getShipment().getShipmentStatus().get(totalShipmenStatus - 1).getStatusCodeGid().getGid().getXid().equals("CA")) {
                        responseAction = "Cancel - UpdateShipmentStatus";
                        pSJson = gpsHubService.updateShipmentStatusJson(nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid(), "CA");
                        PlannedShipmentEvent nShipmentStatusEvent = new PlannedShipmentEvent();
                        nShipmentStatusEvent.setShipmentId(updShipmentId);
                        nShipmentStatusEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                        nShipmentStatusEvent.setXmlOtm(payload);
                        nShipmentStatusEvent.setXmlOtm(" ");
                        nShipmentStatusEvent.setFechaCreacion(LocalDateTime.now());
                        nShipmentStatusEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                        nShipmentStatusEvent.setSentToFc(false);
                        nShipmentStatusEvent.setResponseFc(" ");
                        nShipmentStatusEvent.setAction(" ");
                        nShipmentStatusEvent = plannedShipmentEventRepository.save(nShipmentStatusEvent);

                        if(env.getProperty("spring.profiles.active").equals("prod")) {
                            response = gpsHubService.updateShipmentStatus(pSJson);
                        } else {
                            response = "200 - Mocked Response";
                        }

                        nShipmentStatusEvent.setResponseFc(response);
                        nShipmentStatusEvent.setSentToFc(response.startsWith("200") ? true : false);
                        nShipmentStatusEvent.setAction(responseAction);
                        plannedShipmentEventRepository.save(nShipmentStatusEvent);
                    } else if (nPS.getShipment().getShipmentStatus().get(totalShipmenStatus - 1).getStatusCodeGid().getGid().getXid().equals("D1")) {
                        responseAction = "Delivery - UpdateShipmentStatus";
                        pSJson = gpsHubService.updateShipmentStatusJson(nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid(), "D1");
                        PlannedShipmentEvent nShipmentStatusEvent = new PlannedShipmentEvent();
                        nShipmentStatusEvent.setShipmentId(updShipmentId);
                        nShipmentStatusEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                        nShipmentStatusEvent.setXmlOtm(payload);
                        nShipmentStatusEvent.setXmlOtm(" ");
                        nShipmentStatusEvent.setFechaCreacion(LocalDateTime.now());
                        nShipmentStatusEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                        nShipmentStatusEvent.setSentToFc(false);
                        nShipmentStatusEvent.setResponseFc(" ");
                        nShipmentStatusEvent.setAction(" ");
                        nShipmentStatusEvent = plannedShipmentEventRepository.save(nShipmentStatusEvent);

                        if(env.getProperty("spring.profiles.active").equals("prod")) {
                            response = gpsHubService.updateShipmentStatus(pSJson);
                        } else {
                            response = "200 - Mocked Response";
                        }

                        nShipmentStatusEvent.setResponseFc(response);
                        nShipmentStatusEvent.setSentToFc(response.startsWith("200") ? true : false);
                        nShipmentStatusEvent.setAction(responseAction);
                        plannedShipmentEventRepository.save(nShipmentStatusEvent);
                    } else {
                        responseAction = "Update - UpdatePlannedShipment";
                        pSJson = gpsHubService.createPlannedShipmentJson((PlannedShipmentType) transmission.getTransmissionBody().getGLogXMLElement().get(0).getGLogXMLTransaction().getValue());
                        PlannedShipmentEvent nShipmentEvent = new PlannedShipmentEvent();
                        nShipmentEvent.setShipmentId(updShipmentId);
                        nShipmentEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                        nShipmentEvent.setXmlOtm(PrettyPrint.prettyPrintXml(payload));
                        nShipmentEvent.setFechaCreacion(LocalDateTime.now());
                        nShipmentEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                        nShipmentEvent.setSentToFc(false);
                        nShipmentEvent.setResponseFc(" ");
                        nShipmentEvent.setAction(" ");
                        nShipmentEvent = plannedShipmentEventRepository.save(nShipmentEvent);

                        if(env.getProperty("spring.profiles.active").equals("prod")) {
                            response = gpsHubService.updatePlannedShipment(pSJson);
                        } else {
                            response = "200 - Mocked Response";
                        }

                        nShipmentEvent.setResponseFc(response);
                        nShipmentEvent.setSentToFc(response.startsWith("200") ? true : false);
                        nShipmentEvent.setAction(responseAction);
                        plannedShipmentEventRepository.save(nShipmentEvent);
                    }
                }
            } else {
                responseAction = "Insert - CreatePlannedShipment";
                String newShipmentId = nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid();
                pSJson = gpsHubService.createPlannedShipmentJson((PlannedShipmentType) transmission.getTransmissionBody().getGLogXMLElement().get(0).getGLogXMLTransaction().getValue());
                nShipment = new global.blk.gpshub.model.PlannedShipmentInfo();
                nShipment.setOrderRelease("BLK." + nPS.getShipment().getRelease().get(0).getReleaseGid().getGid().getXid());
                nShipment.setShipmentId(newShipmentId);
                nShipment.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                nShipment.setFechaCreacion(LocalDateTime.now());
                plannedShipmentInfoRepository.save(nShipment);

                PlannedShipmentEvent nShipmentEvent = new PlannedShipmentEvent();
                nShipmentEvent.setShipmentId(newShipmentId);
                nShipmentEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                nShipmentEvent.setXmlOtm(PrettyPrint.prettyPrintXml(payload));
                nShipmentEvent.setFechaCreacion(LocalDateTime.now());
                nShipmentEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                nShipmentEvent.setSentToFc(false);
                nShipmentEvent.setResponseFc(" ");
                nShipmentEvent.setAction(" ");
                nShipmentEvent = plannedShipmentEventRepository.save(nShipmentEvent);

                if(env.getProperty("spring.profiles.active").equals("prod")) {
                    response = gpsHubService.createPlannedShipment(pSJson);
                } else {
                    response = "200 - Mocked Response";
                }

                nShipmentEvent.setResponseFc(response);
                nShipmentEvent.setSentToFc(response.startsWith("200") ? true : false);
                nShipmentEvent.setAction(responseAction);
                plannedShipmentEventRepository.save(nShipmentEvent);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Async("processExecutor")
    public void processFCRequest(global.blk.gpshub.model.ShipmentStatus nSStatus, JsonNode obj) {
        String sSOtmResponse = "";

        try {
            com.oracle.xmlns.apps.otm.transmission.v6.ShipmentStatusType shipmentStatusToOtm = new com.oracle.xmlns.apps.otm.transmission.v6.ShipmentStatusType();
            shipmentStatusToOtm.setShipmentGid(new GLogXMLGidType());
            shipmentStatusToOtm.getShipmentGid().setGid(new GidType());
            shipmentStatusToOtm.getShipmentGid().getGid().setDomainName(obj.get("shipmentId").asText().substring(0, obj.get("shipmentId").asText().indexOf(".")));
            shipmentStatusToOtm.getShipmentGid().getGid().setXid(obj.get("shipmentId").asText().substring(obj.get("shipmentId").asText().indexOf(".") + 1));

            shipmentStatusToOtm.setStatusCodeGid(new GLogXMLGidType());
            shipmentStatusToOtm.getStatusCodeGid().setGid(new GidType());
            EventEntry currEvEntry = eventEntryRepository.findByStatusCode(obj.get("statusUpdateType").asText());
            if (currEvEntry == null) {
                shipmentStatusToOtm.getStatusCodeGid().getGid().setXid("_");
                shipmentStatusToOtm.setStatusLevel("SHIPMENT");
            } else {
                shipmentStatusToOtm.getStatusCodeGid().getGid().setXid(currEvEntry.getStatusCode());
                shipmentStatusToOtm.setStatusLevel("SHIPMENT");
            }
            EventGroupType eG = new EventGroupType();
            eG.setEventGroupGid(new GLogXMLGidType());
            eG.getEventGroupGid().setGid(new GidType());
            eG.getEventGroupGid().getGid().setXid(currEvEntry.getEventGroup());
            eG.setEventGroupDescription(currEvEntry.getEventGroup());
            shipmentStatusToOtm.getEventGroup().add(eG);
            StatusGroupType sG = new StatusGroupType();
            sG.setStatusGroupGid(new GLogXMLGidType());
            sG.getStatusGroupGid().setGid(new GidType());
            sG.getStatusGroupGid().getGid().setXid(currEvEntry.getStatusGroup());
            sG.setStatusGroupDescription(currEvEntry.getStatusGroup());
            shipmentStatusToOtm.setStatusGroup(sG);

            ShipmentRefnumType sRN = new ShipmentRefnumType();
            sRN.setShipmentRefnumQualifierGid(new GLogXMLGidType());
            sRN.getShipmentRefnumQualifierGid().setGid(new GidType());
            sRN.getShipmentRefnumQualifierGid().getGid().setXid("GLOG");
            sRN.setShipmentRefnumValue(obj.get("shipmentId").asText());
            shipmentStatusToOtm.getShipmentRefnum().add(sRN);

            GLogXMLGidType sRCG = new GLogXMLGidType();
            sRCG.setGid(new GidType());
            sRCG.getGid().setXid("NS");
            shipmentStatusToOtm.setStatusReasonCodeGid(sRCG);

            StatusCodeType sSCG = new StatusCodeType();
            sSCG.setStatusCodeGid(new GLogXMLGidType());
            sSCG.getStatusCodeGid().setGid(new GidType());
            sSCG.getStatusCodeGid().getGid().setXid(currEvEntry.getStatusCode());
            shipmentStatusToOtm.setStatusCodeGid(sSCG.getStatusCodeGid());

            ReasonGroupType sRG = new ReasonGroupType();
            sRG.setReasonGroupGid(new GLogXMLGidType());
            sRG.getReasonGroupGid().setGid(new GidType());
            sRG.getReasonGroupGid().getGid().setXid("NO_ERROR");
            sRG.setReasonGroupDescription("No_error");
            shipmentStatusToOtm.setReasonGroup(sRG);

            GLogXMLGidType sRPG = new GLogXMLGidType();
            sRPG.setGid(new GidType());
            sRPG.getGid().setXid("CARRIER");
            shipmentStatusToOtm.setResponsiblePartyGid(sRPG);

            shipmentStatusToOtm.setEventDt(new GLogDateTimeType());
            shipmentStatusToOtm.getEventDt().setGLogDate(LocalDateTime.parse(obj.get("statusUpdateDateTime").asText()).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
            shipmentStatusToOtm.getEventDt().setTZId("America/Mexico_City");
            shipmentStatusToOtm.getEventDt().setTZOffset(obj.get("statusUpdateTimeZone").asText());

            shipmentStatusToOtm.setTimeZoneGid(new GLogXMLGidType());
            shipmentStatusToOtm.getTimeZoneGid().setGid(new GidType());
            shipmentStatusToOtm.getTimeZoneGid().getGid().setXid("America/Mexico_City");

            shipmentStatusToOtm.setSSStop(new SSStopType());
            shipmentStatusToOtm.getSSStop().setSSLocation(new SSLocationType());
            shipmentStatusToOtm.getSSStop().getSSLocation().setLocationRefnumQualifierGid(new GLogXMLGidType());
            shipmentStatusToOtm.getSSStop().getSSLocation().getLocationRefnumQualifierGid().setGid(new GidType());
            shipmentStatusToOtm.getSSStop().getSSLocation().getLocationRefnumQualifierGid().getGid().setXid("GLOG");
            if (currEvEntry.getEventGroup().equals("ARRIVED") && currEvEntry.getEventGroup().equals("DEPARTED")) {
                shipmentStatusToOtm.getSSStop().setSSStopSequenceNum(obj.get("stopId").asText());
            } else {
                if (obj.get("stopId").asInt() != 0) {
                    shipmentStatusToOtm.getSSStop().setSSStopSequenceNum(obj.get("stopId").asText());
                }
            }
            shipmentStatusToOtm.getSSStop().getSSLocation().setLatitude(obj.get("latitude").asText());
            shipmentStatusToOtm.getSSStop().getSSLocation().setLongitude(obj.get("longitude").asText());

            com.oracle.xmlns.apps.otm.transmission.v6.Transmission transmissionToOtm = new com.oracle.xmlns.apps.otm.transmission.v6.Transmission();
            transmissionToOtm.setTransmissionBody(new Transmission.TransmissionBody());
            GLogXMLElementType glogElement = new GLogXMLElementType();
            glogElement.setGLogXMLTransaction(new JAXBElement<ShipmentStatusType>(QName.valueOf(getAddressFromEndpointReference(((BindingProvider) new TransmissionService().getTransmission()).getEndpointReference().toString())), ShipmentStatusType.class, shipmentStatusToOtm));
            transmissionToOtm.getTransmissionBody().getGLogXMLElement().add(glogElement);

            String strTransmission = "";
            JAXBContext jcTransmission = JAXBContext.newInstance(com.oracle.xmlns.apps.otm.transmission.v6.Transmission.class);
            Marshaller mTransmission = jcTransmission.createMarshaller();
            StringWriter swTransmission = new StringWriter();
            mTransmission.marshal(transmissionToOtm, swTransmission);
            JAXBContext jcTransmissionV6 = JAXBContext.newInstance(com.oracle.xmlns.apps.otm.transmission.v6.Transmission.class);
            Unmarshaller uTransmissionV6 = jcTransmissionV6.createUnmarshaller();
            com.oracle.xmlns.apps.otm.transmission.v6.Transmission transmissionV6Otm = ((com.oracle.xmlns.apps.otm.transmission.v6.Transmission) uTransmissionV6.unmarshal(new InputSource(new StringReader(swTransmission.toString()))));

            nSStatus.setShipmentXmlOtm(PrettyPrint.prettyPrintXml(swTransmission.toString()));
            nSStatus = shipmentStatusRepository.save(nSStatus);

            com.oracle.xmlns.apps.otm.transmission.v6.TransmissionAck response = new com.oracle.xmlns.apps.otm.transmission.v6.TransmissionAck();
            TransmissionService service = new TransmissionService();
            service.setHandlerResolver(new HeaderHandlerResolver());
            TransmissionPortType transmissionPortType = service.getTransmission();
            ((BindingProvider) transmissionPortType).getRequestContext().put("user", otmConfig.getUser());
            ((BindingProvider) transmissionPortType).getRequestContext().put("password", otmConfig.getPwd());
            ((BindingProvider) transmissionPortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, getAddressFromEndpointReference(((BindingProvider) transmissionPortType).getEndpointReference().toString()));

            String responseXml = "";
            if(env.getProperty("spring.profiles.active").equals("prod")) {
                response = transmissionPortType.publish(transmissionV6Otm);

                JAXBContext jcTransmissionAck = JAXBContext.newInstance(com.oracle.xmlns.apps.otm.transmission.v6.TransmissionAck.class);
                Marshaller mTransmissionAck = jcTransmissionAck.createMarshaller();
                StringWriter swTransmissionAck = new StringWriter();
                mTransmissionAck.marshal(response, swTransmissionAck);
                responseXml = swTransmissionAck.toString();
            } else {
                responseXml = "200 - Mocked Response";
            }

            nSStatus.setResponse(PrettyPrint.prettyPrintXml(responseXml));
            nSStatus.setSendToOtm(true);

            TransmissionAck tAck = (TransmissionAck) JAXBContext.newInstance(TransmissionAck.class).createUnmarshaller().unmarshal(new InputSource(new StringReader(responseXml)));
            nSStatus.setTransmitionId(tAck.getEchoedTransmissionHeader().getTransmissionHeader().getReferenceTransmissionNo());
            nSStatus = shipmentStatusRepository.save(nSStatus);

            sSOtmResponse = "Snapshot saved with Transmition ID: " + tAck.getEchoedTransmissionHeader().getTransmissionHeader().getReferenceTransmissionNo();
            LOGGER.info(sSOtmResponse);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /* Utils */
    public String getAddressFromEndpointReference(String reference) {
        try {
            strAddr = "";
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                boolean isAddr = false;


                public void startElement(String uri, String localName, String qName,
                                         Attributes attributes) throws SAXException {
                    if (qName.contains("Address")) {
                        isAddr = true;
                    }
                }

                public void endElement(String uri, String localName,
                                       String qName) throws SAXException {
                    if (qName.contains("Address")) {
                        isAddr = false;
                    }
                }

                public void characters(char ch[], int start, int length) throws SAXException {
                    if (isAddr) {
                        strAddr = new String(ch, start, length);
                    }
                }

            };
            saxParser.parse(new ByteArrayInputStream(reference.getBytes()), handler);
        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.error(ex.getLocalizedMessage());
        }
        return strAddr;
    }

}

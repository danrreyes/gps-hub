package global.blk.gpshub.repository;

import global.blk.gpshub.model.ShipmentStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShipmentStatusRepository extends JpaRepository<ShipmentStatus, Long> {

    public List<ShipmentStatus> findBySendToOtmIsFalse();

}

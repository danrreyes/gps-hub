package global.blk.gpshub.repository;

import global.blk.gpshub.model.PlannedShipmentEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlannedShipmentEventRepository extends JpaRepository<PlannedShipmentEvent, Long> {
}

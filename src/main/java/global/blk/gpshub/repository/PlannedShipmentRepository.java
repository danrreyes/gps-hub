package global.blk.gpshub.repository;

import global.blk.gpshub.model.PlannedShipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlannedShipmentRepository extends JpaRepository<PlannedShipment, Long> {

    public List<PlannedShipment> findByEnviadoIsFalse();

    public List<PlannedShipment> findByShipmentGid(String ShipmentGid);

}

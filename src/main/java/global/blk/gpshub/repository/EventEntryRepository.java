package global.blk.gpshub.repository;

import global.blk.gpshub.model.EventEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventEntryRepository extends JpaRepository<EventEntry, Long> {

    public EventEntry findByStatusCode(String statusCode);

}

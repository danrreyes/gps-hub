package global.blk.gpshub.repository;

import global.blk.gpshub.model.PlannedShipmentInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlannedShipmentInfoRepository extends JpaRepository<PlannedShipmentInfo, Long> {

    public List<PlannedShipmentInfo> findByShipmentId(String shipmentId);

}

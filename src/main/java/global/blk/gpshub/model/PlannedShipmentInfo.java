package global.blk.gpshub.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "TAPLANNEDSHIPMENTSNEW", schema = "TRAZABILIDAD")
public class PlannedShipmentInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="FIPSNID")
    private Long id;
    @Column(name="FCTRANSMITIONID")
    private String transmitionId;
    @Column(name="FCSHIPMENTGID")
    private String shipmentId;
    @Column(name="FCORDERRELEASE")
    private String orderRelease;
    @Column(name="FDFECHA", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", updatable = false)
    private LocalDateTime fechaCreacion;

    public PlannedShipmentInfo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransmitionId() {
        return transmitionId;
    }

    public void setTransmitionId(String transmitionId) {
        this.transmitionId = transmitionId;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getOrderRelease() {
        return orderRelease;
    }

    public void setOrderRelease(String orderRelease) {
        this.orderRelease = orderRelease;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

}

package global.blk.gpshub.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "TAPLANNEDSHIPMENTS", schema = "TRAZABILIDAD")
public class PlannedShipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FIPSID")
    private Long id;
    @Column(name = "FCTRANSMITIONID")
    private String transmitionId;
    @Column(name = "FCSHIPMENTGID")
    private String shipmentGid;
    @Column(name = "FCSHIPMENTXMLDEOTM")
    private String shipmentXml;
    @Column(name = "FIENVIADO")
    private boolean enviado;
    @Column(name = "FDFECHA", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", updatable = false)
    private LocalDateTime fecha;
    @Column(name = "FCSHIPMENTJSONAFC")
    private String shipmentJson;
    @Column(name = "FCRESPONSE")
    private String response;
    @Column(name = "FCORDERRELEASE")
    private String orderRelease;
    @Column(name = "FCACTION")
    private String action;

    public PlannedShipment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransmitionId() {
        return transmitionId;
    }

    public void setTransmitionId(String transmitionId) {
        this.transmitionId = transmitionId;
    }

    public String getShipmentGid() {
        return shipmentGid;
    }

    public void setShipmentGid(String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public boolean isEnviado() {
        return enviado;
    }

    public void setEnviado(boolean enviado) {
        this.enviado = enviado;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getShipmentXml() {
        return shipmentXml;
    }

    public void setShipmentXml(String shipmentXml) {
        this.shipmentXml = shipmentXml;
    }

    public String getShipmentJson() {
        return shipmentJson;
    }

    public void setShipmentJson(String shipmentJson) {
        this.shipmentJson = shipmentJson;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getOrderRelease() {
        return orderRelease;
    }

    public void setOrderRelease(String orderRelease) {
        this.orderRelease = orderRelease;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}

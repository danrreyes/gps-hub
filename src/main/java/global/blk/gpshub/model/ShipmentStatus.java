package global.blk.gpshub.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "TASHIPMENTSTATUS", schema = "TRAZABILIDAD")
public class ShipmentStatus {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FISSID")
    private Long id;
    @Column(name = "FCTRANSMITIONID")
    private String transmitionId;
    @Column(name = "FCSHIPMENTGID")
    private String shipmentGid;
    @Column(name = "FCSHIPMENTDEFC")
    private String shipmentXml;
    @Column(name = "FIENVIADO")
    private boolean sendToOtm;
    @Column(name = "FDFECHA", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", updatable = false)
    private LocalDateTime fecha;
    @Column(name = "FCSHIPMENTXMLAOTM")
    private String shipmentXmlOtm;
    @Column(name = "FCRESPONSE")
    private String response;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransmitionId() {
        return transmitionId;
    }

    public void setTransmitionId(String transmitionId) {
        this.transmitionId = transmitionId;
    }

    public String getShipmentGid() {
        return shipmentGid;
    }

    public void setShipmentGid(String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getShipmentXml() {
        return shipmentXml;
    }

    public void setShipmentXml(String shipmentXml) {
        this.shipmentXml = shipmentXml;
    }

    public boolean isSendToOtm() {
        return sendToOtm;
    }

    public void setSendToOtm(boolean sendToOtm) {
        this.sendToOtm = sendToOtm;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getShipmentXmlOtm() {
        return shipmentXmlOtm;
    }

    public void setShipmentXmlOtm(String shipmentXmlOtm) {
        this.shipmentXmlOtm = shipmentXmlOtm;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}

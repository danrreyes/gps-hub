package global.blk.gpshub.model;

import javax.persistence.*;

@Entity
@Table(name = "TAEVENTS", schema = "TRAZABILIDAD")
public class EventEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FIEVENTID")
    private Long id;
    @Column(name = "FCSTATUSGROUPID")
    private String statusGroup;
    @Column(name = "FCSTATUSCODEGID")
    private String statusCode;
    @Column(name = "FCEVENTGROUPID")
    private String eventGroup;

    public EventEntry() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatusGroup() {
        return statusGroup;
    }

    public void setStatusGroup(String statusGroup) {
        this.statusGroup = statusGroup;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getEventGroup() {
        return eventGroup;
    }

    public void setEventGroup(String eventGroup) {
        this.eventGroup = eventGroup;
    }

}

package global.blk.gpshub.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "TAEVENTSXPLANNEDSHIPMENT", schema = "TRAZABILIDAD")
public class PlannedShipmentEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="FIEPSID")
    private String id;
    @Column(name="FCSHIPMENTGID")
    private String shipmentId;
    @Column(name="FCTRANSMITIONID")
    private String transmitionId;
    @Column(name="FCSHIPMENTXMLDEOTM")
    private String xmlOtm;
    @Column(name="FIENVIADO")
    private boolean sentToFc;
    @Column(name="FCSHIPMENTJSONAFC")
    private String jsonToFC;
    @Column(name="FCRESPONSE")
    private String responseFc;
    @Column(name="FCACTION")
    private String action;
    @Column(name="FDFECHA", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", updatable = false)
    private LocalDateTime fechaCreacion;

    public PlannedShipmentEvent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getTransmitionId() {
        return transmitionId;
    }

    public void setTransmitionId(String transmitionId) {
        this.transmitionId = transmitionId;
    }

    public String getXmlOtm() {
        return xmlOtm;
    }

    public void setXmlOtm(String xmlOtm) {
        this.xmlOtm = xmlOtm;
    }

    public boolean isSentToFc() {
        return sentToFc;
    }

    public void setSentToFc(boolean sentToFc) {
        this.sentToFc = sentToFc;
    }

    public String getJsonToFC() {
        return jsonToFC;
    }

    public void setJsonToFC(String jsonToFC) {
        this.jsonToFC = jsonToFC;
    }

    public String getResponseFc() {
        return responseFc;
    }

    public void setResponseFc(String responseFc) {
        this.responseFc = responseFc;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

}

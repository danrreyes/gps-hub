package global.blk.gpshub.service.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.oracle.xmlns.apps.otm.transmission.v6.PlannedShipmentType;
import com.oracle.xmlns.apps.otm.transmission.v6.LocationType;
import com.oracle.xmlns.apps.otm.transmission.v6.ShipmentStopType;
import global.blk.gpshub.config.FleetCompleteConfig;
import global.blk.gpshub.service.GpsHubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;

@Service
public class GpsHubServiceImpl implements GpsHubService {

    private Logger LOGGER = LoggerFactory.getLogger(GpsHubServiceImpl.class);
    private FleetCompleteConfig fleetCompleteConfig;

    public GpsHubServiceImpl(FleetCompleteConfig fleetCompleteConfig) {
        this.fleetCompleteConfig = fleetCompleteConfig;
    }

    @Override
    public String consumePlannedShipment() {
        return null;
    }

    @Override
    public String createPlannedShipment(String plannedShipmentJson) {
        RestTemplate restTemplate = configRestTemplateToHttps();
        RequestEntity<String> requestEntity = null;
        ResponseEntity<String> responseEntity = null;
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        String responseBody = "";

        try {
            String endpoint = fleetCompleteConfig.getHost() + "/createPlannedShipment";
            MultiValueMap<String, String> headers = getHeaders();
            requestEntity = new RequestEntity<>(plannedShipmentJson,
                    headers, HttpMethod.POST, new URI(endpoint));
            responseEntity = restTemplate.exchange(requestEntity, String.class);
            responseBody = responseEntity.getStatusCodeValue() + " - " + responseEntity.getBody();
        } catch (HttpClientErrorException.BadRequest ex) {
            responseBody = ex.getRawStatusCode() + " - ";
            if(ex.getResponseBodyAsString() != null){ responseBody += ex.getResponseBodyAsString(); }
        } catch (Exception ex) {
            responseBody = "";
            ex.printStackTrace();
        }

        return responseBody;
    }

    @Override
    public String updatePlannedShipment(String plannedShipmentJson) {
        RestTemplate restTemplate = configRestTemplateToHttps();
        RequestEntity<String> requestEntity = null;
        ResponseEntity<String> responseEntity = null;
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        String responseBody = "";

        try {
            //String endpoint = "http://54.219.187.255/BlkOtmApi/updatePlannedShipment";
            String endpoint = fleetCompleteConfig.getHost() + "/updatePlannedShipment";
            MultiValueMap<String, String> headers = getHeaders();
            requestEntity = new RequestEntity<>(plannedShipmentJson,
                    headers, HttpMethod.PUT, new URI(endpoint));
            responseEntity = restTemplate.exchange(requestEntity, String.class);
            responseBody = responseEntity.getStatusCodeValue() + " - " + responseEntity.getBody();
        } catch (HttpClientErrorException.BadRequest ex) {
            responseBody = ex.getRawStatusCode() + " - ";
            if(ex.getResponseBodyAsString() != null){ responseBody += ex.getResponseBodyAsString(); }
        } catch (Exception ex) {
            responseBody = "";
            ex.printStackTrace();
        }

        return responseBody;
    }

    @Override
    public String createPlannedShipmentJson(PlannedShipmentType plannedShipment) {
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        String payload = "";

        try {
            ObjectNode _plannedShipment = mapper.createObjectNode();
            _plannedShipment.put("shipmentId", plannedShipment.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + plannedShipment.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid());
            ArrayNode _segments = mapper.createArrayNode();
            ArrayNode _stops = mapper.createArrayNode();
            ObjectNode _segment = mapper.createObjectNode();
            _segment.put("segmentId", "1");
            _segment.put("gpsFrequency", plannedShipment.getShipment().getShipmentHeader().getFlexFieldNumbers().getAttributeNumber1());
            ArrayNode _devices = mapper.createArrayNode();

            if (plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute5() != null || plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute6() != null) {
                ObjectNode _device = mapper.createObjectNode();
                _device.put("imei", plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute6());
                _device.put("isPrimary", "0");
                _devices.add(_device);
                _device = mapper.createObjectNode();
                _device.put("imei", plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute3());
                _device.put("isPrimary", "0");
                _devices.add(_device);
                _device = mapper.createObjectNode();
                _device.put("imei", plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute4());
                _device.put("isPrimary", "1");
                _devices.add(_device);
            } else if (plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute3() != null) {
                ObjectNode _device = mapper.createObjectNode();
                _device.put("imei", plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute3());
                _device.put("isPrimary", "0");
                _devices.add(_device);
                _device = mapper.createObjectNode();
                _device.put("imei", plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute4());
                _device.put("isPrimary", "1");
                _devices.add(_device);
            } else {
                ObjectNode _device = mapper.createObjectNode();
                _device.put("imei", plannedShipment.getShipment().getShipmentHeader().getFlexFieldStrings().getAttribute4());
                _device.put("isPrimary", "1");
                _devices.add(_device);
            }
            _segment.set("devices", _devices);
            LocationType sL = getLocationByXid(plannedShipment.getShipment().getLocation(), plannedShipment.getShipment().getShipmentHeader().getSourceLocationRef().getLocationRef().getLocationGid().getGid().getXid());
            String radiusGen = plannedShipment.getShipment().getShipmentStop().get(0).getFlexFieldNumbers().getAttributeNumber1();
            ObjectNode _poiSource = mapper.createObjectNode();
            _poiSource.put("latitude", sL.getAddress().getLatitude());
            _poiSource.put("longitude", sL.getAddress().getLongitude());
            _poiSource.put("radius", radiusGen);
            _segment.set("poiSource", _poiSource);
            LocalDateTime sDT = LocalDateTime.parse(plannedShipment.getShipment().getShipmentHeader().getStartDt().getGLogDate(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
            _segment.put("startDateTime", sDT.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
            _segment.put("startTimeZone", plannedShipment.getShipment().getShipmentHeader().getStartDt().getTZOffset());

            LocationType dL = getLocationByXid(plannedShipment.getShipment().getLocation(), plannedShipment.getShipment().getShipmentHeader().getDestinationLocationRef().getLocationRef().getLocationGid().getGid().getXid());
            ObjectNode _poiDest = mapper.createObjectNode();
            _poiDest.put("latitude", dL.getAddress().getLatitude());
            _poiDest.put("longitude", dL.getAddress().getLongitude());
            _poiDest.put("radius", radiusGen);
            _segment.set("poiDestination", _poiDest);
            LocalDateTime eDT = LocalDateTime.parse(plannedShipment.getShipment().getShipmentHeader().getEndDt().getGLogDate(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
            _segment.put("endDateTime", eDT.format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
            _segment.put("endTimeZone", plannedShipment.getShipment().getShipmentHeader().getEndDt().getTZOffset());

            for (ShipmentStopType sS : plannedShipment.getShipment().getShipmentStop()) {
                ObjectNode _stop = mapper.createObjectNode();
                ObjectNode _poiStop = mapper.createObjectNode();
                _stop.put("stopId", sS.getStopSequence());
                LocationType pL = getLocationByXid(plannedShipment.getShipment().getLocation(), sS.getLocationRef().getLocationGid().getGid().getXid());
                _poiStop.put("latitude", pL.getAddress().getLatitude());
                _poiStop.put("longitude", pL.getAddress().getLongitude());
                _poiStop.put("radius", sS.getFlexFieldNumbers().getAttributeNumber1());
                _stop.set("poiStop", _poiStop);
                _stop.put("startDateTime", LocalDateTime.parse(sS.getArrivalTime().getEventTime().getEstimatedTime().getGLogDate(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
                _stop.put("startTimeZone", sS.getArrivalTime().getEventTime().getEstimatedTime().getTZOffset());
                _stop.put("endDateTime", LocalDateTime.parse(sS.getDepartureTime().getEventTime().getEstimatedTime().getGLogDate(), DateTimeFormatter.ofPattern("yyyyMMddHHmmss")).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));
                _stop.put("endTimeZone", sS.getDepartureTime().getEventTime().getEstimatedTime().getTZOffset());
                _stops.add(_stop);
            }

            _segment.set("stops", _stops);
            _segments.add(_segment);
            _plannedShipment.set("segments", _segments);
            payload = mapper.writeValueAsString(_plannedShipment);
        } catch (Exception ex) {
            ex.printStackTrace();
            payload = "";
        }

        return payload;
    }

    @Override
    public String updateShipmentStatus(String updateShipmentJson) {
        RestTemplate restTemplate = configRestTemplateToHttps();
        RequestEntity<String> requestEntity = null;
        ResponseEntity<String> responseEntity = null;
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        String responseBody = "";

        try {
            String endpoint = fleetCompleteConfig.getHost() + "/updateShipmentStatus";
            MultiValueMap<String, String> headers = getHeaders();
            requestEntity = new RequestEntity<>(updateShipmentJson,
                    headers, HttpMethod.PUT, new URI(endpoint));
            responseEntity = restTemplate.exchange(requestEntity, String.class);
            responseBody = responseEntity.getStatusCodeValue() + " - " + responseEntity.getBody();
        } catch (HttpClientErrorException.BadRequest ex) {
            responseBody = ex.getRawStatusCode() + " - ";
            if(ex.getResponseBodyAsString() != null){ responseBody += ex.getResponseBodyAsString(); }
        } catch (Exception ex) {
            responseBody = "";
            ex.printStackTrace();
        }

        return responseBody;
    }

    @Override
    public String updateShipmentStatusJson(String shipmentGid, String statusCode) {
        return "{\"shipmentId\": \"" + shipmentGid + "\",\"updateType\": \"" + statusCode + "\"}";
    }

    /* Utils */
    public HttpHeaders getHeaders() {
        String usr = fleetCompleteConfig.getUser();
        String pwd = fleetCompleteConfig.getPwd();
        String plainAuth = usr + ":" + pwd;
        String b64Auth = Base64.getEncoder().encodeToString(plainAuth.getBytes());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + b64Auth);
        headers.add("Content-Type", "application/json");
        return headers;
    }

    public RestTemplate configRestTemplateToHttps() {
        /*CloseableHttpClient httpClient =
                HttpClients.custom()
                        .setSSLHostnameVerifier(new NoopHostnameVerifier())
                        .build();
        HttpComponentsClientHttpRequestFactory reqFactory =
                new HttpComponentsClientHttpRequestFactory();
        reqFactory.setHttpClient(httpClient);
        RestTemplate restTemplate = new RestTemplate(reqFactory);*/
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate;
    }

    public LocationType getLocationByXid(List<LocationType> locations, String locationXid){
        LocationType location = new LocationType();
        for (LocationType l : locations) {
            if (l.getLocationGid().getGid().getXid().equals(locationXid)) {
                location = l;
            }
        }
        return location;
    }

}

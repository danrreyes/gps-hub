package global.blk.gpshub.service;

import com.oracle.xmlns.apps.otm.transmission.v6.PlannedShipmentType;

public interface GpsHubService {

    public String consumePlannedShipment();

    public String createPlannedShipment(String plannedShipmentJson);

    public String updatePlannedShipment(String plannedShipmentJson);

    public String createPlannedShipmentJson(PlannedShipmentType plannedShipment);

    public String updateShipmentStatus(String updateShipmentJson);

    public String updateShipmentStatusJson(String shipmentGid, String statusCode);

}

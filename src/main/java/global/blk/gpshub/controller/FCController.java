package global.blk.gpshub.controller;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import global.blk.gpshub.config.OtmConfig;
import global.blk.gpshub.model.ShipmentStatus;
import global.blk.gpshub.repository.EventEntryRepository;
import global.blk.gpshub.repository.ShipmentStatusRepository;
import global.blk.gpshub.util.PrettyPrint;
import global.blk.gpshub.util.async.AsyncTasks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.time.LocalDateTime;

@RestController
@RequestMapping("inbound")
public class FCController {

    private final Logger LOGGER = LoggerFactory.getLogger(FCController.class);
    public String strAddr = "";
    private ShipmentStatusRepository shipmentStatusRepository;
    private EventEntryRepository eventEntryRepository;
    private OtmConfig otmConfig;
    private AsyncTasks asyncTasks;

    public FCController(ShipmentStatusRepository shipmentStatusRepository, EventEntryRepository eventEntryRepository, OtmConfig otmConfig, AsyncTasks asyncTasks) {
        this.shipmentStatusRepository = shipmentStatusRepository;
        this.eventEntryRepository = eventEntryRepository;
        this.otmConfig = otmConfig;
        this.asyncTasks = asyncTasks;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/shipmentStatus")
    public ResponseEntity<?> postPlannedShipments(@RequestBody String payload) {
        ShipmentStatus nSStatus = new ShipmentStatus();
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        String sSOtmResponse = "";

        try {
            JsonNode obj = mapper.readTree(factory.createParser(payload));
            nSStatus.setShipmentGid(obj.get("shipmentId").asText());
            nSStatus.setShipmentXml(PrettyPrint.prettyPrintJson(payload));
            nSStatus.setSendToOtm(false);
            nSStatus.setFecha(LocalDateTime.now());
            nSStatus.setTransmitionId("0");
            nSStatus = shipmentStatusRepository.save(nSStatus);

            asyncTasks.processFCRequest(nSStatus, obj);

            sSOtmResponse = "Snapshot saved.";
        } catch (Exception ex) {
            ex.printStackTrace();
            sSOtmResponse = "An internal error occured. Snapshot was not saved.";
            return new ResponseEntity<>(sSOtmResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(sSOtmResponse, HttpStatus.OK);
    }

    /* Utils */
    public String getAddressFromEndpointReference(String reference){
        try {
            strAddr = "";
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                boolean isAddr = false;


                public void startElement(String uri, String localName, String qName,
                                         Attributes attributes) throws SAXException {
                    if (qName.contains("Address")){
                        isAddr = true;
                    }
                }

                public void endElement(String uri, String localName,
                                       String qName) throws SAXException {
                    if (qName.contains("Address")){
                        isAddr = false;
                    }
                }

                public void characters(char ch[], int start, int length) throws SAXException {
                    if (isAddr){
                        strAddr = new String(ch, start, length);
                    }
                }

            };
            saxParser.parse(new ByteArrayInputStream(reference.getBytes()), handler);
        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.error(ex.getLocalizedMessage());
        }
        return strAddr;
    }

}

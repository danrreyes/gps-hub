package global.blk.gpshub.controller;

import com.oracle.xmlns.apps.otm.transmission.v6.PlannedShipmentType;
import com.oracle.xmlns.apps.otm.transmission.v6.Transmission;
import global.blk.gpshub.model.PlannedShipmentEvent;
import global.blk.gpshub.model.PlannedShipmentInfo;
import global.blk.gpshub.repository.PlannedShipmentEventRepository;
import global.blk.gpshub.repository.PlannedShipmentInfoRepository;
import global.blk.gpshub.service.GpsHubService;
import global.blk.gpshub.util.PrettyPrint;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Unmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.InputSource;

import javax.servlet.http.HttpServletRequest;
import java.io.StringReader;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("outbound")
public class OtmController {

    private final Logger LOGGER = LoggerFactory.getLogger(OtmController.class);
    private PlannedShipmentInfoRepository plannedShipmentInfoRepository;
    private PlannedShipmentEventRepository plannedShipmentEventRepository;
    private GpsHubService gpsHubService;
    @Autowired
    private Environment env;

    public OtmController(PlannedShipmentInfoRepository plannedShipmentInfoRepository, PlannedShipmentEventRepository plannedShipmentEventRepository, GpsHubService gpsHubService) {
        this.plannedShipmentInfoRepository = plannedShipmentInfoRepository;
        this.plannedShipmentEventRepository = plannedShipmentEventRepository;
        this.gpsHubService = gpsHubService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/plannedShipment")
    public ResponseEntity<?> postPlannedShipments(HttpServletRequest request, @RequestBody String payload){
        String responseAction = "";

        try {
            //String cXml = Normalizer.normalize(xml, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
            JAXBContext jc = JAXBContext.newInstance(Transmission.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            Transmission transmission = (Transmission) unmarshaller.unmarshal(new InputSource(new StringReader(payload)));
            com.oracle.xmlns.apps.otm.transmission.v6.PlannedShipmentType nPS = (PlannedShipmentType) transmission.getTransmissionBody().getGLogXMLElement().get(0).getGLogXMLTransaction().getValue();

            List<PlannedShipmentInfo> shipments = plannedShipmentInfoRepository.findByShipmentId(nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid());
            PlannedShipmentInfo nShipment = null;
            String pSJson = "";
            String response = "";
            if (shipments.size() > 0){
                LOGGER.info("Size: {} /// {}", nPS.getShipment().getShipmentStatus().size(), nPS.getShipment().getShipmentStatus().size() > 0);
                if(nPS.getShipment().getShipmentStatus().size() > 0){
                    int totalShipmenStatus = nPS.getShipment().getShipmentStatus().size();
                    String updShipmentId = nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid();
                    if(nPS.getShipment().getShipmentStatus().get(totalShipmenStatus-1).getStatusCodeGid().getGid().getXid().equals("CA")){
                        responseAction = "Cancel - UpdateShipmentStatus";
                        pSJson = gpsHubService.updateShipmentStatusJson(nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid(), "CA");
                        PlannedShipmentEvent nShipmentStatusEvent = new PlannedShipmentEvent();
                        nShipmentStatusEvent.setShipmentId(updShipmentId);
                        nShipmentStatusEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                        nShipmentStatusEvent.setXmlOtm(payload);
                        nShipmentStatusEvent.setXmlOtm(" ");
                        nShipmentStatusEvent.setFechaCreacion(LocalDateTime.now());
                        nShipmentStatusEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                        nShipmentStatusEvent.setSentToFc(false);
                        nShipmentStatusEvent.setResponseFc(" ");
                        nShipmentStatusEvent.setAction(" ");
                        nShipmentStatusEvent = plannedShipmentEventRepository.save(nShipmentStatusEvent);

                        if(env.getProperty("spring.profiles.active").equals("prod")) {
                            response = gpsHubService.updateShipmentStatus(pSJson);
                        } else {
                            response = "200 - Mocked Response";
                        }

                        nShipmentStatusEvent.setResponseFc(response);
                        nShipmentStatusEvent.setSentToFc(response.startsWith("200") ? true : false);
                        nShipmentStatusEvent.setAction(responseAction);
                        plannedShipmentEventRepository.save(nShipmentStatusEvent);
                    } else if(nPS.getShipment().getShipmentStatus().get(totalShipmenStatus-1).getStatusCodeGid().getGid().getXid().equals("D1")){
                        responseAction = "Delivery - UpdateShipmentStatus";
                        pSJson = gpsHubService.updateShipmentStatusJson(nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid(), "D1");
                        PlannedShipmentEvent nShipmentStatusEvent = new PlannedShipmentEvent();
                        nShipmentStatusEvent.setShipmentId(updShipmentId);
                        nShipmentStatusEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                        nShipmentStatusEvent.setXmlOtm(payload);
                        nShipmentStatusEvent.setXmlOtm(" ");
                        nShipmentStatusEvent.setFechaCreacion(LocalDateTime.now());
                        nShipmentStatusEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                        nShipmentStatusEvent.setSentToFc(false);
                        nShipmentStatusEvent.setResponseFc(" ");
                        nShipmentStatusEvent.setAction(" ");
                        nShipmentStatusEvent = plannedShipmentEventRepository.save(nShipmentStatusEvent);

                        if(env.getProperty("spring.profiles.active").equals("prod")) {
                            response = gpsHubService.updateShipmentStatus(pSJson);
                        } else {
                            response = "200 - Mocked Response";
                        }

                        nShipmentStatusEvent.setResponseFc(response);
                        nShipmentStatusEvent.setSentToFc(response.startsWith("200") ? true : false);
                        nShipmentStatusEvent.setAction(responseAction);
                        plannedShipmentEventRepository.save(nShipmentStatusEvent);
                    } else {
                        responseAction = "Update - UpdatePlannedShipment";
                        pSJson = gpsHubService.createPlannedShipmentJson((PlannedShipmentType) transmission.getTransmissionBody().getGLogXMLElement().get(0).getGLogXMLTransaction().getValue());
                        PlannedShipmentEvent nShipmentEvent = new PlannedShipmentEvent();
                        nShipmentEvent.setShipmentId(updShipmentId);
                        nShipmentEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                        nShipmentEvent.setXmlOtm(PrettyPrint.prettyPrintXml(payload));
                        nShipmentEvent.setFechaCreacion(LocalDateTime.now());
                        nShipmentEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                        nShipmentEvent.setSentToFc(false);
                        nShipmentEvent.setResponseFc(" ");
                        nShipmentEvent.setAction(" ");
                        nShipmentEvent = plannedShipmentEventRepository.save(nShipmentEvent);

                        if(env.getProperty("spring.profiles.active").equals("prod")) {
                            response = gpsHubService.updatePlannedShipment(pSJson);
                        } else {
                            response = "200 - Mocked Response";
                        }

                        nShipmentEvent.setResponseFc(response);
                        nShipmentEvent.setSentToFc(response.startsWith("200") ? true : false);
                        nShipmentEvent.setAction(responseAction);
                        plannedShipmentEventRepository.save(nShipmentEvent);
                    }
                }
            } else {
                responseAction = "Insert - CreatePlannedShipment";
                String newShipmentId = nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getDomainName() + "." + nPS.getShipment().getShipmentHeader().getShipmentGid().getGid().getXid();
                pSJson = gpsHubService.createPlannedShipmentJson((PlannedShipmentType) transmission.getTransmissionBody().getGLogXMLElement().get(0).getGLogXMLTransaction().getValue());
                nShipment = new PlannedShipmentInfo();
                nShipment.setOrderRelease("BLK." + nPS.getShipment().getRelease().get(0).getReleaseGid().getGid().getXid());
                nShipment.setShipmentId(newShipmentId);
                nShipment.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                nShipment.setFechaCreacion(LocalDateTime.now());
                plannedShipmentInfoRepository.save(nShipment);

                PlannedShipmentEvent nShipmentEvent = new PlannedShipmentEvent();
                nShipmentEvent.setShipmentId(newShipmentId);
                nShipmentEvent.setTransmitionId(transmission.getTransmissionHeader().getSenderTransmissionNo());
                nShipmentEvent.setXmlOtm(PrettyPrint.prettyPrintXml(payload));
                nShipmentEvent.setFechaCreacion(LocalDateTime.now());
                nShipmentEvent.setJsonToFC(PrettyPrint.prettyPrintJson(pSJson));
                nShipmentEvent.setSentToFc(false);
                nShipmentEvent.setResponseFc(" ");
                nShipmentEvent.setAction(" ");
                nShipmentEvent = plannedShipmentEventRepository.save(nShipmentEvent);

                if(env.getProperty("spring.profiles.active").equals("prod")) {
                    response = gpsHubService.createPlannedShipment(pSJson);
                } else {
                    response = "200 - Mocked Response";
                }

                nShipmentEvent.setResponseFc(response);
                nShipmentEvent.setSentToFc(response.startsWith("200") ? true : false);
                nShipmentEvent.setAction(responseAction);
                plannedShipmentEventRepository.save(nShipmentEvent);
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }

        return new ResponseEntity<>(responseAction, HttpStatus.OK);
    }



}